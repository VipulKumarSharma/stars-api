package com.mind.api.stars;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StarsApiApplicationTests {

	@Test
	public void contextLoads() {
		assertTrue(1 > 0);
	}

}
