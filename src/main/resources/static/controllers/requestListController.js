app.controller("requestListController", ['$scope', '$http', '$sce', '$window', 'apiCallService', function($scope, $http, $sce, $window, apiCallService) {
	
	$scope.travelTypeHidden 			= "";
	$scope.requestTypeGlobal    		= "";
	$scope.approvedRequestsCount 		= "";
	$scope.temporaryRequestsCount 		= "";
	$scope.requestsInWorkflowCount 		= "";
	$scope.returnedRequestsCount 		= "";
	$scope.rejectedRequestsCount 		= "";
	$scope.cancelledRequestsCount 		= "";
	$scope.yearlyRequestApprovedCount 	= "";
	$scope.yearlyRequestCreatedCount 	= "";
	$scope.requestType					= "";
	$scope.requestDetails 		 		= "";
	$scope.commentDesc					= "";
	$scope.cancelCommentDesc			= "";
	$scope.traveller 					= "";
	$scope.travelDetails  				= "";
	$scope.approversDivHtml 			= "";
	$scope.timelineDetailsDiv 			= "";
	$scope.requestMailFlow 				= "";
	$scope.commentDescBox				= "";
	$scope.mailBody 					= "";
	$scope.requestList					= [];
	$scope.commentsList 		 		= [];
	$scope.cancelledCommentsList	 	= [];
	$scope.attachmentList				= [];
	$scope.commentsListDetailsSection 	= [];
	$scope.approversList   				= [];
	$scope.journeys						= [];
	$scope.cancelledCommentsListDetailsSection 	= [];
	
	const monthNames 			= ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
	
	/************************************************* UTILITY METHODS *********************************************************/
	
	$scope.convert24To12HoursTimeFormat = function(time) {
    	try {
    		time = time.toString().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    	    if (time.length > 1) {
    	    	time 	= time.slice(1);
    	    	time[5] = +time[0] < 12 ? ' AM' : ' PM';
    	    	time[0] = +time[0] % 12 || 12;
    	    }
    	    return time.join('');
        	
    	} catch(e) {
    		return "12:00 PM";
    	}
    };
    
    $scope.getDateTimeObject = function(dateString, timeString, is12HoursFormat) {
    	var year = 1900;
    	var month = 01;
    	var day = 01;
    	var hour = 00;
    	var minutes = 00;
    	
    	if(((dateString).trim()).indexOf(" ") != -1) {
    		var dateArr = dateString.split(" ");
    		day = dateArr[0];
    		month = monthNames.indexOf(dateArr[1]);
    		year = dateArr[2];
    	}
    	
    	if(timeString.indexOf(":") != -1) {
    		
    		var tmp_timeString = timeString;
    		if(is12HoursFormat) {
    			tmp_timeString = $scope.convert12To24HoursTimeFormat(tmp_timeString);
    		}
    		
    		var timeArr = tmp_timeString.split(":");
    		
    		hour = timeArr[0];
    		minutes = timeArr[1];
    	}
    	
    	return new Date(year, month, day, hour, minutes, 00);
    };
    
    $scope.convert12To24HoursTimeFormat = function(time) {
    	try {
    		time = time.toString();
    		
        	var hours = Number(time.match(/^(\d+)/)[1]);
        	var minutes = Number(time.match(/:(\d+)/)[1]);
        	var AMPM = time.match(/\s(.*)$/)[1];
        	
        	if(AMPM == "PM" && hours < 12) hours = hours+12;
        	if(AMPM == "AM" && hours == 12) hours = hours-12;
        	
        	if(minutes < 8) {
        		minutes = 0;
        	} else if (minutes <= 15 || minutes <= 21) {
        		minutes = 15;
        	} else if (minutes <= 30 || minutes <= 37) {
        		minutes = 30;
        	} else if (minutes <= 45 || minutes <= 52) {
        		minutes = 45;
        	} else if (minutes >= 53) {
        		minutes = 0;
        		if(hours == 23) {
        			hours = 0;
        		} else {
        			++hours;
        		}
        	}
        	
        	var sHours = hours.toString();
        	var sMinutes = minutes.toString();
        	
        	if(hours<10) sHours = "0" + sHours;
        	if(minutes<10) sMinutes = "0" + sMinutes;
        	
        	return sHours + ":" + sMinutes;
    	
    	} catch(e) {
    		return "00:00";
    	}
    };
    
    $scope.dateTime_sort_asc = function (journey1, journey2) {
		if (journey1.dateTime > journey2.dateTime) return 1;
		if (journey1.dateTime < journey2.dateTime) return -1;
		return 0;
	};
    
    $scope.sortJourneysByDateTime = function () {
		if($scope.journeys.length > 1) {
			($scope.journeys).sort($scope.dateTime_sort_asc);
		}
	};
    
    $scope.generateFlightJourney = function (tempFlightJourney, flightJourneyType) {
    	var tmp_journey = {
  	  		journeyMode			: "F",
  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  		source				: tempFlightJourney.source,
  	  		destination			: tempFlightJourney.destination,
  	  		dateOfJourney		: tempFlightJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempFlightJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempFlightJourney.dateOfJourney, tempFlightJourney.preferredTimeDesc, true),
  	  		stayType			: tempFlightJourney.stayType,
  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if(flightJourneyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "F",
  	  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  	  		source				: tempFlightJourney.destination,
  	  	  		destination			: tempFlightJourney.source,
  	  	  		dateOfJourney		: $scope.flightJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.flightJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.flightJourneyDetails.returnDate, $scope.flightJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempFlightJourney.stayType,
  	  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	  	};
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    };
    
    $scope.generateTrainJourney = function (tempTrainJourney, trainJourneyType) {
    	var tmp_journey = {
  	  		journeyMode			: "T",
  	  		journeyCode 		: tempTrainJourney.journeyCode,
  	  		source				: tempTrainJourney.source,
  	  		destination			: tempTrainJourney.destination,
  	  		dateOfJourney		: tempTrainJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempTrainJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempTrainJourney.dateOfJourney, tempTrainJourney.preferredTimeDesc, true),
  	  		stayType			: tempTrainJourney.stayType,
  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if(trainJourneyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "T",
  	  	  		journeyCode 		: tempTrainJourney.journeyCode,
  	  	  		source				: tempTrainJourney.destination,
  	  	  		destination			: tempTrainJourney.source,
  	  	  		dateOfJourney		: $scope.trainJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.trainJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.trainJourneyDetails.returnDate, $scope.trainJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempTrainJourney.stayType,
  	  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  	  	  	};
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    };
    
    $scope.generateCarJourney = function (tempCarJourney, carJourneyType) {
    	var tmp_journey = {
  	  		journeyMode			: "C",
  	  		journeyCode 		: tempCarJourney.journeyCode,
  	  		source				: tempCarJourney.source,
  	  		destination			: tempCarJourney.destination,
  	  		dateOfJourney		: tempCarJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempCarJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempCarJourney.dateOfJourney, tempCarJourney.preferredTimeDesc, true),
  	  		stayType			: tempCarJourney.location,
  	  		stayTypeDesc		: tempCarJourney.locationDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if(carJourneyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "C",
  	  	  		journeyCode 		: tempCarJourney.journeyCode,
  	  	  		source				: tempCarJourney.destination,
  	  	  		destination			: tempCarJourney.source,
  	  	  		dateOfJourney		: $scope.carJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.carJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.carJourneyDetails.returnDate, $scope.carJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempCarJourney.location,
  	  	  		stayTypeDesc		: tempCarJourney.locationDesc
  	  	  	};
    		
    		if(!editFlag) {
    			tmp_journey.journeyCode 		= $scope.getMaxJourneyOrder("C");
    			tmp_journey.dateOfJourney 		= $scope.carReturnDate;
    			tmp_journey.preferredTimeDesc 	= $scope.carReturnTimeDesc;
  	  		}
    		
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    };
    
    $scope.generateAccommodation = function (tempAccommodation) {
    	var tmp_journey = {
  	  		journeyMode			: "A",
  	  		journeyCode 		: tempAccommodation.journeyCode,
  	  		source				: "",
  	  		destination			: tempAccommodation.place,
  	  		dateOfJourney		: tempAccommodation.checkInDate,
  	  		preferredTimeDesc	: "",
  	  		dateTime			: $scope.getDateTimeObject(tempAccommodation.checkInDate, "", false),
  	  		journeyOrder		: tempAccommodation.journeyOrder,
  	  		checkOutDate		: tempAccommodation.checkOutDate,
  	  		stayType			: tempAccommodation.stayType,
  	  		stayTypeDesc		: tempAccommodation.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	$scope.sortJourneysByDateTime();
    };
	
    $scope.showProcessing = function () {
    	if(!$scope.getElement("#loading-div").hasClass("overlay-alert-popup-show")) {
    		$scope.getElement("#loading-div").addClass("overlay-alert-popup-show");
    	}
    };
    
    $scope.hideProcessing = function () {
    	if($scope.getElement("#loading-div").hasClass("overlay-alert-popup-show")) {
    		$scope.getElement("#loading-div").removeClass("overlay-alert-popup-show");
    	}
    };
	
	/**************************************************** Function for get element ********************************************************/
	$scope.getElement = function (selector) {
    	return angular.element(document.querySelector(selector));
    };
    
    $scope.getAllElements = function (selector) {
  	  	return angular.element(document.querySelectorAll(selector));
    };
	/**************************************************** GET REQUEST COUNT ********************************************************/
    
	$scope.getRequestCounts = function() {
		var url = $scope.apiBaseUrl+"/api/request/counts/D/0";
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName
		};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.approvedRequestsCount 		= JSON.stringify(response.data.data.approvedRequests);
				$scope.temporaryRequestsCount 		= JSON.stringify(response.data.data.temporaryRequests);
				$scope.requestsInWorkflowCount 		= JSON.stringify(response.data.data.requestsInWorkflow);
				$scope.returnedRequestsCount 		= JSON.stringify(response.data.data.returnedRequests);
				$scope.rejectedRequestsCount 		= JSON.stringify(response.data.data.rejectedRequests);
				$scope.cancelledRequestsCount 		= JSON.stringify(response.data.data.cancelledRequests);
				$scope.yearlyRequestApprovedCount 	= JSON.stringify(response.data.data.yearlyRequestApproved);
				$scope.yearlyRequestCreatedCount 	= JSON.stringify(response.data.data.yearlyRequestCreated);
			}
		});
	};
	
	/**************************************************** GET TEMPRORY REQUESTS ON PAGE LOAD ********************************************/
	
	$scope.loadRequestData = function( requestType ) {
		
		$scope.requestTypeGlobal = requestType;
		$scope.requestType 		 = requestType;
		
		var url = $scope.apiBaseUrl+"/api/request/D/"+requestType+"/0/list";
		
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName
			};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestList = response.data.data;
				$scope.hideProcessing();
			}
		});
    };
    
    /**************************************************** GET REQUESTS ********************************************/
    
	$scope.requestData = function($event, requestType) {
		
		$scope.showProcessing();
		
		$scope.requestTypeGlobal = requestType;
		$scope.requestType 		 = requestType;
		
		var actionItems = $('.filter-tabs li');
		actionItems.each(function() {
				$(this).removeClass( "active" );	
		});
		
		$(event.currentTarget).parent('li').addClass("active");
		
		var url = $scope.apiBaseUrl+"/api/request/D/"+requestType+"/0/list";
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestList = response.data.data;
				$scope.hideProcessing();
			}
		});
    };
    
    /**************************************************** GET REQUEST COMMENT DETAILS *******************************/
    
    $scope.getRequestCommentDetails = function(element, travelId, travelType) {
    	
    	$scope.travelTypeHidden = travelType;
    	var url = $scope.apiBaseUrl+"/api/request/comment";
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travelId		: travelId,
	    	travelType		: travelType
		};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				if(element == "sideOverlayData"){
					$scope.requestDetails 		 = response.data.data.requestDetails;
					$scope.commentsList 		 = response.data.data.commentsList;
					$scope.cancelledCommentsList = response.data.data.cancelledCommentsList;
				}else if( element == "boxBodyData" ){
					$scope.commentsList		   = response.data.data.commentsList;
				}
			}
		});
    };
    
    /**************************************************** SAVE REQUEST COMMENT ********************************************/
    
    $scope.saveComment = function(element, travelId) {
    	
		var travelType  = $scope.travelTypeHidden;
		var commentDesc = "";
		if(element == "boxBodyData"){
			commentDesc = angular.element(document.getElementById("commentDescBox"+travelId)).val();      
		}else {
			commentDesc = $scope.commentDesc;
		}
		
    	var url = $scope.apiBaseUrl+"/api/request/comment";
    	var headerData = {
    			'If-Modified-Since': '0',
    	        "Pragma": "no-cache",
    	        "Expires": -1,
    	        "Cache-Control": "no-cache, no-store, must-revalidate"
    	};
		var postData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				travelId		: travelId,
				commentDesc		: commentDesc,
				travelType		: travelType
		}
		
		apiCallService.postRequest(url, postData, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.getRequestCommentDetails(element,travelId,travelType);
			}else if(response == null && response.data.status == "500") {
				$window.alert("Error in save comment.");
			}
		});
    };
    
    /**************************************************** DELETE REQUEST COMMENT ********************************************/
    
    $scope.deleteComment = function(element, travelId, commentId) {
    	
    	if ( $window.confirm("Do you want to delete comment ?") ){
    		var travelType = $scope.travelTypeHidden;
        	var url = $scope.apiBaseUrl+"/api/request/comment";
        	var headerData = {
        			winUserId		: $scope.winUserId,
        			domainName 		: $scope.domainName,
        			deviceId 		: $scope.ipAddress,
        			requestSource 	: $scope.requestSource,
        			systemName 		: $scope.systemName,
        			travelId		: travelId,
        	    	travelType		: travelType,
        	    	commentId		: commentId
        	};
    		apiCallService.deleteRequest(url, headerData)
    		.then(function(response){
    			if(response != null && response.data.status == "200") {
    				$scope.getRequestCommentDetails(element,travelId,travelType);
    			}else if(response == null && response.data.status == "500") {
    				$window.alert("Error in save comment.");
    			}
    		});
    	}
    };
    
    
    /**************************************************** GET REQUEST ATTACHMENT DETAILS ********************************************/
    
    $scope.getAttachmentDetails = function(travelId,travelType) {
    	
    	$scope.travelTypeHidden = travelType;
    	var url = $scope.apiBaseUrl+"/api/request/attachment";
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travelId		: travelId,
	    	travelType		: travelType
			
		};

		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestDetails = response.data.data.requestDetails;
				$scope.attachmentList = response.data.data.attachmentList;
			}
		});
    };
    
    /**************************************************** DELETE REQUEST ATTACHMENT ********************************************/
    
    $scope.deleteAttachment = function(attachmentId,travelId) {
    	
    	if ($window.confirm("Do you want to delete attachment ?")){
    		var travelType = $scope.travelTypeHidden;
        	var url = $scope.apiBaseUrl+"/api/request/attachment";
        	var headerData = {
        			winUserId		: $scope.winUserId,
        			domainName 		: $scope.domainName,
        			attachmentId	: attachmentId
        	};
        	
        	apiCallService.deleteRequest(url, headerData)
    		.then(function(response){
    			if(response != null && response.data.status == "200") {
    				$window.alert("Attachment Deleted Successfully. ");
    				$scope.getAttachmentDetails(travelId,travelType);
    			}else if(response == null && response.data.status == "500") {
    				$window.alert("Error in deleting attachment.");
    			}
    		});
    	}
    };
    
    /**************************************************** DELETE REQUEST ********************************************************/
    
    $scope.deleteTravelRequest = function(travelId,travelType,travelRequestId,requestType) {
    	
    	if ($window.confirm("Do you want to delete request ?")){
			var url = $scope.apiBaseUrl+"/api/request";
	    	var headerData = {
	    			winUserId		: $scope.winUserId,
	    			domainName 		: $scope.domainName,
	    			
	    			travelId		: travelId,
	    	    	travelType		: travelType,
	    	    	travelReqId		: travelRequestId
	    	};
	    	apiCallService.deleteRequest(url, headerData)
			.then(function(response){
				if(response != null && response.data.status == "200") {
					$scope.getRequestCounts();
					$scope.loadRequestData(requestType);
				}else if(response == null && response.data.status == "500") {
					$window.alert("Error in deleting request.");
				}
			});
    	}	
    };
    
    /**************************************************** GET REQUEST WHEN CANCELING ********************************************************/
    
    $scope.getCancelRequestDetail = function(travelId,travelType) {
    	$scope.travelTypeHidden = travelType;
    	
    	var url = $scope.apiBaseUrl+"/api/request/cancel";
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travelId		: travelId,
	    	travelType		: travelType
			
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestDetails = response.data.data.requestDetails;
			}
		});
    };
    
    /**************************************************** SAVE CANCEL COMMENT ********************************************************/
    
    $scope.saveCancelRequestComment = function(travelId) {
    	
    	var travelType 			= $scope.travelTypeHidden;
    	var requestType 		= $scope.requestTypeGlobal;
    	var cancelCommentDesc 	= $scope.cancelCommentDesc;
    	
    	var url = $scope.apiBaseUrl+"/api/request/cancel";
    	var headerData = {};
    	
		var postData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				travelId		: travelId,
				commentDesc		: cancelCommentDesc,
				travelType		: travelType
		}
		
		apiCallService.postRequest(url, postData, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.getRequestCounts();
				$scope.loadRequestData(requestType);
				$('#sideOverlay').removeClass('active-agent');
			}
		});
    };
    
	$scope.getRequestCounts();
	$scope.loadRequestData('TEM');
	
	/**************************************************** OTHER PAGE UTILITY FUNCTIONS ********************************************************/
	
	$scope.openActions = function($event) {
		$( "div.action-list-scope" ).each(function() {
			$(this).fadeOut();
		});
		$(event.currentTarget).parents('.action-button-scope').find('.action-list-scope').toggle();
	};
	
	$scope.openOverlay = function( type, travelId, travelType ) {
		
		$( "div.action-list-scope" ).each(function() {
			$(this).fadeOut();
		});
		
		if( type == 'attachment' ) {
			$("#attachmentTab").addClass('active');
			$("#attachmentTabBody").addClass('active');
			
			$("#commentTab").css("display","block");
			
			$('#commentTab').removeClass('active');
			$('#commentTabBody').removeClass('active');
			$('#cancelRequestTab').removeClass('active');
			$('#cancelRequestTabBody').removeClass('active');
			
			$('#cancelRequestTab').css("display","none");
			
			$scope.getAttachmentDetails(travelId, travelType);
			
		}else if( type == 'comment' ) {
			
			$("#commentTab").addClass('active');
			$("#commentTabBody").addClass('active');
			
			$("#attachmentTab").css("display","block");
			
			$('#attachmentTab').removeClass('active');
			$('#attachmentTabBody').removeClass('active');
			$('#cancelRequestTab').removeClass('active');
			$('#cancelRequestTabBody').removeClass('active');
			
			$('#cancelRequestTab').css("display","none");
			
			$scope.getRequestCommentDetails( 'sideOverlayData', travelId, travelType);
			
		}else if( type == 'rejectedComment' || type == 'cancelComment' ) {
			
			$("#commentTab").addClass('active');
			$("#commentTabBody").addClass('active');
			
			$('#attachmentTab').removeClass('active');
			$('#cancelRequestTab').removeClass('active');
			$("#attachmentTab").css("display","none");
			$('#cancelRequestTab').css("display","none");
			$('#attachmentTabBody').removeClass('active');
			$('#cancelRequestTabBody').removeClass('active');
			
			$scope.getRequestCommentDetails( 'sideOverlayData', travelId, travelType);
			
		}else if(type == 'cancel' ) {
			
			$("#cancelRequestTab").addClass('active');
			$("#cancelRequestTabBody").addClass('active');
			
			$('#commentTab').removeClass('active');
			$('#commentTabBody').removeClass('active');
			$('#attachmentTab').removeClass('active');
			$('#attachmentTabBody').removeClass('active');
			$("#attachmentTab").css("display","none");
			$("#commentTab").css("display","none");
			
			$scope.getCancelRequestDetail(travelId, travelType);
			
		}
		$("#sideOverlay").addClass('active-agent');
	};
	
	$scope.closeOverlay = function() {
		$('#sideOverlay').removeClass('active-agent');
	};
	
	$scope.toggleContent = function() {
		$(".requisition-details-scope").slideToggle();
	};
	
	$scope.openAttachmentTab = function( travelId ) {
		$("#commentTab").css("display","block");
		var travelType = $scope.travelTypeHidden;
		$scope.getAttachmentDetails(travelId, travelType);
	};
	
	$scope.openCommentTab = function( element, travelId, travelType ) {
		
		if(element == "sideOverlayData"){
			if($scope.requestTypeGlobal != 'CAN' &&	 $scope.requestTypeGlobal != 'REJ'){
				$("#attachmentTab").css("display","block");
			}
			travelType = $scope.travelTypeHidden;
		}
		
		$scope.getRequestCommentDetails(element,travelId,travelType);
		
	};
	
	/************************************************* SHOW REQUEST DETAILS SECTION *******************************************/
	
	$scope.showDetails = function( $event, travelType, travelId, requestNo ) {
		$( "button.show-detail-btn" ).each(function() {
			if($(this).parent('div').attr('id') != $(event.currentTarget).parent('div').attr('id')){
				$(this).find('.btn-icon').removeClass( "fa-minus" );
				$(this).find('.btn-icon').addClass( "fa-plus" );
			}
		});
		var detailsDivHeader = $('.request-data-box'); 
		detailsDivHeader.each(function() {
			if($(this).find('.show-detail-btn-parent').attr('id') != $(event.currentTarget).parent('div').attr('id')){
				$(this).addClass('collapsed-box');
			}
		});
		
		var detailsDiv = $('.details-div');
		detailsDiv.each(function() {
			if($(this).find('.show-detail-btn-parent').attr('id') != $(event.currentTarget).parent('div').attr('id')){
				$(this).css("display","none");
			}
		});
		$scope.getTravelDetails( travelType, travelId, requestNo);
	};
	
	/************************************************* FETCH TRAVELLER PROFILE DETAILS *******************************************/
    
	$scope.getTravelDetails = function( travelType, travelId, requestNo ) {
		
		$scope.showProcessing();
		
		var url = $scope.apiBaseUrl+"/api/request/"+travelType+"/"+travelId;
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.travelDetails = response.data.data;
				$scope.approversList = $scope.travelDetails.approvers;
				$scope.hideProcessing();
			}
		});
    };
	
    
    $scope.showItinerary = function(){
    	
    	$scope.journeys				= [];
    	
    	$scope.showProcessing();
    	
    	var flightJourneyType = $scope.travelDetails.flightJourneyDetails.journeyType;
		angular.forEach($scope.travelDetails.flightJourneyDetails.flightJourney, function(journey) {
			  journey.preferredTime = 0;
			  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
			  
		  	  $scope.generateFlightJourney(journey,flightJourneyType);
		  });
		
		var trainJourneyType = $scope.travelDetails.trainJourneyDetails.journeyType;
		angular.forEach($scope.travelDetails.trainJourneyDetails.trainJourney, function(journey) {
			  journey.preferredTime = 0;
			  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
			  
			  $scope.generateTrainJourney(journey, trainJourneyType);
		  });
		
		var carJourneyType = $scope.travelDetails.carJourneyDetails.journeyType;
		angular.forEach($scope.travelDetails.carJourneyDetails.carJourney, function(journey) {
			  journey.preferredTime = 0;
			  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
			  
			  $scope.generateCarJourney(journey, carJourneyType);
		  });
		
		if($scope.travelDetails.accommodationDetails != 'undefined' && $scope.travelDetails.accommodationDetails != ''){
			angular.forEach($scope.travelDetails.accommodationDetails.accommodations, function(accommodation) {
				  $scope.generateAccommodation(accommodation);
			  });
		}
		
		$scope.sortJourneysByDateTime();
		$scope.hideProcessing();
    };
    
    $scope.setApprovalWorkflow = function(travelId) {
    	
    	$scope.approversDivHtml = "";
    	
    	if($scope.approversList.length > 0){
    		angular.forEach($scope.approversList, function(value, key) {
	    	  if(value.approveStatusDesc == "Approved"){
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step complete" onmouseover="angular.element(this).scope().showApproverDetail(this,'+travelId+','+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Pending"){
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step process" onmouseover="angular.element(this).scope().showApproverDetail(this,'+travelId+','+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Waiting") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step disabled" onmouseover="angular.element(this).scope().showApproverDetail(this,'+travelId+','+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Returned") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step returned" onmouseover="angular.element(this).scope().showApproverDetail(this,'+travelId+','+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Rejected") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step rejected" onmouseover="angular.element(this).scope().showApproverDetail(this,'+travelId+','+key+')"  >';
	    	  }
	    	  $scope.approversDivHtml += '<div class="text-center bs-wizard-stepnum"><a href="#">'+value.name +'</a></div><div class="progress"><div class="progress-bar"></div></div><a href="#" class="bs-wizard-dot"></a><!--  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>--></div>';
	    	  
	    	});
    	}else {
    		 $scope.approversDivHtml += 'No Records!';
    	}
    	$scope.approversDivHtml = $sce.trustAsHtml($scope.approversDivHtml);
    };	
    
    $scope.showApproverDetail = function(event,travelId,index) {
    	
    	var element = $scope.getElement("#timelineTooltipDiv"+travelId);
    	$scope.timelineDetailsDiv = "";
    	element.html($scope.timelineDetailsDiv);
    	var left = $(event).offset().left-45;
    	$scope.timelineDetailsDiv += '<div class="timeline-step-details approverDetail">'+
	        '<div class="details-inner approverDetailInner " style="left: '+left+'px; text-align:left">'+
	           '<div  class="aero-Class"></div>'+
	           '<strong>'+$scope.approversList[index].name+'</strong> | '+$scope.approversList[index].designationName+' | '+ $scope.approversList[index].siteName+'<br> Received on: Oct 7 2017 12:11 PM<br> Approved on: '+$scope.approversList[index].approveDate+' '+$scope.approversList[index].approveTime+
	        '</div>'+
	    '</div>';
    	element.html($scope.timelineDetailsDiv);
    	$scope.getElement("#timelineTooltipDiv"+travelId).css("display","block");
    };
    
    $scope.hideDetails = function(event,index) {
    	$scope.getElement(".timeline-step-details").css("display","none");
    };
    
	$(document).click(function(e) {
	var target = e.target; 
		if (!$(target).is('.btn-action') ) {
			$(this).find('.action-list-scope').fadeOut();
		}
	})
	
	$scope.viewDetails = function( travelId, travelType ) {
		var newWindow = window.open('details', '_blank');
		$window.travelId 		= travelId ;
		$window.travelType 		= travelType ;
	};
	
	/************************************************* FETCH MAIL FLOW *******************************************/
	
	$scope.getMailFlow = function(requestNo) {
		$scope.showProcessing();
		var url = $scope.apiBaseUrl+"/api/request/mailflow/list";
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				requestNo		: requestNo
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestMailFlow = response.data.data;
				$scope.hideProcessing();
			}
		});
    };
    
    /************************************************* FETCH MAIL BODY *******************************************/
    
    $scope.getMailDetails = function(mailId) {
    	
    	$scope.mailBody = "No Data !";
		
    	var url = $scope.apiBaseUrl+"/api/request/mail/detail";
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				mailId			: mailId
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.mailBody = response.data.data.mailBody;
				$scope.mailBody = $sce.trustAsHtml($scope.mailBody);
			}
		});
    };
    
}]);
