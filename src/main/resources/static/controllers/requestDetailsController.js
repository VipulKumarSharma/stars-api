app.controller("requestDetailsController", ['$scope', '$http', '$sce', '$window', '$compile', 'apiCallService', function($scope, $http, $sce, $window, $compile, apiCallService ) {
	
	$scope.requestDetails  	= "";
	$scope.requestNo 	  	 = "";
	$scope.requestMailFlow 	= "";
	$scope.approversList   	= [];
	$scope.approversDivHtml = "";
	$scope.otherInfoFlag    = false;
	$scope.hideFlag       = "true";
	$scope.mailBody 	  = "";
	$scope.timelineDetailsDiv = "";
	
	var travelType = window.opener.travelType;
	var travelId   = window.opener.travelId;
	
	$scope.travelType  = travelType;
	
	$scope.getElement = function (selector) {
    	return angular.element(document.querySelector(selector));
    };
	
	/**************************************************** LOAD REQUEST DETAILS ON PAGE LOAD ********************************************/
	
	$scope.loadRequestDetails = function() {
		var url = $scope.apiBaseUrl+"/api/request/"+travelType+"/"+travelId;
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestDetails = response.data.data;
				$scope.requestNo 	  = $scope.requestDetails.requestNo;
				$scope.getMailFlow();
				$scope.approversList  = $scope.requestDetails.approvers;
				$scope.setApprovalWorkflow();
				
				if($scope.travelType == 'D' &&(($scope.requestDetails.trainTravelFlag == 'Y') || ($scope.requestDetails.carTravelFlag == 'Y') )){
					$scope.otherInfoFlag = true;
				}else{
					$scope.otherInfoFlag = false;
				}
			}
		});
    };
    
    $scope.getMailFlow = function() {
		var url = $scope.apiBaseUrl+"/api/request/mailflow/list";
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				requestNo		: $scope.requestNo
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.requestMailFlow = response.data.data;
			}
		});
    };	
    $scope.loadRequestDetails();
    
    $scope.setApprovalWorkflow = function() {
    	
    	if($scope.approversList.length > 0){
    		angular.forEach($scope.approversList, function(value, key) {
	    	  if(value.approveStatusDesc == "Approved"){
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step complete" onmouseover="angular.element(this).scope().showDetail(this,'+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Pending"){
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step process" onmouseover="angular.element(this).scope().showDetail(this,'+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Waiting") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step disabled" onmouseover="angular.element(this).scope().showDetail(this,'+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Returned") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step returned" onmouseover="angular.element(this).scope().showDetail(this,'+key+')"  >';
	    	  }else if(value.approveStatusDesc == "Rejected") {
	    		  $scope.approversDivHtml += '<div class="step-timeline-box bs-wizard-step rejected" onmouseover="angular.element(this).scope().showDetail(this,'+key+')"  >';
	    	  }
	    	  $scope.approversDivHtml += '<div class="text-center bs-wizard-stepnum"><a href="#">'+value.name +'</a></div><div class="progress"><div class="progress-bar"></div></div><a href="#" class="bs-wizard-dot"></a><!--  <div class="bs-wizard-info text-center">Lorem ipsum dolor sit amet.</div>--></div>';
	    	  
	    	});
    	}else {
    		 $scope.approversDivHtml += 'No Records!';
    	}
    	$scope.approversDivHtml = $sce.trustAsHtml($scope.approversDivHtml);
    };	
    
    $scope.showDetail = function(event,index) {
    	var element = $scope.getElement("#timelineTooltipDiv");
    	$scope.timelineDetailsDiv = "";
    	var left = $(event).offset().left-25;
    	$scope.timelineDetailsDiv += '<div class="timeline-step-details approverDetail">'+
	        '<div class="details-inner approverDetailInner " style="left: '+left+'px; text-align:left">'+
	           '<div  class="aero-Class"></div>'+
	           '<strong>'+$scope.approversList[index].name+'</strong> | '+$scope.approversList[index].designationName+' | '+ $scope.approversList[index].siteName+'<br> Received on: Oct 7 2017 12:11 PM<br> Approved on: '+$scope.approversList[index].approveDate+' '+$scope.approversList[index].approveTime+
	        '</div>'+
	    '</div>';
    	element.html($scope.timelineDetailsDiv);
    	$scope.getElement("#timelineTooltipDiv").css("display","block");
    };
    
    $scope.hideDetails = function(event,index) {
    	$scope.getElement("#timelineTooltipDiv").css("display","none");
    };
    
    $scope.getMailDetails = function(mailId) {
    	
    	$scope.mailBody = "No Data !";
		
    	var url = $scope.apiBaseUrl+"/api/request/mail/detail";
		var headerData = {
				winUserId		: $scope.winUserId,
				domainName 		: $scope.domainName,
				deviceId 		: $scope.ipAddress,
				requestSource 	: $scope.requestSource,
				systemName 		: $scope.systemName,
				mailId			: mailId
			};
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.data.status == "200") {
				$scope.mailBody = response.data.data.mailBody;
				$scope.mailBody = $sce.trustAsHtml($scope.mailBody);
			}
		});
    };
    
}]);
