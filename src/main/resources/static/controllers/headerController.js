app.controller("headerController", ['$rootScope', '$scope','$http','apiCallService','$window', function($rootScope, $scope, $http, apiCallService,$window) {
	
	$scope.loggedInUserId 			= angular.element(document.querySelector("#userId")).val();
	$rootScope.loggedInUser			= {fullName:"", designation:"", siteName:"", winUserId:"", domainName:"", dateOfBirth:"", contactNumber:"", email:"", identityProof:"", address:""};
	$rootScope.loggedInUserImageURL	= "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&accountname=";
	
	$scope.encWinUserId = encWinUserId;
	$scope.encDomainName = encDomainName;
	$scope.encbaseStarUrl = encbaseStarUrl;
	
	$scope.getLoggedInUserDetails = function () {
    	var url = $scope.apiBaseUrl+"/api/request/user/detail";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travellerId		: $scope.loggedInUserId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$rootScope.loggedInUser = response.data.data;
				$rootScope.loggedInUserImageURL = "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&accountname="+$rootScope.loggedInUser.domainName+"\\"+$rootScope.loggedInUser.winUserId;
			}
			//console.log($rootScope.loggedInUser);
		});
    };
    
    $scope.getLoggedInUserDetails();
    
    $scope.openOldStar = function () {
    	var url = $scope.apiBaseUrl+"/appLogout";
    	var headerData = {};
    	apiCallService.getRequest(url, headerData)
		.then(function(response){
			if(response != null && response.status == "200") {
				$window.location.href = $scope.encbaseStarUrl+"reqWinId="+$scope.encWinUserId+"&reqDomain="+$scope.encDomainName+"&reqErrorCode=0&frmApp=STARSAPI";
			}
		});
    };
    
}]);