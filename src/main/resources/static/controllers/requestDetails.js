  $(document).ready(function(){
	  
	$(function() {
    $("#toggle").on('click', function() {   
       $(".fa-chevron-circle-down").toggleClass("rotate");
    });
});
	  
	$("#hide").click(function(){
			$("#frame").addClass('active-agent');
		});
		
		$("#close").click(function(){
			$("#frame").removeClass('active-agent');
		});
		
		 $('#overlayShow').on('click touchleave', function () {
            $('.info-details-scope').addClass('active-agent');
        });
		
		 $('#close-overlay').on('click touchleave', function () {
            $('.overlay').removeClass('active-agent');
        });	
	  
	  $(".user-list-dropdown").click(function(){
			$(".user-list-dropdown").addClass('active-agent');
		});
	});



 	$(document).on('mouseup touchleave', function (e) {
		if ( !$('.user-list-dropdown').is(e.target) && $('.user-list-dropdown').has(e.target).length === 0 ) {
			$('.user-list-dropdown').removeClass('active-agent');
		}
		
	});


// select 2 js//
  $(function () {
		//Initialize Select2 Elements
		$(".select2").select2();
	});	
// end select 2 js//


//start side panel js//
 $(document).ready(function() {	
		$('#information-box').on('click touchleave', function(e) {
			e.preventDefault(); $('#infoDetails').addClass('active-agent');
			document.body.style.overflow="hidden";	
			$('body').css('padding-right',"18px");

});

	$('#info-close-btn').on('click touchleave', function(e) {
		e.preventDefault();
		$('#infoDetails').removeClass('active-agent');
		 document.body.style.overflow="auto";
		 $('body').css('padding-right',"0px");
	});		 

});	

//end start side panel js//

//Popup alert//
$('.btn-alert-popup').on('click', function () {
	'use strict';
	$($(this).attr('data-target')).addClass('overlay-alert-popup-show');
	$('body').addClass('body-overflow-hidden');
});
$('.pop-up-close').on('click', function () {	
	$(this).parents('.overlay-alert-popup').removeClass('overlay-alert-popup-show');
	$('body').removeClass('body-overflow-hidden');
});
//end Popup alert//

/*select radio button show div (calendar) */
$(document).ready(function () {
    $('.any-time').click(function () {
		$(this).parents('.radio-box-section').hide().siblings('.calendar-content').show();
    });
	
	$(".close-btn-scope").children('.execute-btn').on('click', function () {
		$(this).parents('.calendar-content').hide().siblings('.radio-box-section').show();
    });
});	
/*end select radio button show div (calendar) */
