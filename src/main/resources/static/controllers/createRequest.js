$(function() {
	$("div.flightReturnJourneyDiv, div.trainReturnJourneyDiv, div.carReturnJourneyDiv").addClass("disabled-group");
	$("#liveStatusDiv").hide();
	
	$(".user-list-dropdown").click(function() {
        $(".user-list-dropdown").addClass('active-agent');
    });

    $('#information-box').on('click touchleave', function(e) {
        e.preventDefault();
        $('#infoDetails').addClass('active-agent');
        document.body.style.overflow = "hidden";
        $('body').css('padding-right', "18px");

    });
    $('#info-close-btn').on('click touchleave', function(e) {
        e.preventDefault();
        $('#infoDetails').removeClass('active-agent');
        document.body.style.overflow = "auto";
        $('body').css('padding-right', "0px");
    });


    $('.btn-alert-popup').on('click', function() {
        'use strict';
        $($(this).attr('data-target')).addClass('overlay-alert-popup-show');
        $('body').addClass('body-overflow-hidden');
    });
    $('.pop-up-close').on('click', function() {
        $(this).parents('.overlay-alert-popup').removeClass('overlay-alert-popup-show');
        $('body').removeClass('body-overflow-hidden');
    });


    $('.any-time').click(function() {
    	var timeModeName = $(this).find("[name='timeModeName']").val();
    	$(this).parents('.radio-box-section').hide().siblings('.calendar-content').find(".data-inside-scope .heading-text").html(timeModeName);
        $(this).parents('.radio-box-section').hide().siblings('.calendar-content').show();
    });

    $(".close-btn-scope").children('.execute-btn').on('click', function() {
        $(this).parents('.calendar-content').hide().siblings('.radio-box-section').show();
    });


    $(document).on('mouseup touchleave', function(e) {
    	if (!$('.user-list-dropdown').is(e.target) && $('.user-list-dropdown').has(e.target).length === 0) {
            $('.user-list-dropdown').removeClass('active-agent');
        }
    });
	
   $(".user-list-data ul").on('click', function(e) {
    	//if (!$('.user-list-dropdown').is(e.target) && $('.user-list-dropdown').has(e.target).length === 0) {
            $('.user-list-dropdown').removeClass('active-agent');
        //}
    });

    $(".select2").select2();

    function getDaysDifference (date1, date2) {
    	var diff = date1 - date2;
    	return (diff / 1000 / 60 / 60 / 24);
    }
    
    function returnDateTimeObject(dateString) {
    	var year = 1900;
    	var month = 01;
    	var day = 01;
    	const monthNamesArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    	
    	if(dateString.indexOf("-") != -1) {
    		var dateArr = dateString.split("-");
    		
    		day = dateArr[0];
    		month = monthNamesArray.indexOf(dateArr[1]);
    		year = dateArr[2];
    	}
    	
    	return new Date(year, month, day, 00, 00, 00);
    }
    
    $('.datePicker').datepicker({
        format				: "dd-M-yyyy",
        startDate			: "new Date()",
        orientation			: "bottom left",
        autoclose			: true,
        todayHighlight		: true,
        
        clearBtn			: false,
        keyboardNavigation	: false

    }).on('show', function(e) {
        setDatePickerLocation($(this));
        
    }).on('changeDate', function(e) {
    	var selectedDate = $(this).datepicker('getDate');
    	
    	if($(this).attr("id") == 'flightDateOfJourney') {
    		$("#flightReturnDate").datepicker("setStartDate", selectedDate);
    		
    		if(getDaysDifference(selectedDate, returnDateTimeObject($("#flightReturnDate").val())) > 0) {
    			$('#flightReturnDate').datepicker('update', selectedDate);
    		}
    	}
    	
    	if($(this).attr("id") == 'trainDateOfJourney') {
    		$("#trainReturnDate").datepicker("setStartDate", selectedDate);
    		
    		if(getDaysDifference(selectedDate, returnDateTimeObject($("#trainReturnDate").val())) > 0) {
    			$('#trainReturnDate').datepicker('update', selectedDate);
    		}
    	}
    	
    	if($(this).attr("id") == 'carDateOfJourney') {
    		$("#carReturnDate").datepicker("setStartDate", selectedDate);
    		
    		if(getDaysDifference(selectedDate, returnDateTimeObject($("#carReturnDate").val())) > 0) {
    			$('#carReturnDate').datepicker('update', selectedDate);
    		}
    	}
    	
    	if($(this).attr("id") == 'accommodationCheckInDate') {
    		$("#accommodationCheckOutDate").datepicker("setStartDate", selectedDate);
    		
    		if(getDaysDifference(selectedDate, returnDateTimeObject($("#accommodationCheckOutDate").val())) > 0) {
    			$('#accommodationCheckOutDate').datepicker('update', selectedDate);
    		}
    	}
    });
    
    function setDatePickerLocation(element) {
        var datePickerYposition = element.offset().top;
        datePickerYposition = (datePickerYposition + 30);

        $("div.datepicker").offset({
            top: datePickerYposition
        });
    }

    $('.timePicker').datetimepicker({
        format: 'LT'
    });

    $("#save, #saveAndExit, #submit").on('click', function() {
        $(".request-summary").removeClass("hide");
    });

    $("#back").on('click', function() {
        $(".request-summary").addClass("hide");
    });
    
    $("#live_status").on('click', function() {
    	$("#liveStatusDiv").show();
    });
    
    $("#liveStatusDivClose").on('click', function() {
    	$("#liveStatusDiv").hide();
    });
    
    $("#flightPreferencesBtn").on('click', function() {
    	$("div.flightPreferencesDiv").toggleClass("hide");
    });
    
    $("#trainPreferencesBtn").on('click', function() {
    	$("div.trainPreferencesDiv").toggleClass("hide");
    });
    
    /*$("#flying_from_value, #flying_to_value").on('onkeyup', function() {
    	validateChars(this, 30, 'sector');
    });*/
});

function openCalendarDiv(element) {
	var timeModeName = $(element).find("[name='timeModeName']").val();
	$(element).parents('.radio-box-section').hide().siblings('.calendar-content').find(".data-inside-scope .heading-text").html(timeModeName);
	$(element).parents('.radio-box-section').hide().siblings('.calendar-content').show();
}

function validateChars(element, length, validationType) {	
	if(element.name == 'reasonForSkip') {		
		 upToTwoHyphen(element);
	}	
	if(element.name == 'reasonForTravel') {
		 upToTwoHyphen(element);
	}	
	if(element.name == 'accommodationRemarks') {
		 upToTwoHyphen(element);
	}	
	if(element.name == 'accommodationBudget') {
		zeroChecking(element);
	}	
	if(element.name == 'advanceForexRemarks') {
		upToTwoHyphen(element);
	}
	if(element.name == 'currencyDominationDetails') {
		upToTwoHyphen(element);
	}
	if(element.name == 'expRemarks') {
		upToTwoHyphen(element);
	}
	if(element.name == 'nonMATASourceRemarks') {
		upToTwoHyphen(element);
	}
			
	charactercheck(element, validationType);
	limitlength(element, length);	
	spaceChecking(element);	
}