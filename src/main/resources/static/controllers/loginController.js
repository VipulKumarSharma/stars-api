app.controller("loginController", function($scope, $http) {
 
	localStorage.clear();
	
    $scope.loginDTO = {
    	userName	: "",
    	password	: "",
    	encPassword	: "",
    	language	: "en_US"
    };
 
    $scope._encryption = function() {
    	var password = $scope.loginDTO.password;
    	var encPassword = $.trim(encrypt(password));
		
		$scope.loginDTO.encPassword = encPassword;
		$("#encPassword").val(encPassword);
		
		return true;
	}
    
    $scope.login = function() {
    	var userNameElem = $("#userName");
		var passwordElem = $("#password");
		
		if($.trim(userNameElem.val()) == "") {
			displayAlertMessage('Please enter username to login');
			userNameElem.focus();
			return false;
		}
		
		if($.trim(passwordElem.val()) == "") {
			displayAlertMessage('Please enter password to login');
		 	passwordElem.focus();
			return false;
		}
		
    	if($scope._encryption()) {
    		$http({
                method	: "POST",
                url		: "/login",
                data	: JSON.stringify($scope.loginDTO)
            })
            .then(_success, _error);
    	}
    	return true;
    };
 
    function _error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        
		//console.log("Error: " + status + ":" + data);
    }
	
	function _success(res) {
        //console.log("success");
    }
});