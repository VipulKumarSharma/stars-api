app.controller("createRequestController", ['$scope','$http','apiCallService', '$document', '$timeout', '$window' ,function($scope, $http, apiCallService, $document, $timeout, $window) {
	
	$scope.loggedInUserId 			= angular.element(document.querySelector("#userId")).val();
	$scope.loggedInUserSiteId 		= angular.element(document.querySelector("#siteId")).val();
	
	$scope.linkedTravelRequestId	= "0";
	$scope.isIndividualRequest		= true;
	$scope.isGroupRequest			= false;
	$scope.isGuestRequest			= false;
	
	$scope.isFlightSelected			= true;
	$scope.isTrainSelected			= false;
	$scope.isCarSelected			= false;
	$scope.isAccommodationSelected	= false;
	$scope.isAdvanceRequired		= false;
	
	$scope.isTransitHouseSelected 	= true;
    $scope.isOtherAccommodationSelected = false;
    $scope.isHotelSelected 			= false;
    
    $scope.requestType				= "Individual";
	$scope.travelId					= travelId;
	$scope.requestId				= "0";
	$scope.pageType 				= "REQ_CRE_IN";
	$scope.baseCurrency				= "INR";
	$scope.baseCurrencyExchangeRate	= "1.0000";
	$scope.groupTravelFlag 			= groupTravelFlag;
	$scope.travelType 				= travelType;
	$scope.travelTypeDesc 			= travelTypeDesc;
	$scope.travelRequestNo			= "";
	
	$scope.selectedSiteId 			= $scope.siteId;
	$scope.travelAgencyId			= "1";
	$scope.selectedTraveller 		= {domainName: "", winUserId: "", userName: "Select Traveller", userId: "0"};
	$scope.traveller				= {fullName:"N/A", designation:"N/A", siteName:"N/A", winUserId:"N/A", domainName:"N/A", dateOfBirth:"N/A", contactNumber:"N/A", email:"N/A", identityProof:"N/A", address:"N/A"};
	const monthNames 				= ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
	$scope.originatorId				= 0;
	$scope.originatorName			= "";
	$scope.travellerId				= 0;
	$scope.travellerName			= "";
	$scope.otherAccommodationsFlag	= "N";
	$scope.totalAdvanceAmountINR	= 0;
	$scope.travelVisaFlag			= "N";

	/******************************************************************************************/
	
	$scope.requestSetting			= {};
	$scope.itineraries				= [];
	$scope.accommodatedItineraries	= [];
	$scope.journeys					= [];
	
	$scope.TESCount					= {};
	$scope.canCreateRequest			= "Y";
	$scope.TESMessage				= "";
	
	$scope.linkedTravelRequestList	= [];
	
	$scope.travelPreferences		= {};
	$scope.mealList					= [];
	$scope.flightSeatList			= [];
	$scope.flightTimeModeList		= [];
	$scope.flightClassList			= [];
	$scope.trainSeatList			= [];
	$scope.trainTimeModeList		= [];
	$scope.trainClassList			= [];
	$scope.trainTicketTypeList		= [];
	$scope.carTimeModeList			= [];
	$scope.carClassList				= [];
	
	$scope.requestApprovers			= {};
	$scope.approverLevel1List		= [];
	$scope.approverLevel2List		= [];
	$scope.approverLevel3List		= [];
	
	$scope.siteList					= [];
	$scope.travellerList			= [];
	$scope.currencyList				= [];
	$scope.flightStayTypeList		= [];
	$scope.transitHouseList			= [];
	$scope.siteLocationList			= [];
	$scope.billingSiteList			= [];
	$scope.billingApproverList		= [];
	$scope.defaultApproverList		= [];
	$scope.costCentreList			= [];
	$scope.carCategorylist			= [];
	$scope.carLocationList			= [];
	
	$scope.approverExistInDefaultWorkflow = "N";
	$scope.advanceForexLabel 		= "Advance Required";
	$scope.oooForApproverExists		= "N";
	$scope.oooForApproverMessage	= "";
	
	/************************************************* UTILITY METHODS & PLUGINS DATA *****************************************/
	
	$scope.citiesList				= [];
	$scope.imgSrc					= "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&amp;accountname=\\";
    
    /************************************************* SITE MULTISELECT PLUGIN *****************************************/
    
	$scope.siteModel                = [];
    $scope.siteData                 = [];
    
    /*$scope.settings_SiteMultiSelect = {
  		smartButtonMaxItems: 2,
  		scrollableHeight: '200px', 
  		scrollable: true,
  		enableSearch: true,
  		keyboardControls: true
    };*/
      
    $scope.settings_SiteSingleSelect = {
    	//selectedToTop : true,
    	smartButtonMaxItems: 1,
  		selectionLimit: 1,
  		scrollableHeight: '110px', 
  		scrollable: true,
  		enableSearch: true,
  		keyboardControls: true
    };
      
    $scope.callback_SiteSingleSelect = {
	   onItemSelect: function (item) {
           $scope.selectedSiteId = item.id;
           $scope.selectedSiteName = item.label;
           
           angular.element(document.querySelector("#traveller_value")).val("");
           $scope.getTravellersBySiteId();
           $scope.imgSrc = "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&amp;accountname=\\";
       }
    };
      
    /************************************************* CITIES MULTISELECT PLUGIN *****************************************/
    $scope.selectedFlightSourceCity			= [];
    $scope.selectedFlightDestinationCity	= [];
    
    $scope.selectedTrainSourceCity			= [];
    $scope.selectedTrainDestinationCity		= [];
    
    $scope.settings_CitySingleSelect = {
    	//selectedToTop : true,
  		smartButtonMaxItems: 1,
  		selectionLimit: 1,
  		scrollable: true,
  		enableSearch: true,
  		keyboardControls: true
    };
      
    $scope.callback_FlyingFromCity = {
    	onItemSelect: function (item) {
    		$scope.flightSourceCity = item.label;
	    }
    };
    
    $scope.callback_FlyingToCity = {
	   onItemSelect: function (item) {
		   $scope.flightDestinationCity = item.label;
		   $scope.countryDesc = item.country;
       }
    };
    
    $scope.callback_TrainSourceCity = {
    	onItemSelect: function (item) {
    		$scope.trainSourceCity = item.label;
	    }
    };
    
    $scope.callback_TrainDestinationCity = {
	   onItemSelect: function (item) {
		   $scope.trainDestinationCity = item.label;
       }
    };
    
    /************************************************* UTILITY METHODS *********************************************************/
    
    $scope.toLowerCaseString = function (object) {
    	
    	if(typeof(object) != 'undefined' && object != null && object != "") {
    		object = object.toLowerCase();
    	}
    	return object;
    };
    
    $scope.getElement = function (selector) {
    	return angular.element(document.querySelector(selector));
    };
    
    $scope.getAllElements = function (selector) {
  	  	return angular.element(document.querySelectorAll(selector));
    };
    
    $scope.convertNumberToWords = function(num) {
    	var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
        var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

        if ((num = num.toString()).length > 9) return 'overflow';
        var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return; var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
        return str;
    };
    
    $scope.convert12To24HoursTimeFormat = function(time) {
    	try {
    		time = time.toString();
    		
        	var hours = Number(time.match(/^(\d+)/)[1]);
        	var minutes = Number(time.match(/:(\d+)/)[1]);
        	var AMPM = time.match(/\s(.*)$/)[1];
        	
        	if(AMPM == "PM" && hours < 12) hours = hours+12;
        	if(AMPM == "AM" && hours == 12) hours = hours-12;
        	
        	if(minutes < 8) {
        		minutes = 0;
        	} else if (minutes <= 15 || minutes <= 21) {
        		minutes = 15;
        	} else if (minutes <= 30 || minutes <= 37) {
        		minutes = 30;
        	} else if (minutes <= 45 || minutes <= 52) {
        		minutes = 45;
        	} else if (minutes >= 53) {
        		minutes = 0;
        		if(hours == 23) {
        			hours = 0;
        		} else {
        			++hours;
        		}
        	}
        	
        	var sHours = hours.toString();
        	var sMinutes = minutes.toString();
        	
        	if(hours<10) sHours = "0" + sHours;
        	if(minutes<10) sMinutes = "0" + sMinutes;
        	
        	return sHours + ":" + sMinutes;
    	
    	} catch(e) {
    		return "00:00";
    	}
    };
    
    $scope.convert24To12HoursTimeFormat = function(time) {
    	try {
    		time = time.toString().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    	    if (time.length > 1) {
    	    	time 	= time.slice(1);
    	    	time[5] = +time[0] < 12 ? ' AM' : ' PM';
    	    	time[0] = +time[0] % 12 || 12;
    	    }
    	    return time.join('');
        	
    	} catch(e) {
    		return "12:00 PM";
    	}
    };
    
    $scope.getTodayDateInddmmmyyyyFormat = function(daysToAdd) {
    	var today 	= new Date();
    	
    	today.setDate(today.getDate() + daysToAdd);
    	
    	var day 	= today.getDate();
    	var month 	= today.getMonth();

    	var year 	= today.getFullYear();
    	
    	if(day < 10){
    	    day		= '0' + day;
    	} 
    	
    	return (day+'-'+monthNames[month]+'-'+year);
    };
    
    $scope.getDateTimeObject = function(dateString, timeString, is12HoursFormat) {
    	var year = 1900;
    	var month = 01;
    	var day = 01;
    	var hour = 00;
    	var minutes = 00;
    	
    	if(dateString.indexOf("-") != -1) {
    		var dateArr = dateString.split("-");
    		
    		day = dateArr[0];
    		month = monthNames.indexOf(dateArr[1]);
    		year = dateArr[2];
    	}
    	
    	if(timeString.indexOf(":") != -1) {
    		
    		var tmp_timeString = timeString;
    		if(is12HoursFormat) {
    			tmp_timeString = $scope.convert12To24HoursTimeFormat(tmp_timeString);
    		}
    		
    		var timeArr = tmp_timeString.split(":");
    		
    		hour = timeArr[0];
    		minutes = timeArr[1];
    	}
    	
    	return new Date(year, month, day, hour, minutes, 00);
    };
    
    $scope.getTimeIn12HoursFormat = function (date) {
    	var hours = date.getHours();
    	var minutes = date.getMinutes();
    	var ampm = hours >= 12 ? 'pm' : 'am';
    	hours = hours % 12;
    	hours = hours ? hours : 12; // the hour '0' should be '12'
    	minutes = minutes < 10 ? '0'+minutes : minutes;
    	var strTime = hours + ':' + minutes + ' ' + ampm;
    	return strTime;
    }
    
    $scope.dateTime_sort_asc = function (journey1, journey2) {
		if (journey1.dateTime > journey2.dateTime) return 1;
		if (journey1.dateTime < journey2.dateTime) return -1;
		return 0;
	};
	
	$scope.sortJourneysByDateTime = function () {
		if($scope.journeys.length > 1) {
			($scope.journeys).sort($scope.dateTime_sort_asc);
		}
	};
	
    $scope.getMaxJourneyOrder = function (journeyType) {
    	var maxJourneyOrder = "";
    	var journeyOrderArr	= [];
    	
    	if (journeyType == 'F' && $scope.flightJourneyDetails.flightJourney.length == 0) {
    		maxJourneyOrder = "F1";
    		
    	} else if (journeyType == 'F' && $scope.flightJourneyDetails.flightJourney.length > 0) {
    		try {
    			angular.forEach($scope.flightJourneyDetails.flightJourney, function(flightJourney) {
    				var journeyOrder = flightJourney.journeyCode;
    				
    				if(journeyOrder != "" && journeyOrder != "0") {
    					journeyOrder = journeyOrder.substring(1);
    				}
    				journeyOrderArr.push(journeyOrder);
    			});
    			
    			maxJourneyOrder = Math.max.apply(Math, journeyOrderArr);
    			maxJourneyOrder += 1;
    			maxJourneyOrder = "F"+maxJourneyOrder;
    		
    		} catch (e) {
    			maxJourneyOrder = "F111";
    		}
    		
    	} else if(journeyType == 'T' && $scope.trainJourneyDetails.trainJourney.length == 0) {
    		maxJourneyOrder = "T1";
    		
    	} else if (journeyType == 'T' && $scope.trainJourneyDetails.trainJourney.length > 0) {
    		try {
    			angular.forEach($scope.trainJourneyDetails.trainJourney, function(trainJourney) {
    				var journeyOrder = trainJourney.journeyCode;
    				
    				if(journeyOrder != "" && journeyOrder != "0") {
    					journeyOrder = journeyOrder.substring(1);
    				}
    				journeyOrderArr.push(journeyOrder);
    			});
    			
    			maxJourneyOrder = Math.max.apply(Math, journeyOrderArr);
    			maxJourneyOrder += 1;
    			maxJourneyOrder = "T"+maxJourneyOrder;
    		
    		} catch (e) {
    			maxJourneyOrder = "T111";
    		}
    	
    	} else if(journeyType == 'C' && $scope.carJourneyDetails.carJourney.length == 0) {
    		maxJourneyOrder = "C1";
    		
    	} else if (journeyType == 'C' && $scope.carJourneyDetails.carJourney.length > 0) {	
    		try {
    			angular.forEach($scope.carJourneyDetails.carJourney, function(carJourney) {
    				var journeyOrder = carJourney.journeyCode;
    				
    				if(journeyOrder != "" && journeyOrder != "0") {
    					journeyOrder = journeyOrder.substring(1);
    				}
    				journeyOrderArr.push(journeyOrder);
    			});
    			
    			maxJourneyOrder = Math.max.apply(Math, journeyOrderArr);
    			maxJourneyOrder += 1;
    			maxJourneyOrder = "C"+maxJourneyOrder;
    		
    		} catch (e) {
    			maxJourneyOrder = "C111";
    		}
    		
    	} else if(journeyType == 'A' && $scope.accommodationDetails.accommodations.length == 0) {
    		maxJourneyOrder = "A1";
    		
    	} else if (journeyType == 'A' && $scope.accommodationDetails.accommodations.length > 0) {
    		try {
    			angular.forEach($scope.accommodationDetails.accommodations, function(accommodation) {
    				var journeyOrder = accommodation.journeyOrder;
    				
    				if(journeyOrder != "" && journeyOrder != "0") {
    					journeyOrder = journeyOrder.substring(1);
    				}
    				journeyOrderArr.push(journeyOrder);
    			});
    			
    			maxJourneyOrder = Math.max.apply(Math, journeyOrderArr);
    			maxJourneyOrder += 1;
    			maxJourneyOrder = "A"+maxJourneyOrder;
    		
    		} catch (e) {
    			maxJourneyOrder = "A111";
    		}
    		
    	} else {
    		maxJourneyOrder = "";
    	}
    	
    	journeyOrderArr	= [];
    	
    	return maxJourneyOrder;
    };
    
    /*************************************** Delete All Itineraries (i.e. Flight/Train) for accommodation linking **********************************/
    
    $scope.clearItineraries = function (journeyMode) {
		var tmp_itineraries = [];
    	
    	angular.forEach($scope.itineraries, function(tmp_itinerary){
	  		if(tmp_itinerary.journeyMode != journeyMode) {
	  			tmp_itineraries.push(tmp_itinerary);
	  		}
	  	});
    	
    	$scope.itineraries = angular.copy(tmp_itineraries);
    	
    	//console.log("Itineraries after clearing itineraries : "+journeyMode);
    	//console.log($scope.itineraries);
	};
	
	/*************************************** Delete Itinerary (i.e. Flight, Train) for accommodation linking ***************************************/
	
	$scope.deleteItinerary = function (journeyCode) {
		var tmp_itineraries = [];
		
		angular.forEach($scope.itineraries, function(tmp_itinerary){
	  		if(tmp_itinerary.journeyCode != journeyCode) {
	  			tmp_itineraries.push(tmp_itinerary);
	  		}
	  	});
		
		$scope.itineraries = angular.copy(tmp_itineraries);
		
    	//console.log("Itineraries after deleting itinerary : "+journeyCode);
    	//console.log($scope.itineraries);
	};
	
    /*************************************** Delete Accommodation By Journey Code ***************************************/
    
    $scope.deleteAccommodationByJourneyCode = function (journeyCode) {
    	if($.trim(journeyCode) != "") {
    		var tmp_accommodations = [];
    		
    		angular.forEach($scope.accommodationDetails.accommodations, function(tmp_accommodation){
    	  		if(tmp_accommodation.journeyCode != journeyCode) {
    	  			tmp_accommodations.push(tmp_accommodation);
    	  		}
    	  	});
    		
    		$scope.accommodationDetails.accommodations = angular.copy(tmp_accommodations);
    	}
    };
    
    /*************************************** Delete Accommodation By Journey Order ***************************************/
    
    $scope.deleteAccommodationByJourneyOrder = function (journeyOrder) {
    	var tmp_accommodations = [];
		
		angular.forEach($scope.accommodationDetails.accommodations, function(tmp_accommodation) {
	  		if(tmp_accommodation.journeyOrder != journeyOrder) {
	  			tmp_accommodations.push(tmp_accommodation);
	  		}
	  	});
		
		$scope.accommodationDetails.accommodations = angular.copy(tmp_accommodations);
    };
    
    /*************************************** Delete All Flight/Train Accommodations *************************************************************/
	
	$scope.clearAccommodations = function (journeyMode) {
		var tmp_accommodations = [];
		var ids = "";
		
		angular.forEach($scope.accommodationDetails.accommodations, function(tmp_accommodation) {
			if(tmp_accommodation.journeyCode != "" && (tmp_accommodation.journeyCode).indexOf(journeyMode) != -1) {
				ids += tmp_accommodation.accommodationId + ",";
				
			} else {
				tmp_accommodations.push(tmp_accommodation);
			}
		});
		
		if(ids.length > 1) {
			$scope.deleteJourneyDetailsFromDB(7, ids.substring(0, ids.length-1));
    	}
		
		$scope.accommodationDetails.accommodations = angular.copy(tmp_accommodations);
		
		//console.log("Accommodations after clearing journeys : "+journeyMode);
    	//console.log($scope.accommodationDetails.accommodations);
		
	};
    
	/*************************************** Delete Accommodated Itinerary ***************************************/
	
	$scope.deleteAccommodatedItinerary = function (journeyCode) {
		if($.trim(journeyCode) != "") {
			var tmp_accommodatedItineraries = [];
			
			angular.forEach($scope.accommodatedItineraries, function(tmp_accommodatedItinerary){
		  		if(tmp_accommodatedItinerary.journeyCode != journeyCode) {
		  			tmp_accommodatedItineraries.push(tmp_accommodatedItinerary);
		  		}
		  	});
			
			$scope.accommodatedItineraries = angular.copy(tmp_accommodatedItineraries);
			
			//console.log("Accommodated itineraries after deletion : "+journeyCode);
			//console.log($scope.accommodatedItineraries);
		}
    };
	
	/*************************************** Delete All Flight/Train Accommodated Itineraries ****************************************************/
	
	$scope.clearAccommodatedItineraries = function (journeyMode) {
		var tmp_accommodatedItineraries = [];
		
		angular.forEach($scope.accommodatedItineraries, function(tmp_accommodatedItinerary) {
			if(tmp_accommodatedItinerary.journeyMode != journeyMode) {
				tmp_accommodatedItineraries.push(tmp_accommodatedItinerary);
			}
		});
		
		$scope.accommodatedItineraries = angular.copy(tmp_accommodatedItineraries);
		
		//console.log("Accommodated Itineraries after clearing journeys : "+journeyMode);
    	//console.log($scope.accommodatedItineraries);
	};
	
	/*************************************** Delete All Travel Journeys (i.e. Flight/Train/Car/Accommodation) *************************************/
	
	$scope.clearJourneys = function (journeyMode) {
		var tmp_journeys = [];
		var ids = "";
    	
    	angular.forEach($scope.journeys, function(tmp_journey) {
	  		
    		if(tmp_journey.journeyMode == journeyMode) {
	  			
    			if(tmp_journey.journeyMode == "C" && tmp_journey.carJourneyId != 0) {
    				ids += tmp_journey.carJourneyId +",";
    				
	  			} else if(tmp_journey.journeyMode == "A" && tmp_journey.accommodationId != 0) {
	  				ids += tmp_journey.accommodationId +",";
	  			}
	  			
	  		} else if ((journeyMode == "F" && tmp_journey.journeyMode == "A" && tmp_journey.journeyCode.indexOf("F") != -1 && tmp_journey.accommodationId != 0) || 
	  				   (journeyMode == "T" && tmp_journey.journeyMode == "A" && tmp_journey.journeyCode.indexOf("T") != -1 && tmp_journey.accommodationId != 0)) {
	  			ids += tmp_journey.accommodationId +",";
	  			
	  		} else {
	  			tmp_journeys.push(tmp_journey);
	  		}
    		
	  	});
    	
    	if(ids.length > 1) {
    		if(journeyMode == "C") {
        		$scope.deleteJourneyDetailsFromDB(5, ids.substring(0, ids.length-1));
        	} else if (journeyMode == "A") {
        		$scope.deleteJourneyDetailsFromDB(7, ids.substring(0, ids.length-1));
        	}
    	}
    	
    	$scope.journeys = angular.copy(tmp_journeys);
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log("Journeys after clearing journeys : "+journeyMode);
    	//console.log($scope.journeys);
	};
	
	/*************************************** Delete Travel Journey (i.e. Flight/Train/Car/Accommodation) By Journey Code ****************/
	
	$scope.deleteJourney = function (journeyCode) {
		var tmp_journeys = [];
		
		angular.forEach($scope.journeys, function(tmp_journey){
	  		if(tmp_journey.journeyCode != journeyCode) {
	  			tmp_journeys.push(tmp_journey);
	  		}
	  	});
    	
		$scope.journeys = angular.copy(tmp_journeys);
		
    	$scope.sortJourneysByDateTime();
    	
    	//console.log("Journeys after deleting journey by journeyCode : "+journeyCode);
    	//console.log($scope.journeys);
	};
	
	/*************************************** Delete Travel Journeys from DB according to their ID's *************************************/
    
    $scope.deleteJourneyDetailsFromDB = function (journeyMode, ids) {
    	
    	if($.trim($scope.travelId) != "" && $scope.travelId != "0" && $.trim(ids) != "") {
    		
    		var travelCarIds 	= "";
    		var travelAccommIds = "";
    		
    		var url = $scope.apiBaseUrl+"/api/request/journey/detail";
    	  	  
    		if(journeyMode == 5) {
    			travelCarIds = ids;
    			
    			//console.log("Deleting car journeys with ids : "+travelCarIds);
    		
    		} else if(journeyMode == 7) {
    			travelAccommIds = ids;
    			
    			//console.log("Deleting Accommodations with ids : "+travelCarIds);
    		}
    		
    	  	var headerData = {
    	  		winUserId			: $scope.winUserId,
    	  		domainName 			: $scope.domainName,
    	  		deviceId 			: $scope.ipAddress,
    	  		requestSource 		: $scope.requestSource,
    	  		systemName 			: $scope.systemName,
    	  		travelId			: $scope.travelId,
    	  		travelType			: $scope.travelType,
    	  		modeId				: journeyMode,
    	  		travelCarIds		: travelCarIds,
    	  		travelAccommIds		: travelAccommIds
    	  	};
    	
    	  	apiCallService.deleteRequest(url, headerData).then(function(response) {
    	  		if(response != null && response.data.status == "200") {
    	  			//console.log("Journey deleted : ");
    	  			//console.log(response.data);
    	  		}
    	  	});
    	}
    };
    
    /************************************************* REQUEST DETAILS SECTION *************************************************/
    
    $scope.setRequestType = function(requestType) {
  	  
    	if(requestType == 'individual') {
  		  	$scope.isIndividualRequest	= true;
  		  	$scope.isGroupRequest		= false;
  		  	$scope.isGuestRequest		= false;
  	  
  	  	} else if (requestType == 'group') {
  	  		$scope.isIndividualRequest	= false;
  	  		$scope.isGroupRequest		= true;
  	  		$scope.isGuestRequest		= false;
  			
  	  	} else if (requestType == 'guest') {
  	  		$scope.isIndividualRequest	= false;
  	  		$scope.isGroupRequest		= false;
  	  		$scope.isGuestRequest		= true;
  	  	}
    };
    
    /************************************************* SET ACCOMMODATION CHECKBOX **********************************************/
    
    $scope.setAccommodationRequired = function () {
  	  	if($scope.itineraries.length > 0 || $scope.accommodatedItineraries.length > 0) {
  	  		$scope.isAccommodationSelected = true;
  	  	
  	  	} else if ($scope.itineraries.length == 0 && $scope.accommodatedItineraries.length == 0 && $scope.accommodationDetails.accommodations.length == 0) {
  	  		$scope.isAccommodationSelected = false;
  	  	}
    };
    
    /************************************************* SHOW / HIDE TABS & DIVS *************************************************/
    
    $scope.setJourneyDivs = function (journeyMode) {
    	
    	if(journeyMode == "F") {
    		$("div.tab-pane").removeClass("active");
    		
    		if($("li.flightTab").hasClass("active")) {
    			if($scope.isTrainSelected) {
        			$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
        			
            	} else if($scope.isCarSelected) {
            		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            		
            	} else if($scope.isAccommodationSelected) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            		
            	} else if($scope.isFlightSelected) {
            		$("li.requestTab").removeClass("active");
            		$("li.flightTab, div#flight").addClass("active");
            	}
    			
    		} else {
    			if($scope.isTrainSelected && $("li.trainTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
        			
            	} else if($scope.isCarSelected && $("li.carTab").hasClass("active")) {
             		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            		
            	} else if($scope.isAccommodationSelected && $("li.accommodationTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            		
            	} else if($scope.isFlightSelected) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
            	}
    		}
    		
    		if(!$scope.isFlightSelected) {
        		$("li.flightTab").removeClass("active");
        	}
    		
    	} else if (journeyMode == "T") {
    		$("div.tab-pane").removeClass("active");
    		
    		if($("li.trainTab").hasClass("active")) {
    			if($scope.isFlightSelected) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             		
             	} else if($scope.isCarSelected) {
             		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            		
            	} else if($scope.isAccommodationSelected) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            		
            	} else if($scope.isTrainSelected) {
            		$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
            	}
    		
    		} else {
    			if($scope.isFlightSelected && $("li.flightTab").hasClass("active")) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             		
             	} else if($scope.isCarSelected && $("li.carTab").hasClass("active")) {
             		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            		
            	} else if($scope.isAccommodationSelected && $("li.accommodationTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            		
            	} else if($scope.isTrainSelected) {
            		$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
            	}
    		}
    		
    		if(!$scope.isTrainSelected) {
        		$("li.trainTab").removeClass("active");
        	}
    		
    	} else if (journeyMode == "C") {
    		$("div.tab-pane").removeClass("active");

    		if($("li.carTab").hasClass("active")) {
    			if($scope.isFlightSelected) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             	
        		} else if($scope.isTrainSelected) {
        			$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
            	
        		} else if($scope.isAccommodationSelected) {
        			$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            	
        		} else if($scope.isCarSelected) {
        			$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            	}
    			
    		} else {
    			if($scope.isFlightSelected && $("li.flightTab").hasClass("active")) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             		
             	} else if($scope.isTrainSelected && $("li.trainTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
        			
            	} else if($scope.isAccommodationSelected && $("li.accommodationTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            	
            	} else if($scope.isCarSelected) {
             		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            	}
    		}
    		
    		if(!$scope.isCarSelected) {
        		$("li.carTab").removeClass("active");
        	}
    		
    	} else if (journeyMode == "A") {
    		$("div.tab-pane").removeClass("active");

    		if($("li.accommodationTab").hasClass("active")) {
    			if($scope.isFlightSelected) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             	
        		} else if($scope.isTrainSelected) {
        			$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
            	
        		} else if($scope.isCarSelected) {
        			$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            	
        		} else if($scope.isAccommodationSelected) {
        			$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            	} 
    			
    		} else {
    			if($scope.isFlightSelected && $("li.flightTab").hasClass("active")) {
        			$("li.requestTab").removeClass("active");
             		$("li.flightTab, div#flight").addClass("active");
             		
             	} else if($scope.isTrainSelected && $("li.trainTab").hasClass("active")) {
            		$("li.requestTab").removeClass("active");
        			$("li.trainTab, div#train").addClass("active");
        			
            	} else if($scope.isCarSelected && $("li.carTab").hasClass("active")) {
             		$("li.requestTab").removeClass("active");
            		$("li.carTab, div#car").addClass("active");
            		
            	} else if($scope.isAccommodationSelected) {
            		$("li.requestTab").removeClass("active");
            		$("li.accommodationTab, div#hotel").addClass("active");
            	}
    		}
    		
    		if(!$scope.isAccommodationSelected) {
        		$("li.accommodationTab").removeClass("active");
        	}
    	}
    };
    
    /************************************************* FLIGHT JOURNEY SECTION *************************************************/
    
    $scope.toggleFlightBookingSection = function() {
    	var selectTabsFlag = true;
		
    	if($scope.isFlightSelected && $scope.flightJourneyDetails.flightJourney.length > 0) {
    		if($window.confirm("Do you want to delete all flight journeys ?")) {
    			$scope.isFlightSelected = !$scope.isFlightSelected;
    			$scope.clearFlightJourneyDetails();
    			
    		} else {
    			selectTabsFlag = false;
    			$scope.isFlightSelected = true;
    			$("#flightRequired").prop("checked", true);
    		}
    	
    	} else {
    		$scope.isFlightSelected = !$scope.isFlightSelected;
    	}
    	
    	if(selectTabsFlag) {
    		$scope.setJourneyDivs("F");
    	}
    };
    
    $scope.flyingFrom 				= {};
    $scope.flyingTo					= {};
    $scope.flightSourceCity 		= "";
    $scope.flightDestinationCity	= "";
    $scope.country					= 0;
    $scope.countryDesc				= "";
    $scope.flightDateOfJourney		= $scope.getTodayDateInddmmmyyyyFormat(0);
    $scope.flightPreferredTimeMode	= "-1";
    $scope.flightPreferredTimeModeDesc = "No Preference";
    $scope.flightPreferredTime		= 0;
    $scope.flightPreferredTimeDesc 	= $scope.getTimeIn12HoursFormat(new Date());
    
    $scope.flightPreferredAirline 	= "No Preference";
    $scope.flightPreferredClass 	= 31;
    $scope.flightPreferredClassDesc = "No Preference";
    $scope.flightPreferredSeat 		= 30;
    $scope.flightPreferredSeatDesc 	= "No Preference";
    $scope.flightStayType			= "2";
    $scope.flightStayTypeDesc		= "Transit House";
    
    $scope.flightReturnDate 		= $scope.getTodayDateInddmmmyyyyFormat(1);
	$scope.flightReturnTimeMode 	= -1;
	$scope.flightReturnTimeModeDesc = "No Preference";
	$scope.flightReturnTime 		= 0;
	$scope.flightReturnTimeDesc 	= "";
    
	$scope.flightJourneyDetails		= {
		flightJourney 			: [],
	    flightJourneyRemarks 	: "",
	    journeyType				: "O",
	    returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
	    returnTimeMode			: "-1",
	    returnTimeModeDesc		: "No Preference",
	    returnTime				: 0,
	    returnTimeDesc			: ""
	};
	
    $scope.setFlightJourneyType = function (event) {
    	
    	var journeyType = $(event.currentTarget).attr('id');
    	
    	if(!$(event.currentTarget).hasClass("active-agent")) {
    		
	    	if($scope.isFlightSelected && $scope.flightJourneyDetails.flightJourney.length > 0) {
	    		if($window.confirm("Are you sure you want to change journey type, this will delete all flight journeys ?")) {
	    			
	    			$scope.clearItineraries("F");
	    	    	$scope.clearJourneys("F");
	    			$scope.clearAccommodations("F");
	    	    	$scope.clearAccommodatedItineraries("F");
	    	    	
	    	    	$scope.flightJourneyDetails.flightJourney = [];
	    	  
	    	    	$scope.getAllElements(".flightJourneyType").removeClass("active-agent");
	    	    	$(event.currentTarget).addClass('active-agent');
	    	  
	    	    	if(journeyType == 'flightOneway') {
	    	    		$scope.flightJourneyDetails.journeyType = "O";
	    	    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
	    	    		$scope.clearFlightReturnJourneyDetails();
	    		
	    	    	} else if(journeyType == 'flightReturn') {
	    	    		$scope.flightJourneyDetails.journeyType = "R";
	    	    		$scope.getElement("div.flightReturnJourneyDiv").removeClass("disabled-group");
	    	  
	    	    	} else if(journeyType == 'flightMulticity') {
	    	    		$scope.flightJourneyDetails.journeyType = "M";
	    	    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
	    	    		$scope.clearFlightReturnJourneyDetails();
	    	    	}
	    	    	
	    		} else {
	    			$scope.getAllElements(".flightJourneyType").removeClass("active-agent");
	    			
	    			if($scope.flightJourneyDetails.journeyType == "O") {
	    				$scope.getElement("#flightOneway").addClass('active-agent');
	    				
	    			} else if ($scope.flightJourneyDetails.journeyType == "R") {
	    				$scope.getElement("#flightReturn").addClass('active-agent');
	    				
	    			} else if ($scope.flightJourneyDetails.journeyType == "M") {
	    				$scope.getElement("#flightMulticity").addClass('active-agent');
	    			}
	    		}
	    	
	    	} else {
	    		$scope.clearItineraries("F");
		    	$scope.clearJourneys("F");
				$scope.clearAccommodations("F");
		    	$scope.clearAccommodatedItineraries("F");
		    	
		    	$scope.flightJourneyDetails.flightJourney = [];
		  
		    	$scope.getAllElements(".flightJourneyType").removeClass("active-agent");
		    	$(event.currentTarget).addClass('active-agent');
	
		    	if(journeyType == 'flightOneway') {
		    		$scope.flightJourneyDetails.journeyType = "O";
		    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
		    		$scope.clearFlightReturnJourneyDetails();
			
		    	} else if(journeyType == 'flightReturn') {
		    		$scope.flightJourneyDetails.journeyType = "R";
		    		$scope.getElement("div.flightReturnJourneyDiv").removeClass("disabled-group");
		  
		    	} else if(journeyType == 'flightMulticity') {
		    		$scope.flightJourneyDetails.journeyType = "M";
		    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
		    		$scope.clearFlightReturnJourneyDetails();
		    	}
	    	}
    	}
    };
    
    $scope.setFlightJourneyTypeOnEdit = function () {
    	
    	$scope.getAllElements(".flightJourneyType").removeClass("active-agent");

    	if($scope.flightJourneyDetails.journeyType == 'O') {
    		$("#flightOneway").addClass('active-agent');
    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearFlightReturnJourneyDetails();

    	} else if($scope.flightJourneyDetails.journeyType == 'R') {
    		$("#flightReturn").addClass('active-agent');
    		$scope.getElement("div.flightReturnJourneyDiv").removeClass("disabled-group");
  
    	} else if($scope.flightJourneyDetails.journeyType == 'M') {
    		$("#flightMulticity").addClass('active-agent');
    		$scope.getElement("div.flightReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearFlightReturnJourneyDetails();
    	}
    };
    
    $scope.clearFlightJourneyDetails = function () {
    	$scope.clearJourneys("F");
    	$scope.clearItineraries("F");
    	$scope.clearAccommodations("F");
    	$scope.clearAccommodatedItineraries("F");
    	$scope.setAccommodationRequired();
    	
    	$scope.flightJourneyDetails		= {
			flightJourney 			: [],
		    flightJourneyRemarks 	: "",
		    journeyType				: "O",
		    returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
		    returnTimeMode			: "-1",
		    returnTimeModeDesc		: "No Preference",
		    returnTime				: 0,
		    returnTimeDesc			: ""
		};
    	
    	$scope.getAllElements(".flightJourneyType").removeClass("active-agent");
    	$scope.getElement("#flightOneway").addClass('active-agent');
    };
    
    $scope.clearFlightReturnJourneyDetails = function () {
    	$scope.flightJourneyDetails.returnDate 			= $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.flightJourneyDetails.returnTimeMode 		= -1;
    	$scope.flightJourneyDetails.returnTimeModeDesc 	= "No Preference";
    	$scope.flightJourneyDetails.returnTime 			= 0;
    	$scope.flightJourneyDetails.returnTimeDesc 		= "";
    	
    	$scope.flightReturnDate 		= $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.flightReturnTimeMode 	= -1;
    	$scope.flightReturnTimeModeDesc = "No Preference";
    	$scope.flightReturnTime 		= 0;
    	$scope.flightReturnTimeDesc 	= "";
    };
    
    $scope.localSearch = function(str, citiesList) {
    	//validateChars(document.getElementById("flying_from_value"), 30, 'sector');
    	
    	var matches = [];
    	citiesList.forEach(function(city) {
    		var cityNameCode = city.cityName + ' - ' + city.airportCode + ' , ' + city.countryCode;
        
	        if ((city.cityName.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || (city.airportCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
	            (city.countryCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || (cityNameCode.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) 
	        {
	        	city["cityNameCode"] = cityNameCode;
	            matches.push(city);
	        }
    	});
    	
    	return matches;
    };
      
    $scope.onFlyingFromSelected = function(selected) {
    	if (selected) {
    		$scope.flyingFrom = selected.originalObject;
    		
    		//$scope.onCountryChange('flyingFrom');
  	  	}
    };
    
    $scope.onFlyingToSelected = function(selected) {
  	  	if (selected) {
  	  		$scope.flyingTo = selected.originalObject;
  	  		$scope.country = selected.originalObject.countryId;
  	  		$scope.countryDesc = selected.originalObject.countryName;
  	  		
  	  		//$scope.onCountryChange('flyingTo');
  	  	}
    };
    
    $scope.setFlyingFrom = function () {
    	var flyingFromVal = $scope.getElement("#flying_from_value").val();
    	
    	for(var i=0; i < $scope.citiesList.length; i++) {
    		var cityNameCode = $scope.citiesList[i].cityName + ' - ' + $scope.citiesList[i].airportCode + ' , ' + $scope.citiesList[i].countryCode;
    		
    		if ($.trim(cityNameCode) == $.trim(flyingFromVal)) {
    			$scope.flyingFrom = $scope.citiesList[i];
    			$scope.flyingFrom["cityNameCode"] = cityNameCode;
 	            
 	        	break;
 	        }
    	}
    	
    	//console.log($scope.flyingFrom);
    };
    
    $scope.setFlyingTo = function () {
    	var flyingToVal = $scope.getElement("#flying_to_value").val();
    	
    	for(var i=0; i < $scope.citiesList.length; i++) {
    		var cityNameCode = $scope.citiesList[i].cityName + ' - ' + $scope.citiesList[i].airportCode + ' , ' + $scope.citiesList[i].countryCode;
    		
    		if ($.trim(cityNameCode) == $.trim(flyingToVal)) {
    			$scope.flyingTo = $scope.citiesList[i];
    			$scope.flyingTo["cityNameCode"] = cityNameCode;
 	            
 	        	break;
 	        }
    	}
    	
    	//console.log($scope.flyingTo);
    };
    
    $scope.onCountryChange = function(place) {
    	//console.log($scope.flyingFrom.countryId + " : " +$scope.flyingTo.countryId);
    	var timeoutInterval = 100;
    	
    	if(typeof($scope.flyingFrom.countryId) != 'undefined' && typeof($scope.flyingTo.countryId) != 'undefined') {
    	
	    	if($scope.travelType != "I" && $scope.flyingFrom.countryId != $scope.flyingTo.countryId) {
	    		if($window.confirm("Do you want to travel internationally ? Your Train & Car itinerary details will get deleted !")) {
	    			$scope.travelType = 'I';
	        		$scope.travelTypeDesc = 'International';
	        		$scope.advanceForexLabel = "Forex Required";
	        		
	        		$scope.isTrainSelected = false;
		    		$scope.isCarSelected = false;
		    		
		    		$scope.clearTrainJourneyDetails();
		    		$scope.clearCarJourneyDetails();
		    		
		    		$scope.getRequestDataOnTravelTypeChange();
	    		
	    		} else if(place == 'flyingFrom') {
	    			$scope.flyingFrom = {};
	    			
	    			$timeout(function(){
	    				$scope.getElement("#flying_from_value").val("");
	    	        }, timeoutInterval);
	    			
	    		} else if(place == 'flyingTo') {
	    			$scope.flyingTo = {};
	      	  		$scope.country = 0;
	      	  		$scope.countryDesc = "";
	      	  		
		      	  	$timeout(function(){
		      	  		$scope.getElement("#flying_to_value").val("");
	    	        }, timeoutInterval);
	    		}
	    	
	    	} else if ($scope.travelType != "D" && $scope.flyingFrom.countryId == $scope.flyingTo.countryId) {
	    		if($window.confirm("Do you want to travel within same country ?")) {
	    			$scope.travelType = 'D';
		    		$scope.travelTypeDesc = 'Domestic';
		    		$scope.advanceForexLabel = "Advance Required";
		    		
		    		$scope.isTrainSelected = true;
		    		$scope.isCarSelected = true;
		    		
		    		$scope.clearTrainJourneyDetails();
		    		$scope.clearCarJourneyDetails();
		    		
		    		$scope.getRequestDataOnTravelTypeChange();
	    		
	    		} else if(place == 'flyingFrom') {
	    			$scope.flyingFrom = {};
	    			
	    			$timeout(function(){
	    				$scope.getElement("#flying_from_value").val("");
	    	        }, timeoutInterval);
	    			
	    		} else if(place == 'flyingTo') {
	    			$scope.flyingTo = {};
	      	  		$scope.country = 0;
	      	  		$scope.countryDesc = "";
	      	  		
		      	  	$timeout(function(){
		      	  		$scope.getElement("#flying_to_value").val("");
	    	        }, timeoutInterval);
	    		}
	    	}
	    	
    	}
    };
    
    $scope.getRequestDataOnTravelTypeChange = function () {
    	$scope.getElement("#loading-div p.processingMessage").html("Please wait while fetching traveller settings.");
    	$scope.showProcessing();
    	
    	$timeout(function(){
    		$scope.getRequestSetting();
        	$scope.getLinkedTravelRequestList();
        	$scope.getTravelPreferences(1, "");
        	$scope.getTravelPreferences(2, "");
        	$scope.getTravelPreferences(5, "");
        	$scope.getRequestApproverList();
        	$scope.getDefaultApproverList();
        	$scope.getCitieslist("");
    	}, 500);
    };
    
    $scope.openFlightPreferredTimeDiv = function(event, timeMode, journeyType) {
    	
    	if(timeMode != null) {
    		$(event.currentTarget).parents('.radio-box-section').hide().siblings('.calendar-content').show();
    		
    		if(journeyType == "R") {
    			$scope.flightReturnTimeMode 	= timeMode.timeModeId;
            	$scope.flightReturnTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.flightReturnTimeMode == "-1") {
            		$scope.flightReturnTimeDesc = "";
            		$scope.getElement("#flightReturnTimeDesc").val("");
        		}
            	
    		} else {
    			$scope.flightPreferredTimeMode = timeMode.timeModeId;
            	$scope.flightPreferredTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.flightPreferredTimeMode == "-1") {
            		$scope.flightPreferredTimeDesc = "";
            		$scope.getElement("#flightPreferredTimeDesc").val("");
        		}
    		}
    		
    	} else {
    		if(journeyType == "R") {
    			$scope.flightReturnTimeMode = "-1";
            	$scope.flightReturnTimeModeDesc = "No Preference";
            	$scope.flightReturnTimeDesc = "";
        		$scope.getElement("#flightReturnTimeDesc").val("");
            	
    		} else {
    			$scope.flightPreferredTimeMode = "-1";
            	$scope.flightPreferredTimeModeDesc = "No Preference";
            	$scope.flightPreferredTimeDesc = "";
        		$scope.getElement("#flightPreferredTimeDesc").val("");
    		}
    	}
    };
    
    $scope.onFlightClassClick = function (event, flightClass) {
  	  	$scope.getAllElements(".flightClass").removeClass("active-agent");
  	  	$(event.currentTarget).addClass('active-agent');
  	  
  	  	$scope.flightPreferredClass = flightClass.classId;
  	  	$scope.flightPreferredClassDesc = flightClass.eligibility;
    };
    
    $scope.onFlightSeatClick = function (event, flightSeat) {
  	  	$scope.getAllElements(".flightSeat").removeClass("active-agent");
  	  	$(event.currentTarget).addClass('active-agent');
  	  
  	  	$scope.flightPreferredSeat = flightSeat.seatId;
  	  	$scope.flightPreferredSeatDesc = flightSeat.seatName;
    };
    
    $scope.onFlightStayTypeChange = function() {
    	if($scope.flightStayType == "-1") {
	  		$scope.flightStayTypeDesc = "N/A";
	  		
	  	} else {
	  		var keepGoing = true;
	  		
	  		angular.forEach($scope.flightStayTypeList, function(flightStayType){
		  		if(keepGoing && flightStayType.stayTypeId == $scope.flightStayType) {
		  			$scope.flightStayTypeDesc = flightStayType.stayTypeName;
		  			keepGoing = false;
		  		}
		  	});
	  	}
    };
    
    $scope.validateFlightJourney = function () {
    	
    	var flyingFrom 		= $("#flying_from_value");
    	var flyingTo 		= $("#flying_to_value");
    	var dateOfJourney 	= $("#flightDateOfJourney");
    	var prefTimeDesc	= $("#flightPreferredTimeDesc");
    	var prefAirline		= $("#preferredAirline");
    	
    	if($.trim(flyingFrom.val()) == "") {
    		$window.alert("Please enter flying from city.");
    		flyingFrom.focus();
    		return false;
    		
    	} else if ($.trim(flyingTo.val()) == "") {
    		$window.alert("Please enter flying to city.");
    		flyingTo.focus();
    		return false;
    	
    	} else if ($.trim($scope.countryDesc) == "") {
    		$window.alert("No country found !\nPlease select a valid flying to city.");
    		flyingTo.focus();
    		return false;
    	
    	} else if ($.trim(dateOfJourney.val()) == "") {
    		$window.alert("Please enter journey date.");
    		dateOfJourney.focus();
    		return false;
    	
    	} else if($scope.toLowerCaseString($scope.flightPreferredTimeModeDesc) != 'no preference' && $.trim(prefTimeDesc.val()) == "") {
    		$window.alert("Please enter journey preferred time.");
    		$(".flightPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
    		prefTimeDesc.focus();
    		return false;
    	
    	} else if($.trim(prefAirline.val()) == "") {
    		$window.alert("Please enter preferred airline.");
    		if(!$(".flightPreferencesDiv").is(":visible")) {
    			$("#flightPreferencesBtn").click();
    		}
    		prefAirline.focus();
    		return false;
    	}
    	
    	if($scope.flightJourneyDetails.journeyType == 'R') {
    		var returnDate		= $("#flightReturnDate");
        	var returnTimeDesc	= $("#flightReturnTimeDesc");
        	
    		if($.trim(returnDate.val()) == "") {
        		$window.alert("Please enter return journey date.");
        		returnDate.focus();
        		return false;
        		
        	} else if($scope.toLowerCaseString($scope.flightReturnTimeModeDesc) != "no preference" && $.trim(returnTimeDesc.val()) == "") {
        		$window.alert("Please enter return journey preferred time.");
        		$(".flightPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
        		returnTimeDesc.focus();
        		return false;
        	}
    	}
    	
    	return true;
    };
    
    $scope.addFlightJourney = function() {
    	
    	if($scope.validateFlightJourney()) {
	  	  	if($scope.flightJourneyDetails.journeyType == 'R') {
	  	  		$scope.flightReturnDate 	= $scope.getElement("#flightReturnDate").val();
  	  			$scope.flightReturnTimeDesc	= $scope.getElement("#flightReturnTimeDesc").val();
  	  			
	  	      	$scope.flightJourneyDetails.returnDate 			= $scope.flightReturnDate;
	  	      	$scope.flightJourneyDetails.returnTimeMode 		= $scope.flightReturnTimeMode;
	  	      	$scope.flightJourneyDetails.returnTimeModeDesc 	= $scope.flightReturnTimeModeDesc;
	  	      	$scope.flightJourneyDetails.returnTime 			= $scope.flightReturnTime;
	  	      	$scope.flightJourneyDetails.returnTimeDesc 		= $scope.flightReturnTimeDesc;
	  	  	}
	    	
	  	  	$scope.flightSourceCity 		= $scope.getElement("#flying_from_value").val();
	  	  	$scope.flightDestinationCity 	= $scope.getElement("#flying_to_value").val();
	  	  	
	  	  	$scope.flightDateOfJourney 		= $scope.getElement("#flightDateOfJourney").val();
		  	$scope.flightPreferredTimeDesc 	= $scope.getElement("#flightPreferredTimeDesc").val();
		  	
		  	if($scope.toLowerCaseString($scope.flightPreferredTimeModeDesc) == 'no preference') {
		  		$scope.flightPreferredTimeDesc = "";
		  	}
		  	
	  	  	var tempFlightJourney = {
				journeyCode				: $scope.getMaxJourneyOrder("F"),
				source					: $scope.flightSourceCity,
				destination				: $scope.flightDestinationCity,
				country					: $scope.country,
				countryDesc				: $scope.countryDesc,
				dateOfJourney			: $scope.flightDateOfJourney,
				preferredTimeMode		: $scope.flightPreferredTimeMode,
				preferredTimeModeDesc	: $scope.flightPreferredTimeModeDesc,
				preferredTime			: $scope.flightPreferredTime,
				preferredTimeDesc		: $scope.flightPreferredTimeDesc,
				preferredAirline		: $scope.flightPreferredAirline,
				travelClass				: $scope.flightPreferredClass,
				travelClassDesc			: $scope.flightPreferredClassDesc,
				preferredSeat			: $scope.flightPreferredSeat,
				preferredSeatDesc		: $scope.flightPreferredSeatDesc,
				stayType				: $scope.flightStayType,
				stayTypeDesc			: $scope.flightStayTypeDesc
	  	  	};
	  	  	
	  	  	$scope.flightJourneyDetails.flightJourney.push(tempFlightJourney);
	  	  	//$scope.isAccommodationSelected = true;
	  	  	//console.log($scope.flightJourneyDetails);
	  	  	
	  	  	$scope.generateFlightItinerary(tempFlightJourney, false);
	  	  	$scope.generateFlightJourney(tempFlightJourney, false);
	  	  	
	  	  	$scope.setAccommodationRequired();
    	}
    };
    
    $scope.generateFlightItinerary = function (tempFlightJourney, editFlag) {
    	
    	if(tempFlightJourney.stayType != "-1") {
  	  		var tmp_itinerary = {
  	  	  		journeyMode			: "F",
  	  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  	  		source				: tempFlightJourney.source,
  	  	  		destination			: tempFlightJourney.destination,
  	  	  		journeyDesc			: tempFlightJourney.source + " -> " +tempFlightJourney.destination + " (Flight)",
  	  	  		dateOfJourney		: tempFlightJourney.dateOfJourney,
  	  	  		preferredTimeDesc	: tempFlightJourney.preferredTimeDesc,
  	  	  		stayType			: tempFlightJourney.stayType,
	  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	  	};
  	  	  	$scope.itineraries.push(tmp_itinerary);
  	  	  	
  	  	  	if($scope.flightJourneyDetails.journeyType == 'R') {
  	  	  		tmp_itinerary = {
  	  	  			journeyMode			: "F",
  	  	  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  	  	  		source				: tempFlightJourney.destination,
  	  	  	  		destination			: tempFlightJourney.source,
  	  	  	  		journeyDesc			: tempFlightJourney.destination + " -> " +tempFlightJourney.source + " (Flight)",
  	  	  	  		dateOfJourney		: $scope.flightJourneyDetails.returnDate,
  	  	  	  		preferredTimeDesc	: $scope.flightJourneyDetails.returnTimeDesc,
  	  	  	  		stayType			: tempFlightJourney.stayType,
  	  	  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	  	  	};
  	  	  	
  	  	  		if(!editFlag) {
  	  	  			tmp_itinerary.journeyCode 		= $scope.getMaxJourneyOrder("F");
  	  	  			tmp_itinerary.dateOfJourney 	= $scope.flightReturnDate;
  	  	  			tmp_itinerary.preferredTimeDesc = $scope.flightReturnTimeDesc;
  	  	  		}
  	  	  		
  	  	  	  	$scope.itineraries.push(tmp_itinerary);
  	  	  	}
  	  	  	
  	  	  	//console.log($scope.itineraries);
  	  	}
    };
    
    $scope.generateFlightJourney = function (tempFlightJourney, editFlag) {
    	var tmp_journey = {
  	  		journeyMode			: "F",
  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  		source				: tempFlightJourney.source,
  	  		destination			: tempFlightJourney.destination,
  	  		dateOfJourney		: tempFlightJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempFlightJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempFlightJourney.dateOfJourney, tempFlightJourney.preferredTimeDesc, true),
  	  		stayType			: tempFlightJourney.stayType,
  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if($scope.flightJourneyDetails.journeyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "F",
  	  	  		journeyCode 		: tempFlightJourney.journeyCode,
  	  	  		source				: tempFlightJourney.destination,
  	  	  		destination			: tempFlightJourney.source,
  	  	  		dateOfJourney		: $scope.flightJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.flightJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.flightJourneyDetails.returnDate, $scope.flightJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempFlightJourney.stayType,
  	  	  		stayTypeDesc		: tempFlightJourney.stayTypeDesc
  	  	  	};
    		
    		if(!editFlag) {
    			tmp_journey.journeyCode 		= $scope.getMaxJourneyOrder("F");
    			tmp_journey.dateOfJourney 		= $scope.flightReturnDate;
    			tmp_journey.preferredTimeDesc 	= $scope.flightReturnTimeDesc;
    			tmp_journey.dateTime 			= $scope.getDateTimeObject($scope.flightReturnDate, $scope.flightReturnTimeDesc, true);
  	  		}
    		
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log($scope.journeys);
    };
    
    $scope.editFlightJourney = function (index, flightJourney) {
  	  	//console.log(flightJourney);
  	  			
	  	$scope.flightSourceCity 		= flightJourney.source;
	    $scope.flightDestinationCity	= flightJourney.destination;
	    $scope.country					= flightJourney.country;
	    $scope.countryDesc				= flightJourney.countryDesc;
	    $scope.flightDateOfJourney		= flightJourney.dateOfJourney;
	    $scope.flightPreferredTimeMode	= flightJourney.preferredTimeMode;
	    $scope.flightPreferredTimeModeDesc = flightJourney.preferredTimeModeDesc;
	    $scope.flightPreferredTime		= flightJourney.preferredTime;
	    $scope.flightPreferredTimeDesc 	= flightJourney.preferredTimeDesc;
	    
	    $scope.flightPreferredAirline 	= flightJourney.preferredAirline;
	    
	    $scope.flightPreferredClass 	= flightJourney.travelClass;
	    $scope.flightPreferredClassDesc = flightJourney.travelClassDesc;
	    $scope.flightPreferredSeat 		= flightJourney.preferredSeat;
	    $scope.flightPreferredSeatDesc 	= flightJourney.preferredSeatDesc;
	    $scope.flightStayType			= flightJourney.stayType.toString();
	    $scope.flightStayTypeDesc		= flightJourney.stayTypeDesc;
	    
	    if($scope.toLowerCaseString(flightJourney.preferredTimeModeDesc) == 'no preference') {
	  		$scope.flightPreferredTimeDesc = "";
	  	}
	    
	    $scope.getElement("#flying_from_value").val(flightJourney.source);
	  	$scope.getElement("#flying_to_value").val(flightJourney.destination);
	  	
	  	$scope.setFlyingFrom();
	  	$scope.setFlyingTo();
	  	
	  	$('#flightDateOfJourney').datepicker('update', flightJourney.dateOfJourney);
	  	$scope.getElement("#flightPreferredTimeDesc").val(flightJourney.preferredTimeDesc);
	  	
	  	$scope.getAllElements("[name='flightPreferredTimeMode']").prop("checked", false);
	  	$scope.getElement("#flightPreferredTimeMode"+$scope.flightPreferredTimeMode).prop("checked", true);
	  	
	  	if(parseInt($scope.flightPreferredTimeMode) != -1) {
	  		$(".flightPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
	  	} else {
	  		$(".flightPreferredTimeModeDescDiv").show().siblings('.calendar-content').hide();
	  	}
	  	
	  	if($scope.flightJourneyDetails.journeyType == 'R') {
	  		$scope.flightReturnDate			= $scope.flightJourneyDetails.returnDate;
  	      	$scope.flightReturnTimeMode		= $scope.flightJourneyDetails.returnTimeMode;
  	      	$scope.flightReturnTimeModeDesc	= $scope.flightJourneyDetails.returnTimeModeDesc;
  	      	$scope.flightReturnTime			= $scope.flightJourneyDetails.returnTime;
  	      	$scope.flightReturnTimeDesc		= $scope.flightJourneyDetails.returnTimeDesc;
  	      	
  	      	$('#flightReturnDate').datepicker('update', $scope.flightReturnDate);
		  	$scope.getElement("#flightReturnTimeDesc").val($scope.flightReturnTimeDesc);
		  	
	  		$scope.getAllElements("[name='flightPreferredReturnTimeMode']").prop("checked", false);
	  		$scope.getElement("#flightPreferredReturnTimeMode"+$scope.flightReturnTimeMode).prop("checked", true);
	  		
	  		if(parseInt($scope.flightReturnTimeMode) != -1) {
		  		$(".flightPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
		  	} else {
		  		$(".flightPreferredReturnTimeModeDescDiv").show().siblings('.calendar-content').hide();
		  	}
	  	}
	  	
	    $scope.getAllElements(".flightClass").removeClass("active-agent");
	  	$scope.getElement("li#flightClass_"+flightJourney.travelClass).addClass('active-agent');
	  	
	    $scope.getAllElements(".flightSeat").removeClass("active-agent");
	  	$scope.getElement("li#flightSeat_"+flightJourney.preferredSeat).addClass('active-agent');
	    
	    $scope.deleteFlightJourney(index, flightJourney);
    };
    
    $scope.deleteFlightJourney = function (index, flightJourney) {
    	//console.log(flightJourney);
    	
    	if($scope.flightJourneyDetails.journeyType == 'R') {
    		$scope.clearItineraries("F");
    		$scope.clearJourneys("F");
    		$scope.clearAccommodations("F");
        	$scope.clearAccommodatedItineraries("F");
    	
    	} else {
    		$scope.deleteJourney(flightJourney.journeyCode);
    		
    		if(flightJourney.stayType != "-1") {
    			$scope.deleteItinerary(flightJourney.journeyCode);
    	    	$scope.deleteAccommodatedItinerary(flightJourney.journeyCode);
    	    	$scope.deleteAccommodationByJourneyCode(flightJourney.journeyCode);
    		}
    	}
    	
    	($scope.flightJourneyDetails.flightJourney).splice(index, 1);
    	
    	$scope.setAccommodationRequired();
    };
    
    /************************************************* TRAIN JOURNEY SECTION *************************************************/
    
    $scope.toggleTrainBookingSection = function() {
    	var selectTabsFlag = true;
    	
    	if($scope.isTrainSelected && $scope.trainJourneyDetails.trainJourney.length > 0) {
    		if($window.confirm("Do you want to delete all train journeys ?")) {
    			$scope.isTrainSelected = !$scope.isTrainSelected;
    			$scope.clearTrainJourneyDetails();
    			
    		} else {
    			selectTabsFlag = false;
    			$scope.isTrainSelected = true;
    			$("#trainRequired").prop("checked", true);
    		}
    	
    	} else {
    		$scope.isTrainSelected = !$scope.isTrainSelected;
    	}
    	
    	if(selectTabsFlag) {
    		$scope.setJourneyDivs("T");
    	}
    	
    };
    
    $scope.trainSource 				= {};
    $scope.trainDestination			= {};
    $scope.trainSourceCity 			= "";
    $scope.trainDestinationCity		= "";
    $scope.trainDateOfJourney		= $scope.getTodayDateInddmmmyyyyFormat(0);
    $scope.trainPreferredTimeMode	= "-1";
    $scope.trainPreferredTimeModeDesc = "No Preference";
    $scope.trainPreferredTime		= 0;
    $scope.trainPreferredTimeDesc 	= $scope.getTimeIn12HoursFormat(new Date());
    
    $scope.trainPreferred 			= "No Preference";
    $scope.trainPreferredClass 		= "30";
    $scope.trainPreferredClassDesc = "No Preference";
    $scope.trainPreferredSeat 		= "3";
    $scope.trainPreferredSeatDesc 	= "Lower";
    $scope.trainTicketType			= "0";
    $scope.trainTicketTypeDesc		= "Normal";
    $scope.trainStayType			= "2";
    $scope.trainStayTypeDesc		= "Transit House";
    
    $scope.trainReturnDate 			= $scope.getTodayDateInddmmmyyyyFormat(1);
	$scope.trainReturnTimeMode 		= -1;
	$scope.trainReturnTimeModeDesc 	= "No Preference";
	$scope.trainReturnTime 			= 0;
	$scope.trainReturnTimeDesc 		= "";
    
    $scope.trainJourneyDetails		= {
		trainJourney			: [],
        trainJourneyRemarks 	: "",
    	journeyType				: "O",
        returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
        returnTimeMode			: "-1",
        returnTimeModeDesc		: "No Preference",
        returnTime				: 0,
        returnTimeDesc			: ""
    };

    $scope.setTrainJourneyType = function (event) {
    	
    	var journeyType = $(event.currentTarget).attr('id');
    	
    	if(!$(event.currentTarget).hasClass("active-agent")) {
    		
    		if($scope.isTrainSelected && $scope.trainJourneyDetails.trainJourney.length > 0) {
        		if($window.confirm("Are you sure you want to change journey type, this will delete all train journeys ?")) {
        			
        			$scope.clearItineraries("T");
        	    	$scope.clearJourneys("T");
        	    	$scope.clearAccommodations("T");
        	    	$scope.clearAccommodatedItineraries("T");
        	    	
        	    	$scope.trainJourneyDetails.trainJourney = [];
        	    	
        	  	  	$scope.getAllElements(".trainJourneyType").removeClass("active-agent");
        	  	  	$(event.currentTarget).addClass("active-agent");
        	    	
        	    	if(journeyType == 'trainOneway') {
        	    		$scope.trainJourneyDetails.journeyType = "O";
        	    		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
        	    		$scope.clearTrainReturnJourneyDetails();
        	    		
        	    	} else if(journeyType == 'trainReturn') {
        	    		$scope.trainJourneyDetails.journeyType = "R";
        	    		$scope.getElement("div.trainReturnJourneyDiv").removeClass("disabled-group");
        	  	  
        	    	} else if(journeyType == 'trainMulticity') {
        	    		$scope.trainJourneyDetails.journeyType = "M";
        	    		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
        	    		$scope.clearTrainReturnJourneyDetails();
        	    	}
        	    	
        		} else {
        			$scope.getAllElements(".trainJourneyType").removeClass("active-agent");
        	    			
        			if($scope.trainJourneyDetails.journeyType == "O") {
        				$scope.getElement("#trainOneway").addClass('active-agent');
        				
        			} else if ($scope.trainJourneyDetails.journeyType == "R") {
        				$scope.getElement("#trainReturn").addClass('active-agent');
        				
        			} else if ($scope.trainJourneyDetails.journeyType == "M") {
        				$scope.getElement("#trainMulticity").addClass('active-agent');
        			}
        		}
        				
        	} else {
        		$scope.clearItineraries("T");
            	$scope.clearJourneys("T");
            	$scope.clearAccommodations("T");
            	$scope.clearAccommodatedItineraries("T");
            	
            	$scope.trainJourneyDetails.trainJourney = [];
            	
          	  	$scope.getAllElements(".trainJourneyType").removeClass("active-agent");
          	  	$(event.currentTarget).addClass("active-agent");
            	
            	if(journeyType == 'trainOneway') {
            		$scope.trainJourneyDetails.journeyType = "O";
            		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
            		$scope.clearTrainReturnJourneyDetails();
            		
            	} else if(journeyType == 'trainReturn') {
            		$scope.trainJourneyDetails.journeyType = "R";
            		$scope.getElement("div.trainReturnJourneyDiv").removeClass("disabled-group");
          	  
            	} else if(journeyType == 'trainMulticity') {
            		$scope.trainJourneyDetails.journeyType = "M";
            		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
            		$scope.clearTrainReturnJourneyDetails();
            	}
        	}
    	}
    };
      
    $scope.setTrainJourneyTypeOnEdit = function () {
    	
    	$scope.getAllElements(".trainJourneyType").removeClass("active-agent");

    	if($scope.trainJourneyDetails.journeyType == 'O') {
    		$("#trainOneway").addClass('active-agent');
    		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearTrainReturnJourneyDetails();

    	} else if($scope.trainJourneyDetails.journeyType == 'R') {
    		$("#trainReturn").addClass('active-agent');
    		$scope.getElement("div.trainReturnJourneyDiv").removeClass("disabled-group");
  
    	} else if($scope.trainJourneyDetails.journeyType == 'M') {
    		$("#trainMulticity").addClass('active-agent');
    		$scope.getElement("div.trainReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearTrainReturnJourneyDetails();
    	}
    };
    
    $scope.clearTrainJourneyDetails = function () {
    	
    	$scope.clearItineraries("T");
    	$scope.clearJourneys("T");
    	$scope.clearAccommodations("T");
    	$scope.clearAccommodatedItineraries("T");
    	$scope.setAccommodationRequired();
    	
    	$scope.trainJourneyDetails	= {
			trainJourney			: [],
	        trainJourneyRemarks 	: "",
	    	journeyType				: "O",
	        returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
	        returnTimeMode			: "-1",
	        returnTimeModeDesc		: "No Preference",
	        returnTime				: 0,
	        returnTimeDesc			: ""
	    };
    	
    	$scope.getAllElements(".trainJourneyType").removeClass("active-agent");
    	$scope.getElement("#trainOneway").addClass('active-agent');
    };
    
    $scope.clearTrainReturnJourneyDetails = function () {
    	$scope.trainJourneyDetails.returnDate = $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.trainJourneyDetails.returnTimeMode = -1;
    	$scope.trainJourneyDetails.returnTimeModeDesc = "No Preference";
    	$scope.trainJourneyDetails.returnTime = 0;
    	$scope.trainJourneyDetails.returnTimeDesc = "";
    	
    	$scope.trainReturnDate 			= $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.trainReturnTimeMode 		= -1;
    	$scope.trainReturnTimeModeDesc 	= "No Preference";
    	$scope.trainReturnTime 			= 0;
    	$scope.trainReturnTimeDesc 		= "";
    };
    
    $scope.onTrainSourceSelected = function(selected) {
    	if (selected) {
    		$scope.trainSource = selected.originalObject;
  	  	}
    };
    
    $scope.onTrainDestinationSelected = function(selected) {
  	  	if (selected) {
  	  		$scope.trainDestination = selected.originalObject;
  	  	}
    };
    
    $scope.openTrainPreferredTimeDiv = function(event, timeMode, journeyType) {
    	
    	if(timeMode != null) {
    		$(event.currentTarget).parents('.radio-box-section').hide().siblings('.calendar-content').show();
    		
    		if(journeyType == "R") {
    			$scope.trainReturnTimeMode = timeMode.timeModeId;
            	$scope.trainReturnTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.trainReturnTimeMode == "-1") {
            		$scope.trainReturnTimeDesc = "";
            		$scope.getElement("#trainReturnTimeDesc").val("");
        		}
            	
    		} else {
    			$scope.trainPreferredTimeMode = timeMode.timeModeId;
            	$scope.trainPreferredTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.trainPreferredTimeMode == "-1") {
            		$scope.trainPreferredTimeDesc = "";
            		$scope.getElement("#trainPreferredTimeDesc").val("");
        		}
    		}
    	
    	} else {
    		if(journeyType == "R") {
    			$scope.trainReturnTimeMode = "-1";
            	$scope.trainReturnTimeModeDesc = "No Preference";
            	$scope.trainReturnTimeDesc = "";
        		$scope.getElement("#trainReturnTimeDesc").val("");
        		
    		} else {
    			$scope.trainPreferredTimeMode = "-1";
            	$scope.trainPreferredTimeModeDesc = "No Preference";
            	$scope.trainPreferredTimeDesc = "";
        		$scope.getElement("#trainPreferredTimeDesc").val("");
    		}
    	}
    };
    
    $scope.onTrainTicketTypeChange = function () {
    	if($scope.trainTicketType == 1) {
    		$scope.trainTicketTypeDesc = "Tatkal";
    	} else {
    		$scope.trainTicketTypeDesc = "Normal";
    	}
    };
    
    $scope.onTrainPreferredClassChange = function () {
    	var keepGoing = true;
    	
  		angular.forEach($scope.trainClassList, function(trainClass){
	  		if(keepGoing && trainClass.classId == $scope.trainPreferredClass) {
	  			$scope.trainPreferredClassDesc = trainClass.eligibility;
	  			keepGoing = false;
	  		}
	  	});
    };
    
    $scope.onTrainPreferredSeatChange = function () {
    	var keepGoing = true;
    	
  		angular.forEach($scope.trainSeatList, function(trainSeat){
	  		if(keepGoing && trainSeat.seatId == $scope.trainPreferredSeat) {
	  			$scope.trainPreferredSeatDesc = trainSeat.seatName;
	  			keepGoing = false;
	  		}
	  	});
    };
    
    $scope.onTrainStayTypeChange = function() {
    	if($scope.trainStayType == "-1") {
	  		$scope.trainStayTypeDesc = "N/A";
	  		
	  	} else {
	  		var keepGoing = true;
	  		
	  		angular.forEach($scope.flightStayTypeList, function(stayType) {
		  		if(keepGoing && stayType.stayTypeId == $scope.trainStayType) {
		  			$scope.trainStayTypeDesc = stayType.stayTypeName;
		  			keepGoing = false;
		  		}
		  	});
	  	}
    };
    
    $scope.validateTrainJourney = function () {
    	
    	var departureCity 	= $("#source_value");
    	var arrivalCity 	= $("#destination_value");
    	var dateOfJourney 	= $("#trainDateOfJourney");
    	var prefTimeDesc	= $("#trainPreferredTimeDesc");
    	var prefTrain		= $("#preferredTrain");
    	
    	if($.trim(departureCity.val()) == "") {
    		$window.alert("Please enter departure city.");
    		departureCity.focus();
    		return false;
    		
    	} else if ($.trim(arrivalCity.val()) == "") {
    		$window.alert("Please enter arrival city.");
    		arrivalCity.focus();
    		return false;
    	
    	} else if ($.trim(dateOfJourney.val()) == "") {
    		$window.alert("Please enter journey date.");
    		dateOfJourney.focus();
    		return false;
    	
    	} else if($scope.toLowerCaseString($scope.trainPreferredTimeModeDesc) != 'no preference' && $.trim(prefTimeDesc.val()) == "") {
    		$window.alert("Please enter journey preferred time.");
    		$(".trainPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
    		prefTimeDesc.focus();
    		return false;
    	
    	} else if($.trim(prefTrain.val()) == "") {
    		$window.alert("Please enter preferred train.");
    		if(!$(".trainPreferencesDiv").is(":visible")) {
    			$("#trainPreferencesBtn").click();
    		}
    		prefTrain.focus();
    		return false;
    	}
    	
    	if($scope.trainJourneyDetails.journeyType == 'R') {
    		var returnDate		= $("#trainReturnDate");
        	var returnTimeDesc	= $("#trainReturnTimeDesc");
        	
    		if($.trim(returnDate.val()) == "") {
        		$window.alert("Please enter return journey date.");
        		returnDate.focus();
        		return false;
        		
        	} else if($scope.toLowerCaseString($scope.trainReturnTimeModeDesc) != 'no preference' && $.trim(returnTimeDesc.val()) == "") {
        		$window.alert("Please enter return journey preferred time.");
        		$(".trainPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
        		returnTimeDesc.focus();
        		return false;
        	}
    	}
    	
    	return true;
    };
    
    $scope.addTrainJourney = function() {
  	  
    	if($scope.validateTrainJourney()) {
	  	  	if($scope.trainJourneyDetails.journeyType == 'R') {
	  	  		$scope.trainReturnDate 		= $scope.getElement("#trainReturnDate").val();
	  	  		$scope.trainReturnTimeDesc	= $scope.getElement("#trainReturnTimeDesc").val();
	  	  		
	  	  		$scope.trainJourneyDetails.returnDate 			= $scope.trainReturnDate;
	  	  		$scope.trainJourneyDetails.returnTimeMode 		= $scope.trainReturnTimeMode;
	  	  		$scope.trainJourneyDetails.returnTimeModeDesc 	= $scope.trainReturnTimeModeDesc;
	  	  		$scope.trainJourneyDetails.returnTime 			= $scope.trainReturnTime;
	  	  		$scope.trainJourneyDetails.returnTimeDesc 		= $scope.trainReturnTimeDesc;
	  	  	}
	    	
	  	  	$scope.trainSourceCity 			= $scope.getElement("#source_value").val();
	  	  	$scope.trainDestinationCity 	= $scope.getElement("#destination_value").val();
	  	  	
	  	  	$scope.trainDateOfJourney 		= $scope.getElement("#trainDateOfJourney").val();
		  	$scope.trainPreferredTimeDesc 	= $scope.getElement("#trainPreferredTimeDesc").val();
		  	
		  	if($scope.toLowerCaseString($scope.trainPreferredTimeModeDesc) == 'no preference') {
		  		$scope.trainPreferredTimeDesc = "";
		  	}
		  	
	  	  	var tempTrainJourney = {
				journeyCode				: $scope.getMaxJourneyOrder("T"),
				source					: $scope.trainSourceCity,
				destination				: $scope.trainDestinationCity,
				dateOfJourney			: $scope.trainDateOfJourney,
				preferredTimeMode		: $scope.trainPreferredTimeMode,
				preferredTimeModeDesc	: $scope.trainPreferredTimeModeDesc,
				preferredTime			: $scope.trainPreferredTime,
				preferredTimeDesc		: $scope.trainPreferredTimeDesc,
				preferredTrain			: $scope.trainPreferred,
				ticketType				: $scope.trainTicketType,
	            ticketTypeDesc			: $scope.trainTicketTypeDesc,
				travelClass				: $scope.trainPreferredClass,
				travelClassDesc			: $scope.trainPreferredClassDesc,
				preferredSeat			: $scope.trainPreferredSeat,
				preferredSeatDesc		: $scope.trainPreferredSeatDesc,
				stayType				: $scope.trainStayType,
				stayTypeDesc			: $scope.trainStayTypeDesc
	  	  	};
	  	  	
	  	  	$scope.trainJourneyDetails.trainJourney.push(tempTrainJourney);
	  	  	//$scope.isAccommodationSelected = true;
	  	  	//console.log($scope.trainJourneyDetails);
	  	  	
	  	  	$scope.generateTrainItinerary(tempTrainJourney, false);
	  	  	$scope.generateTrainJourney(tempTrainJourney, false);
	  	  	
	  	  	$scope.setAccommodationRequired();
    	}
    };

    $scope.generateTrainItinerary = function (tempTrainJourney, editFlag) {
    	
    	if(tempTrainJourney.stayType != "-1") {
  	  		var tmp_itinerary = {
  	  	  		journeyMode			: "T",
  		  		journeyCode 		: tempTrainJourney.journeyCode,
  		  		source				: tempTrainJourney.source,
  		  		destination			: tempTrainJourney.destination,
  		  		journeyDesc			: tempTrainJourney.source + " -> " +tempTrainJourney.destination + " (Train)",
  		  		dateOfJourney		: tempTrainJourney.dateOfJourney,
  		  		preferredTimeDesc	: tempTrainJourney.preferredTimeDesc,
  		  		stayType			: tempTrainJourney.stayType,
	  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  		  	};
  		  	$scope.itineraries.push(tmp_itinerary);
  		  	
  		  	if($scope.trainJourneyDetails.journeyType == 'R') {
  		  		tmp_itinerary = {
  		  			journeyMode			: "T",
  		  	  		journeyCode 		: tempTrainJourney.journeyCode,
  		  	  		source				: tempTrainJourney.destination,
  		  	  		destination			: tempTrainJourney.source,
  		  	  		journeyDesc			: tempTrainJourney.destination + " -> " +tempTrainJourney.source + " (Train)",
  		  	  		dateOfJourney		: $scope.trainJourneyDetails.returnDate,
  		  	  		preferredTimeDesc	: $scope.trainJourneyDetails.returnTimeDesc,
  		  	  		stayType			: tempTrainJourney.stayType,
	  	  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  		  	  	};
  		  		
  		  		if(!editFlag) {
	  	  			tmp_itinerary.journeyCode 		= $scope.getMaxJourneyOrder("T");
	  	  			tmp_itinerary.dateOfJourney 	= $scope.trainReturnDate;
	  	  			tmp_itinerary.preferredTimeDesc = $scope.trainReturnTimeDesc;
	  	  		}
  		  	
  		  	  	$scope.itineraries.push(tmp_itinerary);
  		  	}
  		  	//console.log($scope.itineraries);
  	  	}
    };
    
    $scope.generateTrainJourney = function (tempTrainJourney, editFlag) {
    	var tmp_journey = {
  	  		journeyMode			: "T",
  	  		journeyCode 		: tempTrainJourney.journeyCode,
  	  		source				: tempTrainJourney.source,
  	  		destination			: tempTrainJourney.destination,
  	  		dateOfJourney		: tempTrainJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempTrainJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempTrainJourney.dateOfJourney, tempTrainJourney.preferredTimeDesc, true),
  	  		stayType			: tempTrainJourney.stayType,
  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if($scope.trainJourneyDetails.journeyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "T",
  	  	  		journeyCode 		: tempTrainJourney.journeyCode,
  	  	  		source				: tempTrainJourney.destination,
  	  	  		destination			: tempTrainJourney.source,
  	  	  		dateOfJourney		: $scope.trainJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.trainJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.trainJourneyDetails.returnDate, $scope.trainJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempTrainJourney.stayType,
  	  	  		stayTypeDesc		: tempTrainJourney.stayTypeDesc
  	  	  	};
    		
    		if(!editFlag) {
    			tmp_journey.journeyCode 		= $scope.getMaxJourneyOrder("T");
    			tmp_journey.dateOfJourney 		= $scope.trainReturnDate;
    			tmp_journey.preferredTimeDesc 	= $scope.trainReturnTimeDesc;
    			tmp_journey.dateTime 			= $scope.getDateTimeObject($scope.trainReturnDate, $scope.trainReturnTimeDesc, true);
  	  		}
    		
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log($scope.journeys);
    };
    
    $scope.editTrainJourney = function (index, trainJourney) {
  	  	//console.log(trainJourney);
		
		$scope.trainSourceCity 			= trainJourney.source;
  	    $scope.trainDestinationCity		= trainJourney.destination;
  	    $scope.trainDateOfJourney		= trainJourney.dateOfJourney;
  	    $scope.trainPreferredTimeMode	= trainJourney.preferredTimeMode;
  	    $scope.trainPreferredTimeModeDesc = trainJourney.preferredTimeModeDesc;
  	    $scope.trainPreferredTime		= trainJourney.preferredTime;
  	    $scope.trainPreferredTimeDesc 	= trainJourney.preferredTimeDesc;
  	    
  	    $scope.trainPreferred 			= trainJourney.preferredTrain;
  	    $scope.trainPreferredClass 		= trainJourney.travelClass.toString();
  	    $scope.trainPreferredClassDesc 	= trainJourney.travelClassDesc;
  	    $scope.trainPreferredSeat 		= trainJourney.preferredSeat.toString();
  	    $scope.trainPreferredSeatDesc 	= trainJourney.preferredSeatDesc;
  	    $scope.trainTicketType			= trainJourney.ticketType.toString();
  	    $scope.trainTicketTypeDesc		= trainJourney.ticketTypeDesc;
  	    $scope.trainStayType			= trainJourney.stayType.toString();
  	    $scope.trainStayTypeDesc		= trainJourney.stayTypeDesc;
  	    
  	    if($scope.toLowerCaseString(trainJourney.preferredTimeModeDesc) == 'no preference') {
	  		$scope.trainPreferredTimeDesc = "";
	  	}
  	  
  	    $scope.getElement("#source_value").val(trainJourney.source);
	  	$scope.getElement("#destination_value").val(trainJourney.destination);
	  	
	  	$('#trainDateOfJourney').datepicker('update', trainJourney.dateOfJourney);
	  	$scope.getElement("#trainPreferredTimeDesc").val(trainJourney.preferredTimeDesc);
	  	
  	    $scope.getAllElements("[name='trainPreferredTimeMode']").prop("checked", false);
	  	$scope.getElement("#trainPreferredTimeMode"+$scope.trainPreferredTimeMode).prop("checked", true);
	  	
	  	if(parseInt($scope.trainPreferredTimeMode) != -1) {
	  		$(".trainPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
	  	} else {
	  		$(".trainPreferredTimeModeDescDiv").show().siblings('.calendar-content').hide();
	  	}
	  	
	  	if($scope.trainJourneyDetails.journeyType == 'R') {
	  		$scope.trainReturnDate			= $scope.trainJourneyDetails.returnDate;
  	      	$scope.trainReturnTimeMode		= $scope.trainJourneyDetails.returnTimeMode;
  	      	$scope.trainReturnTimeModeDesc	= $scope.trainJourneyDetails.returnTimeModeDesc;
  	      	$scope.trainReturnTime			= $scope.trainJourneyDetails.returnTime;
  	      	$scope.trainReturnTimeDesc		= $scope.trainJourneyDetails.returnTimeDesc;
  	      	
  	      	$('#trainReturnDate').datepicker('update', $scope.trainReturnDate);
		  	$scope.getElement("#trainReturnTimeDesc").val($scope.trainReturnTimeDesc);
		  	
	  		$scope.getAllElements("[name='trainPreferredReturnTimeMode']").prop("checked", false);
	  		$scope.getElement("#trainPreferredReturnTimeMode"+$scope.trainReturnTimeMode).prop("checked", true);
	  		
	  		if(parseInt($scope.trainReturnTimeMode) != -1) {
		  		$(".trainPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
		  	} else {
		  		$(".trainPreferredReturnTimeModeDescDiv").show().siblings('.calendar-content').hide();
		  	}
	  	}
  	    
  	    $scope.deleteTrainJourney(index, trainJourney);
    };
    
    $scope.deleteTrainJourney = function (index, trainJourney) {
    	//console.log(trainJourney);
    	
    	if($scope.trainJourneyDetails.journeyType == 'R') {
    		$scope.clearItineraries("T");
    		$scope.clearJourneys("T");
    		$scope.clearAccommodations("T");
        	$scope.clearAccommodatedItineraries("T");
    		
    	} else {
    		$scope.deleteJourney(trainJourney.journeyCode);
    		
    		if(trainJourney.stayType != "-1") {
    			$scope.deleteItinerary(trainJourney.journeyCode);
    	    	$scope.deleteAccommodatedItinerary(trainJourney.journeyCode);
    	    	$scope.deleteAccommodationByJourneyCode(trainJourney.journeyCode);
    		}
    	}
    	
    	($scope.trainJourneyDetails.trainJourney).splice(index, 1);
    	
    	$scope.setAccommodationRequired();
    };
    
    /************************************************* CAR JOURNEY SECTION *************************************************/
    
    $scope.toggleCarBookingSection = function() {
    	var selectTabsFlag = true;
    	
    	if($scope.isCarSelected && $scope.carJourneyDetails.carJourney.length > 0) {
    		if($window.confirm("Do you want to delete all car journeys ?")) {
    			$scope.isCarSelected = !$scope.isCarSelected;
    			$scope.clearCarJourneyDetails();
    			
    		} else {
    			selectTabsFlag = false;
    			$scope.isCarSelected = true;
    			$("#carRequired").prop("checked", true);
    		}
    	
    	} else {
    		$scope.isCarSelected = !$scope.isCarSelected;
    	}
    	
    	if(selectTabsFlag) {
    		$scope.setJourneyDivs("C");
    	}
    	
    };
    
    /*$scope.carPickup 				= {};
    $scope.carDrop					= {};*/
    
    $scope.carClass					= "16";
    $scope.carClassDesc				= "Cars";
    $scope.carCategory				= "6";
    $scope.carCategoryDesc			= "ETIOS OR SIMILAR";
    
    $scope.carJourneyId				= "0";
    $scope.carPickupLocation 		= "";
    $scope.carDropLocation			= "";
    $scope.carDateOfJourney			= $scope.getTodayDateInddmmmyyyyFormat(0);
    $scope.carPreferredTimeMode		= "-1";
    $scope.carPreferredTimeModeDesc = "No Preference";
    $scope.carPreferredTime			= 0;
    $scope.carPreferredTimeDesc 	= $scope.getTimeIn12HoursFormat(new Date());
    $scope.carLocation				= "1";
    $scope.carLocationDesc			= "Transit House";
    $scope.carMobileNo				= "";
    
    $scope.carReturnDate 			= $scope.getTodayDateInddmmmyyyyFormat(1);
	$scope.carReturnTimeMode 		= -1;
	$scope.carReturnTimeModeDesc 	= "No Preference";
	$scope.carReturnTime 			= 0;
	$scope.carReturnTimeDesc 		= "";
	
    $scope.carJourneyDetails		= {
    	carClass				: 16,
    	carClassDesc			: "Cars",
    	carCategory				: "6",
    	carCategoryDesc			: "ETIOS OR SIMILAR",
		carJourney				: [],
        carJourneyRemarks 		: "",
    	journeyType				: "O",
        returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
        returnTimeMode			: "-1",
        returnTimeModeDesc		: "No Preference",
        returnTime				: 0,
        returnTimeDesc			: ""
    };

    $scope.setCarJourneyType = function (event) {
    	
    	var journeyType = $(event.currentTarget).attr('id');
    	
    	if(!$(event.currentTarget).hasClass("active-agent")) {
    		
    		if($scope.isCarSelected && $scope.carJourneyDetails.carJourney.length > 0) {
        		if($window.confirm("Are you sure you want to change journey type, this will delete all car journeys ?")) {
        			$scope.clearJourneys("C");
        	    	
        	    	$scope.carJourneyDetails.carJourney = [];
        	    	
        	    	$scope.getAllElements(".carJourneyType").removeClass("active-agent");
        	    	$(event.currentTarget).addClass("active-agent");

        	    	if(journeyType == 'carOneway') {
        	    		$scope.carJourneyDetails.journeyType = "O";
        	    		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
        	    		$scope.clearCarReturnJourneyDetails();
        		
        	    	} else if(journeyType == 'carReturn') {
        	    		$scope.carJourneyDetails.journeyType = "R";
        	    		$scope.getElement("div.carReturnJourneyDiv").removeClass("disabled-group");
        	  
        	    	} else if(journeyType == 'carMulticity') {
        	    		$scope.carJourneyDetails.journeyType = "M";
        	    		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
        	    		$scope.clearCarReturnJourneyDetails();
        	    	}
        	    	
        		} else {
        			$scope.getAllElements(".carJourneyType").removeClass("active-agent");
        	    			
        			if($scope.carJourneyDetails.journeyType == "O") {
        				$scope.getElement("#carOneway").addClass('active-agent');
        				
        			} else if ($scope.carJourneyDetails.journeyType == "R") {
        				$scope.getElement("#carReturn").addClass('active-agent');
        				
        			} else if ($scope.carJourneyDetails.journeyType == "M") {
        				$scope.getElement("#carMulticity").addClass('active-agent');
        			}
        		}
        				
        	} else {
        		$scope.clearJourneys("C");
            	
            	$scope.carJourneyDetails.carJourney = [];
            	
            	$scope.getAllElements(".carJourneyType").removeClass("active-agent");
            	$(event.currentTarget).addClass("active-agent");

            	if(journeyType == 'carOneway') {
            		$scope.carJourneyDetails.journeyType = "O";
            		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
            		$scope.clearCarReturnJourneyDetails();
        	
            	} else if(journeyType == 'carReturn') {
            		$scope.carJourneyDetails.journeyType = "R";
            		$scope.getElement("div.carReturnJourneyDiv").removeClass("disabled-group");
          
            	} else if(journeyType == 'carMulticity') {
            		$scope.carJourneyDetails.journeyType = "M";
            		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
            		$scope.clearCarReturnJourneyDetails();
            	}
        	}
    	}
    };
    
    $scope.setCarJourneyTypeOnEdit = function () {
    	
    	$scope.getAllElements(".carJourneyType").removeClass("active-agent");

    	if($scope.carJourneyDetails.journeyType == 'O') {
    		$("#carOneway").addClass('active-agent');
    		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearCarReturnJourneyDetails();

    	} else if($scope.carJourneyDetails.journeyType == 'R') {
    		$("#carReturn").addClass('active-agent');
    		$scope.getElement("div.carReturnJourneyDiv").removeClass("disabled-group");
  
    	} else if($scope.carJourneyDetails.journeyType == 'M') {
    		$("#carMulticity").addClass('active-agent');
    		$scope.getElement("div.carReturnJourneyDiv").addClass("disabled-group");
    		$scope.clearCarReturnJourneyDetails();
    	}
    };
    
    $scope.clearCarJourneyDetails = function () {
    	$scope.clearJourneys("C");
    	
    	$scope.carJourneyDetails		= {
	    	carClass				: 16,
	    	carClassDesc			: "Cars",
	    	carCategory				: "6",
	    	carCategoryDesc			: "ETIOS OR SIMILAR",
			carJourney				: [],
	        carJourneyRemarks 		: "",
	    	journeyType				: "O",
	        returnDate				: $scope.getTodayDateInddmmmyyyyFormat(1),
	        returnTimeMode			: "-1",
	        returnTimeModeDesc		: "No Preference",
	        returnTime				: 0,
	        returnTimeDesc			: ""
	    };
    	
    	$scope.getAllElements(".carJourneyType").removeClass("active-agent");
    	$scope.getElement("#carOneway").addClass('active-agent');
    };
    
    $scope.clearCarReturnJourneyDetails = function () {
    	$scope.carJourneyDetails.returnDate = $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.carJourneyDetails.returnTimeMode = -1;
    	$scope.carJourneyDetails.returnTimeModeDesc = "No Preference";
    	$scope.carJourneyDetails.returnTime = 0;
    	$scope.carJourneyDetails.returnTimeDesc = "";
    	
    	$scope.carReturnDate 			= $scope.getTodayDateInddmmmyyyyFormat(1);
    	$scope.carReturnTimeMode 		= -1;
    	$scope.carReturnTimeModeDesc	= "No Preference";
    	$scope.carReturnTime 			= 0;
    	$scope.carReturnTimeDesc 		= "";
    };
    
    $scope.onCarClassClick = function(carClass) {
    	$scope.carClass 	= carClass.classId;
    	$scope.carClassDesc = carClass.eligibility;
    	
    	$scope.getCarCategorylist(carClass.classId, false);
    };
    
    $scope.onCarCategoryChange = function() {
    	var keepGoing = true;
    	
    	angular.forEach($scope.carCategorylist, function(carCategory){
	  		if(keepGoing && carCategory.carCategoryId == $scope.carCategory) {
	  			$scope.carCategoryDesc = carCategory.carCategoryName;
	  			keepGoing = false;
	  		}
	  	});
    };
      
    /*$scope.onCarPickupLocationSelected = function(selected) {
    	if (selected) {
    		$scope.carPickup = selected.originalObject;
  	  	}
    };
    
    $scope.onCarDropLocationSelected = function(selected) {
  	  	if (selected) {
  	  		$scope.carDrop = selected.originalObject;
  	  	}
    };*/
    
    $scope.openCarPreferredTimeDiv = function(event, timeMode, journeyType) {
    	
    	if(timeMode != null) {
    		$(event.currentTarget).parents('.radio-box-section').hide().siblings('.calendar-content').show();
    		
    		if(journeyType == "R") {
    			$scope.carReturnTimeMode = timeMode.timeModeId;
            	$scope.carReturnTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.carReturnTimeMode == "-1") {
            		$scope.carReturnTimeDesc = "";
            		$scope.getElement("#carReturnTimeDesc").val("");
        		}
        		
    		} else {
    			$scope.carPreferredTimeMode = timeMode.timeModeId;
            	$scope.carPreferredTimeModeDesc = timeMode.timeModeName;
            	
            	if($scope.carPreferredTimeMode == "-1") {
            		$scope.carPreferredTimeDesc = "";
            		$scope.getElement("#carPreferredTimeDesc").val("");
        		}
    		}
    	
    	} else {
    		if(journeyType == "R") {
    			$scope.carReturnTimeMode = "-1";
            	$scope.carReturnTimeModeDesc = "No Preference";
            	$scope.carReturnTimeDesc = "";
        		$scope.getElement("#carReturnTimeDesc").val("");
        		
    		} else {
    			$scope.carPreferredTimeMode = "-1";
            	$scope.carPreferredTimeModeDesc = "No Preference";
            	$scope.carPreferredTimeDesc = "";
        		$scope.getElement("#carPreferredTimeDesc").val("");
    		}
    	}
    };
    
    $scope.onCarLocationChange = function () {
    	var keepGoing = true;
    	
    	angular.forEach($scope.carLocationList, function(location){
	  		if(keepGoing && location.carLocationId == $scope.carLocation) {
	  			$scope.carLocationDesc = location.carLocationName;
	  			keepGoing = false;
	  		}
	  	});
    };
    
    $scope.validateCarJourney = function () {
    	
    	var pickupLocation 	= $("#carPickupLocation");
    	var dropLocation 	= $("#carDropLocation");
    	var dateOfJourney 	= $("#carDateOfJourney");
    	var prefTimeDesc	= $("#carPreferredTimeDesc");
    	var mobileNo		= $("#carMobileNo");
    	
    	if($.trim(pickupLocation.val()) == "") {
    		$window.alert("Please enter pickup location.");
    		pickupLocation.focus();
    		return false;
    		
    	} else if ($.trim(dropLocation.val()) == "") {
    		$window.alert("Please enter drop location.");
    		dropLocation.focus();
    		return false;
    	
    	} else if ($.trim(dateOfJourney.val()) == "") {
    		$window.alert("Please enter journey date.");
    		dateOfJourney.focus();
    		return false;
    	
    	} else if($scope.toLowerCaseString($scope.carPreferredTimeModeDesc) != 'no preference' && $.trim(prefTimeDesc.val()) == "") {
    		$window.alert("Please enter journey preferred time.");
    		$(".carPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
    		prefTimeDesc.focus();
    		return false;
    	
    	} else if ($.trim(mobileNo.val()) == "") {
    		$window.alert("Please enter mobile no.");
    		mobileNo.focus();
    		return false;
    	}
    	
    	if($scope.carJourneyDetails.journeyType == 'R') {
    		var returnDate		= $("#carReturnDate");
        	var returnTimeDesc	= $("#carReturnTimeDesc");
        	
    		if($.trim(returnDate.val()) == "") {
        		$window.alert("Please enter return journey date.");
        		returnDate.focus();
        		return false;
        		
        	} else if($scope.toLowerCaseString($scope.carReturnTimeModeDesc) != 'no preference' && $.trim(returnTimeDesc.val()) == "") {
        		$window.alert("Please enter return journey preferred time.");
        		$(".carPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
        		returnTimeDesc.focus();
        		return false;
        	}
    	}
    	
    	return true;
    };
	
    $scope.addCarJourney = function() {
    	
    	if($scope.validateCarJourney()) {
	    	if($scope.carJourneyDetails.journeyType == 'R') {
	    		$scope.carReturnDate 		= $scope.getElement("#carReturnDate").val();
  	  			$scope.carReturnTimeDesc	= $scope.getElement("#carReturnTimeDesc").val();
  	  			
	  	      	$scope.carJourneyDetails.returnDate 		= $scope.carReturnDate;
	  	      	$scope.carJourneyDetails.returnTimeMode 	= $scope.carReturnTimeMode;
	  	      	$scope.carJourneyDetails.returnTimeModeDesc = $scope.carReturnTimeModeDesc;
	  	      	$scope.carJourneyDetails.returnTime 		= $scope.carReturnTime;
	  	      	$scope.carJourneyDetails.returnTimeDesc 	= $scope.carReturnTimeDesc;
	  	  	}
	    	
	    	$scope.carJourneyDetails.carClass			= $scope.carClass;
			$scope.carJourneyDetails.carClassDesc		= $scope.carClassDesc;
			$scope.carJourneyDetails.carCategory		= $scope.carCategory;
			$scope.carJourneyDetails.carCategoryDesc	= $scope.carCategoryDesc;
			
	  	  	$scope.carDateOfJourney 		= $scope.getElement("#carDateOfJourney").val();
		  	$scope.carPreferredTimeDesc 	= $scope.getElement("#carPreferredTimeDesc").val();
		  	
		  	if($scope.toLowerCaseString($scope.carPreferredTimeModeDesc) == 'no preference') {
		  		$scope.carPreferredTimeDesc = "";
		  	}
		  	
	  	  	var tempCarJourney = {
	  	  		carJourneyId			: $scope.carJourneyId,
				journeyCode				: $scope.getMaxJourneyOrder("C"),
				source					: $scope.carPickupLocation,
				destination				: $scope.carDropLocation,
				dateOfJourney			: $scope.carDateOfJourney,
				preferredTimeMode		: $scope.carPreferredTimeMode,
				preferredTimeModeDesc	: $scope.carPreferredTimeModeDesc,
				preferredTime			: $scope.carPreferredTime,
				preferredTimeDesc		: $scope.carPreferredTimeDesc,
				location				: $scope.carLocation,
				locationDesc			: $scope.carLocationDesc,
				mobileNo				: $scope.carMobileNo
	  	  	};
	  	  	
	  	  	$scope.carJourneyDetails.carJourney.push(tempCarJourney);
	  	  	//console.log($scope.carJourneyDetails);
	  	  	
	  	  	$scope.generateCarJourney(tempCarJourney, false);
    	}
    };
    
    $scope.generateCarJourney = function (tempCarJourney, editFlag) {
    	var tmp_journey = {
  	  		journeyMode			: "C",
  	  		journeyCode 		: tempCarJourney.journeyCode,
  	  		source				: tempCarJourney.source,
  	  		destination			: tempCarJourney.destination,
  	  		dateOfJourney		: tempCarJourney.dateOfJourney,
  	  		preferredTimeDesc	: tempCarJourney.preferredTimeDesc,
  	  		dateTime			: $scope.getDateTimeObject(tempCarJourney.dateOfJourney, tempCarJourney.preferredTimeDesc, true),
  	  		stayType			: tempCarJourney.location,
  	  		stayTypeDesc		: tempCarJourney.locationDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	if($scope.carJourneyDetails.journeyType == 'R') {
    		tmp_journey = {
  	  			journeyMode			: "C",
  	  	  		journeyCode 		: tempCarJourney.journeyCode,
  	  	  		source				: tempCarJourney.destination,
  	  	  		destination			: tempCarJourney.source,
  	  	  		dateOfJourney		: $scope.carJourneyDetails.returnDate,
  	  	  		preferredTimeDesc	: $scope.carJourneyDetails.returnTimeDesc,
  	  	  		dateTime			: $scope.getDateTimeObject($scope.carJourneyDetails.returnDate, $scope.carJourneyDetails.returnTimeDesc, true),
  	  	  		stayType			: tempCarJourney.location,
  	  	  		stayTypeDesc		: tempCarJourney.locationDesc
  	  	  	};
    		
    		if(!editFlag) {
    			tmp_journey.journeyCode 		= $scope.getMaxJourneyOrder("C");
    			tmp_journey.dateOfJourney 		= $scope.carReturnDate;
    			tmp_journey.preferredTimeDesc 	= $scope.carReturnTimeDesc;
  	  		}
    		
  	  	  	$scope.journeys.push(tmp_journey);
  	  	}
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log($scope.journeys);
    };
    
    $scope.editCarJourney = function (index, carJourney) {
  	  	//console.log(carJourney);
  	  	
  	  	$scope.carClass					= $scope.carJourneyDetails.carClass.toString();
		$scope.carClassDesc				= $scope.carJourneyDetails.carClassDesc;
		$scope.carCategory				= $scope.carJourneyDetails.carCategory.toString();
		$scope.carCategoryDesc			= $scope.carJourneyDetails.carCategoryDesc;
  	  	
  	  	$scope.carPickupLocation 		= carJourney.source;
  	  	$scope.carDropLocation			= carJourney.destination;
  	  	$scope.carDateOfJourney			= carJourney.dateOfJourney;
  	  	$scope.carPreferredTimeMode		= carJourney.preferredTimeMode;
      	$scope.carPreferredTimeModeDesc = carJourney.preferredTimeModeDesc;
      	$scope.carPreferredTime			= carJourney.preferredTime;
      	$scope.carPreferredTimeDesc 	= carJourney.preferredTimeDesc;
      	$scope.carLocation				= carJourney.location.toString();
      	$scope.carLocationDesc			= carJourney.locationDesc;
      	$scope.carMobileNo				= carJourney.mobileNo;
      	
      	if($scope.toLowerCaseString(carJourney.preferredTimeModeDesc) == 'no preference') {
	  		$scope.carPreferredTimeDesc = "";
	  	}
      	
      	$('#carDateOfJourney').datepicker('update', carJourney.dateOfJourney);
	  	$scope.getElement("#carPreferredTimeDesc").val(carJourney.preferredTimeDesc);
	  	
	  	$scope.getAllElements("[name='carPreferredTimeMode']").prop("checked", false);
	  	$scope.getElement("#carPreferredTimeMode"+$scope.carPreferredTimeMode).prop("checked", true);
	  	
	  	if(parseInt($scope.carPreferredTimeMode) != -1) {
	  		$(".carPreferredTimeModeDescDiv").hide().siblings('.calendar-content').show();
	  	} else {
	  		$(".carPreferredTimeModeDescDiv").show().siblings('.calendar-content').hide();
	  	}
	  	
	  	if($scope.carJourneyDetails.journeyType == 'R') {
	  		$scope.carReturnDate			= $scope.carJourneyDetails.returnDate;
  	      	$scope.carReturnTimeMode		= $scope.carJourneyDetails.returnTimeMode;
  	      	$scope.carReturnTimeModeDesc	= $scope.carJourneyDetails.returnTimeModeDesc;
  	      	$scope.carReturnTime			= $scope.carJourneyDetails.returnTime;
  	      	$scope.carReturnTimeDesc		= $scope.carJourneyDetails.returnTimeDesc;
  	      	
  	      	$('#carReturnDate').datepicker('update', $scope.carReturnDate);
		  	$scope.getElement("#carReturnTimeDesc").val($scope.carReturnTimeDesc);
		  	
	  		$scope.getAllElements("[name='carPreferredReturnTimeMode']").prop("checked", false);
	  		$scope.getElement("#carPreferredReturnTimeMode"+$scope.carReturnTimeMode).prop("checked", true);

	  		if(parseInt($scope.carReturnTimeMode) != -1) {
		  		$(".carPreferredReturnTimeModeDescDiv").hide().siblings('.calendar-content').show();
		  	} else {
		  		$(".carPreferredReturnTimeModeDescDiv").show().siblings('.calendar-content').hide();
		  	}
	  	}
      	
      	$scope.deleteCarJourney(index, carJourney);
    };
    
    $scope.deleteCarJourney = function (index, carJourney) {
    	//console.log(carJourney);
    	
    	if($scope.carJourneyDetails.journeyType == 'R') {
    		$scope.clearJourneys("C");
    	
    	} else {
    		$scope.deleteJourney(carJourney.journeyCode);
    		
    		if(carJourney.carJourneyId != 0) {
    			$scope.deleteJourneyDetailsFromDB(5, carJourney.carJourneyId);
    		}
    	}
    	
    	//console.log("Journeys after deleting car journey : ");
    	//console.log($scope.journeys);
    	
    	($scope.carJourneyDetails.carJourney).splice(index, 1);
    };
    
    /************************************************* ACCOMMODATION SECTION *********************************************/
    
    $scope.toggleAccommodationBookingSection = function(event) {
    	var selectTabsFlag = true;
    	
		if($scope.isAccommodationSelected && $scope.accommodationDetails.accommodations.length > 0) {
    		if($window.confirm("Do you want to delete all accommodations ?")) {
    			$scope.isAccommodationSelected = !$scope.isAccommodationSelected;
    			$scope.clearAllWithoutItineraryAccommodations();
    			
    		} else {
    			selectTabsFlag = false;
    			$scope.isAccommodationSelected = true;
    			$("#accommodationRequired").prop("checked", true);
    		}
    	
    	} else {
    		$scope.isAccommodationSelected = !$scope.isAccommodationSelected;
    	}
		
		if(selectTabsFlag) {
			$scope.setJourneyDivs("A");
		}
    	
    };
    
    $scope.accommodationJourneyCode		= "";
    $scope.accommodationStayType		= "2";
    $scope.accommodationStayTypeDesc	= "Transit House";
    $scope.accommodationCurrency 		= $scope.baseCurrency;
    $scope.accommodationBudget			= 0;
    $scope.accommodationCheckInDate		= $scope.getTodayDateInddmmmyyyyFormat(0);
    $scope.accommodationCheckOutDate	= $scope.getTodayDateInddmmmyyyyFormat(1);
    $scope.accommodationTransitHouse	= "0";
    $scope.accommodationPreferredPlace 	= "";
    $scope.accommodationAddress			= "";
    $scope.accommodationStayReason 		= "";
    $scope.transitHouseLatitude			= "";
    $scope.transitHouseLongitude		= "";
    
    $scope.accommodationDetails		= {
    	accommodations 			: [],
    	accommodationRemarks 	: ""
    };
	
    $scope.setAccommodations = function(stayType) {
    	
    	$scope.accommodationStayType			= stayType.stayTypeId;
        $scope.accommodationStayTypeDesc		= stayType.stayTypeName;
        $scope.accommodationCurrency 			= $scope.baseCurrency;
	  	$scope.accommodationBudget				= 0;
	  	$scope.accommodationTransitHouse		= "0";
	  	$scope.accommodationPreferredPlace 		= "";
	  	$scope.accommodationAddress				= "";
	  	$scope.accommodationStayReason 			= "";
	  	$scope.transitHouseLatitude				= "";
	  	$scope.transitHouseLongitude			= "";
        
	  	$scope.setAccommodationTypeDivs(stayType.stayTypeName);
    };
    
    $scope.onAccommodationJourneyChange = function () {
    	if($scope.accommodationJourneyCode == "" || $scope.accommodationJourneyCode == null || typeof($scope.accommodationJourneyCode) == 'undefined') {
    		$scope.getAllElements(".accommodationType").removeClass("disabled-element");
    		
    	} else {
    		$scope.getAllElements(".accommodationType").addClass("disabled-element");
    		
    		var stayType		= "2";
    		var stayTypeDesc 	= "transit house";
    		
    		angular.forEach($scope.itineraries, function(tmp_itinerary) {
        		if($scope.accommodationJourneyCode == tmp_itinerary.journeyCode) {
        			stayType		= tmp_itinerary.stayType;
        			stayTypeDesc 	= tmp_itinerary.stayTypeDesc;
        		}
        	});
    		
    		$scope.getElement("#accommodationType"+stayType).parent(".accommodationType").removeClass("disabled-element");
			$("input#accommodationType"+stayType).click();
			
    		$scope.accommodationStayType = stayType;
            $scope.accommodationStayTypeDesc = stayTypeDesc;
    		
    		$scope.setAccommodationTypeDivs(stayTypeDesc);
    	}
    };
    
    $scope.selectAccommodationTypeOnEdit = function () {
    	if($scope.accommodationJourneyCode == "" || $scope.accommodationJourneyCode == null || typeof($scope.accommodationJourneyCode) == 'undefined') {
    		$scope.getAllElements(".accommodationType").removeClass("disabled-element");
    	} else {
    		$scope.getAllElements(".accommodationType").addClass("disabled-element");
    	}
		
		$scope.getElement("#accommodationType"+$scope.accommodationStayType).parent(".accommodationType").removeClass("disabled-element");
		$("input#accommodationType"+$scope.accommodationStayType).click();
		
		$scope.setAccommodationTypeDivs($scope.accommodationStayTypeDesc);
    };
    
    $scope.setAccommodationTypeDivs = function (stayTypeName) {
    	var stayTypeDesc = $scope.toLowerCaseString(stayTypeName);
  	  	
    	$scope.accommodationCurrency 		= $scope.baseCurrency;
        $scope.accommodationBudget			= 0;
        $scope.accommodationTransitHouse	= "0";
        $scope.accommodationPreferredPlace 	= "";
        $scope.accommodationAddress			= "";
        $scope.accommodationStayReason 		= "";
        $scope.transitHouseLatitude			= "";
        $scope.transitHouseLongitude		= "";
        
    	if(stayTypeDesc == 'transit house') {
    		
    		$scope.isTransitHouseSelected = true;
  		  	$scope.isHotelSelected = false;
  		  	$scope.isOtherAccommodationSelected = false;
  		  
  		  	$scope.getElement("div.accommodationLocationDiv label").html("Transit House Address");
  		  
  		  	/*if(($(window).width() + 17) >= 1024) {
  		  		$scope.getElement("div.accommodationDetailsDiv").css({"padding-right":"330px"});
  		  	}*/
  		  	$scope.getElement("#accommodationAddress").attr("readonly", true);
  		  	
  	  	} else if(stayTypeDesc == 'other') {
  	  		
  	  		$scope.isTransitHouseSelected = false;
  	  		$scope.isHotelSelected = false;
  	  		$scope.isOtherAccommodationSelected = true;
  		  
  	  		$scope.getElement("div.preferredPlaceDiv label").html("Preferred Place");
  	  		$scope.getElement("div.accommodationLocationDiv label").html("Address");
  		  
  	  		$scope.getElement("#accommodationAddress").removeAttr("readonly");
  	  		$scope.getElement("div.accommodationDetailsDiv").css({"padding-right":"0px"});
  	  
  	  	} else if(stayTypeDesc == 'hotel') {
  	  		
  	  		$scope.isTransitHouseSelected = false;
  	  		$scope.isHotelSelected = true;
  	  		$scope.isOtherAccommodationSelected = false;
  		  
  	  		$scope.getElement("div.preferredPlaceDiv label").html("Preferred Hotel");
  	  		$scope.getElement("div.accommodationLocationDiv label").html("Hotel Address");
  		  
  	  		$scope.getElement("#accommodationAddress").removeAttr("readonly");
  	  		$scope.getElement("div.accommodationDetailsDiv").css({"padding-right":"0px"});
  	  	}
    };
    
    $scope.onTransitHouseChange = function () {
    	var keepGoing = true;
    	
    	angular.forEach($scope.transitHouseList, function(transitHouse) {
	  		if(keepGoing && transitHouse.transitHouseId == $scope.accommodationTransitHouse) {
	  			$scope.accommodationPreferredPlace 	= transitHouse.transitHouseName;
	  			$scope.accommodationAddress 		= transitHouse.transitHouseAddress;
	  		  	$scope.transitHouseLatitude			= transitHouse.latitude;
	  		  	$scope.transitHouseLongitude		= transitHouse.longitude;
	  		  	
	  		  	keepGoing = false;
	  		}
	  	});
    };
    
    $scope.clearWithoutItineraryAccommodationJourneys = function () {
    	var tmp_journeys = [];
    	
    	angular.forEach($scope.journeys, function(tmp_journey) {
    		
    		if(!(tmp_journey.journeyMode == 'A' && (tmp_journey.journeyCode == "" || tmp_journey.journeyCode == null))) {
    			tmp_journeys.push(tmp_journey);
	  		}
	  	});
    	
    	$scope.journeys = angular.copy(tmp_journeys);
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log("Journeys after clearing accommodation journeys without Itinerary : ");
    	//console.log($scope.journeys);
    };
    
    $scope.clearWithoutItineraryAccommodations = function () {
    	
    	var tmp_accommodations = [];
		var ids = "";
		
		angular.forEach($scope.accommodationDetails.accommodations, function(tmp_accommodation) {
			if(tmp_accommodation.journeyCode == "" || tmp_accommodation.journeyCode == null) {
				if(tmp_accommodation.accommodationId != 0) {
					ids += tmp_accommodation.accommodationId + ",";
				}
				
			} else {
				tmp_accommodations.push(tmp_accommodation);
			}
		});
		
		if(ids.length > 1) {
			$scope.deleteJourneyDetailsFromDB(7, ids.substring(0, ids.length-1));
    	}
		
		$scope.accommodationDetails.accommodations = [];
		$scope.accommodationDetails.accommodations = angular.copy(tmp_accommodations);
		
		//console.log("Accommodations after clearing accommodations without Itinerary : ");
    	//console.log($scope.accommodationDetails.accommodations);
    };
    
    $scope.clearAllWithoutItineraryAccommodations = function () {
    	$scope.clearWithoutItineraryAccommodationJourneys();
    	$scope.clearWithoutItineraryAccommodations();
    };
    
    $scope.validateAccommodation = function () {
    	
    	var budgetPerDay 	= $("#accommodationBudget");
    	var checkInDate 	= $("#accommodationCheckInDate");
    	var checkOutDate 	= $("#accommodationCheckOutDate");
    	var transitHouse	= $("#accommodationTransitHouse");
    	var preferredPlace 	= $("#accommodationPreferredPlace");
    	var address 		= $("#accommodationAddress");
    	var stayReason 		= $("#accommodationStayReason");
    	
    	if ($scope.toLowerCaseString($scope.accommodationStayTypeDesc) == 'transit house') {
    		
    		if ($.trim(checkInDate.val()) == "") {
        		$window.alert("Please enter check in date.");
        		checkInDate.focus();
        		return false;
        	
        	} else if($.trim(checkOutDate.val()) == "") {
        		$window.alert("Please enter check out date.");
        		checkOutDate.focus();
        		return false;
        	
        	} else if($.trim(transitHouse.find(" option:selected").val()) == "0") {
        		$window.alert("Please select transit house.");
        		transitHouse.focus();
        		return false;
        	}
    		
    	} else if ($scope.toLowerCaseString($scope.accommodationStayTypeDesc) == 'hotel') {
    		
    		if ($.trim(budgetPerDay.val()) == "" || $.trim(budgetPerDay.val()) == "0") {
        		$window.alert("Please enter budget/day.");
        		budgetPerDay.focus();
        		return false;
        	
        	} else if ($.trim(checkInDate.val()) == "") {
        		$window.alert("Please enter check in date.");
        		checkInDate.focus();
        		return false;
        	
        	} else if($.trim(checkOutDate.val()) == "") {
        		$window.alert("Please enter check out date.");
        		checkOutDate.focus();
        		return false;
        	
        	} else if ($.trim(preferredPlace.val()) == "") {
        		$window.alert("Please enter preferred hotel name.");
        		preferredPlace.focus();
        		return false;
        	
        	} else if ($.trim(address.val()) == "") {
        		$window.alert("Please enter hotel address.");
        		address.focus();
        		return false;
        	}
    		
    	} else if ($scope.toLowerCaseString($scope.accommodationStayTypeDesc) == 'other') {
    		
    		if ($.trim(checkInDate.val()) == "") {
        		$window.alert("Please enter check in date.");
        		checkInDate.focus();
        		return false;
        	
        	} else if($.trim(checkOutDate.val()) == "") {
        		$window.alert("Please enter check out date.");
        		checkOutDate.focus();
        		return false;
        	
        	} else if ($.trim(preferredPlace.val()) == "") {
        		$window.alert("Please enter preferred place.");
        		preferredPlace.focus();
        		return false;
        	
        	} else if ($.trim(address.val()) == "") {
        		$window.alert("Please enter address.");
        		address.focus();
        		return false;
        	
        	} else if ($.trim(stayReason.val()) == "") {
        		$window.alert("Please enter stay reason.");
        		stayReason.focus();
        		return false;
        	}
    	}
    	
    	return true;
    };
    
    /*************************************** Add Accommodated Itinerary By Journey Code ***************************************/
    
	$scope.addAccommodatedItinerary = function (journeyCode) {
		if($.trim(journeyCode) != "") {
			var keepGoing = true;
			
			angular.forEach($scope.itineraries, function(tmp_itinerary, $index){
				if(keepGoing && tmp_itinerary.journeyCode == journeyCode) {
		  			($scope.accommodatedItineraries).push(tmp_itinerary);
		  			($scope.itineraries).splice($index, 1);
		  			
		  			keepGoing = false;
		  		}
		  	});
			
			//console.log("Itineraries after generating accommodation : "+journeyCode);
			//console.log($scope.itineraries);
			
			//console.log("Accommodated itineraries after generating accommodation : "+journeyCode);
			//console.log($scope.accommodatedItineraries);
		}
	};
	
	/*************************************** Delete Travel Journey (i.e. Flight/Train/Car/Accommodation) By Journey Order ***************/
	
	$scope.deleteJourneyByJourneyOrder = function (journeyOrder) {
		var tmp_journeys = [];
		
		angular.forEach($scope.journeys, function(tmp_journey){
	  		if(tmp_journey.journeyOrder != journeyOrder) {
	  			tmp_journeys.push(tmp_journey);
	  		}
	  	});
    	
		$scope.journeys = angular.copy(tmp_journeys);
		
    	$scope.sortJourneysByDateTime();
    	
    	//console.log("Journeys after deleting journey by journeyOrder : "+journeyOrder);
    	//console.log($scope.journeys);
	};
	
	/*************************************** Move Accommodated Itinerary To Itineraries Array ***************************************/
	
	$scope.moveAccommodatedItinerary = function (journeyCode) {
		if($.trim(journeyCode) != "") {
			var keepGoing = true;
			
			angular.forEach($scope.accommodatedItineraries, function(tmp_accommodatedItinerary, $index){
		  		if(keepGoing && tmp_accommodatedItinerary.journeyCode == journeyCode) {
		  			($scope.itineraries).push(tmp_accommodatedItinerary);
		  			($scope.accommodatedItineraries).splice($index, 1);
		  			
		  			keepGoing = false;
		  		}
		  	});
			
			//console.log("Itineraries after deleting accommodation : "+journeyCode);
			//console.log($scope.itineraries);
			
			//console.log("Accommodated itineraries after deleting accommodation : "+journeyCode);
			//console.log($scope.accommodatedItineraries);
		}
	};
	
    $scope.addAccommodation = function() {
    	
    	if($scope.validateAccommodation()) {
	    	$scope.accommodationCheckInDate 	= $scope.getElement("#accommodationCheckInDate").val();
		  	$scope.accommodationCheckOutDate 	= $scope.getElement("#accommodationCheckOutDate").val();
		    
	  	  	var tempAccommodation = {
	  	  		accommodationId 		: 0,
	            journeyCode 			: $scope.accommodationJourneyCode,
	            journeyOrder			: $scope.getMaxJourneyOrder("A"),
	            stayType 				: $scope.accommodationStayType,
	            stayTypeDesc 			: $scope.accommodationStayTypeDesc,
	            currency 				: $scope.accommodationCurrency,
	            budget 					: $scope.accommodationBudget,
	            checkInDate 			: $scope.accommodationCheckInDate,
	            checkOutDate 			: $scope.accommodationCheckOutDate,
	            place 					: $scope.accommodationPreferredPlace,
	            transitHouseId 			: $scope.accommodationTransitHouse,
	            transitHouseLatitude	: $scope.transitHouseLatitude,
	            transitHouseLongitude	: $scope.transitHouseLongitude,
	            address 				: $scope.accommodationAddress,
	            reason 					: $scope.accommodationStayReason
	  	  	};
	  	  	
	  	  	$scope.accommodationDetails.accommodations.push(tempAccommodation);
	  	  	//console.log($scope.accommodationDetails);
	  	  	
	  	  	$scope.generateAccommodation(tempAccommodation);
	  	  	$scope.addAccommodatedItinerary(tempAccommodation.journeyCode);
	  	  	
	  	  	$scope.accommodationJourneyCode = "";
	  	  	$scope.onAccommodationJourneyChange();
    	}
    };
    
    $scope.generateAccommodation = function (tempAccommodation) {
    	var tmp_journey = {
  	  		journeyMode			: "A",
  	  		journeyCode 		: tempAccommodation.journeyCode,
  	  		source				: "",
  	  		destination			: tempAccommodation.place,
  	  		dateOfJourney		: tempAccommodation.checkInDate,
  	  		preferredTimeDesc	: "",
  	  		dateTime			: $scope.getDateTimeObject(tempAccommodation.checkInDate, "", false),
  	  		journeyOrder		: tempAccommodation.journeyOrder,
  	  		checkOutDate		: tempAccommodation.checkOutDate,
  	  		stayType			: tempAccommodation.stayType,
  	  		stayTypeDesc		: tempAccommodation.stayTypeDesc
  	  	};
    	$scope.journeys.push(tmp_journey);
    	
    	$scope.sortJourneysByDateTime();
    	
    	//console.log("Journeys after generating accommodation : ");
    	//console.log($scope.journeys);
    };
    
    $scope.editAccommodation = function (index, accommodation) {
  	  	//console.log(accommodation);
  	  	
  	  	$scope.accommodationJourneyCode		= accommodation.journeyCode;
  	  	$scope.accommodationStayType		= accommodation.stayType;
  	  	$scope.accommodationStayTypeDesc	= accommodation.stayTypeDesc;
  	  	$scope.accommodationCurrency 		= accommodation.currency;
  	  	$scope.accommodationBudget			= accommodation.budget;
  	  	$scope.accommodationCheckInDate		= accommodation.checkInDate;
  	  	$scope.accommodationCheckOutDate	= accommodation.checkOutDate;
  	  	$scope.accommodationTransitHouse	= accommodation.transitHouseId;
  	  	$scope.accommodationPreferredPlace 	= accommodation.place;
  	  	$scope.accommodationAddress			= accommodation.address;
  	  	$scope.accommodationStayReason 		= accommodation.reason;
  	  	$scope.transitHouseLatitude			= accommodation.transitHouseLatitude;
  	  	$scope.transitHouseLongitude		= accommodation.transitHouseLongitude;
  	  	
  	  	$('#accommodationCheckInDate').datepicker('update', accommodation.checkInDate);
  	  	$('#accommodationCheckOutDate').datepicker('update', accommodation.checkOutDate);
  	  	
  	  	$scope.deleteAccommodation(index, accommodation);
  	  	$scope.selectAccommodationTypeOnEdit();
    };
    
    $scope.deleteAccommodation = function (index, accommodation) {
    	//console.log(accommodation);
    	
    	if(accommodation.journeyCode == "" || accommodation.journeyCode == null) {
    		$scope.deleteJourneyByJourneyOrder(accommodation.journeyOrder);
    	} else {
    		$scope.deleteJourney(accommodation.journeyCode);
    	}
    	
    	//console.log("Journeys after deleting accommodation : ");
    	//console.log($scope.journeys);
    	
    	if(accommodation.accommodationId != 0) {
			$scope.deleteJourneyDetailsFromDB(7, accommodation.accommodationId);
		}
    	
    	($scope.accommodationDetails.accommodations).splice(index, 1);
    	
    	$scope.moveAccommodatedItinerary(accommodation.journeyCode);
    };
    
    /************************************************* ADVANCE/FOREX SECTION *************************************************/
    
    $scope.advanceForexDetails		= {
    	advanceAmountDetails 		: [
    		{
    			currencyCode 			: $scope.baseCurrency,
    			dailyAllowanceExpPerDay : 0,
    			dailyAllowanceTourDays 	: 0,
    			dailyAllowanceExpTotal 	: 0,
    			hotelChargesExpPerDay 	: 0,
    			hotelChargesTourDays 	: 0,
    			hotelChargesExpTotal 	: 0,
    			contingencies 			: 0,
    			others 					: 0,
    			total 					: 0,
    			exchangeRateINR 		: $scope.baseCurrencyExchangeRate,
    			totalINR 				: 0
            }
    	],
    	totalAmountINR 				: 0.00,
    	totalUSDPerDay 				: 0.00,
    	advanceRemarks 				: "",
    	currencyDominationDetails 	: ""
    };
    
    $scope.toggleAdvanceRequiredSection = function() {
    	if($scope.isAdvanceRequired && parseInt($scope.advanceForexDetails.totalAmountINR) > 0) {
    		if($window.confirm("Do you want to delete all advance information ?")) {
    			$scope.isAdvanceRequired = !$scope.isAdvanceRequired;
    			$scope.clearAllAdvanceForexDetails();
    			
    		} else {
    			$scope.isAdvanceRequired = true;
    			$("#advanceRequired").prop("checked", true);
    		}
    	
    	} else {
    		$scope.isAdvanceRequired = !$scope.isAdvanceRequired;
    	}
    };
    
    $scope.clearAllAdvanceForexDetails = function() {
    	 $scope.advanceForexDetails		= {
	    	advanceAmountDetails 		: [
	    		{
	    			currencyCode 			: $scope.baseCurrency,
	    			dailyAllowanceExpPerDay : 0,
	    			dailyAllowanceTourDays 	: 0,
	    			dailyAllowanceExpTotal 	: 0,
	    			hotelChargesExpPerDay 	: 0,
	    			hotelChargesTourDays 	: 0,
	    			hotelChargesExpTotal 	: 0,
	    			contingencies 			: 0,
	    			others 					: 0,
	    			total 					: 0,
	    			exchangeRateINR 		: $scope.baseCurrencyExchangeRate,
	    			totalINR 				: 0
	            }
	    	],
	    	totalAmountINR 				: 0.00,
	    	totalUSDPerDay 				: 0.00,
	    	advanceRemarks 				: "",
	    	currencyDominationDetails 	: ""
	    };
    };
    
    $scope.getBaseCurrencyExchangeRate = function() {
    	var url = $scope.apiBaseUrl+"/api/request/exchange/rate/list";
    	  
	  	var headerData = {
	  		winUserId			: $scope.winUserId,
	  		domainName 			: $scope.domainName,
	  		deviceId 			: $scope.ipAddress,
	  		requestSource 		: $scope.requestSource,
	  		systemName 			: $scope.systemName,
	  		selectedCurrency	: $scope.baseCurrency
	  	};
	
	  	apiCallService.getRequest(url, headerData).then(function(response) {
	  		if(response != null && response.data.status == "200") {
	  			$scope.baseCurrencyExchangeRate = response.data.data.exchangeRate;
	  		}
	  	}); 
    };
    
    $scope.getCurrencyExchangeRate = function(advanceForex) {
    	var url = $scope.apiBaseUrl+"/api/request/exchange/rate/list";
    	  
	  	var headerData = {
	  		winUserId			: $scope.winUserId,
	  		domainName 			: $scope.domainName,
	  		deviceId 			: $scope.ipAddress,
	  		requestSource 		: $scope.requestSource,
	  		systemName 			: $scope.systemName,
	  		selectedCurrency	: advanceForex.currencyCode
	  	};
	
	  	apiCallService.getRequest(url, headerData).then(function(response) {
	  		if(response != null && response.data.status == "200") {
	  			var exchangeRate = response.data.data.exchangeRate;
	  			//console.log(exchangeRate);
	  			
	  			if(parseFloat(exchangeRate) == 0) {
	  				$window.alert("Exchange rate for "+advanceForex.currencyCode+" is not defined, please contact STARS administrator");
	  			} else {
	  				advanceForex.exchangeRateINR = exchangeRate;
	  			}
	  		}
	  	}); 
    };
    
    $scope.onAdvanceForexCurrencyChange = function(index, advanceForex) {
    	
    	if(advanceForex.currencyCode == "") {
    		advanceForex.exchangeRateINR = 0;
    		advanceForex.totalINR = 0;
    		$scope.calculateTotalAdvanceForexAmountINR();
    		
    		if(index > 0) {
    			$scope.getForexTotal(index-1, $scope.advanceForexDetails.advanceAmountDetails[index-1]);
    		}
    		
    		$window.alert("Please select currency.");
    	
    	} else {
    		var currencyExistsFlag = false;
    		
    		angular.forEach($scope.advanceForexDetails.advanceAmountDetails, function(tmp_advanceForex, $index) {
        		if(!currencyExistsFlag && index != $index && advanceForex.currencyCode == tmp_advanceForex.currencyCode) {
        			currencyExistsFlag = true;
        		}
    		});
    		
    		if(currencyExistsFlag) {
    			advanceForex.currencyCode = "";
    			advanceForex.exchangeRateINR = 0;
    			advanceForex.totalINR = 0;
    			$scope.calculateTotalAdvanceForexAmountINR();
    			
        		if(index > 0) {
        			$scope.getForexTotal(index-1, $scope.advanceForexDetails.advanceAmountDetails[index-1]);
        		}
    			
    			$window.alert("Please select currency other than already selected.");
    			
    		} else {
    			$scope.getCurrencyExchangeRate(advanceForex);
    	    	$scope.getForexTotal(index, advanceForex);
    		}
    	}
    };
    
    /****************** Calculate Total Advance/Forex Amount In INR ********************/
    
    $scope.calculateTotalAdvanceForexAmountINR = function () {
    	var totalAdvanceForexAmountINR = 0.0;
    	
    	angular.forEach($scope.advanceForexDetails.advanceAmountDetails, function(tmp_advanceForex) {
    		totalAdvanceForexAmountINR += parseFloat(tmp_advanceForex.totalINR);
		});
    	
    	$scope.advanceForexDetails.totalAmountINR = totalAdvanceForexAmountINR.toFixed(2);
    };
    
    /****************** Calculate Selected Currency Total Amount ***********************/
    
    $scope.calculateCurrencyTotalAmount = function(advanceForex) {
    	advanceForex.total = parseInt(advanceForex.dailyAllowanceExpPerDay) * parseInt(advanceForex.dailyAllowanceTourDays) +
					    	 parseInt(advanceForex.hotelChargesExpPerDay) * parseInt(advanceForex.hotelChargesTourDays) +
					    	 parseInt(advanceForex.contingencies) +
					    	 parseInt(advanceForex.others);
    	
    	$scope.calculateTotalAdvanceForexAmountINR();
    };
    
    /****************** Calculate Total Journey Days ************************************/
    
    $scope.getJourneyDays = function () {
		if($scope.journeys.length > 1) {
			var date1 = $scope.journeys[0].dateTime;
			var date2 = $scope.journeys[$scope.journeys.length - 1].dateTime;
			
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

			return diffDays;
		}
		return 0;
    };
    
    /****************** Calculate Total (Journey + Advance Section) Days ****************/
    
    $scope.getTotalTourDays = function () {
    	var totalTourDays = $scope.getJourneyDays();
    	
    	if(totalTourDays <= 0) {
    		totalTourDays = 0;
    		
    		angular.forEach($scope.advanceForexDetails.advanceAmountDetails, function(tmp_advanceForex, $index) {
    			if(parseInt(tmp_advanceForex.hotelChargesTourDays) > 0) {
    				totalTourDays += parseInt(tmp_advanceForex.hotelChargesTourDays);
    			} else if(parseInt(tmp_advanceForex.dailyAllowanceTourDays) > 0) {
            		totalTourDays += parseInt(tmp_advanceForex.dailyAllowanceTourDays);
            	}
    		});
    	}
    	
    	if(totalTourDays == 0) {
    		totalTourDays += 1;
    	}
    	//console.log("TotalTourDays : "+totalTourDays);
    	
    	return totalTourDays;
    };
    
    $scope.calculatePreviousTotal = function(index, advanceForex) {
    	var previousTotal = 0;
    	
    	if($scope.advanceForexDetails.advanceAmountDetails.length > 1) {
    		angular.forEach($scope.advanceForexDetails.advanceAmountDetails, function(tmp_advanceForex, $index) {
    			if($index != index) {
    				previousTotal += parseFloat(tmp_advanceForex.totalINR);
    			}
    		});
    	}
    	//console.log("previousTotal : "+previousTotal);
    	
    	return previousTotal;
    };
    
    /****************** Calculate Selected Currency Total Amount In INR & USD/Day ***********************/
    
    $scope.getForexTotal = function(index, advanceForex) {
    	
    	var url = $scope.apiBaseUrl+"/api/request/forex/total";
  	  
	  	var headerData = {
	  		winUserId			: $scope.winUserId,
	  		domainName 			: $scope.domainName,
	  		deviceId 			: $scope.ipAddress,
	  		requestSource 		: $scope.requestSource,
	  		systemName 			: $scope.systemName,
	  		selectedCurrency	: advanceForex.currencyCode,
	  		days				: $scope.getTotalTourDays(),
	  		baseCurrency		: $scope.baseCurrency,
	  		selectedTotal		: advanceForex.total,
	  		previousTotal		: $scope.calculatePreviousTotal(index, advanceForex),
	  		currentDate			: ""
	  	};
	
	  	apiCallService.getRequest(url, headerData).then(function(response) {
	  		if(response != null && response.data.status == "200") {
	  			//console.log(response.data.data);
	  			
	  			var totalAmountINR  = response.data.data.totalAmountINR;
	  			var usdPerDay		= response.data.data.totalAmountUSD;
	  			
	  			if($.trim(totalAmountINR) != "") {
	  				advanceForex.totalINR = parseFloat(totalAmountINR);
	  			} else {
	  				advanceForex.totalINR = 0;
	  			}
	  			
	  			if($.trim(usdPerDay) != "") {
	  				$scope.advanceForexDetails.totalUSDPerDay = parseFloat(usdPerDay);
	  			} else {
	  				$scope.advanceForexDetails.totalUSDPerDay = 0;
	  			}
	  			
	  			if($scope.advanceForexDetails.totalUSDPerDay > 350) {
	  				$scope.getElement(".usd-per-day").css({"color":"red"});
	  				$window.alert("Total Per Day expense should not exceed $350 or in equivalent currencies.");
	  				
	  			} else {
	  				$scope.getElement(".usd-per-day").css({"color":"#414042"});
	  			}
	  			
	  			$scope.calculateTotalAdvanceForexAmountINR();
	  		}
	  	}); 
    };
    
    /****************** Calculate Selected Currency Total Amount In INR & USD/Day From UI ***********************/
    
    $scope.calculateCurrencyTotalINR = function(index, advanceForex) {
    	if(advanceForex.currencyCode != "") {
    		$scope.getForexTotal(index, advanceForex);
    	}
    };
    
    $scope.addAdvanceForex = function() {
    	var temp_advanceAmountDetail = {
			currencyCode 			: "",
			dailyAllowanceExpPerDay : 0,
			dailyAllowanceTourDays 	: 0,
			dailyAllowanceExpTotal 	: 0,
			hotelChargesExpPerDay 	: 0,
			hotelChargesTourDays 	: 0,
			hotelChargesExpTotal 	: 0,
			contingencies 			: 0,
			others 					: 0,
			total 					: 0,
			exchangeRateINR 		: 0,
			totalINR 				: 0
        };
    	
    	$scope.advanceForexDetails.advanceAmountDetails.push(temp_advanceAmountDetail);
    	
    	//console.log($scope.advanceForexDetails.advanceAmountDetails);
    	
    	//$scope.calculateTotalAdvanceForexAmountINR();
    };
    
    $scope.deleteAdvanceCurrency = function (index, advanceForex) {
    	($scope.advanceForexDetails.advanceAmountDetails).splice(index, 1);
    	
    	$scope.calculateTotalAdvanceForexAmountINR();
    	$scope.getForexTotal(index-1, $scope.advanceForexDetails.advanceAmountDetails[index-1]);
    };
    
    /************************************************* BUDGET DETAILS SECTION *************************************************/
    
    $scope.calculateGrandTotalAmount = function () {
    	// calculate Total INR amount & USD/day
    };
    
    $scope.calculateAvailableBudget = function () {
    	if(isNaN(parseFloat($scope.ytmBudget) - parseFloat($scope.ytdActual))) {
    		$scope.availableBudget = 0.0;
    	} else {
    		$scope.availableBudget = parseFloat($scope.ytmBudget) - parseFloat($scope.ytdActual);
    	}
    };
    
    /************************************************* TRAVEL SUMMARY SECTION *************************************************/
    
    $scope.ytmBudget				= 0;
	$scope.ytdActual				= 0;
	$scope.availableBudget			= 0;
	$scope.estimate					= 0;
	$scope.expRemarks				= "";
	
	$scope.totalTravelFareCurrency	= "INR";
  	$scope.totalTravelFareAmount	= 0;
  	
  	$scope.localAgentFlag			= "N";
  	$scope.nonMATASourceFlag		= false;
	$scope.nonMATASourceAirlineName	= "";
	$scope.nonMATASourceCurrency	= "INR";
	$scope.nonMATASourceAmount		= 0;
	$scope.nonMATASourceRemarks		= "";
	
	$scope.reasonForTravel			= "";
	$scope.reasonForSkip			= "";
	
	$scope.selectedMealPreference	= "0";
	$scope.selectedCostCentre		= "0";
	
	$scope.selectedSiteLocation 	= "-1";
	$scope.selectedSiteLocationGSTNo= "";
	
	$scope.selectedBillingSiteId	= $scope.selectedSiteId;
	$scope.multiSiteAccessFlag		= "1";
	$scope.selectedBillingApprover	= "-1";
	
	$scope.selectedApproverLevel1	= "-1";
	$scope.selectedApproverLevel2	= "-1";
	$scope.selectedApproverLevel3	= "-1";
	
	$scope.approverLevel1Name		= "";
	$scope.approverLevel2Name		= "";
	
	$scope.setReasonForSkip = function () {
		var approverLevel1Val = $("#approverLevel1 option:selected").val();
		var approverLevel2Val = $("#approverLevel2 option:selected").val();
		
		if((approverLevel1Val != "-1" && approverLevel1Val != '') && (approverLevel2Val != "-1" && approverLevel2Val != '')) {		
			$("textarea#reasonForSkip").prop('disabled', true);
			$scope.reasonForSkip = "";
		
		} else if((approverLevel1Val == "-1" || approverLevel1Val == '') && (approverLevel2Val == "-1" || approverLevel2Val == '')) {
			$("textarea#reasonForSkip").prop('disabled', false);
		
		} else if((approverLevel1Val != "-1" || approverLevel1Val != '') && (approverLevel2Val == "-1" || approverLevel2Val == '')) 	{
			$("textarea#reasonForSkip").prop('disabled', false);
		
		} else if((approverLevel1Val == "-1" || approverLevel1Val == '') && (approverLevel2Val != "-1" || approverLevel2Val != '')) 	{
			$("textarea#reasonForSkip").prop('disabled', false);
		}
	};
    
    /************************************************* GET TEMP REQUEST DATA *************************************************/
    
    $scope.getRequestData = function() {
  	  
    	if($.trim($scope.travelId) != "0") {
    		var url = $scope.apiBaseUrl+"/api/request/edit/"+$scope.travelType+"/"+$scope.travelId;
  	  
  	  		var headerData = {
  	  		  winUserId			: $scope.winUserId,
			  domainName 		: $scope.domainName,
			  deviceId 			: $scope.ipAddress,
			  requestSource 	: $scope.requestSource,
			  systemName 		: $scope.systemName
  	  		};
		
  	  		apiCallService.getRequest(url, headerData).then(function(response) {
			  
			  if(response != null && response.data.status == "200") {
				  var requestData = response.data.data;
				  
				  if(typeof requestData != "undefined" && requestData != null) {
					  
					  /************************************************* SET REQUEST DATA ************************************************************/
					  
					  $scope.requestId 					= requestData.requestId;
					  $scope.travelId 					= requestData.travelId;
					  $scope.travelRequestNo 			= requestData.travelRequestNo;
					  $scope.travelType 				= requestData.travelType;
					  $scope.selectedSiteId 			= requestData.siteId;
					  $scope.groupTravelFlag			= requestData.groupTravelFlag;
					  $scope.linkedTravelRequestId 		= requestData.linkedTravelRequestId.toString();
					  
					  $scope.reasonForTravel 			= requestData.reasonForTravel;
					  $scope.selectedMealPreference		= requestData.preferredMealId.toString();
					  $scope.selectedCostCentre			= requestData.costCentreId.toString();
					  $scope.selectedSiteLocation		= requestData.siteLocation.toString();
					  $scope.selectedSiteLocationGSTNo	= requestData.gstNo;
					  
					  var billingClient					= requestData.billingClient.toString();
					  if((typeof(billingClient) != 'undefined') && (billingClient != null) && (billingClient != "") && (billingClient != "0") && (billingClient != "-1")) {
						  $scope.selectedBillingSiteId	= requestData.billingClient.toString();
					  
					  } else if((typeof(billingClient) == 'undefined') || (billingClient == null) || billingClient == "" || billingClient == "0" || billingClient == "-1") {
						  $scope.selectedBillingSiteId	= $scope.selectedSiteId;
						  
					  } else {
						  scope.selectedBillingSiteId	= "-1";
					  }
					  
					  if((typeof($scope.selectedBillingSiteId) != 'undefined') && ($scope.selectedBillingSiteId != null) &&
					     ($scope.selectedBillingSiteId != "") && ($scope.selectedBillingSiteId != "0") && ($scope.selectedBillingSiteId != "-1")) {
			    		
						  $scope.selectedBillingApprover= requestData.billingApprover.toString();
					  } else {
						  scope.selectedBillingApprover	= "-1";
					  }
					  
					  $scope.selectedApproverLevel1		= requestData.approverLevel1.toString();
					  $scope.selectedApproverLevel2		= requestData.approverLevel2.toString();
					  $scope.selectedApproverLevel3		= requestData.approverLevel3.toString();
					  $scope.approverLevel1Name			= requestData.approverLevel1Name;
					  $scope.approverLevel2Name			= requestData.approverLevel2Name;
					  $scope.approverLevel3Name			= requestData.approverLevel3Name;
					  $scope.reasonForSkip				= requestData.reasonForSkip;
					  
					  $scope.originatorId				= requestData.originatorId;
					  $scope.originatorName				= requestData.originatorName;
					  $scope.travellerId				= requestData.travellerId;
					  $scope.travellerName				= requestData.travellerName;
					  $scope.otherAccommodationsFlag	= requestData.otherAccommodationsFlag;
					  $scope.totalAdvanceAmountINR		= requestData.totalAdvanceAmountINR;
					  $scope.travelVisaFlag				= requestData.travelVisaFlag;
					  
					  $scope.journeys					= [];
					  $scope.itineraries				= [];
					  $scope.accommodatedItineraries	= [];
					  
					  /************************************************* IF FLIGHT JOURNEY DATA EXISTS *************************************************/
					  
					  if($scope.toLowerCaseString(requestData.flightTravelFlag)  == 'y') {
						  $scope.isFlightSelected = true;
						  
						  $scope.flightJourneyDetails = requestData.flightJourneyDetails;
						  
						  if($scope.flightJourneyDetails.journeyType == 'R' && $scope.flightJourneyDetails.returnTimeDesc != '') {
							  $scope.flightJourneyDetails.returnTime = 0;
							  $scope.flightJourneyDetails.returnTimeDesc = $scope.convert24To12HoursTimeFormat($scope.flightJourneyDetails.returnTimeDesc);
						  }
						  
						  angular.forEach($scope.flightJourneyDetails.flightJourney, function(journey) {
							  journey.preferredTime = 0;
							  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
							  
						  	  $scope.generateFlightJourney(journey, true);
						  	  $scope.generateFlightItinerary(journey, true);
						  });
						  
						  $scope.setFlightJourneyTypeOnEdit();
					  
					  } else {
						  $scope.isFlightSelected = false;
					  }
					  
					  /************************************************* IF TRAIN JOURNEY DATA EXISTS *************************************************/
					  
					  if($scope.toLowerCaseString(requestData.trainTravelFlag)  == 'y') {
						  $scope.isTrainSelected = true;
						  
						  $scope.trainJourneyDetails = requestData.trainJourneyDetails;
						  
						  if($scope.trainJourneyDetails.journeyType == 'R' && $scope.trainJourneyDetails.returnTimeDesc != '') {
							  $scope.trainJourneyDetails.returnTime = 0;
							  $scope.trainJourneyDetails.returnTimeDesc = $scope.convert24To12HoursTimeFormat($scope.trainJourneyDetails.returnTimeDesc);
						  }
						  
						  angular.forEach($scope.trainJourneyDetails.trainJourney, function(journey) {
							  journey.preferredTime = 0;
							  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
							  
							  $scope.generateTrainJourney(journey, true);
						  	  $scope.generateTrainItinerary(journey, true);
						  });
					  
						  $scope.setTrainJourneyTypeOnEdit();
						  
					  } else {
						  $scope.isTrainSelected = false;
					  }
					  
					  /************************************************* IF CAR JOURNEY DATA EXISTS *************************************************/
					  
					  if($scope.toLowerCaseString(requestData.carTravelFlag)  == 'y') {
						  $scope.isCarSelected = true;
						  
						  $scope.carJourneyDetails = requestData.carJourneyDetails;
						  
						  $("#carClass_"+requestData.carJourneyDetails.carClass).prop("checked", true);
						  $scope.getCarCategorylist(requestData.carJourneyDetails.carClass, true);
						  
						  if($scope.carJourneyDetails.journeyType == 'R' && $scope.carJourneyDetails.returnTimeDesc != '') {
							  $scope.carJourneyDetails.returnTime = 0;
							  $scope.carJourneyDetails.returnTimeDesc = $scope.convert24To12HoursTimeFormat($scope.carJourneyDetails.returnTimeDesc);
						  }
						  
						  angular.forEach($scope.carJourneyDetails.carJourney, function(journey) {
							  journey.preferredTime = 0;
							  journey.preferredTimeDesc = $scope.convert24To12HoursTimeFormat(journey.preferredTimeDesc);
							  
							  $scope.generateCarJourney(journey, true);
						  });
						  
						  $scope.setCarJourneyTypeOnEdit();
					  
					  } else {
						  $scope.isCarSelected = false;
					  }
					  
					  /************************************************* IF ACCOMMODATION DATA EXISTS ************************************************/
					  
					  if($scope.toLowerCaseString(requestData.accommodationDetailsFlag)  == 'y') {
						  $scope.isAccommodationSelected 	= true;
						  
						  $scope.accommodationDetails 		= requestData.accommodationDetails;
						  
						  angular.forEach($scope.accommodationDetails.accommodations, function(accommodation) {
							  $scope.generateAccommodation(accommodation);
						  	  $scope.addAccommodatedItinerary(accommodation.journeyCode);
						  });
						  
					  } else {
						  $scope.isAccommodationSelected 	= false;
					  }
					  
					  $scope.setAccommodationRequired();
					  
					  /************************************************* IF ADVANCE/FOREX DATA EXISTS ************************************************/
					  
					  if($scope.toLowerCaseString(requestData.advanceForexFlag)  == 'y') {
						  $scope.isAdvanceRequired 		= true;
						  $scope.advanceForexDetails 	= requestData.advanceForexDetails;
						  
						  var advanceForexRows = $scope.advanceForexDetails.advanceAmountDetails.length;
						  if (advanceForexRows == 1) {
							  $scope.getForexTotal(0, $scope.advanceForexDetails.advanceAmountDetails[0]);
						  
						  } else if (advanceForexRows > 1) {
							  $scope.getForexTotal(advanceForexRows - 2, $scope.advanceForexDetails.advanceAmountDetails[advanceForexRows - 2]);
						  }
					  
					  } else {
						  $scope.isAdvanceRequired 		= false;
					  }
					  
					  /************************************************* IF BUDGET ACTUAL DETAILS EXISTS *********************************************/
					  
					  if($scope.toLowerCaseString(requestData.budgetActualDetailsFlag) == 'y') {
						  $scope.ytmBudget			= requestData.budgetActualDetails.ytmBudget;
						  $scope.ytdActual			= requestData.budgetActualDetails.ytdActual;
					      $scope.availableBudget	= requestData.budgetActualDetails.availableBudget;
					      $scope.estimate			= requestData.budgetActualDetails.estimate;
					      $scope.expRemarks			= requestData.budgetActualDetails.expRemarks;
					  }
					  
					  /************************************************* IF TOTAL TRAVEL FARE DETAILS EXISTS *****************************************/
					  
					  if($scope.toLowerCaseString(requestData.totalTravelFareDetailsFlag) == 'y') {
						  $scope.totalTravelFareCurrency	= requestData.totalTravelFareDetails.currencyCode;
						  $scope.totalTravelFareAmount		= requestData.totalTravelFareDetails.fareAmount;
					  }
					  
					  /************************************************* IF TOTAL TRAVEL FARE DETAILS EXISTS *****************************************/
					  
					  if($scope.toLowerCaseString(requestData.localAgentFlag) == 'y' && $scope.toLowerCaseString(requestData.nonMATASourceDetails.nonMATASourceFlag) == 'y') {
						  $scope.nonMATASourceFlag 			= true;
						  $scope.nonMATASourceAirlineName	= requestData.nonMATASourceDetails.airlineName;
						  $scope.nonMATASourceCurrency		= requestData.nonMATASourceDetails.currency;
						  $scope.nonMATASourceAmount		= requestData.nonMATASourceDetails.amount;
						  $scope.nonMATASourceRemarks		= requestData.nonMATASourceDetails.remarks;
					  
					  } else {
						  $scope.nonMATASourceFlag = false;
					  }
					  
					  /*******************************************************************************************************************************/
					  
				  }
				  
				  $scope.checkApproversForOutOfOffice();
				  
				  //console.log(requestData);
			  }
			  
  	  		});
    	}
    };
    
    /************************************************* FETCH TRAVELLER PROFILE DETAILS *******************************************/
    
    $scope.getTravellerProfileDetails = function () {
    	var url = $scope.apiBaseUrl+"/api/request/user/detail";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travellerId		: $scope.selectedTraveller.userId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.traveller = response.data.data;
			}
			//console.log($scope.traveller);
		});
    };
    
	/************************************************* FETCH REQUEST SETTINGS *************************************************/
	
	$scope.getRequestSetting = function() {
		var url = $scope.apiBaseUrl+"/api/request/setting/"+$scope.pageType;
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travellerId		: $scope.selectedTraveller.userId,
			siteId			: $scope.selectedSiteId,
			billingSiteId	: $scope.selectedBillingSiteId,
			travelType		: $scope.travelType
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.requestSetting = response.data.data;
				
				$scope.baseCurrency = $scope.requestSetting.currency;
				
				if($scope.travelType.toLowerCase == 'i') {
					$scope.localAgentFlag = $scope.requestSetting.intLocalAgentFlag.toUpperCase();
				} else if($scope.travelType.toLowerCase == 'd') {
					$scope.localAgentFlag = $scope.requestSetting.domLocalAgentFlag.toUpperCase();
				} else {
					$scope.localAgentFlag = "N";
				}
			}
			//console.log($scope.requestSetting);
		});
    };
    
    /************************************************* FETCH LINKED TRAVEL REQUEST LIST ********************************/
    
    $scope.getLinkedTravelRequestList = function() {
    	
    	var url = $scope.apiBaseUrl+"/api/request/linked/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travellerId		: $scope.selectedTraveller.userId,
			travelType		: $scope.travelType
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.linkedTravelRequestList = response.data.data;
			}
			//console.log($scope.linkedTravelRequestList);
		});
    };
    
    /************************************************* FETCH SITE LIST *************************************************/
    
    $scope.getSiteList = function() {
		var url = $scope.apiBaseUrl+"/api/master/site/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			syncDate		: ""
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.siteList = response.data.data;
				
				//console.log($scope.siteList);
				
				angular.forEach($scope.siteList, function(site){
					
					$scope.siteObject   		= {};
					$scope.siteObject["id"] 	= site.siteId;
					$scope.siteObject["label"] 	= site.siteName;
					
					if($scope.siteList.length == 1 || site.isPrimarySite == 'Y' || site.isPrimarySite == 'y') {
						$scope.selectedSiteId = site.siteId;
						$scope.siteModel.push($scope.siteObject);
					}
					$scope.siteData.push($scope.siteObject);
	            });
				
				//console.log($scope.siteData);
				//console.log('Selected Site : '+$scope.selectedSiteId);
				
				$scope.getBaseCurrencyExchangeRate();
				$scope.getTravellersBySiteId();
				$scope.getCurrencyList();
				$scope.getSiteLocationList("");
				$scope.getBillingSiteList("");
				$scope.getBillingApproverList("", false);
				$scope.getCostCentreList("");
				$scope.getCarLocationlist("");
				
				if($.trim($scope.travelId) == "0" || ($.trim($scope.travelId) != "0" && !$scope.isCarSelected)) {
					$scope.getCarCategorylist($scope.carClass, false);
				}
			}
			
		});
    };
    
    /************************************************* MAIN API'S CALLING  *************************************************/
    
    $scope.getRequestData();
    
    $scope.getRequestSetting();
    $scope.getLinkedTravelRequestList();
    $scope.getSiteList();
    
    /************************************************* FETCH TRAVELLERS BY SITE *************************************************/
    
    $scope.getTravellersBySiteId = function() {
    	
		var url = $scope.apiBaseUrl+"/api/request/site/"+$scope.selectedSiteId+"/user/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.travellerList = response.data.data;
				
				angular.forEach($scope.travellerList, function(traveller) {
					traveller["pic"] = "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&accountname="+traveller.domainName+"\\"+traveller.winUserId+" ";
					if(traveller.userId == $scope.loggedInUserId) {
						$scope.selectedTraveller = traveller;
						$scope.selectedTravellerModel = traveller;
					}
	            });
			}
			//console.log($scope.travellerList);
		});
    };
    
    $scope.$watch('selectedTraveller', function(newValue, oldValue) {
    	$scope.setTraveller(newValue);
    });
    
    /************************************************* SET TRAVELLER DETAILS *************************************************/
    
    $scope.setTraveller = function(traveller) {
    	
    	if(traveller.userId != "0") {
    		
    		$scope.selectedTraveller = traveller;
        	
        	angular.element(document.querySelector(".user-list-dropdown")).removeClass("active-agent");
        	
        	$scope.getElement("#loading-div p.processingMessage").html("Please wait while fetching traveller settings.");
        	$scope.showProcessing();
        	
        	$timeout(function(){
        		$scope.getTravellerProfileDetails();
        		$scope.getRequestSetting();
            	$scope.getLinkedTravelRequestList();
            	
            	$scope.getTravelPreferences(1, "");
            	$scope.getTravelPreferences(2, "");
            	$scope.getTravelPreferences(5, "");
            	
            	$scope.getRequestApproverList();
            	$scope.getDefaultApproverList();
        	}, 500);
        	
    	}
    };
    
    /************************************************* FETCH CURRENCIES *************************************************/
    
    $scope.getCurrencyList = function() {
    	
    	var url = $scope.apiBaseUrl+"/api/master/currency/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.currencyList = response.data.data;
			}
			//console.log($scope.currencyList);
		});
    
    };
    
    /************************************************* SET SITE LOCATION'S GST NO *************************************************/
    
    $scope.setSiteLocationGSTNo = function() {
    	var keepGoing = true;
    	
    	angular.forEach($scope.siteLocationList, function(siteLocation) {
			if(keepGoing && siteLocation.locationId == $scope.selectedSiteLocation) {
				$scope.selectedSiteLocationGSTNo = siteLocation.gstNo;
				keepGoing = false;
			}
        });
    };
    
    /************************************************* CHECK FLIGHT CLASSES *************************************************/
    
    $scope.checkFlightClasses = function () {
    	$timeout(function() {
    		
    		if($scope.flightJourneyDetails.flightJourney.length > 0) {
    			angular.forEach($scope.flightJourneyDetails.flightJourney, function(journey) {
    				
    				if(journey.travelClass != "31" && $scope.toLowerCaseString(journey.travelClassDesc) != "no preference") {
    					var classExists = false;
        				
    					for(var i = 0; i < $scope.flightClassList.length; i++) {
        				    if ($scope.flightClassList[i].classId.toString() == journey.travelClass && $scope.flightClassList[i].eligibility == journey.travelClassDesc) {
        				        classExists = true;
        				        break;
        				    }
        				}
        				
        				if(!classExists) {
        					journey.travelClass = "31";
        					journey.travelClassDesc = "No Preference";
        				}
    				}
    				
    			});
    		}
    	},1000);
    };
    
    /************************************************* FETCH TRAVEL PREFERENCES *************************************************/
    
    $scope.getTravelPreferences = function(modeId, syncDate) {
    	
    	var url = $scope.apiBaseUrl+"/api/request/travel/preferences";
    	
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			travellerId 	: $scope.selectedTraveller.userId,
	    	modeId			: modeId,
	    	groupTravelFlag : $scope.groupTravelFlag,
	    	travelType		: $scope.travelType,
	    	syncDate		: syncDate,
	    	travelAgencyId	: $scope.travelAgencyId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.travelPreferences = response.data.data;
				
				if(modeId == 1) {
					$scope.mealList				= $scope.travelPreferences.mealList;
					$scope.flightTimeModeList	= $scope.travelPreferences.timeModeList;
					$scope.flightClassList		= $scope.travelPreferences.travelClassList;
					$scope.flightSeatList		= $scope.travelPreferences.seatList;
					
					angular.forEach($scope.flightClassList, function(travelClass) {
						if($scope.toLowerCaseString(travelClass.eligibility) == "economy") {
							travelClass.icon = "economy.png";
						} else if($scope.toLowerCaseString(travelClass.eligibility) == "business") {
							travelClass.icon = "business.png";
						} else if($scope.toLowerCaseString(travelClass.eligibility) == "1st class") {
							travelClass.icon = "first-class.png";
						} else if($scope.toLowerCaseString(travelClass.eligibility) == "premium economy") {
							travelClass.icon = "premium-economy.png";
						} else {
							travelClass.icon = "no-preference.png";
						}
			        });
					
					angular.forEach($scope.flightSeatList, function(seat) {
						if($scope.toLowerCaseString(seat.seatName) == "window") {
							seat.icon = "window-seat.png";
						} else if($scope.toLowerCaseString(seat.seatName) == "aisle") {
							seat.icon = "aisle-seat.png";
						} else {
							seat.icon = "no-preference.png";
						}
			        });
					
					$scope.checkFlightClasses();
					
					//console.log($scope.timeModeList);
					//console.log($scope.flightClassList);
					//console.log($scope.flightSeatList);
					//console.log($scope.mealList);
				}
				
				if(modeId == 2) {
					$scope.trainTimeModeList	= $scope.travelPreferences.timeModeList;
					$scope.trainClassList		= $scope.travelPreferences.travelClassList;
					$scope.trainSeatList		= $scope.travelPreferences.seatList;
							
					//console.log($scope.trainTimeModeList);
					//console.log($scope.trainClassList);
					//console.log($scope.trainSeatList);
				}
				
				if(modeId == 5) {
					$scope.mealList			= $scope.travelPreferences.mealList;
					$scope.carTimeModeList	= $scope.travelPreferences.timeModeList;
					$scope.carClassList		= $scope.travelPreferences.travelClassList;
					
					//console.log($scope.mealList);
					//console.log($scope.carTimeModeList);
					//console.log($scope.carClassList);
				}
			}
			//console.log($scope.travelPreferences);
		});
    };
    
    /************************************************* FETCH FLIGHT STAY TYPES *************************************************/
    
    $scope.getFlightStayTypeList = function(syncDate) {
    	
    	var url = $scope.apiBaseUrl+"/api/request/stay/type/list";
    	
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
	    	syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.flightStayTypeList = response.data.data;
			}
			//console.log($scope.flightStayTypeList);
		});
    };
    
    /************************************************* FETCH TRANSIT HOUSE LIST *************************************************/
    
    $scope.getTransitHouseList = function(syncDate) {
    	
    	var url = $scope.apiBaseUrl+"/api/request/transit/house/list";
    	
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
	    	syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.transitHouseList = response.data.data;
			}
			//console.log($scope.transitHouseList);
		});
    };
    
    /************************************************* FETCH TRAIN TICKET TYPE LIST ******************************************/
    
    $scope.getTrainTicketTypeList = function(syncDate) {
    	var url = $scope.apiBaseUrl+"/api/request/ticketType/list";
    	
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
	    	syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.trainTicketTypeList = response.data.data;
			}
			//console.log($scope.trainTicketTypeList);
		});
    };
    
    /************************************************* FETCH CURRENCIES LIST *************************************************/
    
    $scope.getSiteLocationList = function(syncDate) {
    	
    	var url = $scope.apiBaseUrl+"/api/request/unit/location/list";
    	
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
	    	siteId			: $scope.selectedSiteId,
	    	syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response){
			
			if(response != null && response.data.status == "200") {
				$scope.siteLocationList = response.data.data;
			}
			//console.log($scope.siteLocationList);
		});
    };
    
    /************************************************* FETCH BILLING SITE LIST *************************************************/
    
    $scope.getBillingSiteList = function(syncDate) {
		
		var url = $scope.apiBaseUrl+"/api/request/billing/unit/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.billingSiteList = response.data.data;
			}
			//console.log($scope.billingSiteList);
		});
    };
    
    /************************************************* FETCH BILLING APPROVERS LIST *************************************************/
    
    $scope.getBillingApproverList = function(syncDate, billingSiteChangeFlag) {
    	
    	if(($scope.selectedBillingSiteId != $scope.selectedSiteId) && ($scope.requestSetting.billingSiteFlag != "1") && 
    	   (typeof($scope.selectedBillingSiteId) != 'undefined') && ($scope.selectedBillingSiteId != null) &&
    	   ($scope.selectedBillingSiteId != "") && ($scope.selectedBillingSiteId != "0") && ($scope.selectedBillingSiteId != "-1")) {
    		
    		var url = $scope.apiBaseUrl+"/api/request/billing/approver/list";
   			
   			var headerData = {
   				winUserId		: $scope.winUserId,
   				domainName 		: $scope.domainName,
   				deviceId 		: $scope.ipAddress,
   				requestSource 	: $scope.requestSource,
   				systemName 		: $scope.systemName,
   				siteId			: $scope.selectedSiteId,
   				syncDate		: syncDate,
   				billingSiteId	: $scope.selectedBillingSiteId
   			};
   			
   			apiCallService.getRequest(url, headerData)
   			.then(function(response) {
   				
   				if(response != null && response.data.status == "200") {
   					$scope.billingApproverList = response.data.data;
   				}
   				
   				if(billingSiteChangeFlag) {
   					$scope.selectedBillingApprover = "-1";
   				}
   				
   				//console.log($scope.billingApproverList);
   			});
    	
    	} else {
    		$scope.billingApproverList = [];
   			$scope.selectedBillingApprover = "-1";
   			
   			//console.log($scope.billingApproverList);
    	}
    };
    
    /************************************************* ON BILLING SITE CHANGE *************************************************/
    
    $scope.onBillingSiteChange = function() {
    	$scope.getRequestSetting();
    	
	   	$timeout(function(){
	   		//console.log("billingSiteFlag : " + $scope.requestSetting.billingSiteFlag);
	   		
	   		if(($scope.selectedBillingSiteId == $scope.selectedSiteId) || ($scope.requestSetting.billingSiteFlag == "1")) {
	   			$scope.billingApproverList = [];
	   			$scope.selectedBillingApprover = "-1";
	   		
	   		} else if($scope.selectedBillingSiteId != $scope.selectedSiteId) {
	   			$scope.getBillingApproverList("", true);
	   		}
	   	}, 500);
    };
    
    /************************************************* FETCH APPROVERS LIST *************************************************/
    
    $scope.getRequestApproverList = function() {
		
		var url = $scope.apiBaseUrl+"/api/request/approver/levels/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			travellerId		: $scope.selectedTraveller.userId,
			travelType		: $scope.travelType
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.requestApprovers = response.data.data;
				
				$scope.approverLevel1List = $scope.requestApprovers.approverLevel1;
				$scope.approverLevel2List = $scope.requestApprovers.approverLevel2;
				$scope.approverLevel3List = $scope.requestApprovers.approverLevel3;
				
				if($scope.toLowerCaseString($scope.requestSetting.apprLvlAutoSelFlag) == 'y') {
					
					if($scope.approverLevel1List.length > 0) {
						$scope.selectedApproverLevel1 	= $scope.approverLevel1List[0].approverId;
						$scope.approverLevel1Name		= $scope.approverLevel1List[0].approverName;
					
					} else {
						$scope.selectedApproverLevel1 	= "0";
						$scope.approverLevel1Name		= "";
					}
					
					if($scope.approverLevel2List.length > 0) {
						$scope.selectedApproverLevel2 	= $scope.approverLevel2List[0].approverId;
						$scope.approverLevel2Name		= $scope.approverLevel2List[0].approverName;
						
					} else {
						$scope.selectedApproverLevel2 	= "0";
						$scope.approverLevel2Name		= "";
					}
					
					if($scope.approverLevel1List.length < 1 || $scope.approverLevel2List.length < 1) {
						$scope.disableSubmitButton();
						
						var apprLvlMsg = "You won't be able to submit request to workflow because";
						
						if($scope.approverLevel1List.length < 1 && $scope.approverLevel2List.length < 1) {
							apprLvlMsg += " Approver Level 1 & 2 are";
						
						} else if($scope.approverLevel1List.length < 1) {
							apprLvlMsg += " Approver Level 1 is";
						
						} else if($scope.approverLevel2List.length < 1) {
							apprLvlMsg += " Approver Level 2 is";
						}
						apprLvlMsg += " not defined in you profile. Please contact your Local Administrator for the updation of same.";
						
						$window.alert(apprLvlMsg);
						
					}
				
				} else {
					$scope.setReasonForSkip();
				}
			}
			
			//console.log($scope.requestApprovers);
			
			/*console.log($scope.approverLevel1List);
			console.log($scope.approverLevel2List);
			console.log($scope.approverLevel3List);*/
		});
    };
    
    /************************************************* FETCH DEFAULT APPROVERS LIST *************************************************/
    
    $scope.getDefaultApproverList = function() {
		
		var url = $scope.apiBaseUrl+"/api/request/approver/default/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			travellerId		: $scope.selectedTraveller.userId,
			travelType		: $scope.travelType
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.defaultApproverList = response.data.data;
			}
			$scope.hideProcessing();
			//console.log($scope.defaultApproverList);
		});
    };
    
    /************************************************* CHECK APPROVER EXIST IN DEFAULT WORKFLOW *************************************************/
    
    $scope.checkApproverExistInDefaultWorkflow = function(approverId) {
    	
    	if(parseInt(approverId) > 0) {
    		var url = $scope.apiBaseUrl+"/api/request/check/approver";
    		
    		var headerData = {
    			winUserId		: $scope.winUserId,
    			domainName 		: $scope.domainName,
    			deviceId 		: $scope.ipAddress,
    			requestSource 	: $scope.requestSource,
    			systemName 		: $scope.systemName,
    			siteId			: $scope.selectedSiteId,
    			travellerId		: $scope.selectedTraveller.userId,
    			travelType		: $scope.travelType,
    			approverId		: approverId
    		};
    		
    		apiCallService.getRequest(url, headerData)
    		.then(function(response) {
    			
    			if(response != null && response.data.status == "200") {
    				$scope.approverExistInDefaultWorkflow = response.data.data.isExists;
    				
    				if(response.data.data.isExists == "Y") {
    					$window.alert("This approver exists in default workflow.");
    				}
    			}
    			//console.log('Approver exist in DW: ' + $scope.approverExistInDefaultWorkflow);
    		});
    	}
    };
    
    /************************************************* CHECK APPROVERS FOR OUT OF OFFICE *************************************************/
    
    $scope.checkApproversForOutOfOffice = function() {
    	
    	var url = $scope.apiBaseUrl+"/api/request/check/ooo/approver";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			approverLevel1	: $scope.selectedApproverLevel1,
			approverLevel2	: $scope.selectedApproverLevel2,
			approverLevel3	: $scope.selectedApproverLevel3,
			travelType		: $scope.travelType,
			travellerId		: $scope.selectedTraveller.userId,
			siteId			: $scope.selectedSiteId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.oooForApproverExists = response.data.data.oooExistsFlag;
				
				if($scope.oooForApproverExists == "Y") {
					$scope.oooForApproverMessage = response.data.data.oooApproverText;
					//console.log($scope.oooForApproverMessage);
				}
			}
		});
    };
    
    /************************************************* CHECK TES COUNT *************************************************/
    
    $scope.checkTESCount = function() {
		
		var url = $scope.apiBaseUrl+"/api/request/check/tescount";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			travellerId		: $scope.selectedTraveller.userId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.TESCount  = response.data.data;
				
				$scope.canCreateRequest	= $scope.TESCount.canCreateRequest;
				$scope.TESMessage		= $scope.TESCount.message;
			}
			//console.log($scope.TESCount);
			//console.log($scope.canCreateRequest);
			//console.log($scope.TESMessage);
		});
    };
    
    /************************************************* FETCH COST CENTER LIST *************************************************/
    
    $scope.getCostCentreList = function(syncDate) {
		
		var url = $scope.apiBaseUrl+"/api/request/costcentre/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			siteId			: $scope.selectedSiteId,
			syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.costCentreList = response.data.data;
			}
			//console.log($scope.costCentreList);
		});
    };
    
    /************************************************* FETCH CAR CATEGORIES *************************************************/
    
    $scope.getCarCategorylist = function(carClassId, editFlag) {
		var url = $scope.apiBaseUrl+"/api/request/carcategory/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travelAgencyId	: $scope.travelAgencyId,
			travelType		: "B",
			carClass		: carClassId
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.carCategorylist = response.data.data;
				
				if(editFlag) {
					$scope.carClass			= $scope.carJourneyDetails.carClass.toString();
					$scope.carClassDesc		= $scope.carJourneyDetails.carClassDesc;
					$scope.carCategory		= $scope.carJourneyDetails.carCategory.toString();
					$scope.carCategoryDesc	= $scope.carJourneyDetails.carCategoryDesc;
					
				} else {
					if(carClassId == "16") {
			    		$scope.carCategory = "6";
			    	} else if(carClassId == "17") {
			    		$scope.carCategory = "10";
			    	}
				}
			}
			//console.log($scope.carCategorylist);
		});
    };
    
    /************************************************* FETCH CAR LOCATIONS *************************************************/
    
    $scope.getCarLocationlist = function(syncDate) {
    	
		var url = $scope.apiBaseUrl+"/api/request/car/location/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			syncDate		: syncDate
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			
			if(response != null && response.data.status == "200") {
				$scope.carLocationList = response.data.data;
			}
			//console.log($scope.carLocationList);
		});
    };
    
    /************************************************* FETCH CITIES *************************************************/
    
    $scope.getCitieslist = function(cityAirportName) {
    	$scope.citiesList = [];
    	
		var url = $scope.apiBaseUrl+"/api/request/cityairport/list";
		
		var headerData = {
			winUserId		: $scope.winUserId,
			domainName 		: $scope.domainName,
			deviceId 		: $scope.ipAddress,
			requestSource 	: $scope.requestSource,
			systemName 		: $scope.systemName,
			travelType		: $scope.travelType,
			travelAgencyId	: $scope.travelAgencyId,
			cityAirportName	: cityAirportName
		};
		
		apiCallService.getRequest(url, headerData)
		.then(function(response) {
			if(response != null && response.data.status == "200") {
				//$scope.citiesList = response.data.data;
				var count = 0;
				angular.forEach(response.data.data, function(city){
					//if(count < 100) {
						city["id"] = city.countryCode;
						city["label"] = city.cityName + ' - ' + city.airportCode + ' , ' + city.countryCode;
						//city["country"] = city.country;
						
						$scope.citiesList.push(city);
						count++;
					//}
	            });
				
				//console.log(response.data.data);
			}
		});
    };
    
    $scope.userLocalSearch = function(str, travellerList) {
        var matches = [];
        travellerList.forEach(function(traveller) {
          if ((traveller.userName.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
        	  traveller["pic"] = "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&accountname="+traveller.domainName+"\\"+traveller.winUserId+" ";
              matches.push(traveller);
          }
        });
        return matches;
    };
    
    $scope.userSelected = function(selected) {
        if (selected) {
        	$scope.enableSubmitButton();
        	
        	$scope.imgSrc = selected.originalObject.pic;
        	$scope.selectedTraveller = selected.originalObject;
        	
        	$scope.getLinkedTravelRequestList();
        	
        } else {
        	$scope.imgSrc = "https://one.motherson.com/_layouts/15/userphoto.aspx?size=s&amp;accountname=\\";
            //console.log('cleared');
        }
    	$scope.hideProcessing();
        
    	//console.log($scope.selectedTraveller);
    };
      
      
    /************************************************* CALL METHODS *************************************************/
      
    $scope.getFlightStayTypeList("");
    $scope.getTransitHouseList("");
    $scope.getTrainTicketTypeList("");
    $scope.getCitieslist("");
    
    /************************************************* VALIDATE REQUEST DATA ****************************************/
    
    $scope.disableSubmitButton = function () {
    	$("#submit").removeClass("btn-primary");
    	$("#submit").attr("disabled", true);
    };
    
    $scope.enableSubmitButton = function () {
    	$("#submit").addClass("btn-primary");
    	$("#submit").removeAttr("disabled");
    };
    
    $scope.disableButtons = function () {
    	$("#back").removeClass("btn-default");
    	$("#saveAndExit").removeClass("btn-primary");
    	$("#save").removeClass("btn-primary");
    	$("#submit").removeClass("btn-primary");
    	
    	$("#back, #saveAndExit, #save, #submit").attr("disabled", true);
    };
    
    $scope.enableButtons = function () {
    	$("#back").addClass("btn-default");
    	$("#saveAndExit").addClass("btn-primary");
    	$("#save").addClass("btn-primary");
    	$("#submit").addClass("btn-primary");
    	
    	$("#back, #saveAndExit, #save, #submit").removeAttr("disabled");
    };
    
    $scope.validateJourneyDate = function (dateOfJourney, preferredTimeDesc, is12HoursFormat) {
    	var currentDate = new Date();
    	
    	if(preferredTimeDesc == "") {
    		preferredTimeDesc = currentDate.getHours()+":"+(currentDate.getMinutes()+1);
    		is12HoursFormat = false;
    	}
    	
    	var dateObject = $scope.getDateTimeObject(dateOfJourney, preferredTimeDesc, is12HoursFormat);
    	
    	if(dateObject < currentDate) {
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateFlightJourneySection = function () {
    	var validationFlag = false;
    	
    	if($scope.flightJourneyDetails.flightJourney.length == 0) {
    		$window.alert("Please add at least one flight journey.");
    		return false;
    	
    	} else {
    		var keepGoing = true;
    		
    		angular.forEach($scope.flightJourneyDetails.flightJourney, function(journey, $index) {
    			if(keepGoing && !$scope.validateJourneyDate(journey.dateOfJourney, journey.preferredTimeDesc, true)) {
    				
    				journey.dateOfJourney = $scope.getTodayDateInddmmmyyyyFormat(0);
    				$scope.editFlightJourney($index, journey);
    				
    				$window.alert("Flight journey date (or time) for itinerary : ["+ journey.source +" - "+ journey.destination +"] cannot be less than current date (or time).");
    				$("#flightDateOfJourney").focus();
    				
    				validationFlag = true;
    				keepGoing = false;
    			}
    		});
    	}
    	
    	if(validationFlag) {
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateTrainJourneySection = function () {
    	var validationFlag = false;
    	
    	if($scope.trainJourneyDetails.trainJourney.length == 0) {
    		$window.alert("Please add at least one train journey.");
    		return false;
    	
    	} else {
    		var keepGoing = true;
    		
    		angular.forEach($scope.trainJourneyDetails.trainJourney, function(journey, $index) {
    			if(keepGoing && !$scope.validateJourneyDate(journey.dateOfJourney, journey.preferredTimeDesc, true)) {
    				
    				journey.dateOfJourney = $scope.getTodayDateInddmmmyyyyFormat(0);
    				$scope.editTrainJourney($index, journey);
    				
    				$window.alert("Train journey date (or time) for itinerary : ["+ journey.source +" - "+ journey.destination +"] cannot be less than current date (or time).");
    				$("#trainDateOfJourney").focus();
    				
    				validationFlag = true;
    				keepGoing = false;
    			}
    		});
    	}
    	
    	if(validationFlag) {
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateCarJourneySection = function () {
    	var validationFlag = false;
    	
    	if($scope.carJourneyDetails.carJourney.length == 0) {
    		$window.alert("Please add at least one car journey.");
    		return false;
    	
    	} else {
    		var keepGoing = true;
    		
    		angular.forEach($scope.carJourneyDetails.carJourney, function(journey, $index) {
    			if(keepGoing && !$scope.validateJourneyDate(journey.dateOfJourney, journey.preferredTimeDesc, true)) {
    				
    				journey.dateOfJourney = $scope.getTodayDateInddmmmyyyyFormat(0);
    				$scope.editCarJourney($index, journey);
    				
    				$window.alert("Car journey date (or time) for itinerary : ["+ journey.source +" - "+ journey.destination +"] cannot be less than current date (or time).");
    				$("#carDateOfJourney").focus();
    				
    				validationFlag = true;
    				keepGoing = false;
    			}
    		});
    	}
    	
    	if(validationFlag) {
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateAccommodationSection = function () {
    	var validationFlag = false;
    	
    	if($scope.itineraries.length > 0) {
    		$window.alert("Please fill accommodation details of all Flight/Train/Car journeys.");
    		return false;
    		
    	} else if($scope.accommodationDetails.accommodations.length == 0) {
    		$window.alert("Please add at least one accommodation.");
    		return false;
    	
    	} else {
    		var keepGoing = true;
    		
    		angular.forEach($scope.accommodationDetails.accommodations, function(accommodation, $index) {
    			if(keepGoing && !$scope.validateJourneyDate(accommodation.checkInDate, "", false)) {
    				
    				accommodation.checkInDate = $scope.getTodayDateInddmmmyyyyFormat(0);
    				$scope.editAccommodation($index, accommodation);
    				
    				$window.alert("Check in date for accommodation at "+ accommodation.place +" cannot be less than current date.");
    				$("#accommodationCheckInDate").focus();
    				
    				validationFlag = true;
    				keepGoing = false;
    			}
    		});
    	}
    	
    	if(validationFlag) {
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateAdvanceForexData = function () {
    	var validationFlag = false;
    	
    	$("table.advanceRequiredTable tr.advanceRequiredRow").each(function() {
    		
    		var currency 				= $(this).find("[name='advanceForexCurrency']");
    		var dailyAllowanceExpPerDay = $(this).find("[name='dailyAllowanceExpPerDay']");
    		var dailyAllowanceTourDays 	= $(this).find("[name='dailyAllowanceTourDays']");
    		var hotelChargesExpPerDay 	= $(this).find("[name='hotelChargesExpPerDay']");
    		var hotelChargesTourDays 	= $(this).find("[name='hotelChargesTourDays']");
    		/*var contingencies 			= $(this).find("[name='contingencies']");
    		var others 					= $(this).find("[name='others']");*/
    		var exchangeRateINR 		= $(this).find("[name='exchangeRateINR']");
    		var totalINR				= $(this).find("[name='totalINR']");
    		
    		var currencyVal				= currency.find("option:selected").val();
    		
    		if(currencyVal == "") {
    			$window.alert("Please select currency.");
    			currency.focus();
    			validationFlag = true;
    			return false;
    		
    		} else if($.trim(dailyAllowanceExpPerDay.val()) == "") {
    			$window.alert("Please enter the daily allowance expenditure per day.").
    			dailyAllowanceExpPerDay.focus();
    			validationFlag = true;
    			return false;
    			
    		} else if($.trim(hotelChargesExpPerDay.val()) == "") {
    			$window.alert("Please enter the hotel charges expenditure per day.").
    			hotelChargesExpPerDay.focus();
    			validationFlag = true;
    			return false;
    			
    		} else if($.trim(dailyAllowanceExpPerDay.val()) != "0") {
    			if($.trim(dailyAllowanceTourDays.val()) == "" || $.trim(dailyAllowanceTourDays.val()) == "0") {
    				$window.alert("Please enter the daily allowance tour days.");
    				dailyAllowanceTourDays.focus();
    				validationFlag = true;
        			return false;
    			}
    		
    		} else if($.trim(hotelChargesExpPerDay.val()) != "0") {
    			if($.trim(hotelChargesTourDays.val()) == "" || $.trim(hotelChargesTourDays.val()) == "0") {
    				$window.alert("Please enter the Hotel charges tour days.");
    				hotelChargesTourDays.focus();
    				validationFlag = true;
        			return false;
    			}
    			
    		} else if($.trim(exchangeRateINR.val()) == "0") {
    			$window.alert("Currency exchange rate cannot be 0.");
    			exchangeRateINR.focus();
    			validationFlag = true;
    			return false;
    		
    		} else if(totalINR.val() == 0) {
    			$window.alert("Total INR amount of currency cannot be 0.");
    			totalINR.focus();
    			validationFlag = true;
    			return false;
    		}
    		
    	});
    	
    	if(validationFlag) {
    		return false;
    	}
    	
    	var totalAmountINR 		= parseFloat($scope.advanceForexDetails.totalAmountINR);
    	var totalUSDPerDay 		= parseFloat($scope.advanceForexDetails.totalUSDPerDay);
    	var advanceForexRemarks = $("#advanceForexRemarks");
    	
    	if(totalAmountINR == 0) {
    		$window.alert("Please enter advance/forex details.");
    		return false;
    	
    	} else if(totalUSDPerDay > 350) {
    		$window.alert("Total Per Day expense should not exceed $350 or in equivalent currencies.");
    		return false;
    	
    	} else if ($.trim(advanceForexRemarks.val()) == "") {
    		$window.alert("Please enter the remarks for contingencies/any other expenditure");
    		advanceForexRemarks.focus();
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateTotalTravelFareSection = function () {
    	return true;
    };
    
    $scope.validateBudgetInputSection = function () {
    	var ytmBudget 			= $("#ytmBudget");
    	var ytdActual			= $("#ytdActual");
    	var estimate			= $("#estimate");
    	var expRemarks			= $("#expRemarks");
    	
    	if($.trim(ytmBudget.val()) == "" || $.trim(ytdActual.val()) == "" || $.trim(estimate.val()) == "") {
    		if($.trim(ytmBudget.val()) == "" && $.trim(ytdActual.val()) == "" && $.trim(estimate.val()) == "" && $.trim(expRemarks.val()) == "") {
        		$window.alert("Please enter the remarks for not entering the Budget Actual Details.");
        		expRemarks.focus();
        		return false;
        		
        	} else if($.trim(expRemarks.val()) == "") {
        		if($.trim(ytmBudget.val()) == "") {
        			$window.alert("Please enter YTM Budget Details.");
        			ytmBudget.focus();
            		return false;
            		
        		} else if ($.trim(ytdActual.val()) == "") {
        			$window.alert("Please enter YTD Actual Details.");
        			ytdActual.focus();
            		return false;
            		
        		} else if($.trim(estimate.val()) == "") {
        			$window.alert("Please enter Estimate Details.");
        			estimate.focus();
            		return false;
        		}
        	}
    	}
    	
    	return true;
    };
    
    $scope.validateNonMATASourceSection = function () {
    	var nonMATASourceAirlineName 	= $("#nonMATASourceAirlineName");
    	var nonMATASourceAmount			= $("#nonMATASourceAmount");
    	var nonMATASourceRemarks		= $("#nonMATASourceRemarks");
    	
    	if($.trim(nonMATASourceAirlineName.val()) == "") {
    		$window.alert("Please enter airline name.");
    		nonMATASourceAirlineName.focus();
    		return false;
    	
    	} else if($.trim(nonMATASourceAmount.val()) == "") {
    		$window.alert("Please enter the local price.");
    		nonMATASourceAmount.focus();
    		return false;
    	
    	} else if($.trim(nonMATASourceRemarks.val()) == "") {
    		$window.alert("Please enter the remarks.");
    		nonMATASourceRemarks.focus();
    		return false;
    	}
    	
    	return true;
    };
    
    $scope.validateTravelSummaryData = function (submitFlag) {
    	
    	var reasonForTravel 	= $("#reasonForTravel");
    	var mealPreference 		= $("#mealPreference");
    	var costCentre			= $("#costCentre");
    	var siteLocation		= $("#siteLocation");
    	var gstNo				= $("#gstNo");
    	var billingClient		= $("#billingClient");
    	var billingApprover		= $("#billingApprover");
    	
    	var approverLevel1		= $("#approverLevel1");	
    	var approverLevel2		= $("#approverLevel2");
    	var approverLevel3		= $("#approverLevel3");
    	var reasonForSkip		= $("#reasonForSkip");
    	
    	var mealPreferenceVal 	= mealPreference.find("option:selected").val();
    	var costCentreVal		= costCentre.find("option:selected").val();
    	var siteLocationVal		= siteLocation.find("option:selected").val();
    	var billingClientVal 	= billingClient.find("option:selected").val();
    	var billingApproverVal 	= billingApprover.find("option:selected").val();
    	var approverLevel1Val	= approverLevel1.find("option:selected").val();
    	var approverLevel2Val	= approverLevel2.find("option:selected").val();
    	var approverLevel3Val	= approverLevel3.find("option:selected").val();
    	
    	if($.trim(reasonForTravel.val()) == "") {
    		$window.alert("Please enter reason for travel.");
    		reasonForTravel.focus();
			return false;
    	}
    	
    	if(submitFlag == "Y") {
    		if(mealPreferenceVal == "0") {
    			$window.alert("Please select meal preference.");
    			mealPreference.focus();
    			return false;
    		
    		} else if($scope.requestSetting.apprLvl3Flag.toLowerCase() == 'y' && $scope.requestSetting.costCentreFlag.toLowerCase() == 'y' && costCentreVal == "0") {
    			$window.alert("Please select cost centre.");
    			costCentre.focus();
    			return false;
    		}
    	}
    		
		if($scope.requestSetting.gstApplicableFlag.toLowerCase() == 'y' && siteLocationVal == "-1") {
			$window.alert("Please select unit location.");
			siteLocation.focus();
			return false;
		
		} else if($scope.requestSetting.gstApplicableFlag.toLowerCase() == 'y' && $.trim(gstNo.val()) == "") {
			$window.alert("Please update GST No. for the unit's location, otherwise you will not be able to submit request.");
			siteLocation.focus();
			return false;
		
		} else if(billingClientVal == "" || billingClientVal == "0") {
			$window.alert("Please select billing client.");
			billingClient.focus();
			return false;
			
		} 
		
		if(billingClientVal != $scope.selectedSiteId && (billingApproverVal == "-1" || billingApproverVal == "" || billingApproverVal == "0") && $scope.requestSetting.billingSiteFlag != "1") {
			$window.alert("Please select billing approver.");
			billingApprover.focus();
			return false;
		}
		
		if($scope.requestSetting.apprLvlAutoSelFlag.toLowerCase() != 'y') {
			
			if($scope.requestSetting.mandatoryAppFlag.toLowerCase() == 'y') {
				if(approverLevel1Val == "-1" || approverLevel2Val == "-1") {
					$window.alert("Selection of Both Approval Level 1 and 2 is mandatory");
					
					if(approverLevel1Val == "-1") {
						approverLevel1.focus();
					} else if(approverLevel2Val == "-1") {
						approverLevel2.focus();
					}
					return false;
				}
				
			} else {
				var apprLvl3Flag 	= $scope.requestSetting.apprLvl3Flag.toLowerCase();
				var unitHeadFlag 	= $scope.requestSetting.siteHeadFlag;
				
				if(apprLvl3Flag =='y' && unitHeadFlag !='1') {
					if(approverLevel1Val == "-1" && approverLevel2Val == "-1") {
						$window.alert("Please select approval level 1 or approval level 2.");
						approverLevel1.focus();
						return false;
					}
				}
				
				if($scope.travelType == 'D') {
					if(unitHeadFlag == '1' && approverLevel2Val == '-1' && apprLvl3Flag != 'y') {
					  	$window.alert("Please select approval Level 2.");
					  	approverLevel2.focus();
					  	return false;
					}
				}
				
				if((approverLevel1Val == "-1" || approverLevel1Val == '') && (approverLevel2Val == "-1" || approverLevel2Val == '')) {			
					if(reasonForSkip.val() == '') {
						$window.alert("Please give the reason for skipping approval level 1 and approval level 2");
						reasonForSkip.focus();
						return false;
					}
				
				} else if((approverLevel1Val == "-1" || approverLevel1Val == '') && (approverLevel2Val != "-1" || approverLevel2Val != '')) {			
					if(reasonForSkip.val() == '') {
						$window.alert("Please give the reason for skipping approval level 1.");
						reasonForSkip.focus();
						return false;
					}
				
				} else if((approverLevel1Val != "-1" || approverLevel1Val != '') && (approverLevel2Val == "-1" || approverLevel2Val == '')) {			
					if(reasonForSkip.val() == '') {
						$window.alert("Please give the reason for skipping approval level 2.");
						reasonForSkip.focus();
						return false;
					}
				}
				
			}
		}
		
		var apprLvl3ForBmFlag = $scope.requestSetting.apprLvl3ForBmFlag.toLowerCase();
		
		if(apprLvl3ForBmFlag == 'y') {
			if(approverLevel3Val == '-1' || approverLevel3Val == '') {
				$window.alert("Please select board member");
				approverLevel3.focus();
				return false;
			}
		}
    		
    	return true;
    };
    
    $scope.validateRequestData = function (submitFlag) {
    	$scope.checkTESCount();
    	
    	if($scope.selectedSiteId == "" || $scope.selectedSiteId == "0") {
    		$window.alert("Please select unit.");
    		$("div.siteDiv").find("button").click();
    		return false;
    		
    	} else if($scope.selectedTraveller.userId == "" || $scope.selectedTraveller.userId == "0") {
    		$window.alert("Please select traveller.");
    		$("#traveller_value").focus();
    		return false;
    	}
    	
    	if(!$scope.isFlightSelected && !$scope.isTrainSelected && !$scope.isCarSelected && !$scope.isAccommodationSelected && !$scope.isAdvanceRequired) {
    		$window.alert("At least provide details either Flight, Train, Car, Accommodation or Advance is required to create a travel request.");
    		return false;
    	}
    	
    	if($scope.requestSetting.cancelledReqFlag.toLowerCase() == 'y' && !$scope.isFlightSelected && !$scope.isTrainSelected && !$scope.isCarSelected && !$scope.isAccommodationSelected && $scope.isAdvanceRequired) {
    		$window.alert("Please select the On-Air Travel Request");
    		$("#linkedTravelRequest").focus();
    		return false;
    	}
    	
    	if($scope.isFlightSelected) {
    		if(!$scope.validateFlightJourneySection()) {
    			return false;
    		}
    	}
    	
    	if($scope.travelType != "I" && $scope.isTrainSelected) {
    		if(!$scope.validateTrainJourneySection()) {
    			return false;
    		}
    	}
    	
    	if($scope.travelType != "I" && $scope.isCarSelected) {
    		if(!$scope.validateCarJourneySection()) {
    			return false;
    		}
    	}
    	
    	if($scope.isAccommodationSelected) {
    		if(!$scope.validateAccommodationSection()) {
    			return false;
    		}
    	}
    	
    	if($scope.isAdvanceRequired) {
    		if(!$scope.validateAdvanceForexData()) {
     			return false;
     		}
    	}
    	
    	if($scope.requestSetting.apprLvl3Flag.toLowerCase() == 'y') {
    		if(!$scope.validateTotalTravelFareSection()) {
     			return false;
     		}
    	}
    	
    	if($scope.requestSetting.budgetInputFlag.toLowerCase() == 'y') {
    		if(!$scope.validateBudgetInputSection()) {
     			return false;
     		}
    	}
    	
    	if($scope.localAgentFlag.toLowerCase() == 'y') {
    		if(!$scope.validateNonMATASourceSection()) {
     			return false;
     		}
    	}
    	
    	if(submitFlag == "Y") {
    		if(!$scope.canCreateRequest) {
    			$scope.disableSubmitButton();
				$window.alert($scope.TESMessage);
				return false;
			}
    	}
    	
    	if(!$scope.validateTravelSummaryData(submitFlag)) {
    		return false;
    	}
    	
    	return true;
    };
    
    /************************************************* SAVE REQUEST DATA ********************************************/
      
    $scope.getReqSubmitMessage = function() {
    	$scope.checkApproversForOutOfOffice();
    	
    	var message 	= "Are you sure you want to submit this";
    	var dataFlag	= false;
    	var source		= "";
    	var destination	= "";
    	
    	if($scope.travelType == "I") {
    		message += ' <b>INTERNATIONAL</b> travel request';
    	} else {
    		message += ' <b>DOMESTIC</b> travel request';
    	}
    	
    	if($scope.isFlightSelected && $scope.flightJourneyDetails.flightJourney.length > 0) {
    		source 		= $scope.flightJourneyDetails.flightJourney[0].source;
    		destination = $scope.flightJourneyDetails.flightJourney[0].destination;
    		
    		message 	+= ' for Sector : <b>[ ' + source + '-' + destination + ' ]</b>';
    		dataFlag	= true;
    		
    		if($scope.flightJourneyDetails.flightJourney.length > 1) {
    			message 	+= '...';
    		}
    	}
    	
    	if($scope.travelType == "D" && !dataFlag) {
    		if($scope.isTrainSelected && $scope.trainJourneyDetails.trainJourney.length > 0) {
    			source 		= $scope.trainJourneyDetails.trainJourney[0].source;
    			destination = $scope.trainJourneyDetails.trainJourney[0].destination;
    			
    			message 	+= ' for Sector : <b>[ ' + source + '-' + destination + ' ]</b>';
    			
    			if($scope.trainJourneyDetails.trainJourney.length > 1) {
    				message += '...';
    			}
    		}
    		
    		if($scope.isCarSelected && $scope.carJourneyDetails.carJourney.length > 0) {
    			source 		= $scope.carJourneyDetails.carJourney[0].source;
    			destination = $scope.carJourneyDetails.trainJourney[0].destination;
    			
    			message 	+= ' for Sector : <b>[ ' + source + '-' + destination + ' ]</b>';
    			
    			if($scope.carJourneyDetails.carJourney.length > 1) {
    				message += '...';
    			}
    		}
    	}
    	
    	message += ' to workflow ?';
    	
    	if ($scope.travelType == "I") {
    		message += '\n\nHope you have verified your FOREX as well !';
    	}
    	
    	return "<font style='color:darkorange; font-weight:bold;'>"+$scope.oooForApproverMessage + "</font><br>" + message;
    }
    
    $scope.saveRequestDataToDB = function(submitFlag) {
    	
    	if($scope.travelType == 'D') {
    		$scope.localAgentFlag = $scope.requestSetting.domLocalAgentFlag;
    	
    	} else if($scope.travelType == 'I') {
    		$scope.localAgentFlag = $scope.requestSetting.intLocalAgentFlag;
    	}
    	
    	var temp_flightJourneyDetails;
    	var temp_trainJourneyDetails;
    	var temp_carJourneyDetails;
    	var temp_accommodationDetails;
    	
    	var temp_advanceForexDetails;
    	var temp_budgetActualDetails;
    	var temp_totalTravelFareDetails;
    	var temp_nonMATASourceDetails;
    	
    	if($scope.isFlightSelected) {
    		temp_flightJourneyDetails 	= angular.copy($scope.flightJourneyDetails);
    	} else {
    		temp_flightJourneyDetails 	= {};
    	}
    	
    	if($scope.isTrainSelected && $scope.travelType == 'D') {
    		temp_trainJourneyDetails 	= angular.copy($scope.trainJourneyDetails);
    	} else {
    		temp_trainJourneyDetails 	= {};
    	}
    	
    	if($scope.isCarSelected && $scope.travelType == 'D') {
    		temp_carJourneyDetails 		= angular.copy($scope.carJourneyDetails);
    	} else {
    		temp_carJourneyDetails 		= {};
    	}
    	
    	if($scope.isAccommodationSelected) {
    		temp_accommodationDetails 	= $scope.accommodationDetails;
    	} else {
    		temp_accommodationDetails 	= {};
    	}
    	
    	if($scope.isAdvanceRequired) {
    		temp_advanceForexDetails 	= $scope.advanceForexDetails;
    	} else {
    		temp_advanceForexDetails 	= {};
    	}
    	
    	if($scope.toLowerCaseString($scope.requestSetting.budgetInputFlag) == 'y') {
    		temp_budgetActualDetails = {
				ytmBudget			: $scope.ytmBudget,
		    	ytdActual			: $scope.ytdActual,
		    	availableBudget		: $scope.availableBudget,
		    	estimate			: $scope.estimate,
		    	expRemarks			: $scope.expRemarks
			};
    	
    	} else {
    		temp_budgetActualDetails = {};
    	}
    	
    	if($scope.toLowerCaseString($scope.requestSetting.apprLvl3Flag) == 'y') {
    		temp_totalTravelFareDetails = {
				currency			: $scope.totalTravelFareCurrency,
			  	fareAmount			: $scope.totalTravelFareAmount
			};
    	
    	} else {
    		temp_totalTravelFareDetails = {};
    	}
    	
    	if($scope.toLowerCaseString($scope.localAgentFlag) == 'y') {
    		temp_nonMATASourceDetails = {
				nonMATASourceFlag	: $scope.nonMATASourceFlag,
				airlineName			: $scope.nonMATASourceAirlineName,
				currency			: $scope.nonMATASourceCurrency,
				amount				: $scope.nonMATASourceAmount,
				remarks				: $scope.nonMATASourceRemarks
			};
    	
    	} else {
    		temp_nonMATASourceDetails = {};
    	}
    	
    	angular.forEach(temp_flightJourneyDetails.flightJourney, function(journey) {
    		journey.preferredTimeDesc = $scope.convert12To24HoursTimeFormat(journey.preferredTimeDesc);
    	});
    	if(temp_flightJourneyDetails.journeyType == 'R' && temp_flightJourneyDetails.returnTimeDesc != '') {
    		temp_flightJourneyDetails.returnTimeDesc = $scope.convert12To24HoursTimeFormat(temp_flightJourneyDetails.returnTimeDesc);
    	}
    	
    	angular.forEach(temp_trainJourneyDetails.trainJourney, function(journey) {
    		journey.preferredTimeDesc = $scope.convert12To24HoursTimeFormat(journey.preferredTimeDesc);
    	});
    	if(temp_trainJourneyDetails.journeyType == 'R' && temp_trainJourneyDetails.returnTimeDesc != '') {
    		temp_trainJourneyDetails.returnTimeDesc = $scope.convert12To24HoursTimeFormat(temp_trainJourneyDetails.returnTimeDesc);
    	}
    	
    	angular.forEach(temp_carJourneyDetails.carJourney, function(journey) {
    		journey.preferredTimeDesc = $scope.convert12To24HoursTimeFormat(journey.preferredTimeDesc);
    	});
    	if(temp_carJourneyDetails.journeyType == 'R' && temp_carJourneyDetails.returnTimeDesc != '') {
    		temp_carJourneyDetails.returnTimeDesc = $scope.convert12To24HoursTimeFormat(temp_carJourneyDetails.returnTimeDesc);
    	}
    	
    	var url = $scope.apiBaseUrl+"/api/request";
    	
    	var requestData = {
    		travelId				: $scope.travelId,
    		linkedTravelRequestId	: $scope.linkedTravelRequestId,
    		travelType				: $scope.travelType,
    		siteId					: $scope.selectedSiteId,
    		costCentreId			: $scope.selectedCostCentre,
    		travellerId				: $scope.selectedTraveller.userId,
    		preferredMealId			: $scope.selectedMealPreference,
    		siteLocation			: $scope.selectedSiteLocation,
    		gstNo					: $scope.selectedSiteLocationGSTNo,
    		billingClient			: $scope.selectedBillingSiteId,
    		billingApprover			: $scope.selectedBillingApprover,
    		approverLevel1			: $scope.selectedApproverLevel1,
    		approverLevel2			: $scope.selectedApproverLevel2,
    		approverLevel3			: $scope.selectedApproverLevel3,
    		reasonForTravel			: $scope.reasonForTravel,
    		reasonForSkip			: $scope.reasonForSkip,
    		baseCurrency			: $scope.requestSetting.currency,
    		submitFlag				: submitFlag,
    		
    		requestSource			: $scope.requestSource,
    		deviceId				: $scope.ipAddress,
    		systemName				: $scope.systemName,
    		domainName				: $scope.domainName,
    		winUserId				: $scope.winUserId,
    		
    		flightJourneyDetails 	: temp_flightJourneyDetails,
    		trainJourneyDetails 	: temp_trainJourneyDetails,
    		carJourneyDetails 		: temp_carJourneyDetails,
    		accommodationDetails 	: temp_accommodationDetails,
    		advanceForexDetails 	: temp_advanceForexDetails,
    		budgetActualDetails 	: temp_budgetActualDetails,
    		totalTravelFareDetails 	: temp_totalTravelFareDetails,
    		
    		localAgentFlag			: $scope.localAgentFlag,
    		nonMATASourceDetails	: temp_nonMATASourceDetails
    	};
    	
    	//console.log(requestData);
    	
    	apiCallService.postRequest(url, requestData, {})
  		.then(function(response) {
  			if(response != null) {
  				//console.log(response.data);
  				
  				var message 			= response.data.data.requestStatus;
  				$scope.travelRequestNo 	= response.data.data.requestNo;
  				$scope.travelId 		= response.data.data.travelId;
  				
  				if(message.indexOf("success") == -1) {
  					$scope.enableButtons();
  					
  					$scope.getElement("#temp-save-popup p.statusMessage").html(message);
	  				$scope.getElement("#temp-save-popup").addClass("overlay-alert-popup-show");
  					
  				} else {
  					if(submitFlag == "Y") {
  						$scope.disableButtons();
  						
  						$scope.getElement("#final-submit-popup p.statusMessage span").html(message);
  	  					$scope.getElement("#final-submit-popup").addClass("overlay-alert-popup-show");
  	  					
  					} else {
  						$scope.getRequestData();
  						$scope.enableButtons();
  						
  	  					$scope.getElement("#temp-save-popup p.statusMessage span").html(message);
  	  					$scope.getElement("#temp-save-popup").addClass("overlay-alert-popup-show");
  					}
  				}
  			}
  			$scope.hideProcessing();
  		});
    };
    
    $scope.saveRequestData = function(submitFlag) {
    	$scope.disableButtons();
    	$scope.getElement("#loading-div p.processingMessage").html("Please wait while request data is validating");
    	$scope.showProcessing();
    	
    	$timeout(function(){
        	if($scope.validateRequestData(submitFlag)) {
        		
        		if(submitFlag == "N") {
        			$scope.saveRequestDataToDB(submitFlag);
        			
        		} else if(submitFlag == "Y") {
        			if($scope.travelType == "I" && !$scope.isAdvanceRequired) {
        				if($window.confirm("You have not selected FOREX, Are you sure you want to continue without FOREX ?")) {
        					$scope.hideProcessing();
            				$scope.getElement("#alert-popup-confirm p.requestSubmitMessage").html($scope.getReqSubmitMessage());
            				$scope.getElement("#alert-popup-confirm").addClass("overlay-alert-popup-show");
            				
            			} else {
            				$scope.hideProcessing();
            				$scope.enableButtons();
            			}
            			
            		} else {
            			$scope.hideProcessing();
            			$scope.getElement("#alert-popup-confirm p.requestSubmitMessage").html($scope.getReqSubmitMessage());
            			$scope.getElement("#alert-popup-confirm").addClass("overlay-alert-popup-show");
            		}
        		}
        		
        	} else {
        		$scope.enableButtons();
        		$scope.hideProcessing();
        	}
        }, 500);
    	
    };
    
    $scope.hideConfirmationPopup = function () {
    	$scope.enableButtons();
    	$scope.getElement("#alert-popup-confirm").removeClass("overlay-alert-popup-show");
    };
    
    $scope.saveRequestDataToDataBase = function () {
    	$scope.saveRequestDataToDB("Y");
    };
    
    $scope.openHomePage = function () {
    	$window.location.href = '../home';
    };
    
    $scope.showProcessing = function () {
    	if(!$scope.getElement("#loading-div").hasClass("overlay-alert-popup-show")) {
    		$scope.getElement("#loading-div").addClass("overlay-alert-popup-show");
    	}
    };
    
    $scope.hideProcessing = function () {
    	if($scope.getElement("#loading-div").hasClass("overlay-alert-popup-show")) {
    		$scope.getElement("#loading-div").removeClass("overlay-alert-popup-show");
    	}
    };
    
    $scope.hideMessagePopup = function () {
    	$scope.getElement('#message-popup').removeClass('overlay-alert-popup-show');
    };
    
    $scope.hideTempSavePopup = function () {
    	$scope.getElement('#temp-save-popup').removeClass('overlay-alert-popup-show');
    };
      
    $scope.getTravelIdByRequestNo = function(travelRequestNo) {
    	if(typeof(travelRequestNo) != 'undefined' && travelRequestNo != null && travelRequestNo != '' && travelRequestNo.indexOf("/") >= 0) {
    		return travelRequestNo.substring(travelRequestNo.lastIndexOf('/')+1);
    	} else {
    		return "0";
    	}
    };
    
    $scope.openRequestDetails = function() {
		
    	if(parseInt($scope.linkedTravelRequestId) > 0) {
    		window.open('details', '_blank');
    		
    		for(var i=0; i < $scope.linkedTravelRequestList.length; i++) {
    			if($scope.linkedTravelRequestList[i].travelId.toString() == $scope.linkedTravelRequestId) {
    				
    				$window.travelId 	= $scope.linkedTravelRequestId;
    				$window.travelType 	= $scope.linkedTravelRequestList[i].travelType;
    				break;
    			}
    		}
    	
    	} else {
    		$window.alert("Please select request number.");
    		$("#linkedTravelRequest").focus();
    	}
    };
      
}]);
