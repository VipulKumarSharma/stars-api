
/***************************** Main AngularJS Web Application **********************************/

var app = angular.module('app-dashboard', ["angucomplete-alt", "angularjs-dropdown-multiselect"] );

var appName = 'STARS';

app.service('apiCallService', function($http){
	
    this.getRequest = function(url, headerData){
    	
    	url = url + "?buildstamp=" + new Date().valueOf().toString();
    	
    	return $http.get(url, {headers: headerData})
		.then(function (response) {
			return response;
		
		}).catch(function(response) {
			console.error('Error occurred in apiCallService: getRequest() : ', response.status, response.data);
			return response;
		  
		}).finally(function() {
			
		});
    }
    
    this.postRequest = function(url, postData, headerData){
    	
    	url = url + "?buildstamp=" + new Date().valueOf().toString();
    	
    	return $http.post(url, postData, {headers: headerData})
		.then(function (response) {
			return response;
		
		}).catch(function(response) {
			console.error('Error occurred in apiCallService: postRequest() : ', response.status, response.data);
			return response;
		  
		}).finally(function() {
			
		});
    }
    
    this.putRequest = function(url, putData, headerData){
    	
    	url = url + "?buildstamp=" + new Date().valueOf().toString();
    	
    	return $http.put(url, putData, {headers: headerData})
		.then(function (response) {
			return response;
		
		}).catch(function(response) {
			console.error('Error occurred in apiCallService: putRequest() : ', response.status, response.data);
			return response;
		  
		}).finally(function() {
			
		});
    }
    
    this.deleteRequest = function(url, headerData){
    	
    	url = url + "?buildstamp=" + new Date().valueOf().toString();
    	
    	return $http.delete(url, {headers: headerData})
		.then(function (response) {
			return response;
		
		}).catch(function(response) {
			console.error('Error occurred in apiCallService: deleteRequest() : ', response.status, response.data);
			return response;
		  
		}).finally(function() {
			
		});
    }
    
});