function convertToDecimal(e) {
   var t = 0;
   var n = 0;
   var r = 2;
   var i = "";
   for (t = 0, j = 8; t < 9; t++, j--) {
      i = e.charAt(t);
      n += Math.pow(r, j) * parseInt(i)
   }
   return n
}

function convertToBoolean(e) {
   var t = e;
   var n;
   var r = 2;
   var i;
   var s = new Array(9);
   var o = s.length;
   var u;
   var a;
   var f = "";
   for (u = 0; t != 0; u++) {
      n = parseInt(t / r);
      i = t % r;
      s[u] = i;
      t = n
   }
   for (a = u; a < o; a++) s[a] = 0;
   s = s.reverse();
   for (a = 0; a < o; a++) f += s[a];
   return f
}

function encrypt(e) {
   var t = e.length;
   var n = "";
   var r = new Array;
   var i = 0;
   var s = Math.floor(Math.random() * 256) + 1;
   for (i = 0; i < t; i++) {
      r[i] = e.charCodeAt(i);
      if (r[i] >= 0 && r[i] < 64) r[i] += 128;
      else if (r[i] >= 64 && r[i] < 128) r[i] += 128;
      else if (r[i] >= 128 && r[i] < 192) r[i] -= 128;
      else if (r[i] >= 192 && r[i] < 256) r[i] -= 128;
      else;
      r[i] += s;
      n += convertToBoolean(r[i])
   }
   n += convertToBoolean(s);
   return n
}