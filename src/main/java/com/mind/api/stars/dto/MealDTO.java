package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MealDTO implements Serializable {
	
	private static final long serialVersionUID = -8889651054598286706L;
	
	private String mealId;
	private String mealName;
}
