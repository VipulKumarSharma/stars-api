package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class VisaDetailDTO  implements Serializable {

	private static final long serialVersionUID = 5427151554015414900L;
	private String country;
	private String comment;
	private String validFrom;
	private String validTo;
	private String stayDuration;
	private String validityFlag;
	
}
