package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class WorkflowApproverDTO implements Serializable {
	
	private static final long serialVersionUID = 6858957473997098179L;
	
	private String name;
	private String designationName;
	private String approveDate;
	private String approveStatus;
	private int originalApprover;
	
}
