package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IdentityProofDetailsDTO implements Serializable {
	
	
	private static final long serialVersionUID = -7330018920312208289L;
	private String identityId;
	private String identityNumber;

}
