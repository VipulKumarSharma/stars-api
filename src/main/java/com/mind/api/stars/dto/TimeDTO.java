package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TimeDTO implements Serializable {

	private static final long serialVersionUID = -1968992023337983454L;

	private int timeId;
	private String preferTime;
}
