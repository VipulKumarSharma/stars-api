package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransitHouseDTO implements Serializable {
	
	private static final long serialVersionUID = -6522180992042197457L;
	
	private String transitHouseId;
	private String transitHouseName;
	private String transitHouseAddress;
	private String areaCode;
	private String longitude;
	private String latitude;
	
}
