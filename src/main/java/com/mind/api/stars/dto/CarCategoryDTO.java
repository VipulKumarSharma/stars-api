package com.mind.api.stars.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CarCategoryDTO {

	private String carCategoryId;
	private String carCategoryName;
}
