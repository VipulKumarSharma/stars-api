package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PassportDetailDTO  implements Serializable {

	private static final long serialVersionUID = -1637891284636008060L;
	
	private String passportNo;
	private String nationality;
	private String issuePlace;
	private String ppIssueCountryId;
	private String issueDate;
	private String expiryDate;
	private String dateOfBirth;
	
}
