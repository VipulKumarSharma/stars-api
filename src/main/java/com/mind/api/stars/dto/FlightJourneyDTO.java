package com.mind.api.stars.dto;

import java.io.Serializable;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@ToString
public class FlightJourneyDTO implements Serializable {

	private static final long serialVersionUID = 5134709469208996701L;
	
	private long flightJourneyId = 0;
	private int journeyOrder = 0;
	private String journeyCode = "F1";
	private String source = "";
	private String destination = "";
	private int country	= 0;
	private String dateOfJourney = "";
	private int preferredTimeMode = -1;
	private int preferredTime = 0;
	private int travelClass = 31;
	private String preferredAirline = Constants.NO_PREFERENCE;
	private int preferredSeat = 30;
	private int stayType = 2;
	
	private String type = "";
	private String preferredTimeModeDesc= Constants.NO_PREFERENCE;
	private String preferredTimeDesc = "";
	private String travelClassDesc = Constants.NO_PREFERENCE;
	private String preferredSeatDesc = Constants.NO_PREFERENCE;
	private String stayTypeDesc	= Constants.TRANSIT_HOUSE;
	private String countryDesc	= "";
	
}
