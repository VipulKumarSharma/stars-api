package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AttachmentResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 4681867829633605581L;
	
	private String attachmentId;
	private byte[] fileBytes;
	private String fileName;
	private String fileSize;
	private String fileExtension;
	
}
