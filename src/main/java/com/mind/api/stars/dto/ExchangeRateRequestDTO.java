package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class ExchangeRateRequestDTO extends RequestDTO implements Serializable {	 
	 
	private static final long serialVersionUID = -2188976188845188158L;
	
	private String selectedCurrency;
	private String currentDate;
	
	private String days;
	private String previousTotal;
	private String baseCurrency;
	private String selectedTotal;
}
