package com.mind.api.stars.dto;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AttachmentRequestDTO  extends AppRequestDTO implements Serializable {
	
	private static final long serialVersionUID = -7609501300979180372L;
	
	private String attachmentId;
	private String uUID;
	private String fileName;
	private String fileExtension;
	private String fileSize;
	private MultipartFile file;

}
