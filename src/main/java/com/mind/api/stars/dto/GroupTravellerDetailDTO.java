package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class GroupTravellerDetailDTO implements Serializable {
	
	private static final long serialVersionUID = 8254866837779835762L;
	
	private GroupTravellerDTO groupUserDetails;
	private List<AdvanceDTO> advanceAmountDetails;
	
}
