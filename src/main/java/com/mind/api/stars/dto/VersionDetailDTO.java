package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class VersionDetailDTO implements Serializable{
	private static final long serialVersionUID = -7148244247918199490L;
	private String version;
}
