package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class OOOApproverDTO implements Serializable {
	
	private static final long serialVersionUID = 6173005461345849888L;

	private String oooExistsFlag;
	private String oooApproverText;
}
