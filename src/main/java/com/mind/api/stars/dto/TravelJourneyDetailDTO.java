package com.mind.api.stars.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

import com.mind.api.stars.utility.ExceptionLogger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@ToString
public class TravelJourneyDetailDTO implements Serializable, Comparable<TravelJourneyDetailDTO> {

	private static final long serialVersionUID = 5134709469208996701L;
	
	private String journey;
	private String departure;
	private String arrival;
	private String departureDate;
	private String preferredTime;
	private String preferredTimeDesc;
	private String travelMode;
	private String travelClass;
	private String prefferedTravel;
	private String prefferedSeat;
	private String stayType;
	private String otherInfo;
	
	@Override
        public int compareTo(TravelJourneyDetailDTO o) {
            boolean timeFlag = true;
            LocalTime thisTime = null;
            LocalTime otherTime = null;
	    try 
                {
                    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                        Date thisDate = dateFormat.parse(this.getDepartureDate());
                        Date otherDate = dateFormat.parse(o.getDepartureDate());
                        
                        if( this.getPreferredTime()==null || this.getPreferredTime().equalsIgnoreCase("No Preference") ||  o.getPreferredTime()==null || o.getPreferredTime().equalsIgnoreCase("No Preference")) {
                            timeFlag=false;
                        }
                        
                        if(timeFlag) {
                             thisTime = LocalTime.parse(this.getPreferredTime());
                             otherTime = LocalTime.parse(o.getPreferredTime());
                        }
                        if(thisDate.compareTo(otherDate) == 0)
                        {
                            if(timeFlag) {
                                if(thisTime.compareTo(otherTime) > 0)
                                        return 1;
                                else if(thisTime.compareTo(otherTime) == 0)
                                        return 0;
                                else
                                        return -1;
                             }
                            else {
                        	return 0;
                            }
                        }
                        else 
                        {       
                                if(thisDate.compareTo(otherDate) > 0)
                                        return 1;
                                else
                                        return -1;
                        }
                } catch (ParseException e) {
                    ExceptionLogger.logExceptionToDB(e);
                }
                catch (Exception e) {
                    ExceptionLogger.logExceptionToDB(e);
                }
                return -1;              
        }
	
	
}
