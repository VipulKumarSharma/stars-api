package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class DeleteJourneyDetailRequestDTO extends AppRequestDTO implements Serializable {
	
	private static final long serialVersionUID = 171864850535620905L;
	
	private Integer modeId;
	private String travelType;
	private String travelCarIds;
	private String travelAccommIds;
}
