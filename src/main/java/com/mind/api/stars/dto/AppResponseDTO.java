package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AppResponseDTO implements Serializable {

	private static final long serialVersionUID = -392293109518441406L;

	private String travelId;
}
