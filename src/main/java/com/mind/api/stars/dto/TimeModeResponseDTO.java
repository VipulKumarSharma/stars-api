package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TimeModeResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 4544291940667824740L;
	
	private String timeModeId;
	private String timeModeName;
}
