package com.mind.api.stars.dto;

import java.io.Serializable;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TrainJourneyDTO implements Serializable {

	private static final long serialVersionUID = -1784703994789188092L;

	private long trainJourneyId = 0;
	private int journeyOrder = 0;
	private String journeyCode = "T1";
	private String source = "";
	private String destination = "";
	private String dateOfJourney = "";
	private int preferredTimeMode = -1;
	private int preferredTime = 0;
	private int travelClass	= 30;
	private String preferredTrain = Constants.NO_PREFERENCE;
	private int preferredSeat = 3;
	private int ticketType = 0;
	private int stayType = 2;

	private String type = "";
	private String preferredTimeModeDesc = Constants.NO_PREFERENCE;
	private String preferredTimeDesc = "";
	private String travelClassDesc = Constants.NO_PREFERENCE;
	private String preferredSeatDesc = "Lower";
	private String ticketTypeDesc = "Normal";
	private String stayTypeDesc = Constants.TRANSIT_HOUSE;
	private boolean isTatkaalTicket;
	
}
