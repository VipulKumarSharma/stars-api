package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter 
public class PostCommentDTO extends RequestDTO implements Serializable {
	
	private static final long serialVersionUID = -7906286983570708688L;
	
	private String travelId;
	private String commentDesc;
	private String travelType;
	
}
