package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CountryDTO implements Serializable {

	private static final long serialVersionUID = -7148244247918199490L;
	
	private int countryId;
	private String countryCode;
	private String countryName;
	private String recordStatus;
}
