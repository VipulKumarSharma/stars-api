package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class UserDetailDTO implements Serializable {

	private static final long serialVersionUID = 6930787565399498529L;

	private Integer userId;
	private String userName;
	private String empCode;
	private String fullName;
	private String siteName;
	private String divisionName;
	private String firstName;
	private String lastName;
	private String designationName;
	private String departmentName;
	private String costCentreName;
	private String projectNo;
	private String gender;
	private String contactNo;
	private String dateOfBirth;
	private int mealPreferenceId = 0;
	private String mealPreferenceDesc = "";
	private String frequentFlyer;
	private boolean returnJourneyFlag;
	private String permanentAddress = "-";
	private String currentAddress = "-";
	private String email = "-";
	private String identityProofType = "-";
	private String identityProofNo = "-";
	private String amountRequired = "-";
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof UserDetailDTO){
			UserDetailDTO user = (UserDetailDTO) obj;
			if(this.getSiteName().equals(user.getSiteName())){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.userId;
	}
	
}
