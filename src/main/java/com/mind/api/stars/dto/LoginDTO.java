package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class LoginDTO extends RequestDTO implements Serializable {
	
	private static final long serialVersionUID = -3790976055279257770L;
	
	private String userName;
	private String encPassword;
	private String language;
	
}
