package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class UserInfoDTO implements Serializable {
	
	private static final long serialVersionUID = -996578695038487311L;
	
	private int userId;
	private int siteId;
	private int divId;
	private int travelAgencyId;
	private int currentYear;
	private int workflowNo;
	private int visaFlag;
	
	private String userName;
	private String encFlag;
	private String disabledTime;
	private String email;
	private String acceptanceFlag;
	private String relievingDateFlag;
	private String dummyUserFlag;
	private String password;
	private String ssoFlag;
	private String language;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String designationName;
	private String shortDesignationName;
	private String siteName;
	private String departmentName;
	private String startedNewStars;
	private String updateProfileFlag;
	private String approverLevel;
	private String gender;
	private String roles;
	private String showRequestForApproverFlag;
	private String domainName;
	private String winUserId;
	private String loginDateTime;
	private String lastLoginTime;
	private String lastLoginDuration;
	private String ipAddress;
	private String browser;
	private String tokenId;
	private String customSsoFlag;

}
