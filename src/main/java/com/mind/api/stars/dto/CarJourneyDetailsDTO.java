package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CarJourneyDetailsDTO implements Serializable {

	private static final long serialVersionUID = -3738658109357587303L;
	
	private int carClass = 0;
	private int carCategory	= 0;
	private String carClassDesc = "";
	private String carCategoryDesc = "";
	
	private List<CarJourneyDTO> carJourney;
	
	private String journeyType = "O";
	private String returnDate = "";
	private int returnTimeMode = -1;
	private int returnTime = 0;
	private String returnTimeDesc = "";
	private String returnTimeModeDesc = Constants.NO_PREFERENCE;
	private String carJourneyRemarks = "";
		
}
