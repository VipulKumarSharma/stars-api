package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CostCentreDTO implements Serializable {
	
	private static final long serialVersionUID = 567524673777207480L;
	
	private String costCentreId;
	private String costCentreName;

}
