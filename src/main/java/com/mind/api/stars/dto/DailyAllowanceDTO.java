package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper=true)
public class DailyAllowanceDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID = 888610561375110464L;
	
	private String empCode;
	private String amount;
	
}
