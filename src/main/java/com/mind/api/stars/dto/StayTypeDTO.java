package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StayTypeDTO implements Serializable {
	
	private static final long serialVersionUID = -2255773153996256951L;
	private String stayTypeId;
	private String stayTypeName;
}
