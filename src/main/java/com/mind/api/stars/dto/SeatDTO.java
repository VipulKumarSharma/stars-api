package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SeatDTO implements Serializable {
	
	private static final long serialVersionUID = 3842134239557512516L;
	
	private String seatId;
	private String seatName;
	
}
