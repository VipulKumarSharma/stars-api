package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserProfileInfoDTO implements Serializable {
	
	private static final long serialVersionUID = -8572984601654510086L;
	
	private String winUserId;
	private String domainName;
	private String userName;
	private String firstName;
	private String lastName;
	private String middleName;
	private String fullName;
	private String siteName;
	private String empCode;
	private String designation;
	private String department;
	private String email;
	private String gender;
	private String identityProofType;
	private String identityProofNo;
	private String salutation;
	private String dateOfBirth;
	private String contactNumber;
	private String ecnr;
	private String address;
	private String currentAddress;
	private String frequentFlightName;
	private String frequentFlightName1;
	private String frequentFlightName2;
	private String frequentFlightName3;
	private String frequentFlightName4;
	private String frequentFlightNumber;
	private String frequentFlightNumber1;
	private String frequentFlightNumber2;
	private String frequentFlightNumber3;
	private String frequentFlightNumber4;
	private String hotelName;
	private String hotelName1;
	private String hotelName2;
	private String hotelName3;
	private String hotelName4;
	private String hotelNumber;
	private String hotelNumber1;
	private String hotelNumber2;
	private String hotelNumber3;
	private String hotelNumber4;

}
