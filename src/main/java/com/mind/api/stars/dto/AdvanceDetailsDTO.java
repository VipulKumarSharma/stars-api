package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AdvanceDetailsDTO implements Serializable {

	private static final long serialVersionUID = 3593612526861966168L;

	private List<AdvanceDTO> advanceAmountDetails;
	private double totalAmountINR				= 0.0d;
	private double totalUSDPerDay				= 0.0d;
	private String advanceRemarks				= "";
	private String currencyDominationDetails	= "";
	
}
