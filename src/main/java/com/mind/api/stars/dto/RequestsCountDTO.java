package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class RequestsCountDTO implements Serializable {

	private static final long serialVersionUID = -5925019864252875815L;
	
	private int temporaryRequests		= 0;
	private int requestsInWorkflow		= 0;
	private int returnedRequests		= 0;
	private int rejectedRequests		= 0;
	private int cancelledRequests		= 0;
	private int approvedRequests		= 0;
	private int requestsToApprove		= 0;
	private int yearlyRequestCreated	= 0;
	private int yearlyRequestApproved	= 0;
	private String isApprover			= "N";
	
}
