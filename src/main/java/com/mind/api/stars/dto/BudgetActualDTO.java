package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class BudgetActualDTO implements Serializable {

	private static final long serialVersionUID = -6526630484738713274L;
	
	private double ytmBudget;
	private double ytdActual;
	private double availableBudget;
	private double estimate;
	private String expRemarks;

}
