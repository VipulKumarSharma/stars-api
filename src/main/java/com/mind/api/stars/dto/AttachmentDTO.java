package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AttachmentDTO implements Serializable {
	
	private static final long serialVersionUID = -4627299293800339313L;
	
	private String travelId;
	private String travelType;
	private String attachmentId;
	private String fileName;
	private String fileType;
	private String fileSize;
	private String uploadedBy;
	private String uploadedOn;

	private String originatorId;
	private String travelStatusId;
	private String docRef;
	private String fileLocation;
	private String uploadedByName;
	private String reqUuid;
	private String deleteEnableFlag;
	
}
