package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter 
public class FeedbackRequestDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID = -4929558135678763612L;
	
	private String likeMostFeedback;
	private String specificFeedback;
	private String lookAndFeelRating;
	private String creatingRequestRating;
	private String approvingRequestRating;
	private String systemSpeedRating;
	private String supportRating;
	private String overallRating;
	private String applicationId;
	
}
