package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SiteBasedUserRequestDTO extends AppRequestDTO implements Serializable {
	
	private static final long serialVersionUID = -7820719427351650117L;
	
	private String selectedSiteId;
}
