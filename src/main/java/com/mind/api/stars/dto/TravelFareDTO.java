package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TravelFareDTO implements Serializable {

	private static final long serialVersionUID = -922540679068510979L;

	private String currency = "";
	private String currencyCode = "";
	private double fareAmount = 0.0d;
	
}
