package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CityResponseDTO implements Serializable {

	private static final long serialVersionUID = 5717842113044239796L;

	private List<CityDTO> cities;
	private String lastSyncDate;
	private String syncMessage;
	
}
