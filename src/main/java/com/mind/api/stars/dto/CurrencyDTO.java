package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CurrencyDTO implements Serializable {
	
	private static final long serialVersionUID = -5037591136814215076L;
	
	private String currencyId;
	private String currencyName;
	private String currencyDesc;
	private String currencyCode;
	private String currencySymbol;
	private String baseCurrencyFlag;

}
