package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class BookingDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID = -8236801497595576488L;
	
	private String source;
	private String destination;
	private String paidAmount;
	private String airline;
	private String date;
	
}
