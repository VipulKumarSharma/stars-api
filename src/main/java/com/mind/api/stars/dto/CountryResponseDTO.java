package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CountryResponseDTO implements Serializable {

	private static final long serialVersionUID = -4453868460835496038L;
	
	private List<CountryDTO> countries;
	private String lastSyncDate;
	private String syncMessage;
}
