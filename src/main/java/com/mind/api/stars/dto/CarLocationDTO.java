package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CarLocationDTO implements Serializable {

	private static final long serialVersionUID = 2646196637277845937L;

	private String carLocationId;
	private String carLocationName;
}
