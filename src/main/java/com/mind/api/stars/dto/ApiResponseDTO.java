package com.mind.api.stars.dto;

import java.io.Serializable;

import com.mind.api.stars.common.Constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponseDTO implements Serializable {

	private static final long serialVersionUID = -7466011256955284332L;
	
	private String responseStatus = Constants.ERROR_CODE_500;
	private String responseMessage = Constants.FAILURE;
}
