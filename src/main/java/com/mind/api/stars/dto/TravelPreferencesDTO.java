package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelPreferencesDTO implements Serializable {

	private static final long serialVersionUID = 7334581976123155578L;
	
	List<TravelClassDTO> travelClassList;
	List<SeatDTO> seatList;
	List<MealDTO> mealList;
	List<TimeModeResponseDTO> timeModeList;

}
