package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class FlightJourneyDetailsDTO implements Serializable {

	private static final long serialVersionUID = 9142938561037278619L;
	
	private List<FlightJourneyDTO> flightJourney;
	private String journeyType = "O";
	private String returnDate = "";
	private int returnTimeMode = -1;
	private int returnTime = 0;
	private String returnTimeDesc = "";
	private String returnTimeModeDesc = Constants.NO_PREFERENCE;
	private String flightJourneyRemarks = "";
	
}
