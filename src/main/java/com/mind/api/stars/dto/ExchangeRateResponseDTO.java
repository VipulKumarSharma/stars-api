package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class ExchangeRateResponseDTO implements Serializable {	 
	 
	private static final long serialVersionUID = -4235022642330517724L;

	private String exchangeRate;
}
