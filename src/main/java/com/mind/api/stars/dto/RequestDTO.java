package com.mind.api.stars.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestDTO implements Serializable {

	private static final long serialVersionUID = 6357469507612741086L;
	
	//@JsonProperty(access = Access.WRITE_ONLY)
	@ApiModelProperty(value="Window's username")
	private String winUserId;
	
	@ApiModelProperty(value="Domain name")
	private String domainName;
	
	@ApiModelProperty(value="Source of request")
	private String requestSource;
	
	@ApiModelProperty(value="Client's device ID")
	private String deviceId;
	
	@ApiModelProperty(value="System's Name")
	private String systemName;
	
}
