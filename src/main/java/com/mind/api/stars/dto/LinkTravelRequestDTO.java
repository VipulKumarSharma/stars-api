package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class LinkTravelRequestDTO implements Serializable {

	private static final long serialVersionUID = 1323139853551232393L;
	
	private long travelId;
	private String travelType;
	private String travelReqNo;
	private String travelStatus;	
}
