package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class ApproverLevelWiseDTO implements Serializable {

	private static final long serialVersionUID = -332241845762660392L;

	private List<ApproverDetailDTO>  approverLevel1;
	private List<ApproverDetailDTO>  approverLevel2;
	private List<ApproverDetailDTO>  approverLevel3;
}
