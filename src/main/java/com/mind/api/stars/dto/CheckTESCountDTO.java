package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CheckTESCountDTO implements Serializable {
	
	private static final long serialVersionUID = -4203815083821923668L;
	
	private String message = "";
	private Boolean canCreateRequest = false;

}
