package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AdvanceDTO implements Serializable {

	private static final long serialVersionUID = 4296344461234538985L;
	
	private String currencyCode				= "";
	private double dailyAllowanceExpPerDay	= 0;
	private int dailyAllowanceTourDays		= 0;
	private double hotelChargesExpPerDay	= 0;
	private int hotelChargesTourDays		= 0;
	private double contingencies			= 0.0d;
	private double others					= 0.0d;
	private double total					= 0.0d;
	private double exchangeRateINR			= 0.0d;
	private double totalINR					= 0.0d;
	
	private String travellerName			= "";
	private double dailyAllowanceExpTotal	= 0;
	private double hotelChargesExpTotal		= 0;
	private String remarks					= "";
	private String cashBreakupRemarks		= "";
	
}
