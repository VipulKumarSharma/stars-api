package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class FareDTO implements Serializable {
	
	private static final long serialVersionUID = -1831442746979518565L;
	
	private boolean changeableAgainstFee;
	private boolean refundableAgainstFee;
	private int checkedBaggageCount;
	private String bahnCardNo;
	private String discount;
	private String fareClass;
	private String validityDate;
	private boolean isOnlineTicket;	
	
}
