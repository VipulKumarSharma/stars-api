package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AccommodationDetailsDTO implements Serializable {

	private static final long serialVersionUID = -5137083626542171774L;
	
	private List<AccommodationDTO> accommodations;
	private String accommodationRemarks;

}
