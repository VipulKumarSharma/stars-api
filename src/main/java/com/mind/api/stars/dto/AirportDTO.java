package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class AirportDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID 	= 5429610231622608553L;
	
	private String code;
	private String city;
	private String state;
	
}
