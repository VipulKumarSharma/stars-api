package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class ApproversRequestDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID = -3197369665942964757L;
	
	private int travellerId;
	private int siteId;
	
}
