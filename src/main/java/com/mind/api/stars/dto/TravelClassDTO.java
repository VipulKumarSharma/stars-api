package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelClassDTO implements Serializable {
	
	private String classId;
	private String eligibility;
}
