package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UnitLocationDTO implements Serializable {

	private static final long serialVersionUID = -3819014452007572977L;
	
	private String locationId;
	private String locationName;
	private String siteId;
	private String gstNo;
	private String lastLocationId;
}
