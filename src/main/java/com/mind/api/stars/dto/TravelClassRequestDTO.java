package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelClassRequestDTO extends AppRequestDTO implements Serializable {
	
	private static final long serialVersionUID = 7785378139076926020L;
	
	private Integer modeId;
	private String groupTravelFlag;
}
