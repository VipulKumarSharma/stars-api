package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NonMATASourceDetailsDTO implements Serializable {

	private static final long serialVersionUID = 3064637745956819678L;
	
	private String nonMATASourceFlag = "";
	private String airlineName = "";
	private String currency = "";
	private double amount = 0.0d;
	private String remarks = "";

}
