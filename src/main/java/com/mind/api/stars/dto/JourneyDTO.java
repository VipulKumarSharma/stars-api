package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class JourneyDTO implements Serializable {
	
	private static final long serialVersionUID = -1374588665359367835L;
	
	private long travelId;
	private int travellerId;
	private String travelType;
	private String requestNo;
	private String source;
	private String destination;
	private String departureDate;
	private String returnDate;
	private int daysLeft;
	private String departureTime;
	private String returnTime;
	
}
