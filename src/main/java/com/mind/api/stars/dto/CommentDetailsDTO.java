package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter 
@Setter 
@ToString

public class CommentDetailsDTO implements Serializable {

	private static final long serialVersionUID = 8233170412512335658L;
	
	private CommentRequestDetailsDTO requestDetails;
	private List<CommentDTO> commentsList;
	private List<CommentDTO> cancelledCommentsList;
	
}
