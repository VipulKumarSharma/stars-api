package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CityDTO implements Serializable {

	private static final long serialVersionUID = 771195474606711532L;

	private int cityId;
	private String cityCode;
	private String cityName;
	private int countryId;
	private String countryCode;
	private String countryName;
	private String recordStatus;
	
}
