package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class HotelDTO extends RequestDTO implements Serializable {

	private static final long serialVersionUID = -4119610222866791051L;

	private String name;
	private String star;
	private String rate;
		
}
