package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UnitDTO implements Serializable {
	
	private static final long serialVersionUID = -7919958526853956701L;
	
	private String siteId;
	private String siteName;
	private String isPrimarySite;

}
