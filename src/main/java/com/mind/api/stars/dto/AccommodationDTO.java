package com.mind.api.stars.dto;

import java.io.Serializable;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AccommodationDTO implements Serializable {

	private static final long serialVersionUID = 4296344461234538985L;
	
	private int accommodationId	= 0;
	private String journeyCode = "";
	
	private int stayType = 2;
	private String currency = "";
	private double budget = 0.00d;
	private String checkInDate = "";
	private String checkOutDate = "";
	private String place = "";
	private String address = "";
	private String reason = "";

	private String stayTypeDesc = Constants.TRANSIT_HOUSE;
	private int transitHouseId = 0;
	
	private String transitHouseLatitude = "";
	private String transitHouseLongitude = "";
	
}
