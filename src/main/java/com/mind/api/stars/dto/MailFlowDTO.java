package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MailFlowDTO implements Serializable {

	private static final long serialVersionUID = -5988218756510160958L;
	
	private String mailId;
	private String mailFrom;
	private String mailTo;
	private String mailCC;
	private String mailSubject;
	private String mailCreator;
	private String mailStatus;
	private String mailCreatedDate;
	private String mailSendDate;

}
