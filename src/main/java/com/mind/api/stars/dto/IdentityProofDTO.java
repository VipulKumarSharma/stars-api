package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IdentityProofDTO implements Serializable {
	
	private static final long serialVersionUID = 8542694812053201998L;
	
	private String identityId;
	private String identityName;
	
}
