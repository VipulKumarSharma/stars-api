package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString

public class CommentRequestDetailsDTO implements Serializable {
	
	private static final long serialVersionUID = 4421170405359029481L;
	
	private String travelId;
	private String requestNo;
	private String traveller;
	private String expenditure;
	private String originator;
	private String createdOn;
	private String source;
	private String destination;
	private String groupTravelFlag;
	private String travelAgencyId;
	private String travelStatusId;
	private String tkProviderFlag;
	private String originatorId;
	private String approveStatus;
	private String approveId;
	private String userId;
	private String saveUploadEnableFlag;
	
}




