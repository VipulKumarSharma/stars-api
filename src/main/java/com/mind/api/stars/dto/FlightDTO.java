package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class FlightDTO implements Serializable {
	
	private static final long serialVersionUID = 855063471502599597L;
	
	private String journeyType;
	private String destination;
	private String source;
	private String dateOfJourney;
	private String preferredTimeMode;
	private String preferredTime;
	private String travelClass;
	private String preferredSeat;
	private String remarks;
	private String preferredAirline;
	private boolean isRefundable;
	private String stayType;
		
}
