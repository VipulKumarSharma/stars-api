package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SeatRequestDTO extends AppRequestDTO implements Serializable {
	
	private static final long serialVersionUID = -5011575624029402349L;
	
	private Integer modeId;

}
