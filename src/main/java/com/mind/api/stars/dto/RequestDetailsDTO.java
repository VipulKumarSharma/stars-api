package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestDetailsDTO implements Serializable {

	private static final long serialVersionUID = 5121135901909294685L;

	private String parentId;
	private String travelId;
	private String travellerId;
	private String requestNo;
	private String itineraryDetails;
	private String dateOfJourney;
	private String journeyDates;
	private String journeyDuration;
	private String requestStatus;
	private String travelType;
	private String siteName;
	private String siteDesc;
	private String groupTravelFlag;
	private String reasonForTravel;
	
	private String travelAgencyId;
	
	private String createdBy;
	private String createdOn;
	private String cancelledBy;
	
	private String travelRequestId;
	private String traveller;
	private String expenditure;
	private String source;
	private String destination;
	
	private String flightTravelFlag;
	private String trainTravelFlag;
	private String carTravelFlag;
	private String accommTravelFlag;
	private String advanceForexFlag;
	private String commentFlag;
	private String attachmentFlag;
	
	private String journeyType;
	private String cancelUserRole;
	private List<JourneyDTO> journeyList;
	
}
