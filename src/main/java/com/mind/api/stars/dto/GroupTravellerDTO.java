package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class GroupTravellerDTO implements Serializable {

	private static final long serialVersionUID = -4688288728879184278L;

	private long gUserId;
	private int siteId;
	private int designationId;
	private int genderId;
	private int mealId;
	private int ecnrId;
	private int identityId;
	private int visaRequiredId;
	
	private String travellerName;
	private String empCode;
	private String email;
	private String firstName;
	private String lastName;
	private String siteName;
	private String designationName;
	private String dateOfBirth;
	private String genderDesc;
	private String mealDesc;
	private String passportNumber;
	private String nationality;
	private String dateOfIssue;
	private String expiryDate;
	private String placeOfIssue;
	private String ecnrDesc;
	private String identityNumber;
	private String identityName;
	private String mobileNumber;
	private String visaRequiredDesc;
	private String expRemarks;
	private String age;
	private String totalAmount;
	private String returnTravel;
	private String frequentFlyer;
	
}
