package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InsuranceDetailDTO  implements Serializable {

	private String comments;
	private String nominee;
	private String relation;
	
}
