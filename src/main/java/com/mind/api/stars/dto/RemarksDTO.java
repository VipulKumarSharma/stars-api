package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RemarksDTO implements Serializable {
	
	private static final long serialVersionUID = -6298732155017421079L;

	private String flightRemarks = "";
	private String carRemarks = "";
	private String trainRemarks = "";
	private String accommodationRemarks = "";
	private String advanceRemarks ="";
	private String cashBreakupRemarks = "";
}
