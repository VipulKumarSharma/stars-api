package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestSettingDTO implements Serializable {

	private static final long serialVersionUID = -5980651928262175582L;
	
	private String currency;
	private String apprLvl3Flag;
	private String siteHeadFlag;
	private String costCentreFlag;
	private String apprLvl3ForBmFlag;
	private String mandatoryAppFlag;
	private String budgetInputFlag;
	private String domLocalAgentFlag;
	private String intLocalAgentFlag;
	private String gstApplicableFlag;
	private String apprLvlAutoSelFlag;
	private String billingSiteFlag;
	private String workflowNo;
	private String cancelledReqFlag;
	private String defaultWFExistsFlag;

}
