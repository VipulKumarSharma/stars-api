package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CommentDTO implements Serializable {
	
	private static final long serialVersionUID = 7305306563232753920L;
	
	private String commentId;
	private String commentDesc;
	private String postedById;
	private String postedByName;
	private String postedOnDate;
	private String cancelId;
	private String deleteEnableFlag;
	
}
