package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestDataDTO implements Serializable {

	private String travelId;
	private String requestNo;
	private String requestStatus;
	
}
