package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class AdvanceDetailResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 7446852170301303795L;
	
	private String totalAmountINR;
	private String totalAmountUSD;

}
