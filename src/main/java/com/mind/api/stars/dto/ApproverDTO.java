package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApproverDTO implements Serializable {

	private static final long serialVersionUID = -5282500263092250664L;

	private Integer approverId;
	private Integer siteId;
	private String name;
	private String approverLevel;
	private String approverRole;
	private String designationName;
	private String siteName;
	private String approveStatus;
	private String approveDate;
	private String originalApprover;
	private String approveTime;
	private String onBehalfOfFlag;
	private String approveStatusDesc;
	private String transferFrom;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ApproverDTO) {
			return this.getApproverId().equals(((ApproverDTO) obj).getApproverId());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getApproverId();
	}

}
