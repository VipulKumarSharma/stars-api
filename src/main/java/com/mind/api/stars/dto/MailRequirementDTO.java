package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class MailRequirementDTO implements Serializable {

	private String strRequistionNumber;
	private String strRequistionCreatorName;
	private String strRequistionCreatorMail;
	private String strRequistionApproverName;
	private String strRequistionApproverEmail;
	private String strRequistionCreatedDate1;
	private String strUserNm;
	private String strTravelDate;
	private String strTravellerSex;
	private String strSiteName;
	private String strTravelAgencyTypeId;
	private String strMailToMATA;
	private String strGroupTravelFlag;
	private String expenditure;
	private String strBillingSite;
	private String strreasonFortravel;
	private String strbillingSiteid;
	private String strBillingClient;
	private String strSiteID;
	
}
