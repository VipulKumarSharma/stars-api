package com.mind.api.stars.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApproverDetailDTO implements Serializable {

	private static final long serialVersionUID = 4667580274893624016L;
	
	private String approverId;
	private String approverName;
	private String lastApproverId;

}
