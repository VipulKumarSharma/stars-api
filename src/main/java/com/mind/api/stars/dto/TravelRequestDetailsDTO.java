package com.mind.api.stars.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TravelRequestDetailsDTO implements Serializable {
	
	private static final long serialVersionUID = -7831178107878784984L;
	
	private String requestNo;
	
	private UserDetailDTO originator;
	private UserDetailDTO traveller;
	
	private String flightTravelFlag;
	private String trainTravelFlag;
	private String carTravelFlag;
	private String accommodationDetailsFlag;
	private String otherAccommodationsFlag;
	private String advanceForexFlag;
	private String budgetActualDetailsFlag;
	private String totalTravelFareDetailsFlag;
	
	private String groupTravelFlag;
	private String travelVisaFlag;
	
	private FlightJourneyDetailsDTO flightJourneyDetails;
	private TrainJourneyDetailsDTO trainJourneyDetails;
	private CarJourneyDetailsDTO carJourneyDetails;
	private AccommodationDetailsDTO accommodationDetails;
	private AdvanceDetailsDTO advanceForexDetails;
	private BudgetActualDTO budgetActualDetails;
	private TravelFareDTO totalTravelFareDetails;
	
	private PassportDetailDTO passport;
	private List<VisaDetailDTO> visa;
	private InsuranceDetailDTO insurance;
	
	private boolean visaRequired;
	private boolean insuranceRequired;
	
	private ApproverDTO billingApprover;
	private List<ApproverDTO> approvers;
	private List<ApproverDTO> approverLevels;
	
	private int statusId;
	private String divisionName;
	private String creationDate;
	private String updationDate;
	private String projectNo;
	private String returnStatus;
	
	private String siteLocation;
	private String gstNo;
	private String reasonForTravel;
	private String journeyDuration;
	private String reasonOfSkipApprover;
	private String travelAgencyId;
	private String linkedTravelRequest;
	private String advanceForex;
	
	private List<CommentDTO> approverCommentsList;
	private List<CommentDTO> approverCancellationCommentsList;
	
	private List<AttachmentDTO> attachmentsList;
	private List<TravelJourneyDetailDTO> travelJourneyDetailDTOList;
	
	private List<GroupTravellerDTO> groupTravellerDetailsList;
	
}
