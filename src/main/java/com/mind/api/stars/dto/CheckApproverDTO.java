package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CheckApproverDTO implements Serializable {
	
	private static final long serialVersionUID = -9028435763430299179L;
	
	String isExists = "N";
}
