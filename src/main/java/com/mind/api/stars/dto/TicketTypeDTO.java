package com.mind.api.stars.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TicketTypeDTO implements Serializable {
	
	private static final long serialVersionUID = 9189050021771135563L;

	private int ticketTypeId;
	private String ticketTypeName;
	private String recordStatus;
	
}
