package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum APICallAttributes {
	
	DOMAIN_NAME("domainName"),
	WIN_USER_ID("winUserId"),
	REQUEST_SOURCE("requestSource"),
	DEVICE_ID("deviceId"),
	SYSTEM_NAME("systemName"),
	SESSION_TIMEOUT("sessionTimeout"),
	USER_ID("userId"),
	AUTH_KEY("authKey"),
	TRAVELLER_ID("travellerId"),
	TRAVEL_AGENCY_ID("travelAgencyId"),
	SITE_ID("siteId"),
	BILLING_SITE_ID("billingSiteId"),
	TRAVEL_TYPE("travelType"),
	SELECTED_CURRENCY("selectedCurrency"),
	CURRENT_DATE("currentDate"),
	BASE_CURRENCY("baseCurrency"),
	SELECTED_TOTAL("selectedTotal"),
	DAYS("days"),
	PREVIOUS_TOTAL("previousTotal"),
	SYNC_DATE("syncDate"),
	MODE_ID("modeId"),
	GROUP_TRAVEL_FLAG("groupTravelFlag"),
	CAR_CLASS("carClass"),
	APPROVER_ID("approverId"),
	IDENTITY_ID("identityId"),
	
	TRAVEL_ID("travelId"),
	PAGE_TYPE("pageType"),
	COMMENT_ID("commentId"),
	TRAVEL_REQ_ID("travelReqId"),
	REQUEST_COUNT("requestCount"),
	REQUEST_NO("requestNo"),
	MAIL_ID("mailId"),
	ATTACHMENT_ID("attachmentId"),
	TRAVEL_CAR_IDS("travelCarIds"),
	TRAVEL_ACCOMM_IDS("travelAccommIds"),
	
	TRAVELLER_TYPE("travellerType"),
	FIRST_NAME("firstName"),
	G_USER_ID("gUserId"),
	REQUEST_TYPE("requestType"),
	CITY_AIRPORT_NAME("cityAirportName"),
	TOKEN_ID("tokenId"),
	APPROVER_LEVEL1("approverLevel1"),
	APPROVER_LEVEL2("approverLevel2"),
	APPROVER_LEVEL3("approverLevel3");
	
	@Getter @Setter
	String key;
	
	APICallAttributes(String key){
		this.key = key;
	}

	public static APICallAttributes fromKey(String key){
		for(APICallAttributes type : APICallAttributes.values()){
			if(key.equals(type.getKey())){
				return type;
			}
		}
		return null;
	}
}
