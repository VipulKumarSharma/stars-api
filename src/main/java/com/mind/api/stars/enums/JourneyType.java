package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum JourneyType {

	FORWARD("1", "FORWARD", ""),
	INTERMEDIATE("2", "INTERMEDIATE", ""),
	RETURNED("3", "RETURNED", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	JourneyType(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static JourneyType fromId(String id){
		for(JourneyType type : JourneyType.values()){
			if(id.equals(type.getId())){
				return type;
			}
		}
		return null;
	}

}
