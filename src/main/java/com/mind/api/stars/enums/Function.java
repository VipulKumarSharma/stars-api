package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum Function {

	GET_FIRST_LEVEL_APPROVER 		("FN_GET_APPROVER_LEVEL1", "TO GET APPROVER LEVEL 1"),
	GET_SECOND_LEVEL_APPROVER 		("FN_GET_APPROVER_LEVEL2", "TO GET APPROVER LEVEL 2"),
	GET_THIRD_LEVEL_APPROVER 		("FN_GET_APPROVER_LEVEL3", "TO GET APPROVER LEVEL 3"),
	GET_EXCHANGE_RATE				("FN_GET_EXCHANGE_RATE","TO GET EXCHANGE RATE"),
	GET_FOREX_TOTAL					("FN_CURRENCY_CONVERTOR","To GET TOTAL BY CURRENCY"),
	GET_USERROLE_UNITHEAD			("FN_GET_USERROLE_UNITHEAD", "Get User role and unit head"),
	GET_DEFAULT_WORKFLOW_APPROVERS	("FN_GET_DEFAULT_WORKFLOW_APPROVERS", "Get default workflow approvers"),
	CHECK_MULTIPLE_UNITHEADS_ON_SITE("FN_GET_SITE_MULTIPLE_UNITHEADS","Check whether multiple unitheads exists on site or not"),
	GET_TRAVELLER_ROLE				("FN_GET_TRAVELLER_ROLE_ID", "To Get traveller role"),
	APPROVER_IS_IN_DEFAULT_WORKFLOW	("FN_APPROVER_IS_IN_DEFAULT_WORKFLOW", "To check selected approver is exists in default workflow"),
	GET_COUNTRY_NAME_BY_ID		    ("FN_COUNTRY_NAME","Get country name by id.");
	
	@Getter @Setter
	private String name;

	@Getter @Setter
	private String description;
	
	Function(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
}
