package com.mind.api.stars.enums;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;

public enum PreferredTimeMode {

	NO_PREFERENCE("0", Constants.NO_PREFERENCE, ""),
	DEPARTURE_NOT_BEFORE("1", Constants.DEPARTURE_NOT_BEFORE, ""),
	ARRIVAL_UNTIL("", Constants.ARRIVAL_UNTIL, "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	PreferredTimeMode(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static PreferredTimeMode fromId(String id){
		for(PreferredTimeMode type : PreferredTimeMode.values()){
			if(type.getId().equals(id)){
				return type;
			}
		}
		return PreferredTimeMode.ARRIVAL_UNTIL;
	}

}
