package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum CarCategories {

	ECONOMY("1", "Economy (i.e. VW Polo)", ""),
	COMPACT("2", "Compact (i.e. VW Golf)", ""),
	COMPACT_STATION_WAGON("3", "Compact Station Wagon(i.e. Ford Focus Turnier)", ""),
	INTERMEDIATE("4", "Intermediate (i.e. VW Passat)", ""),
	INTERMEDIATE_STATION_WAGON ("5", "Intermediate Station Wagon (i.e. VW Passat Variant)", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	CarCategories(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static CarCategories fromId(String id){
		for(CarCategories type : CarCategories.values()){
			if(id.equals(type.getId())){
				return type;
			}
		}
		return null;
	}

}
