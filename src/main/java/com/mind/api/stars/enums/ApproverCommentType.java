package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum ApproverCommentType {

	APPROVE("1", "APPROVE", ""),
	CANCEL("2", "CANCEL", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	ApproverCommentType(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static ApproverCommentType fromId(String id){
		for(ApproverCommentType type : ApproverCommentType.values()){
			if(id.equals(type.getId())){
				return type;
			}
		}
		return null;
	}

}
