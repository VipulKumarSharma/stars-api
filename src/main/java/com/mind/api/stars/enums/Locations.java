package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum Locations {

	AIRPORT("1", "Airport", ""),
	TRAIN_STATION("2", "Train Station", ""),
	CITY("3", "City", ""),
	OFFICE("4", "Office", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	Locations(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}

	public static Locations fromId(String id){
		for(Locations type : Locations.values()){
			if(id.equals(type.getId())){
				return type;
			}
		}
		return null;
	}
	
}
