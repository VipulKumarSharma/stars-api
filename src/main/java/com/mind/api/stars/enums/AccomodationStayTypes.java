package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum AccomodationStayTypes {

	NA("-1", "N/A", ""),
	HOTEL("1", "Hotel", ""),
	TRANSIT_HOUSE("2", "Transit House", ""),
	OTHER("3", "Other", ""),
	NONE("", "-", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	AccomodationStayTypes(String id, String name, String displayKey) {
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static AccomodationStayTypes fromId(String id) {
		for(AccomodationStayTypes type : AccomodationStayTypes.values()){
			if(type.getId().equals(id)){
				return type;
			}
		}
		return AccomodationStayTypes.NONE;
	}

}
