package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum Procedure {

	SAVE_TRAVEL_REQUEST						("PROC_SAVE_T_TRAVEL_REQUEST_MATA_INDIA", "Procedure for saving request data"),
	MATAINDIA_REQUEST_DETAILS				("SPG_TRAVEL_DETAIL_MATA_INDIA", "Procedure to get request details"),
	MATAINDIA_USER_RECENT_REQUESTS			("SPG_MATAINDIA_USER_REQUEST_DETAILS", "Procedure to get user's recent travel requisitions"),
	CITY_MASTER_DATA						("PR_CITY_MASTER_DATA",	"Procedure to fetch city master data"),
	COUNTRY_MASTER_DATA						("PR_COUNTRY_MASTER_DATA","Procedure to fetch country master data"),
	TIME_MASTER_DATA						("PR_PREFER_TIME_MASTER", "Procedure to fetch time master data"),
	LAST_APPROVED_REQUESTS					("PR_LAST_APPROVED_REQUESTS", "Procedure to fetch user's last approved by all request"),
	ALL_REQUEST_COUNTS						("PR_ALL_REQUEST_COUNT", "Procedure to fetch counts for all types of requests"),
	REQUESTS_BY_TYPE						("PR_REQUEST_LIST_DETAILS_TEST1", "Procedure to fetch list of requests by type"),
	REQUEST_WORKFLOW						("PR_GET_REQUEST_WORKFLOW", "Procedure to fetch request workflow"),
	UPCOMING_JOURNEY_DETAILS				("PR_GET_UPCOMING_JOURNEY_DETAILS", "Procedure to fetch traveller's upcoming journey details"),
	REQUEST_FLAGS							("PR_CHECK_VALIDATION_FLAG_TYPE", "Procedure to fetch request flags"),
	FIRST_APPROVER_LEVEL					("PR_GET_APPROVER_LEVEL1", "Procedure to fetch First Approver Level"),
	SECOND_APPROVER_LEVEL					("PR_GET_APPROVER_LEVEL2", "Procedure to fetch Second Approver Level"),
	THIRD_APPROVER_LEVEL					("PR_GET_APPROVER_LEVEL3", "Procedure to fetch Third Approver Level"),
	DEFAULT_WORKFLOW_APPROVER				("PR_GET_DEFAULT_WORKFLOW_LIST","Procedure to fetch Default Workflow Approvers"),
	AUTHENTICATE_USER						("PR_AUTHENTICATE_USER", "Authenticate user while logging in"),
	AUTHENTICATED_USER_DETAILS				("PR_GET_AUTHENTICATE_USER_DETAILS", "Get details of authenticated user"),
	UPDATE_EUROPEAN_USER_DETAILS			("PROC_UPDATE_EUROPEAN_USER_DETAILS", "Update European user details"),
	CREATE_USER_LOGIN_LOGOUT_INFO			("PROC_LOGIN_INFO", "Create user's login info in database"),
	UNIT_MASTER_DATA						("PR_GET_UNIT_LIST","Procedure to fetch unit master data"),
	CURRENCY_MASTER_DATA					("PR_GET_CURRENCY_LIST","Procedure to fetch currency master data"),
	PREFFERED_SEAT_LIST						("PR_GET_SEAT_LIST", "Procedure to feat Seat List"),
	USER_LIST_BY_SITE						("PR_GET_USER_LIST","Procedure to fetch User list based on Unit"),
	TRAVEL_CLASS_LIST						("PR_GET_TRAVEL_CLASS_LIST","Procedure to fetch Travel Class List"),
	TIME_MODE_LIST							("PR_GET_TIME_MODE_LIST","Procedure to fetch Time Mode List"),
	UNIT_LOCATION_LIST						("PR_GET_SITE_LOCATION_LIST","Procedure to fetch Unit Location List"),
	BILLING_UNIT_LIST						("PR_GET_BILLING_SITE_LIST","Procedure to fetch Billing Unit List"),
	BILLING_APPROVER_LIST					("PR_GET_BILLING_APPROVER_LIST","Procedure to fetch Billing Approver List"),
	MEAL_LIST								("PR_GET_MEAL_LIST","Procedure to fetch Meal List"),
	COST_CENTRE_LIST						("PR_GET_COST_CENTRE_LIST","Procedure to fetch Cost Centre List"),
	CAR_CATEGORY_LIST						("PR_GET_CAR_CATEGORY_LIST","Procedure to fetch Car Category List"),
	DELETE_SSO_HISTORY_ENTRY				("SPG_SSO_HISTORY", "Procedure to remove TID & SID entry from SSO_HISTORY table"),
	CHECK_TES_COUNT							("PR_CHECK_TES_COUNT", "Procedure to check pending TES count"),
	IDENTITY_PROOF_LIST						("PR_GET_IDENTITY_PROOF_LIST", "Procedure to get identity proof list"),
	IDENTITY_PROOF_DETAILS					("PR_GET_IDENTITY_PROOF_DETAILS","Procedure to get identity proof details"),
	PASSPORT_DETAILS						("PR_GET_PASSPORT_DETAILS","Procedure to get passport details"),
	USER_DETAILS							("PR_GET_USER_DETAILS","Procedure to get user details"),
	TRANSIT_HOUSE_LIST						("PR_GET_TRANSIT_HOUSE_LIST","Procedure to get transit house list"),
	STAY_TYPE_LIST							("PR_GET_STAY_TYPE_LIST","Procedure to get stay type list"),
	REQUESTS_COMMENT_DETAILS				("PR_GET_TRAVEL_REQ_COMMENTS_CANCEL_DETAIL", "Procedure to comment details of request."),
	TRAVEL_REQ_COMMENT_DELETE 				("PR_DELETE_TRAVEL_REQ_COMMENTS", "Procedure to delete comment."),
	POST_REQUEST_COMMENT					("PROC_TRAVEL_REQ_ADDCOMMENTS","Post Request Comment"),
	POST_REQUEST_CANCEL_COMMENT_MATA		("PROC_TRAVEL_REQ_CANCELATION","Post Request Cancel Comment for User Role Mata"),
	POST_REQUEST_CANCEL_COMMENT				("PROC_TRAVEL_REQ_CANCELATION_BY_ORIGINATOR","Post Request Cancel Comment"),
	PROC_T_GROUP_USERINFO					("PROC_T_GROUP_USERINFO","PROCEDURE WILL BE USED TO INSERT & UPDATE  GROUP TRAVEL USER INFORMATION"),
	PROC_DELETE_TRAVEL_REQUISITION			("PROC_DELETE_TRAVEL_REQUISITION","PROCEDURE WILL BE USED TO INSERT TRAVEL DELETED"),
	PROC_UNAUTH_ACCESS_LOG					("PROC_UNAUTH_ACCESS_LOG","Procedure to log unauthorised access"),
	PROC_TRAVEL_REQ_ADDCOMMENTS				("PROC_TRAVEL_REQ_ADDCOMMENTS","Procedure used to add comments on travel requests"),
	GET_TRAVEL_REQ_MAILS_LIST				("PR_GET_TRAVEL_REQ_MAILS_LIST","Procedure used to get mail flow list"),
	GET_TRAVEL_REQ_MAIL_DETAIL			    ("PR_GET_TRAVEL_REQ_MAIL_DETAIL", "Get Travel Request Mail Detail"),
	GET_CAR_LOCATION_LIST					("PR_GET_CAR_LOCATION_LIST", "Procedure to get car location list"),
	REQUESTS_ATTACHMENT_DETAILS				("PR_GET_TRAVEL_REQ_ATTACHMENT_DETAIL", "Procedure to get attachment details of request."),
	TRAVEL_REQ_ATTACHMENT_DELETE 			("PROC_SANCTIONDELETEATTACHMENTS", "Procedure to delete request attachment."),
	DELETE_TRAVEL_JOURNEY_DETAILS			("PR_DELETE_TRAVEL_JOURNEY_DETAILS", "Procedure to delete request journey details"),
	PROC_GET_REQUEST_DETAILS 				("PROC_GET_REQUEST_DETAILS","Get all details of request to edit"),
	PROC_GET_GROUP_USER_DETAILS_LIST		("PR_GET_GROUP_GUEST_USERS_LIST","Get group guest user details list for selection"),
	PROC_GET_GROUP_USER_DETAILS   			("PR_GET_GROUP_GUEST_USER_DETAILS","Get group guest user details for auto fill"),
	PROC_GET_GROUP_TRAVELLERS_DETAILS_LIST	("PR_GET_GROUP_GUEST_TRAVELLER_DETAILS","Get group guest travellers details list"),
	PROC_GET_TRAVEL_EXPENDITURE_DETAILS		("SPG_GET_TRAVEL_EXPENDITURE_DETAILS","Get travel expenditure details"),
	TRAVELLERS_DETAILS_DELETE				("PR_DELETE_T_GROUP_USERINFO","Delete traveller request"),
	PROC_REQUISITION_MAIL_ADD			    ("PROC_REQUISITION_MAIL_ADD","To add email in the database"),
	REQUEST_RESP_DATA						("REQUEST_RESP_DATA","To Insert request response data"),
	PROC_UPDATE_REQUEST_RESP_DATA			("PROC_UPDATE_REQUEST_RESP_DATA","To update request response data"),
	PROC_MRS_ACCOMMODATION					("PROC_MRS_ACCOMMODATION","To Synchronization of MRS results"),
	POST_FEEDBACK							("PR_FEEDBACK_DETAIL","Post Feedback"),
	PROC_GET_TRAVELLER_CANCELLED_REQ		("PR_GET_USER_CANCELLED_REQ","Get traveller's cancelled request"),
	PROC_GET_TICKET_TYPE			    	("PR_GET_TICKET_TYPE_LIST","Get ticket type."),
	PROC_GET_CITY_AIRPORT_LIST			   	("PR_GET_CITY_AIRPORT_LIST","Get city/airport list."),
	SPG_ERP_TRAVEL_REQUISITION_XML			("SPG_ERP_TRAVEL_REQUISITION_XML","Procedure will be used in ERP Mata Client"),
	SAVE_GROUP_TRAVEL_REQUEST				("PROC_SAVE_GROUP_REQUEST", "Procedure for saving group travel request data"),
	CHECKED_OOO_APPROVER					("PR_CHECKED_OOO_APPROVER", "Procedure to check out of office approver"),
	PROC_TRAVEL_ATTACHMENT					("PROC_SPG_TRAVEL_ATTACHMENTS","Procedure for attachments"),
	PROC_GET_TRAVEL_ATTACHMENTS				("PR_GET_TRAVEL_ATTACHMENTS_DETAIL","Procedure for get travel attachments");
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String description;

	Procedure(String name, String description) {
		this.name = name;
		this.description = description;
	}

}