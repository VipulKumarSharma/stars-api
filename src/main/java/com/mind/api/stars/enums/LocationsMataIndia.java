package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum LocationsMataIndia {

	TRANSIT_HOUSE("1", "Transit House", ""),
	AIRPORT("2", "Airport", ""),
	HOTEL("3", "Hotel", ""),
	RAILWAY_STATION("4", "Railway Station", ""),
	OFFICE("5", "Office", ""),
	RESIDENCE("6", "Residence", ""),
	OTHERS("7", "Others", ""),
	NONE("", "-", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	LocationsMataIndia(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}

	public static LocationsMataIndia fromId(String id){
		for(LocationsMataIndia type : LocationsMataIndia.values()){
			if(type.getId().equals(id)){
				return type;
			}
		}
		return LocationsMataIndia.NONE;
	}
	
}
