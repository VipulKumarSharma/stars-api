package com.mind.api.stars.enums;

import lombok.Getter;
import lombok.Setter;

public enum TrainSeatPreferredTypes {

	WINDOW_GROSSRAUM_WITH_TABLE("22", "Window-Grossraum-With Table", ""),
	AISLE_GROSSRAUM_WITH_TABLE("23", "Aisle-Grossraum-With Table", ""),
	WINDOW_GROSSRAUM_WITHOUT_TABLE("24", "Window-Grossraum-Without Table", ""),
	AISLE_GROSSRAUM_WITHOUT_TABLE("25", "Aisle-Grossraum-Without Table", ""),
	WINDOW_ABTEIL_WITH_TABLE("26", "Window-Abteil-With Table", ""),
	AISLE_ABTEIL_WITH_TABLE("27", "Aisle-Abteil-With Table", ""),
	WINDOW_ABTEIL_WITHOUT_TABLE("28", "Window-Abteil-Without Table", ""),
	AISLE_ABTEIL_WITHOUT_TABLE("29", "Aisle-Abteil-Without Table", "");

	@Getter @Setter
	private String id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private String displayKey;

	TrainSeatPreferredTypes(String id, String name, String displayKey){
		this.id = id;
		this.name = name;
		this.displayKey = displayKey;
	}
	
	public static TrainSeatPreferredTypes fromName(String name){
		for(TrainSeatPreferredTypes type : TrainSeatPreferredTypes.values()){
			if(name.equals(type.getName())){
				return type;
			}
		}
		return null;
	}
	
	public static TrainSeatPreferredTypes fromId(String id){
		for(TrainSeatPreferredTypes type : TrainSeatPreferredTypes.values()){
			if(id.equals(type.getId())){
				return type;
			}
		}
		return null;
	}
	
}
