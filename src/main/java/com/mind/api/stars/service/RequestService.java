package com.mind.api.stars.service;

import java.util.List;

import com.mind.api.stars.dto.AdvanceDetailResponseDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.ApproverDetailDTO;
import com.mind.api.stars.dto.ApproverLevelWiseDTO;
import com.mind.api.stars.dto.AttachmentDetailsDTO;
import com.mind.api.stars.dto.CarCategoryDTO;
import com.mind.api.stars.dto.CarLocationDTO;
import com.mind.api.stars.dto.CheckApproverDTO;
import com.mind.api.stars.dto.CheckTESCountDTO;
import com.mind.api.stars.dto.CityAirportDTO;
import com.mind.api.stars.dto.CommentDetailsDTO;
import com.mind.api.stars.dto.CostCentreDTO;
import com.mind.api.stars.dto.DeleteJourneyDetailRequestDTO;
import com.mind.api.stars.dto.ExchangeRateRequestDTO;
import com.mind.api.stars.dto.ExchangeRateResponseDTO;
import com.mind.api.stars.dto.FeedbackRequestDTO;
import com.mind.api.stars.dto.GroupTravelRequestDTO;
import com.mind.api.stars.dto.GroupTravellerDTO;
import com.mind.api.stars.dto.GroupTravellerDetailDTO;
import com.mind.api.stars.dto.IdentityProofDTO;
import com.mind.api.stars.dto.IdentityProofDetailsDTO;
import com.mind.api.stars.dto.JourneyDTO;
import com.mind.api.stars.dto.LinkTravelRequestDTO;
import com.mind.api.stars.dto.MailDetailDTO;
import com.mind.api.stars.dto.MailFlowDTO;
import com.mind.api.stars.dto.MealDTO;
import com.mind.api.stars.dto.OOOApproverDTO;
import com.mind.api.stars.dto.PassportDetailDTO;
import com.mind.api.stars.dto.PostCommentDTO;
import com.mind.api.stars.dto.RequestDataDTO;
import com.mind.api.stars.dto.RequestDetailsDTO;
import com.mind.api.stars.dto.RequestSettingDTO;
import com.mind.api.stars.dto.RequestsCountDTO;
import com.mind.api.stars.dto.SeatDTO;
import com.mind.api.stars.dto.SeatRequestDTO;
import com.mind.api.stars.dto.SiteBasedUserRequestDTO;
import com.mind.api.stars.dto.StayTypeDTO;
import com.mind.api.stars.dto.TicketTypeDTO;
import com.mind.api.stars.dto.TimeModeResponseDTO;
import com.mind.api.stars.dto.TransitHouseDTO;
import com.mind.api.stars.dto.TravelClassDTO;
import com.mind.api.stars.dto.TravelClassRequestDTO;
import com.mind.api.stars.dto.TravelRequestDTO;
import com.mind.api.stars.dto.TravelRequestDetailsDTO;
import com.mind.api.stars.dto.UnitDTO;
import com.mind.api.stars.dto.UnitLocationDTO;
import com.mind.api.stars.dto.UserDTO;
import com.mind.api.stars.dto.UserProfileInfoDTO;
import com.mind.api.stars.dto.WorkflowApproverDTO;

public interface RequestService {
	
	RequestDataDTO saveRequestData(TravelRequestDTO travelRequestDTO);
	
	TravelRequestDetailsDTO getRequestDetails(AppRequestDTO appRequestDTO);
	
	List<RequestDetailsDTO> getRecentTravelRequestsData(AppRequestDTO appRequestDTO);
	
	List<TravelRequestDetailsDTO> getLastApprovedRequestDetailsList(AppRequestDTO appRequestDTO);
	
	RequestsCountDTO getRequestsCount(AppRequestDTO appRequestDTO);
	
	List<RequestDetailsDTO> getRequestsByType(AppRequestDTO appRequestDTO);
	
	List<WorkflowApproverDTO> getRequestWorkflow(AppRequestDTO appRequestDTO);
	
	List<JourneyDTO> getUpcomingJourneyDetails(AppRequestDTO appRequestDTO);
	
	RequestSettingDTO getRequestSetting(AppRequestDTO appRequestDTO);
	
	List<ApproverDetailDTO> getFirstLevelApprovers(AppRequestDTO appRequestDTO);
	
	List<ApproverDetailDTO> getSecondLevelApprovers(AppRequestDTO appRequestDTO);
	
	List<ApproverDetailDTO> getThirdLevelApprovers(AppRequestDTO appRequestDTO);
	
	List<WorkflowApproverDTO> getDefaultWorkflowApprover(AppRequestDTO appRequestDTO);
	
	ApproverLevelWiseDTO getApproverLevelWise(AppRequestDTO appRequestDTO);
	
	ExchangeRateResponseDTO getExchangeRate(ExchangeRateRequestDTO exchangeRateRequestDTO);
	
	AdvanceDetailResponseDTO getForexTotal(ExchangeRateRequestDTO exchangeRateRequestDTO);
	
	List<UserDTO> getSiteBasedUsers(SiteBasedUserRequestDTO siteBasedRequestDTO);
	
	List<SeatDTO> getSeatList(SeatRequestDTO seatRequestDTO);
	
	List<TravelClassDTO> getTravelClassList(TravelClassRequestDTO travelClassRequestDTO);

	List<TimeModeResponseDTO> getTimeModeList(AppRequestDTO appRequestDTO);
	
	List<UnitLocationDTO> getUnitLocationList(AppRequestDTO appRequestDTO);
	
	List<UnitDTO> getBillingUnitList(AppRequestDTO appRequestDTO);
	
	List<UserDTO> getBillingApproverList(AppRequestDTO appRequestDTO);
	
	List<MealDTO> getMealList(AppRequestDTO appRequestDTO);
	
	List<CostCentreDTO> getCostCentreList(AppRequestDTO appRequestDTO);
	
	List<CarCategoryDTO> getCarCategoryList(AppRequestDTO appRequestDTO);
	
	CheckTESCountDTO checkTESCount(AppRequestDTO appRequestDTO);
	
	CheckApproverDTO checkApproverExistsInDefaultWorkflow(AppRequestDTO appRequestDTO);
	
	List<IdentityProofDTO> getIdentityProofList(AppRequestDTO appRequestDTO);
	
	PassportDetailDTO getPassportDetails(AppRequestDTO appRequestDTO);
	
 	UserProfileInfoDTO getUserDetails(AppRequestDTO appRequestDTO);
 	
 	IdentityProofDetailsDTO getIdentityProofDetails(AppRequestDTO appRequestDTO);
 	
 	List<TransitHouseDTO> getTransitHouseList(AppRequestDTO appRequestDTO);
 	
 	List<StayTypeDTO> getStayTypeList(AppRequestDTO appRequestDTO);
 	
 	CommentDetailsDTO getRequestCommentsDetails(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String deleteRequestComment(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String saveRequestComment(PostCommentDTO postCommentDTO) throws Exception;
 	
 	String cancelTravelRequest(PostCommentDTO postCommentDTO) throws Exception;
 	
 	CommentDetailsDTO getRequestCancelDetails(AppRequestDTO appRequestDTO) throws Exception;
 	
 	AppRequestDTO deleteRequestData(AppRequestDTO requestDetailsDTO);
 	
 	List<MailFlowDTO> getMailFlowList(AppRequestDTO appRequestDTO);
 	
 	MailDetailDTO getMailDetail(AppRequestDTO appRequestDTO);
 	
 	List<CarLocationDTO> getCarLocationList(AppRequestDTO appRequestDTO);
 	
 	AttachmentDetailsDTO getRequestAttachmentDetails(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String deleteRequestAttachment(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String deleteRequestJourneyDetail(DeleteJourneyDetailRequestDTO deleteJourneyDetailRequestDTO) throws Exception;
 	
 	TravelRequestDTO getRequestDetailToEdit(AppRequestDTO appRequestDTO);
 	
 	List<GroupTravellerDTO> getGroupUserDetailsList(AppRequestDTO appRequestDTO) throws Exception;
 	
 	GroupTravellerDTO getGroupUserDetails(AppRequestDTO appRequestDTO) throws Exception;
 	
 	List<GroupTravellerDTO> getGroupTravellersDetailsList(AppRequestDTO appRequestDTO) throws Exception;
 	
 	GroupTravellerDetailDTO getGroupTravellerDetailsToEdit(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String deleteTravellerDetails(AppRequestDTO appRequestDTO) throws Exception;
 	
 	String saveFeedback(FeedbackRequestDTO feedbackRequestDTO) throws Exception;
 	
 	List<LinkTravelRequestDTO> getTravellerCancelledRequest(AppRequestDTO appRequestDTO) throws Exception;
 	
 	List<TicketTypeDTO> getTicketType(AppRequestDTO appRequestDTO) throws Exception;
 	
 	List<CityAirportDTO> getCityAirportList(AppRequestDTO appRequestDTO) throws Exception;
 	
 	RequestDataDTO saveGroupRequestData(GroupTravelRequestDTO groupTravelRequestDTO) throws Exception;
 	
 	OOOApproverDTO checkOOOApprover(AppRequestDTO appRequestDTO) throws Exception;
 	
}
