package com.mind.api.stars.service;

import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;

public interface AppService {

	UserInfoDTO getUserInfo(LoginDTO loginDTO);
	
	UserInfoDTO getAuthenticatedUserDetails(UserInfoDTO userInfoDTO);
	
	UserInfoDTO acceptPolicy(UserInfoDTO userInfoDTO);

	UserInfoDTO ssoLogin(String winUserId, String domain);
	
	void logout(int loggedInUserId, String ipAddress);
	
}
