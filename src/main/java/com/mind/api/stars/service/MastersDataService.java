package com.mind.api.stars.service;

import java.util.List;

import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.AttachmentRequestDTO;
import com.mind.api.stars.dto.AttachmentResponseDTO;
import com.mind.api.stars.dto.CityResponseDTO;
import com.mind.api.stars.dto.CountryResponseDTO;
import com.mind.api.stars.dto.CurrencyDTO;
import com.mind.api.stars.dto.TimeDTO;
import com.mind.api.stars.dto.UnitDTO;

public interface MastersDataService {
	
	CityResponseDTO getCityMasterData(AppRequestDTO appRequestDTO);
	
	CountryResponseDTO getCountryMasterData(AppRequestDTO appRequestDTO);
	
	List<TimeDTO> getTimeMasterData(AppRequestDTO appRequestDTO);
	
	List<UnitDTO> getUnitMasterData(AppRequestDTO appRequestDTO);
	
	List<CurrencyDTO> getCurrencyMasterData(AppRequestDTO appRequestDTO);
	
	AttachmentResponseDTO uploadAttachment(AttachmentRequestDTO attachmentRequestDTO);
	
	List<AttachmentResponseDTO> getAttachment(AttachmentRequestDTO attachmentRequestDTO);
}
