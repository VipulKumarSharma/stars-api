package com.mind.api.stars.service;

import java.util.List;

import com.mind.api.stars.dto.AirportDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.BookingDTO;
import com.mind.api.stars.dto.CityDTO;
import com.mind.api.stars.dto.CountryDTO;
import com.mind.api.stars.dto.DailyAllowanceDTO;
import com.mind.api.stars.dto.HotelDTO;
import com.mind.api.stars.dto.VersionDetailDTO;

public interface DataService {
	
	CityDTO getCityByCountry(AppRequestDTO appRequestDTO);
	
	List<AirportDTO> getAirportsInfo(AppRequestDTO appRequestDTO);
	
	List<HotelDTO> getHotelsInfo(AppRequestDTO appRequestDTO);
	
	DailyAllowanceDTO getdailyAllowanceAmount(AppRequestDTO appRequestDTO);
	
	List<BookingDTO> getPastBookings(AppRequestDTO appRequestDTO);
	
	List<BookingDTO> getFutureBookings(AppRequestDTO appRequestDTO);
	
	CountryDTO getCountryById(AppRequestDTO appRequestDTO) throws Exception;
	
	VersionDetailDTO getCurrentVersion(AppRequestDTO appRequestDTO);
	
}
