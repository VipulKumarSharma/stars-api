package com.mind.api.stars.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mind.api.stars.dao.DataDAO;
import com.mind.api.stars.dto.AirportDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.BookingDTO;
import com.mind.api.stars.dto.CityDTO;
import com.mind.api.stars.dto.CountryDTO;
import com.mind.api.stars.dto.DailyAllowanceDTO;
import com.mind.api.stars.dto.HotelDTO;
import com.mind.api.stars.dto.VersionDetailDTO;
import com.mind.api.stars.model.Airport;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Booking;
import com.mind.api.stars.model.City;
import com.mind.api.stars.model.Country;
import com.mind.api.stars.model.DailyAllowance;
import com.mind.api.stars.model.GroupTraveller;
import com.mind.api.stars.model.Hotel;
import com.mind.api.stars.model.VersionDetail;
import com.mind.api.stars.service.DataService;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DataServiceImpl implements DataService {

	@Resource
	private DataDAO dataDAO;

	
	@Override
	public CityDTO getCityByCountry(AppRequestDTO appRequestDTO) {
		CityDTO cityDetails	= new CityDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			City city = dataDAO.getCityByCountry(appRequest);
			
			BeanUtils.copyProperties(city, cityDetails);
		
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getCityByCountry() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return cityDetails;
	}

	
	@Override
	public List<AirportDTO> getAirportsInfo(AppRequestDTO appRequestDTO) {
		
		List<AirportDTO> airportsInfo = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Airport> airports = dataDAO.getAirportsInfo(appRequest);
			
			if (airports == null) {
	            return Collections.emptyList();
	        
			} else {
	        	airports.forEach(airportInfo -> {
		        	AirportDTO airportDTO = new AirportDTO();
		        	BeanUtils.copyProperties(airportInfo, airportDTO);
		        	
		        	airportsInfo.add(airportDTO);
		        });
	        }
		
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getAirportsInfo() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return airportsInfo;
	}
	
	
	@Override
	public List<HotelDTO> getHotelsInfo(AppRequestDTO appRequestDTO) {
		
		List<HotelDTO> hotelsInfo = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Hotel> hotels = dataDAO.getHotelsInfo(appRequest);
		
			if (hotels == null) {
	            return Collections.emptyList();
	        
			} else {
	        	hotels.forEach(hotelInfo -> {
					HotelDTO hotelDTO = new HotelDTO();
		        	BeanUtils.copyProperties(hotelInfo, hotelDTO);
		        	
		        	hotelsInfo.add(hotelDTO);
		        });
	        }
	        
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getHotelsInfo() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return hotelsInfo;
	}

	
	@Override
	public DailyAllowanceDTO getdailyAllowanceAmount(AppRequestDTO appRequestDTO) {
		
		DailyAllowanceDTO dailyAllowanceInfo = new DailyAllowanceDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			DailyAllowance dailyAllowance = dataDAO.getdailyAllowanceAmount(appRequest);
			
			BeanUtils.copyProperties(dailyAllowance, dailyAllowanceInfo);
		
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getdailyAllowanceAmount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return dailyAllowanceInfo;
	}

	
	@Override
	public List<BookingDTO> getPastBookings(AppRequestDTO appRequestDTO) {
		
		List<BookingDTO> pastBookingsInfo = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Booking> pastBookings = dataDAO.getPastBookings(appRequest);
			
			if (pastBookings == null) {
	            return Collections.emptyList();
	        
			} else {
	        	pastBookings.forEach(pastBooking -> {
					BookingDTO booking = new BookingDTO();
		        	BeanUtils.copyProperties(pastBooking, booking);
		        	
		        	pastBookingsInfo.add(booking);
		        });
	        }
		
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getPastBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return pastBookingsInfo;
	}

	
	@Override
	public List<BookingDTO> getFutureBookings(AppRequestDTO appRequestDTO) {
		
		List<BookingDTO> futureBookingsInfo = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Booking> futureBookings = dataDAO.getFutureBookings(appRequest);
		
			if (futureBookings == null) {
	            return Collections.emptyList();
	        
			} else {
	        	futureBookings.forEach(futureBooking -> {
					BookingDTO booking = new BookingDTO();
		        	
		        	futureBookingsInfo.add(booking);
		        });
	        }
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getFutureBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return futureBookingsInfo;
	}
	
	@Override
	public CountryDTO getCountryById(AppRequestDTO appRequestDTO) {
		
		CountryDTO countryInfo = new CountryDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			Country country = dataDAO.getCountryById(appRequest);
				
		    BeanUtils.copyProperties(country, countryInfo);
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getFutureBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return countryInfo;
	}


	@Override
	public VersionDetailDTO getCurrentVersion(AppRequestDTO appRequestDTO) {

		
		VersionDetailDTO versionDto = new VersionDetailDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			VersionDetail version = dataDAO.getVersion(appRequest);
				
		    BeanUtils.copyProperties(version, versionDto);
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getFutureBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return versionDto;
		}
	
}