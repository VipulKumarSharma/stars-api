package com.mind.api.stars.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.RequestDAO;
import com.mind.api.stars.dto.AccommodationDTO;
import com.mind.api.stars.dto.AccommodationDetailsDTO;
import com.mind.api.stars.dto.AdvanceDTO;
import com.mind.api.stars.dto.AdvanceDetailResponseDTO;
import com.mind.api.stars.dto.AdvanceDetailsDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.ApproverDTO;
import com.mind.api.stars.dto.ApproverDetailDTO;
import com.mind.api.stars.dto.ApproverLevelWiseDTO;
import com.mind.api.stars.dto.AttachmentDTO;
import com.mind.api.stars.dto.AttachmentDetailsDTO;
import com.mind.api.stars.dto.BudgetActualDTO;
import com.mind.api.stars.dto.CarCategoryDTO;
import com.mind.api.stars.dto.CarJourneyDTO;
import com.mind.api.stars.dto.CarJourneyDetailsDTO;
import com.mind.api.stars.dto.CarLocationDTO;
import com.mind.api.stars.dto.CheckApproverDTO;
import com.mind.api.stars.dto.CheckTESCountDTO;
import com.mind.api.stars.dto.CityAirportDTO;
import com.mind.api.stars.dto.CommentDTO;
import com.mind.api.stars.dto.CommentDetailsDTO;
import com.mind.api.stars.dto.CommentRequestDetailsDTO;
import com.mind.api.stars.dto.CostCentreDTO;
import com.mind.api.stars.dto.DeleteJourneyDetailRequestDTO;
import com.mind.api.stars.dto.ExchangeRateRequestDTO;
import com.mind.api.stars.dto.ExchangeRateResponseDTO;
import com.mind.api.stars.dto.FeedbackRequestDTO;
import com.mind.api.stars.dto.FlightJourneyDTO;
import com.mind.api.stars.dto.FlightJourneyDetailsDTO;
import com.mind.api.stars.dto.GroupTravelRequestDTO;
import com.mind.api.stars.dto.GroupTravellerDTO;
import com.mind.api.stars.dto.GroupTravellerDetailDTO;
import com.mind.api.stars.dto.IdentityProofDTO;
import com.mind.api.stars.dto.IdentityProofDetailsDTO;
import com.mind.api.stars.dto.InsuranceDetailDTO;
import com.mind.api.stars.dto.JourneyDTO;
import com.mind.api.stars.dto.LinkTravelRequestDTO;
import com.mind.api.stars.dto.MailDetailDTO;
import com.mind.api.stars.dto.MailFlowDTO;
import com.mind.api.stars.dto.MealDTO;
import com.mind.api.stars.dto.OOOApproverDTO;
import com.mind.api.stars.dto.PassportDetailDTO;
import com.mind.api.stars.dto.PostCommentDTO;
import com.mind.api.stars.dto.RemarksDTO;
import com.mind.api.stars.dto.RequestDataDTO;
import com.mind.api.stars.dto.RequestDetailsDTO;
import com.mind.api.stars.dto.RequestSettingDTO;
import com.mind.api.stars.dto.RequestsCountDTO;
import com.mind.api.stars.dto.SeatDTO;
import com.mind.api.stars.dto.SeatRequestDTO;
import com.mind.api.stars.dto.SiteBasedUserRequestDTO;
import com.mind.api.stars.dto.StayTypeDTO;
import com.mind.api.stars.dto.TicketTypeDTO;
import com.mind.api.stars.dto.TimeModeResponseDTO;
import com.mind.api.stars.dto.TrainJourneyDTO;
import com.mind.api.stars.dto.TrainJourneyDetailsDTO;
import com.mind.api.stars.dto.TransitHouseDTO;
import com.mind.api.stars.dto.TravelClassDTO;
import com.mind.api.stars.dto.TravelClassRequestDTO;
import com.mind.api.stars.dto.TravelFareDTO;
import com.mind.api.stars.dto.TravelJourneyDetailDTO;
import com.mind.api.stars.dto.TravelRequestDTO;
import com.mind.api.stars.dto.TravelRequestDetailsDTO;
import com.mind.api.stars.dto.UnitDTO;
import com.mind.api.stars.dto.UnitLocationDTO;
import com.mind.api.stars.dto.UserDTO;
import com.mind.api.stars.dto.UserDetailDTO;
import com.mind.api.stars.dto.UserProfileInfoDTO;
import com.mind.api.stars.dto.VisaDetailDTO;
import com.mind.api.stars.dto.WorkflowApproverDTO;
import com.mind.api.stars.enums.AccomodationStayTypes;
import com.mind.api.stars.enums.LocationsMataIndia;
import com.mind.api.stars.enums.PreferredTimeMode;
import com.mind.api.stars.model.AdvanceDetailResponse;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.ApproverDetail;
import com.mind.api.stars.model.ApproverLevelWise;
import com.mind.api.stars.model.CarCategory;
import com.mind.api.stars.model.CarLocation;
import com.mind.api.stars.model.CheckApprover;
import com.mind.api.stars.model.CheckTESCount;
import com.mind.api.stars.model.CityAirport;
import com.mind.api.stars.model.Comment;
import com.mind.api.stars.model.CostCentre;
import com.mind.api.stars.model.DeleteJourneyDetailRequest;
import com.mind.api.stars.model.ExchangeRateRequest;
import com.mind.api.stars.model.ExchangeRateResponse;
import com.mind.api.stars.model.FeedbackRequest;
import com.mind.api.stars.model.GroupTravelRequest;
import com.mind.api.stars.model.GroupTraveller;
import com.mind.api.stars.model.GroupTravellerDetail;
import com.mind.api.stars.model.IdentityProof;
import com.mind.api.stars.model.IdentityProofDetails;
import com.mind.api.stars.model.Journey;
import com.mind.api.stars.model.LinkTravelRequest;
import com.mind.api.stars.model.MailDetail;
import com.mind.api.stars.model.MailFlow;
import com.mind.api.stars.model.Meal;
import com.mind.api.stars.model.OOOApprover;
import com.mind.api.stars.model.PassportDetail;
import com.mind.api.stars.model.PostComment;
import com.mind.api.stars.model.RequestData;
import com.mind.api.stars.model.RequestDetails;
import com.mind.api.stars.model.RequestSetting;
import com.mind.api.stars.model.RequestsCount;
import com.mind.api.stars.model.Seat;
import com.mind.api.stars.model.SeatRequest;
import com.mind.api.stars.model.SiteBasedUserRequest;
import com.mind.api.stars.model.StayType;
import com.mind.api.stars.model.TicketType;
import com.mind.api.stars.model.TimeMode;
import com.mind.api.stars.model.TransitHouse;
import com.mind.api.stars.model.TravelClass;
import com.mind.api.stars.model.TravelClassRequest;
import com.mind.api.stars.model.TravelRequest;
import com.mind.api.stars.model.Unit;
import com.mind.api.stars.model.UnitLocation;
import com.mind.api.stars.model.User;
import com.mind.api.stars.model.UserProfileInfo;
import com.mind.api.stars.model.WorkflowApprover;
import com.mind.api.stars.service.RequestService;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestConvertor;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RequestServiceImpl implements RequestService {

	@Resource
	private RequestDAO requestDAO;
	
	@Override
	public RequestDataDTO saveRequestData(TravelRequestDTO travelRequestDTO) {
		
		RequestDataDTO requestDataDTO = new RequestDataDTO();
		
		try {
			TravelRequest travelRequest = new TravelRequest();
			travelRequest = (TravelRequest) RequestConvertor.convertObjectWithState(travelRequestDTO, travelRequest);
			
			RequestData requestData = requestDAO.saveRequestData(travelRequest);
			
			if(!AppUtility.isNull(requestData)) {
				BeanUtils.copyProperties(requestData, requestDataDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : saveRequestData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestDataDTO;
	}

	
	@Override
	public TravelRequestDetailsDTO getRequestDetails(AppRequestDTO appRequestDTO) {
		
		TravelRequestDetailsDTO travelRequestDetailsDTO = null;
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			Map<String, Object> requestDataMap = requestDAO.getRequestDetails(appRequest);
			
			travelRequestDetailsDTO = extractRequestDetails(requestDataMap, appRequestDTO.getTravelId(), appRequestDTO.getTravelType());
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return travelRequestDetailsDTO;
	}
	
	private TravelRequestDetailsDTO extractRequestDetails(Map<String, Object> requestDataMap, long travelId, String travelType) {
		TravelRequestDetailsDTO travelRequestDetailsDTO = null;
		
		if(requestDataMap != null && !requestDataMap.isEmpty()) {
			travelRequestDetailsDTO = new TravelRequestDetailsDTO();
			
			try {
				getRequestDetailsExistsData(travelRequestDetailsDTO, requestDataMap);
				
				RemarksDTO remarksDTO = getRequestDetailsData(travelRequestDetailsDTO, requestDataMap);
				getFlightJourneyDetailsData(travelRequestDetailsDTO, requestDataMap, remarksDTO);
				getTrainJourneyDetailsData(travelRequestDetailsDTO, requestDataMap, remarksDTO);
				getCarJourneyDetailsData(travelRequestDetailsDTO, requestDataMap, remarksDTO);
				getAccommodationDetailsData(travelRequestDetailsDTO, requestDataMap, remarksDTO);
				getAdvanceForexData(travelRequestDetailsDTO, requestDataMap);
				getGroupTravellerInfo(travelRequestDetailsDTO, requestDataMap);
				getBudgetActualDetails(travelRequestDetailsDTO, requestDataMap);
				getTotalTravelFareDetails(travelRequestDetailsDTO, requestDataMap);
				getRequestBillingInfo(travelRequestDetailsDTO, requestDataMap);
				getRequestApproverLevels(travelRequestDetailsDTO, requestDataMap);
				getRequestApproversData(travelRequestDetailsDTO, requestDataMap);
				getAttachmentsData(travelRequestDetailsDTO, requestDataMap);
				getVisaDetails(travelRequestDetailsDTO, requestDataMap);
				extractPassportDetails(travelRequestDetailsDTO, requestDataMap);
				getInsuranceDetails(travelRequestDetailsDTO, requestDataMap);
				getApproversCommentsData(travelRequestDetailsDTO, travelId, travelType, "APPROVE");
				getApproversCommentsData(travelRequestDetailsDTO, travelId, travelType, "CANCEL");
				
				getItineraryInformation(travelRequestDetailsDTO);
				
			} catch (Exception e) {
				log.error("Error occurred in RequestServiceImpl : extractRequestDetails() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return travelRequestDetailsDTO;
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestDetailsExistsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map 		= new HashMap<>(requestDataMap);
			Object requestDetailsExistsData = map.get("#result-set-1");
			
			if(requestDetailsExistsData instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetailsExistsData;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsExistsDataMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsExistsDataMap)) {
	        	    	if(requestInfo == null) {
				    		requestInfo = new TravelRequestDetailsDTO();
				    	}
	        	    	
	        	    	requestInfo.setFlightTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("FLIGHT_TRAVEL_FLAG")));
	        	    	requestInfo.setTrainTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAIN_TRAVEL_FLAG")));
	        	    	requestInfo.setCarTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("CAR_TRAVEL_FLAG")));
	        	    	requestInfo.setAccommodationDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ACCOMMODATION_TRAVEL_FLAG")));
	        	    	requestInfo.setOtherAccommodationsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("OTHER_ACCOMMODATION_FLAG")));
	        	    	requestInfo.setAdvanceForexFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ADVANCE_FLAG")));
	        	    	requestInfo.setTravelVisaFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_VISA_FLAG")));
	        	    	requestInfo.setBudgetActualDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SHOW_BUD_FLAG")));
	        	    	requestInfo.setTotalTravelFareDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SHOW_APP_LVL_3_FLAG")));
	        	    	requestInfo.setGroupTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("GROUP_TRAVEL_FLAG")));
	        	    }
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailsFromMap() while fetching request details existence data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private RemarksDTO getRequestDetailsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		RemarksDTO remarksDTO = null;
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object requestDetails = map.get("#result-set-2");
			
			
			if(requestDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetails;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsMap)) {
	        	    	requestInfo.setRequestNo(AppUtility.checkNullString(requestDetailsMap.get("TRAVEL_REQ_NO")));
						
	        	    	UserDetailDTO originator = new UserDetailDTO();
						originator.setFullName(AppUtility.checkNullString(requestDetailsMap.get("ORIGINATED_BY")));
						originator.setSiteName(AppUtility.checkNullString(requestDetailsMap.get("ORIG_UNIT")));
						originator.setDepartmentName(AppUtility.checkNullString(requestDetailsMap.get("ORIG_DEPT")));
						originator.setDivisionName(AppUtility.checkNullString(requestDetailsMap.get("ORIG_DIVISION")));
						originator.setDesignationName(AppUtility.checkNullString(requestDetailsMap.get("ORIG_DESIG")));
						requestInfo.setOriginator(originator);
						
						requestInfo.setCreationDate(AppUtility.checkNullString(requestDetailsMap.get("C_DATETIME")));
						requestInfo.setDivisionName(AppUtility.checkNullString(requestDetailsMap.get("DIVISION")));
						
						String projectNo = AppUtility.checkNullString(requestDetailsMap.get("PROJECTNO"));
						requestInfo.setProjectNo(AppUtility.isNull(projectNo) || "".equals(projectNo) ? "-" : projectNo);
						
						requestInfo.setReasonForTravel(!AppUtility.isBlankString(AppUtility.checkNullString(requestDetailsMap.get("REASON_FOR_TRAVEL"))) ? AppUtility.checkNullString(requestDetailsMap.get("REASON_FOR_TRAVEL")) : "-");
						
						requestInfo.setJourneyDuration(AppUtility.checkNullString(requestDetailsMap.get("JOURNEY_DURATION")));
						requestInfo.setLinkedTravelRequest((String) requestDetailsMap.get("LINK_TRAVEL_ID"));
						
						requestInfo.setInsuranceRequired(!"2".equalsIgnoreCase(AppUtility.checkNullString(requestDetailsMap.get("INSURANCE_REQUIRED"))));
						requestInfo.setVisaRequired(!"2".equalsIgnoreCase(AppUtility.checkNullString(requestDetailsMap.get("VISA_REQUIRED"))));
						
						UserDetailDTO traveller = new UserDetailDTO();
						traveller.setUserName(AppUtility.checkNullString(requestDetailsMap.get("CURRENT_TRAVELLER")));
						traveller.setSiteName(AppUtility.checkNullString(requestDetailsMap.get("UNIT")));
						traveller.setDepartmentName(AppUtility.checkNullString(requestDetailsMap.get("DEPT")));
						traveller.setIdentityProofType(AppUtility.checkNullString(requestDetailsMap.get("IDENTITY_NAME")));
						traveller.setIdentityProofNo(AppUtility.checkNullString(requestDetailsMap.get("IDENTITY_PROOFNO")));
						
						String costCentre = AppUtility.checkNullString(requestDetailsMap.get("COSTCENTRE"));
						traveller.setCostCentreName(AppUtility.isNull(costCentre) || "-".equals(costCentre) ? "Not Applicable" : costCentre.toUpperCase());
						
						String expenditure = AppUtility.checkNullString(requestDetailsMap.get("EXPENDITURE"));
						requestInfo.setAdvanceForex(!AppUtility.isNull(expenditure) || !"-".equals(expenditure) ? expenditure : "");
							
						traveller.setFirstName(AppUtility.checkNullString(requestDetailsMap.get("FIRST_NAME")));
						traveller.setLastName(AppUtility.checkNullString(requestDetailsMap.get("LAST_NAME")));
						traveller.setEmpCode(AppUtility.checkNullString(requestDetailsMap.get("EMP_CODE")));
						traveller.setDesignationName(AppUtility.checkNullString(requestDetailsMap.get("TRAVELLER_DESIG")));
						traveller.setContactNo(AppUtility.checkNullString(requestDetailsMap.get("CONTACT_NUMBER")));
						traveller.setGender(AppUtility.checkNullString(requestDetailsMap.get("GENDER")));
						traveller.setMealPreferenceDesc(AppUtility.checkNullString(requestDetailsMap.get("MEAL_NAME")));
						traveller.setDateOfBirth(AppUtility.checkNullString(requestDetailsMap.get("DOB")));
						traveller.setPermanentAddress(AppUtility.checkNullString(requestDetailsMap.get("ADDRESS")));
						traveller.setCurrentAddress(AppUtility.checkNullString(requestDetailsMap.get("CURRENT_ADDRESS")));
						traveller.setEmail(AppUtility.checkNullString(requestDetailsMap.get("EMAIL")));
						
						String travellerId = AppUtility.checkNullString(requestDetailsMap.get("TRAVELLER_ID"));
						traveller.setUserId(!AppUtility.isNull(travellerId) && !"-".equals(travellerId) ? AppUtility.convertStringToInt(travellerId) : 0);
						
						requestInfo.setTraveller(traveller);
						
						remarksDTO = new RemarksDTO();
						remarksDTO.setFlightRemarks(AppUtility.checkNullString(requestDetailsMap.get("FLIGHT_REMARKS")));
						remarksDTO.setTrainRemarks(AppUtility.checkNullString(requestDetailsMap.get("TRAIN_REMARKS")));
						remarksDTO.setCarRemarks(AppUtility.checkNullString(requestDetailsMap.get("CAR_REMARKS")));
						remarksDTO.setAccommodationRemarks(AppUtility.checkNullString(requestDetailsMap.get("ACCOMMODATION_REMARKS")));
						
						String siteLocation = AppUtility.checkNullString(requestDetailsMap.get("LOCATION_NAME"));
						requestInfo.setSiteLocation(AppUtility.isNull(siteLocation) || "".equals(siteLocation) ? "" : siteLocation);
						
						String gstNo = AppUtility.checkNullString(requestDetailsMap.get("GST_NO"));
						requestInfo.setGstNo(AppUtility.isNull(gstNo) || "".equals(gstNo) ? "-" : gstNo);
	        	    }
	        	}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailsData() while fetching request details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return remarksDTO;
	}
	
	@SuppressWarnings("unchecked")
	private void getFlightJourneyDetailsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap, RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object flightDetails = map.get("#result-set-3");
			
			if("Y".equals(requestInfo.getFlightTravelFlag()) && !AppUtility.isNull(flightDetails) && flightDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) flightDetails;
				
				FlightJourneyDetailsDTO flightJourneyDetails = new FlightJourneyDetailsDTO();
				List<FlightJourneyDTO> flightDetailsList = new ArrayList<>();
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> flightDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(flightDetailsMap) && (!AppUtility.isBlankString(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !AppUtility.isBlankString(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_TO)))) ) {
	        	    	FlightJourneyDTO flight = new FlightJourneyDTO();
	        	    	
						flight.setType(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.JOURNEY)));
						flight.setSource(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_FROM)));
						flight.setDestination(!AppUtility.isBlankString(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_TO))) ? AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_TO)) : "-");
						
						flight.setDateOfJourney(AppUtility.checkNullString(flightDetailsMap.get("TRAVEL_DATE")));
						
						flight.setPreferredTimeModeDesc(!AppUtility.isNull(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))) ? PreferredTimeMode.fromId(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))).getName() : Constants.ARRIVAL_UNTIL);
						flight.setPreferredTimeDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME)));
						
						flight.setTravelClassDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_CLASS)));
						flight.setPreferredSeatDesc(AppUtility.checkNullString(flightDetailsMap.get("SEAT_PREFFERED")));
						
						String preferredAirline = AppUtility.checkNullString(flightDetailsMap.get("pref_airline"));
						flight.setPreferredAirline(!AppUtility.isNull(preferredAirline) ? preferredAirline.toUpperCase() : "");
						
						String stayType = AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRANSIT_TYPE));
						flight.setStayTypeDesc(!AppUtility.isBlankString(stayType) ? AccomodationStayTypes.fromId(stayType).getName() : "-");
						
						flightDetailsList.add(flight);
	        	    }
	        	}
    	    	flightJourneyDetails.setFlightJourney(flightDetailsList);
				flightJourneyDetails.setFlightJourneyRemarks(!AppUtility.isBlankString(AppUtility.checkNullString(remarksDTO.getFlightRemarks())) ? AppUtility.checkNullString(remarksDTO.getFlightRemarks()) : "-");
				
	        	requestInfo.setFlightJourneyDetails(flightJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getFlightJourneyDetailsData() while fetching flight journey details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getTrainJourneyDetailsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap, RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object trainDetails = map.get("#result-set-4");
			
			if("Y".equals(requestInfo.getTrainTravelFlag()) && !AppUtility.isNull(trainDetails) && trainDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) trainDetails;
				
				TrainJourneyDetailsDTO trainJourneyDetails = new TrainJourneyDetailsDTO();
				List<TrainJourneyDTO> trainDetailsList = new ArrayList<>();
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> trainDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(trainDetailsMap) && (!AppUtility.isBlankString(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !AppUtility.isBlankString(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_TO)))) ) {
	        	    	TrainJourneyDTO train = new TrainJourneyDTO();
						
						train.setType(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.JOURNEY)));
						train.setSource(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_FROM)));
						train.setDestination(!AppUtility.isBlankString(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_TO))) ? AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_TO)) : "-");
						
						train.setDateOfJourney(AppUtility.checkNullString(trainDetailsMap.get("TRAVEL_DATE")));
						
						train.setPreferredTimeModeDesc(!AppUtility.isNull(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))) ? PreferredTimeMode.fromId(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))).getName() : Constants.ARRIVAL_UNTIL);
						train.setPreferredTimeDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME)));
						
						train.setTravelClassDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_CLASS)));
						train.setPreferredSeatDesc(AppUtility.checkNullString(trainDetailsMap.get("SEAT_PREFFERED")));
						
						String preferredTrain = AppUtility.checkNullString(trainDetailsMap.get("pref_airline"));
						train.setPreferredTrain(!AppUtility.isNull(preferredTrain) ? preferredTrain.toUpperCase() : "");
						
						train.setTatkaalTicket("Y".equalsIgnoreCase(AppUtility.checkNullString(trainDetailsMap.get("TATKAAL_FLAG"))) ? Boolean.TRUE : Boolean.FALSE);
						
						String stayType = AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRANSIT_TYPE));
						train.setStayTypeDesc(!AppUtility.isBlankString(stayType) ? AccomodationStayTypes.fromId(stayType).getName() : "-");
						
						trainDetailsList.add(train);
	        	    }
	        	}
    	    	trainJourneyDetails.setTrainJourney(trainDetailsList);
    	    	trainJourneyDetails.setTrainJourneyRemarks(!AppUtility.isBlankString(AppUtility.checkNullString(remarksDTO.getTrainRemarks())) ? AppUtility.checkNullString(remarksDTO.getTrainRemarks()) : "-");
	        	
    	    	requestInfo.setTrainJourneyDetails(trainJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getTrainJourneyDetailsData() while fetching train ourney details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getCarJourneyDetailsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap, RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object carDetails = map.get("#result-set-5");
			
			if("Y".equals(requestInfo.getCarTravelFlag()) && !AppUtility.isNull(carDetails) && carDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) carDetails;
				
				CarJourneyDetailsDTO carJourneyDetails = new CarJourneyDetailsDTO();
				List<CarJourneyDTO> carDetailsList = new ArrayList<>();
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> carDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(carDetailsMap) && (!AppUtility.isBlankString(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !AppUtility.isBlankString(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_TO)))) ) {
	        	    	CarJourneyDTO car = new CarJourneyDTO();
						
						car.setType(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.JOURNEY)));
						car.setSource(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_FROM)));
						car.setDestination(!AppUtility.isBlankString(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_TO))) ? AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_TO)) : "-");
						car.setDateOfJourney(AppUtility.checkNullString(carDetailsMap.get("START_DATE")));
						
						car.setPreferredTimeModeDesc(!AppUtility.isNull(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))) ? PreferredTimeMode.fromId(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))).getName() : Constants.ARRIVAL_UNTIL);
						car.setPreferredTimeDesc(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME)));
						
						car.setLocationDesc(!AppUtility.isBlankString(AppUtility.checkNullString(carDetailsMap.get("LOCATION"))) ? LocationsMataIndia.fromId(AppUtility.checkNullString(carDetailsMap.get("LOCATION"))).getName() : "-");
						car.setMobileNo(AppUtility.checkNullString(carDetailsMap.get("MOBILENO")));
						
						car.setCarClass(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_CLASS)));
						car.setCarCategory(!AppUtility.isNull(AppUtility.checkNullString(carDetailsMap.get("CLASS"))) ? AppUtility.checkNullString(carDetailsMap.get("CLASS")).toUpperCase() : "");
												
						carDetailsList.add(car);
	        	    }
	        	}
    	    	carJourneyDetails.setCarJourney(carDetailsList);
    	    	carJourneyDetails.setCarJourneyRemarks(!AppUtility.isBlankString(AppUtility.checkNullString(remarksDTO.getCarRemarks())) ? AppUtility.checkNullString(remarksDTO.getCarRemarks()) : "-");
    	    	
	        	requestInfo.setCarJourneyDetails(carJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getCarJourneyDetailsData() while fetching car journey details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getAccommodationDetailsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap, RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object accommodationDetailsData = map.get("#result-set-6");
			
			if("Y".equals(requestInfo.getAccommodationDetailsFlag()) && !AppUtility.isNull(accommodationDetailsData) && accommodationDetailsData instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) accommodationDetailsData;
				
				AccommodationDetailsDTO accommodationDetails = new AccommodationDetailsDTO();
				List<AccommodationDTO> accommodationDetailsList = new ArrayList<>();
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> accommodationDetailsMap = list.get(i);
	        		
	        		if(!AppUtility.isNull(accommodationDetailsMap)) {
	        			AccommodationDTO accommodation = new AccommodationDTO();
						
						String stayType = AppUtility.checkNullString(accommodationDetailsMap.get(ParamConstant.TRANSIT_TYPE));
						accommodation.setStayType(AppUtility.convertStringToInt(stayType));
						accommodation.setStayTypeDesc(!AppUtility.isBlankString(stayType) ? AccomodationStayTypes.fromId(stayType).getName() : "-");
						
						accommodation.setBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(accommodationDetailsMap.get("TRANSIT_BUDGET"))));
						accommodation.setCurrency(AppUtility.checkNullString(accommodationDetailsMap.get("BUDGET_CURRENCY_DESC")));
						accommodation.setCheckInDate(AppUtility.checkNullString(accommodationDetailsMap.get("CHECK_IN_DATE")));
						accommodation.setCheckOutDate(AppUtility.checkNullString(accommodationDetailsMap.get("CHECK_OUT_DATE")));
						accommodation.setPlace(AppUtility.checkNullString(accommodationDetailsMap.get("PLACE")));
						accommodation.setAddress(!AppUtility.isBlankString(AppUtility.checkNullString(accommodationDetailsMap.get("ACCOMMODATION_ADDRESS"))) ? AppUtility.checkNullString(accommodationDetailsMap.get("ACCOMMODATION_ADDRESS")) : "-");
						accommodation.setReason(!AppUtility.isBlankString(AppUtility.checkNullString(accommodationDetailsMap.get("REASON"))) ? AppUtility.checkNullString(accommodationDetailsMap.get("REASON")) : "-");
						
						accommodation.setTransitHouseId(AppUtility.convertStringToInt(AppUtility.checkNullString(accommodationDetailsMap.get("TRANSIT_HOUSE_ID"))));
						accommodation.setTransitHouseLatitude(AppUtility.checkNullString(accommodationDetailsMap.get("LATITUDE")));
						accommodation.setTransitHouseLongitude(AppUtility.checkNullString(accommodationDetailsMap.get("LONGITUDE")));
						
						accommodationDetailsList.add(accommodation);
	        		}
	        	}
    	    	accommodationDetails.setAccommodations(accommodationDetailsList);
    	    	accommodationDetails.setAccommodationRemarks(!AppUtility.isBlankString(remarksDTO.getAccommodationRemarks()) ? AppUtility.checkNullString(remarksDTO.getAccommodationRemarks()) : "-");
    	    	
	        	requestInfo.setAccommodationDetails(accommodationDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getAccommodationDetailsData() while fetching accommodation details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getAdvanceForexData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object advanceForexDetailsData = map.get("#result-set-7");
			
			if("Y".equals(requestInfo.getAdvanceForexFlag()) && !AppUtility.isNull(advanceForexDetailsData) && advanceForexDetailsData instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) advanceForexDetailsData;
				
				AdvanceDetailsDTO advanceForexDetails = new AdvanceDetailsDTO();
				List<AdvanceDTO> advanceForexDetailsList = new ArrayList<>();
				double totalAmountINR = 0;
				
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> advanceForexDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(advanceForexDetailsMap)) {
	        	    	AdvanceDTO advanceForex = new AdvanceDTO();
						
						advanceForex.setTravellerName(AppUtility.checkNullString(advanceForexDetailsMap.get("TRAVELLER_NAME")));
						advanceForex.setCurrencyCode(AppUtility.checkNullString(advanceForexDetailsMap.get("CURRENCY")));
						
						advanceForex.setDailyAllowanceExpPerDay(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("ENT_PER_DAY1"))));
						advanceForex.setDailyAllowanceTourDays(AppUtility.convertStringToInt(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_TOUR_DAYS1"))));
						advanceForex.setDailyAllowanceExpTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_EXP_ID1"))));
						
						advanceForex.setHotelChargesExpPerDay(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("ENT_PER_DAY2"))));
						advanceForex.setHotelChargesTourDays(AppUtility.convertStringToInt(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_TOUR_DAYS2"))));
						advanceForex.setHotelChargesExpTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_EXP_ID2"))));
						
						advanceForex.setContingencies(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_EXP_ID3"))));
						advanceForex.setOthers(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_EXP_ID4"))));
						
						advanceForex.setTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_EXP"))));
						advanceForex.setExchangeRateINR(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("EXCHANGE_RATE"))));
						advanceForex.setTotalINR(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL"))));
						
						advanceForex.setRemarks(!AppUtility.isBlankString(AppUtility.checkNullString(advanceForexDetailsMap.get("EXP_REMARK"))) ? AppUtility.checkNullString(advanceForexDetailsMap.get("EXP_REMARK")) : "-");
						
						advanceForex.setCashBreakupRemarks(!AppUtility.isBlankString(AppUtility.checkNullString(advanceForexDetailsMap.get("CASH_BREAKUP_REMARKS"))) ? AppUtility.checkNullString(advanceForexDetailsMap.get("CASH_BREAKUP_REMARKS")) : "-");
						
						advanceForexDetailsList.add(advanceForex);
						
						totalAmountINR = AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_ADVANCE_AMOUNT_INR")));
	        	    }
	        	}
    	    	advanceForexDetails.setAdvanceAmountDetails(advanceForexDetailsList);
    	    	advanceForexDetails.setTotalAmountINR(totalAmountINR);
    	    	
	        	requestInfo.setAdvanceForexDetails(advanceForexDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getAdvanceForexData() while fetching advance forex details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestBillingInfo(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object billingInfo = map.get("#result-set-8");
			
			if(!AppUtility.isNull(billingInfo) && billingInfo instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) billingInfo;
    	    	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> billingInfoMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(billingInfoMap)) {
	        	    	ApproverDTO billingApprover = new ApproverDTO();

						billingApprover.setSiteName(AppUtility.checkNullString(billingInfoMap.get("SITE_NAME")));
						billingApprover.setName(AppUtility.checkNullString(billingInfoMap.get("BILLING_CLIENT")));
						
						requestInfo.setBillingApprover(billingApprover);
	        	    }
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestBillingInfo() while fetching request billing information : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestApproverLevels(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object approverLevels = map.get("#result-set-9");
			
			if(!AppUtility.isNull(approverLevels) && approverLevels instanceof List) {
				List<Map<String, Object>> list 	= (List<Map<String, Object>>) approverLevels;
    	    	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> approverLevelsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(approverLevelsMap)) {
	        	    	List<ApproverDTO> approveList = new ArrayList<>();
	        	    	
	        	    	ApproverDTO approverLevel1 = new ApproverDTO();
	        	    	String appLevel1 = !AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("MANAGER_ID"))) ? AppUtility.checkNullString(approverLevelsMap.get("MANAGER_ID")) : "-";
						approverLevel1.setName("-".equals(appLevel1) ? "Not Applicable" : appLevel1);
						approveList.add(approverLevel1);
						
						ApproverDTO approverLevel2 = new ApproverDTO();
						String appLevel2 = !AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("HOD_ID"))) ? AppUtility.checkNullString(approverLevelsMap.get("HOD_ID")) : "-";
						approverLevel2.setName("-".equals(appLevel2) ? "Not Applicable" : appLevel2);
						approveList.add(approverLevel2);
						
						ApproverDTO approverLevel3 = new ApproverDTO();
						String appLevel3 = !AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("BOARD_MEMBER_ID"))) ? AppUtility.checkNullString(approverLevelsMap.get("BOARD_MEMBER_ID")) : "-";
						approverLevel3.setName("-".equals(appLevel3) ? "Not Applicable" : appLevel3);
						approveList.add(approverLevel3);
						
						requestInfo.setApproverLevels(approveList);
						
						String reasonForSkip = AppUtility.checkNullString(approverLevelsMap.get("REASON_FOR_SKIP"));
						requestInfo.setReasonOfSkipApprover(!AppUtility.isBlankString(reasonForSkip) && !"-".equals(reasonForSkip) ? reasonForSkip : "-");
	        	    }
	        	}
				
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestApproverLevels() while fetching approver levels data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestApproversData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object approvers = map.get("#result-set-10");
			
			if(!AppUtility.isNull(approvers) && approvers instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) approvers;
    	    	
				List<ApproverDTO> approveList = new ArrayList<>();
	        	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> approversMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(approversMap)) {
	        	    	ApproverDTO approver = new ApproverDTO();
	        	    	
						approver.setName(AppUtility.checkNullString(approversMap.get("APPROVER_NAME")));
						approver.setDesignationName(AppUtility.checkNullString(approversMap.get("DESIGN_DESC")));
						approver.setApproveStatus(AppUtility.checkNullString(approversMap.get("APPROVE_STATUS")));
						approver.setApproveDate(AppUtility.checkNullString(approversMap.get("APPROVE_DATE")));
						approver.setApproveTime(AppUtility.checkNullString(approversMap.get("HHMM")));
						approver.setOriginalApprover(AppUtility.checkNullString(approversMap.get("ORIGINAL_APPROVER")));
						
						approver.setSiteName(AppUtility.checkNullString(approversMap.get("SITE_NAME")));
						approver.setOnBehalfOfFlag(AppUtility.checkNullString(approversMap.get("ON_BEHALF_OF")));
						approver.setApproveStatusDesc(AppUtility.checkNullString(approversMap.get("APPROVE_STATUS_NAME")));
						
						approveList.add(approver);
	        	    }
				}
				requestInfo.setApprovers(approveList);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestApproversData() while fetching approvers data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getGroupTravellerInfo(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object groupTravellerDetails = map.get("#result-set-11");
			
			if("Y".equals(requestInfo.getGroupTravelFlag()) && !AppUtility.isNull(groupTravellerDetails) && groupTravellerDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) groupTravellerDetails;
    	    	
				List<GroupTravellerDTO> groupTravellerList = new ArrayList<>();
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> groupTravellerDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(groupTravellerDetailsMap)) {
	        	    	
	        	    	GroupTravellerDTO groupTraveller = new GroupTravellerDTO();
	        	    	
	        	    	groupTraveller.setTravellerName(AppUtility.checkNullString(groupTravellerDetailsMap.get("CURRENT_TRAVELLER")));
	        	    	groupTraveller.setFirstName(AppUtility.checkNullString(groupTravellerDetailsMap.get("FIRST_NAME")));
	        	    	groupTraveller.setLastName(AppUtility.checkNullString(groupTravellerDetailsMap.get("LAST_NAME")));
	        	    	groupTraveller.setDesignationName(AppUtility.checkNullString(groupTravellerDetailsMap.get("TRAVELLER_DESIG")));
	        	    	groupTraveller.setPassportNumber(AppUtility.checkNullString(groupTravellerDetailsMap.get("PASSPORT_NO")));
	        	    	groupTraveller.setPlaceOfIssue(AppUtility.checkNullString(groupTravellerDetailsMap.get("PLACE_ISSUE")));
	        	    	groupTraveller.setDateOfIssue(AppUtility.checkNullString(groupTravellerDetailsMap.get("DATE_ISSUE")));
	        	    	groupTraveller.setExpiryDate(AppUtility.checkNullString(groupTravellerDetailsMap.get("EXPIRY_DATE")));
	        	    	groupTraveller.setNationality(AppUtility.checkNullString(groupTravellerDetailsMap.get("NATIONALITY")));
	        	    	groupTraveller.setAge(AppUtility.checkNullString(groupTravellerDetailsMap.get("AGE")));
	        	    	groupTraveller.setDateOfBirth(AppUtility.checkNullString(groupTravellerDetailsMap.get("DOB")));
	        	    	groupTraveller.setGenderDesc(AppUtility.checkNullString(groupTravellerDetailsMap.get("GENDER")));
	        	    	groupTraveller.setVisaRequiredId(AppUtility.convertStringToInt(AppUtility.checkNullString(groupTravellerDetailsMap.get("VISA_REQUIRED"))));
	        	    	groupTraveller.setIdentityId(AppUtility.convertStringToInt(AppUtility.checkNullString(groupTravellerDetailsMap.get("IDENTITY_ID"))));
	        	    	groupTraveller.setIdentityName(AppUtility.checkNullString(groupTravellerDetailsMap.get("IDENTITY_NAME")));
	        	    	groupTraveller.setIdentityNumber(AppUtility.checkNullString(groupTravellerDetailsMap.get("IDENTITY_NO")));
	        	    	groupTraveller.setMealDesc(AppUtility.checkNullString(groupTravellerDetailsMap.get("MEAL_NAME")));
	        	    	groupTraveller.setTotalAmount(AppUtility.checkNullString(groupTravellerDetailsMap.get("TOTAL_AMOUNT")));
	        	    	groupTraveller.setExpRemarks(AppUtility.checkNullString(groupTravellerDetailsMap.get("EXP_REMARKS")));
	        	    	groupTraveller.setMobileNumber(AppUtility.checkNullString(groupTravellerDetailsMap.get("MOBILE_NO")));
	        	    	groupTraveller.setReturnTravel(AppUtility.checkNullString(groupTravellerDetailsMap.get("RETURN_TRAVEL")));
	        	    	groupTraveller.setFrequentFlyer(AppUtility.checkNullString(groupTravellerDetailsMap.get("FREQUENT_FLYER")));
	        	    	groupTraveller.setEmpCode(AppUtility.checkNullString(groupTravellerDetailsMap.get("G_EMP_CODE")));
	        	    	groupTraveller.setEmail(AppUtility.checkNullString(groupTravellerDetailsMap.get("G_EMAIL")));
						
	        	    	int ecnrId = AppUtility.convertStringToInt(AppUtility.checkNullString(groupTravellerDetailsMap.get("ECNR")));
	        	    	groupTraveller.setEcnrId(ecnrId);
	        	    	
						if(ecnrId == 1) {
							groupTraveller.setEcnrDesc("Yes");
						} else if(ecnrId == 2) {
							groupTraveller.setEcnrDesc("No");
						} else {
							groupTraveller.setEcnrDesc("N/A");
						}
						
	        	    	groupTravellerList.add(groupTraveller);
	        	    }
				}
				requestInfo.setGroupTravellerDetailsList(groupTravellerList);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getBudgetActualDetails() while fetching budget actual details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getBudgetActualDetails(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object budgetActualDetails = map.get("#result-set-12");
			
			if("Y".equals(requestInfo.getBudgetActualDetailsFlag()) && !AppUtility.isNull(budgetActualDetails) && budgetActualDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) budgetActualDetails;
    	    	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> budgetActualDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(budgetActualDetailsMap)) {
	        	    	BudgetActualDTO budgetActual = new BudgetActualDTO();

	        	    	budgetActual.setYtmBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(budgetActualDetailsMap.get("YTM_BUDGET"))));
						budgetActual.setYtdActual(AppUtility.convertStringToDouble(AppUtility.checkNullString(budgetActualDetailsMap.get("YTD_ACTUAL"))));
						budgetActual.setAvailableBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(budgetActualDetailsMap.get("AVAIL_BUDGET"))));
						budgetActual.setEstimate(AppUtility.convertStringToDouble(AppUtility.checkNullString(budgetActualDetailsMap.get("EST_EXP"))));						
						budgetActual.setExpRemarks(AppUtility.checkNullString(budgetActualDetailsMap.get("EXP_REMARKS")));
						
						requestInfo.setBudgetActualDetails(budgetActual);
	        	    }
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getBudgetActualDetails() while fetching budget actual details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getTotalTravelFareDetails(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object totalTravelFareDetails = map.get("#result-set-13");
			
			if("Y".equals(requestInfo.getTotalTravelFareDetailsFlag()) && !AppUtility.isNull(totalTravelFareDetails) && totalTravelFareDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) totalTravelFareDetails;
    	    	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> totalTravelFareDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(totalTravelFareDetailsMap)) {
	        	    	
	        	    	String currency = AppUtility.checkNullString(totalTravelFareDetailsMap.get("FARE_CURRENCY"));
	        	    	double fareAmount = AppUtility.convertStringToDouble(AppUtility.checkNullString(totalTravelFareDetailsMap.get("FARE_AMOUNT")));
						
	        	    	if(!AppUtility.isBlankString(currency) && fareAmount != 0) {
	        	    		TravelFareDTO travelFare = new TravelFareDTO();
	        	    		
							travelFare.setCurrencyCode(currency);
							travelFare.setFareAmount(fareAmount);
							
							requestInfo.setTotalTravelFareDetails(travelFare);
						}
	        	    }
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getTotalTravelFareDetails() while fetching total travel fare details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	private void getApproversCommentsData(TravelRequestDetailsDTO requestInfo, long travelId, String travelType, String commentsType) {
		
		try {
			List<Comment> approverCommentsList 			= requestDAO.getApproverComments(travelId, travelType, commentsType);
			List<CommentDTO> approverCommentsDTOList 	= new ArrayList<>();
			
			approverCommentsList.forEach(approverComment -> {
	        	CommentDTO approverCommentDTO = new CommentDTO();
	        	BeanUtils.copyProperties(approverComment, approverCommentDTO);
	        	
	        	approverCommentsDTOList.add(approverCommentDTO);
	        });
			
			if("APPROVE".equals(commentsType)) {
				requestInfo.setApproverCommentsList(approverCommentsDTOList);
				
			} else if("CANCEL".equals(commentsType)) {
				requestInfo.setApproverCancellationCommentsList(approverCommentsDTOList);
			}
		
		} catch(Exception e) {
			log.error("Error occurred in RequestServiceImpl : getApproversCommentsData() while fetching approvers comments data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	private void getItineraryInformation(TravelRequestDetailsDTO requestInfo) {
		
		try {
			TravelJourneyDetailDTO travelJourneyDetailDTO = null;
			List<TravelJourneyDetailDTO> travelJourneyDetailDTOList = new ArrayList<TravelJourneyDetailDTO>();
			
			if(requestInfo.getFlightJourneyDetails() != null) {
				
			}
			
			FlightJourneyDetailsDTO flightJourneyDetails = requestInfo.getFlightJourneyDetails();
			List<FlightJourneyDTO> flightDetailList = flightJourneyDetails==null?null:flightJourneyDetails.getFlightJourney();
			
			TrainJourneyDetailsDTO trainJourneyDetails = requestInfo.getTrainJourneyDetails();
			List<TrainJourneyDTO> trainJourneyList = trainJourneyDetails==null?null:trainJourneyDetails.getTrainJourney();
			
			CarJourneyDetailsDTO carJourneyDetails = requestInfo.getCarJourneyDetails();
			List<CarJourneyDTO> carDetailsList = carJourneyDetails==null?null:carJourneyDetails.getCarJourney();
			
			
			/**Flight Details*/
			if(flightDetailList  != null) {
                for(FlightJourneyDTO flightJourneyDetailsDTO : flightDetailList)
                {       
                    travelJourneyDetailDTO= new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToNA(flightJourneyDetailsDTO.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToNA(flightJourneyDetailsDTO.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToNA(flightJourneyDetailsDTO.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToNA(flightJourneyDetailsDTO.getDateOfJourney()));
                    travelJourneyDetailDTO.setPreferredTime(flightJourneyDetailsDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredTimeModeDesc()) + " " + flightJourneyDetailsDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setTravelMode(Constants.FLIGHT);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToNA(flightJourneyDetailsDTO.getTravelClassDesc()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToNA(flightJourneyDetailsDTO.getPreferredAirline()));
                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToNA(flightJourneyDetailsDTO.getPreferredSeatDesc()));
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToNA(flightJourneyDetailsDTO.getStayTypeDesc()));
                    travelJourneyDetailDTO.setOtherInfo("-");
                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);
                    
                    
                }
			}
                /**Flight Details Ends Here**/
                /**Train Details*/
	        if(trainJourneyList != null) {  
	                for(TrainJourneyDTO trainJourneyDTO : trainJourneyList)
	                {       
	                    
	                    travelJourneyDetailDTO= new TravelJourneyDetailDTO();
	                    travelJourneyDetailDTO.setJourney(AppUtility.blankToNA(trainJourneyDTO.getType()));
	                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToNA(trainJourneyDTO.getSource()));
	                    travelJourneyDetailDTO.setArrival(AppUtility.blankToNA(trainJourneyDTO.getDestination()));
	                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToNA(trainJourneyDTO.getDateOfJourney()));
	                    travelJourneyDetailDTO.setPreferredTime(trainJourneyDTO.getPreferredTimeDesc());
	                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTimeModeDesc()) + " " + trainJourneyDTO.getPreferredTimeDesc());
	                    travelJourneyDetailDTO.setTravelMode(Constants.TRAIN);
	                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToNA(trainJourneyDTO.getTravelClassDesc()));
	                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToNA(trainJourneyDTO.getPreferredTrain()));
	                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToNA(trainJourneyDTO.getPreferredSeatDesc()));
	                    travelJourneyDetailDTO.setStayType(AppUtility.blankToNA(trainJourneyDTO.getStayTypeDesc()));
	                    
	                    travelJourneyDetailDTO.setOtherInfo("Tatkaal" + " - " + ((trainJourneyDTO!=null && trainJourneyDTO.getType()!=null && trainJourneyDTO.getType().equals("Intermediate Journey")) ? "NA" : (trainJourneyDTO.isTatkaalTicket() ? "Yes" : "No")));
	                    
	                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);
	                    
	                }
	        }
	                /**Train Details Ends Here**/
	                /**Car Details*/
	        if(carDetailsList != null) {    
	                for(CarJourneyDTO carJourneyDetail : carDetailsList)
	                {       
	                    
	                    
	                    travelJourneyDetailDTO= new TravelJourneyDetailDTO();
	                    travelJourneyDetailDTO.setJourney(AppUtility.blankToNA(carJourneyDetail.getType()));
	                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToNA(carJourneyDetail.getSource()));
	                    travelJourneyDetailDTO.setArrival(AppUtility.blankToNA(carJourneyDetail.getDestination()));
	                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToNA(carJourneyDetail.getDateOfJourney()));
	                    travelJourneyDetailDTO.setPreferredTime(carJourneyDetail.getPreferredTimeDesc());
	                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(carJourneyDetail.getPreferredTimeModeDesc()) + " " + carJourneyDetail.getPreferredTimeDesc());
	                    travelJourneyDetailDTO.setTravelMode(Constants.CAR);
	                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToNA(carJourneyDetail.getCarClass()));
	                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToNA(carJourneyDetails.getCarCategoryDesc()));
	                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToNA("-"));
	                    travelJourneyDetailDTO.setStayType(carJourneyDetail.getLocationDesc());
	                    travelJourneyDetailDTO.setOtherInfo(AppUtility.checkNull(carJourneyDetail.getMobileNo())?("N.A."):("Mobile No." + " - " + AppUtility.blankToMinus(carJourneyDetail.getMobileNo())));
	                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);
	                        
	                }
	        }
	        /**Car Details Ends Here**/
				
	        /**To Sort Time and date wise**/
	        Collections.sort(travelJourneyDetailDTOList); 
	        for(int listCounter = 0; listCounter < travelJourneyDetailDTOList.size(); listCounter++)
            {
                TravelJourneyDetailDTO tempTravelObj =  travelJourneyDetailDTOList.get(listCounter);
                    
                    if(listCounter == 0)
                    {       tempTravelObj.setJourney("Onward");
                    }
                    else if(listCounter == travelJourneyDetailDTOList.size()-1)
                    {
                            tempTravelObj.setJourney("Return");
                    }
                    else
                    {
                            tempTravelObj.setJourney("Intermediate");
                    }
            } 
	        requestInfo.setTravelJourneyDetailDTOList(travelJourneyDetailDTOList);
	        
			} catch(Exception e) {
				log.error("Error occurred in RequestServiceImpl : getItineraryInformation() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
	}
	
	
	@SuppressWarnings("unchecked")
	private void getAttachmentsData(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map 		= new HashMap<>(requestDataMap);
			Object attachments 				= map.get("#result-set-15");
			
			if(attachments instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) attachments;
    	    	
				List<AttachmentDTO> attachmentList = new ArrayList<>();
				AttachmentDTO attachment = null;
	        	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> attachmentsMap = list.get(i);
	        	      
	        	    if(attachmentsMap != null) {
	        	    	attachment = new AttachmentDTO();
	        	    	
	        	    	attachment.setTravelId(AppUtility.checkNullString(attachmentsMap.get(ParamConstant.TRAVEL_ID)));
	        	    	attachment.setTravelType(AppUtility.checkNullString(attachmentsMap.get("TRAVEL_TYPE")));
	        	    	attachment.setAttachmentId(AppUtility.checkNullString(attachmentsMap.get("ATTACHMENT_ID")));
	        	    	attachment.setFileName(AppUtility.checkNullString(attachmentsMap.get("FILES_NAME")));
	        	    	attachment.setFileType(AppUtility.checkNullString(attachmentsMap.get("FILE_TYPE")));
	        	    	attachment.setFileSize(AppUtility.checkNullString(attachmentsMap.get("FILE_SIZE")));
	        	    	attachment.setUploadedBy(AppUtility.checkNullString(attachmentsMap.get("UPLOADED_BY")));
	        	    	attachment.setUploadedOn(AppUtility.checkNullString(attachmentsMap.get("UPLOADED_ON")));
	        	    	attachmentList.add(attachment);
	        	    }
				}
				
				requestInfo.setAttachmentsList(attachmentList);
			}
		
		} catch(Exception e) {
			log.error("Error occurred in RequestServiceImpl : getAttachmentsData() while fetching attachments data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	private void getVisaDetails(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object visaDetailsData = map.get("#result-set-14");
			List<VisaDetailDTO> visaList = new ArrayList<VisaDetailDTO>();
			if("Y".equals(requestInfo.getTravelVisaFlag()) && !AppUtility.isNull(visaDetailsData) && visaDetailsData instanceof List) {
				
				List<Map<String, Object>> visaDetailsList = (List<Map<String, Object>>) visaDetailsData;
				
				
				
    	    	
    	    	for(int i=0; i<visaDetailsList.size(); i++) {
	        		
	        		Map<String, Object> visaDtlMap = visaDetailsList.get(i);
	        		
	        		if(!AppUtility.isNull(visaDtlMap)) {
	        			 
	        			VisaDetailDTO visaDetails = new VisaDetailDTO();
	        			visaDetails.setComment(AppUtility.checkNullString(visaDtlMap.get("VISA_COMMENT")));
	        			visaDetails.setCountry(AppUtility.checkNullString(visaDtlMap.get("VISA_COUNTRY")));
	        			visaDetails.setValidFrom(AppUtility.checkNullString(visaDtlMap.get("VISA_VALID_FROM")));
	        			visaDetails.setValidTo(AppUtility.checkNullString(visaDtlMap.get("VISA_VALID_TO")));
	        			visaDetails.setStayDuration(AppUtility.checkNullString(visaDtlMap.get("VISA_STAY_DURATION")));
	        			visaDetails.setValidityFlag(AppUtility.checkNullString(visaDtlMap.get("VISA_VALIDITY_FLAG")));
	        			
	        			visaList.add(visaDetails);
	        		}
	        	}
    	    	
    	    	
	        	requestInfo.setVisa(visaList);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getVisaDetails() while fetching visa details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}

	private void extractPassportDetails(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {

		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object requestDetails = map.get("#result-set-2");
			PassportDetailDTO passportDtl = null;
			
			if("Y".equals(requestInfo.getTravelVisaFlag()) && requestDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetails;
				
				for(int i=0; i<list.size(); i++) {
					passportDtl = new PassportDetailDTO();
	        		Map<String, Object> requestDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsMap)) {
						passportDtl.setDateOfBirth(AppUtility.checkNullString(requestDetailsMap.get("DOB")));
						passportDtl.setExpiryDate(AppUtility.checkNullString(requestDetailsMap.get("EXPIRY_DATE")));
						passportDtl.setIssueDate(AppUtility.checkNullString(requestDetailsMap.get("date_issue")));
						passportDtl.setIssuePlace(AppUtility.checkNullString(requestDetailsMap.get("PLACE_ISSUE")));
						passportDtl.setNationality(AppUtility.checkNullString(requestDetailsMap.get("NATIONALITY")));
						passportDtl.setPassportNo(AppUtility.checkNullString(requestDetailsMap.get("PASSPORT_NO")));
	        	    }
	        	}
				requestInfo.setPassport(passportDtl);
			}

			

		} catch (Exception e) {
			log.error(
					"Error occurred in RequestServiceImpl : getPassportDetails() while fetching passport details : ",
					e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	private void getInsuranceDetails(TravelRequestDetailsDTO requestInfo, Map<String, Object> requestDataMap) {

		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object requestDetails = map.get("#result-set-2");
			InsuranceDetailDTO insuranceDtl = new InsuranceDetailDTO();
			
			if("Y".equals(requestInfo.getTravelVisaFlag()) && requestDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetails;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsMap)) {
	        	    	insuranceDtl.setComments(AppUtility.checkNullString(requestDetailsMap.get("INSURANCE_COMMENT")));
	        	    	insuranceDtl.setNominee(AppUtility.checkNullString(requestDetailsMap.get("INSURANCE_NOMINEE")));
	        	    	insuranceDtl.setRelation(AppUtility.checkNullString(requestDetailsMap.get("INSURANCE_RELATION")));
	        	    }
	        	}
				requestInfo.setInsurance(insuranceDtl);
			}

			

		} catch (Exception e) {
			log.error(
					"Error occurred in RequestServiceImpl : getInsuranceDetails() while fetching insurance details : ",
					e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	@Override
	public List<RequestDetailsDTO> getRecentTravelRequestsData(AppRequestDTO appRequestDTO) {
		
		List<RequestDetailsDTO> requestDataDTOs	= new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<RequestDetails> requestsData = requestDAO.getRecentTravelRequestsData(appRequest);
			
			if(AppUtility.containsData(requestsData)) {
				
				requestsData.forEach(requestData -> {
					RequestDetailsDTO requestDataDTO = new RequestDetailsDTO();
		        	BeanUtils.copyProperties(requestData, requestDataDTO);
					
					requestDataDTOs.add(requestDataDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRecentTravelRequestsData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestDataDTOs;
	}

	
	@Override
	public List<TravelRequestDetailsDTO> getLastApprovedRequestDetailsList(AppRequestDTO appRequestDTO) {
		List<TravelRequestDetailsDTO> travelRequestDetailsDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Map<String, Object>> requestDataMapList = requestDAO.getLastApprovedRequestDetailsList(appRequest);
			
			if(AppUtility.containsData(requestDataMapList)) {
				
				for (Map<String, Object> requestDataMap : requestDataMapList) {
					long travelId = Long.parseLong(AppUtility.checkNullString(requestDataMap.get(ParamConstant.TRAVEL_ID)));
					String travelType = AppUtility.checkNullString(requestDataMap.get("TRAVEL_TYPE"));
					
					TravelRequestDetailsDTO travelRequestDetailsDTO = extractRequestDetails(requestDataMap, travelId, travelType);
					
					if(!AppUtility.isNull(travelRequestDetailsDTO)) {
						travelRequestDetailsDTOList.add(travelRequestDetailsDTO);
					}
				}
				
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getLastApprovedRequestDetailsList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return travelRequestDetailsDTOList;
	}
	
	
	@Override
	public RequestsCountDTO getRequestsCount(AppRequestDTO appRequestDTO) {
		RequestsCountDTO requestCountsDTO = new RequestsCountDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			RequestsCount requestCounts = requestDAO.getRequestsCount(appRequest);
			
			if(!AppUtility.isNull(requestCounts)) {
				BeanUtils.copyProperties(requestCounts, requestCountsDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestsCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return requestCountsDTO;
	}


	@Override
	public List<RequestDetailsDTO> getRequestsByType(AppRequestDTO appRequestDTO) {
		List<RequestDetailsDTO> requestsDTO	= new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
 			Map<String, Object> requestDataMap = requestDAO.getRequestsByType(appRequest);
 			
 			requestsDTO = extractRequestDetails(requestDataMap);
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestsByType() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestsDTO;
	}

	private List<RequestDetailsDTO> extractRequestDetails(Map<String, Object> requestDataMap) {
		
		List<RequestDetailsDTO> requestsList	= new ArrayList<>();
		List<JourneyDTO> journeyList 			= new ArrayList<>();
		
		if(requestDataMap != null && !requestDataMap.isEmpty()) {
			try {
				getRequestList(requestsList, requestDataMap);
				getJourneyDetailsList(journeyList, requestDataMap);
			
			} catch (Exception e) {
				log.error("Error occurred in RequestServiceImpl : extractRequestDetails() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		for(int i=0; i<requestsList.size(); i++) {
			RequestDetailsDTO requestDetail  	=  requestsList.get(i);
			List<JourneyDTO> journeyListTemp	= new ArrayList<>();
			
			for(int j=0; j<journeyList.size(); j++) {
				JourneyDTO journeyDetail =  journeyList.get(j);
				
				if( (requestDetail.getTravelId()).equalsIgnoreCase(Long.toString(journeyDetail.getTravelId())) 
					 && (requestDetail.getTravelType()).equalsIgnoreCase(journeyDetail.getTravelType()) ) {
					 journeyListTemp.add(journeyDetail);
				}
			}
			requestDetail.setJourneyList(journeyListTemp);;
		}
		return requestsList;
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestList(List<RequestDetailsDTO> requestsList, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map 		= new HashMap<>(requestDataMap);
			Object requestDetailsExistsData = map.get("#result-set-1");
			
			if(requestDetailsExistsData instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetailsExistsData;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsExistsDataMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsExistsDataMap)) {
	        	    	
	        	    	RequestDetailsDTO requestDetails = new RequestDetailsDTO();
	        	    	
	        	    	requestDetails.setTravelId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_ID")));
	        	    	requestDetails.setRequestNo(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_REQ_NO")));
	        	    	requestDetails.setTraveller(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVELLER")));
	        	    	requestDetails.setJourneyType(AppUtility.checkNullString(requestDetailsExistsDataMap.get("JOURNEY_TYPE")));
	        	    	requestDetails.setExpenditure(AppUtility.checkNullString(requestDetailsExistsDataMap.get("EXPENDITURE")));
	        	    	requestDetails.setCreatedBy(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ORIGINATOR")));
	        	    	requestDetails.setCreatedOn(AppUtility.checkNullString(requestDetailsExistsDataMap.get("C_DATE")));
	        	    	requestDetails.setSource(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_FROM")));
	        	    	requestDetails.setDestination(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_TO")));
	        	    	requestDetails.setTravelType(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_TYPE")));
	        	    	requestDetails.setSiteName(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SITE_NAME")));
	        	    	requestDetails.setSiteDesc(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SITE_DESC")));
	        	    	requestDetails.setDateOfJourney(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_DATE")));
	        	    	requestDetails.setJourneyDates(AppUtility.checkNullString(requestDetailsExistsDataMap.get("JOURNEY_DATES")));
	        	    	requestDetails.setJourneyDuration(AppUtility.checkNullString(requestDetailsExistsDataMap.get("JOURNEY_DURATION")));
	        	    	requestDetails.setReasonForTravel(AppUtility.checkNullString(requestDetailsExistsDataMap.get("REASON_FOR_TRAVEL")));
	        	    	requestDetails.setFlightTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("FLIGHT_TRAVEL_FLAG")));
	        	    	requestDetails.setTrainTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAIN_TRAVEL_FLAG")));
	        	    	requestDetails.setCarTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("CAR_TRAVEL_FLAG")));
	        	    	requestDetails.setAccommTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ACCOMMODATION_TRAVEL_FLAG")));
	        	    	requestDetails.setAdvanceForexFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ADVANCE_FLAG")));
	        	    	requestDetails.setCommentFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("COMMENT_FLAG")));
	        	    	requestDetails.setAttachmentFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ATTACHMENT_FLAG")));
	        	    	requestDetails.setRequestStatus(AppUtility.checkNullString(requestDetailsExistsDataMap.get("APPROVAL_STATUS")));
	        	    	requestDetails.setTravellerId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVELLER_ID")));
	        	    	requestDetails.setTravelRequestId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_REQ_ID")));
	        	    	requestDetails.setGroupTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("GROUP_TRAVEL_FLAG")));
	        	    	requestDetails.setTravelAgencyId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_AGENCY_ID")));
	        	    	requestDetails.setParentId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("PARENT_ID")));
	        	    	requestDetails.setItineraryDetails(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ITINERARY_DETAILS")));
	        	    	requestDetails.setCancelledBy(AppUtility.checkNullString(requestDetailsExistsDataMap.get("CANCELLED_BY")));
	        	    	requestDetails.setCancelUserRole(AppUtility.checkNullString(requestDetailsExistsDataMap.get("CANCEL_USER_ROLE")));
	        	    	requestDetails.setParentId(AppUtility.checkNullString(requestDetailsExistsDataMap.get("PARENT_ID")));
	        	    	
	        	    	requestsList.add(requestDetails);
	        	    }
				}
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailsExistsDataForEdit() while fetching request details existence data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getJourneyDetailsList(List<JourneyDTO> journeyList, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map 		= new HashMap<>(requestDataMap);
			Object journeyDetailsExistsData = map.get("#result-set-2");
			
			if(journeyDetailsExistsData instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) journeyDetailsExistsData;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> journeyDetailsExistsDataMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(journeyDetailsExistsDataMap)) {
	        	    	
	        	    	JourneyDTO journey = new JourneyDTO();
	        	    	
	        	    	journey.setTravelId(Long.parseLong(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("TRAVEL_ID"))));
	        	    	journey.setTravelType(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("TRAVEL_TYPE")));
	        	    	journey.setSource(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("TRAVEL_FROM")));
	        	    	journey.setDestination(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("TRAVEL_TO")));
	        	    	journey.setDepartureDate(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("DEPARTURE_DATE")));
	        	    	journey.setReturnDate(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("RETURN_DATE")));
	        	    	journey.setDepartureTime(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("DEPARTURE_TIME")));
	        	    	journey.setReturnTime(AppUtility.checkNullString(journeyDetailsExistsDataMap.get("RETURN_TIME")));
	        	    	
	        	    	journeyList.add(journey);
	        	    }
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getJourneyDetailsList() while fetching journey details existence data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@Override
	public List<WorkflowApproverDTO> getRequestWorkflow(AppRequestDTO appRequestDTO) {
		List<WorkflowApproverDTO> requestWorkflowDTO = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<WorkflowApprover> requestWorkflow = requestDAO.getRequestWorkflow(appRequest);
			
			if(AppUtility.containsData(requestWorkflow)) {
				
				requestWorkflow.forEach(approver -> {
					WorkflowApproverDTO approverDTO = new WorkflowApproverDTO();
		        	BeanUtils.copyProperties(approver, approverDTO);
					
					requestWorkflowDTO.add(approverDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestWorkflowDTO;
	}
	
	
	@Override
	public List<JourneyDTO> getUpcomingJourneyDetails(AppRequestDTO appRequestDTO) {
		List<JourneyDTO> upcomingJourneyDetailsDTO = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Journey> upcomingJourneyDetails = requestDAO.getUpcomingJourneyDetails(appRequest);
			
			if(AppUtility.containsData(upcomingJourneyDetails)) {
				
				upcomingJourneyDetails.forEach(journey -> {
					JourneyDTO journeyDTO = new JourneyDTO();
		        	BeanUtils.copyProperties(journey, journeyDTO);
					
					upcomingJourneyDetailsDTO.add(journeyDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getUpcomingJourneyDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return upcomingJourneyDetailsDTO;
	}
	
	@Override
	public RequestSettingDTO getRequestSetting(AppRequestDTO appRequestDTO) {
		RequestSettingDTO requestSettingDTO = new RequestSettingDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			RequestSetting requestSetting = requestDAO.getRequestSetting(appRequest);
			
			if(!AppUtility.isNull(requestSetting)) {
				BeanUtils.copyProperties(requestSetting, requestSettingDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestSetting() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return requestSettingDTO;
	}

	@Override
	public List<ApproverDetailDTO> getFirstLevelApprovers(AppRequestDTO appRequestDTO) {
		
		List<ApproverDetailDTO> approversDetails = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<ApproverDetail> firstLevelApprovers = requestDAO.getFirstLevelApprovers(appRequest);
		
			if (firstLevelApprovers == null) {
	            return Collections.emptyList();
	        
			} else {
				firstLevelApprovers.forEach(firstLevel -> {
					ApproverDetailDTO approverDetail = new ApproverDetailDTO();
					BeanUtils.copyProperties(firstLevel, approverDetail);
					approversDetails.add(approverDetail);
		        });
	        }
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getFirstLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return approversDetails;
	}


	@Override
	public List<ApproverDetailDTO> getSecondLevelApprovers(AppRequestDTO appRequestDTO) {
		
		List<ApproverDetailDTO> approversDetails = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<ApproverDetail> secondLevelApprovers = requestDAO.getSecondLevelApprovers(appRequest);
		
			if (secondLevelApprovers == null) {
	            return Collections.emptyList();
	        
			} else {
				secondLevelApprovers.forEach(secondLevel -> {
					ApproverDetailDTO approverDetail = new ApproverDetailDTO();
					BeanUtils.copyProperties(secondLevel, approverDetail);
					approversDetails.add(approverDetail);
		        });
	        }
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getSecondLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return approversDetails;
	}


	@Override
	public List<ApproverDetailDTO> getThirdLevelApprovers(AppRequestDTO appRequestDTO) {
		
		List<ApproverDetailDTO> approversDetails = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<ApproverDetail> thirdLevelApprovers = requestDAO.getThirdLevelApprovers(appRequest);
		
			if (thirdLevelApprovers == null) {
	            return Collections.emptyList();
	        
			} else {
				thirdLevelApprovers.forEach(thirdLevel -> {
					ApproverDetailDTO approverDetail = new ApproverDetailDTO();
					BeanUtils.copyProperties(thirdLevel, approverDetail);
					approversDetails.add(approverDetail);
		        });
	        }
			
		} catch (Exception e) {
			log.error("Error occurred in DataServiceImpl : getThirdLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return approversDetails;
	}


	@Override
	public List<WorkflowApproverDTO> getDefaultWorkflowApprover(AppRequestDTO appRequestDTO) {
		List<WorkflowApproverDTO> requestWorkflowDTO = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<WorkflowApprover> requestWorkflow = requestDAO.getDefaultWorkflowApprover(appRequest);
			
			if(AppUtility.containsData(requestWorkflow)) {
				
				requestWorkflow.forEach(approver -> {
					WorkflowApproverDTO approverDTO = new WorkflowApproverDTO();
		        	BeanUtils.copyProperties(approver, approverDTO);
					
					requestWorkflowDTO.add(approverDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestWorkflowDTO;
	}
	

	@Override
	public ApproverLevelWiseDTO getApproverLevelWise(AppRequestDTO appRequestDTO) {
		ApproverLevelWiseDTO approverLevelWiseDTO = new ApproverLevelWiseDTO();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			ApproverLevelWise approverLevelWise = requestDAO.getApproverLevelWise(appRequest);
			
			if(!AppUtility.isNull(approverLevelWise)) {
				List<ApproverDetailDTO> firstLevelApproverDTO = new ArrayList<>();
				List<ApproverDetailDTO> secondLevelApproverDTO = new ArrayList<>();
				List<ApproverDetailDTO> thirdLevelApproverDTO = new ArrayList<>();
				List<ApproverDetail> firstApprover = approverLevelWise.getApproverLevel1();
				List<ApproverDetail> secondApprover = approverLevelWise.getApproverLevel2();
				List<ApproverDetail> thirdApprover = approverLevelWise.getApproverLevel3();
				
				
				
				if(AppUtility.containsData(firstApprover)) {
					firstApprover.forEach(approver -> {
						ApproverDetailDTO approverDetailDTO = new ApproverDetailDTO();
			        	BeanUtils.copyProperties(approver, approverDetailDTO);						
			        	firstLevelApproverDTO.add(approverDetailDTO);
			        });
				}
				
				if(AppUtility.containsData(secondApprover)) {
					secondApprover.forEach(approver -> {
						ApproverDetailDTO approverDetailDTO = new ApproverDetailDTO();
			        	BeanUtils.copyProperties(approver, approverDetailDTO);
			        	secondLevelApproverDTO.add(approverDetailDTO);
			        });
				}
				
				if(AppUtility.containsData(thirdApprover)) {
					thirdApprover.forEach(approver -> {
						ApproverDetailDTO approverDetailDTO = new ApproverDetailDTO();
			        	BeanUtils.copyProperties(approver, approverDetailDTO);
			        	thirdLevelApproverDTO.add(approverDetailDTO);
			        });
				}
				
				approverLevelWiseDTO.setApproverLevel1(firstLevelApproverDTO);
				approverLevelWiseDTO.setApproverLevel2(secondLevelApproverDTO);
				approverLevelWiseDTO.setApproverLevel3(thirdLevelApproverDTO);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getApproverLevelWise() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return approverLevelWiseDTO;
	}


	@Override
	public ExchangeRateResponseDTO getExchangeRate(ExchangeRateRequestDTO exchangeRateRequestDTO) {
		
		ExchangeRateResponseDTO exchangeRateResponseDTO = new ExchangeRateResponseDTO();
		try {
			ExchangeRateRequest exchangeRateRequest = new ExchangeRateRequest();
			BeanUtils.copyProperties(exchangeRateRequestDTO, exchangeRateRequest);
			
			ExchangeRateResponse exchangeRateResponse = requestDAO.getExchangeRate(exchangeRateRequest);
			
			if(!AppUtility.checkNull(exchangeRateResponse)) {
				BeanUtils.copyProperties(exchangeRateResponse, exchangeRateResponseDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestSetting() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return exchangeRateResponseDTO;
	}


	@Override
	public AdvanceDetailResponseDTO getForexTotal(ExchangeRateRequestDTO exchangeRateRequestDTO) {
		
		AdvanceDetailResponseDTO advanceDetailResponseDTO = new AdvanceDetailResponseDTO();
		try {
			ExchangeRateRequest exchangeRateRequest = new ExchangeRateRequest();
			BeanUtils.copyProperties(exchangeRateRequestDTO, exchangeRateRequest);
			
			AdvanceDetailResponse advanceDetailResponse = requestDAO.getForexTotal(exchangeRateRequest);
			
			if(!AppUtility.checkNull(advanceDetailResponse)) {
				BeanUtils.copyProperties(advanceDetailResponse, advanceDetailResponseDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getForexTotal() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return advanceDetailResponseDTO;
	}
	
	@Override
	public List<UserDTO> getSiteBasedUsers(SiteBasedUserRequestDTO siteBasedRequestDTO) {
		
		List<UserDTO> userDTOList = new ArrayList<>();
		try {
			SiteBasedUserRequest siteBasedUserRequest = new SiteBasedUserRequest();
			BeanUtils.copyProperties(siteBasedRequestDTO, siteBasedUserRequest);
			
			List<User> users = requestDAO.getSiteBasedUsers(siteBasedUserRequest);
			
			if(AppUtility.containsData(users)) {
				users.forEach(user -> {
					UserDTO userDTO = new UserDTO();
		        	BeanUtils.copyProperties(user, userDTO);						
		        	userDTOList.add(userDTO);
		        });
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestServiceImpl : getSiteBasedUsers() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);			
		}
		
		return userDTOList;
	}
	
	
	@Override
	public List<SeatDTO> getSeatList(SeatRequestDTO seatRequestDTO) {
		List<SeatDTO> seatDTOList = new ArrayList<>();  
		
		try {
			
			SeatRequest seatRequest = new SeatRequest();
			BeanUtils.copyProperties(seatRequestDTO, seatRequest);
			
			List<Seat> seatList = requestDAO.getSeatList(seatRequest);
			
			if(AppUtility.containsData(seatList)) {
				seatList.forEach(seat -> {
					SeatDTO userDTO = new SeatDTO();
		        	BeanUtils.copyProperties(seat, userDTO);						
		        	seatDTOList.add(userDTO);
		        });
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestServiceImpl : getSeatList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return seatDTOList;
	}
	
	
	@Override
	public List<TravelClassDTO> getTravelClassList(TravelClassRequestDTO travelClassRequestDTO) {
		List<TravelClassDTO> travelClassDTOList = new ArrayList<>();  
		
		try {
			
			TravelClassRequest travelClassRequest = new TravelClassRequest();
			BeanUtils.copyProperties(travelClassRequestDTO, travelClassRequest);
			
			List<TravelClass> travelClassList = requestDAO.getTravelClassList(travelClassRequest);
			
			if(AppUtility.containsData(travelClassList)) {
				travelClassList.forEach(travelClass -> {
					TravelClassDTO travelClassDTO = new TravelClassDTO();
		        	BeanUtils.copyProperties(travelClass, travelClassDTO);						
		        	travelClassDTOList.add(travelClassDTO);
		        });
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestServiceImpl : getTravelClassList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return travelClassDTOList;
	}


	@Override
	public List<TimeModeResponseDTO> getTimeModeList(AppRequestDTO appRequestDTO) {
		List<TimeModeResponseDTO> timeModeResponseDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<TimeMode> timeModes = requestDAO.getTimeModeList(appRequest);
			
			if(AppUtility.containsData(timeModes)) {
				
				timeModes.forEach(timeMode -> {
					TimeModeResponseDTO timeModeDTO = new TimeModeResponseDTO();
		        	BeanUtils.copyProperties(timeMode, timeModeDTO);
					
		        	timeModeResponseDTOList.add(timeModeDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getTimeModeList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return timeModeResponseDTOList;
	}


	@Override
	public List<UnitLocationDTO> getUnitLocationList(AppRequestDTO appRequestDTO) {
		List<UnitLocationDTO> unitLocationDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<UnitLocation> unitLocations = requestDAO.getUnitLocationList(appRequest);
			
			if(AppUtility.containsData(unitLocations)) {
				
				unitLocations.forEach(unitLocation -> {
					UnitLocationDTO unitLocationDTO = new UnitLocationDTO();
		        	BeanUtils.copyProperties(unitLocation, unitLocationDTO);
					
		        	unitLocationDTOList.add(unitLocationDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getUnitLocationList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return unitLocationDTOList;
	}


	@Override
	public List<UnitDTO> getBillingUnitList(AppRequestDTO appRequestDTO) {
		List<UnitDTO> billingUnitDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Unit> billingUnits = requestDAO.getBillingUnitList(appRequest);
			
			if(AppUtility.containsData(billingUnits)) {
				
				billingUnits.forEach(billingUnit -> {
					UnitDTO billingUnitDTO = new UnitDTO();
		        	BeanUtils.copyProperties(billingUnit, billingUnitDTO);
					
		        	billingUnitDTOList.add(billingUnitDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getBillingUnitList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return billingUnitDTOList;
	}
	
	
	@Override
	public List<UserDTO> getBillingApproverList(AppRequestDTO appRequestDTO) {
		List<UserDTO> billingApproverDTOList = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<User> billingApprovers = requestDAO.getBillingApproverList(appRequest);
			
			if(AppUtility.containsData(billingApprovers)) {
				billingApprovers.forEach(billingApprover -> {
					UserDTO billingApproverDTO = new UserDTO();
		        	BeanUtils.copyProperties(billingApprover, billingApproverDTO);						
		        	billingApproverDTOList.add(billingApproverDTO);
		        });
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestServiceImpl : getBillingApproverList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);			
		}
		
		return billingApproverDTOList;
	}


	@Override
	public List<MealDTO> getMealList(AppRequestDTO appRequestDTO) {
		List<MealDTO> mealDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Meal> meals = requestDAO.getMealList(appRequest);
			
			if(AppUtility.containsData(meals)) {
				
				meals.forEach(meal -> {
					MealDTO mealDTO = new MealDTO();
		        	BeanUtils.copyProperties(meal, mealDTO);
					
		        	mealDTOList.add(mealDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getMealList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return mealDTOList;
	}


	@Override
	public List<CostCentreDTO> getCostCentreList(AppRequestDTO appRequestDTO) {
		List<CostCentreDTO> costCentreDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<CostCentre> costCentres = requestDAO.getCostCentreList(appRequest);
			
			if(AppUtility.containsData(costCentres)) {
				
				costCentres.forEach(costCentre -> {
					CostCentreDTO costCentreDTO = new CostCentreDTO();
		        	BeanUtils.copyProperties(costCentre, costCentreDTO);
					
		        	costCentreDTOList.add(costCentreDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getCostCentreList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return costCentreDTOList;
	}


	@Override
	public List<CarCategoryDTO> getCarCategoryList(AppRequestDTO appRequestDTO) {
		List<CarCategoryDTO> carCategoryDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<CarCategory> carCategories = requestDAO.getCarCategoryList(appRequest);
			
			if(AppUtility.containsData(carCategories)) {
				
				carCategories.forEach(carCategory -> {
					CarCategoryDTO carCategoryDTO = new CarCategoryDTO();
		        	BeanUtils.copyProperties(carCategory, carCategoryDTO);
					
		        	carCategoryDTOList.add(carCategoryDTO);
		        });
			
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getCarCategoryList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return carCategoryDTOList;
	}


	@Override
	public CheckTESCountDTO checkTESCount(AppRequestDTO appRequestDTO) {
		CheckTESCountDTO checkTESCountDTO = new CheckTESCountDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			CheckTESCount checkTESCount = requestDAO.checkTESCount(appRequest);
			
			if(!AppUtility.isNull(checkTESCount)) {
				BeanUtils.copyProperties(checkTESCount, checkTESCountDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : checkTESCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return checkTESCountDTO;
	}


	@Override
	public CheckApproverDTO checkApproverExistsInDefaultWorkflow(AppRequestDTO appRequestDTO) {
		CheckApproverDTO checkApproverDTO = new CheckApproverDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			CheckApprover checkApprover = requestDAO.checkApproverExistsInDefaultWorkflow(appRequest);
			
			if(!AppUtility.isNull(checkApprover)) {
				BeanUtils.copyProperties(checkApprover, checkApproverDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : checkApproverExistsInDefaultWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		
		return checkApproverDTO;
	}


	@Override
	public List<IdentityProofDTO> getIdentityProofList(AppRequestDTO appRequestDTO) {
		List<IdentityProofDTO> identityProofDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<IdentityProof> identityProofList = requestDAO.getIdentityProofList(appRequest);
			
			if(AppUtility.containsData(identityProofList)) {
				identityProofList.forEach(identityProof -> {
					IdentityProofDTO identityProofDTO = new IdentityProofDTO();
					BeanUtils.copyProperties(identityProof, identityProofDTO);
					identityProofDTOList.add(identityProofDTO);
				});
			} else {
				return Collections.emptyList();
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getIdentityProofList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return identityProofDTOList;
	}


	@Override
	public PassportDetailDTO getPassportDetails(AppRequestDTO appRequestDTO) {
		PassportDetailDTO passportDetailDTO = new PassportDetailDTO();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			PassportDetail passportDetail = requestDAO.getPassportDetails(appRequest);

			if(!AppUtility.isNull(passportDetail)) {
				BeanUtils.copyProperties(passportDetail, passportDetailDTO);
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getPassportDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return passportDetailDTO;
	}


	@Override
	public UserProfileInfoDTO getUserDetails(AppRequestDTO appRequestDTO) {
		UserProfileInfoDTO userProfileInfoDTO = new UserProfileInfoDTO();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			UserProfileInfo userProfileInfo = requestDAO.getUserDetails(appRequest);

			if(!AppUtility.isNull(userProfileInfo)) {
				BeanUtils.copyProperties(userProfileInfo, userProfileInfoDTO);
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getUserDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return userProfileInfoDTO;
	}


	@Override
	public IdentityProofDetailsDTO getIdentityProofDetails(AppRequestDTO appRequestDTO) {
		IdentityProofDetailsDTO identityProofDetailsDTO = new IdentityProofDetailsDTO();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			IdentityProofDetails identityProofDetails = requestDAO.getIdentityProofDetails(appRequest);

			if(!AppUtility.isNull(identityProofDetails)) {
				BeanUtils.copyProperties(identityProofDetails, identityProofDetailsDTO);
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getIdentityProofDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return identityProofDetailsDTO;
	}


	@Override
	public List<TransitHouseDTO> getTransitHouseList(AppRequestDTO appRequestDTO) {
		List<TransitHouseDTO> tansitHouseDTOList = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			List<TransitHouse> transitHouseList = requestDAO.getTransitHouseList(appRequest);

			if(AppUtility.containsData(transitHouseList)) {
				transitHouseList.forEach(transitHouse -> {
					TransitHouseDTO transitHouseDTO = new TransitHouseDTO();
					BeanUtils.copyProperties(transitHouse, transitHouseDTO);
					tansitHouseDTOList.add(transitHouseDTO);
				});
			} else {
				return Collections.emptyList();
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getTransitHouseList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return tansitHouseDTOList;
	}


	@Override
	public List<StayTypeDTO> getStayTypeList(AppRequestDTO appRequestDTO) {
		List<StayTypeDTO> stayTypeDTOList = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			List<StayType> stayTypeList = requestDAO.getStayTypeList(appRequest);

			if(AppUtility.containsData(stayTypeList)) {
				stayTypeList.forEach(stayType -> {
					StayTypeDTO stayTypeDTO = new StayTypeDTO();
					BeanUtils.copyProperties(stayType, stayTypeDTO);
					stayTypeDTOList.add(stayTypeDTO);
				});
			} else {
				return Collections.emptyList();
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getStayTypeList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return stayTypeDTOList;
	}
	
	@Override
	public CommentDetailsDTO getRequestCommentsDetails(AppRequestDTO appRequestDTO) throws Exception  {
		CommentDetailsDTO commentDetailsDTO	= null;
		
		appRequestDTO.setPageType("COM");
	
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		Map<String, Object> requestDataMap = requestDAO.getRequestCommentsDetails(appRequest);
		
		commentDetailsDTO = extractRequestsCommentsDetails(requestDataMap);
		
		return commentDetailsDTO;
	}
	
	private CommentDetailsDTO extractRequestsCommentsDetails(Map<String, Object> requestDataMap) {
		CommentDetailsDTO commentDetailsDTO = null;
		
		if(requestDataMap != null && !requestDataMap.isEmpty()) {
			commentDetailsDTO = new CommentDetailsDTO();
			
				getRequestDetailsData(commentDetailsDTO, requestDataMap);
				getCommentListData(commentDetailsDTO, requestDataMap);
				getCancelCommentListData(commentDetailsDTO, requestDataMap);
		}
		
		return commentDetailsDTO;
	}
	
	@SuppressWarnings("unchecked")
	private void getRequestDetailsData(CommentDetailsDTO commentInfo, Map<String, Object> requestDataMap)  {
		
		Map<String, Object> map = new HashMap<>(requestDataMap);
		Object commentDetailsData = map.get("#result-set-1");
		
		CommentRequestDetailsDTO commentRequestDetailsInfo = new CommentRequestDetailsDTO();
		
		if(commentDetailsData instanceof List) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) commentDetailsData;
    		
			if(!list.isEmpty()) {
				Map<String, Object> commentDetailsDataMap = list.get(0);
				
				commentRequestDetailsInfo.setTravelId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_ID")));
				commentRequestDetailsInfo.setRequestNo(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_REQ_NO")));
				commentRequestDetailsInfo.setTraveller(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVELLER")));
				commentRequestDetailsInfo.setExpenditure(AppUtility.checkNullString(commentDetailsDataMap.get("EXPENDITURE")));
				commentRequestDetailsInfo.setOriginator(AppUtility.checkNullString(commentDetailsDataMap.get("ORIGINATOR")));
				commentRequestDetailsInfo.setCreatedOn(AppUtility.checkNullString(commentDetailsDataMap.get("CREATED_ON")));
				commentRequestDetailsInfo.setSource(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_FROM")));
				commentRequestDetailsInfo.setDestination(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_TO")));
				commentRequestDetailsInfo.setGroupTravelFlag(AppUtility.checkNullString(commentDetailsDataMap.get("GROUP_TRAVEL_FLAG")));
				commentRequestDetailsInfo.setTravelAgencyId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_AGENCY_ID")));
				commentRequestDetailsInfo.setTravelStatusId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_STATUS_ID")));
				commentRequestDetailsInfo.setTkProviderFlag(AppUtility.checkNullString(commentDetailsDataMap.get("TK_PROVIDER_FLAG")));
				commentRequestDetailsInfo.setOriginatorId(AppUtility.checkNullString(commentDetailsDataMap.get("ORIGINATOR_ID")));
				commentRequestDetailsInfo.setApproveStatus(AppUtility.checkNullString(commentDetailsDataMap.get("APPROVE_STATUS")));
				commentRequestDetailsInfo.setApproveId(AppUtility.checkNullString(commentDetailsDataMap.get("APPROVE_ID")));
				commentRequestDetailsInfo.setUserId(AppUtility.checkNullString(commentDetailsDataMap.get("USER_ID")));
				commentRequestDetailsInfo.setSaveUploadEnableFlag(AppUtility.checkNullString(commentDetailsDataMap.get("SAVE_FLAG")));
				
				commentInfo.setRequestDetails(commentRequestDetailsInfo);
			}
			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void getCommentListData(CommentDetailsDTO commentInfo, Map<String, Object> requestDataMap) {
		
		Map<String, Object> map = new HashMap<>(requestDataMap);
		Object commentListData = map.get("#result-set-2");
		
		List<CommentDTO> commentsList = new ArrayList<>();
		CommentDTO comments = null;
		
		if(commentListData instanceof List) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) commentListData;
			
			for(int i=0; i<list.size(); i++) {
        		
        		Map<String, Object> commentDetailsMap = list.get(i);
        	      
        	    if(commentDetailsMap != null) {
        	    	comments = new CommentDTO();
					
        	    	comments.setCommentDesc(AppUtility.checkNullString(commentDetailsMap.get("COMMENTS")));
        	    	comments.setCommentId(AppUtility.checkNullString(commentDetailsMap.get("COMMENTS_ID")));
        	    	comments.setPostedById(AppUtility.checkNullString(commentDetailsMap.get("POSTED_BY_USER_ID")));
        	    	comments.setPostedByName(AppUtility.checkNullString(commentDetailsMap.get("POSTED_BY_USERNAME")));
        	    	comments.setPostedOnDate(AppUtility.checkNullString(commentDetailsMap.get("POSTED_ON")));
        	    	comments.setDeleteEnableFlag(AppUtility.checkNullString(commentDetailsMap.get("DELETE_FLAG")));
        	    	
        	    	commentsList.add(comments);
        	    }
        	    
        	}
			commentInfo.setCommentsList(commentsList);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void getCancelCommentListData(CommentDetailsDTO commentInfo, Map<String, Object> requestDataMap) {
		
		Map<String, Object> map = new HashMap<>(requestDataMap);
		Object commentListData = map.get("#result-set-3");
		
		List<CommentDTO> cancellationCommentsList = new ArrayList<>();
		CommentDTO cancellationComment = null;
		
		if(commentListData instanceof List) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) commentListData;
			
			for(int i=0; i<list.size(); i++) {
        		
        		Map<String, Object> commentDetailsMap = list.get(i);
        	      
        	    if(commentDetailsMap != null) {
        	    	cancellationComment = new CommentDTO();
					
        	    	cancellationComment.setCommentDesc(AppUtility.checkNullString(commentDetailsMap.get("COMMENTS")));
        	    	cancellationComment.setCancelId(AppUtility.checkNullString(commentDetailsMap.get("CANCEL_ID")));
        	    	cancellationComment.setPostedById(AppUtility.checkNullString(commentDetailsMap.get("POSTED_BY_USER_ID")));
        	    	cancellationComment.setPostedByName(AppUtility.checkNullString(commentDetailsMap.get("POSTED_BY_USERNAME")));
        	    	cancellationComment.setPostedOnDate(AppUtility.checkNullString(commentDetailsMap.get("POSTED_ON")));
        	    	cancellationComment.setDeleteEnableFlag(AppUtility.checkNullString(commentDetailsMap.get("DELETE_FLAG")));
        	    	
        	    	cancellationCommentsList.add(cancellationComment);
        	    }
        	    
        	}
			commentInfo.setCancelledCommentsList(cancellationCommentsList);
		}
		
	}
	
	@Override
	public String deleteRequestComment(AppRequestDTO appRequestDTO) throws Exception {
		String errorMessage = "";
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		errorMessage = requestDAO.deleteRequestComment(appRequest);
		
		return errorMessage;
	}
	
	@Override
	public String saveRequestComment(PostCommentDTO postCommentDTO) throws Exception {
		
		String errorMessage = "";
		PostComment postComment = new PostComment();
		postComment = (PostComment) RequestConvertor.convertObjectWithState(postCommentDTO, postComment);
		
		errorMessage = requestDAO.saveRequestComment(postComment);
		
		return errorMessage;
	}
	
	@Override
	public String cancelTravelRequest(PostCommentDTO postCommentDTO) throws Exception {
		
		String errorMessage = "";
		PostComment postComment = new PostComment();
		postComment = (PostComment) RequestConvertor.convertObjectWithState(postCommentDTO, postComment);
		
		errorMessage = requestDAO.cancelTravelRequest(postComment);
		
		return errorMessage;
	}
	
	@Override
	public CommentDetailsDTO getRequestCancelDetails(AppRequestDTO appRequestDTO) throws Exception  {
		CommentDetailsDTO commentDetailsDTO	= null;
		
		appRequestDTO.setPageType("CAN");
	
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		Map<String, Object> requestDataMap = requestDAO.getRequestCommentsDetails(appRequest);
		
		commentDetailsDTO = extractRequestsCommentsDetails(requestDataMap);
		
		return commentDetailsDTO;
	}
	
	@Override
	public AppRequestDTO deleteRequestData(AppRequestDTO appRequestDTO) {
		
		AppRequest appRequest = new AppRequest();
		RequestDataDTO requestDataDTO = new RequestDataDTO();
		
		try {
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			boolean delSuccess = requestDAO.deleteRequestData(appRequest);
			
			if(delSuccess) {
				BeanUtils.copyProperties(appRequest, requestDataDTO);
				return appRequestDTO;
			
			} else {
				return null;
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : deleteRequestData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return null;
	}

	@Override
	public List<MailFlowDTO> getMailFlowList(AppRequestDTO appRequestDTO) {
		List<MailFlowDTO> mailFlowDTOList = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			List<MailFlow> mailFlowList = requestDAO.getMailFlowList(appRequest);

			if(AppUtility.containsData(mailFlowList)) {
				mailFlowList.forEach(mailFlow -> {
					MailFlowDTO mailFlowDTO = new MailFlowDTO();
					BeanUtils.copyProperties(mailFlow, mailFlowDTO);
					mailFlowDTOList.add(mailFlowDTO);
				});
			} else {
				return Collections.emptyList();
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getMailFlowList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return mailFlowDTOList;
	}
	
	@Override
	public MailDetailDTO getMailDetail(AppRequestDTO appRequestDTO) {
		MailDetailDTO mailDetailDTO = new MailDetailDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			MailDetail mailDetail = requestDAO.getMailDetail(appRequest);
			
			if(!AppUtility.isNull(mailDetail)) {
				BeanUtils.copyProperties(mailDetail, mailDetailDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getMailDetail() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return mailDetailDTO;
	}


	@Override
	public List<CarLocationDTO> getCarLocationList(AppRequestDTO appRequestDTO) {
		List<CarLocationDTO> carLocationDTOList = new ArrayList<>();
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);

			List<CarLocation> carLocationList = requestDAO.getCarLocationList(appRequest);

			if(AppUtility.containsData(carLocationList)) {
				carLocationList.forEach(carLocation -> {
					CarLocationDTO carLocationDTO = new CarLocationDTO();
					BeanUtils.copyProperties(carLocation, carLocationDTO);
					carLocationDTOList.add(carLocationDTO);
				});
			} else {
				return Collections.emptyList();
			}

		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getCarLocationList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		return carLocationDTOList;
	}
	
	@Override
	public AttachmentDetailsDTO getRequestAttachmentDetails(AppRequestDTO appRequestDTO) throws Exception  {
		AttachmentDetailsDTO attachmentDetailsDTO	= null;
	
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		Map<String, Object> requestDataMap = requestDAO.getRequestAttachmentDetails(appRequest);
		
		attachmentDetailsDTO = extractRequestsAttachmentDetails(requestDataMap);
		
		return attachmentDetailsDTO;
	}
	
	private AttachmentDetailsDTO extractRequestsAttachmentDetails(Map<String, Object> requestDataMap) {
		AttachmentDetailsDTO attachmentDetailsDTO = null;
		
		if(requestDataMap != null && !requestDataMap.isEmpty()) {
			attachmentDetailsDTO = new AttachmentDetailsDTO();
			
				getRequestData(attachmentDetailsDTO, requestDataMap);
				getAttachmentList(attachmentDetailsDTO, requestDataMap);
		}
		return attachmentDetailsDTO;
	}
	
	
	@SuppressWarnings("unchecked")
	private void getRequestData(AttachmentDetailsDTO attachmentDetailsDTO, Map<String, Object> requestDataMap)  {
		
		Map<String, Object> map = new HashMap<>(requestDataMap);
		Object requestDetailsData = map.get("#result-set-1");
		
		CommentRequestDetailsDTO requestInfoData = new CommentRequestDetailsDTO();
		
		if(requestDetailsData instanceof List) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetailsData;
    		
			if(!list.isEmpty()) {
				Map<String, Object> commentDetailsDataMap = list.get(0);
				
				requestInfoData.setTravelId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_ID")));
				requestInfoData.setRequestNo(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_REQ_NO")));
				requestInfoData.setTraveller(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVELLER")));
				requestInfoData.setExpenditure(AppUtility.checkNullString(commentDetailsDataMap.get("EXPENDITURE")));
				requestInfoData.setOriginator(AppUtility.checkNullString(commentDetailsDataMap.get("ORIGINATOR")));
				requestInfoData.setCreatedOn(AppUtility.checkNullString(commentDetailsDataMap.get("CREATED_ON")));
				requestInfoData.setSource(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_FROM")));
				requestInfoData.setDestination(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_TO")));
				requestInfoData.setGroupTravelFlag(AppUtility.checkNullString(commentDetailsDataMap.get("GROUP_TRAVEL_FLAG")));
				requestInfoData.setTravelAgencyId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_AGENCY_ID")));
				requestInfoData.setTravelStatusId(AppUtility.checkNullString(commentDetailsDataMap.get("TRAVEL_STATUS_ID")));
				requestInfoData.setTkProviderFlag(AppUtility.checkNullString(commentDetailsDataMap.get("TK_PROVIDER_FLAG")));
				requestInfoData.setOriginatorId(AppUtility.checkNullString(commentDetailsDataMap.get("ORIGINATOR_ID")));
				requestInfoData.setApproveStatus(AppUtility.checkNullString(commentDetailsDataMap.get("APPROVE_STATUS")));
				requestInfoData.setApproveId(AppUtility.checkNullString(commentDetailsDataMap.get("APPROVE_ID")));
				requestInfoData.setUserId(AppUtility.checkNullString(commentDetailsDataMap.get("USER_ID")));
				requestInfoData.setSaveUploadEnableFlag(AppUtility.checkNullString(commentDetailsDataMap.get("UPLOAD_FLAG")));
				
				attachmentDetailsDTO.setRequestDetails(requestInfoData);
			}
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void getAttachmentList(AttachmentDetailsDTO attachmentDetailsDTO, Map<String, Object> requestDataMap) {
		
		Map<String, Object> map = new HashMap<>(requestDataMap);
		Object attachmentListData = map.get("#result-set-2");
		
		List<AttachmentDTO> attachmentList = new ArrayList<>();
		AttachmentDTO attachmentData = null;
		
		if(attachmentListData instanceof List) {
			List<Map<String, Object>> list = (List<Map<String, Object>>) attachmentListData;
			
			for(int i=0; i<list.size(); i++) {
        		
        		Map<String, Object> attachmentDataMap = list.get(i);
        	      
        	    if(attachmentDataMap != null) {
        	    	attachmentData = new AttachmentDTO();
					
        	    	attachmentData.setOriginatorId(AppUtility.checkNullString(attachmentDataMap.get("ORIGINATOR_ID")));
        	    	attachmentData.setTravelStatusId(AppUtility.checkNullString(attachmentDataMap.get("TRAVEL_STATUS_ID")));
        	    	attachmentData.setDocRef(AppUtility.checkNullString(attachmentDataMap.get("DOC_REF")));
        	    	attachmentData.setFileLocation(AppUtility.checkNullString(attachmentDataMap.get("FILE_LOCATION")));
        	    	attachmentData.setFileName(AppUtility.checkNullString(attachmentDataMap.get("FILES_NAME")));
        	    	attachmentData.setUploadedByName(AppUtility.checkNullString(attachmentDataMap.get("UPLOADED_BY_NAME")));
        	    	attachmentData.setUploadedOn(AppUtility.checkNullString(attachmentDataMap.get("UPLOADED_ON")));
        	    	attachmentData.setUploadedBy(AppUtility.checkNullString(attachmentDataMap.get("UPLOADED_BY")));
        	    	attachmentData.setAttachmentId(AppUtility.checkNullString(attachmentDataMap.get("ATTACHMENT_ID")));
        	    	attachmentData.setDeleteEnableFlag(AppUtility.checkNullString(attachmentDataMap.get("DELETE_FLAG")));
        	    	
        	    	attachmentList.add(attachmentData);
        	    }
        	    
        	}
			attachmentDetailsDTO.setAttachmentList(attachmentList);
		}
		
	}
	
	@Override
	public String deleteRequestAttachment(AppRequestDTO appRequestDTO) throws Exception {
		String errorMessage = "";
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		errorMessage = requestDAO.deleteRequestAttachment(appRequest);
		
		return errorMessage;
	}
	
	@Override
	public String deleteRequestJourneyDetail(DeleteJourneyDetailRequestDTO deleteJourneyDetailRequestDTO)
			throws Exception {
		String errorMessage = "";
		DeleteJourneyDetailRequest deleteJourneyDetailRequest = new DeleteJourneyDetailRequest();
		BeanUtils.copyProperties(deleteJourneyDetailRequestDTO, deleteJourneyDetailRequest);
		
		errorMessage = requestDAO.deleteRequestJourneyDetail(deleteJourneyDetailRequest);
		
		return errorMessage;
	}
	
	@Override
	public TravelRequestDTO getRequestDetailToEdit(AppRequestDTO appRequestDTO) {
		
		TravelRequestDTO travelRequestDTO = null;
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			Map<String, Object> requestDataMap = requestDAO.getRequestDetailForEdit(appRequest);
			
			travelRequestDTO = extractRequestDetailsForEdit(requestDataMap, appRequestDTO.getTravelId(), appRequestDTO.getTravelType());
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailToEdit() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return travelRequestDTO;
	}
	private TravelRequestDTO extractRequestDetailsForEdit(Map<String, Object> requestDataMap, long travelId, String travelType) {
		TravelRequestDTO travelRequestDTO = null;
		RemarksDTO remarksDTO = new RemarksDTO();
		if(requestDataMap != null && !requestDataMap.isEmpty()) {
			travelRequestDTO = new TravelRequestDTO();
			
			try {
				getRequestDetailsExistsDataForEdit(travelRequestDTO, requestDataMap);
				getRequestDetailsDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				getFlightJourneyDetailsDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				getTrainJourneyDetailsDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				getCarJourneyDetailsDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				getRequestApproverLevelsForEdit(travelRequestDTO, requestDataMap);
				//getRequestApproversForEdit(travelRequestDTO, requestDataMap);
				getAdvanceForexDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				getAccommodationDetailsDataForEdit(travelRequestDTO, requestDataMap,remarksDTO);
				
			
			} catch (Exception e) {
				log.error("Error occurred in RequestServiceImpl : extractRequestDetailsForEdit() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return travelRequestDTO;
	}
	@SuppressWarnings("unchecked")
	private void getRequestDetailsExistsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map 		= new HashMap<>(requestDataMap);
			Object requestDetailsExistsData = map.get("#result-set-1");
			
			if(requestDetailsExistsData instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetailsExistsData;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsExistsDataMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(requestDetailsExistsDataMap)) {
	        	    	if(requestInfo == null) {
				    		requestInfo = new TravelRequestDTO();
				    	}
	        	    	
	        	    	requestInfo.setTravelRequestNo(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_REQ_NO")));
	        	    	requestInfo.setRequestId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_REQ_ID"))));
	        	    	requestInfo.setTravelType(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_TYPE")));
	        	    	requestInfo.setTravelId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_ID"))));
	        	    	
	        	    	
	        	    	requestInfo.setFlightTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("FLIGHT_TRAVEL_FLAG")));
	        	    	requestInfo.setTrainTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAIN_TRAVEL_FLAG")));
	        	    	requestInfo.setCarTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("CAR_TRAVEL_FLAG")));
	        	    	requestInfo.setAccommodationDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ACCOMMODATION_TRAVEL_FLAG")));
	        	    	requestInfo.setOtherAccommodationsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("OTHER_ACCOMMODATION_FLAG")));
	        	    	requestInfo.setAdvanceForexFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("ADVANCE_FLAG")));
	        	    	requestInfo.setTravelVisaFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("TRAVEL_VISA_FLAG")));
	        	    	requestInfo.setBudgetActualDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SHOW_BUD_FLAG")));
	        	    	requestInfo.setTotalTravelFareDetailsFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("SHOW_APP_LVL_3_FLAG")));
	        	    	requestInfo.setGroupTravelFlag(AppUtility.checkNullString(requestDetailsExistsDataMap.get("GROUP_TRAVEL_FLAG")));
	        	    	
	        	    }
				}
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailsExistsDataForEdit() while fetching request details existence data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
	}

	@SuppressWarnings("unchecked")
	private void getRequestDetailsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object requestDetails = map.get("#result-set-2");
			
			
			if(requestDetails instanceof List) {
				List<Map<String, Object>> list = (List<Map<String, Object>>) requestDetails;
				
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> requestDetailsMap = list.get(i);
	        	      
	        	    if(requestDetailsMap != null) {
	        	    	
	        	    	requestInfo.setLinkedTravelRequestId(AppUtility.convertStringToLong(AppUtility.checkNullString(requestDetailsMap.get("LINK_TRAVEL_ID"))));
	        	    	
	        	    	requestInfo.setCostCentreId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("COST_CENTER_ID"))));
	        	    	requestInfo.setCostCentreName(AppUtility.checkNullString(requestDetailsMap.get("COST_CENTER_DESC")));
	        	    	
	        	    	requestInfo.setPreferredMealId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("MEAL_ID"))));
	        	    	requestInfo.setPreferredMealDesc(AppUtility.checkNullString(requestDetailsMap.get("MEAL_DESC")));
	        	    	
	        	    	requestInfo.setOriginatorName(AppUtility.checkNullString(requestDetailsMap.get("ORIGINATOR_NAME")));
	        	    	requestInfo.setOriginatorId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("ORIGINATOR_ID"))));
	        	    	
	        	    	requestInfo.setSiteId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("SITE_ID"))));
	        	    	requestInfo.setSiteName(AppUtility.checkNullString(requestDetailsMap.get("SITE_NAME")));
	        	    	
	        			requestInfo.setTravellerId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("TRAVELLER_ID"))));
						requestInfo.setTravellerName(AppUtility.checkNullString(requestDetailsMap.get("TRAVELLER_NAME")));
						
						String reasonForTravel = AppUtility.checkNullString(requestDetailsMap.get("REASON_FOR_TRAVEL"));
						if(!AppUtility.isNull(reasonForTravel)) {
							requestInfo.setReasonForTravel(reasonForTravel);
						} else {
							requestInfo.setReasonForTravel("");
						}
						
						requestInfo.setSiteLocation(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("LOCATION_ID"))));

						String siteLocation = AppUtility.checkNullString(requestDetailsMap.get("LOCATION_NAME"));
						siteLocation = siteLocation==null || "".equals(siteLocation.trim()) ? "":siteLocation.trim();
						requestInfo.setSiteLocationDesc(siteLocation);
						
						
						String gstNo = AppUtility.checkNullString(requestDetailsMap.get("GST_NO"));
						gstNo = AppUtility.isNull(gstNo) ? "":gstNo.trim();
						requestInfo.setGstNo(gstNo);
						
						requestInfo.setBillingClientName(AppUtility.checkNullString(requestDetailsMap.get("BILLING_CLIENT_DESC")));
						requestInfo.setBillingClient(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("BILLING_CLIENT"))));
						
						requestInfo.setBillingApprover(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("BILLING_APPROVER"))));
						requestInfo.setBillingApproverName(AppUtility.checkNullString(requestDetailsMap.get("BILLING_APPROVER_NAME")));
						
						int costCenterId = AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("COST_CENTRE_ID")));
						requestInfo.setCostCentreId(costCenterId);
						
						String costCenter = AppUtility.checkNullString(requestDetailsMap.get("COST_CENTRE_DESC"));
						
						if(AppUtility.isNull(costCenter) || "-".equals(costCenter)) {
							requestInfo.setCostCentreName("Not Applicable");
						} else {
							requestInfo.setCostCentreName(costCenter.toUpperCase());
						}
						
						requestInfo.setPreferredMealId(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("MEAL_ID"))));
						requestInfo.setPreferredMealDesc(AppUtility.checkNullString(requestDetailsMap.get("MEAL_DESC")));
						
						
						BudgetActualDTO budgetActualDTO = new BudgetActualDTO();
						budgetActualDTO.setYtmBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(requestDetailsMap.get("YTM_BUDGET"))));
						budgetActualDTO.setYtdActual(AppUtility.convertStringToDouble(AppUtility.checkNullString(requestDetailsMap.get("YTD_ACTUAL"))));
						budgetActualDTO.setAvailableBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(requestDetailsMap.get("AVAIL_BUDGET"))));
						budgetActualDTO.setEstimate(AppUtility.convertStringToDouble(AppUtility.checkNullString(requestDetailsMap.get("EST_EXP"))));
						budgetActualDTO.setExpRemarks(AppUtility.checkNullString(requestDetailsMap.get("EXP_REMARKS")));
						requestInfo.setBudgetActualDetails(budgetActualDTO);
						
						TravelFareDTO travelFareDTO = new TravelFareDTO();
						travelFareDTO.setCurrency(AppUtility.checkNullString(requestDetailsMap.get("FARE_CURRENCY")));
						travelFareDTO.setCurrencyCode(AppUtility.checkNullString(requestDetailsMap.get("FARE_CURRENCY_CODE")));
						travelFareDTO.setFareAmount(AppUtility.convertStringToInt(AppUtility.checkNullString(requestDetailsMap.get("FARE_AMOUNT"))));
						requestInfo.setTotalTravelFareDetails(travelFareDTO);
						
						remarksDTO.setFlightRemarks(AppUtility.checkNullString(requestDetailsMap.get("FLIGHT_REMARKS")));
						remarksDTO.setTrainRemarks(AppUtility.checkNullString(requestDetailsMap.get("TRAIN_REMARKS")));
						remarksDTO.setCarRemarks(AppUtility.checkNullString(requestDetailsMap.get("CAR_REMARKS")));
						remarksDTO.setAccommodationRemarks(AppUtility.checkNullString(requestDetailsMap.get("ACCOMMODATION_REMARKS")));
						remarksDTO.setAdvanceRemarks(AppUtility.checkNullString(requestDetailsMap.get("ADVANCE_REMARKS")));
						remarksDTO.setCashBreakupRemarks(AppUtility.checkNullString(requestDetailsMap.get("CASH_BREAKUP_REMARKS")));
						
						requestInfo.setTotalAdvanceAmountINR(AppUtility.convertStringToDouble(AppUtility.checkNullString(requestDetailsMap.get("TOTAL_ADVANCE_AMOUNT_INR"))));
	        	    }
	        	}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestDetailsDataForEdit() while fetching request details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	private void getFlightJourneyDetailsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object flightDetails = map.get("#result-set-3");
			
			if("Y".equals(requestInfo.getFlightTravelFlag()) &&flightDetails != null && flightDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) flightDetails;
				
				FlightJourneyDetailsDTO flightJourneyDetails = new FlightJourneyDetailsDTO();
				List<FlightJourneyDTO> flightDetailList = new ArrayList<>();
				
				FlightJourneyDTO flight = null;
				
				for(int i=0; i<list.size(); i++) {
					
					Map<String, Object> flightDetailsMap = list.get(i);
					  
					
					if(!AppUtility.isNull(flightDetailsMap) && (!AppUtility.isBlankString(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !AppUtility.isBlankString(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_TO))))) {
						
						String flightJourneyType = AppUtility.checkNullString(flightDetailsMap.get("FLIGHT_JOURNEY_TYPE"));
						String journeyType = AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.JOURNEY_TYPE));
						
						flightJourneyDetails.setJourneyType(flightJourneyType);
							
						if("R".equalsIgnoreCase(flightJourneyType) && "R".equalsIgnoreCase(journeyType)) {
							flightJourneyDetails.setReturnDate(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							flightJourneyDetails.setReturnTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							flightJourneyDetails.setReturnTimeModeDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							flightJourneyDetails.setReturnTime(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							flightJourneyDetails.setReturnTimeDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
							
						} else {
							flight = new FlightJourneyDTO();
							
							flight.setType(journeyType);
							flight.setJourneyOrder(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.JOURNEY_ORDER))));
							flight.setJourneyCode(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.JOURNEY_CODE)));
							
							flight.setSource(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_FROM)));
							flight.setDestination(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_TO)));
							flight.setCountry((Integer)flightDetailsMap.get(ParamConstant.TRAVEL_COUNTRY));
							flight.setCountryDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.COUNTRY_DESC)));
							flight.setDateOfJourney(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							flight.setPreferredTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							flight.setPreferredTimeModeDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							flight.setPreferredTime(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							flight.setPreferredTimeDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
							
							flight.setTravelClass(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_CLASS))));
							flight.setTravelClassDesc(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRAVEL_CLASS_DESC)));
							
							flight.setPreferredAirline(AppUtility.checkNullString(flightDetailsMap.get("pref_airline")));
							
							flight.setPreferredSeat(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get("SEAT_PREFFERED"))));
							flight.setPreferredSeatDesc(AppUtility.checkNullString(flightDetailsMap.get("SEAT_PREFFERED_DESC")));
							
							flight.setStayType(AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRANSIT_TYPE))));
							flight.setStayTypeDesc((AppUtility.convertStringToInt(AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRANSIT_TYPE))) == -1)? "N/A" : AppUtility.checkNullString(flightDetailsMap.get(ParamConstant.TRANSIT_TYPE_DESC)));
							
							flightDetailList.add(flight);
						}
					}
				}
				flightJourneyDetails.setFlightJourneyRemarks(remarksDTO.getFlightRemarks());
				flightJourneyDetails.setFlightJourney(flightDetailList);
				requestInfo.setFlightJourneyDetails(flightJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getFlightJourneyDetailsDataForEdit() while fetching flight journey details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getTrainJourneyDetailsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object trainDetails = map.get("#result-set-4");
			
			if("Y".equals(requestInfo.getTrainTravelFlag()) && trainDetails != null && trainDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) trainDetails;
				
				List<TrainJourneyDTO> trainDetailList = new ArrayList<>();
				TrainJourneyDetailsDTO trainJourneyDetails = new TrainJourneyDetailsDTO();
				TrainJourneyDTO train = null;
				
				for(int i=0; i<list.size(); i++) {
					
					Map<String, Object> trainDetailsMap = list.get(i);
					if(!AppUtility.isNull(trainDetailsMap) && (!AppUtility.isBlankString(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !AppUtility.isBlankString(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_TO)))) ) {
						
						String trainJourneyType = AppUtility.checkNullString(trainDetailsMap.get("TRAIN_JOURNEY_TYPE"));
						String journeyType = AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.JOURNEY_TYPE));
						
						trainJourneyDetails.setJourneyType(trainJourneyType);
							
						if("R".equalsIgnoreCase(trainJourneyType) && "R".equalsIgnoreCase(journeyType)) {
							trainJourneyDetails.setReturnDate(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							trainJourneyDetails.setReturnTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							trainJourneyDetails.setReturnTimeModeDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							trainJourneyDetails.setReturnTime(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							trainJourneyDetails.setReturnTimeDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
							
						} else {
							train = new TrainJourneyDTO();
							
							train.setType(journeyType);
							train.setJourneyOrder(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.JOURNEY_ORDER))));
							train.setJourneyCode(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.JOURNEY_CODE)));
							
							train.setSource(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_FROM)));
							train.setDestination(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_TO)));
							train.setDateOfJourney(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							train.setPreferredTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							train.setPreferredTimeModeDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							train.setPreferredTime(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							train.setPreferredTimeDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
							
							train.setTravelClass(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_CLASS))));
							train.setTravelClassDesc(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRAVEL_CLASS_DESC)));
							
							train.setPreferredTrain(AppUtility.checkNullString(trainDetailsMap.get("PREF_TRAIN")));
							
							train.setPreferredSeat(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get("SEAT_PREFFERED"))));
							train.setPreferredSeatDesc(AppUtility.checkNullString(trainDetailsMap.get("SEAT_PREFFERED_DESC")));
							
							train.setTatkaalTicket("Y".equalsIgnoreCase(AppUtility.checkNullString(trainDetailsMap.get("TATKAAL_FLAG"))) ? Boolean.TRUE : Boolean.FALSE);
							train.setTicketType(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get("TICKET_TYPE_ID"))));
							train.setTicketTypeDesc(AppUtility.checkNullString(trainDetailsMap.get("TICKET_TYPE_DESC")));
							
							train.setStayType(AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRANSIT_TYPE))));
							train.setStayTypeDesc((AppUtility.convertStringToInt(AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRANSIT_TYPE))) == -1)? "N/A" : AppUtility.checkNullString(trainDetailsMap.get(ParamConstant.TRANSIT_TYPE_DESC)));
							
							trainDetailList.add(train);
						}
					}
				}
				trainJourneyDetails.setTrainJourneyRemarks(remarksDTO.getTrainRemarks());
				trainJourneyDetails.setTrainJourney(trainDetailList);
				requestInfo.setTrainJourneyDetails(trainJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getTrainJourneyDetailsDataForEdit() while fetching train journey details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getCarJourneyDetailsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object carDetails = map.get("#result-set-5");
			
			int carClassId = 0;
			int carCategory = 0;
			
			String carClassDesc = "";
			String carCategoryDesc = "";
			
			if("Y".equals(requestInfo.getCarTravelFlag()) && carDetails != null && carDetails instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) carDetails;
				CarJourneyDetailsDTO carJourneyDetails = new CarJourneyDetailsDTO();
				
				List<CarJourneyDTO> carDetailList = new ArrayList<>();
				CarJourneyDTO car = null;
				
				for(int i=0; i<list.size(); i++) {
					
					Map<String, Object> carDetailsMap = list.get(i);
					  
					if(carDetailsMap != null && (!"".equals(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_FROM))) || !"".equals(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_TO)))) ) {
						
						String carJourneyType = AppUtility.checkNullString(carDetailsMap.get("CAR_JOURNEY_TYPE"));
						String journeyType = AppUtility.checkNullString(carDetailsMap.get(ParamConstant.JOURNEY_TYPE));
						
						carJourneyDetails.setJourneyType(carJourneyType);
							
						if("R".equalsIgnoreCase(carJourneyType) && "R".equalsIgnoreCase(journeyType)) {
							carJourneyDetails.setReturnDate(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							carJourneyDetails.setReturnTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							carJourneyDetails.setReturnTimeModeDesc(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							carJourneyDetails.setReturnTime(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							carJourneyDetails.setReturnTimeDesc(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
						
						} else {
							car = new CarJourneyDTO();
							
							car.setType(journeyType);
							car.setJourneyOrder(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.JOURNEY_ORDER))));
							car.setJourneyCode(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.JOURNEY_CODE)));	
							car.setCarJourneyId(AppUtility.convertStringToLong(AppUtility.checkNullString(carDetailsMap.get("TRAVEL_CAR_ID"))));
							
							car.setSource(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_FROM)));
							car.setDestination(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_TO)));
							car.setDateOfJourney(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.TRAVEL_DATE)));
							
							car.setPreferredTimeMode(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE))));
							car.setPreferredTimeModeDesc(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_TYPE_DESC)));
							
							car.setPreferredTime(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME))));
							car.setPreferredTimeDesc(AppUtility.checkNullString(carDetailsMap.get(ParamConstant.PREFERRED_TIME_DESC)));
							
							car.setLocation(AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get("LOCATION"))));
							car.setLocationDesc(AppUtility.checkNullString(carDetailsMap.get("LOCATION_DESC")));
							
							car.setMobileNo(AppUtility.checkNullString(carDetailsMap.get("MOBILENO")));
							
							carDetailList.add(car);
						}
												
						carClassId = AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get("CAR_CLASS_ID")));
						carClassDesc= AppUtility.checkNullString(carDetailsMap.get("CAR_CLASS_DESC"));
						
						carCategory = AppUtility.convertStringToInt(AppUtility.checkNullString(carDetailsMap.get("CAR_CATEGORY_ID")));
						carCategoryDesc = AppUtility.checkNullString(carDetailsMap.get("CAR_CATEGORY_DESC"));
					}
				}
				carJourneyDetails.setCarCategory(carCategory);
				carJourneyDetails.setCarCategoryDesc(carCategoryDesc);
				carJourneyDetails.setCarClass(carClassId);
				carJourneyDetails.setCarClassDesc(carClassDesc);
				
				carJourneyDetails.setCarJourneyRemarks(remarksDTO.getCarRemarks());
				carJourneyDetails.setCarJourney(carDetailList);
				requestInfo.setCarJourneyDetails(carJourneyDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getCarJourneyDetailsDataForEdit() while fetching car journey details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	private void getRequestApproverLevelsForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object approverLevels = map.get("#result-set-2");
			
			if(!AppUtility.isNull(approverLevels) && approverLevels instanceof List) {
				List<Map<String, Object>> list 	= (List<Map<String, Object>>) approverLevels;
    	    	
				for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> approverLevelsMap = list.get(i);
	        	      
	        	    if(!AppUtility.isNull(approverLevelsMap)) {
	        	    	
	        	    	requestInfo.setApproverLevel1(AppUtility.convertStringToInt(AppUtility.checkNullString(approverLevelsMap.get("MANAGER_ID"))));
	        	    	requestInfo.setApproverLevel1Name(!AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("MANAGER_NAME"))) ? AppUtility.checkNullString(approverLevelsMap.get("MANAGER_NAME")) : "");
						
	        	    	requestInfo.setApproverLevel2(AppUtility.convertStringToInt(AppUtility.checkNullString(approverLevelsMap.get("HOD_ID"))));
	        	    	requestInfo.setApproverLevel2Name(!AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("HOD_NAME"))) ? AppUtility.checkNullString(approverLevelsMap.get("HOD_NAME")) : "");
						
						requestInfo.setApproverLevel3(AppUtility.convertStringToInt(AppUtility.checkNullString(approverLevelsMap.get("BOARD_MEMBER_ID"))));
	        	    	requestInfo.setApproverLevel3Name(!AppUtility.isNull(AppUtility.checkNullString(approverLevelsMap.get("BOARD_MEMBER_NAME"))) ? AppUtility.checkNullString(approverLevelsMap.get("BOARD_MEMBER_NAME")) : "");
						
						String reasonForSkip = AppUtility.checkNullString(approverLevelsMap.get("REASON_FOR_SKIP"));
						requestInfo.setReasonForSkip(!AppUtility.isBlankString(reasonForSkip) && !"-".equals(reasonForSkip) ? reasonForSkip : "");
	        	    }
	        	}
				
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getRequestApproverLevelsForEdit() while fetching approver levels data : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}

	@SuppressWarnings("unchecked")
	private void getAdvanceForexDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object advanceForexDetailsData = map.get("#result-set-7");
			
			if("Y".equals(requestInfo.getAdvanceForexFlag()) && advanceForexDetailsData != null && advanceForexDetailsData instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) advanceForexDetailsData;
				AdvanceDetailsDTO advanceForexDetails = new AdvanceDetailsDTO();
				List<AdvanceDTO> advanceForexDetailsList = new ArrayList<>();
    	    	
				
				AdvanceDTO advanceForex = null;
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> advanceForexDetailsMap = list.get(i);
	        	      
	        	    if(advanceForexDetailsMap != null) {

						advanceForex = new AdvanceDTO();
						
						advanceForex.setCurrencyCode(AppUtility.checkNullString(advanceForexDetailsMap.get("CURRENCY_DESC")));
						
						advanceForex.setDailyAllowanceExpPerDay(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("DAILY_ALLOW_EXP_PER_DAY"))));
						advanceForex.setDailyAllowanceTourDays(AppUtility.convertStringToInt(AppUtility.checkNullString(advanceForexDetailsMap.get("DAILY_ALLOW_TOUR_DAYS"))));
						advanceForex.setDailyAllowanceExpTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("DAILY_ALLOW_TOTAL_EXP"))));
						
						advanceForex.setHotelChargesExpPerDay(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("HOTEL_EXP_PER_DAY"))));
						advanceForex.setHotelChargesTourDays(AppUtility.convertStringToInt(AppUtility.checkNullString(advanceForexDetailsMap.get("HOTEL_TOUR_DAYS"))));
						advanceForex.setHotelChargesExpTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("HOTEL_TOTAL_EXP"))));
						
						advanceForex.setContingencies(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("CONTINGENCY"))));
						advanceForex.setOthers(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("OTHERS"))));
						
						advanceForex.setTotal(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_AMOUNT"))));
						advanceForex.setExchangeRateINR(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("EXCHANGE_RATE"))));
						advanceForex.setTotalINR(AppUtility.convertStringToDouble(AppUtility.checkNullString(advanceForexDetailsMap.get("TOTAL_AMOUNT_INR"))));
						
						advanceForexDetailsList.add(advanceForex);
	        	    }
	        	}
    	    	advanceForexDetails.setAdvanceRemarks(remarksDTO.getAdvanceRemarks());
				advanceForexDetails.setCurrencyDominationDetails(remarksDTO.getCashBreakupRemarks());
    	    	advanceForexDetails.setAdvanceAmountDetails(advanceForexDetailsList);
    	    	advanceForexDetails.setTotalAmountINR(requestInfo.getTotalAdvanceAmountINR());
    	    	
				requestInfo.setAdvanceForexDetails(advanceForexDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getAdvanceForexDataForEdit() while fetching advance forex details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	@SuppressWarnings("unchecked")
	private void getAccommodationDetailsDataForEdit(TravelRequestDTO requestInfo, Map<String, Object> requestDataMap,RemarksDTO remarksDTO) {
		
		try {
			Map<String, Object> map = new HashMap<>(requestDataMap);
			Object accommodationDetailsData = map.get("#result-set-6");
			
			if("Y".equals(requestInfo.getAccommodationDetailsFlag()) && !AppUtility.isNull(accommodationDetailsData) && accommodationDetailsData instanceof List) {
				
				List<Map<String, Object>> list = (List<Map<String, Object>>) accommodationDetailsData;
				
				AccommodationDetailsDTO accommodationDetails = new AccommodationDetailsDTO();
				List<AccommodationDTO> accommodationDetailsList = new ArrayList<>();
    	    	
    	    	for(int i=0; i<list.size(); i++) {
	        		
	        		Map<String, Object> accommodationDetailsMap = list.get(i);
	        		
	        		if(!AppUtility.isNull(accommodationDetailsMap)) {
	        			AccommodationDTO accommodation = new AccommodationDTO();
						
	        			accommodation.setAccommodationId((Integer)accommodationDetailsMap.get("TRAVEL_ACC_ID"));
						String stayType = AppUtility.checkNullString(accommodationDetailsMap.get(ParamConstant.TRANSIT_TYPE));
						accommodation.setStayType(AppUtility.convertStringToInt(stayType));
						accommodation.setStayTypeDesc(!AppUtility.isBlankString(stayType) ? AccomodationStayTypes.fromId(stayType).getName() : "");
						
						accommodation.setBudget(AppUtility.convertStringToDouble(AppUtility.checkNullString(accommodationDetailsMap.get("TRANSIT_BUDGET"))));
						accommodation.setCurrency(AppUtility.checkNullString(accommodationDetailsMap.get("BUDGET_CURRENCY_CODE")));
						
						accommodation.setCheckInDate(AppUtility.checkNullString(accommodationDetailsMap.get("CHECK_IN_DATE")));
						accommodation.setCheckOutDate(AppUtility.checkNullString(accommodationDetailsMap.get("CHECK_OUT_DATE")));
						accommodation.setJourneyCode(AppUtility.checkNullString(accommodationDetailsMap.get("JOURNEY_CODE")));
						
						accommodation.setTransitHouseId(AppUtility.convertStringToInt(AppUtility.checkNullString(accommodationDetailsMap.get("TRANSIT_HOUSE_ID"))));
						accommodation.setPlace(AppUtility.checkNullString(accommodationDetailsMap.get("PLACE")));
						
						accommodation.setAddress(!AppUtility.isBlankString(AppUtility.checkNullString(accommodationDetailsMap.get("ACCOMMODATION_ADDRESS"))) ? AppUtility.checkNullString(accommodationDetailsMap.get("ACCOMMODATION_ADDRESS")) : "");
						accommodation.setReason(!AppUtility.isBlankString(AppUtility.checkNullString(accommodationDetailsMap.get("REASON"))) ? AppUtility.checkNullString(accommodationDetailsMap.get("REASON")) : "");
						
						accommodation.setTransitHouseLatitude(AppUtility.checkNullString(accommodationDetailsMap.get("LATITUDE")));
						accommodation.setTransitHouseLongitude(AppUtility.checkNullString(accommodationDetailsMap.get("LONGITUDE")));
						
						accommodationDetailsList.add(accommodation);
	        		}
	        	}
    	    	accommodationDetails.setAccommodations(accommodationDetailsList);
    	    	accommodationDetails.setAccommodationRemarks(remarksDTO.getAccommodationRemarks());
	        	requestInfo.setAccommodationDetails(accommodationDetails);
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestServiceImpl : getAccommodationDetailsDataForEdit() while fetching accommodation details : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@Override
	public List<GroupTravellerDTO> getGroupUserDetailsList(AppRequestDTO appRequestDTO)  throws Exception {
		List<GroupTravellerDTO>  groupTravellerDTOList = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<GroupTraveller> groupTravellerList = requestDAO.getGroupUserDetailsList(appRequest);

		if(AppUtility.containsData(groupTravellerList)) {
			groupTravellerList.forEach(groupTraveller -> {
				GroupTravellerDTO groupTravellerDTO = new GroupTravellerDTO();
				BeanUtils.copyProperties(groupTraveller, groupTravellerDTO);
				groupTravellerDTOList.add(groupTravellerDTO);
			});
		} else {
			return Collections.emptyList();
		}
		return groupTravellerDTOList;
	}
	
	
	@Override
	public GroupTravellerDTO getGroupUserDetails(AppRequestDTO appRequestDTO) throws Exception {
		GroupTravellerDTO  groupTravellerDTO = new GroupTravellerDTO();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<GroupTraveller> groupTravellerList = requestDAO.getGroupUserDetails(appRequest);
		
		if(AppUtility.containsData(groupTravellerList)) {
			GroupTraveller groupTraveller = groupTravellerList.get(0);
			BeanUtils.copyProperties(groupTraveller, groupTravellerDTO);
		} else {
			return groupTravellerDTO;
		}
		return groupTravellerDTO;
	}
	
	@Override
	public List<GroupTravellerDTO> getGroupTravellersDetailsList(AppRequestDTO appRequestDTO) throws Exception {
		List<GroupTravellerDTO>  groupTravellerDTOlist = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<GroupTraveller> groupTravellerList = requestDAO.getGroupTravellersDetailsList(appRequest);
		
		if(AppUtility.containsData(groupTravellerList)) {
			groupTravellerList.forEach(groupTraveller -> {
				GroupTravellerDTO groupTravellerDTO = new GroupTravellerDTO();
				BeanUtils.copyProperties(groupTraveller, groupTravellerDTO);
				groupTravellerDTOlist.add(groupTravellerDTO);
			});
		} else {
			return Collections.emptyList();
		}
		return groupTravellerDTOlist;
	}
	
	@Override
	public GroupTravellerDetailDTO getGroupTravellerDetailsToEdit(AppRequestDTO appRequestDTO) throws Exception {
		GroupTravellerDetailDTO  groupTravellerDetailDTO = new GroupTravellerDetailDTO();
		GroupTravellerDTO groupTravellerDTO              = new GroupTravellerDTO();
		List<AdvanceDTO> advanceDTOList                  = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		GroupTravellerDetail groupTravellerDetail = requestDAO.getGroupTravellerDetailsToEdit(appRequest);
		
		BeanUtils.copyProperties(groupTravellerDetail.getGroupUserDetails(), groupTravellerDTO);
		
		
		if(AppUtility.containsData(groupTravellerDetail.getAdvanceAmountDetails())) {
			
			groupTravellerDetail.getAdvanceAmountDetails().forEach(advanceAmountDetails -> {
				AdvanceDTO advanceDTO = new AdvanceDTO();
				BeanUtils.copyProperties(advanceAmountDetails, advanceDTO);
				advanceDTOList.add(advanceDTO);
			});
		}
		
		groupTravellerDetailDTO.setGroupUserDetails(groupTravellerDTO);
		groupTravellerDetailDTO.setAdvanceAmountDetails(advanceDTOList);
		
		return groupTravellerDetailDTO;
	}
	
	@Override
	public String deleteTravellerDetails(AppRequestDTO appRequestDTO) throws Exception {
		String errorMessage = "";
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		errorMessage = requestDAO.deleteTravellerDetails(appRequest);
		
		return errorMessage;
	}
	
	@Override
	public String saveFeedback(FeedbackRequestDTO feedbackRequestDTO) throws Exception {
		String errorMessage = "";
		FeedbackRequest feedbackRequest = new FeedbackRequest();
		feedbackRequest = (FeedbackRequest) RequestConvertor.convertObjectWithState(feedbackRequestDTO, feedbackRequest);
		
		errorMessage = requestDAO.saveFeedback(feedbackRequest);
		
		return errorMessage;
	}
	
	@Override
	public List<LinkTravelRequestDTO> getTravellerCancelledRequest(AppRequestDTO appRequestDTO) throws Exception {
		List<LinkTravelRequestDTO>  linkTravelRequestDTOList = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<LinkTravelRequest> linkTravelRequestList = requestDAO.getTravellerCancelledRequest(appRequest);
		
		if(AppUtility.containsData(linkTravelRequestList)) {
			linkTravelRequestList.forEach(travelRequest -> {
				LinkTravelRequestDTO linkTravelRequestDTO = new LinkTravelRequestDTO();
				BeanUtils.copyProperties(travelRequest, linkTravelRequestDTO);
				linkTravelRequestDTOList.add(linkTravelRequestDTO);
			});
		} else {
			return Collections.emptyList();
		}
		
		return linkTravelRequestDTOList;
	}
	
	@Override
	public List<TicketTypeDTO> getTicketType(AppRequestDTO appRequestDTO) throws Exception {
		List<TicketTypeDTO>  ticketTypeDTOList = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<TicketType> ticketTypeList = requestDAO.getTicketType(appRequest);
		
		if(AppUtility.containsData(ticketTypeList)) {
			ticketTypeList.forEach(ticketType -> {
				TicketTypeDTO ticketTypeDTO = new TicketTypeDTO();
				BeanUtils.copyProperties(ticketType, ticketTypeDTO);
				ticketTypeDTOList.add(ticketTypeDTO);
			});
		} else {
			return Collections.emptyList();
		}
		
		return ticketTypeDTOList;
	}


	@Override
	public List<CityAirportDTO> getCityAirportList(AppRequestDTO appRequestDTO) throws Exception {
		List<CityAirportDTO>  cityAirportDTOList = new ArrayList<>();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);

		List<CityAirport> cityAirportList = requestDAO.getCityAirportList(appRequest);
		
		if(AppUtility.containsData(cityAirportList)) {
			cityAirportList.forEach(cityAirport -> {
				CityAirportDTO cityAirportDTO = new CityAirportDTO();
				BeanUtils.copyProperties(cityAirport, cityAirportDTO);
				cityAirportDTOList.add(cityAirportDTO);
			});
		} else {
			return Collections.emptyList();
		}
		
		return cityAirportDTOList;
	}
	
	@Override
	public RequestDataDTO saveGroupRequestData(GroupTravelRequestDTO groupTravelRequestDTO) throws Exception {
		
		RequestDataDTO requestDataDTO = new RequestDataDTO();
		
		GroupTravelRequest groupTravelRequest = new GroupTravelRequest();
		groupTravelRequest = (GroupTravelRequest) RequestConvertor.convertObjectWithState(groupTravelRequestDTO, groupTravelRequest);
		
		RequestData requestData = requestDAO.saveGroupRequestData(groupTravelRequest);
		
		if(!AppUtility.isNull(requestData)) {
			BeanUtils.copyProperties(requestData, requestDataDTO);
		}
		
		return requestDataDTO;
	}


	@Override
	public OOOApproverDTO checkOOOApprover(AppRequestDTO appRequestDTO) throws Exception {

		OOOApproverDTO oooApproverDTO = new OOOApproverDTO();
		AppRequest appRequest = new AppRequest();
		BeanUtils.copyProperties(appRequestDTO, appRequest);
		
		OOOApprover oooApprover = requestDAO.checkOOOApprover(appRequest);
		
		if(!AppUtility.isNull(oooApprover)) {
			BeanUtils.copyProperties(oooApprover, oooApproverDTO);
		}
		return oooApproverDTO;
	}
	
}
