package com.mind.api.stars.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mind.api.stars.dao.MastersDataDAO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.AttachmentRequestDTO;
import com.mind.api.stars.dto.AttachmentResponseDTO;
import com.mind.api.stars.dto.CityDTO;
import com.mind.api.stars.dto.CityResponseDTO;
import com.mind.api.stars.dto.CountryDTO;
import com.mind.api.stars.dto.CountryResponseDTO;
import com.mind.api.stars.dto.CurrencyDTO;
import com.mind.api.stars.dto.TimeDTO;
import com.mind.api.stars.dto.UnitDTO;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Attachment;
import com.mind.api.stars.model.City;
import com.mind.api.stars.model.CityResponse;
import com.mind.api.stars.model.Country;
import com.mind.api.stars.model.CountryResponse;
import com.mind.api.stars.model.Currency;
import com.mind.api.stars.model.Time;
import com.mind.api.stars.model.Unit;
import com.mind.api.stars.service.MastersDataService;
import com.mind.api.stars.utility.ExceptionLogger;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MastersDataServiceImpl implements MastersDataService {

	@Resource
	private MastersDataDAO mastersDataDAO;
	
	
	@Override
	public CityResponseDTO getCityMasterData(AppRequestDTO appRequestDTO) {
		CityResponseDTO cityResponseDTO = new CityResponseDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			CityResponse cityResponse = mastersDataDAO.getCityMasterData(appRequest); 
			
			if(cityResponse != null) {
				cityResponseDTO.setLastSyncDate(cityResponse.getLastSyncDate());
				cityResponseDTO.setSyncMessage(cityResponse.getSyncMessage());
				
				List<City> cities = cityResponse.getCities();
				List<CityDTO> citiesDTOList = new ArrayList<>();
				
				if (cities != null && !cities.isEmpty()) {
					cities.forEach(city -> {
		        		CityDTO cityDTO = new CityDTO();
			        	BeanUtils.copyProperties(city, cityDTO);
			        	
			        	citiesDTOList.add(cityDTO);
			        });
					
					cityResponseDTO.setCities(citiesDTOList);
				
				} else {
					cityResponseDTO.setCities(Collections.emptyList());
				}
			}
	        
		} catch (Exception e) {
			log.error("Error occurred in MastersDataServiceImpl : getCityMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return cityResponseDTO;
	}
	
	
	@Override
	public CountryResponseDTO getCountryMasterData(AppRequestDTO appRequestDTO) {
		CountryResponseDTO countryResponseDTO =  new CountryResponseDTO();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			CountryResponse countryResponse = mastersDataDAO.getCountryMasterData(appRequest); 
			
			if(countryResponse != null) {
				countryResponseDTO.setLastSyncDate(countryResponse.getLastSyncDate());
				countryResponseDTO.setSyncMessage(countryResponse.getSyncMessage());
				
				List<Country> countries = countryResponse.getCountries();
				List<CountryDTO> countriesDTOList = new ArrayList<>();
				
				if (countries != null && !countries.isEmpty()) {
					countries.forEach(country -> {
						CountryDTO countryDTO = new CountryDTO();
			        	BeanUtils.copyProperties(country, countryDTO);
			        	
			        	countriesDTOList.add(countryDTO);
			        });
					
					countryResponseDTO.setCountries(countriesDTOList);
				
				} else {
					countryResponseDTO.setCountries(Collections.emptyList());
				}
			}
	        
		} catch (Exception e) {
			log.error("Error occurred in MastersDataServiceImpl : getCountryMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return countryResponseDTO;
	}


	@Override
	public List<TimeDTO> getTimeMasterData(AppRequestDTO appRequestDTO) {
		List<TimeDTO> timeDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Time> timeList	 	= mastersDataDAO.getTimeMasterData(appRequest);
		
			if (timeList == null) {
	            return Collections.emptyList();
	        
			} else {
	        	timeList.forEach(time -> {
	        		TimeDTO timeDTO = new TimeDTO();
		        	BeanUtils.copyProperties(time, timeDTO);
		        	
		        	timeDTOList.add(timeDTO);
		        });
	        }
	        
		} catch (Exception e) {
			log.error("Error occurred in MastersDataServiceImpl : getTimeMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return timeDTOList;
	}


	@Override
	public List<UnitDTO> getUnitMasterData(AppRequestDTO appRequestDTO) {
		
		List<UnitDTO> unitDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Unit> unitList	 	= mastersDataDAO.getUnitMasterData(appRequest);
		
			if (unitList == null) {
	            return Collections.emptyList();
	        
			} else {
				unitList.forEach(unit -> {
	        		UnitDTO unitDTO = new UnitDTO();
		        	BeanUtils.copyProperties(unit, unitDTO);
		        	
		        	unitDTOList.add(unitDTO);
		        });
	        }
	        
		} catch (Exception e) {
			log.error("Error occurred in MastersDataServiceImpl : getUnitMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return unitDTOList;
	}
	
	@Override
	public List<CurrencyDTO> getCurrencyMasterData(AppRequestDTO appRequestDTO) {
		
		List<CurrencyDTO> currencyDTOList = new ArrayList<>();
		
		try {
			AppRequest appRequest = new AppRequest();
			BeanUtils.copyProperties(appRequestDTO, appRequest);
			
			List<Currency> currencyList	 	= mastersDataDAO.getCurrencyMasterData(appRequest);
		
			if (currencyList == null) {
	            return Collections.emptyList();
	        
			} else {
				currencyList.forEach(currency -> {
					CurrencyDTO currencyDTO = new CurrencyDTO();
		        	BeanUtils.copyProperties(currency, currencyDTO);
		        	
		        	currencyDTOList.add(currencyDTO);
		        });
	        }
	        
		} catch (Exception e) {
			log.error("Error occurred in MastersDataServiceImpl : getCurrencyMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return currencyDTOList;
	}


	@Override
	public AttachmentResponseDTO uploadAttachment(AttachmentRequestDTO attachmentRequestDTO) {
		
		AttachmentResponseDTO response = new AttachmentResponseDTO();
		
		try {
			Attachment attachment = new Attachment();
			BeanUtils.copyProperties(attachmentRequestDTO, attachment);
			response = mastersDataDAO.uploadAttachment(attachment);
			
		} catch(Exception ex) {
			log.error("Error occurred in MastersDataServiceImpl : uploadAttachment() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return response;
	}
	
	@Override
	public List<AttachmentResponseDTO> getAttachment(AttachmentRequestDTO attachmentRequestDTO) {
		
		List<AttachmentResponseDTO> attachmentList = new ArrayList<AttachmentResponseDTO>();
		
		try {
			Attachment attachment = new Attachment();
			BeanUtils.copyProperties(attachmentRequestDTO, attachment);
			List<Attachment> responseList = mastersDataDAO.getAttachment(attachment);
			
			if (responseList == null) {
	            return Collections.emptyList();
	        
			} else {
				responseList.forEach(attach -> {
					AttachmentResponseDTO attachmentResponseDTO = new AttachmentResponseDTO();
		        	BeanUtils.copyProperties(attach, attachmentResponseDTO);
		        	
		        	attachmentList.add(attachmentResponseDTO);
		        });
	        }
	        
			
		} catch(Exception ex) {
			log.error("Error occurred in MastersDataServiceImpl : getAttachment() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return attachmentList;
	}
	
}
