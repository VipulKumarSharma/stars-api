package com.mind.api.stars.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mind.api.stars.dao.AppDAO;
import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.model.LoginBean;
import com.mind.api.stars.model.UserInfo;
import com.mind.api.stars.service.AppService;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AppServiceImpl implements AppService {
	
	@Resource
	private AppDAO appDao;

	@Override
	public UserInfoDTO getUserInfo(LoginDTO loginDTO) {
		UserInfoDTO userInfoDTO = null;
		
		try {
			LoginBean loginBean = new LoginBean();
			BeanUtils.copyProperties(loginDTO, loginBean);
			
			UserInfo userInfo = appDao.getUserInfo(loginBean);
			
			if(!AppUtility.isNull(userInfo)) {
				userInfoDTO = new UserInfoDTO();
				BeanUtils.copyProperties(userInfo, userInfoDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in AppServiceImpl : getUserInfo() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return userInfoDTO;
	}
	
	@Override
	public UserInfoDTO getAuthenticatedUserDetails(UserInfoDTO userInformationDTO) {
		UserInfoDTO userInfoDTO = null;
		
		try {
			UserInfo userInfo = new UserInfo();
			BeanUtils.copyProperties(userInformationDTO, userInfo);
			
			UserInfo userInformation = appDao.getAuthenticatedUserDetails(userInfo);
			
			if(!AppUtility.isNull(userInformation)) {
				userInfoDTO = new UserInfoDTO();
				BeanUtils.copyProperties(userInformation, userInfoDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in AppServiceImpl : getAuthenticatedUserDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return userInfoDTO;
	}
	
	@Override
	public UserInfoDTO acceptPolicy(UserInfoDTO userInformationDTO) {
		UserInfoDTO userInfoDTO = null;
		
		try {
			UserInfo userInfo = new UserInfo();
			BeanUtils.copyProperties(userInformationDTO, userInfo);
			
			UserInfo userInformation = appDao.acceptPolicy(userInfo);
			
			if(!AppUtility.isNull(userInformation)) {
				userInfoDTO = new UserInfoDTO();
				BeanUtils.copyProperties(userInformation, userInfoDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in AppServiceImpl : acceptPolicy() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return userInfoDTO;
	}

	@Override
	public UserInfoDTO ssoLogin(String winUserId, String domain) {
		UserInfoDTO userInfoDTO = null;
		
		try {
			UserInfo userInformation = appDao.ssoLogin(winUserId, domain);
			
			if(!AppUtility.isNull(userInformation)) {
				userInfoDTO = new UserInfoDTO();
				BeanUtils.copyProperties(userInformation, userInfoDTO);
			}
		
		} catch (Exception e) {
			log.error("Error occurred in AppServiceImpl : ssoLogin() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
			
		return userInfoDTO;
	}

	@Override
	public void logout(int loggedInUserId, String ipAddress) {
		appDao.logout(loggedInUserId, ipAddress);
	}

}
