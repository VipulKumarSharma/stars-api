package com.mind.api.stars.service;

import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class StatusChecker implements HealthIndicator {

	@Override
	public Health health() {
		try {
			URL siteURL = new URL("http://stars.mindeservices.com");
			HttpURLConnection connection = (HttpURLConnection)siteURL.openConnection();
			
			connection.setRequestMethod("GET");
			connection.connect();
			
			int code = connection.getResponseCode();
			
			if (code == 200) {
				return Health.up().withDetail("STARS", "Production server is running").build();
			} else {
				return Health.down().withDetail("Error", "Production server is down").build();
			}
		
		} catch(Exception e) {
			return Health.down().withDetail("Error", "Production server is unreachable").build();
		}
	}

	

}
