package com.mind.api.stars.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ModelAndViewConstants;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.enums.APICallAttributes;
import com.mind.api.stars.utility.AppUtility;

public class TemplateInterceptor extends HandlerInterceptorAdapter {
	
	private static final String DEFAULT_VIEW_ATTRIBUTE_NAME = "view";
	private static List<String> pages = new ArrayList<>();
	
	static {
		pages.add(ModelAndViewConstants.API_HOME);
		pages.add(ModelAndViewConstants.SERVER_MAINTAINANCE);
		pages.add(ModelAndViewConstants.ERROR);
		pages.add(ModelAndViewConstants.ERROR_PAGE);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		
		String tokenId = request.getHeader(APICallAttributes.TOKEN_ID.getKey());
		HttpSession httpSession = request.getSession(false);
		if(httpSession!=null) {
			 UserInfoDTO userInfoDTO = (UserInfoDTO) httpSession.getAttribute(Constants.USER);
			 tokenId = userInfoDTO==null?"":userInfoDTO.getTokenId();
		}
		if (AppUtility.isNull(modelAndView) || !modelAndView.hasView() || modelAndView.getViewName().contains("redirect:")) {
			
		} else if(pages.contains(modelAndView.getViewName())) {
			modelAndView.setViewName(modelAndView.getViewName());
			modelAndView.addObject(DEFAULT_VIEW_ATTRIBUTE_NAME, modelAndView.getViewName());
		
		} else if(tokenId==null || tokenId.isEmpty()) {
			
			setTemplateByViewName(modelAndView, "login/login");
			setViewForTemplate(modelAndView, "login/login");
			
		} else {
			String originalViewName = modelAndView.getViewName();			
			setTemplateByViewName(modelAndView, originalViewName);
			setViewForTemplate(modelAndView, originalViewName);
		}	
	}
	
	private void setTemplateByViewName(ModelAndView modelAndView, String originalViewName) {
		
		if(ModelAndViewConstants.LOGIN.equals(originalViewName)) {
			modelAndView.setViewName(ModelAndViewConstants.LOGIN_LAYOUT);
		} else {
			modelAndView.setViewName(ModelAndViewConstants.DEFAULT_LAYOUT);
		}
	}
	
	private void setViewForTemplate(ModelAndView modelAndView, String originalViewName) {
		
		if(!AppUtility.checkNull(originalViewName) && originalViewName.contains("redirect:")) {
			originalViewName = originalViewName.substring(originalViewName.indexOf(':')+1, originalViewName.length());
		}
		
		modelAndView.addObject(DEFAULT_VIEW_ATTRIBUTE_NAME, originalViewName);
	}
	
}
