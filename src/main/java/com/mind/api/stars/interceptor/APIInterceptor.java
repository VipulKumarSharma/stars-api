package com.mind.api.stars.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.dto.ApiResponseDTO;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.model.UserInfo;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestParsingUtility;
import com.mind.api.stars.utility.ResponseGenerator;

public class APIInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private HttpSession httpSession;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HttpSession userSession = request.getSession(false);
		if(RequestParsingUtility.validateRequest(request)) {
			return true;
		}
		else {
			if( AppUtility.checkNull(userSession)) 
			{
				ExceptionLogger.logExceptionToDB(new Exception("session invalidated for request "+request.getRequestURL()));
				ApiResponseDTO apiResponseDTO = setFailureResponseStatus();
				String responseData=ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), "Invalid token", null, false);
				response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
				response.getWriter().write(responseData);
				return false;
			}
			else {
				response.sendRedirect("/");
				return false;
			}
		}
			
	}
	private ApiResponseDTO setFailureResponseStatus() {
		return new ApiResponseDTO(Constants.ERROR_CODE_500,Constants.FAILURE) ;
	}
}
