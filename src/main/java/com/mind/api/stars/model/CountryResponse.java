package com.mind.api.stars.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CountryResponse {
	
	private List<Country> countries;
	private String lastSyncDate;
	private String syncMessage;
}
