package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Hotel {

	private String name;
	private String star;
	private String rate;
	
}
