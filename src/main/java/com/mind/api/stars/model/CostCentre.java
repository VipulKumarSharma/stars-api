package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CostCentre {
	
	private String costCentreId;
	private String costCentreName;

}
