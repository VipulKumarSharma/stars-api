package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class WorkflowApprover {

	private String name;
	private String designationName;
	private String approveDate;
	private String approveStatus;
	private String originalApprover;
	
}
