package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class City {

	private int cityId;
	private String cityCode;
	private String cityName;
	private int countryId;
	private String countryCode;
	private String countryName;
	private String recordStatus;
	
}
