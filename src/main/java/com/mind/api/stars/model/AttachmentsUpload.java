package com.mind.api.stars.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AttachmentsUpload {
	
	private String attachmentId;
	private String uUID;
	private String fileName;
	private String fileExtension;
	private String fileSize;
	private MultipartFile file; 
}
