package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Currency {

	private String currencyId;
	private String currencyName;
	private String currencyDesc;
	private String currencyCode;
	private String currencySymbol;
	private String baseCurrencyFlag;
	
}
