package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CarCategory {
	
	private String carCategoryId;
	private String carCategoryName;

}
