package com.mind.api.stars.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
@JacksonXmlRootElement(localName="accommodationDetails")
public class AccommodationDetails {

	@JacksonXmlElementWrapper(localName="accommodation")
	@JacksonXmlProperty(localName="stay")
	private List<Accommodation> accommodations;
	
	private String accommodationRemarks;
	
}
