package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Booking {

	private String source;
	private String destination;
	private String paidAmount;
	private String airline;
	private String date;
	
}
