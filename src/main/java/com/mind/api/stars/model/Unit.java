package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Unit {
	
	private String siteId;
	private String siteName;
	private String isPrimarySite;
}
