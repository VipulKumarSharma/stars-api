package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LinkTravelRequest {

	private long travelId;
	private String travelType;
	private String travelReqNo;
	private String travelStatus;
	
}
