package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestBean {

	private String winUserId;
	private String domainName;
	private String requestSource;
	private String deviceId;
	private String systemName;
}
