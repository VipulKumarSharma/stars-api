package com.mind.api.stars.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class ExchangeRateRequest {
	
	private String selectedCurrency;
	private String currentDate;
	
	private String days;
	private String previousTotal;
	private String baseCurrency;
	private String selectedTotal;
}
