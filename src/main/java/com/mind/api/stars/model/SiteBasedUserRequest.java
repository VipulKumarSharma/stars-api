package com.mind.api.stars.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SiteBasedUserRequest {
	
	private String winUserId;
	private String domainName;
	private String selectedSiteId;
	private String syncDate;

}
