package com.mind.api.stars.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
@JacksonXmlRootElement(localName="budgetActualDetails")
public class BudgetActual {

	private double ytmBudget;
	private double ytdActual;
	private double availableBudget;
	private double estimate;
	private String expRemarks;

}
