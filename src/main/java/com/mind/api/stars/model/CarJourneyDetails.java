package com.mind.api.stars.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
@JacksonXmlRootElement(localName="carJourneyDetails")
public class CarJourneyDetails {

	private int carClass = 0;
	private int carCategory	= 0;
	private String carClassDesc = "";
	private String carCategoryDesc = "";
	
	@JacksonXmlElementWrapper(localName="carJourney")
	@JacksonXmlProperty(localName="journey")
	private List<CarJourney> carJourney;
	
	private String journeyType = "O";
	private String returnDate = "";
	private int returnTimeMode = -1;
	private int returnTime = 0;
	private String returnTimeDesc = "";
	private String returnTimeModeDesc = Constants.NO_PREFERENCE;
	private String carJourneyRemarks = "";
	
}
