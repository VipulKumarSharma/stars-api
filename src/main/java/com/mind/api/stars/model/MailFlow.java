package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MailFlow {

	private String mailId;
	private String mailFrom;
	private String mailTo;
	private String mailCC;
	private String mailSubject;
	private String mailCreator;
	private String mailStatus;
	private String mailCreatedDate;
	private String mailSendDate;
}
