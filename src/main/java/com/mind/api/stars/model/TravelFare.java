package com.mind.api.stars.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
@JacksonXmlRootElement(localName="totalTravelFareDetails")
public class TravelFare {

	private String currency = "";
	private String currencyCode = "";
	private double fareAmount = 0.0d;
	
}
