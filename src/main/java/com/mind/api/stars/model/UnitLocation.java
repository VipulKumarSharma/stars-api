package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UnitLocation {
	
	private String locationId;
	private String locationName;
	private String siteId;
	private String gstNo;
	private String lastLocationId;
}
