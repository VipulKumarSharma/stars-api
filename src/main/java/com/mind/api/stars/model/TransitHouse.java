package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransitHouse {
	
	private String transitHouseId;
	private String transitHouseName;
	private String transitHouseAddress;
	private String areaCode;
	private String longitude;
	private String latitude;
	
}
