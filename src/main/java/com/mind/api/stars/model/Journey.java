package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Journey {
	
	private long travelId;
	private int travellerId;
	private String travelType;
	private String requestNo;
	private String source;
	private String destination;
	private String departureDate;
	private String returnDate;
	private int daysLeft;
	private String departureTime;
	private String returnTime;
	
}
