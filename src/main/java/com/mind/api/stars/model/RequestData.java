package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class RequestData {

	private String travelId;
	private String requestNo;
	private String requestStatus;

}
