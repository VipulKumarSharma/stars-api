package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class DefaultApprover {

	private int id;
	private int siteId;
	private int orderId;
	private String role;
	
}
