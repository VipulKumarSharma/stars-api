package com.mind.api.stars.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JacksonXmlRootElement(localName="nonMATASourceDetails")
public class NonMATASourceDetails {

	private String nonMATASourceFlag = "";
	private String airlineName = "";
	private String currency = "";
	private double amount = 0.0d;
	private String remarks = "";

}
