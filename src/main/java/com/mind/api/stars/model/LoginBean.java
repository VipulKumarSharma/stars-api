package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class LoginBean {

	private String userName;
	private String encPassword;
	private String language;

}
