package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
public class AppRequest extends RequestBean {

	private int requestCount;
	private int last;
	private int countryId;
	private int travellerId;
	private int travelAgencyId = -1;
	private int siteId;
	private int billingSiteId;
	private int carClass;
	private int approverId;
	
	private long travelId;
	
	private String travelType;
	private String requestType;
	private String syncDate;
	private String source;
	private String destination;
	private String pageType;
	private int identityId;
	
	private String commentId;
	private String type;
	private String travelReqId;
	private String requestNo;
	private String mailId;
	private String attachmentId;
	
	private String travellerType;
	private String firstName;
	private String gUserId;
	private String cityAirportName;
	
	private int approverLevel1 = 0;
	private int approverLevel2 = 0;
	private int approverLevel3 = 0;
	
}
