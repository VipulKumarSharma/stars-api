package com.mind.api.stars.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
@JacksonXmlRootElement(localName="advanceForexDetails")
public class AdvanceDetails {

	@JacksonXmlElementWrapper(localName="advance")
	@JacksonXmlProperty(localName="currency")
	private List<Advance> advanceAmountDetails;
	
	private double totalAmountINR				= 0.0d;
	private double totalUSDPerDay				= 0.0d;
	private String advanceRemarks				= "";
	private String currencyDominationDetails	= "";
		
}
