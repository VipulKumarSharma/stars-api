package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PassportDetail {
	
	private String passportNo;
	private String nationality;
	private String issuePlace;
	private String ppIssueCountryId;
	private String issueDate;
	private String expiryDate;
	private String dateOfBirth;
	
}
