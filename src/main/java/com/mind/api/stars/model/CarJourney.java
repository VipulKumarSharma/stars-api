package com.mind.api.stars.model;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CarJourney {
	
	private long carJourneyId = 0;
	private int journeyOrder = 0;
	private String journeyCode = "";
	private String source = "";
	private String destination = "";
	private String dateOfJourney = "";
	private int preferredTimeMode = -1;
	private int preferredTime = 0;
	private int location = 1;
	private String mobileNo	= "";
	
	private String type = "";
	private String preferredTimeModeDesc= Constants.NO_PREFERENCE;
	private String preferredTimeDesc = "";
	private String locationDesc = Constants.TRANSIT_HOUSE;
	private String carClass = "";
	private String carCategory = "";
}
