package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApproverDetail {
	
	private String approverId;
	private String approverName;
	private String lastApproverId;
}
