package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class TicketType {
	
	private int ticketTypeId;
	private String ticketTypeName;
	private String recordStatus;
}
