package com.mind.api.stars.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TokenResult {
	
	private String status;
	private String message;
	private TokenData data;

}
