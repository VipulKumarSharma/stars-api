package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class VisaDetail {
	
	private String country;
	private String comment;
	private String validFrom;
	private String validTo;
	private String stayDuration;
	
}
