package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class CityAirport {
	
	private String category;
	private String cityName;
	private String airportName;
	private String airportCode;
	private String countryName;
	private String countryCode;
	private int countryId;
}
