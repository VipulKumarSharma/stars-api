package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class FeedbackRequest extends RequestBean {
	
	private String likeMostFeedback;
	private String specificFeedback;
	private String lookAndFeelRating;
	private String creatingRequestRating;
	private String approvingRequestRating;
	private String systemSpeedRating;
	private String supportRating;
	private String overallRating;
	private String applicationId;
	
}
