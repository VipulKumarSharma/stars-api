package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TimeMode {
	
	private String timeModeId;
	private String timeModeName;
}
