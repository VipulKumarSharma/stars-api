package com.mind.api.stars.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter 
@Setter 
@ToString
public class ApproverLevelWise {

	private List<ApproverDetail>  approverLevel1;
	private List<ApproverDetail>  approverLevel2;
	private List<ApproverDetail>  approverLevel3;
}
