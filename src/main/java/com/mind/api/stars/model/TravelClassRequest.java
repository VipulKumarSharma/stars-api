package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelClassRequest {
	
	private Integer siteId;
	private Integer travellerId;
	private String travelType;
	private Integer modeId;
	private String groupTravelFlag;
	private String syncDate;
}
