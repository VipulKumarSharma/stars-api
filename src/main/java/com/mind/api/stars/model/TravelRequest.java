package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString(callSuper=true)
public class TravelRequest extends RequestBean {

	private long requestId = 0 ;
	private long travelId = 0;
	private long linkedTravelRequestId = 0;
	
	private String requestType = "";
	private String travelRequestNo = "";
	private String linkedTravelRequestNo = "";
	
	private int siteId = 0;
	private int costCentreId = 0;
	private String siteName = "";
	private String costCentreName = "";
	
	private int originatorId = 0;
	private int travellerId = 0;
	private String originatorName = "";
	private String travellerName = "";
	
	private int preferredMealId = 0;
	private String preferredMealDesc = "";

	private String flightTravelFlag;
	private String trainTravelFlag;
	private String carTravelFlag;
	private String accommodationDetailsFlag;
	private String otherAccommodationsFlag;
	private String advanceForexFlag;
	private String budgetActualDetailsFlag;
	private String totalTravelFareDetailsFlag;
	private String groupTravelFlag;
	private String travelVisaFlag;
	private String localAgentFlag;
	
	private FlightJourneyDetails flightJourneyDetails;
	private TrainJourneyDetails trainJourneyDetails;
	private CarJourneyDetails carJourneyDetails;
	private AccommodationDetails accommodationDetails;
	private AdvanceDetails advanceForexDetails;
	private BudgetActual budgetActualDetails;
	private TravelFare totalTravelFareDetails;
	private NonMATASourceDetails nonMATASourceDetails;
	
	private int siteLocation = 0;
	private String siteLocationDesc = "";
	private String gstNo = "";
	
	private int billingClient = 0;
	private int billingApprover = 0;
	private String billingClientName = "";
	private String billingApproverName = "";
	
	private int approverLevel1 = 0;
	private int approverLevel2 = 0;
	private int approverLevel3 = 0;
	private String approverLevel1Name = "";
	private String approverLevel2Name = "";
	private String approverLevel3Name = "";
	
	private String reasonForTravel = "";
	private String reasonForSkip = "";
	private String travelType = "";
	private String baseCurrency = "";
	private String submitFlag = "N";
	
	private double totalAdvanceAmountINR = 0.0;
	
}
