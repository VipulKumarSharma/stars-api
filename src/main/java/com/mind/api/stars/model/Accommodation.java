package com.mind.api.stars.model;

import com.mind.api.stars.common.Constants;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Accommodation {

	private int accommodationId	= 0;
	private String journeyCode = "";
	
	private int stayType = 2;
	private String currency = "";
	private double budget = 0.00d;
	private String checkInDate = "";
	private String checkOutDate = "";
	private String place = "";
	private String address = "";
	private String reason = "";

	private String stayTypeDesc = Constants.TRANSIT_HOUSE;
	private int transitHouseId = 0;
	
	private String transitHouseLatitude = "";
	private String transitHouseLongitude = "";
	
}
