package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class DeleteJourneyDetailRequest extends AppRequest {

	private Integer modeId;
	private String travelType;
	private String travelCarIds;
	private String travelAccommIds;
}
