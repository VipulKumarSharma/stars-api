package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenData {
	
	private String tokenId;
	private String domainName;
	private String userId;
	private String sessionTimeOut;
	private String authKey;
	private String timeStamp;
	private String requestFrom;
	
}
