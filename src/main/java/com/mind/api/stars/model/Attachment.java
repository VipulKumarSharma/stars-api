package com.mind.api.stars.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Attachment {
	
	private String winUserId;
	private String domainName;
	private String requestSource;
	private String deviceId;
	private Long travelId;
	private String travelType;
	private String attachmentId;
	private String fileName;
	private String fileType;
	private String fileSize;
	private String fileExtension;
	private MultipartFile file;
	private String uploadedBy;
	private String uploadedOn;
	
	private String originatorId;
	private String travelStatusId;
	private String docRef;
	private String fileLocation;
	private String uploadedByName;
	private String deleteEnableFlag;
	private String uUID;
	private byte[] fileBytes;
	
}
