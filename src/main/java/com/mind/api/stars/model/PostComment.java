package com.mind.api.stars.model;


import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter 
public class PostComment extends RequestBean {

	private String travelId;
	private String commentDesc;
	private String travelType;
	
}
