package com.mind.api.stars.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString
public class Comment {
	
	private String commentId;
	private String commentDesc;
	private String postedById;
	private String postedByName;
	private String postedOnDate;
	private String cancelId;
	private String deleteEnableFlag;
	
}
