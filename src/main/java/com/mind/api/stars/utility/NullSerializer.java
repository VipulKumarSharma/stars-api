package com.mind.api.stars.utility;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class NullSerializer extends JsonSerializer<Object> {
	
   public void serialize(Object value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
       jsonGenerator.writeString("");
   }
   
}