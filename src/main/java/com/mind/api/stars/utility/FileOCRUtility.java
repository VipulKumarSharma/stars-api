package com.mind.api.stars.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.json.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.mind.api.stars.common.FileOCRConstants;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileOCRUtility {
	
	
	public JSONObject uploadFileToOCR(MultipartFile file, Map<String,String> mapData, HttpServletRequest request,ServletContext servletContext){
		JSONObject result = null;
		FileOCRConstants fileOCRConstants = new FileOCRConstants();
		try {
			
			String attachmentId = mapData.get("attachmentId")==null?"":mapData.get("attachmentId");
			String companyId = mapData.get("companyId")==null?"":mapData.get("companyId");
			String winUserId = mapData.get("winUserId")==null?"":mapData.get("winUserId");
			String domainName = mapData.get("domainName")==null?"":mapData.get("domainName");
			String category = mapData.get("category")==null?"":mapData.get("category");
			String siteId = mapData.get("siteId")==null?"":mapData.get("siteId");
			String isOcrEnable = mapData.get("isOcrEnable")==null?"0":(mapData.get("isOcrEnable").equalsIgnoreCase("y")?"1":"0");

			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			long fileSize = 0;
			fileSize = file.getSize();
			fileSize = fileSize/1024;
			
			Map<String,String> values = new HashMap<String, String>();
			values.put("fileName", file.getName());
			values.put("fileSize", String.valueOf(fileSize));
			values.put("fileType", FilenameUtils.getExtension(file.getName()));
			values.put("win_user_id", winUserId);
			values.put("companyId", companyId); 
			values.put("domain_name", domainName);
			values.put("buld", siteId); // siteId
			values.put("deviceType", fileOCRConstants.getDeviceType());
			values.put("category", category);  // category
			values.put("checkSum", generateCheckSum(file.getBytes()));
			values.put("ocr_req", isOcrEnable);
			values.put("file_unique_id", attachmentId);
			values.put("strUDID", request.getRemoteAddr());
			values.put("appKey", fileOCRConstants.getWebAppKey());
			headers.setAll(values);
			String uploadPath = servletContext.getRealPath("");
			File uploadDir = new File(uploadPath);
	        if (!uploadDir.exists()) {
	            uploadDir.mkdir();
	        }
	        
            String fPath = uploadPath;
            FileOutputStream fout=new FileOutputStream(fPath+"\\"+values.get("fileName"));    
            fout.write(file.getBytes());    
            fout.close();
            
            File actualFile = new File(fPath+"\\"+values.get("fileName"));
			
			MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
			map.add("file",  new FileSystemResource(actualFile));
			HttpEntity<MultiValueMap<String, Object>> req = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
			ResponseEntity<String> resp = restTemplate.exchange(fileOCRConstants.getDocumentUrl() , HttpMethod.POST , req , String.class );
			
			result = new JSONObject(resp.getBody());
			
			actualFile.delete();			
			
		} catch (RestClientException e) {
			 log.error("Error occurred while upload OCR details : ", e);
		} catch (IOException e) {
			 log.error("Error occurred while upload OCR details : ", e);
		}catch (Exception e) {
			 log.error("Error occurred while upload OCR details : ", e);
		}
		return result;
	}
	
	public byte[] getFile(JSONObject jsonObject){
		FileOCRConstants fileOCRConstants = new FileOCRConstants();
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> req = new HttpEntity<String>(jsonObject.toString());
		ResponseEntity<ByteArrayResource> resp = restTemplate.exchange(fileOCRConstants.getGetFileUrl() ,HttpMethod.POST , req , ByteArrayResource.class );
		byte[] bytes = null;
		try {
			InputStream in  = resp.getBody().getInputStream();		
			bytes = IOUtils.toByteArray(in);
		} catch (IOException e) {
			 log.error("Error occurred while get file : ", e);
		}
		
		return bytes;
	}
	
	public JSONObject getOCRData(JSONObject jsonObject, HttpServletRequest request){
		
		FileOCRConstants fileOCRConstants = new FileOCRConstants();
		HttpSession session = request.getSession(false);
		String cancelFlag = session.getAttribute("cancelOcr") == null ? "" : session.getAttribute("cancelOcr").toString();
		JSONObject json = null;
		boolean stopWaiting=false;
		if(cancelFlag.equalsIgnoreCase("")){
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> req = new HttpEntity<String>(jsonObject.toString());
			ResponseEntity<String> resp = restTemplate.exchange(fileOCRConstants.getGetResultUrl() , HttpMethod.POST , req , String.class );
		
			try {
				json = new JSONObject(resp.getBody());
				if(json.getString("success").equalsIgnoreCase("200")){
					if(json.getString("ocr_fields_data")!=null){
						stopWaiting = true;
					}
				}
			} catch (Exception e) {
				 log.error("Error occurred while fetch OCR details : ", e);
			}
			
			if(!stopWaiting){
				try {
					Thread.sleep(1000*5); // reduce sleep time from 30 to 5
				} catch (InterruptedException e) {
					 log.error(" : ", e);
				}
			}
		}
		return json;
	}
	
	
	public String generateCheckSum(byte[] fileContents) {
		StringBuilder sb = new StringBuilder("");
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			md.update(fileContents);
			byte[] mdbytes = md.digest();
			// convert the byte to hex format
			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException noSuchAlgorithmException) {
			 log.error("Error occurred while generating CheckSum : ", noSuchAlgorithmException);
		} catch (Exception exception) {
			 log.error("Error occurred while generating CheckSum : ", exception);
		}
		return sb.toString();
	}

}
