package com.mind.api.stars.utility;


import com.mind.api.stars.controller.EmailService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class MailUtility  extends Thread {

	
	
	private String fromEmail;
	private String toEmail;
	private String subject;
	private String messageBody;
	private Boolean attachment;
	private EmailService emailService;
	
	
	public MailUtility() {
		
	}
	
	public MailUtility(String fromEmail, String toEmail, String subject, String messageBody,EmailService emailService) {
		this.fromEmail = fromEmail;
		this.toEmail = toEmail;
		this.subject = subject;
		this.messageBody = messageBody;
		this.emailService = emailService;
	}
	
	public MailUtility(String fromEmail, String toEmail, String subject, String messageBody,EmailService emailService, Boolean attachment) {
		this.fromEmail = fromEmail;
		this.toEmail = toEmail;
		this.subject = subject;
		this.messageBody = messageBody;
		this.emailService = emailService;
		this.attachment = attachment;
	}
	
	@Override
	public void run() {
		emailService.sendSimpleMessage(fromEmail, toEmail, subject, messageBody);
		log.info("Mail sent successfully to "+toEmail);
	}

}
