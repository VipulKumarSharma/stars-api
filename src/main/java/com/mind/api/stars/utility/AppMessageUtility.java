package com.mind.api.stars.utility;

import java.util.Locale;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class AppMessageUtility{
	
	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;
	
	
	 public String getLabel(String key,Object args[]) {

	      try 
	      {
	    	return messageSource.getMessage(key, args, Locale.ENGLISH);
	      }
	      catch(Exception ex){
	    	  ExceptionLogger.logExceptionToDB(ex);
	    	  log.info("error in getting lable for key "+key+this.getClass().getName());
	    	  return null;
	      }
	   }
	 /**
	  * 
	  * @param key
	  * @return message from resource bundle based on the key
	  * 
	  */
	 public String getLabel(String key) {

	      try 
	      {
	    	String message = messageSource.getMessage(key, null, Locale.ENGLISH);
	    	return AppUtility.checkNullString(message);
	      }
	      catch(Exception ex){
	    	  ExceptionLogger.logExceptionToDB(ex);
	    	  log.info("error in getting lable for key "+key+this.getClass().getName());
	    	  return null;
	      }
	   }
	
	 
}
