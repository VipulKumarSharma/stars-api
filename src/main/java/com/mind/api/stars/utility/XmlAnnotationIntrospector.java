package com.mind.api.stars.utility;

import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlAnnotationIntrospector;
import com.mind.api.stars.model.RequestDetails.IgnoreRequestDetail;

public class XmlAnnotationIntrospector extends JacksonXmlAnnotationIntrospector {
	
	private static final long serialVersionUID = -4803297031525292283L;

	@Override
    public boolean hasIgnoreMarker(AnnotatedMember annotatedMember) {
		return annotatedMember.hasAnnotation(IgnoreRequestDetail.class) 
				|| super.hasIgnoreMarker(annotatedMember);
    }
}
