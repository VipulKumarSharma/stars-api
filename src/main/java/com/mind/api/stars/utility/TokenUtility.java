package com.mind.api.stars.utility;

import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mind.api.stars.common.Constants;
import com.mind.api.stars.enums.APICallAttributes;
import com.mind.api.stars.model.TokenData;
import com.mind.api.stars.model.TokenResult;
import com.mind.api.stars.security.EncryptionUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class TokenUtility {
	
	private static final String GENERATE_TOKEN_URL = "https://appstore.mindeservices.com/stage/WebServices/Services2.svc/API/GenerateToken";
	private static final String GET_TOKENDETAILS_URL = "https://appstore.mindeservices.com/stage/WebServices/Services2.svc/API/TokenDetail";
	private static final String REFRESH_TOKEN_URL = "https://appstore.mindeservices.com/stage/WebServices/Services2.svc/API/RefreshToken";
	
	
	public static TokenResult generateToken(TokenData tokenData) throws Exception {

		TokenResult tokenResult = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set(APICallAttributes.AUTH_KEY.getKey(), tokenData.getAuthKey());

		JSONObject json = new JSONObject();
		json.put(APICallAttributes.DOMAIN_NAME.getKey(), tokenData.getDomainName());
		json.put(APICallAttributes.USER_ID.getKey(), tokenData.getUserId());
		json.put(APICallAttributes.SESSION_TIMEOUT.getKey(), tokenData.getSessionTimeOut());

		HttpEntity<String> req = new HttpEntity<>(json.toString(), headers);
		ResponseEntity<String> resp =  restTemplate.exchange(GENERATE_TOKEN_URL , HttpMethod.POST , req , String.class );
		tokenResult = mapJSONToTokenBean(resp.getBody());
		return validateToken(tokenResult);
	}
	
	
	public static TokenResult getTokenDetail(TokenData tokenData) throws Exception {
		TokenResult tokenResult = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		headers.set(Constants.RESPONSE_TOKEN_ID,tokenData.getTokenId());
		headers.set(Constants.REQUEST_FROM, tokenData.getRequestFrom());
		
		HttpEntity<String> req = new HttpEntity<>(null, headers);
		ResponseEntity<String> resp =  restTemplate.exchange(GET_TOKENDETAILS_URL , HttpMethod.GET , req , String.class );
		tokenResult = mapJSONToTokenBean(resp.getBody());
		return validateToken(tokenResult);
	}
	
	public static TokenResult refreshToken(TokenData tokenData) throws Exception {
		TokenResult tokenResult = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		JSONObject json = new JSONObject();
		try {
			json.put(Constants.RESPONSE_TOKEN_ID, tokenData.getTokenId());
		} catch (JSONException e) {
			ExceptionLogger.logExceptionToDB(e);
		}
		
		HttpEntity<String> req = new HttpEntity<>(json.toString(), headers);
		ResponseEntity<String> resp =  restTemplate.exchange(REFRESH_TOKEN_URL, HttpMethod.GET , req , String.class );
		tokenResult = mapJSONToTokenBean(resp.getBody());
		return validateToken(tokenResult);
	}
	
	private static TokenResult mapJSONToTokenBean(String responseData) throws Exception {
		TokenResult tokenResult = null;
		TokenData	tokenData 	= null;
		JSONObject result =  new JSONObject(responseData);
		ObjectMapper mapper = new ObjectMapper();
		
		JSONObject data   = null;
		if(!String.valueOf(result.getString(Constants.RESPONSE_DATA)).equals("null")) 
		{
			try {
				
					data= new JSONObject(result.getString(Constants.RESPONSE_DATA));
				
			}
			catch(Exception ex) {
				data = new JSONArray(result.getString(Constants.RESPONSE_DATA)).getJSONObject(0);
			}
			result.remove(Constants.RESPONSE_DATA);
			tokenData	= mapper.readValue(data.toString(), TokenData.class); 
		}
		tokenResult = mapper.readValue(result.toString(), TokenResult.class);
		tokenResult.setData(tokenData);
		return tokenResult;
	}
	
	private static TokenResult validateToken(TokenResult token) {
		if(token!=null && AppUtility.convertStringToInt(token.getStatus())==HttpStatus.OK.value()) {
			// log.info("token id is "+token.getData().getTokenId());
			return token;
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		try {
			TokenData data = new TokenData();
			data.setAuthKey(Constants.ENCRYPTION_PUBLIC_KEY);
			data.setDomainName(EncryptionUtility.encryptData("MIND"));
			data.setUserId(EncryptionUtility.encryptData("starsadmin"));
			data.setSessionTimeOut("60");
			TokenResult result=generateToken(data);
			log.info("token id is "+result.getData().getTokenId());
			getTokenDetail(result.getData());
		} catch (Exception e) {
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	public static String generateTokenId(String domainName,String userName) throws Exception {
		TokenData data = new TokenData();
		data.setAuthKey(Constants.ENCRYPTION_PUBLIC_KEY);
		data.setDomainName(EncryptionUtility.encryptData(AppUtility.toUpperCase(domainName)));
		data.setUserId(EncryptionUtility.encryptData(userName));
		data.setSessionTimeOut("60");
		return extractTokenId(generateToken(data));
	}
	public static String extractTokenId(TokenResult tokenResult) {
		
		
		return (tokenResult!=null && tokenResult.getData()!=null)?tokenResult.getData().getTokenId():null;
	}
}
