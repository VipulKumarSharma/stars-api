package com.mind.api.stars.utility;

import com.mind.api.stars.global.BaseDAO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionLogger extends BaseDAO {

	public static void logExceptionToDB(Exception e) {
		log.error(e.getLocalizedMessage());
	}
}
