package com.mind.api.stars.utility;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider.Impl;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestConvertor {

	private RequestConvertor() {}
	
	public static String convertResponseObjectToJsonString(Object dataObject) {
		
		JSONObject jsonObj 	= new JSONObject();
		String jsonStr 		= jsonObj.toString();
		
		if(dataObject != null) {
			
			try {
				Impl serializerProviderImpl = new Impl();
				serializerProviderImpl.setNullValueSerializer(new NullSerializer());
				   
				ObjectMapper mapperObj = new ObjectMapper();
				mapperObj.setSerializerProvider(serializerProviderImpl);
				mapperObj.enable(SerializationFeature.INDENT_OUTPUT);
				
				jsonStr = mapperObj.writeValueAsString(dataObject);
				
			} catch (Exception e) {
				jsonStr	= jsonObj.toString();
				
				log.error("Error occurred in RequestConvertor : convertResponseObjectToJsonString() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return jsonStr;
	}

	public static String convertRequestObjectToJsonString(Object dataObject) {
		
		JSONObject jsonObj 	= new JSONObject();
		String jsonStr 		= jsonObj.toString();
		
		if(dataObject != null) {
			
			try {
				ObjectMapper mapperObj = new ObjectMapper();
				mapperObj.enable(SerializationFeature.INDENT_OUTPUT);
				
				jsonStr = mapperObj.writeValueAsString(dataObject);
				
			} catch (Exception e) {
				jsonStr	= jsonObj.toString();
				
				log.error("Error occurred in RequestConvertor : convertRequestObjectToJsonString() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return jsonStr;
	}
	
	public static Object convertObjectWithState(Object sourceObject, Object destinationObject) {
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			String json 		= RequestConvertor.convertRequestObjectToJsonString(sourceObject);
			destinationObject 	= objectMapper.readValue(json, destinationObject.getClass());
			
		} catch (Exception e) {
			destinationObject 	= null;
			
			log.error("Error occurred in RequestConvertor : convertObjectWithState() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return destinationObject;
	}
	
	public static String convertObjectToXmlString(Object dataObject) {
		
		String xmlString = "";
		
		if(dataObject != null) {
			
			try {
				JacksonXmlModule xmlModule = new JacksonXmlModule();
				xmlModule.setDefaultUseWrapper(false);
				
				XmlMapper xmlMapper = new XmlMapper(xmlModule);
				xmlMapper.setAnnotationIntrospector(new XmlAnnotationIntrospector());
				xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
				
				xmlString = xmlMapper.writeValueAsString(dataObject);
				
			} catch (Exception e) {
				xmlString = "";
				
				log.error("Error occurred in RequestConvertor : convertObjectToXmlString() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return xmlString;
	}
	
}
