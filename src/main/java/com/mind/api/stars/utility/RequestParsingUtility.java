package com.mind.api.stars.utility;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.mind.api.stars.common.Constants;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.AttachmentRequestDTO;
import com.mind.api.stars.dto.DeleteJourneyDetailRequestDTO;
import com.mind.api.stars.dto.ExchangeRateRequestDTO;
import com.mind.api.stars.dto.SeatRequestDTO;
import com.mind.api.stars.dto.SiteBasedUserRequestDTO;
import com.mind.api.stars.dto.TravelClassRequestDTO;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.enums.APICallAttributes;
import com.mind.api.stars.model.TokenData;
import com.mind.api.stars.model.TokenResult;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class RequestParsingUtility {
	
	private RequestParsingUtility() {}

	
	public static AppRequestDTO fillDTOForGetRequests(HttpServletRequest request) throws Exception{
		
		AppRequestDTO appRequestDTO = null;
		
		try {	
		if(!AppUtility.checkNull(request)) {
				appRequestDTO = new AppRequestDTO();
				
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					appRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					appRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}				
				appRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				appRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				appRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				appRequestDTO.setTravellerId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVELLER_ID.getKey())));
				appRequestDTO.setSiteId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.SITE_ID.getKey())));
				appRequestDTO.setBillingSiteId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.BILLING_SITE_ID.getKey())));
				appRequestDTO.setTravelType(request.getHeader(APICallAttributes.TRAVEL_TYPE.getKey()));
				appRequestDTO.setSyncDate(request.getHeader(APICallAttributes.SYNC_DATE.getKey()));
				appRequestDTO.setTravelAgencyId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVEL_AGENCY_ID.getKey())));
				appRequestDTO.setCarClass(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.CAR_CLASS.getKey())));
				appRequestDTO.setApproverId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.APPROVER_ID.getKey())));
				appRequestDTO.setIdentityId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.IDENTITY_ID.getKey())));
				
				appRequestDTO.setTravelId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVEL_ID.getKey())));
				appRequestDTO.setPageType(request.getHeader(APICallAttributes.PAGE_TYPE.getKey()));
				appRequestDTO.setCommentId(request.getHeader(APICallAttributes.COMMENT_ID.getKey()));
				appRequestDTO.setTravelReqId(request.getHeader(APICallAttributes.TRAVEL_REQ_ID.getKey()));
				appRequestDTO.setRequestCount(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.REQUEST_COUNT.getKey())));
				appRequestDTO.setRequestNo(request.getHeader(APICallAttributes.REQUEST_NO.getKey()));
				appRequestDTO.setMailId(request.getHeader(APICallAttributes.MAIL_ID.getKey()));
				appRequestDTO.setAttachmentId(request.getHeader(APICallAttributes.ATTACHMENT_ID.getKey()));
				
				appRequestDTO.setTravellerType(request.getHeader(APICallAttributes.TRAVELLER_TYPE.getKey()));
				appRequestDTO.setFirstName(request.getHeader(APICallAttributes.FIRST_NAME.getKey()));
				appRequestDTO.setGUserId(request.getHeader(APICallAttributes.G_USER_ID.getKey()));
				appRequestDTO.setRequestType(request.getHeader(APICallAttributes.REQUEST_TYPE.getKey()));
				appRequestDTO.setCityAirportName(request.getHeader(APICallAttributes.CITY_AIRPORT_NAME.getKey()));
				appRequestDTO.setApproverLevel1(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.APPROVER_LEVEL1.getKey())));
				appRequestDTO.setApproverLevel2(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.APPROVER_LEVEL2.getKey())));
				appRequestDTO.setApproverLevel3(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.APPROVER_LEVEL3.getKey())));
			}
		}
		catch(Exception ex) {
			ExceptionLogger.logExceptionToDB(ex);
		}
			
		
		
		return appRequestDTO;
	}
	
	
	public static ExchangeRateRequestDTO fillDTOForExchangeRate(HttpServletRequest request) {
		ExchangeRateRequestDTO exchangeRateDTO = null;
		try {
			if(!AppUtility.checkNull(request)) {
				exchangeRateDTO = new ExchangeRateRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					exchangeRateDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					exchangeRateDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//exchangeRateDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//exchangeRateDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				exchangeRateDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				exchangeRateDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				exchangeRateDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				exchangeRateDTO.setSelectedCurrency(request.getHeader(APICallAttributes.SELECTED_CURRENCY.getKey()));
				exchangeRateDTO.setCurrentDate(request.getHeader(APICallAttributes.CURRENT_DATE.getKey()));
				exchangeRateDTO.setBaseCurrency(request.getHeader(APICallAttributes.BASE_CURRENCY.getKey()));
				exchangeRateDTO.setDays(request.getHeader(APICallAttributes.DAYS.getKey()));
				exchangeRateDTO.setPreviousTotal(request.getHeader(APICallAttributes.PREVIOUS_TOTAL.getKey()));
				exchangeRateDTO.setSelectedTotal(request.getHeader(APICallAttributes.SELECTED_TOTAL.getKey()));
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForExchangeRate() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return exchangeRateDTO;
	}
	
	
	public static SiteBasedUserRequestDTO fillDTOForSiteBasedUser(HttpServletRequest request) {
		
		SiteBasedUserRequestDTO siteBasedUserRequestDTO = null;
		
		try {
			if(request != null) {
				siteBasedUserRequestDTO = new SiteBasedUserRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					siteBasedUserRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					siteBasedUserRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//siteBasedUserRequestDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//siteBasedUserRequestDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				siteBasedUserRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				siteBasedUserRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				siteBasedUserRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				siteBasedUserRequestDTO.setSiteId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.SITE_ID.getKey())));
				siteBasedUserRequestDTO.setSyncDate(request.getHeader(APICallAttributes.SYNC_DATE.getKey()));
				
				if(!AppUtility.checkNull(siteBasedUserRequestDTO.getSyncDate()) && !siteBasedUserRequestDTO.getSyncDate().isEmpty()) {
					siteBasedUserRequestDTO.setSyncDate(AppUtility.convertMillisToDate(siteBasedUserRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
				} else {
					siteBasedUserRequestDTO.setSyncDate(Constants.NULL);
				}
				
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForSiteBasedUser() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return siteBasedUserRequestDTO;
		
	}
	
	public static SeatRequestDTO fillDTOForSeat(HttpServletRequest request) {
		
		SeatRequestDTO seatRequestDTO = null;
		
		try {
			if(request != null) {
				seatRequestDTO = new SeatRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					seatRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					seatRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//seatRequestDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//seatRequestDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				seatRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				seatRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				seatRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				seatRequestDTO.setSiteId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.SITE_ID.getKey())));
				seatRequestDTO.setSyncDate(request.getHeader(APICallAttributes.SYNC_DATE.getKey()));
				
				if(!AppUtility.checkNull(seatRequestDTO.getSyncDate()) && !seatRequestDTO.getSyncDate().isEmpty()) {
					seatRequestDTO.setSyncDate(AppUtility.convertMillisToDate(seatRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
				} else {
					seatRequestDTO.setSyncDate(Constants.NULL);
				}
				
				seatRequestDTO.setModeId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.MODE_ID.getKey())));
				seatRequestDTO.setTravelAgencyId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVEL_AGENCY_ID.getKey())));
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForSeat() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return seatRequestDTO;
	}
	
	
	public static TravelClassRequestDTO fillDTOForTravelClass(HttpServletRequest request) {
		
		TravelClassRequestDTO travelClassRequestDTO = null;
		
		try {
			if(request != null) {
				travelClassRequestDTO = new TravelClassRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					travelClassRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					travelClassRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//travelClassRequestDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//travelClassRequestDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				travelClassRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				travelClassRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				travelClassRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				travelClassRequestDTO.setSiteId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.SITE_ID.getKey())));
				travelClassRequestDTO.setSyncDate(request.getHeader(APICallAttributes.SYNC_DATE.getKey()));
				
				if(!AppUtility.checkNull(travelClassRequestDTO.getSyncDate()) && !travelClassRequestDTO.getSyncDate().isEmpty()) {
					travelClassRequestDTO.setSyncDate(AppUtility.convertMillisToDate(travelClassRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
				} else {
					travelClassRequestDTO.setSyncDate(Constants.NULL);
				}
				
				travelClassRequestDTO.setModeId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.MODE_ID.getKey())));
				travelClassRequestDTO.setTravellerId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVELLER_ID.getKey())));
				travelClassRequestDTO.setTravelType(request.getHeader(APICallAttributes.TRAVEL_TYPE.getKey()));
				travelClassRequestDTO.setTravelAgencyId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVEL_AGENCY_ID.getKey())));
				travelClassRequestDTO.setGroupTravelFlag(request.getHeader(APICallAttributes.GROUP_TRAVEL_FLAG.getKey()));
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForTravelClass() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return travelClassRequestDTO;
	}
	
	
	public static DeleteJourneyDetailRequestDTO fillDTOForDeleteJourneyDetail(HttpServletRequest request) {
		
		DeleteJourneyDetailRequestDTO deleteJourneyDetailRequestDTO = null;
		
		try {
			if(request != null) {
				deleteJourneyDetailRequestDTO = new DeleteJourneyDetailRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					deleteJourneyDetailRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					deleteJourneyDetailRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//deleteJourneyDetailRequestDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//deleteJourneyDetailRequestDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				deleteJourneyDetailRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				deleteJourneyDetailRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				deleteJourneyDetailRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				
				deleteJourneyDetailRequestDTO.setTravelId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.TRAVEL_ID.getKey())));
				deleteJourneyDetailRequestDTO.setModeId(AppUtility.convertStringToInt(request.getHeader(APICallAttributes.MODE_ID.getKey())));
				deleteJourneyDetailRequestDTO.setTravelType(request.getHeader(APICallAttributes.TRAVEL_TYPE.getKey()));
				deleteJourneyDetailRequestDTO.setTravelCarIds(request.getHeader(APICallAttributes.TRAVEL_CAR_IDS.getKey()));
				deleteJourneyDetailRequestDTO.setTravelAccommIds(request.getHeader(APICallAttributes.TRAVEL_ACCOMM_IDS.getKey()));
				
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForDeleteJourneyDetail() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return deleteJourneyDetailRequestDTO;
		
	}
	
	public static AttachmentRequestDTO fillDTOForAttachments(HttpServletRequest request, MultipartFile file) {
		AttachmentRequestDTO attachmentRequestDTO = null;
		try {
			if(request!=null) {
				attachmentRequestDTO = new AttachmentRequestDTO();
				Map<String, String> dataMap = getUserDetailsFromToken( getTokenIdFromRequest(request),request);
				if(!dataMap.isEmpty()) {
					attachmentRequestDTO.setDomainName(dataMap.get(APICallAttributes.DOMAIN_NAME.getKey()));
					attachmentRequestDTO.setWinUserId(dataMap.get(APICallAttributes.WIN_USER_ID.getKey()));
				}
				//attachmentRequestDTO.setDomainName(request.getHeader(APICallAttributes.DOMAIN_NAME.getKey()));
				//attachmentRequestDTO.setWinUserId(request.getHeader(APICallAttributes.WIN_USER_ID.getKey()));
				attachmentRequestDTO.setRequestSource(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
				attachmentRequestDTO.setDeviceId(request.getHeader(APICallAttributes.DEVICE_ID.getKey()));
				attachmentRequestDTO.setSystemName(request.getHeader(APICallAttributes.SYSTEM_NAME.getKey()));
				attachmentRequestDTO.setAttachmentId(request.getHeader(APICallAttributes.ATTACHMENT_ID.getKey()));
				
				if(!AppUtility.checkNull(file)) {
					attachmentRequestDTO.setFile(file);
					attachmentRequestDTO.setFileExtension("");
					attachmentRequestDTO.setFileSize("");
					attachmentRequestDTO.setFileName("");					
				}
			}
		} catch(Exception ex) {
			log.error("Error occurred in RequestParsingUtility : fillDTOForAttachments() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return attachmentRequestDTO;
	}
	
	public static boolean validateRequest(HttpServletRequest request) {
		TokenResult result = null;
		HttpSession httpSession = request.getSession(false);
		String tokenId=request.getHeader(APICallAttributes.TOKEN_ID.getKey());
		String requestFrom = request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey())==null ? "W" : ("M".equalsIgnoreCase(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey())) ? "smgconnect" : request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
		TokenData tokenData = new TokenData();
		UserInfoDTO userInfoDTO = null; 
		userInfoDTO=httpSession==null?null:(UserInfoDTO)httpSession.getAttribute(Constants.USER);
		
		if(AppUtility.checkNull(tokenId) && !AppUtility.checkNull(userInfoDTO)) {
			tokenId = userInfoDTO.getTokenId()==null?tokenId:userInfoDTO.getTokenId();
		}
		tokenData.setTokenId(tokenId);
		tokenData.setRequestFrom(requestFrom);
		try {
			
			result=TokenUtility.getTokenDetail(tokenData);
			tokenData=result==null?null:result.getData();
			if((AppUtility.checkNull(result))||(!AppUtility.checkNull(result) && AppUtility.checkNull(tokenData)||(!AppUtility.checkNull(tokenData) && (tokenData.getDomainName()==null || tokenData.getUserId()==null)))) {
				throw new Exception("Session expired due to invalid token");
			}
			else {
				return true;
			}
		}
		catch(Exception ex) {
			if(!AppUtility.checkNull(httpSession)) 
			{
				httpSession.invalidate();
			}
			ExceptionLogger.logExceptionToDB(ex);
			log.error("exception at RequestParsingUtility -> validateRequest() -> Session expired due to invalid token");
			return false;
		}
		
		
	}
	private static Map<String, String> getUserDetailsFromToken(String tokenId, HttpServletRequest request) {
		Map<String, String> dataMap = new HashMap<String, String>();
		TokenData tokenData = new TokenData();
		TokenResult result = null;
		String requestFrom = request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey())==null ? "W" : ("M".equalsIgnoreCase(request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey())) ? "smgconnect" : request.getHeader(APICallAttributes.REQUEST_SOURCE.getKey()));
		tokenData.setTokenId(tokenId);
		tokenData.setRequestFrom(requestFrom);
		try {
			result=TokenUtility.getTokenDetail(tokenData);
			if(result!=null && result.getData()!=null) 
			{
				tokenData=result.getData();
				dataMap.put(APICallAttributes.DOMAIN_NAME.getKey(), tokenData.getDomainName());
				dataMap.put(APICallAttributes.WIN_USER_ID.getKey(), tokenData.getUserId());
			}
		} catch (Exception e) {
			ExceptionLogger.logExceptionToDB(e);
			log.error("exception at RequestParsingUtility -> getUserDetailsFromToken() -> "+e);
		}
		
		return dataMap;
	}
	private static String getTokenIdFromRequest(HttpServletRequest request) {
		
		HttpSession httpSession = request.getSession(false);
		String tokenId=request.getHeader(APICallAttributes.TOKEN_ID.getKey());
		UserInfoDTO userInfoDTO = null;
		if(httpSession!=null) {
			userInfoDTO = (UserInfoDTO)httpSession.getAttribute(Constants.USER);
			if(userInfoDTO != null) {
				tokenId = userInfoDTO.getTokenId()==null?tokenId:userInfoDTO.getTokenId();
			}
		}
		
		return tokenId;
	}
	
}
