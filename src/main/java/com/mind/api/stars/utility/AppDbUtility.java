package com.mind.api.stars.utility;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.enums.ApproverCommentType;
import com.mind.api.stars.enums.Function;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.model.Comment;
import com.mind.api.stars.model.DefaultApprover;
import com.mind.api.stars.model.GroupTravelRequest;
import com.mind.api.stars.model.TravelRequest;
import com.mind.api.stars.model.TravellerRole;
import com.mind.api.stars.model.UserRoleAndUnitHead;
import com.mind.api.stars.model.WorkflowApprover;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class AppDbUtility extends BaseDAO {
	
	
	public int getUserId(String winUserId, String domainName) {
		int userId = -1;
		
		winUserId = AppUtility.checkNullString(winUserId);
		domainName = AppUtility.checkNullString(domainName);
		
		if(winUserId != null && !winUserId.isEmpty() && domainName != null && !domainName.isEmpty()) {
			String sql = "SELECT USERID FROM M_USERINFO WHERE STATUS_ID=10 AND WIN_USER_ID=? AND DOMAIN_NAME=?";
			
			try {
				String fetchedUserId = null;
				List<String> userIds = jdbcTemplate.queryForList(sql, new Object[] {winUserId, domainName }, String.class); 
			    if (AppUtility.containsData(userIds)) {
			    	fetchedUserId = userIds.get(0);
			    } else {
			    	fetchedUserId = null;
			    }
			    userId = AppUtility.isNull(fetchedUserId) ? -1 : Integer.parseInt(fetchedUserId);
				
			} catch(EmptyResultDataAccessException ex) {
				log.error("No result set found in AppDbUtility : getUserId() : ", ex);
				userId = -1;
				
			} catch (Exception e) {
				userId = -1;
				log.error("Error occurred in AppDbUtility : getUserId() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return userId;
	}
	
	
	
	
	public String getErrorMessage(String errorCode) {
		String errorMessage = Constants.ERROR_OCCURRED;
		String sql = "SELECT LTRIM(RTRIM(DESCRIPTION)) AS ERROR_MESSAGE FROM ERRORMESSAGE_MST WHERE CODE=?";
		
		try {
			errorMessage = jdbcTemplate.queryForObject(sql, new Object[] { errorCode }, String.class);
		
		} catch(EmptyResultDataAccessException ex) {
			errorMessage = Constants.ERROR_OCCURRED;
			log.error("No result set found in AppUtility : getErrorMessage() : ", ex);
			
		} catch (Exception e) {
			errorMessage = Constants.ERROR_OCCURRED;
			
			log.error("Error occurred in AppDbUtility : getUserId() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return errorMessage;
	}

	public List<Comment> getApproversComment(long travelId, String traveltype, String commentType){
		
		List<Comment> commentList = null;

		try {
			StringBuilder sql = new StringBuilder("SELECT TRAVEL_ID, COMMENTS, dbo.USER_NAME_MATA(POSTED_BY) AS POSTED_BY, DBO.CONVERTDATEDMY1(POSTED_ON) AS POSTED_ON_DATE, POSTED_BY");
			
			if(!AppUtility.checkNull(commentType) && commentType.equals(ApproverCommentType.APPROVE.getName())){
				sql.append(" ,COMMENTS_ID FROM TRAVEL_REQ_COMMENTS");
			} else {
				sql.append(" ,CANCEL_ID AS COMMENTS_ID FROM TRAVEL_REQ_CANCEL");
			}
			
			sql.append(" WHERE TRAVEL_ID=? AND TRAVEL_TYPE=? ORDER BY POSTED_ON DESC");
			
			commentList = jdbcTemplate.query(sql.toString(), new Object[]{travelId, traveltype}, new CommentsMapper());
			
			if(commentList == null) {
				commentList = new ArrayList<>();
			}
		
		} catch(Exception e) {
			commentList = new ArrayList<>();
			
			log.error("Error occurred in AppDbUtility : getApproversComment() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return commentList;
	}
	
	private static final class CommentsMapper implements RowMapper<Comment> {

		@Override
		public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Comment comment = new Comment();
			
			comment.setCommentId(AppUtility.checkNullString(rs.getString("COMMENTS_ID")));
			comment.setCommentDesc(AppUtility.checkNullString(rs.getString("COMMENTS")));
			comment.setPostedByName(AppUtility.checkNullString(rs.getString("POSTED_BY")));
			comment.setPostedOnDate(AppUtility.checkNullString(rs.getString("POSTED_ON_DATE")));
		    
			return comment;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<WorkflowApprover> getRequestWorkflow(String traveltype, long travelId) {
		
		List<WorkflowApprover> requestWorkflow = null;

		try {
			if(travelId > 0 && !AppUtility.isNull(traveltype)) {
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.REQUEST_WORKFLOW.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.TRAVEL_ID, travelId)
				.addValue(ParamConstant.TRAVEL_TYPE, traveltype.toUpperCase())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
				
				if(returnedCode != null && "0".equals(returnedCode)) {
					requestWorkflow	= (List<WorkflowApprover>) result.get(ParamConstant.RESULT_LIST);
				}
			}
		
		} catch(Exception e) {
			log.error("Error occurred in AppDbUtility : getRequestWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestWorkflow;
	}
	
	private static final class ApproversMapper implements RowMapper<WorkflowApprover> {

		@Override
		public WorkflowApprover mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			WorkflowApprover approver = new WorkflowApprover();
			
			String name = AppUtility.checkNullString(rs.getString("APPROVER_NAME"));
			String designationName = AppUtility.checkNullString(rs.getString("DESIGN_DESC"));
			String approveStatus = AppUtility.checkNullString(rs.getString("APPROVAL_STATUS"));
			String approveDate = AppUtility.checkNullString(rs.getString("APPROVE_DATE"));
			
			approver.setName(name);
			approver.setDesignationName(designationName);
			approver.setApproveStatus(approveStatus);
			approver.setApproveDate(approveDate);
		    
			return approver;
		}
	}
	
	public int getRequestTravellerId(String travelType, long travelId) {
		int travellerId = -1;
		
		travelType = AppUtility.checkNullString(travelType);
		
		if(travelType != null && !travelType.isEmpty() && travelId > 0) {
			String sql = "SELECT DBO.FN_GET_TRAVELLER_ID (?,?) AS TRAVELLER_ID";
			
			try {
				String fetchedTravellerId = jdbcTemplate.queryForObject(sql, new Object[] { travelId, travelType }, String.class);
				
				travellerId = AppUtility.isNull(fetchedTravellerId) ? -1 : Integer.parseInt(fetchedTravellerId);
				
			} catch(EmptyResultDataAccessException ex) {
				travellerId = -1;
				
			} catch (Exception e) {
				travellerId = -1;
				log.error("Error occurred in AppDbUtility : getRequestTravellerId() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return travellerId;
	}
	
	
	public int getRequestTravelAgencyId(String travelType, long travelId) {
		int travelAgencyId = 1;
		
		travelType = AppUtility.checkNullString(travelType);
		
		if(travelType != null && !travelType.isEmpty() && travelId > 0) {
			String sql = "SELECT DBO.FN_GET_TRAVEL_AGENCY_ID (?,?) AS TRAVEL_AGENCY_ID";
			
			try {
				String fetchedTravelAgencyId = jdbcTemplate.queryForObject(sql, new Object[] { travelId, travelType }, String.class);
				
				travelAgencyId = AppUtility.isNull(fetchedTravelAgencyId) ? 1 : Integer.parseInt(fetchedTravelAgencyId);
				
			} catch(EmptyResultDataAccessException ex) {
				travelAgencyId = 1;
				
			} catch (Exception e) {
				travelAgencyId = 1;
				log.error("Error occurred in AppDbUtility : getRequestTravelAgencyId() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return travelAgencyId;
	}

	
	public UserRoleAndUnitHead getUserRoleAndUnitHead(int travellerId, int siteId) {
		
		UserRoleAndUnitHead userRoleAndUnitHead = null;

		if(travellerId > 0 && siteId > 0) {
			try {
				String sql = Constants.SELECT_ALL_FROM + Constants.DBO + Function.GET_USERROLE_UNITHEAD.getName()+" (?,?)";
				
				userRoleAndUnitHead = jdbcTemplate.queryForObject(sql, new Object[] { travellerId, siteId }, new UserRoleAndUnitHeadMapper());
			
			} catch(EmptyResultDataAccessException ex) {
				log.error("No result set found in AppDbUtility : getUserRoleAndUnitHead() : ", ex);
				
			} catch(Exception e) {
				log.error("Error occurred in AppDbUtility : getUserRoleAndUnitHead() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return userRoleAndUnitHead;
	}
	
	private static final class UserRoleAndUnitHeadMapper implements RowMapper<UserRoleAndUnitHead> {

		@Override
		public UserRoleAndUnitHead mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			UserRoleAndUnitHead bean = new UserRoleAndUnitHead();
			
			bean.setRoleId(AppUtility.checkNullString(resultSet.getString(ParamConstant.ROLE_ID)));
			bean.setUnitHead(AppUtility.checkNullString(resultSet.getString(ParamConstant.UNIT_HEAD)));
			
			return bean;
		}
		
	}
	
	public List<DefaultApprover> getDefaultApprovers(int travellerId, int siteId, String travelType) {
		
		List<DefaultApprover> defaultApprovers = null;

		if(travellerId > 0 && siteId > 0 && !AppUtility.isNull(travelType)) {
			
			try {
				String sql = Constants.SELECT_ALL_FROM + Constants.DBO + Function.GET_DEFAULT_WORKFLOW_APPROVERS.getName()+" (?,?,?)";
				
				defaultApprovers = jdbcTemplate.query(sql, new Object[] { travellerId, siteId, travelType }, new DefaultApproversDataMapper());
			
			} catch(EmptyResultDataAccessException ex) {
				log.error("No result set found in AppDbUtility : getDefaultWorkflowApprovers() : ", ex);
				
			} catch(Exception e) {
				log.error("Error occurred in AppDbUtility : getDefaultWorkflowApprovers() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return defaultApprovers;
	}
	
	private static final class DefaultApproversDataMapper implements RowMapper<DefaultApprover> {

		@Override
		public DefaultApprover mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			DefaultApprover bean = new DefaultApprover();
			
			bean.setId(resultSet.getInt(ParamConstant.APPROVER_ID));
			bean.setSiteId(resultSet.getInt(ParamConstant.SITE_ID));
			bean.setOrderId(resultSet.getInt(ParamConstant.ORDER_ID));
			bean.setRole(AppUtility.checkNullString(resultSet.getString(ParamConstant.USERROLE)));
			
			return bean;
		}
		
	}
	
	public String getSiteMulitpleUnitHeadsFlag(int approverId, int siteId) {
		String flag = "N";
		
		if(approverId > 0 && siteId > 0) {
			try {
				String sql = Constants.SELECT_ALL_FROM + Constants.DBO + Function.CHECK_MULTIPLE_UNITHEADS_ON_SITE.getName()+" (?,?)";
				
				flag = jdbcTemplate.queryForObject(sql, new Object[] { approverId, siteId }, String.class);
				
			} catch(EmptyResultDataAccessException ex) {
				flag = "N";
				log.error("No result set found in AppDbUtility : getSiteMulitpleUnitHeadsFlag() : ", ex);
				
			} catch(Exception e) {
				flag = "N";
				log.error("Error occurred in AppDbUtility : getSiteMulitpleUnitHeadsFlag() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return flag;
	}
	
	public TravellerRole getTravellerRole(String travelType, long travelId) {
		TravellerRole travellerRole = null;
		
		try {
			String sql = Constants.SELECT_ALL_FROM + Constants.DBO + Function.GET_TRAVELLER_ROLE.getName()+" (?,?)";
			
			travellerRole = jdbcTemplate.queryForObject(sql, new Object[] { travelType, travelId }, new TravellerRoleDataMapper());
		
			
		} catch(EmptyResultDataAccessException ex) {
			log.error("No result set found in AppDbUtility : getTravellerRole() : ", ex);
			
		} catch(Exception e) {
			log.error("Error occurred in AppDbUtility : getTravellerRole() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return travellerRole;
	}
	
	private static final class TravellerRoleDataMapper implements RowMapper<TravellerRole> {

		@Override
		public TravellerRole mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			TravellerRole bean = new TravellerRole();
			
			bean.setTravellerId(resultSet.getInt(ParamConstant.TRAVELLER_ID));
			bean.setRole(AppUtility.checkNullString(resultSet.getString(ParamConstant.ROLE_ID)));
			
			return bean;
		}
		
	}
	
	public String generateRequestApproversXML(TravelRequest travelRequest) {
		StringBuilder requestApproversXML = new StringBuilder("<approvers>");
		
		try {
			List<String> approverList = new ArrayList<>();
			String strTravellerRole = "";
			String strUnitHead;
			
			int travellerId = travelRequest.getTravellerId();
			int strSiteId = travelRequest.getSiteId();
			String strTravelType = travelRequest.getTravelType();
			
			String strManager = Integer.toString(travelRequest.getApproverLevel1());
			String strHod = Integer.toString(travelRequest.getApproverLevel2());
			String strBoardMember = Integer.toString(travelRequest.getApproverLevel3());
			
			String strUserRole = "";
			String strRoleofTraveler = "";
			String strIDofTraveler = "";
			
			UserRoleAndUnitHead userRoleAndUnitHead = getUserRoleAndUnitHead(travellerId, strSiteId);
			
			if(!AppUtility.checkNull(userRoleAndUnitHead)) {
				strTravellerRole = userRoleAndUnitHead.getRoleId();
				strUnitHead = userRoleAndUnitHead.getUnitHead();
				
				if ("CHAIRMAN".equalsIgnoreCase(strTravellerRole)) {
					List<DefaultApprover> defaultApprovers = getDefaultApprovers(travellerId, strSiteId, strTravelType);

					for (DefaultApprover defaultApprover : defaultApprovers) {
						strUserRole = defaultApprover.getRole();

						if (strUserRole != null && "MATA".equalsIgnoreCase(strUserRole)) {
							approverList.add(Integer.toString(defaultApprover.getId()));
							approverList.add(strUserRole);
						}
					}

				} else {
					if (strManager != null) {
						approverList.add(strManager);
						approverList.add(Constants.WORKFLOW);
					}
					if (strHod != null && !approverList.contains(strHod)) {
						approverList.add(strHod);
						approverList.add(Constants.WORKFLOW);
					}
					
					if (("D".equalsIgnoreCase(strTravelType) && !AppUtility.isNull(strBoardMember) && !"-1".equals(strBoardMember) && !"0".equals(strBoardMember))
						|| ("I".equalsIgnoreCase(strTravelType) && !AppUtility.isNull(strBoardMember) && !"-1".equals(strBoardMember)) ) {
						
						approverList.add(strBoardMember);
						approverList.add(Constants.WORKFLOW);
					}
					
					List<DefaultApprover> defaultApprovers = getDefaultApprovers(travellerId, strSiteId, strTravelType);

					for (DefaultApprover defaultApprover : defaultApprovers) {
						String strTempApproverId = Integer.toString(defaultApprover.getId());
						strUserRole = defaultApprover.getRole();

						String flag = getSiteMulitpleUnitHeadsFlag(defaultApprover.getId(), strSiteId);
						
						if(!("1".equals(strUnitHead) && "Y".equalsIgnoreCase(flag))) {

							if ("D".equalsIgnoreCase(strTravelType)) {
								if (approverList.contains(strTempApproverId)) {
									int j = approverList.indexOf(strTempApproverId);
									int k = j + 1;

									if (j == k - 1) {
										approverList.remove(j);
										approverList.remove(j);
									}
								}
								approverList.add(strTempApproverId);
								
								if (!AppUtility.isNull(strUserRole) && "OR".equalsIgnoreCase(strUserRole)) {
									strUserRole = Constants.DEFAULT;
								}
								approverList.add(strUserRole);

							} else if ("I".equalsIgnoreCase(strTravelType)) {
								if (approverList.contains(strTempApproverId) && approverList.contains(Constants.WORKFLOW)) {
									approverList.remove(strTempApproverId);
									approverList.remove(Constants.WORKFLOW);
								}
								approverList.add(strTempApproverId);
								
								if (!AppUtility.isNull(strUserRole) && "OR".equalsIgnoreCase(strUserRole)) {
									strUserRole = Constants.DEFAULT;
								}
								approverList.add(strUserRole);
							}
						
						}
					}
				}
				
				TravellerRole travellerRole = getTravellerRole(strTravelType, 0);
				
				if(!AppUtility.checkNull(travellerRole)) {
					strRoleofTraveler = travellerRole.getRole();
					strIDofTraveler= Integer.toString(travellerRole.getTravellerId());
				}
				
				if ("OR".equalsIgnoreCase(strRoleofTraveler)) {
					strRoleofTraveler = Constants.DEFAULT;
				}

				if ((strManager == null || !strManager.equals(strIDofTraveler)) 
					&& (strHod == null || !strHod.equals(strIDofTraveler)) 
					&& (approverList.contains(strIDofTraveler) && !"MATA".equalsIgnoreCase(strRoleofTraveler))) {
					
					int i = approverList.indexOf(strIDofTraveler);
					approverList.remove(i);
					approverList.remove(i);
				}
			}
			
			int level = 0;
			ListIterator<String> itr =  approverList.listIterator();
			
			while(itr.hasNext()) {
			   ++level;
			   
			   String id = AppUtility.checkNullString(itr.next());
			   String role = AppUtility.checkNullString(itr.next());
			   
			   if(id != null && !id.isEmpty() && !"0".equals(id) && !"-1".equals(id)) {
				   requestApproversXML.append("<approver>");
				   requestApproversXML.append("<level>"+level+"</level>");
				   requestApproversXML.append("<id>"+id+"</id>");
				   requestApproversXML.append("<role>"+role+"</role>");
				   requestApproversXML.append("</approver>");
			   }
			}
			 
		} catch (Exception e) {
			log.error("Error occurred in AppDbUtility : generateRequestApproversXML() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			requestApproversXML.append("</approvers>");
		}
		
		return requestApproversXML.toString();
	}
	
	
	public int getPrimarySiteIdByUser(String winUserId, String domainName) {
		int siteId = -1;
		
		winUserId = AppUtility.checkNullString(winUserId);
		domainName = AppUtility.checkNullString(domainName);
		
		if(winUserId != null && !winUserId.isEmpty() && domainName != null && !domainName.isEmpty()) {
			String sql = "SELECT SITE_ID FROM M_USERINFO WHERE STATUS_ID=10 AND WIN_USER_ID=? AND DOMAIN_NAME=?";
			
			try {
				String fetchedUserId = jdbcTemplate.queryForObject(sql, new Object[] {winUserId, domainName }, String.class);
				
				siteId = AppUtility.isNull(fetchedUserId) ? -1 : Integer.parseInt(fetchedUserId);
				
			} catch(EmptyResultDataAccessException ex) {
				log.error("No result set found in AppDbUtility : getPrimarySiteIdByUser() : ", ex);
				siteId = -1;
				
			} catch (Exception e) {
				siteId = -1;
				log.error("Error occurred in AppDbUtility : getPrimarySiteIdByUser() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return siteId;
	}
	
	
	public String getUserRole(String userId) {
		String userRole = "";
		if(userId != null && !userId.isEmpty()) {
			String sql = "SELECT ROLE_ID FROM M_USERINFO WHERE STATUS_ID=10 AND USERID=? ";
			
			try {
				userRole = jdbcTemplate.queryForObject(sql, new Object[] {userId}, String.class);
				
			} catch(EmptyResultDataAccessException ex) {
				log.error("No result set found in AppDbUtility : getUserRole() : ", ex);
				
			} catch (Exception e) {
				log.error("Error occurred in AppDbUtility : getUserRole() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return userRole;
	}
	public boolean validateAuthCancelTravelReq(String travelId, String travelType, String userId){
		
		String strUserAccessCheckFlag = null;
		String sql = "SELECT DBO.FN_AUTH_CANCEL_TRAVEL_REQ(?,?,?)";
		
		try
		{
				
			
			strUserAccessCheckFlag=jdbcTemplate.queryForObject(sql, new Object[] { travelId, travelType,userId }, String.class);
				 		    
			if(!AppUtility.checkNull(strUserAccessCheckFlag) && !strUserAccessCheckFlag.equals("420")) {
				return true;
			}
			else {
				return false;
			}
		}
		catch (Exception e) 
		{
		  log.error("Error in validateAuthCancelTravelReq() of AppDbUtility.java=============== "+e);
		  ExceptionLogger.logExceptionToDB(e);
		  return false;
		}
		
	}
	
	public boolean validateAuthCommentsTravelReq(String travelId, String travelType, String userId){
		
		String strUserAccessCheckFlag = "";
		String strSql = "";
		
		try
		{
			strSql = "SELECT DBO.FN_AUTH_COMMENTS_TRAVEL_REQ(?,?,?)";
			strUserAccessCheckFlag=jdbcTemplate.queryForObject(strSql, new Object[] { travelId, travelType,userId }, String.class);		
			if(!AppUtility.checkNull(strUserAccessCheckFlag) && !strUserAccessCheckFlag.equals("420")) {
				return true;
			}
			else {
				return false;
			}
		   
		}
		catch (Exception e) 
		{
		  log.error("Error in validateAuthCommentsTravelReq() of AppDbUtility.java=============== "+e);
		  ExceptionLogger.logExceptionToDB(e);
		}
		return false;
	}
	/*****************************************************DB Functions****************************************************************/ 
	public void registerUnauthAccessLog(String strUserId, String ipAddress, String strPageName, String strActionDesc){
		try{
		SimpleJdbcCall jdbcCall	 	= new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.PROC_UNAUTH_ACCESS_LOG.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue("P_UNAUTH_USER_INFO_ID", strUserId)
				.addValue(ParamConstant.P_IP_ADDRESS, ipAddress)
				.addValue(ParamConstant.P_PAGE_NAME, strPageName)
				.addValue("P_ACTION_DESC", strActionDesc)
				.addValue(ParamConstant.OUTPUT_STATUS,"0");
			jdbcCall.execute(procParams);
		}catch(Exception e){
			log.error("Error in registerUnauthAccessLog() of AppDbUtility.java=============== "+e);
			 ExceptionLogger.logExceptionToDB(e);
		}		
	}
	
	
	public Map<String,Object> getLastResult(String sql){
		if(AppUtility.checkNull(sql) || AppUtility.isBlankString(sql)) {
			return null;
		}
		List<Map<String,Object>> resultList = null;
		try {
			resultList=jdbcTemplate.queryForList(sql);
		}
		catch(Exception ex) {
			log.error("error in AppDbUtility -->getLastResult() "+ex);
			 ExceptionLogger.logExceptionToDB(ex);
		}
		if(resultList!=null && !resultList.isEmpty()) {
			return resultList.get(resultList.size()-1);
		}
		
		
		return null;
	}
	public Map<String,Object> getFirstResult(String sql){
		if(AppUtility.checkNull(sql) || AppUtility.isBlankString(sql)) {
			return null;
		}
		List<Map<String,Object>> resultList = null;
		try {
			resultList=jdbcTemplate.queryForList(sql);
		}
		catch(Exception ex) {
			log.error("error in AppDbUtility -->getFirstResult() "+ex);
			 ExceptionLogger.logExceptionToDB(ex);
		}
		if(resultList!=null && ! resultList.isEmpty()) {
			return resultList.get(0);
		}
		
		
		return null;
	}
	public Map<String,Object> getFirstResult(String sql,Object[]  params){
		if(AppUtility.checkNull(sql) || AppUtility.isBlankString(sql)) {
			return null;
		}
		List<Map<String,Object>> resultList = null;
		try {
			resultList=jdbcTemplate.queryForList(sql,params);
		}
		catch(Exception ex) {
			log.error("error in AppDbUtility -->getFirstResult() "+ex);
			 ExceptionLogger.logExceptionToDB(ex);
		}
		if(resultList!=null && ! resultList.isEmpty()) {
			return resultList.get(0);
		}
		
		
		return null;
	}
	public String sSSOUrlByMailid(String strEmailId)
	{
		String strSSOUrl="";
		
		
		try{						
			
			String strSql		= "Select SSO_MAIL_URL from M_SITE s, M_USERINFO u  where ltrim(rtrim(email)) = '"+strEmailId.trim()+"' and loginstatus in ('active' , 'enable') and s.site_id = u.site_id";			
			Map<String,Object> ssoMail		= getFirstResult(strSql);
			if(ssoMail != null)
			{
				strSSOUrl		=	AppUtility.getResultFromMap(ssoMail, "SSO_MAIL_URL");
			}
					
		}catch(Exception e){
			log.error("Error in sSSOUrlByMailid() of DbUtilityMethods.java");
			 ExceptionLogger.logExceptionToDB(e);
		}
		if(strSSOUrl.equalsIgnoreCase(""))
		{
			strSSOUrl="https://stars.mindeservices.com";
			return strSSOUrl;
		}
		else
			return strSSOUrl;
			
	}
	public String getMaxMailRefNo() {
		String sSqlStr="SELECT ISNULL(MAX(MAIL_ID),'999')+1 FROM REQ_MAILBOX";
		String mailRefNo = null;
		try {
			
		mailRefNo=jdbcTemplate.queryForObject(sSqlStr, String.class);
		}
		catch(Exception ex){
			log.error("Error in getMaxMailRefNo()");
		}
		return mailRefNo;
		
	}

	public String getLangPrefByEmail(String email) {
		String strLanguage = null;
		String sSqlStr	=	"SELECT LANGUAGE_PREF FROM M_USERINFO WHERE EMAIL =N'"+email+"' AND STATUS_ID=10";
		Map<String,Object> langPrefMap=getFirstResult(sSqlStr);
		if(langPrefMap != null){
			strLanguage=AppUtility.getResultFromMap(langPrefMap, "LANGUAGE_PREF");
			if(strLanguage==null || strLanguage.equals("")){
				strLanguage="en_US";
			}
		}
		else {
			strLanguage="en_US";
		}
		return strLanguage;
		
	}
	public String getRoleIdByTravellerId(int travellerId) {
		String strSql="SELECT ISNULL(ROLE_ID,'OR') AS ROLE_ID FROM M_USERINFO WHERE USERID="+travellerId;
		String strRoleId = null;
		
		Map<String,Object> roledMap=getFirstResult(strSql);
		if(roledMap != null)
		{
			strRoleId = AppUtility.getResultFromMap(roledMap, "ROLE_ID");
		}
		return strRoleId;
	}
	
	public String getIsTravellerUnitHead(int travllerId,int siteId) {
		String sql = "SELECT ISNULL(UNIT_HEAD,'0') AS UNIT_HEAD   FROM USER_MULTIPLE_ACCESS where userid="+travllerId+" and site_id="+siteId+" and status_id=10 and unit_head=1";
	   	Map<String,Object> travelReqMap = getFirstResult(sql);
	   	String strIsTravellerUnitHead = null;
		if(travelReqMap != null)
		{
			strIsTravellerUnitHead			=	AppUtility.getResultFromMap(travelReqMap, "UNIT_HEAD");
		}
		return strIsTravellerUnitHead;
	}
	/*****************************************************DB Functions Ends Here****************************************************************/
	
	public boolean pushEmailDataToTable(String requisitionId, String requisitionNo, String emailTo, String emailFrom,
			String ccTo, String mailSubject, String mailMsg, String mailCreator, String sourcePage) {
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.PROC_REQUISITION_MAIL_ADD.getName())
					.withReturnValue();
			
			SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.REQUISITON_ID, requisitionId)
					.addValue(ParamConstant.REQUISITION_NUMBER, requisitionNo)
					.addValue(ParamConstant.RECEIPENT_TO, emailTo)
					.addValue(ParamConstant.RECEIPENT_FROM, emailFrom)
					.addValue(ParamConstant.RECEIPMENT_CC, ccTo)
					.addValue(ParamConstant.MAIL_SUBJECT, mailSubject)
					.addValue(ParamConstant.MAIL_MSG, mailMsg)
					.addValue(ParamConstant.TRIES, Constants.ZERO)
					.addValue(ParamConstant.ERROR_SUCCESS, Constants.NEW)
					.addValue(ParamConstant.MAIL_CREATOR, mailCreator)
					.addValue(ParamConstant.REQUISITION_STATUS, Constants.NEW)
					.addValue(ParamConstant.SOURCE_PAGE, sourcePage);
					
			simpleJdbcCall.execute(procParams);
			log.info("success at pushEmailDataToTable() of DbUtilityMethods.java"+ mailSubject );
			return true;
			
		}
		catch(Exception e) {
			log.error("Error in pushEmailDataToTable() of DbUtilityMethods.java"+e);
			 ExceptionLogger.logExceptionToDB(e);
			return false;
		}
		
	}
	public boolean pushEmailDataToTable(String requisitionId, String requisitionNo, String emailTo, String emailFrom,
			String ccTo, String mailSubject, String mailMsg, String mailCreator, String sourcePage, String requisitionStatus) {
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.PROC_REQUISITION_MAIL_ADD.getName())
					.withReturnValue();
			
			SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.REQUISITON_ID, requisitionId)
					.addValue(ParamConstant.REQUISITION_NUMBER, requisitionNo)
					.addValue(ParamConstant.RECEIPENT_TO, emailTo)
					.addValue(ParamConstant.RECEIPENT_FROM, emailFrom)
					.addValue(ParamConstant.RECEIPMENT_CC, ccTo)
					.addValue(ParamConstant.MAIL_SUBJECT, mailSubject)
					.addValue(ParamConstant.MAIL_MSG, mailMsg)
					.addValue(ParamConstant.TRIES, Constants.ZERO)
					.addValue(ParamConstant.ERROR_SUCCESS, Constants.NEW)
					.addValue(ParamConstant.MAIL_CREATOR, mailCreator)
					.addValue(ParamConstant.REQUISITION_STATUS, requisitionStatus)
					.addValue(ParamConstant.SOURCE_PAGE, sourcePage);
					
			simpleJdbcCall.execute(procParams);
			log.info("success at pushEmailDataToTable() of DbUtilityMethods.java"+ mailSubject );
			//log.info(mailMsg);
			return true;
			
		}
		catch(Exception e) {
			log.error("Error in pushEmailDataToTable() of DbUtilityMethods.java"+e);
			 ExceptionLogger.logExceptionToDB(e);
			return false;
		}
		
	}
	public String generateGroupTravelRequestApproversXML(GroupTravelRequest groupTravelRequest) {
		StringBuilder requestApproversXML = new StringBuilder("<approvers>");
		
		try {
			List<String> approverList = new ArrayList<>();
			String strTravellerRole = "";
			String strUnitHead;
			
			int travellerId = groupTravelRequest.getTravellerId();
			int strSiteId = groupTravelRequest.getSiteId();
			String strTravelType = groupTravelRequest.getTravelType();
			
			String strManager = Integer.toString(groupTravelRequest.getApproverLevel1());
			String strHod = Integer.toString(groupTravelRequest.getApproverLevel2());
			String strBoardMember = Integer.toString(groupTravelRequest.getApproverLevel3());
			
			String strUserRole = "";
			String strRoleofTraveler = "";
			String strIDofTraveler = "";
			
			UserRoleAndUnitHead userRoleAndUnitHead = getUserRoleAndUnitHead(travellerId, strSiteId);
			
			if(!AppUtility.checkNull(userRoleAndUnitHead)) {
				strTravellerRole = userRoleAndUnitHead.getRoleId();
				strUnitHead = userRoleAndUnitHead.getUnitHead();
				
				if ("CHAIRMAN".equalsIgnoreCase(strTravellerRole)) {
					List<DefaultApprover> defaultApprovers = getDefaultApprovers(travellerId, strSiteId, strTravelType);

					for (DefaultApprover defaultApprover : defaultApprovers) {
						strUserRole = defaultApprover.getRole();

						if (strUserRole != null && "MATA".equalsIgnoreCase(strUserRole)) {
							approverList.add(Integer.toString(defaultApprover.getId()));
							approverList.add(strUserRole);
						}
					}

				} else {
					if (strManager != null) {
						approverList.add(strManager);
						approverList.add(Constants.WORKFLOW);
					}
					if (strHod != null && !approverList.contains(strHod)) {
						approverList.add(strHod);
						approverList.add(Constants.WORKFLOW);
					}
					
					if (("D".equalsIgnoreCase(strTravelType) && !AppUtility.isNull(strBoardMember) && !"-1".equals(strBoardMember) && !"0".equals(strBoardMember))
						|| ("I".equalsIgnoreCase(strTravelType) && !AppUtility.isNull(strBoardMember) && !"-1".equals(strBoardMember)) ) {
						
						approverList.add(strBoardMember);
						approverList.add(Constants.WORKFLOW);
					}
					
					List<DefaultApprover> defaultApprovers = getDefaultApprovers(travellerId, strSiteId, strTravelType);

					for (DefaultApprover defaultApprover : defaultApprovers) {
						String strTempApproverId = Integer.toString(defaultApprover.getId());
						strUserRole = defaultApprover.getRole();

						String flag = getSiteMulitpleUnitHeadsFlag(defaultApprover.getId(), strSiteId);
						
						if(!("1".equals(strUnitHead) && "Y".equalsIgnoreCase(flag))) {

							if ("D".equalsIgnoreCase(strTravelType)) {
								if (approverList.contains(strTempApproverId)) {
									int j = approverList.indexOf(strTempApproverId);
									int k = j + 1;

									if (j == k - 1) {
										approverList.remove(j);
										approverList.remove(j);
									}
								}
								approverList.add(strTempApproverId);
								
								if (!AppUtility.isNull(strUserRole) && "OR".equalsIgnoreCase(strUserRole)) {
									strUserRole = Constants.DEFAULT;
								}
								approverList.add(strUserRole);

							} else if ("I".equalsIgnoreCase(strTravelType)) {
								if (approverList.contains(strTempApproverId) && approverList.contains(Constants.WORKFLOW)) {
									approverList.remove(strTempApproverId);
									approverList.remove(Constants.WORKFLOW);
								}
								approverList.add(strTempApproverId);
								
								if (!AppUtility.isNull(strUserRole) && "OR".equalsIgnoreCase(strUserRole)) {
									strUserRole = Constants.DEFAULT;
								}
								approverList.add(strUserRole);
							}
						
						}
					}
				}
				
				TravellerRole travellerRole = getTravellerRole(strTravelType, 0);
				
				if(!AppUtility.checkNull(travellerRole)) {
					strRoleofTraveler = travellerRole.getRole();
					strIDofTraveler= Integer.toString(travellerRole.getTravellerId());
				}
				
				if ("OR".equalsIgnoreCase(strRoleofTraveler)) {
					strRoleofTraveler = Constants.DEFAULT;
				}

				if ((strManager == null || !strManager.equals(strIDofTraveler)) 
					&& (strHod == null || !strHod.equals(strIDofTraveler)) 
					&& (approverList.contains(strIDofTraveler) && !"MATA".equalsIgnoreCase(strRoleofTraveler))) {
					
					int i = approverList.indexOf(strIDofTraveler);
					approverList.remove(i);
					approverList.remove(i);
				}
			}
			
			int level = 0;
			ListIterator<String> itr =  approverList.listIterator();
			
			while(itr.hasNext()) {
			   ++level;
			   
			   String id = AppUtility.checkNullString(itr.next());
			   String role = AppUtility.checkNullString(itr.next());
			   
			   if(id != null && !id.isEmpty() && !"0".equals(id) && !"-1".equals(id)) {
				   requestApproversXML.append("<approver>");
				   requestApproversXML.append("<level>"+level+"</level>");
				   requestApproversXML.append("<id>"+id+"</id>");
				   requestApproversXML.append("<role>"+role+"</role>");
				   requestApproversXML.append("</approver>");
			   }
			}
			 
		} catch (Exception e) {
			log.error("Error occurred in AppDbUtility : generateRequestApproversXML() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			requestApproversXML.append("</approvers>");
		}
		
		return requestApproversXML.toString();
	}
	
	
}
