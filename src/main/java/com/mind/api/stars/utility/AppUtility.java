package com.mind.api.stars.utility;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.global.BaseDAO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AppUtility extends BaseDAO {
	
	public static String checkNullString(Object obj) {
		return "null".equals(String.valueOf(obj)) ? null : String.valueOf(obj).trim();
	}
	
	public static boolean isBlankString(String string) {
		return !isNull(string) && "".trim().equals(string);
	}
	
	public static boolean isNull(Object obj) {
		return "null".equals(String.valueOf(obj));
	}
	public static String encrypt(String oldPassword) {
		int len = oldPassword.length();
		String newPassword = "";
		int temp[] = new int[len];
		int i = 0;
		int ran = (int) (Math.floor(Math.random() * 256) + 1);
		int ch;
		for (i = 0; i < len; i++) {
			ch = oldPassword.charAt(i);
			temp[i] = Integer.parseInt("" + ch);
			if ((temp[i] >= 0) && (temp[i] < 64))
				temp[i] += 128;
			else if ((temp[i] >= 64) && (temp[i] < 128))
				temp[i] += 128;
			else if ((temp[i] >= 128) && (temp[i] < 192))
				temp[i] -= 128;
			else if ((temp[i] >= 192) && (temp[i] < 256))
				temp[i] -= 128;
			else
				;
			temp[i] += ran;
			newPassword += convertToBoolean(temp[i]);
		}
		newPassword += convertToBoolean(ran);
		return newPassword;
	}

	public static String convertToBoolean(int intcode) {
		int num = intcode;
		int quot;
		int div = 2;
		int rem;
		int temp[] = new int[9];
		int temp2[] = new int[9];
		int len = temp.length;
		String str = "";
		int k = 0;
		for (k = 0; num != 0; k++) {
			quot = num / div;
			rem = num % div;
			temp[k] = rem;
			num = quot;
		}
		// for suffixing zeros to make it 9bit.
		for (int i = k; i < len; i++)
			temp[i] = 0;
		// For storing valuesreversing array
		for (int i = len - 1, j = 0; i >= 0; i--, j++) {
			temp2[j] = temp[i];
		}
		// for converting array to string value.
		for (int i = 0; i < len; i++)
			str += temp2[i];
		return str;
	}

	public static String decrypt(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = ((len / 9) - 1);
		int temp[] = new int[pwdLen];
		int k = 0;
		int i = 0;
		int ran;
		char p;
		String strbooleantemp = "";
		String strbooleanRan = password.substring(len - 9);
		ran = convertToDecimal(strbooleanRan);

		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((9 * k), 9 * (k + 1));
			temp[k] = (convertToDecimal(strbooleantemp) - ran);

			if ((temp[k] >= 0) && (temp[k] < 64)) {
				temp[k] += 128;
			} else if ((temp[k] >= 64) && (temp[k] < 128)) {
				temp[k] += 128;
			} else if ((temp[k] >= 128) && (temp[k] < 192)) {
				temp[k] -= 128;
			} else if ((temp[k] >= 192) && (temp[k] < 256)) {
				temp[k] -= 128;
			}

			else
				;

			p = (char) temp[k];
			oldPassword += p;
		}
		return oldPassword;
	}

	public static int convertToDecimal(String strbooleancode)
	{
	     int i      = 0;
		 int j      = 0; 
	     int result = 0;
	     int base   = 2;
	     char temp   = '-';
	     for(i=0,j=8; i<9; i++,j--)
	     {
	        temp=strbooleancode.charAt(i);
	        result += Math.pow(base,j)*Integer.parseInt(""+temp);
	     }     
	     return result;
	}

	public static String decryptInDecimal(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = ((len / 9) - 1);
		int temp[] = new int[pwdLen];
		int k = 0;
		int i = 0;
		int ran;

		String strbooleantemp = "";
		String strbooleanRan = password.substring(len - 9);
		ran = convertToDecimal(strbooleanRan);

		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((9 * k), 9 * (k + 1));
			temp[k] = (convertToDecimal(strbooleantemp) - ran);
			oldPassword += temp[k] + "";
		}
		return oldPassword;
	}

	public static String decryptFromDecimalToString(String password) {
		int len = password.length();
		String oldPassword = "";
		int pwdLen = (len / 3);
		int temp[] = new int[pwdLen];
		int k = 0;
		int i = 0;
		char p;
		String strbooleantemp = "";
		for (k = 0; k < (pwdLen); k++) {
			strbooleantemp = password.substring((3 * k), 3 * (k + 1));
			temp[k] = Integer.parseInt(strbooleantemp);

			if ((temp[k] >= 0) && (temp[k] < 64)) {
				temp[k] += 128;
			} else if ((temp[k] >= 64) && (temp[k] < 128)) {
				temp[k] += 128;
			} else if ((temp[k] >= 128) && (temp[k] < 192)) {
				temp[k] -= 128;
			} else if ((temp[k] >= 192) && (temp[k] < 256)) {
				temp[k] -= 128;
			}

			p = (char) temp[k];
			oldPassword += p;
		}
		return oldPassword;
	}
	
	
	public static String getCurrentTimeInMillis(String dateFormat, String dateToConvert) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date = sdf.parse(dateToConvert);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);		
        return String.valueOf(calendar.getTime());
    }
	
	public static String convertMillisToDate(String timeInMillis) {
		long time = Long.parseLong(timeInMillis);
		Date date = new Date(time);
		Format format = new SimpleDateFormat(Constants.DB_DATE_FORMAT);
		return format.format(date);
	}

	
	public static String syncDate() {
        Date date = new Date();
        return Long.toString(date.getTime());
    }
	
	public static String syncDate(String dateToConvert) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
		Date d = sdf.parse(dateToConvert); 
		long millis = d.getTime();
		return Long.toString(millis);
    }
	
	public static String getUTCDateTime(Date date, String format) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        return f.format(date);
    }
	
	public static boolean isSuccess(String returnedCode) {
		return (returnedCode != null && (Constants.PROC_EXEC_SUCCESS_CODE).equals(returnedCode));
	}
	
	public static <E> boolean containsData(Collection<E> collection) {
		return (collection != null && !collection.isEmpty());
	}
	
	public static int convertStringToInt(String string) {
		int number = 0;
		
		try {
			if(!checkNull(string) && !"".equals(string)) {
				number = Integer.parseInt(string.trim());
			}
			
		} catch (Exception ex) {
			number = 0;
		}
		return number;
	}
	
	public static String convertIntToString(Integer in) {
		String out = "";
		
		try {
			if(!checkNull(in)) {
				out = Integer.toString(in);
			}
			
		} catch (Exception ex) {
			out = "";
		}
		return out;
	}
	
	public static long convertStringToLong(String string) {
		long longNumber = 0L;
		
		try {
			if(!checkNull(string) && !"".equals(string)) {
				longNumber = Long.parseLong(string.trim());
			}
			
		} catch (Exception ex) {
			longNumber = 0L;
		}
		return longNumber;
	}
	public static String convertLongToString(Long in) {
		String out = "";
		
		try {
			if(!checkNull(in)) {
				out = Long.toString(in);
			}
			
		} catch (Exception ex) {
			out = "";
		}
		return out;
	}
	public static double convertStringToDouble(String string) {
		double doubleNumber = 0.0D;
		
		try {
			if(!checkNull(string) && !"".equals(string)) {
				doubleNumber = Double.parseDouble(string.trim());
			}
			
		} catch (Exception ex) {
			doubleNumber = 0D;
		}
		return doubleNumber;
	}
	
	public static Date convertSyncDateToDB(String timeInMillis) throws ParseException {
		long time = Long.parseLong(timeInMillis);
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		return sdf.parse(format.format(date)); 
    }
	
	public static boolean checkNull(Object obj) {
		return obj==null ? Boolean.TRUE : Boolean.FALSE;
	}
	
	public static String convertMillisToDate(String timeInMillis, String outputFormat) {
		long time = Long.parseLong(timeInMillis);
		Date date = new Date(time);
		Format format = new SimpleDateFormat(outputFormat);
		return format.format(date);
	}
	
	public static String getBrowserInfo(String browser) {
		String browserType = "";
		
		if (!AppUtility.isNull(browser)) {
			if (browser.contains("rv") && browser.contains(Constants.TRIDENT) && !browser.contains(Constants.CHROME) && !browser.contains(Constants.FIREFOX) && !browser.contains(Constants.SAFARI) && !browser.contains("AppleWebKit")) {
				
				browserType = "IE v" + browser.substring(browser.indexOf("rv") + 3, browser.indexOf(')'));
			}
			if (browser.contains("Edge") && !browser.contains(Constants.TRIDENT) && !browser.contains(Constants.FIREFOX)) {
				browserType = "EDGE v" + browser.substring(browser.indexOf("Edge") + 5);
			}
			if (browser.contains(Constants.CHROME) && !browser.contains(Constants.TRIDENT) && !browser.contains("Edge") && !browser.contains(Constants.FIREFOX)) {
				browserType = "CHROME v" + browser.substring(browser.indexOf(Constants.CHROME) + 7, browser.indexOf(" Safari"));
			}
			if (browser.contains(Constants.FIREFOX) && !browser.contains(Constants.TRIDENT) && !browser.contains(Constants.CHROME) && !browser.contains(Constants.SAFARI) && !browser.contains("AppleWebKit")) {
				browserType = "FIREFOX v" + browser.substring(browser.indexOf(Constants.FIREFOX) + 8);
			}
			
			if (browserType.equals("")) {
				browserType = getBrowserDetails(browser);
			}
		}

		return browserType;
	}
	
	private static String getBrowserDetails(String browser) {
		String browsername = "";
		String browserversion = "";
		String subsString = "";
		String[] browserSubString = { "MSIE", Constants.FIREFOX, Constants.CHROME, "Opera", "Netscape", Constants.SAFARI };

		if (browser.contains("MSIE")) {
			subsString = browser.substring(browser.indexOf("MSIE"));
			String[] info = (subsString.split(";")[0]).split(" ");
			browsername = info[0];
			browserversion = info[1];
		
		} else {
			for (int index = 1; index < browserSubString.length; index++) {
				if (browser.contains(browserSubString[index])) {
					subsString = browser.substring(browser.indexOf(browserSubString[index]));
					String[] info = (subsString.split(" ")[0]).split("/");
					browsername = info[0];

					if (browser.indexOf("Version") != -1) {
						browserversion = browser.substring(browser.indexOf("Version")).split(" ")[0]
								.split(";")[0].split("/")[1];
					} else {
						browserversion = info[1];
					}
					break;
				}
			}
			if (browsername.equals("")) {
				subsString = browser.substring(browser.indexOf("Windows NT"));
				browsername = subsString.split(";")[0];
				subsString = browser.substring(browser.indexOf(Constants.TRIDENT));
				browserversion = subsString.split(";")[0];
			}
		}
		
		return (browsername+" "+browserversion);
	}
	
	
	public static Double roundOffValues(String value) {
		if(value==null || value.isEmpty())
			return 0.00d;
		else 
			return Math.round((Double.parseDouble(value))*100)/100.00d;
	}
	
	 public static String getResultFromMap(Map<String,Object> rsMap,String key) {

	      if(rsMap!=null && rsMap.containsKey(key)) {
	    	  return checkNullString(rsMap.get(key));
	      }
	      else {
	    	  return null;
	      }
	   }
	 public static String ddmmyyyyStrToddmmmyyyy(String strDate) {
			String tmpDate = "";
			try {
				if((strDate != null && !"".equals(strDate))&& (strDate.indexOf('/') == 2 && strDate.lastIndexOf('/') == 5)) {
						strDate = strDate.trim().substring(0,10);
						LocalDate dateTime = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_IN, Locale.ENGLISH));
						tmpDate = dateTime.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_HIFEN, Locale.ENGLISH));
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: ddmmyyyyStrToddmmmyyyy() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String ddmmmyyyyStrToddmmyyyy(String strDate) {
			String tmpDate = "";
			try {
				if((strDate != null && !"".equals(strDate) ) && (strDate.indexOf('-') == 2 && strDate.lastIndexOf('-') == 6) ) {
						strDate = strDate.trim().substring(0,11);
						LocalDate localDate = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_HIFEN, Locale.ENGLISH));
						tmpDate = localDate.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_IN, Locale.ENGLISH));
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: ddmmmyyyy_StrTo_ddmmyyyy() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String[] ddmmmyyyyArrToddmmyyyy(String[] dateArr) {
			String[] tmpDate = null;
			try {
				if(dateArr != null && dateArr.length > 0) {
					tmpDate = new String[dateArr.length];
					
					for (int i=0;i<dateArr.length;i++) {
						tmpDate[i] = "";
						
						if((dateArr[i] != null && !"".equals(dateArr[i])) && (dateArr[i].indexOf('-') == 2 && dateArr[i].lastIndexOf('-') == 6)) {
								dateArr[i] = dateArr[i].trim().substring(0,11);
								LocalDate dateTime = LocalDate.parse(dateArr[i], DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_HIFEN, Locale.ENGLISH));
								tmpDate[i] = dateTime.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_IN, Locale.ENGLISH));
						}
					}
				} else if (dateArr != null && dateArr.length == 0) {
				    tmpDate = new String[0];
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: ddmmmyyyyArrToddmmyyyy() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String ddmmyyyyStrToDbDateFormat(String strDate) {	
			String tmpDate = "";
			try {
				if((strDate != null && !"".equals(strDate)) && (strDate.indexOf('/') == 2 && strDate.lastIndexOf('/') == 5)) {
						strDate = strDate.trim().substring(0,10);
						
						LocalDate dateTime = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_IN, Locale.ENGLISH));
						tmpDate = dateTime.format(DateTimeFormatter.ofPattern(Constants.DB_DATE_FORMAT_WITHOUT_TIME, Locale.ENGLISH));
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: ddmmyyyyStrToDbDateFormat() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String ddmmmyyyyStrToDbDateFormat(String strDate) {	
			String tmpDate = "";
			try {
				if((strDate != null && !"".equals(strDate)) && (strDate.indexOf('-') == 2 && strDate.lastIndexOf('-') == 6)) {
						strDate = strDate.trim().substring(0,11);
						
						LocalDate dateTime = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_HIFEN, Locale.ENGLISH));
						tmpDate = dateTime.format(DateTimeFormatter.ofPattern(Constants.DB_DATE_FORMAT_WITHOUT_TIME, Locale.ENGLISH));
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: ddmmmyyyyStrToDbDateFormat() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String dbDateFormatStrToddmmyyyy(String strDate) {	
			String tmpDate = "";
			try {
				if((strDate != null && !"".equals(strDate))&& (strDate.indexOf('-') == 4 && strDate.lastIndexOf('-') == 7)) {
						strDate = strDate.trim().substring(0,10);
						
						LocalDate dateTime = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(Constants.DB_DATE_FORMAT_WITHOUT_TIME, Locale.ENGLISH));
						tmpDate = dateTime.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_IN, Locale.ENGLISH));
				}
			} catch(Exception e) {
				log.error("Error in Date Conversion AppUtility: dbDateFormatStrToddmmyyyy() method");
				ExceptionLogger.logExceptionToDB(e);
			}
			return tmpDate;
		}
		
		public static String toUpperCase(String string) {
			return AppUtility.checkNull(string)?null:string.toUpperCase();
		}
		public static String blankToNull(String msg){
			if(msg==null ||( msg.trim().equals(""))){
				return null;
			}
			else{
				return msg;
			}
		}
		public static String blankToNA(String msg){
			if(msg==null ||( msg.trim().equals(""))){
				return "N.A.";
			}
			else{
				return msg;
			}
		}
		public static String blankToNA(Object msg){
		
			return blankToNA(checkNullString(msg));
				
		}
		public static String blankToMinus(String msg){
			if(msg==null ||( msg.trim().equals(""))){
				return "-";
			}
			else{
				return msg;
			}
		}
		public static int travelReqNoToTravelId(String travelReqNo) {
			if(travelReqNo==null || "".equals(travelReqNo)) {
				return 0;
			} else {
				 return	convertStringToInt(travelReqNo.substring(travelReqNo.lastIndexOf('/')+1));
			}
		}
		public static boolean isNullOrBlankString(String string) {
			return isNull(string) || "".trim().equals(string);
		}
		public static boolean isBlankOrHyphenString(String string) {
			return isNull(string) || "-".trim().equals(string) || "".trim().equals(string);
		}
		public static String boolToString(boolean data) {
			return data?"Yes":"No";
		}
		
}
