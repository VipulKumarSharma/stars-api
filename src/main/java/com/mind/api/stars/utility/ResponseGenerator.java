package com.mind.api.stars.utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mind.api.stars.common.Constants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResponseGenerator {
	
	private ResponseGenerator() {}
	
	public static String generateResponse(String responseStatus, String responseMessage, Object data, boolean isCollection) {
		
		JSONObject response = new JSONObject();
		
		try {
			response.put(Constants.RESPONSE_STATUS, 	responseStatus);
			response.put(Constants.RESPONSE_MESSAGE, 	responseMessage);
			response.put(Constants.LAST_SYNC_DATE, 	AppUtility.syncDate());
			
			if (data != null) {
				String jsonStr = RequestConvertor.convertResponseObjectToJsonString(data);
						
				if(isCollection) {
					JSONArray jsonArr = new JSONArray(jsonStr);
					response.put(Constants.RESPONSE_DATA, jsonArr);
				
				} else {
					JSONObject jsonObj = new JSONObject(jsonStr);
					response.put(Constants.RESPONSE_DATA, jsonObj);
				}
			
			} else {
				if(isCollection) {
					JSONArray jsonArr = new JSONArray();
					response.put(Constants.RESPONSE_DATA, jsonArr);
				
				} else {
					JSONObject jsonObj = new JSONObject();
					response.put(Constants.RESPONSE_DATA, jsonObj);
				}
				
			}
		
		} catch (JSONException e) {
			log.error("Error occurred in ResponseGenerator : generateResponse() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return response.toString();
	}
}
