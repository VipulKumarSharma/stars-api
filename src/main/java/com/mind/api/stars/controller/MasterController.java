package com.mind.api.stars.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.servlet.ModelAndView;

public interface MasterController extends ErrorController {

	@Override
	public String getErrorPath();
	
	public ModelAndView error();
}
