package com.mind.api.stars.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ControllerURL;
import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;

public interface AppController extends MasterController {
	
	@GetMapping(ControllerURL.ROOT)
	ModelAndView loginPage(
		HttpServletRequest request
	);
	
	@PostMapping(ControllerURL.LOGIN)
	ModelAndView login(
		@ModelAttribute LoginDTO loginDTO, 
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	);
	
	@GetMapping(ControllerURL.SSO_LOGIN)
	ModelAndView ssoLogin(
		HttpServletRequest request, 
		RedirectAttributes attributes
	);
	
	@PostMapping(ControllerURL.ACCEPT_POLICY)
	ModelAndView acceptPolicy(
		@ModelAttribute UserInfoDTO userInfoDTO, 
		HttpServletRequest request, 
		RedirectAttributes redirectAttributes
	);
	
	@GetMapping(ControllerURL.HOME)
	ModelAndView home(
		@ModelAttribute(Constants.USER) UserInfoDTO userInfoDTO, 
		@ModelAttribute(Constants.PAGE) String fromPage, 
		@ModelAttribute(Constants.ENC_PASSWORD) String encPassword,
		@ModelAttribute("requestedScreen") String requestedScreen,
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	);
	
	@GetMapping(ControllerURL.API_HOME)
	ModelAndView apiHome();
	
	@GetMapping(ControllerURL.ERROR)
	ModelAndView error();
	
	@GetMapping(ControllerURL.MAINTAINANCE)
	ModelAndView serverUnderMaintainance();
	
	@GetMapping(ControllerURL.LOGOUT)
	ModelAndView logout(
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	);
	
	@PostMapping(ControllerURL.CREATE_REQUEST)
	ModelAndView createRequest(
		HttpServletRequest request
	);
	
	@PostMapping(ControllerURL.EDIT_REQUEST)
	ModelAndView editRequest(
		HttpServletRequest request
	);
	
	@GetMapping(ControllerURL.APPROVE_REQUEST)
	ModelAndView approveRequest();
	
    @GetMapping(ControllerURL.REQUEST_LIST)
    ModelAndView requestList();
    
    @GetMapping(ControllerURL.REQUEST_DETAILS)
    ModelAndView requestDetails();
    
}
