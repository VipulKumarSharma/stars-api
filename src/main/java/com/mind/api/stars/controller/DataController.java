package com.mind.api.stars.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mind.api.stars.common.EndpointURL;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="DataController")
public interface DataController {
	
	/************************************* FETCH CITY DETAILS BY COUNTRY *********************************/
	
	/*@ApiOperation(value="Fetch city details by country")
	@GetMapping(
		value = EndpointURL.GET_CITY_BY_COUNTRY, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody*/
	String getCityByCountry(
		@PathVariable int countryId,
		HttpServletRequest request
	);
	
	
	/************************************* FETCH AIRPORTS INFORMATION ************************************/
	
	/*@ApiOperation(value="Fetch airports information")
	@GetMapping(
		value = EndpointURL.GET_AIRPORTS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody*/
	String getAirportsInfo(
		HttpServletRequest request
	);
	
	
	/************************************* FETCH HOTELS INFORMATION **************************************/
	
	/*@ApiOperation(value="Fetch hotels information")
	@GetMapping(
		value = EndpointURL.GET_HOTELS_INFO, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody*/
	String getHotelsInfo(
		HttpServletRequest request
	);
	
	
	/************************************* FETCH DAILY ALLOWANCE AMOUNT *********************************/
	
	@ApiOperation(value="Fetch daily allowance amount")
	@GetMapping(
		value = EndpointURL.GET_DAILY_ALLOWANCE_AMOUNT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getdailyAllowanceAmount(
		HttpServletRequest request
	);
	
	
	/************************************* FETCH PAST BOOKINGS ******************************************/
	
	/*@ApiOperation(value="Fetch past bookings")
	@GetMapping(
		value = EndpointURL.GET_PAST_BOOKINGS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody*/
	String getPastBookings(
		HttpServletRequest request
	);
	
	
	/************************************* FETCH FUTURE BOOKINGS ***************************************/
	
	/*@ApiOperation(value="Fetch future bookings")
	@GetMapping(
		value = EndpointURL.GET_FUTURE_BOOKINGS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody*/
	String getFutureBookings(
		HttpServletRequest request
	);
	
	
    /************************************* FETCH COUNTRY BY ID ***************************************/
	
	@ApiOperation(value="Fetch country by id")
	@GetMapping(
		value = EndpointURL.GET_COUNTRY_BY_ID, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCountryById(
		@PathVariable int countryId,
		HttpServletRequest request
	);
	
	/************************************* FETCH CURRENT VERSION ***************************************/
	
	@ApiOperation(value="Fetch version")
	@GetMapping(
		value = EndpointURL.GET_VERSION, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getVersion(
		HttpServletRequest request
	);
	
	/***************************************************************************************************/
	
}
