package com.mind.api.stars.controller;

public interface EmailService {

	void sendSimpleMessage(String from, String to, String subject, String text);
	void sendRequisitionMailOnOriginating(String strTravelId, String strReqType, String Suser_id, String strsesLanguage, String strCurrentYear);
}
