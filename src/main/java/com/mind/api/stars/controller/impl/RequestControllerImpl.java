package com.mind.api.stars.controller.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.EndpointURL;
import com.mind.api.stars.controller.EmailService;
import com.mind.api.stars.controller.RequestController;
import com.mind.api.stars.dto.AdvanceDetailResponseDTO;
import com.mind.api.stars.dto.ApiResponseDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.ApproverDetailDTO;
import com.mind.api.stars.dto.ApproverLevelWiseDTO;
import com.mind.api.stars.dto.AttachmentDetailsDTO;
import com.mind.api.stars.dto.CarCategoryDTO;
import com.mind.api.stars.dto.CarLocationDTO;
import com.mind.api.stars.dto.CheckApproverDTO;
import com.mind.api.stars.dto.CheckTESCountDTO;
import com.mind.api.stars.dto.CityAirportDTO;
import com.mind.api.stars.dto.CommentDetailsDTO;
import com.mind.api.stars.dto.CostCentreDTO;
import com.mind.api.stars.dto.DeleteJourneyDetailRequestDTO;
import com.mind.api.stars.dto.ExchangeRateRequestDTO;
import com.mind.api.stars.dto.ExchangeRateResponseDTO;
import com.mind.api.stars.dto.FeedbackRequestDTO;
import com.mind.api.stars.dto.GroupTravelRequestDTO;
import com.mind.api.stars.dto.GroupTravellerDTO;
import com.mind.api.stars.dto.GroupTravellerDetailDTO;
import com.mind.api.stars.dto.IdentityProofDTO;
import com.mind.api.stars.dto.IdentityProofDetailsDTO;
import com.mind.api.stars.dto.JourneyDTO;
import com.mind.api.stars.dto.LinkTravelRequestDTO;
import com.mind.api.stars.dto.MailDetailDTO;
import com.mind.api.stars.dto.MailFlowDTO;
import com.mind.api.stars.dto.MealDTO;
import com.mind.api.stars.dto.OOOApproverDTO;
import com.mind.api.stars.dto.PassportDetailDTO;
import com.mind.api.stars.dto.PostCommentDTO;
import com.mind.api.stars.dto.RequestDataDTO;
import com.mind.api.stars.dto.RequestDetailsDTO;
import com.mind.api.stars.dto.RequestSettingDTO;
import com.mind.api.stars.dto.RequestsCountDTO;
import com.mind.api.stars.dto.SeatDTO;
import com.mind.api.stars.dto.SeatRequestDTO;
import com.mind.api.stars.dto.SiteBasedUserRequestDTO;
import com.mind.api.stars.dto.StayTypeDTO;
import com.mind.api.stars.dto.TicketTypeDTO;
import com.mind.api.stars.dto.TimeModeResponseDTO;
import com.mind.api.stars.dto.TransitHouseDTO;
import com.mind.api.stars.dto.TravelClassDTO;
import com.mind.api.stars.dto.TravelClassRequestDTO;
import com.mind.api.stars.dto.TravelPreferencesDTO;
import com.mind.api.stars.dto.TravelRequestDTO;
import com.mind.api.stars.dto.TravelRequestDetailsDTO;
import com.mind.api.stars.dto.UnitDTO;
import com.mind.api.stars.dto.UnitLocationDTO;
import com.mind.api.stars.dto.UserDTO;
import com.mind.api.stars.dto.UserProfileInfoDTO;
import com.mind.api.stars.dto.WorkflowApproverDTO;
import com.mind.api.stars.enums.APICallAttributes;
import com.mind.api.stars.service.RequestService;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestParsingUtility;
import com.mind.api.stars.utility.ResponseGenerator;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@RequestMapping(EndpointURL.API)
@Slf4j
public class RequestControllerImpl implements RequestController {
	
		
	@Autowired
	private RequestService requestService;
	
	@Autowired
	EmailService emailService;
	
	
	private ApiResponseDTO setSuccessResponseStatus() {
		return new ApiResponseDTO(Constants.SUCCESS_CODE_200,Constants.SUCCESS) ;
	}
	
	private ApiResponseDTO setFailureResponseStatus() {
		return new ApiResponseDTO(Constants.ERROR_CODE_500,Constants.FAILURE) ;
	}

	private ApiResponseDTO setResponseStatus(Object dtoObject) {
		if(dtoObject == null) {
			return setFailureResponseStatus();
		} else {
			return setSuccessResponseStatus();
		}
	}

	
	@Override
	public String saveRequestData(
		@RequestBody TravelRequestDTO travelRequestDTO
	) {
		RequestDataDTO requestDataDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			requestDataDTO = requestService.saveRequestData(travelRequestDTO);
			
			apiResponseDTO = setResponseStatus(requestDataDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : saveRequestData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestDataDTO, false);
		}
		
		return response;
	}

	
	@Override
	public String getRequestDetails(
		@PathVariable long travelId, 
		@PathVariable String travelType, 
		HttpServletRequest request
	) {
		TravelRequestDetailsDTO travelRequestDetailsDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setTravelId(travelId);
			appRequestDTO.setTravelType(travelType);
			
			travelRequestDetailsDTO = requestService.getRequestDetails(appRequestDTO);

			apiResponseDTO = setResponseStatus(travelRequestDetailsDTO);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getRequestDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), travelRequestDetailsDTO, false);
		}
		
		return response;
	}
	

	@Override
	public String getRecentTravelRequestsData(
		HttpServletRequest request
	) {
		List<RequestDetailsDTO> recentRequestsData = new ArrayList<>();
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			recentRequestsData 	= requestService.getRecentTravelRequestsData(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRecentTravelRequestsData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
	   
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), recentRequestsData, true);
		}
		
		return response;
	}
	
	
	@Override
	public String getLastApprovedRequestDetailsList(
		@PathVariable int last,
		@PathVariable String source,
		@PathVariable String destination,
		HttpServletRequest request
	) {
		List<TravelRequestDetailsDTO> travelRequestDetailsDTOList = new ArrayList<>();
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setLast(last);
			appRequestDTO.setSource(source);
			appRequestDTO.setDestination(destination);
			
			travelRequestDetailsDTOList = requestService.getLastApprovedRequestDetailsList(appRequestDTO);

			apiResponseDTO = setSuccessResponseStatus();

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getLastApprovedRequestDetailsList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), travelRequestDetailsDTOList, true);
		}
		
		return response;
	}


	@Override
	public String getRequestsCount(
		@PathVariable String travelType,
		@PathVariable int last,
		HttpServletRequest request
	) {
		RequestsCountDTO requestsCountDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setTravelType(travelType);
			appRequestDTO.setLast(last);
			
			requestsCountDTO = requestService.getRequestsCount(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(requestsCountDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestsCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestsCountDTO, false);
		}
		
		return response;
	}


	@Override
	public String getRequestsByType(
		@PathVariable String travelType, 
		@PathVariable String requestType, 
		@PathVariable int last,
		HttpServletRequest request
	) {
		List<RequestDetailsDTO> requestDetailsDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setTravelType(travelType);
			appRequestDTO.setRequestType(requestType);
			appRequestDTO.setLast(last);
			
			requestDetailsDTO 	= requestService.getRequestsByType(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestsByType() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestDetailsDTO, true);
		}
		
		return response;
	}


	@Override
	public String getRequestWorkflow(
		@PathVariable String travelType, 
		@PathVariable long travelId, 
		HttpServletRequest request
	) {
		List<WorkflowApproverDTO> requestWorkflow = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setTravelType(travelType);
			appRequestDTO.setTravelId(travelId);
			
			requestWorkflow	= requestService.getRequestWorkflow(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(requestWorkflow);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestWorkflow, true);
		}
		
		return response;
	}
	
	@Override
    public String getUpcomingJourneyDetails(
    	@PathVariable String travelType, 
    	@PathVariable int count, 
    	HttpServletRequest request
    ) {
        List<JourneyDTO> upcomingJourneyDetailsDTO = null;
        String response = "[]";
        ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
        try {
            AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
            appRequestDTO.setTravelType(travelType);
            appRequestDTO.setRequestCount(count);
            
            upcomingJourneyDetailsDTO = requestService.getUpcomingJourneyDetails(appRequestDTO);
            
            apiResponseDTO = setSuccessResponseStatus();
            
        } catch (Exception e) {
        	apiResponseDTO = setFailureResponseStatus();
            
            log.error("Error occurred in RequestControllerImpl : getUpcomingJourneyDetails() : ", e);
            ExceptionLogger.logExceptionToDB(e);
        
        } finally {
            response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), upcomingJourneyDetailsDTO, true);
        }
        
        return response;
    }
	
	@Override
	public String getRequestSetting(
		@PathVariable String pageType, 
		HttpServletRequest request
	) {
		RequestSettingDTO requestSettingDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setPageType(pageType);
			
			requestSettingDTO = requestService.getRequestSetting(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(requestSettingDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestSetting() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestSettingDTO, false);
		}
		
		return response;
	}
	

	@Override
	public String getLevelWiseApprover(@PathVariable String level, HttpServletRequest request) {
		String response = "[]";
		List<ApproverDetailDTO> approversDetailsDTO = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			if(Constants.FIRST.equals(level)) {
				approversDetailsDTO = requestService.getFirstLevelApprovers(appRequestDTO);
			} else if(Constants.SECOND.equals(level)) {
				approversDetailsDTO = requestService.getSecondLevelApprovers(appRequestDTO);
			} else if(Constants.THIRD.equals(level)) {
				approversDetailsDTO = requestService.getThirdLevelApprovers(appRequestDTO);
			} else {
				approversDetailsDTO = new ArrayList<>();
			}
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getFirstLevelApprover() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), approversDetailsDTO, true);
		}
		
		return response;
	}

	@Override
	public String getDefaultWorkflowApprover(HttpServletRequest request) {
		
		String response = "[]";
		List<WorkflowApproverDTO> requestWorkflow = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			requestWorkflow	= requestService.getDefaultWorkflowApprover(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestWorkflow, true);
		}
		
		return response;
	}

	@Override
	public String getApproverLevelWise(HttpServletRequest request) {
		String response = "[]";
		ApproverLevelWiseDTO approverLevelWiseDTO = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			approverLevelWiseDTO = requestService.getApproverLevelWise(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getSecondLevelApprover() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), approverLevelWiseDTO, false);
		}
		
		return response;
	}

	@Override
	public String getExchangeRate(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		ExchangeRateResponseDTO exchangeRateResponseDTO = null;
		
		try {
			ExchangeRateRequestDTO exchangeRateRequestDTO = RequestParsingUtility.fillDTOForExchangeRate(request);
			if(!AppUtility.checkNull(exchangeRateRequestDTO.getCurrentDate()) && !exchangeRateRequestDTO.getCurrentDate().isEmpty()) {
				exchangeRateRequestDTO.setCurrentDate(AppUtility.convertMillisToDate(exchangeRateRequestDTO.getCurrentDate(), Constants.DATE_FORMAT_IN));
			} else {
				exchangeRateRequestDTO.setCurrentDate(AppUtility.convertMillisToDate(AppUtility.syncDate(), Constants.DATE_FORMAT_IN));
			}
			exchangeRateResponseDTO = requestService.getExchangeRate(exchangeRateRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getSecondLevelApprover() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			 response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), exchangeRateResponseDTO, false);
		}
		
		return response;
	}

	@Override
	public String getForexTotal(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		AdvanceDetailResponseDTO advanceDetailResponseDTO = null;
		
		try {
			ExchangeRateRequestDTO exchangeRateRequestDTO = RequestParsingUtility.fillDTOForExchangeRate(request);
			if(!AppUtility.checkNull(exchangeRateRequestDTO.getCurrentDate()) && !exchangeRateRequestDTO.getCurrentDate().isEmpty()) {
				exchangeRateRequestDTO.setCurrentDate(AppUtility.convertMillisToDate(exchangeRateRequestDTO.getCurrentDate(), Constants.DATE_FORMAT_IN));
			} else {
				exchangeRateRequestDTO.setCurrentDate(AppUtility.convertMillisToDate(AppUtility.syncDate(), Constants.DATE_FORMAT_IN));
			}
			
			advanceDetailResponseDTO = requestService.getForexTotal(exchangeRateRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getForexTotal() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			 response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), advanceDetailResponseDTO, false);
		}
		
		return response;
	}

	@Override
	public String getSiteBasedUsers(@PathVariable String selectedSiteId, HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		List<UserDTO> userList = null;
		try {
			
			SiteBasedUserRequestDTO siteBasedUserRequestDTO = RequestParsingUtility.fillDTOForSiteBasedUser(request);
			if(!AppUtility.checkNull(selectedSiteId) && !selectedSiteId.isEmpty()) {
				siteBasedUserRequestDTO.setSelectedSiteId(selectedSiteId);
			} else  {
				siteBasedUserRequestDTO.setSelectedSiteId(String.valueOf(siteBasedUserRequestDTO.getSiteId()));
			}
			userList = requestService.getSiteBasedUsers(siteBasedUserRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getSiteBasedUsers() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), userList, true);
		}
		
		return response;
	}

	@Override
	public String getSeatList(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		List<SeatDTO> seatList = null;
		try {
			SeatRequestDTO seatRequestDTO  = RequestParsingUtility.fillDTOForSeat(request);
			seatList = requestService.getSeatList(seatRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getSeatList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			 response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), seatList, true);
		}
		
		return response;
	}
	
	
	@Override
	public String getTravelClassList(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		List<TravelClassDTO> travelClassList = null;
		try {
			TravelClassRequestDTO travelClassRequestDTO  = RequestParsingUtility.fillDTOForTravelClass(request);
			travelClassList = requestService.getTravelClassList(travelClassRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getSeatList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			 response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), travelClassList, true);
		}
		
		return response;
	}

	@Override
	public String getTimeModeList(HttpServletRequest request) {
		String response = "[]";
		List<TimeModeResponseDTO> timeModeResponseDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			timeModeResponseDTOList = requestService.getTimeModeList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getTimeModeList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), timeModeResponseDTOList, true);
		}
		
		return response;
	}

	@Override
	public String getUnitLocationList(HttpServletRequest request) {
		String response = "[]";
		List<UnitLocationDTO> unitLocationDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			unitLocationDTOList = requestService.getUnitLocationList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getUnitLocationList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), unitLocationDTOList, true);
		}
		
		return response;
	}

	@Override
	public String getBillingUnitList(HttpServletRequest request) {
		String response = "[]";
		List<UnitDTO> billingUnitDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			billingUnitDTOList = requestService.getBillingUnitList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getBillingUnitList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), billingUnitDTOList, true);
		}
		
		return response;
	}
	
	@Override
	public String getBillingApproverList(HttpServletRequest request) {
		String response = "[]";
		List<UserDTO> billingApproverDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			billingApproverDTOList = requestService.getBillingApproverList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getBillingApproverList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), billingApproverDTOList, true);
		}
		
		return response;
	}

	@Override
	public String getMealList(HttpServletRequest request) {
		String response = "[]";
		List<MealDTO> mealList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			mealList = requestService.getMealList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getMealList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), mealList, true);
		}
		
		return response;
	}

	@Override
	public String getCostCentreList(HttpServletRequest request) {
		String response = "[]";
		List<CostCentreDTO> costCentreDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			costCentreDTOList = requestService.getCostCentreList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getCostCentreList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), costCentreDTOList, true);
		}
		
		return response;
	}

	@Override
	public String getCarCategoryList(HttpServletRequest request) {
		String response = "[]";
		List<CarCategoryDTO> carCategoryDTOList = null;
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			carCategoryDTOList = requestService.getCarCategoryList(appRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getCarCategoryList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), carCategoryDTOList, true);
		}
		
		return response;
	}

	@Override
	public String checkTESCount(HttpServletRequest request) {
		CheckTESCountDTO checkTESCountDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			checkTESCountDTO = requestService.checkTESCount(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(checkTESCountDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : checkTESCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), checkTESCountDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String getTravelPreferences(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		TravelPreferencesDTO travelPreferences = new TravelPreferencesDTO();
		try {
			TravelClassRequestDTO travelClassRequestDTO  = RequestParsingUtility.fillDTOForTravelClass(request);
			if(!AppUtility.checkNull(travelClassRequestDTO.getSyncDate()) && !travelClassRequestDTO.getSyncDate().isEmpty()) {
				travelClassRequestDTO.setSyncDate(AppUtility.convertMillisToDate(travelClassRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				travelClassRequestDTO.setSyncDate(Constants.NULL);
			}
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			SeatRequestDTO seatRequestDTO  = RequestParsingUtility.fillDTOForSeat(request);
			if(!AppUtility.checkNull(seatRequestDTO.getSyncDate()) && !seatRequestDTO.getSyncDate().isEmpty()) {
				seatRequestDTO.setSyncDate(AppUtility.convertMillisToDate(seatRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				seatRequestDTO.setSyncDate(Constants.NULL);
			}
			
			travelPreferences.setTravelClassList(requestService.getTravelClassList(travelClassRequestDTO));
			travelPreferences.setSeatList(requestService.getSeatList(seatRequestDTO));
			travelPreferences.setMealList(requestService.getMealList(appRequestDTO));
			travelPreferences.setTimeModeList(requestService.getTimeModeList(appRequestDTO));
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : getTravelPreferences() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			 response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), travelPreferences, false);
		}
		
		return response;
	}

	@Override
	public String checkApproverExistsInDefaultWorkflow(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		CheckApproverDTO checkApproverDTO = null;
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			checkApproverDTO = requestService.checkApproverExistsInDefaultWorkflow(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(checkApproverDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : checkApproverExistsInDefaultWorkflow() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), checkApproverDTO, false);
		}
		
		return response;
	}

	@Override
	public String getIdentityProofList(HttpServletRequest request) {
		List<IdentityProofDTO> identityProofDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			identityProofDTO = requestService.getIdentityProofList(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(identityProofDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getIdentityProofList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), identityProofDTO, true);
		}
		
		return response;
	}

	@Override
	public String getIdentityProofDetails(HttpServletRequest request) {
		IdentityProofDetailsDTO identityProofDetailsDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			identityProofDetailsDTO = requestService.getIdentityProofDetails(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(identityProofDetailsDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getIdentityProofDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), identityProofDetailsDTO, false);
		}
		
		return response;
	}

	@Override
	public String getPassportDetails(HttpServletRequest request) {
		PassportDetailDTO passportDetailDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			passportDetailDTO = requestService.getPassportDetails(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(passportDetailDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getPassportDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), passportDetailDTO, false);
		}
		
		return response;
	}

	@Override
	public String getUserDetails(HttpServletRequest request) {
		UserProfileInfoDTO userProfileInfoDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			userProfileInfoDTO = requestService.getUserDetails(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(userProfileInfoDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getUserDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), userProfileInfoDTO, false);
		}
		
		return response;
	}

	@Override
	public String getTransitHouseList(HttpServletRequest request) {
		List<TransitHouseDTO> transitHouseDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			transitHouseDTO = requestService.getTransitHouseList(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(transitHouseDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getTransitHouseList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), transitHouseDTO, true);
		}
		
		return response;
	}

	@Override
	public String getStayTypeList(HttpServletRequest request) {
		List<StayTypeDTO> stayTypeList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			stayTypeList = requestService.getStayTypeList(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(stayTypeList);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getStayTypeList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), stayTypeList, true);
		}
		
		return response;
	}

	@Override
	public String deleteTravelRequest(HttpServletRequest request) {
    
       String response = "[]";
       ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
       
       try {
          AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
          AppRequestDTO requestDataDTO = requestService.deleteRequestData(appRequestDTO);
          
          apiResponseDTO = setResponseStatus(requestDataDTO);
              
       } catch (Exception e) {
          apiResponseDTO = setFailureResponseStatus();
          
          log.error("Error occurred in RequestControllerImpl : deleteTravelRequest() : ", e);
          ExceptionLogger.logExceptionToDB(e);
       
       } finally {
    	  response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
       }
       
       return response;
    }
	
	@Override
	public String getRequestCommentsDetails(
		HttpServletRequest request
	) {
		CommentDetailsDTO commentDetailsDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			commentDetailsDTO 	= requestService.getRequestCommentsDetails(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestCommentsDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), commentDetailsDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String deleteRequestComment(
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			errorMessage 	= requestService.deleteRequestComment(appRequestDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			}else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : deleteRequestComment() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		
		return response;
	}
	
	@Override
	public String saveRequestComment(
		@RequestBody PostCommentDTO postCommentDTO,
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			errorMessage = requestService.saveRequestComment(postCommentDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			} else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : saveRequestComment() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		return response;
	}
	
	@Override
	public String cancelTravelRequest(
		@RequestBody PostCommentDTO postCommentDTO,
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			errorMessage = requestService.cancelTravelRequest(postCommentDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			} else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : cancelTravelRequest() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		
		return response;
	}
	
	@Override
	public String getRequestCancelDetails(
		HttpServletRequest request
	) {
		CommentDetailsDTO commentDetailsDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			commentDetailsDTO 	= requestService.getRequestCancelDetails(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestCancelDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), commentDetailsDTO, false);
		}
		
		return response;
	}

	@Override
	public String getMailFlowList(HttpServletRequest request) {
		List<MailFlowDTO> mailFlowList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			mailFlowList = requestService.getMailFlowList(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(mailFlowList);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getMailFlowList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), mailFlowList, true);
		}
		
		return response;
	}
	
	@Override
	public String getMailDetail(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		MailDetailDTO mailDetailDTO = null;
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			mailDetailDTO = requestService.getMailDetail(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(mailDetailDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getMailDetail() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), mailDetailDTO, false);
		}
		
		return response;
	}

	@Override
	public String getCarLocationList(HttpServletRequest request) {
		List<CarLocationDTO> carLocationList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			carLocationList = requestService.getCarLocationList(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(carLocationList);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getCarLocationList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), carLocationList, true);
		}
		
		return response;
	}
	
	@Override
	public String getRequestAttachmentDetails(
		HttpServletRequest request
	) {
		AttachmentDetailsDTO attachmentDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			attachmentDTO 	= requestService.getRequestAttachmentDetails(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : getRequestAttachmentDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), attachmentDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String deleteRequestAttachment(
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			errorMessage 	= requestService.deleteRequestAttachment(appRequestDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			}else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : deleteRequestAttachment() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		
		return response;
	}
	
	@Override
	public String deleteRequestJourneyDetail(HttpServletRequest request) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			DeleteJourneyDetailRequestDTO deleteJourneyDetailRequestDTO = RequestParsingUtility.fillDTOForDeleteJourneyDetail(request);
			
			errorMessage 	= requestService.deleteRequestJourneyDetail(deleteJourneyDetailRequestDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			}else {
				apiResponseDTO.setResponseMessage(errorMessage);
				apiResponseDTO.setResponseStatus(Constants.ERROR_CODE_500);
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : deleteRequestJourneyDetail() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		
		return response;
	}
	
	@Override
	public String getRequestDetailToEdit(
		@PathVariable long travelId,
		@PathVariable String travelType,
		HttpServletRequest request
	) {
		TravelRequestDTO travelRequestDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setTravelId(travelId);
			appRequestDTO.setTravelType(travelType);
			
			travelRequestDTO = requestService.getRequestDetailToEdit(appRequestDTO);

			apiResponseDTO = setResponseStatus(travelRequestDTO);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getRequestDetailToEdit() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), travelRequestDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String getGroupUserDetailsList(
		HttpServletRequest request
	) {
		List<GroupTravellerDTO>  groupTravellerList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			groupTravellerList = requestService.getGroupUserDetailsList(appRequestDTO);

			apiResponseDTO = setResponseStatus(groupTravellerList);
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getGroupUserDetailsList() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), groupTravellerList, true);
		}
		return response;
	}
	
	@Override
	public String getGroupUserDetails(
		HttpServletRequest request
	) {
		GroupTravellerDTO  groupTravellerDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			groupTravellerDTO = requestService.getGroupUserDetails(appRequestDTO);

			apiResponseDTO = setResponseStatus(groupTravellerDTO);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getGroupUserDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), groupTravellerDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String getGroupTravellersDetailsList(
		HttpServletRequest request
	) {
		List<GroupTravellerDTO>  groupTravellerDTOList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			groupTravellerDTOList = requestService.getGroupTravellersDetailsList(appRequestDTO);

			apiResponseDTO = setResponseStatus(groupTravellerDTOList);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getGroupTravellersDetailsList() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), groupTravellerDTOList, true);
		}
		
		return response;
	}
	
	@Override
	public String getGroupTravellerDetailsToEdit(
		HttpServletRequest request
	) {
		GroupTravellerDetailDTO  groupTravellerDetailDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			groupTravellerDetailDTO = requestService.getGroupTravellerDetailsToEdit(appRequestDTO);

			apiResponseDTO = setResponseStatus(groupTravellerDetailDTO);
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getGroupTravellerDetailsToEdit() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), groupTravellerDetailDTO, false);
		}
		
		return response;
	}
	
	@Override
	public String deleteTravellerDetails(
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			errorMessage 	= requestService.deleteTravellerDetails(appRequestDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			}else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : deleteTravellerDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		
		return response;
	}
	
	@Override
	public String testEmail(
			HttpServletRequest request
		) 
	{
			try {
				emailService.sendRequisitionMailOnOriginating("112613", "Domestic Travel", "4375", "en_US", "2018");
			} catch (Exception e) {
				ExceptionLogger.logExceptionToDB(e);
			}
			return "[]";
	}
	
	@Override
	public String saveFeedback(
		@RequestBody FeedbackRequestDTO feedbackRequestDTO,
		HttpServletRequest request
	) {
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		String errorMessage = "";
		
		try {
			errorMessage = requestService.saveFeedback(feedbackRequestDTO);
			
			if("".equals(errorMessage)) {
				apiResponseDTO = setSuccessResponseStatus();
			} else {
				apiResponseDTO = setFailureResponseStatus();
			}
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in RequestControllerImpl : saveFeedback() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), null, false);
		}
		return response;
	}
	
	@Override
	public String getTravellerCancelledRequest(
		HttpServletRequest request
	) {
		List<LinkTravelRequestDTO>  linkTravelRequestDTOList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			linkTravelRequestDTOList = requestService.getTravellerCancelledRequest(appRequestDTO);

			apiResponseDTO = setResponseStatus(linkTravelRequestDTOList);
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getTravellerCancelledRequest() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), linkTravelRequestDTOList, true);
		}
		
		return response;
	}
	
	
	@Override
	public String getTicketType(
		HttpServletRequest request
	) {
		List<TicketTypeDTO>  ticketTypeDTOList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			ticketTypeDTOList = requestService.getTicketType(appRequestDTO);

			apiResponseDTO = setResponseStatus(ticketTypeDTOList);
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getTicketType() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), ticketTypeDTOList, true);
		}
		
		return response;
	}

	@Override
	public String getCityAirportList(HttpServletRequest request) {
		List<CityAirportDTO>  cityAirportDTOList = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			cityAirportDTOList = requestService.getCityAirportList(appRequestDTO);

			apiResponseDTO = setResponseStatus(cityAirportDTOList);
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in RequestControllerImpl : getCityAirportList() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), cityAirportDTOList, true);
		}
		
		return response;
	}
	
	
	@Override
	public String saveGroupRequestData(
		@RequestBody GroupTravelRequestDTO groupTravelRequestDTO
	) {
		RequestDataDTO requestDataDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			requestDataDTO = requestService.saveGroupRequestData(groupTravelRequestDTO);
			
			apiResponseDTO = setResponseStatus(requestDataDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : saveGroupRequestData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), requestDataDTO, false);
		}
		
		return response;
	}

	@Override
	public String checkOOOApprover(HttpServletRequest request) {
		
		OOOApproverDTO oooApproverDTO = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			oooApproverDTO = requestService.checkOOOApprover(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(oooApproverDTO);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in RequestControllerImpl : checkOOOApprover() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), oooApproverDTO, false);
		}
		
		return response;
	}
	
}
