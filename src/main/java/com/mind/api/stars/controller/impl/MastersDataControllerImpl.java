package com.mind.api.stars.controller.impl;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.EndpointURL;
import com.mind.api.stars.controller.MastersDataController;
import com.mind.api.stars.dto.ApiResponseDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.AttachmentRequestDTO;
import com.mind.api.stars.dto.AttachmentResponseDTO;
import com.mind.api.stars.dto.CityResponseDTO;
import com.mind.api.stars.dto.CountryResponseDTO;
import com.mind.api.stars.dto.CurrencyDTO;
import com.mind.api.stars.dto.TimeDTO;
import com.mind.api.stars.dto.UnitDTO;
import com.mind.api.stars.enums.APICallAttributes;
import com.mind.api.stars.service.MastersDataService;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestParsingUtility;
import com.mind.api.stars.utility.ResponseGenerator;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@RequestMapping(EndpointURL.MASTER)
@Slf4j
public class MastersDataControllerImpl implements MastersDataController {
	
	@Autowired
	private MastersDataService mastersDataService;
	
	private ApiResponseDTO setSuccessResponseStatus() {
		return new ApiResponseDTO(Constants.SUCCESS_CODE_200,Constants.SUCCESS) ;
	}
	
	private ApiResponseDTO setFailureResponseStatus() {
		return new ApiResponseDTO(Constants.ERROR_CODE_500,Constants.FAILURE) ;
	}

	private ApiResponseDTO setResponseStatus(Object dtoObject) {
		if(dtoObject == null) {
			return setFailureResponseStatus();
		} else {
			return setSuccessResponseStatus();
		}
	}
	
	
	@Override
	public String getCityMasterData(
		@PathVariable String syncDate, 
		HttpServletRequest request
	) {
		CityResponseDTO cityResponse = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
            appRequestDTO.setSyncDate(Constants.ZERO.equals(syncDate) ? "" : AppUtility.convertMillisToDate(syncDate));
            
			cityResponse = mastersDataService.getCityMasterData(appRequestDTO);

			apiResponseDTO = setSuccessResponseStatus();

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in MastersDataControllerImpl : getCityMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), cityResponse, false);
		}
		
		return response;
	}
	
	
	@Override
	public String getCountryMasterData(String syncDate, HttpServletRequest request) {
		CountryResponseDTO countryResponse = null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
            if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
            
            countryResponse = mastersDataService.getCountryMasterData(appRequestDTO);

			apiResponseDTO = setSuccessResponseStatus();

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in MastersDataControllerImpl : getCountryMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), countryResponse, false);
		}
		
		return response;
	}


	@Override
	public String getTimeMasterData(
		HttpServletRequest request
	) {
		List<TimeDTO> timeList 	= null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			timeList = mastersDataService.getTimeMasterData(appRequestDTO);

			apiResponseDTO = setResponseStatus(timeList);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in MastersDataControllerImpl : getTimeMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), timeList, true);
		}

		return response;
	}

	@Override
	public String getUnitMasterData(HttpServletRequest request) {
		
		List<UnitDTO> unitList 	= null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			} else {
				appRequestDTO.setSyncDate(Constants.NULL);
			}
			unitList = mastersDataService.getUnitMasterData(appRequestDTO);

			apiResponseDTO = setResponseStatus(unitList);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in MastersDataControllerImpl : getUnitMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), unitList, true);
		}

		return response;
	}

	@Override
	public String getCurrencyMasterData(HttpServletRequest request) {
		
		List<CurrencyDTO> currencyList 	= null;
		String response = "[]";
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();

		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			if(!AppUtility.checkNull(appRequestDTO.getSyncDate()) && !appRequestDTO.getSyncDate().isEmpty()) {
				appRequestDTO.setSyncDate(AppUtility.convertMillisToDate(appRequestDTO.getSyncDate(), Constants.DB_DATE_FORMAT));
			}
			currencyList = mastersDataService.getCurrencyMasterData(appRequestDTO);

			apiResponseDTO = setResponseStatus(currencyList);

		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();

			log.error("Error occurred in MastersDataControllerImpl : getCurrencyMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);

		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), currencyList, true);
		}

		return response;
	}

	@Override
	public String uploadAttachment(MultipartFile file, HttpServletRequest request) {
		
		String response = "[]";
		AttachmentResponseDTO attachmentResponseDTO = new AttachmentResponseDTO();
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AttachmentRequestDTO attachmentRequestDTO = RequestParsingUtility.fillDTOForAttachments(request, file);
			attachmentResponseDTO = mastersDataService.uploadAttachment(attachmentRequestDTO);
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in MastersDataControllerImpl : uploadAttachment() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), attachmentResponseDTO, false);
		}
		
		return response;
	}

	@Override
	public String getAttachment(@PathVariable String travelId, @PathVariable String travelType, HttpServletRequest request) {
		
		String response = "[]";
		List<AttachmentResponseDTO> attachmentDTOList = new ArrayList<AttachmentResponseDTO>();
		ApiResponseDTO apiResponseDTO = new ApiResponseDTO();
		
		try {
			AttachmentRequestDTO attachmentRequestDTO = RequestParsingUtility.fillDTOForAttachments(request, null);
			//attachmentRequestDTO.setAttachmentId(attachmentId);
			attachmentRequestDTO.setTravelId(AppUtility.convertStringToInt(travelId==null?"0":(travelId.isEmpty()?"0":travelId)));
			attachmentRequestDTO.setTravelType(travelType);
			attachmentDTOList = mastersDataService.getAttachment(attachmentRequestDTO);
			apiResponseDTO = setResponseStatus(attachmentDTOList);
			
		} catch(Exception ex) {
			apiResponseDTO = setFailureResponseStatus();
			log.error("Error occurred in MastersDataControllerImpl : getAttachment() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), attachmentDTOList, true);
		}
		
		return response;
	}


}
