package com.mind.api.stars.controller.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.EndpointURL;
import com.mind.api.stars.controller.DataController;
import com.mind.api.stars.dto.AirportDTO;
import com.mind.api.stars.dto.ApiResponseDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.BookingDTO;
import com.mind.api.stars.dto.CityDTO;
import com.mind.api.stars.dto.CountryDTO;
import com.mind.api.stars.dto.DailyAllowanceDTO;
import com.mind.api.stars.dto.HotelDTO;
import com.mind.api.stars.dto.VersionDetailDTO;
import com.mind.api.stars.service.DataService;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestParsingUtility;
import com.mind.api.stars.utility.ResponseGenerator;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@RequestMapping(EndpointURL.INFO)
@Slf4j
public class DataControllerImpl implements DataController {
	
	@Autowired
	private DataService dataService;
	
	private ApiResponseDTO setSuccessResponseStatus() {
		return new ApiResponseDTO(Constants.SUCCESS_CODE_200,Constants.SUCCESS) ;
	}
	
	private ApiResponseDTO setFailureResponseStatus() {
		return new ApiResponseDTO(Constants.ERROR_CODE_500,Constants.FAILURE) ;
	}

	private ApiResponseDTO setResponseStatus(Object dtoObject) {
		if(dtoObject == null) {
			return setFailureResponseStatus();
		} else {
			return setSuccessResponseStatus();
		}
	}

	
	@Override
	public String getCityByCountry(
		@PathVariable int countryId,
		HttpServletRequest request
	) {
		String response = "[]";
		CityDTO city = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setCountryId(countryId);
			
			city = dataService.getCityByCountry(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(city);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getCityByCountry() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), city, false);
		}
		
		return response;
	}
	
	
	@Override
	public String getAirportsInfo(
		HttpServletRequest request
	) {
		String response = "[]";
		List<AirportDTO> airportsInfo = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			airportsInfo = dataService.getAirportsInfo(appRequestDTO);
			
			apiResponseDTO = setResponseStatus(airportsInfo);
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getAirportsInfo() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), airportsInfo, true);
		}
		
		return response;
	}
	
	
	@Override
	public String getHotelsInfo(
		HttpServletRequest request
	) {
		String response = "[]";
		List<HotelDTO> hotelsInfo = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			hotelsInfo = dataService.getHotelsInfo(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getHotelsInfo() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), hotelsInfo, true);
		}
		
		return response;
	}

	
	@Override
	public String getdailyAllowanceAmount(
		HttpServletRequest request
	) {
		String response = "[]";
		DailyAllowanceDTO dailyAllowanceInfo = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			dailyAllowanceInfo 	= dataService.getdailyAllowanceAmount(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getdailyAllowanceAmount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), dailyAllowanceInfo, false);
		}
		
		return response;
	}

	
	@Override
	public String getPastBookings(
		HttpServletRequest request
	) {
		String response = "[]";
		List<BookingDTO> pastBookings = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			pastBookings = dataService.getPastBookings(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getPastBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), pastBookings, true);
		}
		
		return response;
	}

	
	@Override
	public String getFutureBookings(
		HttpServletRequest request
	) {
		String response = "[]";
		List<BookingDTO> futureBookings = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			futureBookings = dataService.getFutureBookings(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getFutureBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), futureBookings, true);
		}
		
		return response;
	}
	
	@Override
	public String getCountryById(
		@PathVariable int countryId,
		HttpServletRequest request
	) {
		String response = "[]";
		CountryDTO countryDTO = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			appRequestDTO.setCountryId(countryId);
			
			countryDTO = dataService.getCountryById(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getFutureBookings() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), countryDTO, false);
		}
		
		return response;
	}

	@Override
	public String getVersion(HttpServletRequest request) {

		String response = "[]";
		VersionDetailDTO versionDTO = null;
		ApiResponseDTO apiResponseDTO = null;
		
		try {
			AppRequestDTO appRequestDTO = RequestParsingUtility.fillDTOForGetRequests(request);
			
			versionDTO = dataService.getCurrentVersion(appRequestDTO);
			
			apiResponseDTO = setSuccessResponseStatus();
			
		} catch (Exception e) {
			apiResponseDTO = setFailureResponseStatus();
			
			log.error("Error occurred in DataControllerImpl : getVersion() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		
		} finally {
			response = ResponseGenerator.generateResponse(apiResponseDTO.getResponseStatus(), apiResponseDTO.getResponseMessage(), versionDTO, false);
		}
		
		return response;
	
	}
	

}