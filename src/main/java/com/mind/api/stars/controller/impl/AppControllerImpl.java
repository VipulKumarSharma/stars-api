package com.mind.api.stars.controller.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ControllerURL;
import com.mind.api.stars.common.ModelAndViewConstants;
import com.mind.api.stars.controller.AppController;
import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.logic.AppLogic;
import com.mind.api.stars.utility.AppUtility;

import lombok.extern.slf4j.Slf4j;

@Controller
@CrossOrigin("*")
@Slf4j
public class AppControllerImpl implements AppController {
	
	@Autowired 
	private HttpSession httpSession;
	 
	@Resource
	private AppLogic appLogic;
	
	@Value("${app.name}")
	private String appName;
	
	@Value("${base.star.url}")
	private String baseStarUrl;
	
	@Override
	public ModelAndView loginPage(
		HttpServletRequest request
	) {
		if(!AppUtility.checkNull(httpSession)) {
			httpSession.invalidate();
		}
		
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.LOGIN);
		
		modelAndView.addObject(Constants.APP_NAME, appName);
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		modelAndView.addObject(Constants.APP_LANGUAGE, "en_US");
		
		return modelAndView;
	}

	@Override
	public ModelAndView login(
		@ModelAttribute LoginDTO loginDTO, 
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	) {
		ModelAndView modelAndView = new ModelAndView();
		
		UserInfoDTO userInfoDTO = appLogic.getUserInfo(loginDTO);
		
		if(!AppUtility.isNull(userInfoDTO)) {
			modelAndView.setViewName(ModelAndViewConstants.APP_HOME);
			
			userInfoDTO.setIpAddress(request.getRemoteAddr());
			userInfoDTO.setBrowser(AppUtility.getBrowserInfo(request.getHeader(Constants.USER_AGENT)));
			userInfoDTO.setLanguage(loginDTO.getLanguage());
			
			httpSession.setAttribute(Constants.USER, userInfoDTO);
			httpSession.setAttribute("encWinUserId", AppUtility.encrypt(userInfoDTO.getWinUserId()));
			httpSession.setAttribute("encDomainName", AppUtility.encrypt(userInfoDTO.getDomainName()));
			httpSession.setAttribute("baseStarUrl", baseStarUrl);
			
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.HOME);
			redirectAttributes.addFlashAttribute(Constants.USER, userInfoDTO);
			redirectAttributes.addFlashAttribute(Constants.PAGE, "loginWithCredentials");
			redirectAttributes.addFlashAttribute(Constants.ENC_PASSWORD, loginDTO.getEncPassword());
			redirectAttributes.addFlashAttribute("requestedScreen", "");
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "You have entered invalid credentials !");
		}
		
		return modelAndView;
	}
	
	
	@Override
	public ModelAndView ssoLogin(
		HttpServletRequest request, 
		RedirectAttributes redirectAttributes
	) {
		ModelAndView modelAndView = new ModelAndView();
		
		String encWinUserId = request.getParameter("winUserId");
		String encDomain = request.getParameter("domain");
		String requestedScreen =  request.getParameter("requestedScreen")==null?"":request.getParameter("requestedScreen");
		
		UserInfoDTO userInfoDTO = appLogic.ssoLogin(encWinUserId, encDomain);
		
		if(!AppUtility.isNull(userInfoDTO)) {
			modelAndView.setViewName(requestedScreen.equalsIgnoreCase("create")?ModelAndViewConstants.CREATE_REQUEST :ModelAndViewConstants.APP_HOME);
			
			userInfoDTO.setIpAddress(request.getRemoteAddr());
			userInfoDTO.setBrowser(AppUtility.getBrowserInfo(request.getHeader(Constants.USER_AGENT)));
			userInfoDTO.setLanguage("en_US");
			userInfoDTO.setSsoFlag("Y");
			
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.HOME);
			redirectAttributes.addFlashAttribute(Constants.USER, userInfoDTO);
			redirectAttributes.addFlashAttribute(Constants.PAGE, "ssoLogin");
			redirectAttributes.addFlashAttribute("requestedScreen", requestedScreen);
			
			httpSession.setAttribute("encWinUserId", encWinUserId);
			httpSession.setAttribute("encDomainName", encDomain);
			httpSession.setAttribute("baseStarUrl", baseStarUrl);
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "SSO login failed !");
		}
		
		return modelAndView;
	}
	
	
	@Override
	public ModelAndView acceptPolicy(
		@ModelAttribute UserInfoDTO userInformationDTO, 
		HttpServletRequest request, 
		RedirectAttributes redirectAttributes
	) {
		
		ModelAndView modelAndView = new ModelAndView();
		String buildstamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH));
		
		userInformationDTO.setIpAddress(request.getRemoteAddr());
		userInformationDTO.setBrowser(AppUtility.getBrowserInfo(request.getHeader(Constants.USER_AGENT)));
		
		UserInfoDTO userInfoDTO = appLogic.acceptPolicy(userInformationDTO);
		
		if(!AppUtility.isNull(userInfoDTO)) {
			if(!AppUtility.isNull(userInfoDTO.getFirstName())) {
				modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.HOME);
				redirectAttributes.addFlashAttribute(Constants.USER, userInfoDTO);
				redirectAttributes.addFlashAttribute(Constants.PAGE, "acceptPolicy");
				
			} else {
				modelAndView.setViewName(ModelAndViewConstants.CHANGE_PASSWORD);
				modelAndView.addObject(Constants.USER, userInfoDTO);
				modelAndView.addObject(Constants.BUILDSTAMP, buildstamp);
			}
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "Some error occurred while accepting policy !");
		}
		
		return modelAndView;
	}
	
	
	@Override
	public ModelAndView home(
		@ModelAttribute(Constants.USER) UserInfoDTO userInfoDTO, 
		@ModelAttribute(Constants.PAGE) String fromPage, 
		@ModelAttribute(Constants.ENC_PASSWORD) String encPassword,
		@ModelAttribute("requestedScreen") String requestedScreen,
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	) {
		
		if(!AppUtility.checkNull(httpSession)) {
			if(AppUtility.isNull(userInfoDTO.getUserName())) {
				userInfoDTO = (UserInfoDTO) httpSession.getAttribute(Constants.USER);
			}
			if("".equals(fromPage)) {
				fromPage = AppUtility.checkNullString(httpSession.getAttribute(Constants.PAGE));
			}
			if("".equals(encPassword)) {
				encPassword = AppUtility.checkNullString(httpSession.getAttribute(Constants.ENC_PASSWORD));
			}
		}
		
		if(!AppUtility.checkNull(userInfoDTO) && !AppUtility.checkNull(userInfoDTO.getUserId())) {
			httpSession.setAttribute(Constants.USER, userInfoDTO);
			httpSession.setAttribute(Constants.LOGGED_IN_USER_ID, userInfoDTO.getUserId());
		}
		if(!AppUtility.checkNull(fromPage)) {
			httpSession.setAttribute(Constants.PAGE, fromPage);
		}
		if(!AppUtility.checkNull(encPassword)) {
			httpSession.setAttribute(Constants.ENC_PASSWORD, encPassword);
		}
		
		ModelAndView modelAndView = new ModelAndView();
		
		if("loginWithCredentials".equalsIgnoreCase(fromPage)) {
			setViewDetailsByUserData(userInfoDTO, modelAndView, encPassword, redirectAttributes);
			
		} else if("ssoLogin".equalsIgnoreCase(fromPage)) {
			setSsoViewDetailsByUserData(userInfoDTO, modelAndView, redirectAttributes);
			if("create".equalsIgnoreCase(requestedScreen)) {
				modelAndView.setViewName(ModelAndViewConstants.CREATE_REQUEST);
			}
		} else if("acceptPolicy".equalsIgnoreCase(fromPage)) {
			modelAndView.setViewName(ModelAndViewConstants.APP_HOME);
			modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
			modelAndView.addObject(Constants.USER, userInfoDTO);
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "Some error occurred while opening homepage !");
		}

		return modelAndView;
	}
	
	private void setViewDetailsByUserData(UserInfoDTO userInfoDTO, ModelAndView modelAndView, String encPassword, RedirectAttributes redirectAttributes) {
		
		String decPassword = AppUtility.decrypt(encPassword);
		String buildstamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH));
		
		String encFlag = AppUtility.checkNullString(userInfoDTO.getEncFlag());
		String relievingDateFlag = AppUtility.checkNullString(userInfoDTO.getRelievingDateFlag());
		String dummyUserFlag = AppUtility.checkNullString(userInfoDTO.getDummyUserFlag());
		String acceptanceFlag = AppUtility.checkNullString(userInfoDTO.getAcceptanceFlag());
		
		if("Y".equalsIgnoreCase(encFlag) && "N".equalsIgnoreCase(relievingDateFlag) && "N".equalsIgnoreCase(dummyUserFlag)) {
			modelAndView.addObject(Constants.BUILDSTAMP, buildstamp);
			
			userInfoDTO.setPassword(encPassword);
			userInfoDTO.setSsoFlag("N");
			
			if(!"Y".equalsIgnoreCase(acceptanceFlag)) {
				modelAndView.setViewName(ModelAndViewConstants.ACCEPT_POLICY);
				modelAndView.addObject(Constants.USER, userInfoDTO);
				
			} else {
				modelAndView.setViewName(ModelAndViewConstants.APP_HOME);
				
				UserInfoDTO authenticatedUserInfo = appLogic.getAuthenticatedUserDetails(userInfoDTO);
				modelAndView.addObject(Constants.USER, authenticatedUserInfo);
				httpSession.setAttribute(Constants.USER, authenticatedUserInfo);
			}
		
		} else if("N".equalsIgnoreCase(encFlag) && "N".equalsIgnoreCase(relievingDateFlag) && "N".equalsIgnoreCase(dummyUserFlag)) {
			modelAndView.addObject(Constants.BUILDSTAMP, buildstamp);
			
			userInfoDTO.setPassword(decPassword);
			modelAndView.addObject(Constants.USER, userInfoDTO);
			
			if(!"Y".equalsIgnoreCase(acceptanceFlag)) {
				modelAndView.setViewName(ModelAndViewConstants.ACCEPT_POLICY);
			} else {
				modelAndView.setViewName(ModelAndViewConstants.CHANGE_PASSWORD);
			}
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "Some error occurred while setting homepage data !");
		}
	}

	private void setSsoViewDetailsByUserData(UserInfoDTO userInfoDTO, ModelAndView modelAndView, RedirectAttributes redirectAttributes) {
		
		String encFlag = AppUtility.checkNullString(userInfoDTO.getEncFlag());
		String acceptanceFlag = AppUtility.checkNullString(userInfoDTO.getAcceptanceFlag());
		String buildstamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH));
		
		if("Y".equalsIgnoreCase(encFlag)) {
			modelAndView.addObject(Constants.BUILDSTAMP, buildstamp);
			
			if(!"Y".equalsIgnoreCase(acceptanceFlag)) {
				modelAndView.setViewName(ModelAndViewConstants.ACCEPT_POLICY);
				modelAndView.addObject(Constants.USER, userInfoDTO);
				
			} else {
				modelAndView.setViewName(ModelAndViewConstants.APP_HOME);
				
				UserInfoDTO authenticatedUserInfo = appLogic.getAuthenticatedUserDetails(userInfoDTO);
				modelAndView.addObject(Constants.USER, authenticatedUserInfo);
				httpSession.setAttribute(Constants.USER, authenticatedUserInfo);
			}
		
		} else if("N".equalsIgnoreCase(encFlag)) {
			modelAndView.addObject(Constants.BUILDSTAMP, buildstamp);
			
			if(!"Y".equalsIgnoreCase(acceptanceFlag)) {
				modelAndView.setViewName(ModelAndViewConstants.ACCEPT_POLICY);
			} else {
				modelAndView.setViewName(ModelAndViewConstants.CHANGE_PASSWORD);
			}
			
			modelAndView.addObject(Constants.USER, userInfoDTO);
		
		} else {
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "Some error occurred while setting SSO homepage data !");
		}
	}
	
	
	@Override
	public ModelAndView apiHome() {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.API_HOME);
		modelAndView.addObject(Constants.APP_NAME, appName);
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		
		return modelAndView;
	}
	
	
	@Override
	public String getErrorPath() {
		return ControllerURL.ERROR;
	}
	
	
	@Override
	public ModelAndView error() {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.ERROR_PAGE);
		modelAndView.addObject(Constants.APP_NAME, appName);
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		modelAndView.addObject(Constants.ERROR_MESSAGE, "Something went wrong !");
		
		return modelAndView;
	}
	
	
	@Override
	public ModelAndView serverUnderMaintainance() {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.SERVER_MAINTAINANCE);
		modelAndView.addObject(Constants.APP_NAME, appName);
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		modelAndView.addObject(Constants.MESSAGE, "Server under maintenance");
		
		return modelAndView;
	}

	
	@Override
	public ModelAndView logout(
		HttpServletRequest request,
		RedirectAttributes redirectAttributes
	) {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.LOGIN);
		
		try {
			if(!AppUtility.checkNull(httpSession)) {
				
				String userId = AppUtility.checkNullString(httpSession.getAttribute(Constants.LOGGED_IN_USER_ID));
				int loggedInUserId = AppUtility.convertStringToInt(userId);
				
				appLogic.logout(loggedInUserId, request.getRemoteAddr());
				httpSession.invalidate();
			}
			
			modelAndView.setViewName(ControllerURL.REDIRECT + ControllerURL.ROOT);
			redirectAttributes.addFlashAttribute(Constants.MESSAGE, "You have been logged out from stars <br> successfully.");
		
		} catch (Exception e) {
			log.error("Error occurred in AppControllerImpl : logout() : ", e);
		}
		
		return modelAndView;
	}

	
	@Override
	public ModelAndView createRequest(
		HttpServletRequest request
	) {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.CREATE_REQUEST);
		setTravelRequestDataToModel(request, modelAndView);
		
		return modelAndView;
	}
	
	@Override
	public ModelAndView editRequest(
		HttpServletRequest request
	) {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.CREATE_REQUEST);
		setTravelRequestDataToModel(request, modelAndView);
		
		return modelAndView;
	}
	
	private void setTravelRequestDataToModel (HttpServletRequest request, ModelAndView modelAndView) {
		String travelId = request.getParameter(Constants.TRAVEL_ID) == null ? "0" : request.getParameter(Constants.TRAVEL_ID).trim();
		String travelType = request.getParameter(Constants.TRAVEL_TYPE) == null ? "D" : request.getParameter(Constants.TRAVEL_TYPE).trim();
		String groupTravelFlag = request.getParameter(Constants.GROUP_TRAVEL_FLAG) == null ? "N" : request.getParameter(Constants.GROUP_TRAVEL_FLAG).trim();
	
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		modelAndView.addObject(Constants.TRAVEL_ID, travelId);
		modelAndView.addObject(Constants.TRAVEL_TYPE, travelType);
		modelAndView.addObject(Constants.GROUP_TRAVEL_FLAG, groupTravelFlag);
	}
	
	@Override
	public ModelAndView approveRequest() {
		ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.APPROVE_REQUEST);
		modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
		
		return modelAndView;
	}
	
    @Override
    public ModelAndView requestList() {
        ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.REQUEST_LIST);
        modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
        
        return modelAndView;
    }
    
    @Override
    public ModelAndView requestDetails() {
        ModelAndView modelAndView = new ModelAndView(ModelAndViewConstants.REQUEST_DETAILS);
        modelAndView.addObject(Constants.BUILDSTAMP, LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constants.BUILDSTAMP_DATE_FORMAT, Locale.ENGLISH)));
        
        return modelAndView;
    }
    
}
