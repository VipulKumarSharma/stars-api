package com.mind.api.stars.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mind.api.stars.common.EndpointURL;
import com.mind.api.stars.dto.FeedbackRequestDTO;
import com.mind.api.stars.dto.GroupTravelRequestDTO;
import com.mind.api.stars.dto.PostCommentDTO;
import com.mind.api.stars.dto.TravelRequestDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value="RequestController")
public interface RequestController {
	
	/************************************* SAVE REQUEST **************************************************/
	
	@ApiOperation(value="Save request data")
	@ApiResponses(value= {
		@ApiResponse(code=200, message="Data saved successfully"),
		@ApiResponse(code=404, message="Resource not found"),
		@ApiResponse(code=500, message="Some error occurred while saving data")
	})
	@PostMapping(
		path = EndpointURL.TRAVEL_REQUEST, 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String saveRequestData(
		@RequestBody TravelRequestDTO travelRequestDTO
	);
	
	/************************************* FETCH REQUEST DETAILS *****************************************/
	
	@ApiOperation(value="Fetch request details")
	@GetMapping(
		path = EndpointURL.GET_REQUEST_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestDetails(
		@PathVariable long travelId, 
		@PathVariable String travelType,
		HttpServletRequest request
	);
	
	/************************************* FETCH TRAVELLER'S RECENT REQUESTS *****************************/
	
	@ApiOperation(value="Fetch traveller's recent travel requests")
	@GetMapping(
		path = EndpointURL.GET_RECENT_TRAVEL_REQUESTS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRecentTravelRequestsData(
		HttpServletRequest request
	);

	
	/************************************* FETCH USER'S LAST APPROVED REQUEST ****************************/
	
	@ApiOperation(value="Fetch user's last approved by all request")
	@GetMapping(
		path = EndpointURL.GET_USER_LAST_APPROVED_REQUEST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getLastApprovedRequestDetailsList(
		@PathVariable int last,
		@PathVariable String source,
		@PathVariable String destination,
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST'S COUNT BY TYPE *********************************/
	
	@ApiOperation(value="Fetch requests counts by type")
	@GetMapping(
		path = EndpointURL.GET_REQUESTS_COUNT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestsCount(
		@PathVariable String travelType,
		@PathVariable int last,
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST LIST BY TYPE ************************************/
	
	@ApiOperation(value="Fetch request list by type")
	@GetMapping(
		path = EndpointURL.GET_REQUESTS_BY_TYPE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestsByType(
		@PathVariable String travelType,  
		@PathVariable String requestType,
		@PathVariable int last,
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST WORKFLOW ****************************************/
	
	@ApiOperation(value="Fetch request workflow")
	@GetMapping(
		path = EndpointURL.GET_REQUEST_WORKFLOW, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestWorkflow(
		@PathVariable String travelType, 
		@PathVariable long travelId,  
		HttpServletRequest request
	);
	
	/************************************* FETCH USER'S UPCOMING JOURNEY DETAILS *************************/
	
	@ApiOperation(value="Fetch user's upcoming journey details")
	@GetMapping(
		path = EndpointURL.GET_UPCOMING_JOURNEY_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getUpcomingJourneyDetails(
		@PathVariable String travelType, 
		@PathVariable int count, 
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST FLAGS *******************************************/
	
	@ApiOperation(value="Fetch request flags")
	@GetMapping(
		path = EndpointURL.GET_REQUEST_SETTING, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestSetting(
		@PathVariable String pageType, 
		HttpServletRequest request
	);
	
	/***************************************************************************************************/
	
	
	/* Fetch First Level Approver */
	@ApiOperation(value="Fetch Level Wise Approver")
	@GetMapping(
		value = EndpointURL.GET_LEVEL_WISE_APPROVER, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getLevelWiseApprover(@PathVariable String level, HttpServletRequest request);
	
	
	
	/* Fetch Default Workflow Approver */
	@ApiOperation(value="Fetch Default Workflow Approver")
	@GetMapping(
		value = EndpointURL.GET_DEFAULT_WORKFLOW_APPROVER, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getDefaultWorkflowApprover(HttpServletRequest request);
	
	
	
	/* Fetch Approver Levels */
	@ApiOperation(value="Fetch All 3 Levels Approvers")
	@GetMapping(
		value = EndpointURL.GET_APPROVER_LEVELS_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getApproverLevelWise(HttpServletRequest request);
	
	
	
	/* Fetch Exchange Rate */
	@ApiOperation(value="Fetch All 3 Levels Approvers")
	@GetMapping(
		value = EndpointURL.GET_EXCHANGE_RATE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getExchangeRate(HttpServletRequest request);
	
	
	
	/* Fetch Forex Total By Currency */
	@ApiOperation(value="Fetch Forex Total By Currency")
	@GetMapping(
		value = EndpointURL.GET_FOREX_TOTAL_BY_CURRENCY, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getForexTotal(HttpServletRequest request);
	
	/* Fetch Users based on Site */
	@ApiOperation(value="Fetch Users based on Site")
	@GetMapping(
		value = EndpointURL.GET_SITE_BASED_USERS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getSiteBasedUsers(@PathVariable String selectedSiteId, HttpServletRequest request);
	
	
	/* Fetch Seat List */
	@ApiOperation(value="Fetch Seat List")
	@GetMapping(
		value = EndpointURL.GET_SEAT_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getSeatList(HttpServletRequest request);
	
	
	/* Fetch Travel Class List */
	@ApiOperation(value="Fetch Travel Class List")
	@GetMapping(
		value = EndpointURL.GET_TRAVEL_CLASS_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTravelClassList(HttpServletRequest request);
	
	
	/* Fetch Time Mode List */
	@ApiOperation(value="Fetch Time Mode List")
	@GetMapping(
		value = EndpointURL.GET_TIME_MODE_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTimeModeList(HttpServletRequest request);
	
	/* Fetch Unit Location List */
	@ApiOperation(value="Fetch Unit Location List")
	@GetMapping(
		value = EndpointURL.GET_UNIT_LOCATION_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getUnitLocationList(HttpServletRequest request);
	
	/* Fetch Billing Unit List */
	@ApiOperation(value="Fetch Billing Unit List")
	@GetMapping(
		value = EndpointURL.GET_BILING_UNIT_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getBillingUnitList(HttpServletRequest request);
	
	/* Fetch Billing Approver List */
	@ApiOperation(value="Fetch Billing Approver List")
	@GetMapping(
		value = EndpointURL.GET_BILING_APPROVER_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getBillingApproverList(HttpServletRequest request);
	
	
	/* Fetch Meal List */
	@ApiOperation(value="Fetch Meal List")
	@GetMapping(
		value = EndpointURL.GET_MEAL_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getMealList(HttpServletRequest request);
	
	/* Fetch Meal List */
	@ApiOperation(value="Fetch Cost Centre List")
	@GetMapping(
		value = EndpointURL.GET_COST_CENTRE_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCostCentreList(HttpServletRequest request);
	
	/* Fetch Meal List */
	@ApiOperation(value="Fetch Car Category List")
	@GetMapping(
		value = EndpointURL.GET_CAR_CATEGORY_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCarCategoryList(HttpServletRequest request);
	
	/* Fetch Meal List */
	@ApiOperation(value="Fetch TES count data")
	@GetMapping(
		value = EndpointURL.GET_TES_COUNT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String checkTESCount(HttpServletRequest request);
	
	/* Fetch Travel Preferences List */
	@ApiOperation(value="Fetch Travel Preferences List")
	@GetMapping(
		value = EndpointURL.GET_TRAVEL_PREFERENCES, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTravelPreferences(HttpServletRequest request);
	
	/* Check Approver exists in Default Workflow */
	@ApiOperation(value="Check Approver exists in Default Workflow")
	@GetMapping(
		value = EndpointURL.CHECK_APPROVER_IN_DEFAULT_WF, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String checkApproverExistsInDefaultWorkflow(HttpServletRequest request);
	
	/* Fetch Identity Proof List */
	@ApiOperation(value="Fetch Identity Proof List")
	@GetMapping(
		value = EndpointURL.GET_IDENTITY_PROOF_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getIdentityProofList(HttpServletRequest request);
	
	/* Fetch Identity Proof Details */
	@ApiOperation(value="Fetch Identity Proof Details")
	@GetMapping(
		value = EndpointURL.GET_IDENTITY_PROOF_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getIdentityProofDetails(HttpServletRequest request);
	
	/* Fetch Passport Details */
	@ApiOperation(value="Fetch Passport Details")
	@GetMapping(
		value = EndpointURL.GET_PASSPORT_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getPassportDetails(HttpServletRequest request);
	
	/* Fetch User Details */
	@ApiOperation(value="Fetch User Details")
	@GetMapping(
		value = EndpointURL.GET_USER_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getUserDetails(HttpServletRequest request);
	
	/* Fetch Transit House */
	@ApiOperation(value="Fetch Transit House")
	@GetMapping(
		value = EndpointURL.GET_TRANSIT_HOUSE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTransitHouseList(HttpServletRequest request);
	
	/* Fetch Stay Type */
	@ApiOperation(value="Fetch Stay Type")
	@GetMapping(
		value = EndpointURL.GET_STAY_TYPE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getStayTypeList(HttpServletRequest request);
	
	/************************************* DELETE TRAVEL REQUEST ************************************/
	
	@ApiOperation(value="To Delete Travel Request")
	@DeleteMapping(
			value= EndpointURL.TRAVEL_REQUEST,
			produces = MediaType.APPLICATION_JSON_VALUE
			//consumes = MediaType.APPLICATION_JSON_VALUE
			
			)
	String deleteTravelRequest(HttpServletRequest request); 
	
	/************************************* FETCH REQUEST DETAILS ON POST COMMENT ************************************/
	
	@ApiOperation(value="Fetch Travel Request Detail on Post Comment")
	@GetMapping(
		path = EndpointURL.TRAVEL_REQUEST_COMMENT,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestCommentsDetails(
		HttpServletRequest request
	);
	
	/************************************* DELETE REQUEST COMMENT ************************************/
	
	@ApiOperation(value="Delete Travel Request Comment")
	@DeleteMapping(
		path = EndpointURL.TRAVEL_REQUEST_COMMENT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String deleteRequestComment(
		HttpServletRequest request
	);
	
	/************************************* SAVE REQUEST COMMENT ************************************/
	
	@ApiOperation(value="Save Travel Request Comment")
	@PostMapping(
		path = EndpointURL.TRAVEL_REQUEST_COMMENT, 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String saveRequestComment(
		@RequestBody PostCommentDTO postCommentDTO,
		HttpServletRequest request
	);
	
	/************************************* CANCEL TRAVEl REQUEST ************************************/
	
	@ApiOperation(value="Cancel Travel Request")
	@PostMapping(
		path = EndpointURL.TRAVEL_REQUEST_CANCEL, 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String cancelTravelRequest(
		@RequestBody PostCommentDTO postCommentDTO,
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST DETAILS ON CANCEL ************************************/
	
	@ApiOperation(value="Fetch Travel Request Details on Cancel")
	@GetMapping(
		path = EndpointURL.TRAVEL_REQUEST_CANCEL,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestCancelDetails(
		HttpServletRequest request
	);
	
	/************************************* FETCH MAILFLOW LIST ************************************/
	
	@ApiOperation(value="Fetch MailFlow List")
	@GetMapping(
		value = EndpointURL.GET_MAILFLOW_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getMailFlowList(HttpServletRequest request);
	
	/************************************* FETCH MAIL DETAIL ************************************/
	
	@ApiOperation(value="Fetch Mail Detail")
	@GetMapping(
		value = EndpointURL.GET_MAIL_DETAIL, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getMailDetail(HttpServletRequest request);
	
	/************************************* FETCH CAR LOCATION LIST ************************************/
	
	@ApiOperation(value="Fetch Car Location List")
	@GetMapping(
		value = EndpointURL.GET_CAR_LOCATION_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCarLocationList(HttpServletRequest request);
	
	/************************************* FETCH REQUEST ATTACHMENT DETAILS ************************************/
	
	@ApiOperation(value="Fetch Request Attachment Detail ")
	@GetMapping(
		path = EndpointURL.TRAVEL_REQUEST_ATTACHMENT,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestAttachmentDetails(
		HttpServletRequest request
	);
	
	/************************************* DELETE REQUEST ATTACHMENT ************************************/
	
	@ApiOperation(value="Delete Request Attachment")
	@DeleteMapping(
		path = EndpointURL.TRAVEL_REQUEST_ATTACHMENT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String deleteRequestAttachment(
		HttpServletRequest request
	);
	
	/************************************* DELETE REQUEST JOURNEY DETAIL ************************************/
	
	@ApiOperation(value="Delete Travel Request Journey Detail")
	@DeleteMapping(
		path = EndpointURL.DELETE_REQ_JOURNEY_DETAIL, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String deleteRequestJourneyDetail(
		HttpServletRequest request
	);
	
	/************************************* FETCH REQUEST DETAILS FOR EDIT*****************************************/
	
	@ApiOperation(value="Fetch request details")
	@GetMapping(
		path = EndpointURL.GET_REQUEST_DETAILS_EDIT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getRequestDetailToEdit(
		@PathVariable long travelId, 
		@PathVariable String travelType,
		HttpServletRequest request
	);
	
	
	/************************************* FETCH GROUP GUEST USER DETAILS LIST FOR SELECTION *****************************************/
	
	@ApiOperation(value="Fetch group/guest user details list for selection")
	@GetMapping(
		path = EndpointURL.GET_GROUP_USER_DETAILS_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getGroupUserDetailsList(
		HttpServletRequest request
	);
	
	
	/************************************* FETCH GROUP GUEST USER DETAILS*****************************************/
	
	@ApiOperation(value="Fetch group/guest user details")
	@GetMapping(
		path = EndpointURL.GET_GROUP_USER_DETAILS, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getGroupUserDetails(
		HttpServletRequest request
	);
	
	/************************************* FETCH GROUP GUEST TRAVELLERS DETAILS LiST *****************************************/
	
	@ApiOperation(value="Fetch group/guest travellers details")
	@GetMapping(
		path = EndpointURL.GET_GROUP_TRAVELLERS_DETAILS_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getGroupTravellersDetailsList(
		HttpServletRequest request
	);
	
	/************************************* FETCH GROUP GUEST TRAVELLER DETAIL *****************************************/
	
	@ApiOperation(value="Fetch group/guest traveller detail")
	@GetMapping(
		path = EndpointURL.GROUP_TRAVELLER_DETAIL, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getGroupTravellerDetailsToEdit(
		HttpServletRequest request
	);
	
	/************************************* DELETE TRAVELLER DETAIL ************************************/
	
	@ApiOperation(value="To delete traveller details")
	@DeleteMapping(
			value= EndpointURL.GROUP_TRAVELLER_DETAIL,
			produces = MediaType.APPLICATION_JSON_VALUE
			)
	String deleteTravellerDetails(HttpServletRequest request);
	
	/******************************Temp Method To Test Emails********************************/
	@ApiOperation(value="testEmail")
	@GetMapping(
		path = "/test_email", 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String testEmail(
		HttpServletRequest request
	);
	
	/************************************* SAVE FEEDBACK ************************************/
	
	@ApiOperation(value="Save Feedback")
	@PostMapping(
		path = EndpointURL.SAVE_FEEDBACK, 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String saveFeedback(
		@RequestBody FeedbackRequestDTO feedbackRequestDTO,
		HttpServletRequest request
	);
	
	/************************************* FETCH CANCELLED REQUEST TRAVELLER *****************************************/
	
	@ApiOperation(value="Fetch cancelled of traveller ") 
	@GetMapping(
		path = EndpointURL.GET_LINK_TRAVEL_REQUEST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTravellerCancelledRequest(
		HttpServletRequest request
	);
	
   /************************************* FETCH TICKET TYPE *****************************************/
	
	@ApiOperation(value="Fetch ticket type") 
	@GetMapping(
		path = EndpointURL.GET_TICKET_TYPE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTicketType(
		HttpServletRequest request
	);
	
	/************************************* FETCH CITY/AIRPORT DETAIL *****************************************/
	
	@ApiOperation(value="Fetch city/airport details") 
	@GetMapping(
		path = EndpointURL.GET_CITY_AIRPORT_LIST, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCityAirportList(
		HttpServletRequest request
	);
	
	/************************************* SAVE GROUP TRAVEL REQUEST **************************************************/
	
	@ApiOperation(value="Save group travel request")
	@ApiResponses(value= {
		@ApiResponse(code=200, message="Data saved successfully"),
		@ApiResponse(code=404, message="Resource not found"),
		@ApiResponse(code=500, message="Some error occurred while saving data")
	})
	@PostMapping(
		path = EndpointURL.GROUP_TRAVEL_REQUEST, 
		consumes = MediaType.APPLICATION_JSON_VALUE, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String saveGroupRequestData(
		@RequestBody GroupTravelRequestDTO groupTravelRequestDTO
	);
	
/************************************* CHECK OUT OF OFFICE APPROVER *****************************************/
	
	@ApiOperation(value="Check Out Of Office Approver") 
	@GetMapping(
		path = EndpointURL.CHECK_OOO_APPROVER, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String checkOOOApprover(
		HttpServletRequest request
	);
	
}
