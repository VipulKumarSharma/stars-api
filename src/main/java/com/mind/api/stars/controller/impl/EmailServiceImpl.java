package com.mind.api.stars.controller.impl;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.controller.EmailService;
import com.mind.api.stars.dao.EmailDAO;
import com.mind.api.stars.utility.ExceptionLogger;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {

	@Autowired
	private JavaMailSender emailSender;
	
	@Resource
	private EmailDAO emailDAO; 
	
	@Override
	public void sendSimpleMessage(String from, String to, String subject, String text) {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");

		try {
			helper.setFrom(from);
			helper.setTo(to);
			helper.setText(text);
			helper.setSubject(subject);
			emailSender.send(message);
		} catch (MessagingException e) {
			log.error("Error occurred in EmailServiceImpl : sendSimpleMessage() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	}
	
	@Override
	public void sendRequisitionMailOnOriginating(String strTravelId, String strReqType, String sUserId, String strsesLanguage, String strCurrentYear) {
		try {
			emailDAO.sendReqsMailOnOriginating("115073", Constants.DOMESTIC_S_TRAVEL, "1");
			//emailDAO.sendReqsMailOnOriginating("43606", Constants.INTERNATIONAL_S_TRAVEL, "1");
			//emailDAO.getMailBodyForCancellation("114608", "1162", "testing cancel email", "D", "4375");
		} catch (Exception e) {
			ExceptionLogger.logExceptionToDB(e);
		}
	}

}
