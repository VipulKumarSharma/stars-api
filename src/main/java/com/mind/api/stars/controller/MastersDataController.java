package com.mind.api.stars.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mind.api.stars.common.EndpointURL;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="MastersDataController")
public interface MastersDataController {
	
	/************************************* FETCH CITY MASTER DATA ****************************************/
	
	@ApiOperation(value="Fetch cities list")
	@GetMapping(
		path = {EndpointURL.GET_CITY_MASTER_DATA}, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCityMasterData(
		@PathVariable String syncDate,
		HttpServletRequest request
	);
	
	/************************************* FETCH COUNTRY MASTER DATA ****************************************/
	
	@ApiOperation(value="Fetch countries list")
	@GetMapping(
		path = {EndpointURL.GET_COUNTRY_MASTER_DATA}, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCountryMasterData(
		@PathVariable String syncDate,
		HttpServletRequest request
	);
	
	/************************************* FETCH TIME MASTER DATA ****************************************/
	
	@ApiOperation(value="Fetch time master data")
	@GetMapping(
		path = EndpointURL.GET_TIME_MASTER_DATA, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getTimeMasterData(
		HttpServletRequest request
	);
	
	/***************************************************************************************************/
	
	
	/************************************* FETCH UNIT MASTER DATA ****************************************/
	
	@ApiOperation(value="Fetch unit master data")
	@GetMapping(
		path = EndpointURL.GET_UNIT_MASTER_DATA, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getUnitMasterData(HttpServletRequest request);
	
	/***************************************************************************************************/
	
	
	/************************************* FETCH CURRENCY MASTER DATA ****************************************/
	
	@ApiOperation(value="Fetch unit master data")
	@GetMapping(
		path = EndpointURL.GET_CURRENCY_MASTER_DATA, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getCurrencyMasterData(HttpServletRequest request);
	
	/***************************************************************************************************/
	
	
	/*************************************** ATTACHMENTS **********************************************/
	/* Upload Attachment */
	@ApiOperation(value="Upload Attachment")
	@PostMapping(
		value = EndpointURL.UPLOAD_ATTACHMENT, 
		consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String uploadAttachment(MultipartFile file, HttpServletRequest request);
	
	
	/* Get Attachment */
	@ApiOperation(value="Get Attachment")
	@GetMapping(
		value = EndpointURL.GET_ATTACHMENT, 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	String getAttachment(@PathVariable String travelId, @PathVariable String travelType, HttpServletRequest request);
	/**************************************************************************************************/
}
