package com.mind.api.stars.logic;

import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;

public interface AppLogic {

	UserInfoDTO getUserInfo(LoginDTO loginDTO);
	
	UserInfoDTO getAuthenticatedUserDetails(UserInfoDTO userInfoDTO);
	
	UserInfoDTO acceptPolicy(UserInfoDTO userInfoDTO);

	UserInfoDTO ssoLogin(String encWinUserId, String encDomain);
	
	void logout(int loggedInUserId, String ipAddress);
	
}
