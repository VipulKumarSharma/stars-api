package com.mind.api.stars.logic.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.mind.api.stars.dto.LoginDTO;
import com.mind.api.stars.dto.UserInfoDTO;
import com.mind.api.stars.logic.AppLogic;
import com.mind.api.stars.service.AppService;
import com.mind.api.stars.utility.AppUtility;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AppLogicImpl implements AppLogic {

	@Resource
	private AppService appService;
	
	@Override
	public UserInfoDTO getUserInfo(LoginDTO loginDTO) {
		return appService.getUserInfo(loginDTO);
	}

	@Override
	public UserInfoDTO getAuthenticatedUserDetails(UserInfoDTO userInfoDTO) {
		return appService.getAuthenticatedUserDetails(userInfoDTO);
	}
	
	@Override
	public UserInfoDTO acceptPolicy(UserInfoDTO userInfoDTO) {
		return appService.acceptPolicy(userInfoDTO);
	}

	@Override
	public UserInfoDTO ssoLogin(String encWinUserId, String encDomain) {
		String winUserId = "";
		String domain = "";
		
		try {
			winUserId = encWinUserId==null?"":AppUtility.decrypt(encWinUserId);
			domain = encDomain==null?"":AppUtility.decrypt(encDomain);
		
		} catch (Exception e) {
			winUserId = "";
			domain = "";
			log.error("Error occurred in AppLogicImpl : ssoLogin() : ", e);
		}
		
		return appService.ssoLogin(winUserId, domain);
	}

	@Override
	public void logout(int loggedInUserId, String ipAddress) {
		appService.logout(loggedInUserId, ipAddress);
	}

}
