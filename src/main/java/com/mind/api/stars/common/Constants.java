package com.mind.api.stars.common;

public class Constants {
	
	public static final String RESPONSE 				= "response";
    public static final String RESPONSE_STATUS 			= "status";
    public static final String RESPONSE_MESSAGE 		= "message";
    public static final String RESPONSE_DATA 			= "data";
    public static final String RESPONSE_XML				= "xmlstring";
    public static final String RESPONSE_OBJECT_WS		= "ResponseObject";
    public static final String RESPONSE_TOKEN_ID		= "tokenId";
    
    public static final String SUCCESS_CODE_200 		= "200";
    
    public static final String ERROR_CODE_403 			= "403";
    public static final String ERROR_CODE_500 			= "500";
    
    public static final String BLANK 					= "";
    public static final String SUCCESS 					= "SUCCESS";
    public static final String FAILURE 					= "FAILURE";
    public static final String REMARKS_IC				= "Remarks";
    
    public static final String KEY_SUCCESS 				= "success";
    public static final String KEY_EXPIRED 				= "expired";
    public static final String KEY_FAILURE 				= "failure";
    public static final String KEY_FAILED				= "Failed";
    
    public static final String INSERT 					= "INSERT";
    public static final String DELETE 					= "DELETE";
    public static final String CANCEL 					= "CANCEL";
    public static final String UPDATE 					= "UPDATE";
    
    public static final String NEW	 					= "NEW";
    public static final String STATUS_ID_ACTIVE			= "10";
    public static final String STATUS_ID_DEACTIVATED	= "30";
    public static final String STATUS_ID_DELETED 		= "50";
    
    public static final String API_SCANNED_PACKAGE 		= "com.mind.api.stars";
    public static final String API_WEBCLIENT_PACKAGE 	= "com.mind.wsclient";
    public static final String SWAGGER_SCANNED_PACKAGE 	= "com.mind.api.stars.controller";
    
    public static final String PROC_EXEC_SUCCESS_CODE 	= "0";
    public static final String STATUS 					= "status";
    
    public static final String APPROVER_ID 				= "APPROVER_ID";
    public static final String APPROVER_NAME 			= "APPROVER_NAME";
    public static final String LAST_APPROVER_ID 		= "LAST_APPROVER_ID";
    public static final String DESIGNATION 				= "DESIGNATION";
    public static final String WORKFLOW                 = "WORKFLOW";
    public static final String DEFAULT                  = "DEFAULT";
    public static final String SELECT_ALL_FROM          = "SELECT * FROM ";
    public static final String DBO                      = "DBO.";
    public static final String ERROR_OCCURRED           = "Some error occurred";
    public static final String USER_NOT_EXIST           = "User does not exist.";
    public static final String USERNAME					= "USERNAME";
    
    public static final String BUILDSTAMP 				= "buildstamp";
    public static final String BUILDSTAMP_DATE_FORMAT 	= "ddMMyyyy_HHmmss";
    public static final String DATE_FORMAT_IN			= "dd/MM/yyyy";
    public static final String DB_DATE_FORMAT			= "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_HIFEN		= "dd-MMM-yyyy";
    public static final String DB_DATE_FORMAT_WITHOUT_TIME = "yyyy-MM-dd";
    
    public static final String APP_NAME 				= "appName";
    public static final String MESSAGE 					= "message";
    public static final String ERROR_MESSAGE 			= "errorMsg";
    public static final String APP_LANGUAGE 			= "language";
    public static final String SSO 						= "SSO";
    public static final String CURRENCY					= "CURRENCY";
    public static final String USER                     = "user";
    public static final String PAGE                     = "page";
    public static final String ENC_PASSWORD             = "encPassword";
    public static final String LOGGED_IN_USER_ID        = "loggedInUserId";
    public static final String TRAVEL_ID       	 		= "travelId";
    public static final String TRAVEL_TYPE       	 	= "travelType";
    public static final String GROUP_TRAVEL_FLAG       	= "groupTravelFlag";
    
    public static final String NO_PREFERENCE        	= "No Preference";
    public static final String ARRIVAL_UNTIL        	= "Arrival Until";
    public static final String DEPARTURE_NOT_BEFORE     = "Departure not Before";
    public static final String TRANSIT_HOUSE        	= "Transit House";


    public static final String TRIDENT 					= "Trident";
    public static final String SAFARI 					= "Safari";
    public static final String CHROME 					= "Chrome";
    public static final String FIREFOX 					= "Firefox";
    public static final String USER_AGENT               = "User-Agent";
    
    public static final String ZERO 					= "0";
    public static final String FIRST 					= "1";
    public static final String SECOND 					= "2";
    public static final String THIRD 					= "3";
    
    public static final String INR 						= "INR";
    public static final String USD 						= "USD";
    public static final String NULL 					= null;
    public static final String LAST_SYNC_DATE 			= "lastSyncDate";
    
    public static final String EMPTY 					= "";
    public static final String Y 						= "Y";
    public static final String N 						= "N";
    
    public static final String APPROVAL_COMMENTS_A 		= "A";
    public static final String APPROVAL_COMMENTS_C 	 	= "C";
    
    public static final String FLIGHT					= "Flight";
    public static final String CAR						= "Car";
    public static final String TRAIN					= "Train";
    
    public static final String DOMESTIC_S_TRAVEL		="Domestic Travel";
    public static final String INTERNATIONAL_S_TRAVEL   ="International Travel";
    public static final String EMAIL 					="EMAIL";
    
    public static final String LABEL_MAIL_STARSNOTIFICATION = "label.mail.starsnotification";
    public static final String LABEL_MAIL_REQUISITIONNO		= "label.mail.requisitionno";
    public static final String USER_SUBMITTING_THE_REQ		= "User Submitting the Req";
	public static final String LABEL_GLOBAL_DEPARTURE_CITY 	= "label.global.departurecity";
	public static final String LABEL_GLOBAL_CLASS			= "label.global.class";
	public static final String LABEL_GLOBAL_ARRIVALCITY		= "label.global.arrivalcity";
	public static final String LABEL_GLOBAL_DEPARTUREDATE	= "label.global.departuredate";
	public static final String LABEL_GLOBAL_PREFERREDTIME	= "label.global.preferredtime";
    
	public static final String HTML_TD_O				= "<td>";
    public static final String HTML_TD_C				= "</td>";
    public static final String HTML_TR_O				= "<tr>";
    public static final String HTML_TR_C				= "</tr>";
    public static final String HTML_NBSP				= "&nbsp;";
    public static final String HTML_TH_C				= "</th>";
    public static final String HTML_TABLE_C				= "</table>";
    public static final String HTML_DIV_C				= "</div>";
    public static final String HTML_STRONG_C			= "</strong>";
    public static final String HTML_SPAN_C				= "</span>";
    public static final String HTML_BR_C				= "<br>";

    public static final String CANCEL_REQUEST_WS		= "CancelRequest";
    public static final String WEB_SERVICE_NOT_FOUND	= "Web Service not Found.";
    public static final String SEND_SITE_DETAILTO_ERP	= "sendSiteDetailToERP";
    public static final String EM_PRO					= "eMPro";
    public static final String CAN_CANCEL_REQUEST		= "canCancelRequest";
    public static final String SEND_STARS_REQDETAIL_TOERP = "sendStarsReqDetailToERP";
    public static final String ENCRYPTION_PUBLIC_KEY= "1d090efc-6b07-4f99-8323-eacdd8066f44";
    public static final String REQUEST_FROM				= "requestFrom";
    
    private Constants() {}
}
