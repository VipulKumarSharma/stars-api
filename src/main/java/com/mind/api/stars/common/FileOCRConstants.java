package com.mind.api.stars.common;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("file.ocr")
public class FileOCRConstants {

	private String documentUrl;
	private String getFileUrl; // get_file_url
	private String getResultUrl; // get_results_url
	private String webAppKey; // easy_web_app_key
	private String deviceType; // device_type	
	
}
