package com.mind.api.stars.common;

public class ControllerURL {
	
	public static final String REDIRECT 						= "redirect:";

	public static final String ROOT 							= "/";
	public static final String API_HOME 						= "/endpoints";
	
	public static final String LOGIN 							= "/login";
	public static final String ERROR 							= "/error";
	public static final String MAINTAINANCE 					= "/maintainance";
	public static final String ACCEPT_POLICY 					= "/acceptPolicy";
	public static final String HOME 							= "/home";
	public static final String SSO_LOGIN 						= "/sso";
	public static final String LOGOUT 							= "/appLogout";
	public static final String CREATE_REQUEST 					= "/request/create";
	public static final String EDIT_REQUEST 					= "/request/edit";
	public static final String APPROVE_REQUEST 					= "/request/approve";
    public static final String REQUEST_LIST                     = "/request/list";
    public static final String REQUEST_DETAILS                  = "/request/details";
	
	private ControllerURL() {}
}
