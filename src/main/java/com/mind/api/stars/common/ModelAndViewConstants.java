package com.mind.api.stars.common;

public class ModelAndViewConstants {
	
	public static final String LOGIN_LAYOUT 			= "login/login-template";
	public static final String DEFAULT_LAYOUT 			= "common/template";
	
	public static final String LOGIN 					= "login/login";
    public static final String ERROR					= "error/error";
    public static final String ERROR_PAGE 				= "error/errorpage";
    public static final String API_HOME 				= "links/api-home";
    public static final String SERVER_MAINTAINANCE 		= "links/serverMaintainance";
    
    public static final String APP_HOME 				= "dashboard/home";
    public static final String ACCEPT_POLICY 			= "links/acceptTermsAndConditions";
    public static final String CHANGE_PASSWORD 			= "links/changePassword";
    public static final String CREATE_REQUEST 			= "request/createRequest";
    public static final String APPROVE_REQUEST 			= "request/approveRequest";
    public static final String REQUEST_LIST             = "/request/requestList";
    public static final String REQUEST_DETAILS          = "/request/requestDetails";
    
    private ModelAndViewConstants() {}
    
}
