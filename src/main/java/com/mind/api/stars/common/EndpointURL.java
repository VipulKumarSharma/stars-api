package com.mind.api.stars.common;

public class EndpointURL {

	public static final String API 								= "/api";
	public static final String MASTER 							= "/api/master";
	public static final String INFO 							= "/api/info";
	
	public static final String TOKEN 							= "/token";
	
	public static final String SWAGGER_FILTER_URL				= "/api.*";
	
	public static final String HTTP_AUTH_REQUESTS_URL			= "**/rest/**";
	public static final String JWT_AUTH_TOKEN_FILTER_URL		= "/rest/**";
	
	public static final String TRAVEL_REQUEST 					= "/request";
	public static final String GET_REQUEST_DETAILS 				= "/request/{travelType}/{travelId}";
	public static final String GET_RECENT_TRAVEL_REQUESTS 		= "/request/recent/list";
	public static final String GET_USER_LAST_APPROVED_REQUEST	= "/request/approved/{last}/{source}/{destination}/list";
	public static final String GET_REQUESTS_COUNT				= "/request/counts/{travelType}/{last}";
	public static final String GET_REQUESTS_BY_TYPE				= "/request/{travelType}/{requestType}/{last}/list";
	public static final String GET_REQUEST_WORKFLOW				= "/request/workflow/{travelType}/{travelId}/list";
	public static final String GET_UPCOMING_JOURNEY_DETAILS		= "/journey/upcoming/{travelType}/{count}/list";
	public static final String GET_REQUEST_SETTING				= "/request/setting/{pageType}";
	
	public static final String GET_CITY_MASTER_DATA				= "/city/{syncDate}/list";
	public static final String GET_COUNTRY_MASTER_DATA			= "/country/list";
	public static final String GET_TIME_MASTER_DATA				= "/time/list";
	
	public static final String GET_CITY_BY_COUNTRY				= "/city/{countryId}";
	public static final String GET_AIRPORTS 					= "/airport/list";
	public static final String GET_HOTELS_INFO 					= "/hotel/list";
	public static final String GET_DAILY_ALLOWANCE_AMOUNT 		= "/amount/dailyAllowance";
	public static final String GET_PAST_BOOKINGS 				= "/bookings/past/list";
	public static final String GET_FUTURE_BOOKINGS 				= "/booking/future/list";
	
	public static final String GET_LEVEL_WISE_APPROVER			= "/request/approver/level/{level}/list";
	public static final String GET_DEFAULT_WORKFLOW_APPROVER	= "/request/approver/default/list";
	public static final String GET_APPROVER_LEVELS_LIST 		= "/request/approver/levels/list";
	
	public static final String GET_EXCHANGE_RATE				= "/request/exchange/rate/list";
	public static final String GET_FOREX_TOTAL_BY_CURRENCY		= "/request/forex/total";
	
	public static final String GET_UNIT_MASTER_DATA				= "/site/list";
	public static final String GET_USER_MASTER_DATA				= "/user/list";
	public static final String GET_CURRENCY_MASTER_DATA			= "/currency/list";
	
	public static final String GET_SITE_BASED_USERS				= "/request/site/{selectedSiteId}/user/list";
	public static final String GET_SEAT_LIST					= "/request/seat/list";
	public static final String GET_TRAVEL_CLASS_LIST			= "/request/travel/class/list";
	public static final String GET_TIME_MODE_LIST				= "/request/time/mode/list";
	public static final String GET_UNIT_LOCATION_LIST			= "/request/unit/location/list";
	public static final String GET_BILING_UNIT_LIST				= "/request/billing/unit/list";
	public static final String GET_BILING_APPROVER_LIST			= "/request/billing/approver/list";
	public static final String GET_MEAL_LIST					= "/request/meal/list";
	public static final String GET_COST_CENTRE_LIST				= "/request/costcentre/list";
	public static final String GET_CAR_CATEGORY_LIST			= "/request/carcategory/list";
	public static final String GET_TES_COUNT					= "/request/check/tescount";
	public static final String GET_TRAVEL_PREFERENCES 			= "/request/travel/preferences";
	public static final String CHECK_APPROVER_IN_DEFAULT_WF		= "/request/check/approver";
	public static final String GET_IDENTITY_PROOF_LIST			= "/request/identity/proof/list";
	public static final String GET_IDENTITY_PROOF_DETAILS		= "/request/identity/proof/detail";
	public static final String GET_PASSPORT_DETAILS				= "/request/passport/detail";
	public static final String GET_USER_DETAILS					= "/request/user/detail";
	public static final String GET_TRANSIT_HOUSE				= "/request/transit/house/list";
	public static final String GET_STAY_TYPE					= "/request/stay/type/list";
	public static final String TRAVEL_REQUEST_COMMENT			= "/request/comment";
	public static final String TRAVEL_REQUEST_CANCEL			= "/request/cancel";
	public static final String GET_MAILFLOW_LIST				= "/request/mailflow/list";
	public static final String GET_MAIL_DETAIL				    = "/request/mail/detail";
	public static final String GET_CAR_LOCATION_LIST			= "/request/car/location/list";
	public static final String TRAVEL_REQUEST_ATTACHMENT		= "/request/attachment";
	public static final String DELETE_REQ_JOURNEY_DETAIL		= "/request/journey/detail";
	public static final String GET_REQUEST_DETAILS_EDIT 		= "/request/edit/{travelType}/{travelId}";
	
	public static final String GET_GROUP_USER_DETAILS_LIST	     = "/request/group/user/list";
	public static final String GET_GROUP_USER_DETAILS     	     = "/request/group/user/detail";
	public static final String GET_GROUP_TRAVELLERS_DETAILS_LIST = "/request/group/travellers/list";
	public static final String GROUP_TRAVELLER_DETAIL            = "/request/group/traveller";
	public static final String UPLOAD_ATTACHMENT				 = "/attachment/upload";
	
	public static final String SAVE_FEEDBACK  	 		 	     = "/feedback";
	public static final String GET_LINK_TRAVEL_REQUEST 		 	 = "/request/linked/list";
	public static final String GET_TICKET_TYPE					 = "/request/ticketType/list";
	public static final String GET_COUNTRY_BY_ID     			 = "/country/{countryId}";
	public static final String GET_CITY_AIRPORT_LIST     		 = "/request/cityairport/list";
	public static final String GROUP_TRAVEL_REQUEST     		 = "/request/group";
	public static final String CHECK_OOO_APPROVER	     		 = "/request/check/ooo/approver";
	public static final String GET_ATTACHMENT					 = "/attachment/{travelId}/{travelType}";
	public static final String GET_VERSION     			 		 = "/request/version";
	
	private EndpointURL() {}
}
