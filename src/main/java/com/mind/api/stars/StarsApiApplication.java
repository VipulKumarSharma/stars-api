package com.mind.api.stars;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import com.mind.api.stars.common.Constants;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@ComponentScan({Constants.API_SCANNED_PACKAGE,Constants.API_WEBCLIENT_PACKAGE})
/*@Import({SpringFoxConfiguration.class})*/
@Slf4j
public class StarsApiApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Value("${spring.profiles.active}")
	String profile = "unknown";
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(StarsApiApplication.class);
    }
	
	public static void main(String[] commandLineArguments) {
		SpringApplication.run(StarsApiApplication.class, commandLineArguments);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("***************************************************************************");
		log.info("STARS API started successfully in '"+profile+"' environment.");
		log.info("***************************************************************************\n");
	}
	
}
