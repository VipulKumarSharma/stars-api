package com.mind.api.stars.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.DataDAO;
import com.mind.api.stars.enums.Function;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.model.Airport;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Booking;
import com.mind.api.stars.model.City;
import com.mind.api.stars.model.Country;
import com.mind.api.stars.model.DailyAllowance;
import com.mind.api.stars.model.Hotel;
import com.mind.api.stars.model.VersionDetail;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;

@Repository
public class DataDAOImpl extends BaseDAO implements DataDAO {
	
	@Resource
	private AppDbUtility appDbUtility;
	
	@Override
	public City getCityByCountry(AppRequest appRequest) {
		return new City();
	}
	
	
	@Override
	public List<Airport> getAirportsInfo(AppRequest appRequest) {
		return new ArrayList<>();
	}
	
	
	@Override
	public List<Hotel> getHotelsInfo(AppRequest appRequest) {
		return new ArrayList<>();
	}

	
	@Override
	public DailyAllowance getdailyAllowanceAmount(AppRequest appRequest) {
		return new DailyAllowance("93423", "YEN 345");
	}

	
	@Override
	public List<Booking> getPastBookings(AppRequest appRequest) {
		return new ArrayList<>();
	}

	
	@Override
	public List<Booking> getFutureBookings(AppRequest appRequest) {
		return new ArrayList<>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Country getCountryById(AppRequest appRequest) {
		
		Country countryInfo = new Country();
		
		if(!AppUtility.isNull(appRequest) &&  appRequest.getCountryId() > 0) {
			
			String sql = Constants.SELECT_ALL_FROM + Constants.DBO + Function.GET_COUNTRY_NAME_BY_ID.getName()+" (?)";
				
			countryInfo = jdbcTemplate.queryForObject(sql, new Object[] { appRequest.getCountryId() }, new CountryDataMapper());
		}
		
		return countryInfo;
	}
	
	private static final class CountryDataMapper implements RowMapper<Country> {
		
		@Override
		public Country mapRow(ResultSet resultSet, int arg1) throws SQLException {
			Country countryInfo = new Country();
			countryInfo.setCountryId(Integer.parseInt(AppUtility.checkNullString(resultSet.getString("COUNTRY_ID"))));
			countryInfo.setCountryCode(AppUtility.checkNullString(resultSet.getString("COUNTRY_CODE")));
			countryInfo.setCountryName(AppUtility.checkNullString(resultSet.getString("COUNTRY_NAME")));
			return countryInfo;
		}
	}

	@Override
	public VersionDetail getVersion(AppRequest appRequest) {
		String sql = "SELECT TOP 1 VER_REL_LOG_ID,ISNULL(RELEASE_VERSION,'') AS  RELEASE_VERSION FROM VERSION_RELEASE_LOG ORDER BY VER_REL_LOG_ID DESC";

		VersionDetail version = jdbcTemplate.queryForObject(sql, new VersionDetailMapper());  

		return version;
	}
	
	private static final class VersionDetailMapper implements RowMapper<VersionDetail> {
		@Override  
		public VersionDetail mapRow(ResultSet rs, int rownumber) throws SQLException {  
			VersionDetail version=new VersionDetail();  
			version.setVersion(rs.getString("RELEASE_VERSION")); 
			return version;  
		}
	}
	
}