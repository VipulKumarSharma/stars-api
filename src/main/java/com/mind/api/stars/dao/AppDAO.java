package com.mind.api.stars.dao;

import com.mind.api.stars.model.LoginBean;
import com.mind.api.stars.model.UserInfo;

public interface AppDAO {

	UserInfo getUserInfo(LoginBean loginBean);
	
	UserInfo getAuthenticatedUserDetails(UserInfo userInfo);
	
	UserInfo acceptPolicy(UserInfo userInfo);

	UserInfo ssoLogin(String winUserId, String domain);
	
	void logout(int loggedInUserId, String ipAddress);
	
}
