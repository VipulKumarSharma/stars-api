package com.mind.api.stars.dao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.EmailDAO;
import com.mind.api.stars.dao.RequestDAO;
import com.mind.api.stars.enums.Function;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.model.Advance;
import com.mind.api.stars.model.AdvanceDetailResponse;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.ApproverDetail;
import com.mind.api.stars.model.ApproverLevelWise;
import com.mind.api.stars.model.CarCategory;
import com.mind.api.stars.model.CarLocation;
import com.mind.api.stars.model.CheckApprover;
import com.mind.api.stars.model.CheckTESCount;
import com.mind.api.stars.model.CityAirport;
import com.mind.api.stars.model.Comment;
import com.mind.api.stars.model.CostCentre;
import com.mind.api.stars.model.DeleteJourneyDetailRequest;
import com.mind.api.stars.model.ExchangeRateRequest;
import com.mind.api.stars.model.ExchangeRateResponse;
import com.mind.api.stars.model.FeedbackRequest;
import com.mind.api.stars.model.GroupTravelRequest;
import com.mind.api.stars.model.GroupTraveller;
import com.mind.api.stars.model.GroupTravellerDetail;
import com.mind.api.stars.model.IdentityProof;
import com.mind.api.stars.model.IdentityProofDetails;
import com.mind.api.stars.model.Journey;
import com.mind.api.stars.model.LinkTravelRequest;
import com.mind.api.stars.model.MailDetail;
import com.mind.api.stars.model.MailFlow;
import com.mind.api.stars.model.Meal;
import com.mind.api.stars.model.OOOApprover;
import com.mind.api.stars.model.PassportDetail;
import com.mind.api.stars.model.PostComment;
import com.mind.api.stars.model.RequestData;
import com.mind.api.stars.model.RequestDetails;
import com.mind.api.stars.model.RequestSetting;
import com.mind.api.stars.model.RequestsCount;
import com.mind.api.stars.model.Seat;
import com.mind.api.stars.model.SeatRequest;
import com.mind.api.stars.model.SiteBasedUserRequest;
import com.mind.api.stars.model.StayType;
import com.mind.api.stars.model.TicketType;
import com.mind.api.stars.model.TimeMode;
import com.mind.api.stars.model.TransitHouse;
import com.mind.api.stars.model.TravelClass;
import com.mind.api.stars.model.TravelClassRequest;
import com.mind.api.stars.model.TravelRequest;
import com.mind.api.stars.model.Unit;
import com.mind.api.stars.model.UnitLocation;
import com.mind.api.stars.model.User;
import com.mind.api.stars.model.UserProfileInfo;
import com.mind.api.stars.model.WorkflowApprover;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.api.stars.utility.RequestConvertor;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class RequestDAOImpl extends AppDbUtility implements RequestDAO {

	@Resource @Lazy
	private EmailDAO emailDAO; 
	
	@Override
	public RequestData saveRequestData(TravelRequest travelRequest) {
		RequestData requestData = null;
		
		if(!AppUtility.isNull(travelRequest)) {
			
			if(travelRequest.getTravellerId() == 0) {
				int userId = getUserId(travelRequest.getWinUserId(), travelRequest.getDomainName());
				travelRequest.setTravellerId(userId);
			}
			
			if(travelRequest.getTravellerId() > 0) {
				String flightDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getFlightJourneyDetails());
				String trainDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getTrainJourneyDetails());
				String carDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getCarJourneyDetails());
				String accommodationDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getAccommodationDetails());
				String advanceDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getAdvanceForexDetails());
				String requestApproversXML = generateRequestApproversXML(travelRequest);
				String budgetActualDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getBudgetActualDetails());
				String totalTravelFareXML = RequestConvertor.convertObjectToXmlString(travelRequest.getTotalTravelFareDetails());
				String nonMATASourceDetailsXML = RequestConvertor.convertObjectToXmlString(travelRequest.getNonMATASourceDetails());
				
				try {
					SimpleJdbcCall jdbcCall	= new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.SAVE_TRAVEL_REQUEST.getName());
					 
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue("P_TRAVEL_ID", travelRequest.getTravelId())
					.addValue("P_WIN_USER_ID", travelRequest.getWinUserId())
					.addValue("P_DOMAIN_NAME", travelRequest.getDomainName())
					.addValue("P_UNIT_ID", travelRequest.getSiteId())
					.addValue(ParamConstant.USER_ID, travelRequest.getTravellerId())
					.addValue("P_REASON_FOR_TRAVEL", travelRequest.getReasonForTravel())
					.addValue("P_REASON_FOR_SKIP", travelRequest.getReasonForSkip())
					.addValue("P_UNIT_LOCATION", travelRequest.getSiteLocation())
					.addValue("P_GST_NO", travelRequest.getGstNo())
					.addValue("P_BILLING_CLIENT", travelRequest.getBillingClient())
					.addValue("P_BILLING_APPROVER", travelRequest.getBillingApprover())
					.addValue("P_REPORT_TO", travelRequest.getApproverLevel1())
					.addValue("P_DEPT_HEAD", travelRequest.getApproverLevel2())
					.addValue("P_BOARD_MEMBER", travelRequest.getApproverLevel3())
                    .addValue("P_APPROVER_XML", requestApproversXML)
					.addValue(ParamConstant.P_TRAVEL_TYPE, travelRequest.getTravelType())
					.addValue("P_BASE_CURRENCY", travelRequest.getBaseCurrency())
					.addValue("P_COST_CENTER_ID", travelRequest.getCostCentreId())
					.addValue("P_MEAL_ID", travelRequest.getPreferredMealId())
					.addValue("P_FLIGHT_XML", flightDetailsXML)
					.addValue("P_TRAIN_XML", trainDetailsXML)
					.addValue("P_CAR_XML", carDetailsXML)
					.addValue("P_ACCOMMODATION_XML",accommodationDetailsXML)
					.addValue("P_ADVANCE_XML", advanceDetailsXML)
					.addValue("P_BUDGET_ACTUAL_XML", budgetActualDetailsXML)
					.addValue("P_TOTAL_TRAVEL_FARE_XML", totalTravelFareXML)
					.addValue("P_LOCAL_AGENT_FLAG", travelRequest.getLocalAgentFlag())
					.addValue("P_NON_MATA_SOURCE_DETAILS_XML", nonMATASourceDetailsXML)
					.addValue("P_IP_ADDRESS", travelRequest.getDeviceId())
					.addValue("P_REQUEST_SOURCE", travelRequest.getRequestSource())
					.addValue("P_SUBMIT_FLAG", travelRequest.getSubmitFlag())
					.addValue("ERR_NO", 0)
					.addValue("REQUEST_NO", "")
					.addValue("REQ_TRAVEL_ID", "0")
					.addValue("RESPONSE_MSG", "");
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
		            String returnedCode = AppUtility.checkNullString(result.get("ERR_NO"));
		            
		            requestData = new RequestData();
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	requestData.setRequestNo(AppUtility.checkNullString(result.get("REQUEST_NO")));
		            	requestData.setTravelId(AppUtility.checkNullString(result.get("REQ_TRAVEL_ID")));
		            	requestData.setRequestStatus(AppUtility.checkNullString(result.get("RESPONSE_MSG")));
		            	
		            	if("Y".equalsIgnoreCase(AppUtility.checkNullString(travelRequest.getSubmitFlag()))) {
		            		try {
		            			String strIsTravellerUnitHead = null;
			            		if(!AppUtility.checkNull(travelRequest) && travelRequest.getSiteId() != 0) {
				            		strIsTravellerUnitHead = getIsTravellerUnitHead(travelRequest.getTravellerId(),travelRequest.getSiteId());
				            	}
				            	
			            		String roleId = getRoleIdByTravellerId(travelRequest.getTravellerId());
				            	
				            	String reqType = "I".equals(StringUtils.trimAllWhitespace(travelRequest.getTravelType())) ? Constants.INTERNATIONAL_S_TRAVEL : Constants.DOMESTIC_S_TRAVEL;
				            	emailDAO.sendReqsMailOnOriginating(requestData.getTravelId(), reqType, AppUtility.convertIntToString(travelRequest.getTravellerId()));
				            	 
				            	if(roleId!=null && roleId.equalsIgnoreCase("CHAIRMAN")){
				            		emailDAO.sendMailToChairman(requestData.getTravelId(), reqType, "", AppUtility.convertIntToString(travelRequest.getSiteId()), "HR&AC", "label.mail.intimationmailofchairman", AppUtility.convertIntToString(travelRequest.getTravellerId()));
				            	} else if("1".equals(strIsTravellerUnitHead)) {
				            		emailDAO.sendMailToChairman(requestData.getTravelId(), reqType, "", AppUtility.convertIntToString(travelRequest.getSiteId()), "HR&AC", "label.mail.intimationmailofunithead", AppUtility.convertIntToString(travelRequest.getTravellerId()));
				            	}
				            	
			            	} catch (Exception e) {
			            		log.error("Error occurred while sending emails in RequestDAOImpl : saveRequestData() : ", e);
								ExceptionLogger.logExceptionToDB(e);
			            	}
		            	}
		            
		            } else {
		            	requestData.setRequestStatus(getErrorMessage(returnedCode));
		            }
	            
				} catch(Exception e) {
					log.error("Error occurred in RequestDAOImpl : saveRequestData() : ", e);
					ExceptionLogger.logExceptionToDB(e);
				}
				
			}
		}
		
		return requestData;
	}

	
	@Override
	public Map<String, Object> getRequestDetails(AppRequest appRequest) {
		
		Map<String, Object> result = null;
		
		try {
			if(!AppUtility.isNull(appRequest)) {
				
				int travellerId = -1;
				int travelAgencyId = -1;
				
				if(!AppUtility.isNull(appRequest.getTravelType()) && !AppUtility.isNull(appRequest.getTravelId())) {
					travellerId = getRequestTravellerId(appRequest.getTravelType(), appRequest.getTravelId());
					travelAgencyId = getRequestTravelAgencyId(appRequest.getTravelType(), appRequest.getTravelId());
				}
				
				if(travellerId > 0 && travelAgencyId > -1) {
					int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.MATAINDIA_REQUEST_DETAILS.getName());
					
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue("P_TRAVEL_ID", appRequest.getTravelId())
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
					.addValue("P_TRAVELLER_ID", travellerId)
					.addValue("P_TRAVEL_AGENCY_ID", travelAgencyId)
					.addValue(ParamConstant.USER_ID, userId)
					.addValue(ParamConstant.OUTPUT_STATUS, 0);
					
					result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
					
					if(AppUtility.isNull(returnedCode) || !"0".equals(returnedCode)) {
						result = new HashMap<>();
					}
				}
			}
			
		} catch (Exception e) {
			result = new HashMap<>();
			
			log.error("Error occurred in RequestDAOImpl : getRequestDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} 
		
		return result;
	}
	
	
	@Override
	public List<Comment> getApproverComments(long travelId, String travelType, String commentType) {
		List<Comment> commentsList = null;
		
		try {
			commentsList = getApproversComment(travelId, travelType, commentType);
			
		} catch (Exception e) {
			commentsList = new ArrayList<>();
			
			log.error("Error occurred in RequestDAOImpl : getApproverComments() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} 
		
		return commentsList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RequestDetails> getRecentTravelRequestsData(AppRequest appRequest) {
		
		List<RequestDetails> requestsDetails = null;
		
		try {
			if(!AppUtility.isNull(appRequest)) {
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				
				if(userId > 0) {
					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.MATAINDIA_USER_RECENT_REQUESTS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new RequestDataMapper());

					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, userId);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					requestsDetails	= (List<RequestDetails>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getRecentTravelRequestsData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return requestsDetails;
	}
	
	private static final class RequestDataMapper implements RowMapper<RequestDetails> {

		@Override
		public RequestDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			RequestDetails bean = new RequestDetails();
			
			String travelId	= AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_ID));
			String requestNo = AppUtility.checkNullString(rs.getString("TRAVEL_REQ_NO"));
			String itineraryDetails = AppUtility.checkNullString(rs.getString("ITINERARY_DETAILS"));
			String journeyDates = AppUtility.checkNullString(rs.getString("JOURNEY_DATES"));
			String createdOn = AppUtility.checkNullString(rs.getString("ORIGINATED_ON"));
			String createdBy = AppUtility.checkNullString(rs.getString("ORIGINATED_BY"));
			String requestStatus = AppUtility.checkNullString(rs.getString("REQUEST_STATUS"));
			
			String travelType = AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_TYPE));
			String grpTravelFlag = AppUtility.checkNullString(rs.getString("GROUP_TRAVEL_FLAG"));
			String travelAgencyId = AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_AGENCY_ID));
			
			travelType = AppUtility.isNull(travelType) ? travelType : travelType.toUpperCase();
			
			bean.setTravelId(travelId);
			bean.setRequestNo(requestNo);
			bean.setItineraryDetails(itineraryDetails);
			bean.setJourneyDates(journeyDates);
			bean.setCreatedOn(createdOn);
			bean.setCreatedBy(createdBy);
			bean.setRequestStatus(requestStatus);
			bean.setTravelType(travelType);
			bean.setGroupTravelFlag(grpTravelFlag);
			bean.setTravelAgencyId(travelAgencyId);
		    
			return bean;
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getLastApprovedRequestDetailsList(AppRequest appRequest) {
		List<Map<String, Object>> requestDetailsList = new ArrayList<>();
		
		try {
			if(!AppUtility.isNull(appRequest)) {
				int travellerId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			
				if(travellerId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.LAST_APPROVED_REQUESTS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new UserLastApprovedRequestDataMapper());
		
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, travellerId)
					.addValue("P_SOURCE", appRequest.getSource())
					.addValue("P_DESTINATION", appRequest.getDestination())
					.addValue("OUTPUT_RECORDS_COUNT", appRequest.getLast())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	List<AppRequest> dataList = (List<AppRequest>) result.get(ParamConstant.RESULT_LIST);
		        		
		            	requestDetailsList = getRequestDetailsList(dataList);
		            }
				}
				
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getLastApprovedRequestDetailsList() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestDetailsList;
	}

	private static final class UserLastApprovedRequestDataMapper implements RowMapper<AppRequest> {

		@Override
		public AppRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			AppRequest bean = new AppRequest();
			
			long travelId = rs.getLong(ParamConstant.TRAVEL_ID);
			String travelType = AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_TYPE));
			int traverllerId = rs.getInt("TRAVELLER_ID");
			int travelAgencyId = rs.getInt(ParamConstant.TRAVEL_AGENCY_ID);
			
			travelType = AppUtility.isNull(travelType) ? travelType : travelType.toUpperCase();
			
			bean.setTravelId(travelId);
			bean.setTravelType(travelType);
			bean.setTravellerId(traverllerId);
			bean.setTravelAgencyId(travelAgencyId);
		    
			return bean;
		}
	}
	
	private List<Map<String, Object>> getRequestDetailsList(List<AppRequest> dataList) {
		List<Map<String, Object>> requestDetailsList = new ArrayList<>();
		
		if(AppUtility.containsData(dataList)) {
    		for (AppRequest requestData : dataList) {
        		if(!AppUtility.isNull(requestData)) {
        			Map<String, Object> data = getRequestDetails(requestData);
            		
            		data.put(ParamConstant.TRAVEL_ID, requestData.getTravelId());
	        		data.put(ParamConstant.TRAVEL_TYPE, requestData.getTravelType());
	        		
	        		requestDetailsList.add(data);
        		}
			}
		}
		
		return requestDetailsList;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public RequestsCount getRequestsCount(AppRequest appRequest) {
		RequestsCount requestCounts = null;
		
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelType())) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			
				if(userId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.ALL_REQUEST_COUNTS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new RequestCountsDataMapper());
		
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, userId)
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
					.addValue(ParamConstant.P_YEAR_COUNT, appRequest.getLast())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					Map<String, Object> result  = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	List<RequestsCount> dataList = (List<RequestsCount>) result.get(ParamConstant.RESULT_LIST);
		        		
		            	if(AppUtility.containsData(dataList)) {
		            		requestCounts = dataList.get(0);
		        		}
		            }
				}
				
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getRequestsCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestCounts;
	}
	
	private static final class RequestCountsDataMapper implements RowMapper<RequestsCount> {

		@Override
		public RequestsCount mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			RequestsCount bean 	= new RequestsCount();
			
			bean.setTemporaryRequests(rs.getInt("TEMP_REQ_COUNT"));
			bean.setRequestsInWorkflow(rs.getInt("REQ_IN_WF_COUNT"));
			bean.setReturnedRequests(rs.getInt("RET_REQ_COUNT"));
			bean.setRejectedRequests(rs.getInt("REJ_REQ_COUNT"));
			bean.setCancelledRequests(rs.getInt("CAN_REQ_COUNT"));
			bean.setApprovedRequests(rs.getInt("APP_REQ_COUNT"));
			bean.setRequestsToApprove(rs.getInt("REQ_TO_APP_COUNT"));
			bean.setYearlyRequestCreated(rs.getInt("REQ_CRE_IN_YEAR_COUNT"));
			bean.setYearlyRequestApproved(rs.getInt("REQ_APP_IN_YEAR_COUNT"));
			bean.setIsApprover(rs.getString("IS_APPROVER"));
			
			return bean;
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getRequestsByType(AppRequest appRequest) {
		Map<String, Object> result = null;
		
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelType()) && !AppUtility.isNull(appRequest.getRequestType())) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				
				if(userId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.REQUESTS_BY_TYPE.getName());

					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, userId)
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
					.addValue("P_REQ_STATUS", appRequest.getRequestType().toUpperCase())
					.addValue(ParamConstant.P_YEAR_COUNT, appRequest.getLast())
					.addValue(ParamConstant.TRAVEL_REQ_NO, appRequest.getRequestNo())
					.addValue(ParamConstant.P_REQ_COUNT, appRequest.getRequestCount())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					result 	= jdbcCall.execute(procParams);
				}
				
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getRequestsByType() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return result;
	}
	
	@Override
	public List<WorkflowApprover> getRequestWorkflow(AppRequest appRequest) {
		List<WorkflowApprover> requestWorkflow = null;
		
		if(appRequest.getTravelId() > 0 && !AppUtility.isNull(appRequest.getTravelType())) {
			requestWorkflow = getRequestWorkflow(appRequest.getTravelType(), appRequest.getTravelId());
		}
		
		return requestWorkflow;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Journey> getUpcomingJourneyDetails(AppRequest appRequest) {
		List<Journey> requestsDetails = null;
		
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelType()) && appRequest.getRequestCount() > 0) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				
				if(userId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.UPCOMING_JOURNEY_DETAILS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new JourneyDetailsMapper());

					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, userId)
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
					.addValue(ParamConstant.OUTPUT_RECORDS_COUNT, appRequest.getRequestCount())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	requestsDetails	= (List<Journey>) result.get(ParamConstant.RESULT_LIST);
		            }
				}
				
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getUpcomingJourneyDetails() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return requestsDetails;
	}
	
	private static final class JourneyDetailsMapper implements RowMapper<Journey> {

		@Override
		public Journey mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Journey bean = new Journey();
			
			bean.setTravelId(rs.getLong(ParamConstant.TRAVEL_ID));
			bean.setTravellerId(rs.getInt(ParamConstant.TRAVELLER_ID));
			bean.setTravelType(AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_TYPE)));
			bean.setRequestNo(AppUtility.checkNullString(rs.getString(ParamConstant.TRAVEL_REQ_NO)));
			bean.setSource(AppUtility.checkNullString(rs.getString("TRAVEL_FROM")));
			bean.setDestination(AppUtility.checkNullString(rs.getString("TRAVEL_TO")));
			bean.setDepartureDate(AppUtility.checkNullString(rs.getString("DEPARTURE_DATE")));
			bean.setReturnDate(AppUtility.checkNullString(rs.getString("RETURN_DATE")));
			bean.setDaysLeft(rs.getInt("DAYS_LEFT"));
			
			return bean;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public RequestSetting getRequestSetting(AppRequest appRequest) {
		RequestSetting requestSetting = null;
		
		try {
			if(!AppUtility.isNull(appRequest)) {
				int travellerId = 0;
				
				if(appRequest.getTravellerId() == 0) {
					travellerId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				} else {
					travellerId = appRequest.getTravellerId();
				}
			
				if(travellerId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.REQUEST_FLAGS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new RequestFlagsDataMapper());
		
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.P_TRAVELLER_ID, travellerId)
					.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
					.addValue(ParamConstant.P_BILLING_SITE_ID, appRequest.getBillingSiteId())
					.addValue("P_PAGE_TYPE", appRequest.getPageType())
					.addValue("P_TRAVEL_TYPE", appRequest.getTravelType())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	List<RequestSetting> dataList = (List<RequestSetting>) result.get(ParamConstant.RESULT_LIST);
		        		
		            	if(AppUtility.containsData(dataList)) {
		            		requestSetting = dataList.get(0);
		        		}
		            }
				}
				
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getRequestSetting() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return requestSetting;
	}
	
	private static final class RequestFlagsDataMapper implements RowMapper<RequestSetting> {

		@Override
		public RequestSetting mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			RequestSetting bean = new RequestSetting();
			
			bean.setCurrency(AppUtility.checkNullString(rs.getString("CURRENCY")));
			bean.setApprLvl3Flag(AppUtility.checkNullString(rs.getString("SHOW_APP_LVL_3")));
			bean.setSiteHeadFlag(AppUtility.checkNullString(rs.getString("UNIT_HEAD")));
			bean.setCostCentreFlag(AppUtility.checkNullString(rs.getString("COST_CENTER_FLAG")));
			bean.setApprLvl3ForBmFlag(AppUtility.checkNullString(rs.getString("SHOW_APP_LVL_3_FOR_BM")));
			bean.setMandatoryAppFlag(AppUtility.checkNullString(rs.getString("MANDATORY_APP_FLAG")));
			bean.setBudgetInputFlag(AppUtility.checkNullString(rs.getString("SHOW_BUD_INPUT")));
			bean.setDomLocalAgentFlag(AppUtility.checkNullString(rs.getString("DOM_LOCAL_AGENT")));
			bean.setIntLocalAgentFlag(AppUtility.checkNullString(rs.getString("INT_LOCAL_AGENT")));
			bean.setGstApplicableFlag(AppUtility.checkNullString(rs.getString("IS_GST_APPLICABLE")));
			bean.setApprLvlAutoSelFlag(AppUtility.checkNullString(rs.getString("APP_LVL_AUTO_SEL_FLAG")));
			bean.setBillingSiteFlag(AppUtility.checkNullString(rs.getString("BILLING_SITE_FLAG")));
			bean.setWorkflowNo(AppUtility.checkNullString(rs.getString("WORKFLOW_NO")));
			bean.setCancelledReqFlag(AppUtility.checkNullString(rs.getString("CANCELLED_REQ_FLAG")));
			bean.setDefaultWFExistsFlag(AppUtility.checkNullString(rs.getString("DEFAULT_WF_EXISTS")));
			
			return bean;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ApproverDetail> getFirstLevelApprovers(AppRequest appRequest) {

		List<ApproverDetail> firstLevelApprovers = null;
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getSiteId()) && !AppUtility.isNull(appRequest.getTravellerId())) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				if(appRequest.getTravellerId() == 0) {
					appRequest.setTravellerId(userId);
				}
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.FIRST_APPROVER_LEVEL.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());

				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, userId)
				.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.OUTPUT_STATUS, 0);

				Map<String, Object> result = jdbcCall.execute(funcParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					firstLevelApprovers	= (List<ApproverDetail>) result.get(ParamConstant.RESULT_LIST);
			    }
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getFirstLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}


		return firstLevelApprovers;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ApproverDetail> getSecondLevelApprovers(AppRequest appRequest) {
		
		List<ApproverDetail> secondLevelApprovers = null;
		
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getSiteId())) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				if(appRequest.getTravellerId() == 0) {
					appRequest.setTravellerId(userId);
				}
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.SECOND_APPROVER_LEVEL.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());

				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, userId)
				.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.OUTPUT_STATUS, 0);

				Map<String, Object> result = jdbcCall.execute(funcParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					secondLevelApprovers	= (List<ApproverDetail>) result.get(ParamConstant.RESULT_LIST);
			    }
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getSecondLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return secondLevelApprovers;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ApproverDetail> getThirdLevelApprovers(AppRequest appRequest) {

		List<ApproverDetail> thirdLevelApprovers = null;
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getSiteId())) {

				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				if(appRequest.getTravellerId() == 0) {
					appRequest.setTravellerId(userId);
				}
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.THIRD_APPROVER_LEVEL.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());

				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, userId)
				.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.OUTPUT_STATUS, 0);

				Map<String, Object> result = jdbcCall.execute(funcParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					thirdLevelApprovers	= (List<ApproverDetail>) result.get(ParamConstant.RESULT_LIST);
			    }
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getThirdLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}

		return thirdLevelApprovers;
	}
	
	
	private static final class ApproversDataMapper implements RowMapper<ApproverDetail> {

		@Override
		public ApproverDetail mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			ApproverDetail approverDetail = new ApproverDetail();
			approverDetail.setApproverId(AppUtility.checkNullString(resultSet.getString(ParamConstant.APPROVER_ID)));
			approverDetail.setApproverName(AppUtility.checkNullString(resultSet.getString(ParamConstant.APPROVER_NAME)));
			approverDetail.setLastApproverId(AppUtility.checkNullString(resultSet.getString(ParamConstant.LAST_APPROVER_ID)));
			return approverDetail;
		}
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<WorkflowApprover> getDefaultWorkflowApprover(AppRequest appRequest) {
		List<WorkflowApprover> defaultWorkflowApprover = null;
		
		try {
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getSiteId())) {

				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				if(appRequest.getTravellerId() == 0) {
					appRequest.setTravellerId(userId);
				}
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.DEFAULT_WORKFLOW_APPROVER.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new DefaultWorkflowApproverDataMapper());

				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.USER_ID, userId)	
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

				Map<String, Object> result = jdbcCall.execute(funcParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
				if(AppUtility.isSuccess(returnedCode)) {
					defaultWorkflowApprover	= (List<WorkflowApprover>) result.get(ParamConstant.RESULT_LIST);
			    }
			}
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : getThirdLevelApprovers() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		
		return defaultWorkflowApprover;
	}
	
	
	private static final class DefaultWorkflowApproverDataMapper implements RowMapper<WorkflowApprover> {

		@Override
		public WorkflowApprover mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			WorkflowApprover workflowApprover = new WorkflowApprover();
			workflowApprover.setName(AppUtility.checkNullString(resultSet.getString(ParamConstant.APPROVER_NAME)));
			workflowApprover.setDesignationName(AppUtility.checkNullString(resultSet.getString(ParamConstant.DESIGNATION)));
			return workflowApprover;
		}
		
	}


	@Override
	public ApproverLevelWise getApproverLevelWise(AppRequest appRequest) {
		
		ApproverLevelWise approverLevelWise = new ApproverLevelWise();
		try {
			List<ApproverDetail> firstLevelApprover = getFirstLevelApprovers(appRequest);
			List<ApproverDetail> secondLevelApprover = getSecondLevelApprovers(appRequest);
			List<ApproverDetail> thirdLevelApprover = getThirdLevelApprovers(appRequest);
			approverLevelWise.setApproverLevel1(firstLevelApprover);
			approverLevelWise.setApproverLevel2(secondLevelApprover);
			approverLevelWise.setApproverLevel3(thirdLevelApprover);
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getApproverLevelWise() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return approverLevelWise;
	}


	@Override
	public ExchangeRateResponse getExchangeRate(ExchangeRateRequest exchangeRateRequest) {
		ExchangeRateResponse exchangeRateResponse = null;
		try {
			
			if(!AppUtility.isNull(exchangeRateRequest) && !AppUtility.isNull(exchangeRateRequest.getSelectedCurrency())) {

				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName(Function.GET_EXCHANGE_RATE.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());
				
				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(Constants.CURRENCY, exchangeRateRequest.getSelectedCurrency())
				.addValue(ParamConstant.CURRENT_DATE, exchangeRateRequest.getCurrentDate());

				BigDecimal result = jdbcCall.executeFunction(BigDecimal.class, funcParams);

				if(!AppUtility.checkNull(result)) {
					exchangeRateResponse = new ExchangeRateResponse();
					exchangeRateResponse.setExchangeRate(result.toString());
			    }
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getExchangeRate() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return exchangeRateResponse;
	}


	@Override
	public AdvanceDetailResponse getForexTotal(ExchangeRateRequest exchangeRateRequest) {
		
		AdvanceDetailResponse advanceDetailResponse = null;
		try {
			
			if(!AppUtility.isNull(exchangeRateRequest) && !AppUtility.isNull(exchangeRateRequest.getSelectedCurrency())) {

				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName(Function.GET_FOREX_TOTAL.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());
				
				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.VALUE, exchangeRateRequest.getSelectedTotal())
				.addValue(ParamConstant.FROM_CURRENY, exchangeRateRequest.getSelectedCurrency())
				.addValue(ParamConstant.TO_CURRENCY, exchangeRateRequest.getBaseCurrency())
				.addValue(ParamConstant.CURRENT_DATE, exchangeRateRequest.getCurrentDate());

				BigDecimal totalAmountINR = jdbcCall.executeFunction(BigDecimal.class, funcParams);
				if(!AppUtility.checkNull(totalAmountINR)) {
					advanceDetailResponse = new AdvanceDetailResponse();
					totalAmountINR = totalAmountINR.setScale(2, BigDecimal.ROUND_DOWN);
					advanceDetailResponse.setTotalAmountINR(totalAmountINR.toString());
				}
				if(!AppUtility.checkNull(exchangeRateRequest.getPreviousTotal()) && AppUtility.isBlankString(exchangeRateRequest.getPreviousTotal())) {
					exchangeRateRequest.setPreviousTotal("0");
				}

				BigDecimal grandTotalAmountINR = totalAmountINR.add(new BigDecimal(exchangeRateRequest.getPreviousTotal()));
				grandTotalAmountINR = grandTotalAmountINR.setScale(2, BigDecimal.ROUND_DOWN);
				funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.VALUE, (String.valueOf(grandTotalAmountINR)))
				.addValue(ParamConstant.FROM_CURRENY, exchangeRateRequest.getBaseCurrency())
				.addValue(ParamConstant.TO_CURRENCY, Constants.USD)
				.addValue(ParamConstant.CURRENT_DATE, exchangeRateRequest.getCurrentDate());

				BigDecimal totalAmountUSD = jdbcCall.executeFunction(BigDecimal.class, funcParams);	
				totalAmountUSD = totalAmountUSD.setScale(2, BigDecimal.ROUND_DOWN);
				if(!AppUtility.checkNull(totalAmountUSD)) {
					BigDecimal tourDays = new BigDecimal(exchangeRateRequest.getDays());
					BigDecimal tempAmountInUSD = totalAmountUSD.divide(tourDays.intValue()==0?BigDecimal.valueOf(1):tourDays, 2, RoundingMode.HALF_UP);
					totalAmountUSD = totalAmountUSD.setScale(2, BigDecimal.ROUND_DOWN);
					advanceDetailResponse.setTotalAmountUSD(String.valueOf(tempAmountInUSD));
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getForexTotal() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return advanceDetailResponse;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<User> getSiteBasedUsers(SiteBasedUserRequest siteBasedUserRequest) {
		List<User> siteBasedUser = null;
		try {
			
			if(!AppUtility.isNull(siteBasedUserRequest) && !AppUtility.isNull(siteBasedUserRequest.getSelectedSiteId())) {
				
				int userId = getUserId(siteBasedUserRequest.getWinUserId(), siteBasedUserRequest.getDomainName());
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.USER_LIST_BY_SITE.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new UserDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_USER_ID, userId)
				.addValue(ParamConstant.P_SITE_ID, siteBasedUserRequest.getSelectedSiteId())
				.addValue(ParamConstant.LAST_SYNC_DATE, siteBasedUserRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, 0);

				Map<String, Object> result = jdbcCall.execute(procParams);
				siteBasedUser = (List<User>) result.get(ParamConstant.RESULT_LIST);
				
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getSiteBasedUsers() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return siteBasedUser;
	}
	
	private static final class UserDataMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet resultSet, int arg1) throws SQLException {
			User user = new User();
			
			user.setUserId(AppUtility.checkNullString(resultSet.getString(ParamConstant.USERID)));
			user.setUserName(AppUtility.checkNullString(resultSet.getString(ParamConstant.USERNAME)));
			user.setWinUserId(AppUtility.checkNullString(resultSet.getString("WIN_USER_ID")));
			user.setDomainName(AppUtility.checkNullString(resultSet.getString("DOMAIN_NAME")));
			
			return user;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Seat> getSeatList(SeatRequest seatRequest) {
		List<Seat> seatList = null;
		try {
			if(!AppUtility.isNull(seatRequest) && !AppUtility.isNull(seatRequest.getTravelAgencyId()) && !AppUtility.isNull(seatRequest.getModeId())) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.PREFFERED_SEAT_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new SeatDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_AGENCY_ID, seatRequest.getTravelAgencyId())
				.addValue(ParamConstant.P_MODE_ID, seatRequest.getModeId())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				seatList = (List<Seat>) result.get(ParamConstant.RESULT_LIST);
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getSeatList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return seatList;
	}
	
	private static final class SeatDataMapper implements RowMapper<Seat> {

		@Override
		public Seat mapRow(ResultSet resultSet, int arg1) throws SQLException {
			Seat seat = new Seat();
			seat.setSeatId(AppUtility.checkNullString(resultSet.getString(ParamConstant.SEAT_ID)));
			seat.setSeatName(AppUtility.checkNullString(resultSet.getString(ParamConstant.SEAT_NAME)));
			return seat;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TravelClass> getTravelClassList(TravelClassRequest travelClassRequest) {
		List<TravelClass> travelClassList = null;
		try {
			if(!AppUtility.isNull(travelClassRequest) && !AppUtility.isNull(travelClassRequest.getTravellerId()) && !AppUtility.isNull(travelClassRequest.getModeId())) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TRAVEL_CLASS_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new TravelClassDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
						.addValue(ParamConstant.P_SITE_ID, travelClassRequest.getSiteId())
				.addValue(ParamConstant.P_TRAVELLER_ID, travelClassRequest.getTravellerId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, travelClassRequest.getTravelType())
				.addValue(ParamConstant.P_MODE_ID, travelClassRequest.getModeId())
				.addValue(ParamConstant.P_GROUP_TRAVEL_FLAG, travelClassRequest.getGroupTravelFlag())
				.addValue(ParamConstant.LAST_SYNC_DATE, travelClassRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					travelClassList = (List<TravelClass>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getTravelClassList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return travelClassList;
	}
	
	private static final class TravelClassDataMapper implements RowMapper<TravelClass> {

		@Override
		public TravelClass mapRow(ResultSet resultSet, int arg1) throws SQLException {
			TravelClass travelClass = new TravelClass();
			travelClass.setClassId(AppUtility.checkNullString(resultSet.getString(ParamConstant.CLASS_ID)));
			travelClass.setEligibility(AppUtility.checkNullString(resultSet.getString(ParamConstant.ELIGIBILITY)));
			return travelClass;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeMode> getTimeModeList(AppRequest appRequest) {
		List<TimeMode>  timeModeList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TIME_MODE_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new TimeModeDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					timeModeList = (List<TimeMode>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getTimeModeList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return timeModeList;
	}
	
	private static final class TimeModeDataMapper implements RowMapper<TimeMode> {

		@Override
		public TimeMode mapRow(ResultSet resultSet, int arg1) throws SQLException {
			TimeMode timeMode = new TimeMode();
			timeMode.setTimeModeId(AppUtility.checkNullString(resultSet.getString(ParamConstant.TIME_MODE_ID)));
			timeMode.setTimeModeName(AppUtility.checkNullString(resultSet.getString(ParamConstant.TIME_MODE_NAME)));
			return timeMode;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<UnitLocation> getUnitLocationList(AppRequest appRequest) {
		List<UnitLocation>  unitLocationList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				int travellerId = 0;
				
				if(appRequest.getTravellerId() == 0) {
					travellerId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				} else {
					travellerId = appRequest.getTravellerId();
				}
				
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.UNIT_LOCATION_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new UnitLocationDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.P_TRAVELLER_ID, travellerId)
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					unitLocationList = (List<UnitLocation>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getUnitLocationList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return unitLocationList;
	}
	
	private static final class UnitLocationDataMapper implements RowMapper<UnitLocation> {

		@Override
		public UnitLocation mapRow(ResultSet resultSet, int arg1) throws SQLException {
			UnitLocation unitLocation = new UnitLocation();
			unitLocation.setLocationId(AppUtility.checkNullString(resultSet.getString(ParamConstant.LOCATION_ID)));
			unitLocation.setLocationName(AppUtility.checkNullString(resultSet.getString(ParamConstant.LOCATION_NAME)));
			unitLocation.setSiteId(AppUtility.checkNullString(resultSet.getString(ParamConstant.SITE_ID)));
			unitLocation.setGstNo(AppUtility.checkNullString(resultSet.getString(ParamConstant.GST_NO)));
			unitLocation.setLastLocationId(AppUtility.checkNullString(resultSet.getString(ParamConstant.LAST_LOCATION_ID)));
			return unitLocation;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Unit> getBillingUnitList(AppRequest appRequest) {
		List<Unit> billingUnitList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.BILLING_UNIT_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new BillingUnitDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					billingUnitList = (List<Unit>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getBillingUnitList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return billingUnitList;
	}
	
	private static final class BillingUnitDataMapper implements RowMapper<Unit> {

		@Override
		public Unit mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Unit bean = new Unit();
			bean.setSiteId(rs.getString("SITE_ID"));
			bean.setSiteName(rs.getString("SITE_NAME"));
		    
			return bean;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getBillingApproverList(AppRequest appRequest) {
		List<User> billingApproverList = null;
		try {
			
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getSiteId()) && !AppUtility.isNull(appRequest.getBillingSiteId())) {
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.BILLING_APPROVER_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new BillingApproverDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.P_BILLING_SITE_ID, appRequest.getBillingSiteId())
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				billingApproverList = (List<User>) result.get(ParamConstant.RESULT_LIST);
				
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getBillingApproverList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return billingApproverList;
	}
	
	private static final class BillingApproverDataMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet resultSet, int arg1) throws SQLException {
			User user = new User();
			user.setUserId(AppUtility.checkNullString(resultSet.getString(ParamConstant.USERID)));
			user.setUserName(AppUtility.checkNullString(resultSet.getString(ParamConstant.USERNAME)));
			return user;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Meal> getMealList(AppRequest appRequest) {
		List<Meal>  mealList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.MEAL_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new MealDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_AGENCY_ID, appRequest.getTravelAgencyId())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					mealList = (List<Meal>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getMealList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return mealList;
	}
	
	private static final class MealDataMapper implements RowMapper<Meal> {

		@Override
		public Meal mapRow(ResultSet resultSet, int arg1) throws SQLException {
			Meal meal = new Meal();
			meal.setMealId(AppUtility.checkNullString(resultSet.getString(ParamConstant.MEAL_ID)));
			meal.setMealName(AppUtility.checkNullString(resultSet.getString(ParamConstant.MEAL_NAME)));
			return meal;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CostCentre> getCostCentreList(AppRequest appRequest) {
		List<CostCentre>  costCentreList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.COST_CENTRE_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new CostCentreDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					costCentreList = (List<CostCentre>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getCostCentreList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return costCentreList;
	}
	
	private static final class CostCentreDataMapper implements RowMapper<CostCentre> {

		@Override
		public CostCentre mapRow(ResultSet resultSet, int arg1) throws SQLException {
			CostCentre costCentre = new CostCentre();
			costCentre.setCostCentreId(AppUtility.checkNullString(resultSet.getString(ParamConstant.COST_CENTRE_ID)));
			costCentre.setCostCentreName(AppUtility.checkNullString(resultSet.getString(ParamConstant.COST_CENTRE_NAME)));
			return costCentre;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CarCategory> getCarCategoryList(AppRequest appRequest) {
		List<CarCategory>  carCategoryList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.CAR_CATEGORY_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new CarCategoryDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_AGENCY_ID, appRequest.getTravelAgencyId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.P_CLASS_ID, appRequest.getCarClass())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					carCategoryList = (List<CarCategory>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getCarCategoryList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return carCategoryList;
	}
	
	private static final class CarCategoryDataMapper implements RowMapper<CarCategory> {

		@Override
		public CarCategory mapRow(ResultSet resultSet, int arg1) throws SQLException {
			CarCategory carCategory = new CarCategory();
			carCategory.setCarCategoryId(AppUtility.checkNullString(resultSet.getString(ParamConstant.CAR_CATEGORY_ID)));
			carCategory.setCarCategoryName(AppUtility.checkNullString(resultSet.getString(ParamConstant.CAR_CATEGORY_NAME)));
			return carCategory;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public CheckTESCount checkTESCount(AppRequest appRequest) {
		CheckTESCount checkTESCount = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				int travellerId = 0;
				
				if(appRequest.getTravellerId() == 0) {
					travellerId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				} else {
					travellerId = appRequest.getTravellerId();
				}
			
				if(travellerId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.CHECK_TES_COUNT.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new CheckTESCountDataMapper());
		
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.P_TRAVELLER_ID, travellerId)
					.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
					.addValue(ParamConstant.OUTPUT_STATUS, "");
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	List<CheckTESCount> checkTESCountDataList = (List<CheckTESCount>) result.get(ParamConstant.RESULT_LIST);
		        		
		            	if(AppUtility.containsData(checkTESCountDataList)) {
		            		checkTESCount = checkTESCountDataList.get(0);
		        		}
		            }
				}
			}
	
		} catch (Exception e) {
			log.error("Error occurred in RequestDAOImpl : checkTESCount() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
		
		return checkTESCount;
	}
	
	private static final class CheckTESCountDataMapper implements RowMapper<CheckTESCount> {

		@Override
		public CheckTESCount mapRow(ResultSet resultSet, int arg1) throws SQLException {
			CheckTESCount checkTESCount = new CheckTESCount();
			String tesFlag = "N#0#0";
			tesFlag = AppUtility.isNull(resultSet.getString("FLAG")) ? tesFlag : resultSet.getString("FLAG");
			
			String[] tesFlagArr = tesFlag.split("#");
			int countDiff = ((Integer.parseInt(tesFlagArr[2])) - (Integer.parseInt(tesFlagArr[1])));
			
			 if("Y".equals(tesFlagArr[0])) {
	        	if(Integer.parseInt(tesFlagArr[1]) != 0 && Integer.parseInt(tesFlagArr[1]) < Integer.parseInt(tesFlagArr[2])) {
	          	  	checkTESCount.setMessage("Please submit your last " + tesFlagArr[1] + " pending travel expense statement(s). After " + countDiff + " more travel request you will not be allowed to create further request in STARS.");
	    			checkTESCount.setCanCreateRequest(true);
		       	 } else {
		       		checkTESCount.setMessage("");
					checkTESCount.setCanCreateRequest(true);
		       	 }
	        	
	         } else if("N".equals(tesFlagArr[0])) {
	        	 if(Integer.parseInt(tesFlagArr[1]) >= Integer.parseInt(tesFlagArr[2]) && Integer.parseInt(tesFlagArr[2]) != 0) {
	          		checkTESCount.setMessage("Please submit your last " + tesFlagArr[1] + " pending travel expenses statement(s) to proceed further in STARS.");
		        	checkTESCount.setCanCreateRequest(false);
	 	       	 } else {
	 	       		checkTESCount.setMessage("");
		        	checkTESCount.setCanCreateRequest(true);
	 	       	 }
	        	 
	         } else if("A".equals(tesFlagArr[0])) {
	        	 checkTESCount.setMessage("");
	        	 checkTESCount.setCanCreateRequest(true);
	         } else {
	        	 checkTESCount.setMessage("");
	        	 checkTESCount.setCanCreateRequest(true);
	         }
			return checkTESCount;
		}
	}


	@Override
	public CheckApprover checkApproverExistsInDefaultWorkflow(AppRequest appRequest) {
		CheckApprover checkApprover = null;
		String isExistsflag = "N";
		
		try {
			
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelType())) {

				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName(Function.APPROVER_IS_IN_DEFAULT_WORKFLOW.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new ApproversDataMapper());
				
				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.SITE_ID, appRequest.getSiteId())
				.addValue(ParamConstant.TRAVELLER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.APPROVER_ID, appRequest.getApproverId())
				.addValue(ParamConstant.TRAVEL_TYPE, appRequest.getTravelType());

				isExistsflag = jdbcCall.executeFunction(String.class, funcParams);
				
				if(!AppUtility.checkNull(isExistsflag)) {
					checkApprover = new CheckApprover();
					checkApprover.setIsExists(isExistsflag);
			    }
				
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : checkApproverExistsInDefaultWorkflow() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return checkApprover;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<IdentityProof> getIdentityProofList(AppRequest appRequest) {
		List<IdentityProof> identityProof = new ArrayList<>();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.IDENTITY_PROOF_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new IdentityProofDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					identityProof = (List<IdentityProof>) result.get(ParamConstant.RESULT_LIST);
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getIdentityProofList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return identityProof;
	}
	
	private static final class IdentityProofDataMapper implements RowMapper<IdentityProof> {

		@Override
		public IdentityProof mapRow(ResultSet resultSet, int arg1) throws SQLException {
			IdentityProof identityProof = new IdentityProof();
			identityProof.setIdentityId(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_ID)));
			identityProof.setIdentityName(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_NAME)));
			return identityProof;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public PassportDetail getPassportDetails(AppRequest appRequest) {
		PassportDetail passportDetail = new PassportDetail();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.PASSPORT_DETAILS.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new PassportDetailDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.OUTPUT_STATUS, Constants.ZERO);

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					List<PassportDetail> passportDetailList = (List<PassportDetail>) result.get(ParamConstant.RESULT_LIST);
					if(AppUtility.containsData(passportDetailList)) {
						passportDetail = passportDetailList.get(0);
					}
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getPassportDetails() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return passportDetail;
	}

	
	private static final class PassportDetailDataMapper implements RowMapper<PassportDetail> {

		@Override
		public PassportDetail mapRow(ResultSet resultSet, int arg1) throws SQLException {
			PassportDetail passportDetail = new PassportDetail();
			passportDetail.setPassportNo(AppUtility.checkNullString(resultSet.getString(ParamConstant.PASSPORT_NO)));
			passportDetail.setNationality(AppUtility.checkNullString(resultSet.getString(ParamConstant.NATIONALITY)));
			passportDetail.setDateOfBirth(AppUtility.checkNullString(resultSet.getString(ParamConstant.DOB)));
			passportDetail.setExpiryDate(AppUtility.checkNullString(resultSet.getString(ParamConstant.EXPIRY_DATE)));
			passportDetail.setIssueDate(AppUtility.checkNullString(resultSet.getString(ParamConstant.DATE_ISSUE)));
			passportDetail.setIssuePlace(AppUtility.checkNullString(resultSet.getString(ParamConstant.PLACE_ISSUE)));
			passportDetail.setPpIssueCountryId(AppUtility.checkNullString(resultSet.getString(ParamConstant.PP_ISSUE_COUNTRY_ID)));
			return passportDetail;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public UserProfileInfo getUserDetails(AppRequest appRequest) {
		UserProfileInfo userProfileInfo = new UserProfileInfo();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.USER_DETAILS.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new UserProfileInfoMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.OUTPUT_STATUS, Constants.ZERO);

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					List<UserProfileInfo> userProfileInfoList = (List<UserProfileInfo>) result.get(ParamConstant.RESULT_LIST);
					if(AppUtility.containsData(userProfileInfoList)) {
						userProfileInfo = userProfileInfoList.get(0);
					}
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getUserDetails() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return userProfileInfo;
	}
	
	private static final class UserProfileInfoMapper implements RowMapper<UserProfileInfo> {

		@Override
		public UserProfileInfo mapRow(ResultSet resultSet, int arg1) throws SQLException {
			UserProfileInfo userProfileInfo = new UserProfileInfo();
			userProfileInfo.setFirstName(AppUtility.checkNullString(resultSet.getString(ParamConstant.FIRSTNAME)));
			userProfileInfo.setMiddleName(AppUtility.checkNullString(resultSet.getString(ParamConstant.MIDDLENAME)));
			userProfileInfo.setLastName(AppUtility.checkNullString(resultSet.getString(ParamConstant.LASTNAME)));
			userProfileInfo.setAddress(AppUtility.checkNullString(resultSet.getString(ParamConstant.ADDRESS)));
			userProfileInfo.setContactNumber(AppUtility.checkNullString(resultSet.getString(ParamConstant.CONTACT_NUMBER)));
			userProfileInfo.setCurrentAddress(AppUtility.checkNullString(resultSet.getString(ParamConstant.CURRENT_ADDRESS)));
			userProfileInfo.setDateOfBirth(AppUtility.checkNullString(resultSet.getString(ParamConstant.DOB)));
			userProfileInfo.setDepartment(AppUtility.checkNullString(resultSet.getString(ParamConstant.DEPT)));
			userProfileInfo.setDesignation(AppUtility.checkNullString(resultSet.getString(ParamConstant.DESIG)));
			userProfileInfo.setEcnr(AppUtility.checkNullString(resultSet.getString(ParamConstant.ECNR)));
			userProfileInfo.setEmail(AppUtility.checkNullString(resultSet.getString(ParamConstant.EMAIL)));
			userProfileInfo.setEmpCode(AppUtility.checkNullString(resultSet.getString(ParamConstant.EMP_CODE)));
			userProfileInfo.setFullName(AppUtility.checkNullString(resultSet.getString(ParamConstant.FULLNAME)));
			userProfileInfo.setGender(AppUtility.checkNullString(resultSet.getString(ParamConstant.GENDER)));
			userProfileInfo.setSalutation(AppUtility.checkNullString(resultSet.getString(ParamConstant.SALUTATION)));
			userProfileInfo.setWinUserId(AppUtility.checkNullString(resultSet.getString(ParamConstant.WIN_USER_ID)));
			userProfileInfo.setDomainName(AppUtility.checkNullString(resultSet.getString(ParamConstant.DOMAIN_NAME)));
			userProfileInfo.setIdentityProofType(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_NAME)));
			userProfileInfo.setIdentityProofNo(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_NUMBER)));
			userProfileInfo.setSiteName(AppUtility.checkNullString(resultSet.getString(ParamConstant.SITE_NAME)));
			
			userProfileInfo.setFrequentFlightName(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_AIR_NAME)));
			userProfileInfo.setFrequentFlightName1(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_AIR_NAME1)));
			userProfileInfo.setFrequentFlightName2(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_AIR_NAME2)));
			userProfileInfo.setFrequentFlightName3(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_AIR_NAME3)));
			userProfileInfo.setFrequentFlightName4(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_AIR_NAME4)));
			userProfileInfo.setFrequentFlightNumber(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_NUMBER)));
			userProfileInfo.setFrequentFlightNumber1(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_NUMBER1)));
			userProfileInfo.setFrequentFlightNumber2(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_NUMBER2)));
			userProfileInfo.setFrequentFlightNumber3(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_NUMBER3)));
			userProfileInfo.setFrequentFlightNumber4(AppUtility.checkNullString(resultSet.getString(ParamConstant.FF_NUMBER4)));
			userProfileInfo.setHotelName(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NAME)));
			userProfileInfo.setHotelName1(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NAME1)));
			userProfileInfo.setHotelName2(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NAME2)));
			userProfileInfo.setHotelName3(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NAME3)));
			userProfileInfo.setHotelName4(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NAME4)));
			userProfileInfo.setHotelNumber(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NUMBER)));
			userProfileInfo.setHotelNumber1(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NUMBER1)));
			userProfileInfo.setHotelNumber2(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NUMBER2)));
			userProfileInfo.setHotelNumber3(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NUMBER3)));
			userProfileInfo.setHotelNumber4(AppUtility.checkNullString(resultSet.getString(ParamConstant.HOTEL_NUMBER4)));
			
			return userProfileInfo;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public IdentityProofDetails getIdentityProofDetails(AppRequest appRequest) {
		IdentityProofDetails identityProofDetail = new IdentityProofDetails();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.IDENTITY_PROOF_DETAILS.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new IdentityProofDetailsMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, appRequest.getTravellerId())
				.addValue(ParamConstant.P_IDENTITY_ID, appRequest.getIdentityId())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					List<IdentityProofDetails> identityProofDetailList = (List<IdentityProofDetails>) result.get(ParamConstant.RESULT_LIST);
					if(AppUtility.containsData(identityProofDetailList)) {
						identityProofDetail = identityProofDetailList.get(0);
					}
					
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getIdentityProofList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return identityProofDetail;
	}
	
	private static final class IdentityProofDetailsMapper implements RowMapper<IdentityProofDetails> {

		@Override
		public IdentityProofDetails mapRow(ResultSet resultSet, int arg1) throws SQLException {
			IdentityProofDetails identityProofDetail = new IdentityProofDetails();
			identityProofDetail.setIdentityId(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_ID)));
			identityProofDetail.setIdentityNumber(AppUtility.checkNullString(resultSet.getString(ParamConstant.IDENTITY_NUMBER)));
			return identityProofDetail;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<TransitHouse> getTransitHouseList(AppRequest appRequest) {
		List<TransitHouse> transitHouse = new ArrayList<>();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TRANSIT_HOUSE_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new TransitHouseDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					transitHouse = (List<TransitHouse>) result.get(ParamConstant.RESULT_LIST);
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getTransitHouseList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return transitHouse;
	}
	
	private static final class TransitHouseDataMapper implements RowMapper<TransitHouse> {

		@Override
		public TransitHouse mapRow(ResultSet resultSet, int arg1) throws SQLException {
			TransitHouse transitHouse = new TransitHouse();
			
			transitHouse.setTransitHouseId(AppUtility.checkNullString(resultSet.getString(ParamConstant.TRANSIT_HOUSE_ID)));
			transitHouse.setTransitHouseName(AppUtility.checkNullString(resultSet.getString(ParamConstant.TRANSIT_HOUSE_NAME)));
			transitHouse.setTransitHouseAddress(AppUtility.checkNullString(resultSet.getString(ParamConstant.TRANSIT_HOUSE_ADDRESS)));
			transitHouse.setAreaCode(AppUtility.checkNullString(resultSet.getString(ParamConstant.AREA_CODE)));
			transitHouse.setLatitude(AppUtility.checkNullString(resultSet.getString("LATITUDE")));
			transitHouse.setLongitude(AppUtility.checkNullString(resultSet.getString("LONGITUDE")));
			
			return transitHouse;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<StayType> getStayTypeList(AppRequest appRequest) {
		List<StayType> stayType = new ArrayList<>();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.STAY_TYPE_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new StayTypeDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));

				if(AppUtility.isSuccess(returnedCode)) {
					stayType = (List<StayType>) result.get(ParamConstant.RESULT_LIST);
				}
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getStayTypeList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return stayType;
	}
	
	private static final class StayTypeDataMapper implements RowMapper<StayType> {

		@Override
		public StayType mapRow(ResultSet resultSet, int arg1) throws SQLException {
			StayType stayType = new StayType();
			stayType.setStayTypeId(AppUtility.checkNullString(resultSet.getString(ParamConstant.STAY_TYPE_ID)));
			stayType.setStayTypeName(AppUtility.checkNullString(resultSet.getString(ParamConstant.STAY_TYPE_NAME)));
			return stayType;
		}
	}
	
	@Override
	public Map<String, Object> getRequestCommentsDetails(AppRequest appRequest) throws Exception {
		Map<String, Object> result  = null;
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType()) && !AppUtility.isNull(appRequest.getPageType()) ) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				
				if(userId > 0) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.REQUESTS_COMMENT_DETAILS.getName());

					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.USER_ID, userId)
					.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
					.addValue(ParamConstant.PAGE_TYPE, appRequest.getPageType().toUpperCase())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					result 	= 	jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
					if(AppUtility.isNull(returnedCode) || !"0".equals(returnedCode)) {
						result = new HashMap<>();
					}
				}
				
			}
	    
		return result;
	}
	
	@Override
	public String  deleteRequestComment(AppRequest appRequest) throws Exception  {
		String errorMessage = "";
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getCommentId()) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType())) {
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TRAVEL_REQ_COMMENT_DELETE.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_COMMENT_ID, appRequest.getCommentId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
				.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result 	= 	jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
				
				if(!AppUtility.isSuccess(returnedCode)) {
					errorMessage = getErrorMessage(returnedCode);
				}
			}
		return errorMessage;
	}
	
	@Override
	public String saveRequestComment(PostComment postComment) throws Exception {
		String errorMessage = "";
		Map<String, Object> result = null;
		int returnStatusCode;
		
		if(!AppUtility.isNull(postComment) && !AppUtility.isNull(postComment.getTravelId()) && !AppUtility.isNull(postComment.getCommentDesc()) && !AppUtility.isNull(postComment.getTravelType())) {
				
			int userId = getUserId(postComment.getWinUserId(), postComment.getDomainName());
			
			if(userId > 0) {
						SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(Procedure.POST_REQUEST_COMMENT.getName());
	
						SqlParameterSource procParams = new MapSqlParameterSource()
						.addValue(ParamConstant.TRAVEL_ID, postComment.getTravelId())
						.addValue(ParamConstant.COMMENTS, postComment.getCommentDesc())
						.addValue(ParamConstant.C_USER_ID, userId)
						.addValue(ParamConstant.TRAVEL_TYPE, postComment.getTravelType().toUpperCase());
						
						result = jdbcCall.execute(procParams);
			            
						returnStatusCode = (int) result.get("#update-count-1");
						
						if(returnStatusCode <= 0 ) {
							errorMessage = Constants.ERROR_OCCURRED;
						}
					
				} else {
					errorMessage = Constants.USER_NOT_EXIST;
				}
		}
		return errorMessage;
	}
	
	@Override
	public String cancelTravelRequest(PostComment postComment) throws Exception {
		String errorMessage = "";
		Map<String, Object> result = null;
		int returnStatusCode;
		
		if(!AppUtility.isNull(postComment) && !AppUtility.isNull(postComment.getTravelId())  && !AppUtility.isNull(postComment.getCommentDesc()) && !AppUtility.isNull(postComment.getTravelType())) {
				
			int userId 		= getUserId(postComment.getWinUserId(), postComment.getDomainName());
			String userRole = getUserRole( Integer.toString(userId));
			
				if(userRole!=null  && userRole.equals("MATA")) {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.POST_REQUEST_CANCEL_COMMENT_MATA.getName());
	
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.TRAVEL_ID, postComment.getTravelId())
					.addValue(ParamConstant.COMMENTS, postComment.getCommentDesc())
					.addValue(ParamConstant.C_USER_ID, userId)
					.addValue(ParamConstant.TRAVEL_TYPE, postComment.getTravelType().toUpperCase());
					
					result = jdbcCall.execute(procParams);
			     } else {
	    	        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
	   				.withProcedureName(Procedure.POST_REQUEST_CANCEL_COMMENT.getName());
	
	   				SqlParameterSource procParams = new MapSqlParameterSource()
	   				.addValue(ParamConstant.TRAVEL_ID, postComment.getTravelId())
	   				.addValue(ParamConstant.COMMENTS, postComment.getCommentDesc())
	   				.addValue(ParamConstant.C_USER_ID, userId)
	   				.addValue(ParamConstant.TRAVEL_TYPE, postComment.getTravelType().toUpperCase());
	   				
	   				result = jdbcCall.execute(procParams);
			     }
				returnStatusCode = (int) result.get("#update-count-1");
				
				if(returnStatusCode <= 0 ) {
					errorMessage = Constants.ERROR_OCCURRED;
				}
				else if(returnStatusCode == 1 ) {
					emailDAO.sendCancellationEmail(postComment.getTravelType().toUpperCase(), postComment.getTravelId(), postComment.getTravelId(), postComment.getCommentDesc(), "", AppUtility.convertIntToString(userId), userRole);
				}
		}
		return errorMessage;
	}
	
	@Override
	public boolean deleteRequestData(AppRequest appRequest) {
		
		if (!AppUtility.isNull(appRequest)) {
			int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			if (userId > 0) {
				try {
					if (validateAuthCancelTravelReq(String.valueOf(appRequest.getTravelId()),
							appRequest.getTravelType(), String.valueOf(userId))) {

						SimpleJdbcCall jdbcDeleteTravelRequest = new SimpleJdbcCall(jdbcTemplate)
								.withProcedureName(Procedure.PROC_DELETE_TRAVEL_REQUISITION.getName());

						SqlParameterSource procDelRequestParams = new MapSqlParameterSource()
								.addValue(ParamConstant.TRAVEL_ID, appRequest.getTravelId())
								.addValue(ParamConstant.TRAVEL_REQ_ID, appRequest.getTravelReqId())
								.addValue(ParamConstant.TRAVEL_TYPE, appRequest.getTravelType());
						jdbcDeleteTravelRequest.execute(procDelRequestParams);

						SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
								.withProcedureName(Procedure.PROC_T_GROUP_USERINFO.getName());

						SqlParameterSource procParams = new MapSqlParameterSource()
								.addValue("P_G_USERID", "-1")
								.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
								.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
								.addValue(ParamConstant.P_FIRST_NAME, "")
								.addValue(ParamConstant.P_LAST_NAME, "")
								.addValue(ParamConstant.P_SITE_ID, "0")
								.addValue(ParamConstant.P_DESIG_ID, "0")
								.addValue(ParamConstant.OUTPUT_STATUS_ID, "50")
								.addValue("P_PASSPORT_NO", "")
								.addValue("P_DATE_OF_ISSUE", "")
								.addValue("P_PLACE_OF_ISSUE", "")
								.addValue("P_EXPIRY_DATE", "")
								.addValue("P_AGE", "")
								.addValue("P_ECNR", "")
								.addValue("P_DOB", "")
								.addValue("P_VISA_REQUIRED", "")
								.addValue("P_RECORD_TYPE", "")
								.addValue(ParamConstant.P_GENDER, "1")
								.addValue("P_IDENTITY_ID", "0")
								.addValue("P_IDENTITY_NO", "")
								.addValue("P_MAEL_ID", "0")
								.addValue("P_TOTAL_AMOUNT", "0")
								.addValue("P_EXP_REMARKS", "")
								.addValue(ParamConstant.P_USER_ID, userId)
								.addValue("P_ACTION", "DELETE")
								.addValue(ParamConstant.P_MOBILE_NO, "")
								.addValue(ParamConstant.P_NATIONALITY, "")
								.addValue(ParamConstant.P_RETURN_TRAVEL, "")
								.addValue(ParamConstant.P_FREQUENT_FLYER, "")
								.addValue(ParamConstant.P_G_EMP_CODE, "")
								.addValue("P_G_EMAIL", "")
								.addValue(ParamConstant.OUTPUT_STATUS, 0)
								.addValue("P_REQUEST_TYPE", 0);

						jdbcCall.execute(procParams);
						return true;

					}
					else {
						return false;
					}

				} catch (Exception ex) {
					return false;
				}
			}
			
		}

		return false;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MailFlow> getMailFlowList(AppRequest appRequest) {
		List<MailFlow> mailFlow = new ArrayList<>();
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.GET_TRAVEL_REQ_MAILS_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new MailFlowDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.TRAVEL_REQ_NO, appRequest.getRequestNo())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

				Map<String, Object> result  = jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
	            if(AppUtility.isSuccess(returnedCode)) {
	            	mailFlow = (List<MailFlow>) result.get(ParamConstant.RESULT_LIST);
	            }
			} 
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getMailFlowList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return mailFlow;
	}
	
	private static final class MailFlowDataMapper implements RowMapper<MailFlow> {

		@Override
		public MailFlow mapRow(ResultSet resultSet, int arg1) throws SQLException {
			MailFlow mailFlow = new MailFlow();
			mailFlow.setMailId(AppUtility.checkNullString(resultSet.getString("MAIL_ID")));
			mailFlow.setMailFrom(AppUtility.checkNullString(resultSet.getString("RECEIPENT_FROM")));
			mailFlow.setMailTo(AppUtility.checkNullString(resultSet.getString("RECEIPENT_TO")));
			mailFlow.setMailCC(AppUtility.checkNullString(resultSet.getString("RECEIPMENT_CC")));
			mailFlow.setMailSubject(AppUtility.checkNullString(resultSet.getString("MAIL_SUBJECT")));
			mailFlow.setMailCreator(AppUtility.checkNullString(resultSet.getString("MAIL_CREATOR")));
			mailFlow.setMailStatus(AppUtility.checkNullString(resultSet.getString("ERROR_SUCCESS")));
			mailFlow.setMailCreatedDate(AppUtility.checkNullString(resultSet.getString("MAIL_CREATED_DATE")));
			mailFlow.setMailSendDate(AppUtility.checkNullString(resultSet.getString("MAIL_SEND_DATE")));
			return mailFlow;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public MailDetail getMailDetail(AppRequest appRequest) {
		MailDetail mailDetail = null;
		
		try {
			
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getMailId())) {

				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withFunctionName(Procedure.GET_TRAVEL_REQ_MAIL_DETAIL.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new MailDetailDataMapper());
				
				SqlParameterSource funcParams = new MapSqlParameterSource()
				.addValue(ParamConstant.MAIL_ID, appRequest.getMailId())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result  = jdbcCall.execute(funcParams);

				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
	            if(AppUtility.isSuccess(returnedCode)) {				
	            	List<MailDetail> mailDetailList = (List<MailDetail>) result.get(ParamConstant.RESULT_LIST);
	            	if(AppUtility.containsData(mailDetailList)) {
	            		mailDetail = mailDetailList.get(0);
	        		}
	            }
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getMailDetail() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		
		return mailDetail;
	}
	
	private static final class MailDetailDataMapper implements RowMapper<MailDetail> {

		@Override
		public MailDetail mapRow(ResultSet resultSet, int arg1) throws SQLException {
			MailDetail mailDetail = new MailDetail();
			mailDetail.setMailBody(AppUtility.checkNullString(resultSet.getString("MAIL_MSG")));
			return mailDetail;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CarLocation> getCarLocationList(AppRequest appRequest) {
		List<CarLocation>  carLocationList = null;
		try {
			if(!AppUtility.isNull(appRequest)) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.GET_CAR_LOCATION_LIST.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new CarLocationDataMapper());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, "");

				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				if(AppUtility.isSuccess(returnedCode)) {
					carLocationList = (List<CarLocation>) result.get(ParamConstant.RESULT_LIST);
				}
			}
			
		} catch(Exception ex) {
			log.error("Error occurred in RequestDAOImpl : getCarLocationList() : ", ex);
			ExceptionLogger.logExceptionToDB(ex);
		}
		return carLocationList;
	}
	

	private static final class CarLocationDataMapper implements RowMapper<CarLocation> {
	
		@Override
		public CarLocation mapRow(ResultSet resultSet, int arg1) throws SQLException {
			CarLocation carLocation = new CarLocation();
			carLocation.setCarLocationId(AppUtility.checkNullString(resultSet.getString("CAR_LOCATION_ID")));
			carLocation.setCarLocationName(AppUtility.checkNullString(resultSet.getString("CAR_LOCATION_NAME")));
			return carLocation;
		}
	}
	
	@Override
	public Map<String, Object> getRequestAttachmentDetails(AppRequest appRequest) throws Exception {
		Map<String, Object> result  = null;
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType()) ) {
			
			int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			
			if(userId > 0) {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.REQUESTS_ATTACHMENT_DETAILS.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType().toUpperCase())
				.addValue(ParamConstant.USER_ID, userId)
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				result 	= 	jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
				if(AppUtility.isNull(returnedCode) || !"0".equals(returnedCode)) {
					result = new HashMap<>();
				}
			}
		}
	return result;
	}
	
	@Override
	public String  deleteRequestAttachment(AppRequest appRequest) throws Exception  {
		String errorMessage = "";
		Map<String, Object> result = null;
		int returnStatusCode;
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getAttachmentId())) {
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TRAVEL_REQ_ATTACHMENT_DELETE.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.ATTACHMENT_ID, appRequest.getAttachmentId());
				
				result = jdbcCall.execute(procParams);
	            
				returnStatusCode = (int) result.get("#update-count-1");
				
				if(returnStatusCode <= 0 ) {
					errorMessage = Constants.ERROR_OCCURRED;
				}
			}
		return errorMessage;
	}
	
	@Override
	public String deleteRequestJourneyDetail(DeleteJourneyDetailRequest deleteJourneyDetailRequest) throws Exception  {
		String errorMessage = "";
		if (!AppUtility.isNull(deleteJourneyDetailRequest)) {
			int userId = getUserId(deleteJourneyDetailRequest.getWinUserId(), deleteJourneyDetailRequest.getDomainName());
			if (userId > 0) {
				SimpleJdbcCall jdbcDeleteJourneyDetail = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(Procedure.DELETE_TRAVEL_JOURNEY_DETAILS.getName());

				SqlParameterSource procDeleteJourneyDetailParams = new MapSqlParameterSource()
						.addValue(ParamConstant.P_MODE_ID, deleteJourneyDetailRequest.getModeId())
						.addValue(ParamConstant.P_TRAVEL_TYPE, deleteJourneyDetailRequest.getTravelType())
						.addValue(ParamConstant.P_TRAVEL_ID, deleteJourneyDetailRequest.getTravelId())
						.addValue(ParamConstant.P_USER_ID, userId)
						.addValue(ParamConstant.P_TRAVEL_CAR_ID, deleteJourneyDetailRequest.getTravelCarIds())
						.addValue(ParamConstant.P_TRAVEL_ACC_ID, deleteJourneyDetailRequest.getTravelAccommIds())
						.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result = jdbcDeleteJourneyDetail.execute(procDeleteJourneyDetailParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
				if(!AppUtility.isSuccess(returnedCode)) {
					errorMessage = getErrorMessage(returnedCode);
				}
			} else {
				errorMessage = Constants.USER_NOT_EXIST;
			}
		}
		return errorMessage;
	}
	
	public Map<String, Object> getRequestDetailForEdit(AppRequest appRequest){
		Map<String, Object> result = null;
		
		try {
			if(!AppUtility.isNull(appRequest)) {
				
				int travellerId = -1;
				
				if(!AppUtility.isNull(appRequest.getTravelType()) && !AppUtility.isNull(appRequest.getTravelId())) {
					travellerId = getRequestTravellerId(appRequest.getTravelType(), appRequest.getTravelId());
				}
				
				if(travellerId > 0 ) {
					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.PROC_GET_REQUEST_DETAILS.getName());
					
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
					.addValue(ParamConstant.P_USER_ID, travellerId)
					.addValue(ParamConstant.P_IP_ADDRESS, appRequest.getDeviceId())
					.addValue(ParamConstant.ERR_NO, 0);
					
					result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.ERR_NO));
					
					if(AppUtility.isNull(returnedCode) || !"0".equals(returnedCode)) {
						result = new HashMap<>();
					}
				}
			}
			
		} catch (Exception e) {
			result = new HashMap<>();
			
			log.error("Error occurred in RequestDAOImpl : getRequestDetailForEdit() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		} 
		
		return result;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GroupTraveller> getGroupUserDetailsList(AppRequest appRequest)  throws Exception {
		List<GroupTraveller>  groupTravellerList = null;
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravellerType()) && !AppUtility.isNull(appRequest.getTravelType()) && !AppUtility.isNull(appRequest.getSiteId()) && !AppUtility.isNull(appRequest.getFirstName())) {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_GROUP_USER_DETAILS_LIST.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new GroupTravellerListDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_TRAVELLER_TYPE, appRequest.getTravellerType())
			.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
			.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
			.addValue(ParamConstant.P_FIRST_NAME, appRequest.getFirstName())
			.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			if(AppUtility.isSuccess(returnedCode)) {
				groupTravellerList = (List<GroupTraveller>) result.get(ParamConstant.RESULT_LIST);
			}
		}
			
		return groupTravellerList;
	}
	
	private static final class GroupTravellerListDataMapper implements RowMapper<GroupTraveller> {
		
		@Override
		public GroupTraveller mapRow(ResultSet resultSet, int arg1) throws SQLException {
			GroupTraveller groupTraveller = new GroupTraveller();
			
			groupTraveller.setGUserId(AppUtility.convertStringToLong(resultSet.getString("G_USERID")));
			groupTraveller.setTravellerName(AppUtility.checkNullString(resultSet.getString("G_TRAVELLER_NAME")));
			groupTraveller.setDateOfBirth(AppUtility.checkNullString(resultSet.getString("DOB")));
			groupTraveller.setPassportNumber(AppUtility.checkNullString(resultSet.getString("PASSPORT_NO")));
			groupTraveller.setIdentityName(AppUtility.checkNullString(resultSet.getString("IDENTITY_NAME")));
			groupTraveller.setIdentityNumber(AppUtility.checkNullString(resultSet.getString("IDENTITY_NO")));
			groupTraveller.setSiteName(AppUtility.checkNullString(resultSet.getString("SITE_NAME")));
			groupTraveller.setEmail(AppUtility.checkNullString(resultSet.getString("G_EMAIL")));
			groupTraveller.setDesignationName(AppUtility.checkNullString(resultSet.getString("DESIG_NAME")));
			
			return groupTraveller;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GroupTraveller> getGroupUserDetails(AppRequest appRequest)  throws Exception {
		List<GroupTraveller>  groupTravellerList = null;
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getRequestType()) && !AppUtility.isNull(appRequest.getTravellerType()) && !AppUtility.isNull(appRequest.getSiteId()) && !AppUtility.isNull(appRequest.getGUserId())) {
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_GROUP_USER_DETAILS.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new GroupTravellerDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_REQUEST_TYPE, appRequest.getRequestType())
			.addValue(ParamConstant.P_TRAVELLER_TYPE, appRequest.getTravellerType())
			.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
			.addValue(ParamConstant.P_G_USER_ID, appRequest.getGUserId())
			.addValue(ParamConstant.P_REQ_DATA_TYPE_FLAG, "ADDUSER")
			.addValue(ParamConstant.P_TRAVEL_TYPE, "")
			.addValue(ParamConstant.P_TRAVEL_ID, "0")
			.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			if(AppUtility.isSuccess(returnedCode)) {
				groupTravellerList = (List<GroupTraveller>) result.get(ParamConstant.RESULT_LIST);
			}
		}
		return groupTravellerList;
	}
	
	private static final class GroupTravellerDataMapper implements RowMapper<GroupTraveller> {
		
		@Override
		public GroupTraveller mapRow(ResultSet resultSet, int arg1) throws SQLException {
			GroupTraveller groupTraveller = new GroupTraveller();
			
			groupTraveller.setEmpCode(AppUtility.checkNullString(resultSet.getString("G_EMP_CODE")));
			groupTraveller.setEmail(AppUtility.checkNullString(resultSet.getString("G_EMAIL")));
			groupTraveller.setFirstName(AppUtility.checkNullString(resultSet.getString("FIRST_NAME")));
			groupTraveller.setLastName(AppUtility.checkNullString(resultSet.getString("LAST_NAME")));
			groupTraveller.setSiteId(AppUtility.convertStringToInt(resultSet.getString("SITE_ID")));
			groupTraveller.setSiteName(AppUtility.checkNullString(resultSet.getString("SITE_NAME")));
			groupTraveller.setDesignationId(AppUtility.convertStringToInt(resultSet.getString("DESIG_ID")));
			groupTraveller.setDesignationName(AppUtility.checkNullString(resultSet.getString("DESIG_NAME")));
			groupTraveller.setDateOfBirth(AppUtility.checkNullString(resultSet.getString("DOB")));
			groupTraveller.setGenderId(AppUtility.convertStringToInt(resultSet.getString("GENDER_ID")));
			groupTraveller.setGenderDesc(AppUtility.checkNullString(resultSet.getString("GENDER_DESC")));
			groupTraveller.setMealId(AppUtility.convertStringToInt(resultSet.getString("MEAL_ID")));
			groupTraveller.setMealDesc(AppUtility.checkNullString(resultSet.getString("MEAL_DESC")));
			groupTraveller.setPassportNumber(AppUtility.checkNullString(resultSet.getString("PASSPORT_NO")));
			groupTraveller.setNationality(AppUtility.checkNullString(resultSet.getString("NATIONALITY")));
			groupTraveller.setDateOfIssue(AppUtility.checkNullString(resultSet.getString("DATE_OF_ISSUE")));
			groupTraveller.setExpiryDate(AppUtility.checkNullString(resultSet.getString("EXPIRY_DATE")));
			groupTraveller.setPlaceOfIssue(AppUtility.checkNullString(resultSet.getString("PLACE_OF_ISSUE")));
			groupTraveller.setVisaRequiredId(AppUtility.convertStringToInt(resultSet.getString("VISA_REQUIRED_ID")));
			groupTraveller.setVisaRequiredDesc(AppUtility.checkNullString(resultSet.getString("VISA_REQUIRED_DESC")));
			groupTraveller.setEcnrId(AppUtility.convertStringToInt(resultSet.getString("ECNR_ID")));
			groupTraveller.setEcnrDesc(AppUtility.checkNullString(resultSet.getString("ECNR_DESC")));
			groupTraveller.setIdentityId(AppUtility.convertStringToInt(resultSet.getString("IDENTITY_ID")));
			groupTraveller.setIdentityNumber(AppUtility.checkNullString(resultSet.getString("IDENTITY_NO")));
			groupTraveller.setMobileNumber(AppUtility.checkNullString(resultSet.getString("MOBILE_NO")));
			groupTraveller.setExpRemarks(AppUtility.checkNullString(resultSet.getString("EXP_REMARKS")));
			
			return groupTraveller;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GroupTraveller> getGroupTravellersDetailsList(AppRequest appRequest) throws Exception {
		List<GroupTraveller>  groupTravellerList = null;
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getSiteId()) && !AppUtility.isNull(appRequest.getTravelType())) {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_GROUP_TRAVELLERS_DETAILS_LIST.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new GroupTravellersDetailsListDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
			.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
			.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
			.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			if(AppUtility.isSuccess(returnedCode)) {
				groupTravellerList = (List<GroupTraveller>) result.get(ParamConstant.RESULT_LIST);
			}
		}
		return groupTravellerList;
	}
	
	private static final class GroupTravellersDetailsListDataMapper implements RowMapper<GroupTraveller> {
		
		@Override
		public GroupTraveller mapRow(ResultSet resultSet, int arg1) throws SQLException {
			GroupTraveller groupTraveller = new GroupTraveller();
			
			groupTraveller.setEmpCode(AppUtility.checkNullString(resultSet.getString("G_EMP_CODE")));
			groupTraveller.setEmail(AppUtility.checkNullString(resultSet.getString("G_EMAIL")));
			groupTraveller.setGUserId(AppUtility.convertStringToLong(resultSet.getString("G_USERID")));
			groupTraveller.setFirstName(AppUtility.checkNullString(resultSet.getString("FIRST_NAME")));
			groupTraveller.setLastName(AppUtility.checkNullString(resultSet.getString("LAST_NAME")));
			groupTraveller.setSiteId(AppUtility.convertStringToInt(resultSet.getString("SITE_ID")));
			groupTraveller.setSiteName(AppUtility.checkNullString(resultSet.getString("SITE_NAME")));
			groupTraveller.setDesignationId(AppUtility.convertStringToInt(resultSet.getString("DESIG_ID")));
			groupTraveller.setDesignationName(AppUtility.checkNullString(resultSet.getString("DESIG_NAME")));
			groupTraveller.setDateOfBirth(AppUtility.checkNullString(resultSet.getString("DOB")));
			groupTraveller.setAge(AppUtility.checkNullString(resultSet.getString("AGE")));
			groupTraveller.setGenderId(AppUtility.convertStringToInt(resultSet.getString("GENDER_ID")));
			groupTraveller.setGenderDesc(AppUtility.checkNullString(resultSet.getString("GENDER_DESC")));
			groupTraveller.setMealId(AppUtility.convertStringToInt(resultSet.getString("MEAL_ID")));
			groupTraveller.setMealDesc(AppUtility.checkNullString(resultSet.getString("MEAL_DESC")));
			groupTraveller.setPassportNumber(AppUtility.checkNullString(resultSet.getString("PASSPORT_NO")));
			groupTraveller.setNationality(AppUtility.checkNullString(resultSet.getString("NATIONALITY")));
			groupTraveller.setDateOfIssue(AppUtility.checkNullString(resultSet.getString("DATE_OF_ISSUE")));
			groupTraveller.setExpiryDate(AppUtility.checkNullString(resultSet.getString("EXPIRY_DATE")));
			groupTraveller.setPlaceOfIssue(AppUtility.checkNullString(resultSet.getString("PLACE_OF_ISSUE")));
			groupTraveller.setVisaRequiredId(AppUtility.convertStringToInt(resultSet.getString("VISA_REQUIRED_ID")));
			groupTraveller.setVisaRequiredDesc(AppUtility.checkNullString(resultSet.getString("VISA_REQUIRED_DESC")));
			groupTraveller.setEcnrId(AppUtility.convertStringToInt(resultSet.getString("ECNR_ID")));
			groupTraveller.setEcnrDesc(AppUtility.checkNullString(resultSet.getString("ECNR_DESC")));
			groupTraveller.setIdentityId(AppUtility.convertStringToInt(resultSet.getString("IDENTITY_ID")));
			groupTraveller.setIdentityNumber(AppUtility.checkNullString(resultSet.getString("IDENTITY_NO")));
			groupTraveller.setReturnTravel(AppUtility.checkNullString(resultSet.getString("RETURN_TRAVEL")));
			groupTraveller.setTotalAmount(AppUtility.checkNullString(resultSet.getString("TOTAL_AMOUNT")));
			groupTraveller.setMobileNumber(AppUtility.checkNullString(resultSet.getString("MOBILE_NO")));
			groupTraveller.setExpRemarks(AppUtility.checkNullString(resultSet.getString("EXP_REMARKS")));
			
			return groupTraveller;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public GroupTravellerDetail getGroupTravellerDetailsToEdit(AppRequest appRequest) throws Exception {
		
		GroupTravellerDetail  groupTravellerDetail  = new GroupTravellerDetail();
		List<GroupTraveller> groupUserDetails       = new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getGUserId()) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType()) ) {
			
			groupUserDetails = getGroupTravellerDetailsForEdit(appRequest);
			List<Advance> advanceList = getTravellerExpenditureDetails(appRequest);
			
			if(!groupUserDetails.isEmpty()) {
				groupTravellerDetail.setGroupUserDetails(groupUserDetails.get(0));
			}
			if(!advanceList.isEmpty()) {
				groupTravellerDetail.setAdvanceAmountDetails(advanceList);
			}
		}
		return groupTravellerDetail;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GroupTraveller> getGroupTravellerDetailsForEdit(AppRequest appRequest) throws Exception {
		List<GroupTraveller> groupUserDetails       = new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getGUserId()) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType()) ) {
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_GROUP_USER_DETAILS.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new GroupTravellersDetailsListDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_REQUEST_TYPE, "")
			.addValue(ParamConstant.P_TRAVELLER_TYPE, "")
			.addValue(ParamConstant.P_SITE_ID, 0)
			.addValue(ParamConstant.P_G_USER_ID, appRequest.getGUserId())
			.addValue(ParamConstant.P_REQ_DATA_TYPE_FLAG, "EDITUSER")
			.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
			.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
			.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			if(AppUtility.isSuccess(returnedCode)) {
				groupUserDetails = (List<GroupTraveller>) result.get(ParamConstant.RESULT_LIST);
			}
			
		}
		return groupUserDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<Advance> getTravellerExpenditureDetails(AppRequest appRequest) throws Exception {
		
		List<Advance> advanceList	= new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getGUserId()) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType()) ) {
			
			SimpleJdbcCall jdbcCallExp = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_TRAVEL_EXPENDITURE_DETAILS.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new TravelExpenditureDetailsDataMapper());
			
			SqlParameterSource procParamsExp = new MapSqlParameterSource()
			.addValue(ParamConstant.TRAVEL_ID, appRequest.getTravelId())
			.addValue(ParamConstant.TRAVEL_TYPE, appRequest.getTravelType())
			.addValue(ParamConstant.G_USER_ID, appRequest.getGUserId());

			Map<String, Object> resultExp = jdbcCallExp.execute(procParamsExp);
			
			advanceList = (List<Advance>) resultExp.get(ParamConstant.RESULT_LIST);
		}
		return advanceList;
	}
	
	private static final class TravelExpenditureDetailsDataMapper implements RowMapper<Advance> {
		
		@Override
		public Advance mapRow(ResultSet resultSet, int arg1) throws SQLException {
			Advance advance = new Advance();
			
			advance.setCurrencyCode(AppUtility.checkNullString(resultSet.getString("CURRENCY")));
			advance.setDailyAllowanceExpPerDay(AppUtility.convertStringToLong(resultSet.getString("ENT_PER_DAY1")));
			advance.setDailyAllowanceTourDays(AppUtility.convertStringToInt(resultSet.getString("TOTAL_TOUR_DAYS1")));
			advance.setHotelChargesExpPerDay(AppUtility.convertStringToInt(resultSet.getString("ENT_PER_DAY2")));
			advance.setHotelChargesTourDays(AppUtility.convertStringToInt(resultSet.getString("TOTAL_TOUR_DAYS2")));
			advance.setContingencies(AppUtility.convertStringToInt(resultSet.getString("TOTAL_EXP_ID3")));
			advance.setOthers(AppUtility.convertStringToInt(resultSet.getString("TOTAL_EXP_ID4")));
			advance.setTotal(AppUtility.convertStringToDouble(resultSet.getString("TOTAL_EXP")));
			advance.setExchangeRateINR(AppUtility.convertStringToDouble(resultSet.getString("EXCHANGE_RATE")));
			advance.setTotalINR(AppUtility.convertStringToInt(resultSet.getString("TOTAL")));
			
			return advance;
		}
	}
	
	@Override
	public String deleteTravellerDetails(AppRequest appRequest) throws Exception  {
		String errorMessage = "";
			if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getGUserId()) && !AppUtility.isNull(appRequest.getTravelId()) && !AppUtility.isNull(appRequest.getTravelType())) {
				
				int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.TRAVELLERS_DETAILS_DELETE.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.G_USER_ID, appRequest.getGUserId())
				.addValue(ParamConstant.P_TRAVEL_ID, appRequest.getTravelId())
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.P_USER_ID, userId)
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result 	= 	jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
				
				if(!AppUtility.isSuccess(returnedCode)) {
					errorMessage = getErrorMessage(returnedCode);
				}
			}
		return errorMessage;
	}
	
	
	@Override
	public String saveFeedback(FeedbackRequest feedbackRequest) throws Exception {
		String errorMessage = "";
		
		if(!AppUtility.isNull(feedbackRequest)) {
				
			int userId = getUserId(feedbackRequest.getWinUserId(), feedbackRequest.getDomainName());
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.POST_FEEDBACK.getName());

			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_LIKEMOST_FEEDBACK, feedbackRequest.getLikeMostFeedback())
			.addValue(ParamConstant.P_SPECIFIC_FEEDBACK, feedbackRequest.getSpecificFeedback())
			.addValue(ParamConstant.P_LOOKANDFEEL_RATING, Integer.parseInt(feedbackRequest.getLookAndFeelRating()))
			.addValue(ParamConstant.P_CREATING_REQ_RATING, Integer.parseInt(feedbackRequest.getCreatingRequestRating()))
			.addValue(ParamConstant.P_APPROVING_REQ_RATING, Integer.parseInt(feedbackRequest.getApprovingRequestRating()))
			.addValue(ParamConstant.P_SYSTEM_SPEED_RATING, Integer.parseInt(feedbackRequest.getSystemSpeedRating()))
			.addValue(ParamConstant.P_SUPPORT_RATING, Integer.parseInt(feedbackRequest.getSupportRating()))
			.addValue(ParamConstant.P_OVERALL_RATING, Integer.parseInt(feedbackRequest.getOverallRating()))
			.addValue(ParamConstant.P_APPLICATION_ID, Integer.parseInt(feedbackRequest.getApplicationId()))
			.addValue(ParamConstant.P_SOURCE, feedbackRequest.getRequestSource())
			.addValue(ParamConstant.P_IP_ADDRESS, feedbackRequest.getDeviceId())
			.addValue(ParamConstant.USER_ID, userId)
			.addValue(ParamConstant.OUTPUT_STATUS, 0);
			
			Map<String, Object> result 	= jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
			
			if(!AppUtility.isSuccess(returnedCode)) {
				errorMessage = getErrorMessage(returnedCode);
			}
		}
		return errorMessage;
	}
	
	@SuppressWarnings("unchecked")
	public List<LinkTravelRequest> getTravellerCancelledRequest(AppRequest appRequest) throws Exception {
		List<LinkTravelRequest> linkTravelRequestList       = new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravellerId()) && !AppUtility.isNull(appRequest.getTravelType())) {
			
			int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			if(appRequest.getTravellerId() == 0) {
				appRequest.setTravellerId(userId);
			}
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_TRAVELLER_CANCELLED_REQ.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new TravellerCancelledRequestDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
			.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
			.addValue(ParamConstant.OUTPUT_STATUS, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
			if(AppUtility.isSuccess(returnedCode)) {
				linkTravelRequestList = (List<LinkTravelRequest>) result.get(ParamConstant.RESULT_LIST);
			}
		}
		return linkTravelRequestList;
	}
	
	private static final class TravellerCancelledRequestDataMapper implements RowMapper<LinkTravelRequest> {
		
		@Override
		public LinkTravelRequest mapRow(ResultSet resultSet, int arg1) throws SQLException {
			LinkTravelRequest linkTravelRequest = new LinkTravelRequest();
			
			linkTravelRequest.setTravelId(Long.parseLong(AppUtility.checkNullString(resultSet.getString("TRAVEL_ID"))));
			linkTravelRequest.setTravelType(AppUtility.checkNullString(resultSet.getString("TRAVEL_TYPE")));
			linkTravelRequest.setTravelReqNo(AppUtility.checkNullString(resultSet.getString("TRAVEL_REQ_NO")));
			linkTravelRequest.setTravelStatus(AppUtility.checkNullString(resultSet.getString("TRAVEL_STATUS")));
			
			return linkTravelRequest;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TicketType> getTicketType(AppRequest appRequest) throws Exception {
		List<TicketType> ticketTypeList       = new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest)) {
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_TICKET_TYPE.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new TicketTypeDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.LAST_SYNC_DATE, "")
			.addValue(ParamConstant.OUTPUT_STATUS, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
			if(AppUtility.isSuccess(returnedCode)) {
				ticketTypeList = (List<TicketType>) result.get(ParamConstant.RESULT_LIST);
			}
		}
		return ticketTypeList;
	}
	
	private static final class TicketTypeDataMapper implements RowMapper<TicketType> {
		
		@Override
		public TicketType mapRow(ResultSet resultSet, int arg1) throws SQLException {
			TicketType ticketType = new TicketType();
			
			ticketType.setTicketTypeId(Integer.parseInt(AppUtility.checkNullString(resultSet.getString("TICKET_TYPE_ID"))));
			ticketType.setTicketTypeName(AppUtility.checkNullString(resultSet.getString("TICKET_TYPE_NAME")));
			ticketType.setRecordStatus(AppUtility.checkNullString(resultSet.getString("RECORD_STATUS")));
			
			return ticketType;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<CityAirport> getCityAirportList(AppRequest appRequest) throws Exception {
		List<CityAirport> cityAirportList       = new ArrayList<>();
		
		if(!AppUtility.isNull(appRequest)) {
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.PROC_GET_CITY_AIRPORT_LIST.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new CityAirportDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
				.addValue(ParamConstant.P_TRAVEL_AGENCY_ID, appRequest.getTravelAgencyId())
				.addValue(ParamConstant.P_CITY_AIRPORT_NAME, appRequest.getCityAirportName() == null ? "" : appRequest.getCityAirportName())
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);

			Map<String, Object> result = jdbcCall.execute(procParams);

			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			if(AppUtility.isSuccess(returnedCode)) {
				cityAirportList	= (List<CityAirport>) result.get(ParamConstant.RESULT_LIST);
		    }
		}
		return cityAirportList;
	}
	
	
	private static final class CityAirportDataMapper implements RowMapper<CityAirport> {
		
		@Override
		public CityAirport mapRow(ResultSet resultSet, int arg1) throws SQLException {
			CityAirport cityAirport = new CityAirport();
			
			cityAirport.setCategory(AppUtility.checkNullString(resultSet.getString("CATEGORY")));
			cityAirport.setCityName(AppUtility.checkNullString(resultSet.getString("CITY_NAME")));
			cityAirport.setAirportName(AppUtility.checkNullString(resultSet.getString("AIRPORT_NAME")));
			cityAirport.setAirportCode(AppUtility.checkNullString(resultSet.getString("AIRPORT_CODE")));
			cityAirport.setCountryName(AppUtility.checkNullString(resultSet.getString("COUNTRY_NAME")));
			cityAirport.setCountryCode(AppUtility.checkNullString(resultSet.getString("COUNTRY_CODE")));
			cityAirport.setCountryId(Integer.parseInt(AppUtility.checkNullString(resultSet.getString("COUNTRY_ID"))));
			
			return cityAirport;
		}
	}
	
	@Override
	public RequestData saveGroupRequestData(GroupTravelRequest groupTravelRequest) throws Exception {
		RequestData requestData = null;
		
		if(!AppUtility.isNull(groupTravelRequest)) {
			
			if(groupTravelRequest.getTravellerId() == 0) {
				int userId = getUserId(groupTravelRequest.getWinUserId(), groupTravelRequest.getDomainName());
				groupTravelRequest.setTravellerId(userId);
			}
			
			if(groupTravelRequest.getTravellerId() > 0) {
				
				String groupTravellerListXML   = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getGroupTravellerDetail());
				String flightDetailsXML        = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getFlightJourneyDetails());
				String trainDetailsXML 	       = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getTrainJourneyDetails());
				String carDetailsXML           = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getCarJourneyDetails());
				String accommodationDetailsXML = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getAccommodationDetails());
				String requestApproversXML     = generateGroupTravelRequestApproversXML(groupTravelRequest);
				String budgetActualDetailsXML  = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getBudgetActualDetails());
				String totalTravelFareXML      = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getTotalTravelFareDetails());
				String nonMATASourceDetailsXNL = RequestConvertor.convertObjectToXmlString(groupTravelRequest.getNonMATASourceDetails());
				
				SimpleJdbcCall jdbcCall	= new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.SAVE_GROUP_TRAVEL_REQUEST.getName());
				
				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_ID, groupTravelRequest.getTravelId())
				.addValue(ParamConstant.P_WIN_USER_ID, groupTravelRequest.getWinUserId())
				.addValue("P_DOMAIN_NAME", groupTravelRequest.getDomainName())
				.addValue("P_UNIT_ID", groupTravelRequest.getSiteId())
				.addValue(ParamConstant.USER_ID, groupTravelRequest.getTravellerId())// GV
				.addValue("P_REASON_FOR_TRAVEL", groupTravelRequest.getReasonForTravel())
				.addValue("P_REASON_FOR_SKIP", groupTravelRequest.getReasonForSkip())
				.addValue("P_UNIT_LOCATION", groupTravelRequest.getSiteLocation())
				.addValue("P_GST_NO", groupTravelRequest.getGstNo())
				.addValue("P_BILLING_CLIENT", groupTravelRequest.getBillingClient())
				.addValue("P_BILLING_APPROVER", groupTravelRequest.getBillingApprover())
				.addValue("P_REPORT_TO", groupTravelRequest.getApproverLevel1())
				.addValue("P_DEPT_HEAD", groupTravelRequest.getApproverLevel2())
				.addValue("P_BOARD_MEMBER", groupTravelRequest.getApproverLevel3())
                .addValue("P_APPROVER_XML", requestApproversXML)
				.addValue(ParamConstant.P_TRAVEL_TYPE, groupTravelRequest.getTravelType())
				.addValue("P_BASE_CURRENCY", groupTravelRequest.getBaseCurrency())
				.addValue("P_COST_CENTER_ID", groupTravelRequest.getCostCentreId())
				.addValue("P_MEAL_ID", groupTravelRequest.getPreferredMealId())
				.addValue("P_TRAVELLER_XML", groupTravellerListXML)
				.addValue("P_FLIGHT_XML", flightDetailsXML)
				.addValue("P_TRAIN_XML", trainDetailsXML)
				.addValue("P_CAR_XML", carDetailsXML)
				.addValue("P_ACCOMMODATION_XML",accommodationDetailsXML)
				.addValue("P_BUDGET_ACTUAL_XML", budgetActualDetailsXML)
				.addValue("P_TOTAL_TRAVEL_FARE_XML", totalTravelFareXML)
				.addValue("P_LOCAL_AGENT_FLAG", groupTravelRequest.getLocalAgentFlag())
				.addValue("P_NON_MATA_SOURCE_DETAILS_XML", nonMATASourceDetailsXNL)
				.addValue("P_IP_ADDRESS", groupTravelRequest.getDeviceId())
				.addValue("P_REQUEST_SOURCE", groupTravelRequest.getRequestSource())
				.addValue("P_SUBMIT_FLAG", groupTravelRequest.getSubmitFlag())
				.addValue("ERR_NO", 0)
				.addValue("REQUEST_NO", "")
				.addValue("RESPONSE_MSG", "");
				
				Map<String, Object> result = jdbcCall.execute(procParams);
	            
				String returnedCode = AppUtility.checkNullString(result.get("ERR_NO"));
	            
				requestData = new RequestData();
	            
				if(AppUtility.isSuccess(returnedCode)) {
	            	requestData.setRequestNo(AppUtility.checkNullString(result.get("REQUEST_NO")));
	            	requestData.setRequestStatus(AppUtility.checkNullString(result.get("RESPONSE_MSG")));
	            } else {
	            	requestData.setRequestStatus(getErrorMessage(returnedCode));
	            }
			}
		}
		return requestData;
	}


	@SuppressWarnings("unchecked")
	@Override
	public OOOApprover checkOOOApprover(AppRequest appRequest) {
		OOOApprover oooApprover = null;

		if(!AppUtility.isNull(appRequest) && !AppUtility.isNull(appRequest.getTravellerId()) && !AppUtility.isNull(appRequest.getTravelType())) {
			
			int userId = getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			if(appRequest.getTravellerId() == 0) {
				appRequest.setTravellerId(userId);
			}
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.CHECKED_OOO_APPROVER.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new OOOApproverDataMapper());
			
			SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue(ParamConstant.P_APPROVER_LVL1, appRequest.getApproverLevel1())
					.addValue(ParamConstant.P_APPROVER_LVL2, appRequest.getApproverLevel2())
					.addValue(ParamConstant.P_APPROVER_LVL3, appRequest.getApproverLevel3())
					.addValue(ParamConstant.P_TRAVELLER_ID, appRequest.getTravellerId())
					.addValue(ParamConstant.P_USER_ID, userId)
					.addValue(ParamConstant.P_TRAVEL_TYPE, appRequest.getTravelType())
					.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
			
			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
			
			 if(AppUtility.isSuccess(returnedCode)) {
            	List<OOOApprover> dataList = (List<OOOApprover>) result.get(ParamConstant.RESULT_LIST);
        		
            	if(AppUtility.containsData(dataList)) {
            		oooApprover = dataList.get(0);
        		}
            }
		}
			
		return oooApprover;
	}
	
	private static final class OOOApproverDataMapper implements RowMapper<OOOApprover> {

		@Override
		public OOOApprover mapRow(ResultSet resultSet, int arg1) throws SQLException {
			
			OOOApprover oooApprover = new OOOApprover();
			oooApprover.setOooExistsFlag(AppUtility.checkNullString(resultSet.getString("IS_OOO_EXIST")));
			oooApprover.setOooApproverText(AppUtility.checkNullString(resultSet.getString("OOO_APPROVER_TXT")));
			return oooApprover;
		}
		
	}
}