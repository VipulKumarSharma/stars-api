package com.mind.api.stars.dao;

import java.util.List;

import com.mind.api.stars.model.Airport;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Booking;
import com.mind.api.stars.model.City;
import com.mind.api.stars.model.Country;
import com.mind.api.stars.model.DailyAllowance;
import com.mind.api.stars.model.Hotel;
import com.mind.api.stars.model.VersionDetail;

public interface DataDAO {

	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	City getCityByCountry(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	List<Airport> getAirportsInfo(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	List<Hotel> getHotelsInfo(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	DailyAllowance getdailyAllowanceAmount(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	List<Booking> getPastBookings(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	List<Booking> getFutureBookings(AppRequest appRequest);
	
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	Country getCountryById(AppRequest appRequest) throws Exception;
	/**
	 * 
	 * @param appRequest
	 * @return
	 */
	VersionDetail getVersion(AppRequest appRequest);
}
