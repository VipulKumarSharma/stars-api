package com.mind.api.stars.dao;

import java.util.List;
import java.util.Map;

import com.mind.api.stars.model.AdvanceDetailResponse;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.ApproverDetail;
import com.mind.api.stars.model.ApproverLevelWise;
import com.mind.api.stars.model.CarCategory;
import com.mind.api.stars.model.CarLocation;
import com.mind.api.stars.model.CheckApprover;
import com.mind.api.stars.model.CheckTESCount;
import com.mind.api.stars.model.CityAirport;
import com.mind.api.stars.model.Comment;
import com.mind.api.stars.model.CostCentre;
import com.mind.api.stars.model.DeleteJourneyDetailRequest;
import com.mind.api.stars.model.ExchangeRateRequest;
import com.mind.api.stars.model.ExchangeRateResponse;
import com.mind.api.stars.model.FeedbackRequest;
import com.mind.api.stars.model.GroupTravelRequest;
import com.mind.api.stars.model.GroupTraveller;
import com.mind.api.stars.model.GroupTravellerDetail;
import com.mind.api.stars.model.IdentityProof;
import com.mind.api.stars.model.IdentityProofDetails;
import com.mind.api.stars.model.Journey;
import com.mind.api.stars.model.LinkTravelRequest;
import com.mind.api.stars.model.MailDetail;
import com.mind.api.stars.model.MailFlow;
import com.mind.api.stars.model.Meal;
import com.mind.api.stars.model.OOOApprover;
import com.mind.api.stars.model.PassportDetail;
import com.mind.api.stars.model.PostComment;
import com.mind.api.stars.model.RequestData;
import com.mind.api.stars.model.RequestDetails;
import com.mind.api.stars.model.RequestSetting;
import com.mind.api.stars.model.RequestsCount;
import com.mind.api.stars.model.Seat;
import com.mind.api.stars.model.SeatRequest;
import com.mind.api.stars.model.SiteBasedUserRequest;
import com.mind.api.stars.model.StayType;
import com.mind.api.stars.model.TicketType;
import com.mind.api.stars.model.TimeMode;
import com.mind.api.stars.model.TransitHouse;
import com.mind.api.stars.model.TravelClass;
import com.mind.api.stars.model.TravelClassRequest;
import com.mind.api.stars.model.TravelRequest;
import com.mind.api.stars.model.Unit;
import com.mind.api.stars.model.UnitLocation;
import com.mind.api.stars.model.User;
import com.mind.api.stars.model.UserProfileInfo;
import com.mind.api.stars.model.WorkflowApprover;

public interface RequestDAO {

	RequestData saveRequestData(TravelRequest travelRequest);
	
	Map<String, Object> getRequestDetails(AppRequest appRequest);
	
	List<Comment> getApproverComments(long travelId, String travelType, String commentType);
	
	List<RequestDetails> getRecentTravelRequestsData(AppRequest appRequest);
	
	List<Map<String, Object>> getLastApprovedRequestDetailsList(AppRequest appRequest);
	
	RequestsCount getRequestsCount(AppRequest appRequest);
	
	Map<String, Object> getRequestsByType(AppRequest appRequest);
	
	List<WorkflowApprover> getRequestWorkflow(AppRequest appRequest);
	
	List<Journey> getUpcomingJourneyDetails(AppRequest appRequest);
	
	RequestSetting getRequestSetting(AppRequest appRequest);
	
	List<ApproverDetail> getFirstLevelApprovers(AppRequest appRequest);
	
	List<ApproverDetail> getSecondLevelApprovers(AppRequest appRequest);
	
	List<ApproverDetail> getThirdLevelApprovers(AppRequest appRequest);
	
	List<WorkflowApprover> getDefaultWorkflowApprover(AppRequest appRequest);
	
	ApproverLevelWise getApproverLevelWise(AppRequest appRequest);
	
	ExchangeRateResponse getExchangeRate(ExchangeRateRequest exchangeRateRequest);
	
	AdvanceDetailResponse getForexTotal(ExchangeRateRequest exchangeRateRequest);
	
	List<User> getSiteBasedUsers(SiteBasedUserRequest siteBasedUserRequest);
	
	List<Seat> getSeatList(SeatRequest seatRequest);
	
	List<TravelClass> getTravelClassList(TravelClassRequest travelClassRequest);
	
	List<TimeMode> getTimeModeList(AppRequest appRequest); 
	
	List<UnitLocation> getUnitLocationList(AppRequest appRequest);
	
	List<Unit> getBillingUnitList(AppRequest appRequest);
	
	List<User> getBillingApproverList(AppRequest appRequest);
	
	List<Meal> getMealList(AppRequest appRequest);
	
	List<CostCentre> getCostCentreList(AppRequest appRequest);
	
	List<CarCategory> getCarCategoryList(AppRequest appRequest);
	
	CheckTESCount checkTESCount(AppRequest appRequest);
	
	CheckApprover checkApproverExistsInDefaultWorkflow(AppRequest appRequest);
	
	List<IdentityProof> getIdentityProofList(AppRequest appRequest);
	
	PassportDetail getPassportDetails(AppRequest appRequest);
	
	UserProfileInfo getUserDetails(AppRequest appRequest);
	
	IdentityProofDetails getIdentityProofDetails(AppRequest appRequest);
	
	List<TransitHouse> getTransitHouseList(AppRequest appRequest);
	
	List<StayType> getStayTypeList(AppRequest appRequest);
	
	Map<String, Object> getRequestCommentsDetails(AppRequest appRequest) throws Exception;
	
	String deleteRequestComment(AppRequest appRequest) throws Exception;
	
	String saveRequestComment(PostComment postComment) throws Exception;
	
	String cancelTravelRequest(PostComment postComment) throws Exception;
	
	boolean deleteRequestData(AppRequest appRequest);
	
	List<MailFlow> getMailFlowList(AppRequest appRequest);
	
	MailDetail getMailDetail(AppRequest appRequest);
	
	List<CarLocation> getCarLocationList(AppRequest appRequest);
	
	Map<String, Object> getRequestAttachmentDetails(AppRequest appRequest) throws Exception;
	
	String deleteRequestAttachment(AppRequest appRequest) throws Exception;
	
	String deleteRequestJourneyDetail(DeleteJourneyDetailRequest deleteJourneyDetailRequest) throws Exception;
	
	Map<String, Object> getRequestDetailForEdit(AppRequest appRequest);
	
	List<GroupTraveller> getGroupUserDetailsList(AppRequest appRequest) throws Exception;
	
	List<GroupTraveller> getGroupUserDetails(AppRequest appRequest) throws Exception;
	
	List<GroupTraveller> getGroupTravellersDetailsList(AppRequest appRequest) throws Exception;
	
	GroupTravellerDetail getGroupTravellerDetailsToEdit(AppRequest appRequest) throws Exception;
	
	String deleteTravellerDetails(AppRequest appRequest) throws Exception;
	
	String saveFeedback(FeedbackRequest feedbackRequest) throws Exception;
	
	List<LinkTravelRequest> getTravellerCancelledRequest(AppRequest appRequest) throws Exception;
	
	List<TicketType> getTicketType(AppRequest appRequest) throws Exception;
	
	List<CityAirport> getCityAirportList(AppRequest appRequest) throws Exception;

	RequestData saveGroupRequestData(GroupTravelRequest groupTravelRequest) throws Exception;
	
	OOOApprover checkOOOApprover(AppRequest appRequest) throws Exception;
	
}
