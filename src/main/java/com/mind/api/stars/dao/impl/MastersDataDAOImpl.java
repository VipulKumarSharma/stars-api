package com.mind.api.stars.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.MastersDataDAO;
import com.mind.api.stars.dto.AttachmentResponseDTO;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Attachment;
import com.mind.api.stars.model.City;
import com.mind.api.stars.model.CityResponse;
import com.mind.api.stars.model.Country;
import com.mind.api.stars.model.CountryResponse;
import com.mind.api.stars.model.Currency;
import com.mind.api.stars.model.Time;
import com.mind.api.stars.model.Unit;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class MastersDataDAOImpl extends BaseDAO implements MastersDataDAO {
	
	@Resource
	private AppDbUtility appDbUtility;
	

	@SuppressWarnings("unchecked")
	@Override
	public CityResponse getCityMasterData(AppRequest appRequest) {
		CityResponse cityResponse = new CityResponse();
		
		if(appRequest != null) {
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.CITY_MASTER_DATA.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new CityDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.LAST_IMPORT_DATE, new Date())
				.addValue(ParamConstant.OUTPUT_STATUS, 	"");
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				
				cityResponse.setCities((List<City>) result.get(ParamConstant.RESULT_LIST));
				cityResponse.setLastSyncDate(AppUtility.syncDate(AppUtility.checkNullString(result.get(ParamConstant.LAST_IMPORT_DATE))));
				cityResponse.setSyncMessage(AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS)));
				
			} catch (Exception e) {
				log.error("Error occurred in MastersDataDAOImpl : getCityMasterData() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
	    
		return cityResponse;
	}

	private static final class CityDataMapper implements RowMapper<City> {

		@Override
		public City mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			City bean = new City();
			
			int cityId	= rs.getInt("CITY_ID");
			String cityCode	= AppUtility.checkNullString(rs.getString("CITY_CODE"));
			String cityName = AppUtility.checkNullString(rs.getString("CITY_NAME"));
			int countryId = rs.getInt("COUNTRY_ID");
			String countryCode = AppUtility.checkNullString(rs.getString("COUNTRY_CODE"));
			String countryName = AppUtility.checkNullString(rs.getString("COUNTRY_NAME"));
			String recordStatus = AppUtility.checkNullString(rs.getString("RECORD_STATUS"));
			
			bean.setCityId(cityId);
			bean.setCityCode(cityCode);
			bean.setCityName(cityName);
			bean.setCountryId(countryId);
			bean.setCountryCode(countryCode);
			bean.setCountryName(countryName);
			bean.setRecordStatus(recordStatus);
		    
			return bean;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public CountryResponse getCountryMasterData(AppRequest appRequest) {
		CountryResponse countryResponse = new CountryResponse();
		
		if(appRequest != null) {
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.COUNTRY_MASTER_DATA.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new CountryDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
				.addValue(ParamConstant.OUTPUT_STATUS, 	"");
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				
				countryResponse.setCountries((List<Country>) result.get(ParamConstant.RESULT_LIST));
				countryResponse.setLastSyncDate(AppUtility.syncDate(AppUtility.checkNullString(result.get(ParamConstant.LAST_SYNC_DATE))));
				countryResponse.setSyncMessage(AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS)));
				
			} catch (Exception e) {
				log.error("Error occurred in MastersDataDAOImpl : getCountryMasterData() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		}
		
		return countryResponse;
	}
	
	private static final class CountryDataMapper implements RowMapper<Country> {

		@Override
		public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Country bean = new Country();
			
			int countryId = rs.getInt("COUNTRY_ID");
			String countryCode = AppUtility.checkNullString(rs.getString("COUNTRY_CODE"));
			String countryName = AppUtility.checkNullString(rs.getString("COUNTRY_NAME"));
			String recordStatus = AppUtility.checkNullString(rs.getString("RECORD_STATUS"));
			
			bean.setCountryId(countryId);
			bean.setCountryCode(countryCode);
			bean.setCountryName(countryName);
			bean.setRecordStatus(recordStatus);
		    
			return bean;
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Time> getTimeMasterData(AppRequest appRequest) {
		List<Time> timeList = null;
		
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.TIME_MASTER_DATA.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new TimeDataMapper());

			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
			
			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
            
            if(AppUtility.isSuccess(returnedCode)) {
            	timeList = (List<Time>) result.get(ParamConstant.RESULT_LIST);
            }
	
		} catch (Exception e) {
			log.error("Error occurred in MastersDataDAOImpl : getTimeMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return timeList;
	}

	private static final class TimeDataMapper implements RowMapper<Time> {

		@Override
		public Time mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Time bean = new Time();
			
			int timeId = rs.getInt("TIME_ID");
			String preferTime = AppUtility.checkNullString(rs.getString("PREFER_TIME"));
			
			bean.setTimeId(timeId);
			bean.setPreferTime(preferTime);
		    
			return bean;
		}
	}
	
	@Override
	public List<Unit> getUnitMasterData(AppRequest appRequest) {
		List<Unit> unitList = null;
		
		try {
			
			int userId = appDbUtility.getUserId(appRequest.getWinUserId(), appRequest.getDomainName());
			
			/* Add this block for handling siteId is 0(zero) */
			if(appRequest.getSiteId()==0) {
				int primarySiteId = appDbUtility.getPrimarySiteIdByUser(appRequest.getWinUserId(), appRequest.getDomainName());
				appRequest.setSiteId(primarySiteId);
			}
			/* End */
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.UNIT_MASTER_DATA.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new UnitDataMapper());

			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.P_USER_ID, userId)
			.addValue(ParamConstant.P_SITE_ID, appRequest.getSiteId())
			.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
			.addValue(ParamConstant.OUTPUT_STATUS, 0);
			
			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
            
            if(AppUtility.isSuccess(returnedCode)) {
            	unitList = (List<Unit>) result.get(ParamConstant.RESULT_LIST);
            }
	
		} catch (Exception e) {
			log.error("Error occurred in MastersDataDAOImpl : getUnitMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return unitList;
	}
	
	private static final class UnitDataMapper implements RowMapper<Unit> {

		@Override
		public Unit mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Unit bean = new Unit();
			bean.setSiteId(rs.getString("SITE_ID"));
			bean.setSiteName(rs.getString("SITE_NAME"));
			bean.setIsPrimarySite(rs.getString("IS_PRIMARY_SITE"));
			
			return bean;
		}
	}
	
	
	@Override
	public List<Currency> getCurrencyMasterData(AppRequest appRequest) {
		List<Currency> currencyList = null;
		
		try {
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
			.withProcedureName(Procedure.CURRENCY_MASTER_DATA.getName())
			.returningResultSet(ParamConstant.RESULT_LIST, new CurrencyDataMapper());

			SqlParameterSource procParams = new MapSqlParameterSource()
			.addValue(ParamConstant.LAST_SYNC_DATE, appRequest.getSyncDate())
			.addValue(ParamConstant.OUTPUT_STATUS, 0);
			
			Map<String, Object> result = jdbcCall.execute(procParams);
			
			String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
            
            if(AppUtility.isSuccess(returnedCode)) {
            	currencyList = (List<Currency>) result.get(ParamConstant.RESULT_LIST);
            }
	
		} catch (Exception e) {
			log.error("Error occurred in MastersDataDAOImpl : getCurrencyMasterData() : ", e);
			ExceptionLogger.logExceptionToDB(e);
		}
	    
		return currencyList;
	}
	
	
	private static final class CurrencyDataMapper implements RowMapper<Currency> {

		@Override
		public Currency mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Currency bean = new Currency();
			bean.setCurrencyId(rs.getString("CUR_ID"));
			bean.setCurrencyName(rs.getString("CURRENCY_NAME"));
			bean.setCurrencyCode(rs.getString("CURRENCY_CODE"));
			bean.setCurrencyDesc(rs.getString("CURRENCY_DESC"));
			bean.setCurrencySymbol(rs.getString("CURRENCY_SYMBOL"));
			bean.setBaseCurrencyFlag(rs.getString("BASE_CUR_FLAG"));
		    
			return bean;
		}
	}


	@Override
	public AttachmentResponseDTO uploadAttachment(Attachment attachment) {
		
		AttachmentResponseDTO attachmentResponseDTO = new AttachmentResponseDTO();
			try {
				
				int userId = appDbUtility.getUserId(attachment.getWinUserId(), attachment.getDomainName());
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.PROC_TRAVEL_ATTACHMENT.getName())/*.returningResultSet(ParamConstant.RESULT_LIST, new CurrencyDataMapper())*/;

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.SANCTION_ID, attachment.getTravelId())
				.addValue(ParamConstant.FILE_NAME, attachment.getFileName())
				.addValue(ParamConstant.FILE_TYPE, attachment.getFileExtension())
				.addValue(ParamConstant.FILE_SIZE, attachment.getFileSize())
				.addValue(ParamConstant.FILE_LOCATION, attachment.getUUID())
				.addValue(ParamConstant.PAGE_REF, "1")
				.addValue(ParamConstant.DOC_REF,  attachment.getFileName())
				.addValue(ParamConstant.UPLOADED_BY, String.valueOf(userId))
				.addValue(ParamConstant.TRAVEL_TYPE, attachment.getTravelType())
				.addValue(ParamConstant.ATTACHMENT_FILE, attachment.getFile().getBytes())
				.addValue(ParamConstant.OUTPUT_STATUS, 0);
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				
				//String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS));
				
				/*if(AppUtility.isSuccess(returnedCode)) {
					result.get(ParamConstant.RESULT_LIST);
				}*/
				
				
			} catch (Exception e) {
				log.error("Error occurred in MastersDataDAOImpl : uploadAttachment() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		return attachmentResponseDTO;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Attachment> getAttachment(Attachment attachment) {
		
		List<Attachment> attachmentList = new ArrayList<Attachment>();
			try {
				
				int userId = appDbUtility.getUserId(attachment.getWinUserId(), attachment.getDomainName());
				
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.PROC_GET_TRAVEL_ATTACHMENTS.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new AttachmentDataMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.P_TRAVEL_ID,  attachment.getTravelId())
				.addValue(ParamConstant.P_TRAVEL_TYPE,  attachment.getTravelType())
				.addValue(ParamConstant.P_USER_ID,  userId)
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
	            if(AppUtility.isSuccess(returnedCode)) {
	            	attachmentList = (List<Attachment>) result.get(ParamConstant.RESULT_LIST);
	            }
				
			} catch (Exception e) {
				log.error("Error occurred in MastersDataDAOImpl : getAttachment() : ", e);
				ExceptionLogger.logExceptionToDB(e);
			}
		return attachmentList;
	}
	
	private static final class AttachmentDataMapper implements RowMapper<Attachment> {

		@Override
		public Attachment mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Attachment bean = new Attachment();
			bean.setAttachmentId(rs.getString("ATTACHMENT_ID"));
			bean.setFileBytes(rs.getBytes("ATTACHMENT_FILE"));
			bean.setFileName(rs.getString("FILES_NAME"));
			bean.setFileSize(rs.getString("FILE_SIZE"));
			bean.setFileExtension(rs.getString("FILE_TYPE"));
		    
			return bean;
		}
	}
	
}
