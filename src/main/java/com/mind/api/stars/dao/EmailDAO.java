package com.mind.api.stars.dao;

public interface EmailDAO {
    void sendReqsMailOnOriginating(String strTravelId, String strReqType, String sUserId) throws Exception;
	boolean isAdvanceTravelRequest(String strTravelId, String strTravelType, String strTravelAgencyId);
	String getMailBodyForCancellation (String purchaseRequisitionId,String mailUserId,String cancelComments,String travelType,String sUserId);
	public void sendMailToChairman(String purchaseRequisitionId,String reqType,String strRequisitionComments,String strTravellerSiteId,String strRoleId,String mailSubject,String userId);
	void sendCancellationEmail(String strTravelType,String purchaseRequisitionId,String strRequisitionId,String strComments,String ipAddr,String sUserId,String sUserRole);
}
