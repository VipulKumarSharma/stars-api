package com.mind.api.stars.dao.impl;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.EmailDAO;
import com.mind.api.stars.dto.AccommodationDTO;
import com.mind.api.stars.dto.AccommodationDetailsDTO;
import com.mind.api.stars.dto.AdvanceDTO;
import com.mind.api.stars.dto.AdvanceDetailsDTO;
import com.mind.api.stars.dto.AppRequestDTO;
import com.mind.api.stars.dto.ApproverDTO;
import com.mind.api.stars.dto.BudgetActualDTO;
import com.mind.api.stars.dto.CarJourneyDTO;
import com.mind.api.stars.dto.CarJourneyDetailsDTO;
import com.mind.api.stars.dto.CommentDTO;
import com.mind.api.stars.dto.FlightJourneyDTO;
import com.mind.api.stars.dto.FlightJourneyDetailsDTO;
import com.mind.api.stars.dto.InsuranceDetailDTO;
import com.mind.api.stars.dto.PassportDetailDTO;
import com.mind.api.stars.dto.TrainJourneyDTO;
import com.mind.api.stars.dto.TrainJourneyDetailsDTO;
import com.mind.api.stars.dto.TravelFareDTO;
import com.mind.api.stars.dto.TravelJourneyDetailDTO;
import com.mind.api.stars.dto.TravelRequestDetailsDTO;
import com.mind.api.stars.dto.UserDetailDTO;
import com.mind.api.stars.dto.VisaDetailDTO;
import com.mind.api.stars.enums.JourneyType;
import com.mind.api.stars.service.RequestService;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.AppMessageUtility;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.wsclient.dao.PushStarsReqDetailsToERPMATA;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class EmailDAOImpl extends AppDbUtility implements EmailDAO
{

    @Autowired
    public AppMessageUtility appMessageUtility;

    @Autowired
    private RequestService requestServices;

    @Autowired
    private PushStarsReqDetailsToERPMATA pushStarsReqDetai;

    public void sendReqsMailOnOriginating(String strTravelId, String strReqType, String sUserId)
    {

        try
        {

            log.info("sendRequisitionMailOnOriginating Start for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

            String strType = "";
            String strUserId = "";
            String sSqlStr = "";
            String strRequistionId = null;
            String reqType = null;
            String strRequistionNumber = null;
            String strMailSubject = null;
            String strMailSubject1 = null;
            String strMailRefNumber = null;
            String strRequistionCreatorName = null;
            String strRequistionCreatorMail = null;
            String strRequistionCreatedDate = null;
            StringBuilder strMailMsg = new StringBuilder();
            StringBuilder strMailMsg1 = new StringBuilder();
            String strstrRequistionApproverName = null;
            String strstrRequistionApproverEmail = null;
            String strSiteName = null;
            String strUserNm = "";
            String strTravellerSex = "";
            String strSex = "";
            String strSql = "";
            String strRequistionCreatedDatewithoutPmAm = "";
            String strSqlSql = "";
            String strGroupTravelFlag = "";
            String strGroupTravel = "";
            String strMailIdOrignalApprover = "";
            String strreasonFortravel = "";
            String strReviewer = "";
            String strReviewerEmail = "";
            String strApproverEmail = "";
            String strIsmailFwd = "N";
            String strTravelAgencyTypeId = "";
            String strMailToMATA = null;
            String strCCMailCreate = "";
            // GET THE VALUES FROM THE PREVIOUS SCREEN
            strRequistionId = strTravelId;
            reqType = strReqType;
            String strsesLanguage = null;
            String strCurrentDate = null;
            String strCurrentYear = null;
            // FETCH THE LATEST MAIL ID FROM REQ_MAILBOX
            int intMaliRefNumber = AppUtility.convertStringToInt(getMaxMailRefNo());

            if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
            {
                strType = "D";
                sSqlStr = "SELECT T.TRAVEL_REQ_NO AS RNO,DBO.USER_NAME(TA.APPROVER_ID)AS NEXTAPPROVER,DBO.USEREMAIL(TA.APPROVER_ID) AS APPROVEREMAIL,DBO.USER_NAME(T.C_USERID) AS CREATOR,DBO.USEREMAIL(T.C_USERID)AS CREATOREMAIL,CONVERT(VARCHAR(30),T.C_DATETIME,113) AS DATETIME,DBO.USER_NAME(TA.TRAVELLER_ID) AS TRAVELLER_NAME ,CONVERT(VARCHAR(30),T.TRAVEL_DATE,113) TRAVEL_DATE,T.SEX,.DBO.SITENAME(T.SITE_ID) AS SITE_NAME ,ISNULL(T.GROUP_TRAVEL_FLAG,'N') AS GROUP_TRAVEL_FLAG,dbo.FN_GET_EXPENDITURE('"
                        + strRequistionId
                        + "','D') AS Expenditure,dbo.sitename(t.BILLING_SITE) as BILLING_SITE,REASON_FOR_TRAVEL,BILLING_SITE as BILLING_SITE_ID ,BILLING_CLIENT,T.SITE_ID as SITE_ID,T.C_USERID AS USER_ID,TA.APPROVER_ID,T.TRAVELLER_ID, (select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = T.SITE_ID) as TRAVEL_AGENCY_ID, (select MAIL_TO_MATA from M_SITE where SITE_ID = T.SITE_ID) as MAIL_TO_MATA FROM T_TRAVEL_DETAIL_DOM T,T_APPROVERS TA WHERE T.TRAVEL_ID=TA.TRAVEL_ID AND TA.APPROVE_STATUS=0 AND TA.ORDER_ID=DBO.MINREQORDERNO("
                        + strRequistionId + ") AND T.TRAVEL_ID=" + strRequistionId + " AND TRAVEL_TYPE='D' AND T.STATUS_ID=10 ";
            } else
            {
                strType = "I";
                sSqlStr = "SELECT T.TRAVEL_REQ_NO AS RNO,DBO.USER_NAME(TA.APPROVER_ID)AS NEXTAPPROVER,DBO.USEREMAIL(TA.APPROVER_ID)AS APPROVEREMAIL,DBO.USER_NAME(T.C_USERID) AS CREATOR,DBO.USEREMAIL(T.C_USERID)AS CREATOREMAIL,CONVERT(VARCHAR(30),T.C_DATETIME,113) as DATETIME,DBO.USER_NAME(TA.TRAVELLER_ID) AS TRAVELLER_NAME ,CONVERT(VARCHAR(30),T.TRAVEL_DATE,113) TRAVEL_DATE,T.SEX,.DBO.SITENAME(T.SITE_ID) AS SITE_NAME ,ISNULL(T.GROUP_TRAVEL_FLAG,'N') AS GROUP_TRAVEL_FLAG,dbo.FN_GET_EXPENDITURE('"
                        + strRequistionId
                        + "','I') AS Expenditure,dbo.sitename(t.BILLING_SITE) as BILLING_SITE,REASON_FOR_TRAVEL,BILLING_SITE as BILLING_SITE_ID ,BILLING_CLIENT,T.SITE_ID SITE_ID,T.C_USERID AS USER_ID,TA.APPROVER_ID,T.TRAVELLER_ID, (select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = T.SITE_ID) as TRAVEL_AGENCY_ID, (select MAIL_TO_MATA from M_SITE where SITE_ID = T.SITE_ID) as MAIL_TO_MATA FROM T_TRAVEL_DETAIL_INT T,T_APPROVERS TA WHERE T.TRAVEL_ID=TA.TRAVEL_ID AND TA.APPROVE_STATUS=0 AND TA.ORDER_ID=DBO.MINREQORDERNO("
                        + strRequistionId + ") AND T.TRAVEL_ID=" + strRequistionId + " AND TRAVEL_TYPE='I' AND T.STATUS_ID=10 ";
            }

            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy H:mm");
            strCurrentDate = (sdf.format(currentDate)).trim();
            strCurrentYear = strCurrentDate.split(" ")[2];

            Map<String, Object> requestDataMap = getFirstResult(sSqlStr);

            if (requestDataMap != null)
            {
                strRequistionNumber = AppUtility.checkNullString(requestDataMap.get("RNO"));
                strRequistionCreatorName = AppUtility.checkNullString(requestDataMap.get("NEXTAPPROVER"));
                strRequistionCreatorMail = AppUtility.checkNullString(requestDataMap.get("APPROVEREMAIL"));
                strstrRequistionApproverName = AppUtility.checkNullString(requestDataMap.get("CREATOR"));
                strstrRequistionApproverEmail = AppUtility.checkNullString(requestDataMap.get("CREATOREMAIL"));
                String strRequistionCreatedDate1 = AppUtility.checkNullString(requestDataMap.get("DATETIME"));

                strCCMailCreate = strstrRequistionApproverEmail;

                // Add the substring Method for time format
                String str = strRequistionCreatedDate1.substring(0, 17);
                String str1 = strRequistionCreatedDate1.substring(0, 11);
                strRequistionCreatedDate = str;
                strRequistionCreatedDatewithoutPmAm = str1;
                // End Modification

                strUserNm = AppUtility.checkNullString(requestDataMap.get("TRAVELLER_NAME"));
                strTravellerSex = AppUtility.checkNullString(requestDataMap.get("SEX"));
                strSiteName = AppUtility.checkNullString(requestDataMap.get("SITE_NAME"));
                strTravelAgencyTypeId = AppUtility.checkNullString(requestDataMap.get("TRAVEL_AGENCY_ID"));
                strMailToMATA = AppUtility.checkNullString(requestDataMap.get("MAIL_TO_MATA"));

                // get mail to person language preference
                strSql = "SELECT LANGUAGE_PREF FROM M_USERINFO WHERE EMAIL =N'" + strRequistionCreatorMail + "' AND STATUS_ID=10";

                strsesLanguage = jdbcTemplate.queryForObject(strSql, new Object[] {}, String.class);
                strsesLanguage = strsesLanguage == null ? "en_US" : strsesLanguage;

                strSex = (strTravellerSex != null && strTravellerSex.equals("1")) ? "\"Mr.\"" : "Ms.";

                if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
                {
                    strSql = "select * FROM  [dbo].[FN_GetDeparturecity](" + strRequistionId + ",'d')";
                    strGroupTravelFlag = AppUtility.checkNullString(requestDataMap.get(requestDataMap.get("GROUP_TRAVEL_FLAG")));

                    if (strGroupTravelFlag == null)
                    {
                        strGroupTravel = "";
                    }

                    if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                    {
                        strGroupTravel = appMessageUtility.getLabel("msg.email.groupguestisscheduletotravel");
                        if (strTravelAgencyTypeId.equals("2"))
                        {
                            strGroupTravel = appMessageUtility.getLabel("label.mail.guestisscheduledtotravel");
                        }
                    } else
                    {
                        strGroupTravel = strSex + " " + " <b>" + strUserNm + "</b> " + appMessageUtility.getLabel("label.mail.isscheduledtotravel");
                    }

                    strSqlSql = "SELECT CONVERT(VARCHAR(11), TRAVEL_DATE, 113) as TRAVEL_RETURN_DATE FROM " + " T_RET_JOURNEY_DETAILS_DOM WHERE  (TRAVEL_ID = " + strRequistionId
                            + ") and  (RETURN_FROM <> '') AND (RETURN_TO <> '')";

                } else
                {
                    strSql = "select * FROM  [dbo].[FN_GetDeparturecity](" + strRequistionId + ",'i')";
                    strGroupTravelFlag = AppUtility.checkNullString("GROUP_TRAVEL_FLAG");

                    if (strGroupTravelFlag == null)
                    {
                        strGroupTravel = "";
                    }

                    if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                    {
                        strGroupTravel = appMessageUtility.getLabel("label.mail.groupguestisscheduledtotravel");
                        if (strTravelAgencyTypeId.equals("2"))
                        {
                            strGroupTravel = appMessageUtility.getLabel("label.mail.guestisscheduledtotravel");
                        }
                    } else
                    {
                        strGroupTravel = strSex + " " + " <b>" + strUserNm + "</b> " + appMessageUtility.getLabel("label.mail.isscheduledtotravel");
                    }
                    	
                    //Removed query to get travel return date
                    
                }

                //Remove text,strBillingSite,strbillingSiteid as it was not used anywhere
                strreasonFortravel = AppUtility.checkNullString((requestDataMap.get("REASON_FOR_TRAVEL")));
                String strSiteID = AppUtility.checkNullString((requestDataMap.get("SITE_ID")));

                //Removed if clause with case of self msg,and case of outside msg , as it was not used
                
                //Removed code to get travel from and travel to as it was not used by Jatin on 23-11-2018
                
                // Removed code to find return date from Query as it was not used.

                if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
                {
                    strSql = "select dbo.USEREMAIL(ORIGINAL_APPROVER) EMAIL from t_approvers where (TRAVEL_ID = " + strRequistionId + ") and travel_type='d' and ORIGINAL_APPROVER<>'0'";

                } else
                {
                    strSql = "select dbo.USEREMAIL(ORIGINAL_APPROVER) EMAIL from t_approvers where (TRAVEL_ID = " + strRequistionId + ") and travel_type='i' and ORIGINAL_APPROVER<>'0'";
                }

                Map<String, Object> orginApprMailIdMap = getFirstResult(strSql);

                if (orginApprMailIdMap != null)
                {
                    strMailIdOrignalApprover = AppUtility.checkNullString(orginApprMailIdMap.get(Constants.EMAIL));// Mail ID of CC mails
                }

                // sending mail on origination for intimation for SMR FRANCE,UK,Spain added on
                // 02-Nov-2009
                if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
                {
                    sSqlStr = "SELECT isnull(dbo.Find_email(MAIL_TO),'') as MAIL_TO  FROM  MAIL_AT_STAGES	WHERE site_id=" + strSiteID + " and (MAIL_STAGE = 'submit')";

                    String strMailtoCCon = "";

                    Map<String, Object> mailTo = getLastResult(sSqlStr);

                    if (mailTo != null)
                    {

                        strMailtoCCon = AppUtility.checkNullString(mailTo.get("MAIL_TO"));
                    }

                    if (strMailtoCCon == null)
                    {
                        strMailtoCCon = "";
                    }

                    strMailIdOrignalApprover = strMailIdOrignalApprover + strMailtoCCon;
                }

                strUserId = AppUtility.checkNullString(requestDataMap.get("USER_ID"));
                String strTravellerId = AppUtility.checkNullString(requestDataMap.get("TRAVELLER_ID"));

                String reqTyp1 = "";
                if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
                {
                    reqTyp1 = "label.mail.domestictravel";
                    if (strTravelAgencyTypeId.equals("2"))
                    {
                        reqTyp1 = "label.global.domesticeuropetravelrequest";
                    }
                } else
                {
                    reqTyp1 = "label.mail.internationaltravel";
                    if (strTravelAgencyTypeId.equals("2"))
                    {
                        reqTyp1 = "label.global.interconttravelrequest";
                    }
                }

                // get mail id of report_to and mail_cc person
                String strQuery = "SELECT MDA.MATA_CC_MAIL CC_MAIL" + " FROM M_DEFAULT_APPROVERS AS MDA  WHERE MDA.APPLICATION_ID = 1 AND MDA.STATUS_ID = 10 AND MDA.SITE_ID = '" + strSiteID
                        + "' AND MDA.TRV_TYPE = '" + strType + "'" + " AND SP_ROLE =(select sp_role from M_userinfo where   userid='" + strTravellerId + "' and loginstatus in ('ACTIVE','ENABLE')) "
                        + " AND MDA.ORDER_ID=(select min(order_id) from M_DEFAULT_APPROVERS MA where MA.site_id='" + strSiteID + "' and MA.STATUS_ID=10 and TRV_TYPE='" + strType + "' "
                        + " AND SP_ROLE =(select sp_role from M_userinfo where   userid='" + strTravellerId + "' and loginstatus in ('ACTIVE','ENABLE')) )" + " UNION"
                        + " select isnull((select m.EMAIL from m_userinfo um inner join m_userinfo m on um.report_to = m.userid inner join m_site s on s.site_id = um.site_id where um.userid='"
                        + strTravellerId + "' and um.status_id=10 and m.status_id=10 and  MAIL_TO_REPORTING ='Y' and um.userid <> 504 ),'') as MATA_CC_MAIL";

                List<Map<String, Object>> mataCCMailList = null;

                mataCCMailList = jdbcTemplate.queryForList(strQuery);

                // get value of report_to person mail and mail_cc from two rows
                String strCCMailOnInitial = "";
                if (mataCCMailList != null)
                {
                    for (Map<String, Object> tempMap : mataCCMailList)
                    {

                        String strCCMailOnInitialTemp = AppUtility.checkNullString(tempMap.get("CC_MAIL"));
                        if (!strCCMailOnInitialTemp.equals("") && strCCMailOnInitial.equals(""))
                        {
                            strCCMailOnInitial = strCCMailOnInitialTemp + strCCMailOnInitial;
                        } else if (!strCCMailOnInitialTemp.equals("") && !strCCMailOnInitial.equals(""))
                        {
                            strCCMailOnInitial = strCCMailOnInitialTemp + ";" + strCCMailOnInitial;
                        }
                    }
                }

                List<Map<String, Object>> strCCMailOnInitialTempList = getCCMailListOnIntialTempList(strSiteID, strTravellerId, strType);

                if (strCCMailOnInitialTempList != null)
                {
                    for (Map<String, Object> tempMap : strCCMailOnInitialTempList)
                    {

                        String strCCMailOnInitialTemp = AppUtility.checkNullString(tempMap.get("CC_MAIL"));

                        if (!strCCMailOnInitialTemp.equals("") && strCCMailOnInitial.equals(""))
                        {
                            strCCMailOnInitial = strCCMailOnInitialTemp + strCCMailOnInitial;

                        } else if (!strCCMailOnInitialTemp.equals("") && !strCCMailOnInitial.equals(""))
                        {
                            strCCMailOnInitial = strCCMailOnInitialTemp + ";" + strCCMailOnInitial;
                        }
                    }
                }

                if (!strCCMailOnInitial.trim().equalsIgnoreCase(""))
                {
                    strMailIdOrignalApprover = strMailIdOrignalApprover + ";" + strCCMailOnInitial;
                }

                String strTravellerMail = "";
                if (!strTravellerId.equals(strUserId))
                {
                    String strQuery1 = "SELECT DBO.USEREMAIL(MUI.USERID) AS TRAVELLEREMAIL FROM M_USERINFO MUI WHERE MUI.USERID=" + strTravellerId + " AND MUI.STATUS_ID=10 AND MUI.APPLICATION_ID=1";

                    Map<String, Object> travelEmailApp = getFirstResult(strQuery1);

                    if (travelEmailApp != null)
                    {
                        strTravellerMail = AppUtility.checkNullString(travelEmailApp.get("TRAVELLEREMAIL"));
                    }

                    if (!"".equals(strTravellerMail))
                    {
                        strMailIdOrignalApprover = strMailIdOrignalApprover + ";" + strTravellerMail;
                        strCCMailCreate = strCCMailCreate + ";" + strTravellerMail;
                    }
                }

                if (strMailIdOrignalApprover.length() > 0 && strMailIdOrignalApprover.charAt(0) == ';')
                {
                    strMailIdOrignalApprover = strMailIdOrignalApprover.substring(1);
                }

                String strSqlQuery = "SELECT DBO.[FN_FIND_ACTIVE_CC_EMAIL]('" + strMailIdOrignalApprover + "') as CC_USER_EMAIL;";
                String strActiveCCUserMail = "";

                Map<String, Object> ccUserEmailMap = getFirstResult(strSqlQuery);

                if (ccUserEmailMap != null)
                {
                    strActiveCCUserMail = AppUtility.checkNullString(ccUserEmailMap.get("CC_USER_EMAIL"));

                }

                String groupGuestLabel = "";
                if (strTravelAgencyTypeId.equals("2"))
                {
                    groupGuestLabel = "label.global.guest";
                } else
                {
                    groupGuestLabel = "label.approverequest.groupguest";
                }

                // change the mailSubject
                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {
		    strMailSubject = appMessageUtility.getLabel(Constants.LABEL_MAIL_STARSNOTIFICATION) + " "
			    + appMessageUtility.getLabel(reqTyp1) + " "
			    + appMessageUtility.getLabel(Constants.LABEL_MAIL_REQUISITIONNO) + "'"
			    + strRequistionNumber.trim() + "' " + appMessageUtility.getLabel(groupGuestLabel) + " "
			    + appMessageUtility.getLabel("label.mainmenu.pendingforapproval") + " ";// Mail  Subject
                } else
                {
		    strMailSubject = appMessageUtility.getLabel(Constants.LABEL_MAIL_STARSNOTIFICATION) + " "
			    + appMessageUtility.getLabel(reqTyp1) + " "
			    + appMessageUtility.getLabel(Constants.LABEL_MAIL_REQUISITIONNO) + "'"
			    + strRequistionNumber.trim() + "' "
			    + appMessageUtility.getLabel("label.mainmenu.pendingforapproval") + " ";// Mail  Subject
                }

                /* Show Advance Required Info Block Start */

                if (!strTravelAgencyTypeId.equals("2") && "D".equalsIgnoreCase(strType) && "86".equals(strSiteID))
                {
                    String unitHeadEmail = "";

                    log.info("sendRequisitionMailOnOriginating [Pending for Approval] Start of get Unit Head of Site block");

                    Map<String, Object> unitHeadEmailMap = getFirstResult("SELECT ISNULL(EMAIL,'') AS EMAIL FROM M_USERINFO WHERE STATUS_ID=10 AND UNIT_HEAD='1' AND SITE_ID='" + strSiteID + "';");

                    if (unitHeadEmailMap != null)
                    {
                        unitHeadEmail = AppUtility.checkNullString(unitHeadEmailMap.get(Constants.EMAIL));
                    }
                    log.info("sendRequisitionMailOnOriginating [Pending for Approval] End of get Unit Head of Site block");

                    /* If unit head is traveler / originator / in "TO" */
                    if ((strRequistionCreatorMail != null && strRequistionCreatorMail.contains(unitHeadEmail)) || unitHeadEmail.equals(strstrRequistionApproverEmail)
                            || unitHeadEmail.equals(strTravellerMail))
                    {
                        strActiveCCUserMail = getActiveCCUserEmail(strActiveCCUserMail, strSiteID);
                        pushStarsRequestDetailsToERPMata(strTravelId, strType, sUserId);
                    }
                }
                // ===========================//
                try
                {

                    String strSSOUrl = sSSOUrlByMailid(strRequistionCreatorMail);

                    strMailMsg.append(
                            "<html><style>.formhead{ font-family:Arial;font-size:11px;font-style: normal;font-weight:normal;color:#000000;text-decoration:none;letter-spacing:normal;word-spacing:normal;border:1px #333333 solid;background:#E2E4D6;}</style><body bgcolor=\"#d0d0d0\">"
                                    + "\n"
                    );
                    strMailMsg.append(
                            " <table style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%' width=80% border=0 cellspacing=0 cellpadding=0 align=center><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center>"
                                    + "\n"
                    );
                    strMailMsg.append(
                            "</td></tr><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=10 style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%'><tr><td align='left' style='background:#aa1220;color:#fff;font-size:28px;padding:12px 12px 0;'><strong>"
                                    + appMessageUtility.getLabel("label.mail.starsmailnotification") + "</strong></td>"
                    ).append(
                            "<td  align='right' style='background:#aa1220;color:#fff;font-size:13px;padding:12px 12px 0;'>[" + strRequistionCreatedDatewithoutPmAm + "]<br/>" + appMessageUtility
                                    .getLabel("label.mail.mailreferencenumber") + Constants.HTML_NBSP + strMailRefNumber + "/" + strCurrentYear
                                    + "</td></tr></table></td></tr><tr><td bgcolor=#000000> </td></tr><tr>" + "\n"
                    );
                    strMailMsg.append(
                            "<td bgcolor=#B6DCDC></td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#FFFFFF><table width=100% border=0 cellspacing=0 cellpadding=5>"
                                    + "\n"
                    );
                    strMailMsg
                            .append("  <tr><td align=left style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:15px'>" + appMessageUtility.getLabel(reqTyp1) + " " + appMessageUtility.getLabel("label.mail.requisition") + Constants.HTML_NBSP + "" + " : [" + strRequistionNumber.trim() + "]&nbsp;|&nbsp;[" + "" + " " + strRequistionCreatedDate + "]</td>").append("<td  align=right style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:13px'>" + appMessageUtility.getLabel("label.mail.forinternalcirculationonly") + "</td></tr><tr><td colspan=2><p><font color=#FFFFFF></font><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + "\n");
                    strMailMsg.append(
                            appMessageUtility.getLabel("label.mail.dear") + " " + strRequistionCreatorName + ",</font></p><p><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + appMessageUtility.getLabel("label.global.requisitionnumber") + " " + strRequistionNumber.trim() + " " + appMessageUtility.getLabel("label.mail.hasbeenoriginatedby") + " " + strstrRequistionApproverName + " " + appMessageUtility.getLabel("label.approverrequest.from") + " " + strSiteName + " " + appMessageUtility.getLabel("label.mail.on") + " " + strRequistionCreatedDate + " " + "</p>" + "\n"
                    );
                    strMailMsg.append(
                            "<u><font size=2 face=Verdana, Arial, Helvetica, sans-serif color=blue>" + appMessageUtility.getLabel("label.mail.requisitionsdetails") + "</u> :- </font><font size=2 face=Verdana, Arial, Helvetica, sans-serif >" + strGroupTravel + "   </font>\n<br>"
                    );
                    strMailMsg.append(
                            "<u><font size=2 face=Verdana, Arial, Helvetica, sans-serif color=blue>" + appMessageUtility.getLabel("label.global.reasonfortravel") + " </u>:- </font><font size=2 face=Verdana, Arial, Helvetica, sans-serif >" + strreasonFortravel + "</font>\n<br>"
                    );

                    if ((strTravelAgencyTypeId != null && strTravelAgencyTypeId.equals("2")))
                    {
                        strMailMsg.append(customizeApproverMailBodyGmbh(strRequistionId, strType));
                    } else
                    {
                        strMailMsg.append(customizeApproverMailBodyIndia(strRequistionId, strType, strGroupTravelFlag));
                    }

                    strMailMsg.append(
                            "</font><br><br><font size=2 face=Verdana, Arial, Helvetica, sans-serif><a href='" + strSSOUrl + "'>"
                                    + appMessageUtility.getLabel("label.mail.pleaseclickheretologintostars") + " </a> " + "\n"
                    );
                    strMailMsg.append(
                            "   </b></font><br><tr><td><p><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + appMessageUtility.getLabel("label.mail.bestregards") + "</font><br><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + strstrRequistionApproverName.trim() + "<br></font></p></td>"
                    );
                    strMailMsg.append(
                            "<td  align=right style='padding:0 12px 12px;font-weight:300;font-size: 13px;'>" + appMessageUtility.getLabel("label.mail.starsadministratorcanbecontacted") + "<br/><a href=mailto:administrator.stars@mind-infotech.com>administrator.stars@mind-infotech.com</a></td></tr></td></tr></table></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=5>" + "\n"
                    );
                    strMailMsg.append(
                            "<tr><td  align=left style='padding:12px;font-weight:300;background:#e7e8e9;color:#6d6e71;font-size:11px;font-family:Calibri;'> <strong>"
                                    + appMessageUtility.getLabel("label.mail.disclaimer") + "</strong> : " + appMessageUtility.getLabel("label.mail.thiscommunicationissystemgenererated") + " " + "\n"
                    );
                    strMailMsg.append(
                            appMessageUtility.getLabel("label.mail.pleasedonnotreplytothismail") + " <br>" + appMessageUtility.getLabel("label.mail.ifyouarenotthecorrectrecipientforthisnotification") + "<br><br>&copy;" + appMessageUtility.getLabel("label.mail.mindallrightsreserved") + "</td></tr></table></td></tr><tr><td bgcolor=#000000></td></tr>" + "\n"
                    );
                    strMailMsg.append("</table><p>&nbsp;</p></body></html>" + "\n");

                    log.info("sendRequisitionMailOnOriginating [Pending for Approval] try block Start for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                    pushEmailDataToTable(
                            strRequistionId, strRequistionNumber, strRequistionCreatorMail, strstrRequistionApproverEmail, strActiveCCUserMail, strMailSubject, strMailMsg.toString(), sUserId,
                            Constants.USER_SUBMITTING_THE_REQ
                    );

                    log.info("sendRequisitionMailOnOriginating [Pending for Approval] try block End for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                    String approverId = "";
                    String approver = "";
                    strSql = "SELECT TA.APPROVER_ID, dbo.user_name(TA.APPROVER_ID) as APPROVER, DBO.USEREMAIL(TA.APPROVER_ID) AS ApproverEmail"
                            + " ,dbo.user_name(TA.PAP_APPROVER) as reviewer, DBO.USEREMAIL(TA.PAP_APPROVER) AS reviewerEmail, pap.IS_MAIL_FWD" + " FROM T_APPROVERS TA inner join"
                            + " VW_PAGE_ACCESS_PERMISSION PAP on pap.viewToUser=TA.PAP_APPROVER and pap.pendingWithUser = TA.APPROVER_ID" + " WHERE TA.TRAVEL_ID='" + strRequistionId + "' "
                            + " AND TA.ORDER_ID=(SELECT MIN(ORDER_ID) FROM T_APPROVERS WHERE T_APPROVERS.TRAVEL_ID='" + strRequistionId + "' AND T_APPROVERS.SITE_ID='" + strSiteID
                            + "' AND T_APPROVERS.TRAVEL_TYPE='" + strType + "' AND T_APPROVERS.APPROVE_STATUS=0) " + " AND TA.SITE_ID='" + strSiteID + "' " + " AND TA.TRAVEL_TYPE='" + strType + "'";

                    Map<String, Object> mapForApproverDetails = null;
                    mapForApproverDetails = getFirstResult(strSql);

                    if (mapForApproverDetails != null)
                    {
                        approverId = AppUtility.getResultFromMap(mapForApproverDetails, ParamConstant.APPROVER_ID);
                        approver = AppUtility.getResultFromMap(mapForApproverDetails, ParamConstant.APPROVER);
                        strApproverEmail = AppUtility.getResultFromMap(mapForApproverDetails, "ApproverEmail");
                        strReviewer = AppUtility.getResultFromMap(mapForApproverDetails, "reviewer");
                        strReviewerEmail = AppUtility.getResultFromMap(mapForApproverDetails, "ReviewerEmail");
                        strIsmailFwd = AppUtility.getResultFromMap(mapForApproverDetails, "is_mail_fwd");
                    }

                    // get mail to person language preference
                    strSql = "SELECT LANGUAGE_PREF FROM M_USERINFO WHERE EMAIL =N'" + strReviewerEmail + "' AND STATUS_ID=10";
                    Map<String, Object> languagePrefMap = getFirstResult(strSql);
                    if (!AppUtility.checkNull(languagePrefMap))
                    {
                        strsesLanguage = AppUtility.getResultFromMap(languagePrefMap, ParamConstant.LANGUAGE_PREF);
                        if (strsesLanguage == null || strsesLanguage.equals(""))
                        {
                            strsesLanguage = "en_US";
                        }
                    }

                    strMailSubject1 = appMessageUtility.getLabel(Constants.LABEL_MAIL_STARSNOTIFICATION) + " " + appMessageUtility.getLabel(reqTyp1) + " "
                            + appMessageUtility.getLabel(Constants.LABEL_MAIL_REQUISITIONNO) + "'" + strRequistionNumber.trim() + "'  " + appMessageUtility.getLabel("label.mail.forreview") + " ";

                    if (strIsmailFwd != null && strIsmailFwd.trim().equalsIgnoreCase("Y"))
                    {

                        strMailMsg1.append(
                                "<html><style>.formhead {	font-family: Arial;	font-size: 11px;	font-style: normal;	font-weight: normal;	color: #000000;"
                        ).append("border: 1px #333333 solid;	background: #00CCFF;}</style>")
                                .append("<body bgcolor=#00CCFF><table width=80% align=center><tr><td align=right><font size=2><b><font color=#FFFFFF>" + strRequistionCreatedDatewithoutPmAm + "</font>").append("<font color=#FFFFFF></font><font color=#000000><br></font></b><font color=#FFFFFF>" + appMessageUtility.getLabel("label.mail.forinternalcirculationonly") + "</font></font></td>").append("</tr><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=9></table>").append("</td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=15><tr><td align=center bgcolor=#aa1220>").append("<font size=5 color=#FFFFFF>" + appMessageUtility.getLabel("label.mail.starsmailnotification") + "</font></td></tr></table>").append("</td></tr><tr><td bgcolor=#FFFFFF><table width=100% border=0 cellspacing=0 cellpadding=5><tr><td align=center bgcolor=#aa1220><font size=2><b>").append("<font	color=#FFFFFF><font size=3>" + appMessageUtility.getLabel("label.mail.forreview") + "</font> </font></b></font></td></tr><tr><td bgcolor=#878787><font size=2></font></td></tr><tr><td><p>").append("<font color=#FFFFFF>.</font><font size=2 face=Verdana, Arial, Helvetica, sans-serif><br>" + appMessageUtility.getLabel("label.mail.dear") + " " + strReviewer + ",</font></p><p><font size=2 face=Verdana, Arial, Helvetica, sans-serif> " + appMessageUtility.getLabel("mail.label.kindlyreviewtherequest") + " " + strRequistionNumber.trim() + " " + appMessageUtility.getLabel("mail.label.pendingwithmeandtakeappropriateaction")).append("<br><br><font color=blue></p></font> </font><font size=2></font></p><font size=2 face=Verdana, Arial, Helvetica, sans-serif><a href='https://stars.mindeservices.com'>" + appMessageUtility.getLabel("label.mail.pleaseclickheretologintostars") + " </a> </b></form>").append("</font><br>	<p><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + appMessageUtility.getLabel("label.mail.bestregards") + "</font><br><font size=2 face=Verdana, Arial, Helvetica, sans-serif>" + approver + "<br></font></p></td></tr></table></td></tr><tr><td bgcolor=#878787 align=center>").append("<font face=Verdana,Arial, Helvetica, sans-serif size=1><b>" + appMessageUtility.getLabel("label.mail.mailreferencenumber") + " </b>" + intMaliRefNumber + "/" + strCurrentYear + "</font></td></tr>").append("<tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=5><tr>").append("<td align=center bgcolor=#aa1220><font size=2 face=Verdana, Arial, Helvetica, sans-serif><b><font size=1>" + appMessageUtility.getLabel("label.mail.starsadministratorcanbecontacted") + " : - </font></b><font size=1><font size=2 face=Verdana, Arial, Helvetica, sans-serif color=#000000> <a href=mailto:administrator.stars@mind-infotech.com><font size=1 color=#ffffff>administrator.stars@mind-infotech.com</font></a></font></font></font></td>").append("</tr><tr><td  align=center bgcolor=#878787 height=15> <b><font size=1 face=Verdana, Arial, Helvetica, sans-serif>" + appMessageUtility.getLabel("label.mail.disclaimer") + "</b> : " + appMessageUtility.getLabel("label.mail.thiscommunicationissystemgenererated")).append("  " + appMessageUtility.getLabel("label.mail.pleasedonnotreplytothismail")).append("<br>").append(appMessageUtility.getLabel("label.mail.ifyouarenotthecorrectrecipientforthisnotification") + "&nbsp;").append("</font>").append(Constants.HTML_TD_C);
                        strMailMsg1.append(
                                "</tr></table></td></tr><tr><td bgcolor=#000000></td></tr><tr><td align=center><font size=2 face=Verdana, Arial, Helvetica, sans-serif><font size=1 color=#000000>&copy;"
                                        + appMessageUtility.getLabel("label.mail.mindallrightsreserved") + "</font></font></td></tr></table><p>&nbsp;</p></body></html>"
                        );

                        log.info("Mail in the center " + strMailMsg1.toString());

                        log.info("sendRequisitionMailOnOriginating [For Review] try block Start for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());
                        pushEmailDataToTable("", strRequistionNumber, strReviewerEmail, strApproverEmail, "", strMailSubject1, strMailMsg1.toString(), approverId, Constants.USER_SUBMITTING_THE_REQ);
                        log.info("sendRequisitionMailOnOriginating [For Review] try block End for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                    }

                    log.info("sendRequisitionMailOnOriginating [Travel Agency Type Id] " + strTravelAgencyTypeId + " for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                    if (strTravelAgencyTypeId != null && !"".equals(strTravelAgencyTypeId) && !"0".equals(strTravelAgencyTypeId) && "Y".equals(strMailToMATA)
                            && !isAdvanceTravelRequest(strTravelId, strType, strTravelAgencyTypeId))
                    {

                        log.info("sendRequisitionMailOnOriginating [Send Mail to TRAVEL MATA GmbH/India] Start for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                        String strMataGmbHMailContent = "";
                        String functionKey = "";
                        String adminIDFlag = "";
                        if (strTravelAgencyTypeId.equals("1"))
                        {

                            functionKey = "@MataindiaSite";
                            adminIDFlag = "AND M_USERINFO.USERID=1129";

                            if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                            {
                                strMataGmbHMailContent = getGroupMailBodyForMataIndia(strRequistionId, strType, strTravellerId);
                            } else if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("N")))
                            {
                                strMataGmbHMailContent = getMailBodyForMataIndia(strRequistionId, strType, strTravellerId);
                            } else
                            {
                                strMataGmbHMailContent = getMailBodyForMataIndia(strRequistionId, strType, strTravellerId);
                            }
                           // System.out.println("mail contgent" + strMataGmbHMailContent);
                            //log.info("content mail" + strMataGmbHMailContent);

                        } else if (strTravelAgencyTypeId.equals("2"))
                        {

                            functionKey = "@MataGmbHSite";
                            /**
                             * 3rd if(strGroupTravelFlag!=null && (strGroupTravelFlag.trim().equals("Y"))) {
                             * strMataGmbHMailContent = new
                             * T_QuicktravelRequestDaoImpl().getGroupMailBodyForMataGmbH(strRequistionId,
                             * strType, strTravellerId); } else if(strGroupTravelFlag!=null &&
                             * (strGroupTravelFlag.trim().equals("N"))){ strMataGmbHMailContent = new
                             * T_QuicktravelRequestDaoImpl().getMailBodyForMataGmbH(strRequistionId,
                             * strType, strTravellerId); }
                             */
                        }

                        String firstDepartureDate = "";
                        String strSqlDepartureDate = "";
                        if ("I".equals(strType) || "INT".equals(strType))
                        {
                            strSqlDepartureDate = "SELECT CONVERT(VARCHAR(10),MIN(TRAVEL_DATE),103) AS FIRST_JOURNY_DATE FROM T_JOURNEY_DETAILS_INT WHERE TRAVEL_ID=" + strRequistionId;
                        } else if ("D".equals(strType) || "DOM".equals(strType))
                        {
                            strSqlDepartureDate = "SELECT CONVERT(VARCHAR(10),MIN(TRAVEL_DATE),103) AS FIRST_JOURNY_DATE FROM T_JOURNEY_DETAILS_DOM WHERE TRAVEL_ID=" + strRequistionId;
                        }

                        Map<String, Object> firstJourneyMap = getFirstResult(strSqlDepartureDate);
                        if (firstJourneyMap != null)
                        {
                            firstDepartureDate = AppUtility.getResultFromMap(firstJourneyMap, "FIRST_JOURNY_DATE");
                        }

                        String emailIdsMataSql = "";
                        if ("I".equals(strType) || "INT".equals(strType))
                        {

                            emailIdsMataSql = " select DBO.USEREMAIL(APPROVER_ID) MATAGMBH from T_TRAVEL_DETAIL_INT TDI " + " INNER JOIN T_APPROVERS TA ON TDI.TRAVEL_ID=TA.TRAVEL_ID AND "
                                    + " TA.TRAVEL_TYPE='" + strType + "' WHERE TDI.TRAVEL_ID=" + strRequistionId + " AND TA.ROLE='MATA' UNION "
                                    + " SELECT TRAVEL_MATA_EMAIL FROM M_SITE WHERE SITE_ID IN (SELECT SITE_ID FROM T_TRAVEL_DETAIL_INT WHERE TRAVEL_ID=" + strRequistionId + ") AND STATUS_ID=10 UNION "
                                    + " SELECT DBO.USEREMAIL(USERID) MATAGMBH FROM T_TRAVEL_DETAIL_INT TDI " + " INNER JOIN  USER_MULTIPLE_ACCESS UMA ON TDI.SITE_ID=UMA.SITE_ID "
                                    + " WHERE TDI.TRAVEL_ID=" + strRequistionId + " AND UMA.TRAVEL_COORDINATOR =1 AND UMA.STATUS_ID=10";

                        } else if ("D".equals(strType) || "DOM".equals(strType))
                        {

                            emailIdsMataSql = " select DBO.USEREMAIL(APPROVER_ID) MATAGMBH from T_TRAVEL_DETAIL_DOM TDD " + " INNER JOIN T_APPROVERS TA ON TDD.TRAVEL_ID=TA.TRAVEL_ID AND "
                                    + " TA.TRAVEL_TYPE='" + strType + "' WHERE TDD.TRAVEL_ID=" + strRequistionId + " AND TA.ROLE='MATA' UNION "
                                    + " SELECT TRAVEL_MATA_EMAIL FROM M_SITE WHERE SITE_ID IN (SELECT SITE_ID FROM T_TRAVEL_DETAIL_DOM WHERE TRAVEL_ID=" + strRequistionId + ") AND STATUS_ID=10 UNION "
                                    + " SELECT DBO.USEREMAIL(USERID) MATAGMBH FROM T_TRAVEL_DETAIL_DOM TDD " + " INNER JOIN  USER_MULTIPLE_ACCESS UMA ON TDD.SITE_ID=UMA.SITE_ID "
                                    + " WHERE TDD.TRAVEL_ID=" + strRequistionId + " AND UMA.TRAVEL_COORDINATOR =1 AND UMA.STATUS_ID=10";

                        }

                        ArrayList<String> emailIdsList = new ArrayList<>();
                        String emailId = "";
                        String emailIds = "";

                        List<Map<String, Object>> emailList = jdbcTemplate.queryForList(emailIdsMataSql);

                        for (Map<String, Object> tempMap : emailList)
                        {
                            emailId = AppUtility.getResultFromMap(tempMap, "MATAGMBH");
                            emailIdsList.add(emailId);
                        }

                        StringBuilder sb = new StringBuilder();
                        int size = emailIdsList.size();
                        if (size > 0)
                        {
                            sb.append(emailIdsList.get(0));
                            for (int i = 1; i < size; ++i)
                            {
                                sb.append("; ").append(emailIdsList.get(i));
                            }

                            emailIds = sb.toString();
                        }

                        String resubmitStatus = "";
                        String resubmitStatusQuery = "";

                        if (("I".equals(strType) || "INT".equals(strType)) || ("D".equals(strType) || "DOM".equals(strType)))
                        {
                            resubmitStatusQuery = "SELECT CASE WHEN EXISTS(SELECT 1 FROM AUDIT_T_TRAVEL_STATUS WHERE TRAVEL_ID= '" + strRequistionId + "'AND TRAVEL_TYPE='" + strType
                                    + "' AND TRAVEL_STATUS_ID=3 AND STATUS_ID=10)" + " THEN 'Y' ELSE 'N' END AS RESUBMIT_STATUS";
                        }

                        List<Map<String, Object>> resubmitStatusRs = jdbcTemplate.queryForList(resubmitStatusQuery);
                        for (Map<String, Object> tempMap : resubmitStatusRs)
                        {
                            resubmitStatusQuery = AppUtility.getResultFromMap(tempMap, "RESUBMIT_STATUS");
                            if (resubmitStatusQuery.equalsIgnoreCase("Y"))
                                resubmitStatus = " - Resubmitted";
                        }

                        String strSqlTravellerName = "";
                        String strGrpTravellerName = "";
                        int grpTravllerCount = 0;

                        if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                        {
                            strSqlTravellerName = "SELECT TOP 1 LTRIM(RTRIM(FIRST_NAME)) +' '+ LTRIM(RTRIM(ISNULL(LAST_NAME,''))) AS TRAVELLER, COUNT(*) OVER (partition by 1) TRAVELLERCOUNT FROM T_GROUP_USERINFO WHERE TRAVEL_ID="
                                    + strRequistionId + " AND SITE_ID=" + strSiteID + " AND TRAVEL_TYPE='" + strType + "' AND STATUS_ID=10 ORDER BY G_USERID";

                            Map<String, Object> travllerMap = getFirstResult(strSqlTravellerName);
                            if (travllerMap != null)
                            {
                                strGrpTravellerName = AppUtility.getResultFromMap(travllerMap, ParamConstant.TRAVELLER);
                                grpTravllerCount = AppUtility.convertStringToInt(AppUtility.getResultFromMap(travllerMap, "TRAVELLERCOUNT"));
                            }

                        }

                        String strFromMail = "";
                        String strMataGmbhSubject = "";

                        if (strTravelAgencyTypeId.equals("1"))
                        {
                            strFromMail = "administrator.stars@mind-infotech.com";

                            if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                            {
                                strMataGmbhSubject = "STARS - Pending" + resubmitStatus + " - [" + strGrpTravellerName + "+" + grpTravllerCount + "] - [" + firstDepartureDate + "] ["
                                        + strRequistionNumber + "] [" + appMessageUtility.getLabel(groupGuestLabel) + "]";
                            } else
                            {
                                strMataGmbhSubject = "STARS - Pending" + resubmitStatus + " - [" + strUserNm + "] - [" + firstDepartureDate + "] [" + strRequistionNumber + "]";
                            }
                        } else if (strTravelAgencyTypeId.equals("2"))
                        {
                            strFromMail = strstrRequistionApproverEmail;

                            if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                            {
                                strMataGmbhSubject = "STARS - " + "[" + strUserNm + "] - [" + AppUtility.ddmmyyyyStrToddmmmyyyy(firstDepartureDate) + "] [" + strRequistionNumber + "] ["
                                        + appMessageUtility.getLabel(groupGuestLabel) + "] - Pending" + resubmitStatus;
                            } else
                            {
                                strMataGmbhSubject = "STARS - " + "[" + strUserNm + "] - [" + AppUtility.ddmmyyyyStrToddmmmyyyy(firstDepartureDate) + "] [" + strRequistionNumber + "] - Pending"
                                        + resubmitStatus;
                            }
                        }

                        log.info("sendRequisitionMailOnOriginating [Send Mail to TRAVEL MATA GmbH/India] try block Start for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                        pushEmailDataToTable(
                                strRequistionId, strRequistionNumber, emailIds, strFromMail, strCCMailCreate, strMataGmbhSubject, strMataGmbHMailContent, sUserId, Constants.USER_SUBMITTING_THE_REQ
                        );

                        log.info("sendRequisitionMailOnOriginating [Send Mail to TRAVEL MATA GmbH/India] try block End for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                        log.info("sendRequisitionMailOnOriginating [Send Mail to TRAVEL MATA GmbH/India] End for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

                    }
                } catch (Exception e)
                {
                    ExceptionLogger.logExceptionToDB(e);
                    log.error("Error in sendRequisitionMailOnOriginating =========" + e);
                }
            }

            log.info("sendRequisitionMailOnOriginating End for Requisition Number-->" + strTravelId + " at " + AppUtility.syncDate());

        } catch (Exception e)
        {
            log.error("Error in sendRequisitionMailOnOriginating main try/catch block=========" + e);
        }
    }

    public boolean isAdvanceTravelRequest(String strTravelId, String strTravelType, String strTravelAgencyId)
    {

        String sSqlStr = "";

        boolean isAdvanceTravelRequestFlag = false;
        String strFlightTravelFlag = "N";
        String strTrainTravelFlag = "N";
        String strCarTravelFlag = "N";
        String strAccommoTravelFlag = "N";
        String strAdvanceTravelFlag = "N";

        if (("D".equals(strTravelType) || "DOM".equals(strTravelType)) && !strTravelAgencyId.equals("2"))
        {
            sSqlStr = " SELECT CASE WHEN EXISTS(SELECT 1 FROM T_JOURNEY_DETAILS_DOM " + " WHERE TRAVEL_MODE=1 AND TRAVEL_ID=" + strTravelId
                    + " AND ISNULL(TRAVEL_FROM,'')<>'') THEN 'Y' ELSE 'N' END AS FLIGHT_TRAVEL_FLAG, " + " CASE WHEN EXISTS(SELECT 1 FROM T_JOURNEY_DETAILS_DOM "
                    + " WHERE TRAVEL_MODE=2 AND TRAVEL_ID=" + strTravelId + " AND ISNULL(TRAVEL_FROM,'')<>'') THEN 'Y' ELSE 'N' END AS TRAIN_TRAVEL_FLAG, "
                    + " CASE WHEN EXISTS(SELECT 1 FROM T_TRAVEL_CAR_DETAIL  WHERE TRAVEL_ID=" + strTravelId + " AND TRAVEL_TYPE='D' AND STATUS_ID=10) THEN 'Y' ELSE 'N' END AS CAR_TRAVEL_FLAG, "
                    + " CASE WHEN EXISTS(SELECT 1 FROM T_TRAVEL_ACCOMMODATION " + " WHERE TRAVEL_ID=" + strTravelId + " AND TRAVEL_TYPE='D') THEN 'Y' ELSE 'N' END AS ACCOMMODATION_TRAVEL_FLAG, "
                    + " CASE WHEN EXISTS(SELECT 1 FROM T_TRAVEL_EXPENDITURE_DOM " + " WHERE TRAVEL_ID=" + strTravelId + ") THEN 'Y' ELSE 'N' END AS ADVANCE_FLAG";

            try
            {
                Map<String, Object> infoFlagMap = getFirstResult(sSqlStr);
                if (infoFlagMap != null)
                {
                    strFlightTravelFlag = AppUtility.getResultFromMap(infoFlagMap, "FLIGHT_TRAVEL_FLAG");
                    strTrainTravelFlag = AppUtility.getResultFromMap(infoFlagMap, "TRAIN_TRAVEL_FLAG");
                    strCarTravelFlag = AppUtility.getResultFromMap(infoFlagMap, "CAR_TRAVEL_FLAG");
                    strAccommoTravelFlag = AppUtility.getResultFromMap(infoFlagMap, "ACCOMMODATION_TRAVEL_FLAG");
                    strAdvanceTravelFlag = AppUtility.getResultFromMap(infoFlagMap, "ADVANCE_FLAG");
                }

                if (strFlightTravelFlag.equals("N") && strTrainTravelFlag.equals("N") && strCarTravelFlag.equals("N") && strAccommoTravelFlag.equals("N") && strAdvanceTravelFlag.equals("Y"))
                {
                    isAdvanceTravelRequestFlag = true;
                }

            } catch (Exception e)
            {
                ExceptionLogger.logExceptionToDB(e);
                log.error("Error occured in EmailDAOImpl -> isAdvanceTravelRequest() ");
            }

        }
        return isAdvanceTravelRequestFlag;
    }

    public String customizeApproverMailBodyIndia(String travelId, String travelType, String strGroupTravelFlag)
    {

        log.info("[customizeApproverMailBodyIndia] method body start ---->");
        StringBuilder strMailMsg = new StringBuilder();

        TravelRequestDetailsDTO requestInfo = null;
        AppRequestDTO appRequestDTO = new AppRequestDTO();
        appRequestDTO.setTravelId(AppUtility.convertStringToLong(travelId));
        appRequestDTO.setTravelType(travelType);
        requestInfo = requestServices.getRequestDetails(appRequestDTO);

        FlightJourneyDetailsDTO flightJourneyDetails = requestInfo.getFlightJourneyDetails();
        List<FlightJourneyDTO> flightDetailList = flightJourneyDetails == null ? null : flightJourneyDetails.getFlightJourney();

        TrainJourneyDetailsDTO trainJourneyDetails = requestInfo.getTrainJourneyDetails();
        List<TrainJourneyDTO> trainJourneyList = trainJourneyDetails == null ? null : trainJourneyDetails.getTrainJourney();

        CarJourneyDetailsDTO carJourneyDetails = requestInfo.getCarJourneyDetails();
        List<CarJourneyDTO> carDetailsList = carJourneyDetails == null ? null : carJourneyDetails.getCarJourney();

        AccommodationDetailsDTO accomodationDetailsDTO = requestInfo.getAccommodationDetails();
        List<AccommodationDTO> accomodationDetailsList = accomodationDetailsDTO == null ? null : accomodationDetailsDTO.getAccommodations();

        TravelJourneyDetailDTO travelJourneyDetailDTO = null;
        List<TravelJourneyDetailDTO> travelJourneyDetailDTOList = new ArrayList<TravelJourneyDetailDTO>();

        String flightRemarks = "";
        String trainRemarks = "";
        String carRemarks = "";
        
        boolean otherInfoExist = false;
        
        boolean otherAccExist = "Y".equals(requestInfo.getOtherAccommodationsFlag());

        String commonDiv = "border: none;font-family: roboto,sans-serif;height: auto;margin: 0;padding: 0;vertical-align: initial;display: block;box-sizing: border-box;";
        String headingScope = "style=\"" + commonDiv + "display: block;border-bottom: 1px solid #ccc;margin: 0px;background: none;padding: 0px;\"";
        String h1 = "style=\"margin: 0;font-size: 16px;position: relative;color: #da2128;padding: 10px;border-bottom: 3px solid #da3128;display: inline-block;font-family: Roboto,sans-serif;line-height: 1.3;height: auto;\"";
        String containerScope = "style=\"padding: 10px 15px;\"";
        String tableResponsive = "style=\"overflow-x: auto;min-height: 0.01%;\"";
        String table = "style=\"border-collapse: collapse;border-spacing: 0;font: inherit;width: 100%;max-width: 100%;margin-bottom: 20px;border-color: #d1d3d4;border: none;\"";
        String th = "style=\"border: 1px Solid #ccc;font-family: \'Roboto\', sans-serif;background: #f6f6f6;font-size: 12px;vertical-align: bottom;padding: 8px;line-height: 1.42857143;text-align: left;display: table-cell;\"";
        String td = "style=\"border: 1px Solid #ccc;font-size: 12px;padding: 8px;line-height: 1.42857143;vertical-align: top;font-weight: 400;display: table-cell;\"";

        /* Display Advance Required Info Block Start */
        if ("D".equalsIgnoreCase(travelType))
        {
            String advReq = "";
            try
            {
                log.info("[customizeApproverMailBodyIndia] get Advance Required for requisition block start ---->");

                advReq = getRequisitionAdvanceInfo(travelId, travelType);
                strMailMsg.append(
                        "<u><font size=2 face=Verdana, Arial, Helvetica, sans-serif color=blue>Advance Required </u>:- </font><font size=2 face=Verdana, Arial, Helvetica, sans-serif >" + advReq
                                + "</font>\n<br><br>"
                );

                log.info("[customizeApproverMailBodyIndia] get Advance Required for requisition block end ---->");

            } catch (SQLException e)
            {
                log.error("[customizeApproverMailBodyIndia] Error occured while fetching Advance Required for requisition : " + e);
            }
        }
        /* Display Advance Required Info Block End */

        strMailMsg.append("<div><table width=\"100%\">\n");
        /*
         * if(strGroupTravelFlag!= null &&
         * (strGroupTravelFlag.trim().equalsIgnoreCase("Y"))) { // Group/Guest Traveler
         * Detail Section Start log.
         * info("[customizeApproverMailBodyIndia] Group/Guest Traveler Detail Section start ---->"
         * ); strMailMsg.append("<tr><td>") .append("<table "+reporttbleStr+">");
         * 
         * if(travelType.equalsIgnoreCase("D"))
         * strMailMsg.append("<tr><td height=\"0\" colspan=\"10\" "
         * +reportHeadingStr+">Guest Travellers Detail</td></tr>");
         * if(travelType.equalsIgnoreCase("I"))
         * strMailMsg.append("<tr><td height=\"0\" colspan=\"11\" "
         * +reportHeadingStr+">Guest Travellers Detail</td></tr>");
         * 
         * strMailMsg.append(Constants.HTML_TR_O)
         * .append("<td "+reportCaptionStr+" >S No.</td>")
         * .append("<td "+reportCaptionStr+" >First Name</td>").append("<td  "
         * +reportCaptionStr+" >Last Name</td>")
         * .append("<td "+reportCaptionStr+" >Designation</td>").append("<td  "
         * +reportCaptionStr+" >Date of Birth</td>")
         * .append("<td "+reportCaptionStr+" >Gender</td>").append("<td  "
         * +reportCaptionStr+" >Meal Preference</td>")
         * .append("<td "+reportCaptionStr+" >Total Amt Required</td>");
         * 
         * if(travelType.equalsIgnoreCase("D")){
         * strMailMsg.append("<td "+reportCaptionStr+" >Proof of Identity</td>").
         * append("<td  "+reportCaptionStr+" >Contact No.</td>")
         * .append(Constants.HTML_TR_C); } if(travelType.equalsIgnoreCase("I")){
         * strMailMsg.append("<td "+reportCaptionStr+" >Passport Number</td>").
         * append("<td  "+reportCaptionStr+" >Passport Valid upto</td>")
         * .append("<td "+reportCaptionStr+" >ECR</td>") .append(Constants.HTML_TR_C); }
         * 
         * int recordCount = 1; List<User> travellerList =
         * requestInfo.getTravelerList(); Passport passport = null; for(User traveler :
         * travellerList){ passport = traveler.getPassport();
         * strMailMsg.append(Constants.HTML_TR_O)
         * .append(Constants.HTML_TR_O).append("<td "+reportdataStr+" >"+recordCount+
         * Constants.HTML_TD_C)
         * .append("<td "+reportdataStr+" >"+traveler.getFirstName()+Constants.HTML_TD_C
         * ).append("<td  "+reportdataStr+" >"+traveler.getLastName()+Constants.
         * HTML_TD_C)
         * .append("<td "+reportdataStr+" >"+traveler.getDesignationName()+Constants.
         * HTML_TD_C).append("<td  "+reportdataStr+" >"+traveler.getDateOfBirth()+
         * Constants.HTML_TD_C)
         * .append("<td "+reportdataStr+" >"+traveler.getGender()+Constants.HTML_TD_C).
         * append("<td  "+reportdataStr+" >"+traveler.getMealPreferrence()+Constants.
         * HTML_TD_C)
         * .append("<td "+reportdataStr+" style=\"text-align: right;\">"+traveler.
         * getAmountRequired()+Constants.HTML_TD_C);
         * 
         * if(travelType.equalsIgnoreCase("D")){
         * strMailMsg.append("<td "+reportdataStr+" >"+ traveler.getProofIdentityName()
         * + " - " + traveler.getProofIdentityNumber()
         * +Constants.HTML_TD_C).append("<td  "+reportdataStr+" >"+
         * traveler.getContactNo() +Constants.HTML_TD_C) .append(Constants.HTML_TR_C); }
         * 
         * if(travelType.equalsIgnoreCase("I")){
         * strMailMsg.append("<td "+reportdataStr+" >"+passport.getPassportNo()+
         * Constants.HTML_TD_C).append("<td  "+reportdataStr+" >"+passport.
         * getDateOfExpiry()+Constants.HTML_TD_C) .append("<td "+reportdataStr+" >"+
         * passport.getEcr() +Constants.HTML_TD_C) .append(Constants.HTML_TR_C); }
         * recordCount++; }
         * strMailMsg.append(Constants.HTML_TABLE_C).append("</td></tr>"); log.
         * info("[customizeApproverMailBodyIndia] Group/Guest Traveler Detail Section end."
         * ); // Group/Guest Traveler Detail Section End }
         */

        // Itinerary Details Section Start
        log.info("[getMailBodyForMataIndia] Itinerary Details Section start ---->");
        if (("Y".equals(requestInfo.getTrainTravelFlag()) && trainJourneyDetails != null && trainJourneyList != null) || ("Y".equals(requestInfo.getCarTravelFlag()) && carJourneyDetails != null
                && carDetailsList != null) || ("Y".equals(requestInfo.getFlightTravelFlag()) && flightJourneyDetails != null && flightDetailList != null))
        {
            
            if(travelType.equalsIgnoreCase("D") && (("Y".equals(requestInfo.getTrainTravelFlag()) && trainJourneyDetails != null && trainJourneyList != null) 
                    || ("Y".equals(requestInfo.getCarTravelFlag()) && carJourneyDetails != null && carDetailsList != null))){
                otherInfoExist=true;
                
        }
            int colspan=9;
            if(otherInfoExist) {
                colspan=10;
            }
            
            strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O).append("<div " + headingScope + ">").append("<h1 " + h1 + ">Itinerary Information</h1>").append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_BR_C).append("<div " + containerScope + ">").append("<div " + tableResponsive + ">").append("<table " + table + ">");

            if ("Y".equals(requestInfo.getCarTravelFlag()) || ("Y".equals(requestInfo.getTrainTravelFlag())) || ("Y".equals(requestInfo.getFlightTravelFlag())))
            {
        	strMailMsg.append("<tr><th colspan=\""+(colspan+1)+"\" " + th + " >Travel Details</th></tr>")
                .append(Constants.HTML_TR_O).append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.journey") + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTURE_CITY) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_ARRIVALCITY) + Constants.HTML_TH_C)
                .append( "<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTUREDATE) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_PREFERREDTIME) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.travelmode") + Constants.HTML_TH_C )
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_CLASS) + Constants.HTML_TH_C)
                .append("<th " + th + " >Preferred Airline/Train/Cab</th>")
                .append("<th " + th + " >Preferred Seat/Location</th>")
                .append("<th " + th + " >" + appMessageUtility.getLabel("label.global.staytype") + Constants.HTML_TH_C);
                if(otherInfoExist) {
                    strMailMsg.append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.otherinformation") + Constants.HTML_TH_C);
                }
                strMailMsg.append(Constants.HTML_TR_C);
            }
            // Flight Detail Section Start
            log.info("[customizeApproverMailBodyIndia] " + Constants.FLIGHT + " Details Section start ---->");

            /** Flight Details */
            if (flightDetailList != null)
            {
                for (FlightJourneyDTO flightJourneyDetailsDTO : flightDetailList)
                {
                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(flightJourneyDetailsDTO.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(flightJourneyDetailsDTO.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(flightJourneyDetailsDTO.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(flightJourneyDetailsDTO.getDateOfJourney()));
                    
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredTimeModeDesc()) + " " + flightJourneyDetailsDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTime(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredTimeDesc()));
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.FLIGHT);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(flightJourneyDetailsDTO.getTravelClassDesc()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredAirline()));
                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredSeatDesc()));
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(flightJourneyDetailsDTO.getStayTypeDesc()));
                    travelJourneyDetailDTO.setOtherInfo("-");
                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }
            /** Flight Details Ends Here **/

            /** Train Details */
            if (trainJourneyList != null)
            {
                for (TrainJourneyDTO trainJourneyDTO : trainJourneyList)
                {

                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(trainJourneyDTO.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(trainJourneyDTO.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(trainJourneyDTO.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(trainJourneyDTO.getDateOfJourney()));
                    
                    
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTimeModeDesc()) + " " + trainJourneyDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTime(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTimeDesc()));
                    
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.TRAIN);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(trainJourneyDTO.getTravelClassDesc()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTrain()));
                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToMinus(trainJourneyDTO.getPreferredSeatDesc()));
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(trainJourneyDTO.getStayTypeDesc()));
                    travelJourneyDetailDTO.setOtherInfo("Tatkaal" + " - " + ((trainJourneyDTO!=null && trainJourneyDTO.getType()!=null && trainJourneyDTO.getType().equals("Intermediate Journey")) ? "NA" : (trainJourneyDTO.isTatkaalTicket() ? "Yes" : "No")));

                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }
            /** Train Details Ends Here **/

            /** Car Details */

            if (carDetailsList != null)
            {
                for (CarJourneyDTO carJourneyDetail : carDetailsList)
                {

                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(carJourneyDetail.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(carJourneyDetail.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(carJourneyDetail.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(carJourneyDetail.getDateOfJourney()));

                    travelJourneyDetailDTO.setPreferredTime(carJourneyDetail.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(carJourneyDetail.getPreferredTimeModeDesc()) + " " + carJourneyDetail.getPreferredTimeDesc());
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.CAR);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(carJourneyDetail.getCarClass()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(carJourneyDetail.getCarCategory()));
                    travelJourneyDetailDTO.setPrefferedSeat(carJourneyDetail.getLocationDesc());
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(""));
                    travelJourneyDetailDTO.setOtherInfo(AppUtility.checkNull(carJourneyDetail.getMobileNo())?("N.A."):("Mobile No." + " - " + AppUtility.blankToMinus(carJourneyDetail.getMobileNo())));
                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }

            /** Car Details Ends Here **/

            /** To Sort Time and date wise **/
            Collections.sort(travelJourneyDetailDTOList);
            for(int listCounter = 0; listCounter < travelJourneyDetailDTOList.size(); listCounter++)
            {
                TravelJourneyDetailDTO tempTravelObj =  travelJourneyDetailDTOList.get(listCounter);
                    
                    if(listCounter == 0)
                    {       tempTravelObj.setJourney("Onward");
                    }
                    else if(listCounter == travelJourneyDetailDTOList.size()-1)
                    {
                            tempTravelObj.setJourney("Return");
                    }
                    else
                    {
                            tempTravelObj.setJourney("Intermediate");
                    }
            }

            /** Iterate over all the travel type requests **/
            for (TravelJourneyDetailDTO tempJourneyDetailDTO : travelJourneyDetailDTOList)
            {
                strMailMsg.append(Constants.HTML_TR_O).append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getJourney()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getDeparture()) + Constants.HTML_TD_C )
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getArrival()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getDepartureDate()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + tempJourneyDetailDTO.getPreferredTimeDesc() + Constants.HTML_TD_C)
                .append("<td " + td + " >" + tempJourneyDetailDTO.getTravelMode() + Constants.HTML_TD_C);

                if ("flight".equalsIgnoreCase(tempJourneyDetailDTO.getTravelMode().trim()) && !"economy".equalsIgnoreCase(AppUtility.blankToMinus(tempJourneyDetailDTO.getTravelClass()).trim()))
                {
                    strMailMsg.append("<td " + td + " ><font color='blue' style='font-weight: bold;'>" + tempJourneyDetailDTO.getTravelClass() + "</font></td>");
                } else
                {
                    strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getTravelClass()) + Constants.HTML_TD_C);
                }

                strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getPrefferedTravel()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getPrefferedSeat()) + Constants.HTML_TD_C )
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getStayType()) + Constants.HTML_TD_C);
                 if(otherInfoExist) {
                     strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getOtherInfo()) + Constants.HTML_TD_C);
                 }
                strMailMsg.append(Constants.HTML_TR_C);
            }
           
            if (flightJourneyDetails != null && !AppUtility.isBlankOrHyphenString(flightJourneyDetails.getFlightJourneyRemarks()))
            {
                flightRemarks = AppUtility.checkNullString(flightJourneyDetails.getFlightJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + " >").append("Flight Remarks").append("</td>").append("<td " + td + " colspan="+colspan+" >").append(AppUtility.blankToMinus(flightRemarks))
                        .append("</td>").append("</tr>");

            }
            if (trainJourneyDetails != null && !AppUtility.isBlankOrHyphenString(trainJourneyDetails.getTrainJourneyRemarks()))
            {
                trainRemarks = AppUtility.checkNullString(trainJourneyDetails.getTrainJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + ">").append("Train Remarks").append("</td>").append("<td " + td + " colspan="+colspan+" >").append(AppUtility.blankToMinus(trainRemarks))
                        .append("</td>").append("</tr>");
            }
            if (carJourneyDetails != null && !AppUtility.isBlankOrHyphenString(carJourneyDetails.getCarJourneyRemarks()))
            {
                carRemarks = AppUtility.checkNullString(carJourneyDetails.getCarJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + ">").append("Car Remarks").append("</td>").append("<td " + td + " colspan="+colspan+">").append(AppUtility.blankToMinus(carRemarks))
                        .append("</td>").append("</tr>");
            }
           

            strMailMsg.append(Constants.HTML_TABLE_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C);
            
            strMailMsg.append("<div " + containerScope + ">")
            .append("<div " + tableResponsive + ">")
            .append("<table " + table + ">");

            
            /** Accommodation Detail Section Start **/
            log.info("[customizeApproverMailBodyIndia] Accommodation Details Section start ---->");
            if ("Y".equals(requestInfo.getAccommodationDetailsFlag()) && accomodationDetailsDTO != null)
            {
                List<AccommodationDTO> accommodationList = accomodationDetailsList;
                AccommodationDTO accommodation = null;

                if (accommodationList != null && !accommodationList.isEmpty())
                {
                    strMailMsg
                            // .append("<tr><td height=\"0\" colspan=\""+headerColspan+"\"
                            // "+reportSubHeadingStr+">Accommodation</td></tr>")
                    	    .append("<tr><th colspan=\"9\" " + th + " >Accommodation Details</th></tr>")
                            .append("<tr><th " + th + " >S.No.</th>")
                            .append("<th " + th + " >Stay Type</th><th " + th + " >Currency</th>")
                            .append("<th " + th + " >Budget</th>")
                            .append("<th " + th + " >Check In Date</th>")
                            .append("<th " + th + " >Check Out Date</th>")
                            .append("<th " + th + " >Preferred Place</th>")
                            .append("<th " + th + " >Address</th>");
                            if(otherAccExist) {
                        	strMailMsg.append("<th " + th + " >Reason</th>");
                        	
                            }

                    strMailMsg.append(Constants.HTML_TR_C);

                    for (int i = 0; i < accommodationList.size(); i++)
                    {
                        accommodation = accommodationList.get(i);

			strMailMsg.append(Constants.HTML_TR_O);
			strMailMsg.append("<td " + td + " >" + (i + 1) + Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getStayTypeDesc())+ Constants.HTML_TD_C);
			
			if(accommodation.getStayType()==1) {
			    strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCurrency())+ Constants.HTML_TD_C);
			    strMailMsg.append("<td " + td + " >"+ AppUtility.blankToMinus(AppUtility.checkNullString(accommodation.getBudget()))+ Constants.HTML_TD_C);
			}else {
			    strMailMsg.append("<td " + td + " >-" + Constants.HTML_TD_C);
			    strMailMsg.append("<td " + td + " >-"+ Constants.HTML_TD_C);
			}
			
			
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCheckInDate())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCheckOutDate())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getPlace())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getAddress())+ Constants.HTML_TD_C);
			 if(otherAccExist) {
			     strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getReason())+ Constants.HTML_TD_C);
			 }

                        strMailMsg.append(Constants.HTML_TR_C);
                    }
                    

                }
            }
            log.info("[customizeApproverMailBodyIndia] Accommodation Details Section end.");
            // Accommodation Detail Section End
            strMailMsg.append(Constants.HTML_TABLE_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_DIV_C)
            .append("</td></tr>");
        }
        log.info("[customizeApproverMailBodyIndia] Itinerary Details Section end.");
        // Itinerary Details Section End

        strMailMsg.append(Constants.HTML_TABLE_C).append("</div>").append("\n");
        log.info("[customizeApproverMailBodyIndia] method body end.");
        return strMailMsg.toString();
    }

    public String customizeApproverMailBodyGmbh(String travelId, String travelType)
    {

        log.info("[customizeApproverMailBodyGmbh] method body start ---->");

        StringBuilder mailBody = new StringBuilder();
        TravelRequestDetailsDTO requestInfo = null;
        AppRequestDTO appRequestDTO = new AppRequestDTO();
        appRequestDTO.setTravelId(AppUtility.convertStringToLong(travelId));
        appRequestDTO.setTravelType(travelType);
        requestInfo = requestServices.getRequestDetails(appRequestDTO);

        FlightJourneyDetailsDTO flightJourneyDetails = requestInfo.getFlightJourneyDetails();
        List<FlightJourneyDTO> flightDetailList = flightJourneyDetails.getFlightJourney();

        TrainJourneyDetailsDTO trainJourneyDetails = requestInfo.getTrainJourneyDetails();
        List<TrainJourneyDTO> trainJourneyList = trainJourneyDetails.getTrainJourney();

        CarJourneyDetailsDTO carJourneyDetails = requestInfo.getCarJourneyDetails();
        List<CarJourneyDTO> carDetailsList = carJourneyDetails.getCarJourney();

        AccommodationDetailsDTO accomodationDetailsDTO = requestInfo.getAccommodationDetails();
        List<AccommodationDTO> accomodationDetailsList = accomodationDetailsDTO.getAccommodations();

        List<CommentDTO> approversCommentList = requestInfo.getApproverCommentsList();

        String tableStyleStr = "align=\"left\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" style=\"max-width: 100%;border: #c7c7c5 1px solid;\" bordercolor=\"#c7c7c5\" style=\"border-collapse: collapse;margin:0;\"";
        String tableHeadingStyle = "style=\"font-family:Verdana, Arial, Helvetica, sans-serif;font-size:13px;font-weight:bold;color:#ffffff;line-height:22px;background-color:#aa1220;padding-left:5px;text-align:left;\"";
        String tableSubHeadingStr = "style=\"font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;color:#ffffff;line-height:20px;background-color:#ea394b;padding-left:5px;text-align:left;\"";
        String headerStyleStr = "style=\"font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;font-weight:bold;color:#1d1d1d;line-height:20px;background-color:#f2f2f2;padding-left:5px;padding-right:5px;text-align:left;\"";
        String dataStyleStr = "style=\"font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;font-weight: normal;color:#373737;line-height:20px;background-color:#f7f7f7;padding-left:5px;padding-right:5px;text-align:left;\"";

        mailBody.append("<div><table><tr><td>\n");
        log.info("[customizeApproverMailBodyGmbh] Flight Details Section start ---->");
        if ("Y".equals(requestInfo.getFlightTravelFlag()) && flightJourneyDetails != null && flightDetailList != null)
        {

            mailBody.append("<table " + tableStyleStr + " >").append("<tr><td height=\"0\" colspan=\"8\" " + tableHeadingStyle + " >Flight Details</td></tr>").append(
                    "<tr><td style=\"font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;font-weight:bold;color:#1d1d1d;line-height:20px;background-color:#f2f2f2;padding-left:5px;padding-right:5px;text-align:left;\" ></td>"
            ).append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTURE_CITY) + Constants.HTML_TD_C)
                    .append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_ARRIVALCITY) + Constants.HTML_TD_C + "<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTUREDATE) + Constants.HTML_TD_C).append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_PREFERREDTIME) + Constants.HTML_TD_C + "<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_CLASS) + Constants.HTML_TD_C + "<td " + headerStyleStr + " >" + appMessageUtility.getLabel("label.global.preferredseat")).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);

            String remarksFlt = null;

            if (flightDetailList != null)
            {
                for (FlightJourneyDTO flight : flightDetailList)
                {
                    if (!flight.getSource().trim().equals("") || !flight.getDestination().trim().equals(""))
                    {

                        if (flight.getType().equalsIgnoreCase(JourneyType.FORWARD.getName()))
                        {
                            mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Outward Leg</td>");
                        } else if (flight.getType().equalsIgnoreCase(JourneyType.INTERMEDIATE.getName()))
                        {
                            mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Intermediate Leg</td>");
                        } else
                        {
                            mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Return Leg</td>");
                        }

                        mailBody.append("<td " + dataStyleStr + " >" + flight.getSource()).append(Constants.HTML_TD_C).append(
                                "<td " + dataStyleStr + " >" + flight.getDestination() + Constants.HTML_TD_C
                        ).append("<td " + dataStyleStr + " >" + flight.getDateOfJourney()).append(Constants.HTML_TD_C).append(
                                "<td " + dataStyleStr + " >" + flight.getPreferredTimeModeDesc() + Constants.HTML_NBSP + flight.getPreferredTimeDesc() + Constants.HTML_TD_C
                        ).append("<td " + dataStyleStr + " >" + flight.getTravelClass() + Constants.HTML_TD_C).append("<td " + dataStyleStr + " >" + flight.getPreferredSeatDesc())
                                .append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);

                        remarksFlt = flightJourneyDetails.getFlightJourneyRemarks();
                    }
                }
            }
            if (remarksFlt != null && !remarksFlt.equals("") && !remarksFlt.trim().equals("-"))
            {
                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + ">" + Constants.REMARKS_IC).append(Constants.HTML_TD_C)
                        .append("<td " + dataStyleStr + " colspan=\"7\">" + remarksFlt).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }
            mailBody.append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        log.info("[customizeApproverMailBodyGmbh] Flight Details Section end.");

        log.info("[customizeApproverMailBodyGmbh] Train Details Section start ---->");
        if ("Y".equals(requestInfo.getTrainTravelFlag()) && trainJourneyDetails != null && trainJourneyList != null)
        {

            TrainJourneyDTO detailTRN = null;

            mailBody.append("<tr><td><table" + tableStyleStr + "><tr>").append("<td height=\"0\" colspan=\"7\" " + tableHeadingStyle + ">Train Details").append(Constants.HTML_TD_C).append(
                    Constants.HTML_TR_C
            ).append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " ></td>").append(
                    "<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTURE_CITY) + Constants.HTML_TD_C
            ).append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_ARRIVALCITY) + Constants.HTML_TD_C).append(
                    "<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTUREDATE) + Constants.HTML_TD_C
            ).append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_PREFERREDTIME) + Constants.HTML_TD_C).append(
                    "<td " + headerStyleStr + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_CLASS) + Constants.HTML_TD_C
            ).append("<td " + headerStyleStr + " >" + appMessageUtility.getLabel("label.global.preferredseat")).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            for (int i = 0; i < trainJourneyList.size(); i++)
            {
                detailTRN = trainJourneyList.get(i);

                if (!AppUtility.isBlankString(detailTRN.getSource()) || AppUtility.isBlankString(detailTRN.getDestination()))
                {

                    if (detailTRN.getType().equalsIgnoreCase(JourneyType.FORWARD.getName()))
                    {
                        mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Outward Leg</td>");
                    }
                    if (detailTRN.getType().equalsIgnoreCase(JourneyType.FORWARD.getName()))
                    {
                        mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Intermediate Leg</td>");
                    }
                    if (detailTRN.getType().equalsIgnoreCase(JourneyType.FORWARD.getName()))
                    {
                        mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Return Leg</td>");

                    }
                    mailBody.append("<td " + dataStyleStr + " >" + detailTRN.getSource() + Constants.HTML_TD_C).append(
                            "<td  " + dataStyleStr + " >" + detailTRN.getDestination() + Constants.HTML_TD_C
                    ).append("<td " + dataStyleStr + " >" + detailTRN.getDateOfJourney() + Constants.HTML_TD_C).append(
                            "<td  " + dataStyleStr + " >" + detailTRN.getPreferredTimeDesc() + Constants.HTML_NBSP + detailTRN.getPreferredTimeDesc() + Constants.HTML_TD_C
                    ).append("<td " + dataStyleStr + " >" + detailTRN.getTravelClass() + Constants.HTML_TD_C).append("<td  " + dataStyleStr + " >" + detailTRN.getPreferredSeatDesc())
                            .append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
                }
            }
            if (!AppUtility.isBlankString(trainJourneyDetails.getTrainJourneyRemarks()) && !trainJourneyDetails.getTrainJourneyRemarks().trim().equals("-"))
            {
                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + ">" + Constants.REMARKS_IC + Constants.HTML_TD_C)
                        .append("<td  " + dataStyleStr + " colspan=\"7\">" + trainJourneyDetails.getTrainJourneyRemarks()).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }
            mailBody.append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);

        }
        log.info("[customizeApproverMailBodyGmbh] Train Details Section end.");

        log.info("[customizeApproverMailBodyGmbh] Car Details Section start ---->");
        if ("Y".equals(requestInfo.getCarTravelFlag()) && carJourneyDetails != null && carDetailsList != null)
        {
            CarJourneyDTO car = null;
            String needGPS = "";
            String carCategory = "";
            String carClass = "";
            String carClassId = "";

            int setColSpan1 = 0;
            int setColSpan2 = 0;
            int setColSpan3 = 0;

            carClass = carJourneyDetails.getCarClassDesc();
            carClassId = AppUtility.convertIntToString(carJourneyDetails.getCarClass());

            if (carClassId != null && !"".equals(carClassId) && ("26".equals(carClassId) || "27".equals(carClassId) || "29".equals(carClassId)))
            {
                setColSpan1 = 6;
                setColSpan2 = 5;
                setColSpan3 = 3;
            } else
            {
                setColSpan1 = 5;
                setColSpan2 = 4;
                setColSpan3 = 2;
            }

            mailBody.append("<tr><td><table " + tableStyleStr + ">").append(
                    "<tr><td height=\"0\" colspan=" + setColSpan1 + " " + tableHeadingStyle + ">Car Reservation&nbsp;-&nbsp;[" + carClass + "]</td></tr>"
            ).append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " ></td>").append("<td " + headerStyleStr + " >Date</td>").append("<td  " + headerStyleStr + " >Time</td>")
                    .append("<td " + headerStyleStr + " >City</td>").append("<td  " + headerStyleStr + " >Location</td>");

            if (carClassId != null && !"".equals(carClassId) && "26".equals(carClassId))
            {
                mailBody.append("<td " + headerStyleStr + " >Mobile Number</td>");
            } else if (carClassId != null && !"".equals(carClassId) && ("27".equals(carClassId) || "29".equals(carClassId)))
            {
                mailBody.append("<td " + headerStyleStr + " >Routing</td>");
            }
            mailBody.append(Constants.HTML_TR_C);

            for (int i = 0; i < carDetailsList.size(); i++)
            {
                car = carDetailsList.get(i);

                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >Start</td>").append("<td " + dataStyleStr + " >" + car.getDateOfJourney() + Constants.HTML_TD_C).append(
                        "<td  " + dataStyleStr + " >" + car.getPreferredTimeDesc() + Constants.HTML_TD_C
                ).append("<td " + dataStyleStr + " >" + car.getSource() + Constants.HTML_TD_C).append("<td  " + dataStyleStr + " >" + car.getLocationDesc() + Constants.HTML_TD_C);

                if (carClassId != null && !"".equals(carClassId) && "26".equals(carClassId))
                {
                    mailBody.append("<td " + dataStyleStr + " >" + car.getMobileNo() + Constants.HTML_TD_C);
                } else if (carClassId != null && !"".equals(carClassId) && ("27".equals(carClassId) || "29".equals(carClassId)))
                {
                    // mailBody.append("<td "+dataStyleStr+"
                    // >"+car.getStartRouting()+Constants.HTML_TD_C); //To be used when start
                    // working on it
                    mailBody.append("<td " + dataStyleStr + " >" + "-" + Constants.HTML_TD_C);
                }
                mailBody.append(Constants.HTML_TR_C);

                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >End</td>")
                        // .append("<td "+dataStyleStr+"
                        // >"+car.getEndDate()+Constants.HTML_TD_C).append("<td "+dataStyleStr+"
                        // >"+car.getEndTime()+Constants.HTML_TD_C)// to be used
                        .append("<td " + dataStyleStr + " >" + "car.getEndDate()" + Constants.HTML_TD_C).append("<td  " + dataStyleStr + " >" + "car.getEndTime()" + Constants.HTML_TD_C).append("<td " + dataStyleStr + " >" + car.getDestination() + Constants.HTML_TD_C).append("<td  " + dataStyleStr + " >" + "car.getEndLocation()" + Constants.HTML_TD_C); // to
                                                                                                                                                                                                                                                                                                                                                                  // be
                                                                                                                                                                                                                                                                                                                                                                  // used

                if (carClassId != null && !"".equals(carClassId) && "26".equals(carClassId))
                {
                    mailBody.append("<td " + dataStyleStr + " >" + "car.getEndMobileNo()" + Constants.HTML_TD_C); // to be used
                } else if (carClassId != null && !"".equals(carClassId) && ("27".equals(carClassId) || "29".equals(carClassId)))
                {
                    mailBody.append("<td " + dataStyleStr + " >" + "car.getEndRouting()" + Constants.HTML_TD_C); // To be used
                }
                mailBody.append(Constants.HTML_TR_C);

                // needGPS = !car.isNeed_GPS() ? "No" : "Yes";
                needGPS = "Yes";
                carCategory = carJourneyDetails.getCarCategoryDesc();
            }

            if (carClassId != null && !"".equals(carClassId) && ("26".equals(carClassId) || "27".equals(carClassId)))
            {
                mailBody.append("<tr><td height=\"0\" colspan=" + setColSpan1 + " " + tableSubHeadingStr + ">Car Options</td></tr>").append(Constants.HTML_TR_O).append(
                        "<td " + headerStyleStr + " >Need GPS</td>"
                ).append("<td " + dataStyleStr + " >" + needGPS + Constants.HTML_TD_C).append("<td  " + headerStyleStr + " >Category</td>")
                        .append("<td " + dataStyleStr + " colspan=" + setColSpan3 + " >" + carCategory).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }

            if (!AppUtility.isBlankString(carJourneyDetails.getCarJourneyRemarks()) && !carJourneyDetails.getCarJourneyRemarks().trim().equals("-"))
            {
                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + ">" + Constants.REMARKS_IC + Constants.HTML_TD_C)
                        .append("<td  " + dataStyleStr + " colspan=" + setColSpan2 + ">" + carJourneyDetails.getCarJourneyRemarks()).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }
            mailBody.append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        log.info("[customizeApproverMailBodyGmbh] Car Details Section end.");

        log.info("[customizeApproverMailBodyGmbh] Accomodation Details Section start ---->");
        if ("Y".equals(requestInfo.getAccommodationDetailsFlag()) && !AppUtility.isNull(accomodationDetailsDTO))
        {

            AccommodationDTO accommodation = null;

            mailBody.append("<tr><td><table  " + tableStyleStr + "><tr><td height=\"0\" colspan=\"5\" ").append(tableHeadingStyle).append(">Accommodation</td></tr>").append(
                    Constants.HTML_TR_O
            ).append("<td " + headerStyleStr + " >##</td>").append("<td " + headerStyleStr + " >Place of Visit (City and Address)</td>").append("<td " + headerStyleStr + " >Accommodation Type</td>")
                    .append("<td " + headerStyleStr + " >Check In Date</td>").append("<td " + headerStyleStr + " >Check Out Date</td></tr>");
            for (int i = 0; i < accomodationDetailsList.size(); i++)
            {
                accommodation = accomodationDetailsList.get(i);

                mailBody.append(Constants.HTML_TR_O).append("<td " + dataStyleStr + " >" + (i + 1) + Constants.HTML_TD_C).append(
                        "<td " + dataStyleStr + " >" + accommodation.getPlace() + Constants.HTML_TD_C
                ).append("<td  " + dataStyleStr + " >" + accommodation.getStayTypeDesc() + Constants.HTML_TD_C);
                mailBody.append("<td " + dataStyleStr + " >" + accommodation.getCheckInDate() + Constants.HTML_TD_C).append("<td  " + dataStyleStr + " >" + accommodation.getCheckOutDate())
                        .append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }
            if (!AppUtility.isBlankString(accomodationDetailsDTO.getAccommodationRemarks()) && !accomodationDetailsDTO.getAccommodationRemarks().trim().equals("-"))
            {
                mailBody.append(Constants.HTML_TR_O).append("<td " + headerStyleStr + " >" + Constants.REMARKS_IC + Constants.HTML_TD_C)
                        .append("<td  " + dataStyleStr + "  colspan=\"4\">" + accomodationDetailsDTO.getAccommodationRemarks()).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
            }
            mailBody.append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        log.info("[customizeApproverMailBodyGmbh] Accomodation Details Section end.");

        log.info("[customizeApproverMailBodyGmbh] Approver Comments List Section start ---->");
        if (approversCommentList != null)
        {
            mailBody.append("<tr><td><table  " + tableStyleStr + "><tr><td height=\"0\" colspan=\"4\" " + tableHeadingStyle + ">Comments</td></tr>" + Constants.HTML_TR_O).append(
                    "<td " + headerStyleStr + " >S No.</td>" + "<td " + headerStyleStr + " >Comments</td>" + "<td " + headerStyleStr + " >Posted By</td>" + "<td " + headerStyleStr
                            + " >Posted On</td></tr>"
            );
            if (approversCommentList != null && !approversCommentList.isEmpty())
            {

                for (int i = 0; i < approversCommentList.size(); i++)
                {
                    CommentDTO comment = approversCommentList.get(i);
                    mailBody.append(Constants.HTML_TR_O).append("<td " + dataStyleStr + " >" + (i + 1) + Constants.HTML_TD_C).append(
                            "<td " + dataStyleStr + " >" + comment.getCommentDesc() + Constants.HTML_TD_C
                    ).append("<td " + dataStyleStr + " >" + comment.getPostedByName() + Constants.HTML_TD_C).append("<td " + dataStyleStr + " >" + comment.getPostedOnDate())
                            .append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
                }

            }
            mailBody.append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        log.info("[customizeApproverMailBodyGmbh] Approver Comments List Section end.");

        mailBody.append("</td></tr></table></div>\n");
        log.info("[customizeApproverMailBodyGmbh] method body end.");
        return mailBody.toString();
    }

    public String getGroupMailBodyForMataIndia(String travelId, String travelType, String travellerId)
    {

        StringBuilder strMailMsg = new StringBuilder("GI" + travelId + travelType + travellerId);
        return strMailMsg.toString();
    }

    public String getMailBodyForMataIndia(String travelId, String travelType, String travellerId)
    {

        // Fetch entire request information
        StringBuilder mailBody = new StringBuilder();
        TravelRequestDetailsDTO requestInfo = null;
        AppRequestDTO appRequestDTO = new AppRequestDTO();
        appRequestDTO.setTravelId(AppUtility.convertStringToLong(travelId));
        appRequestDTO.setTravelType(travelType);
        requestInfo = requestServices.getRequestDetails(appRequestDTO);

        FlightJourneyDetailsDTO flightJourneyDetails = requestInfo.getFlightJourneyDetails();
        List<FlightJourneyDTO> flightDetailList = flightJourneyDetails == null ? null : flightJourneyDetails.getFlightJourney();

        TrainJourneyDetailsDTO trainJourneyDetails = requestInfo.getTrainJourneyDetails();
        List<TrainJourneyDTO> trainJourneyList = trainJourneyDetails == null ? null : trainJourneyDetails.getTrainJourney();

        CarJourneyDetailsDTO carJourneyDetails = requestInfo.getCarJourneyDetails();
        List<CarJourneyDTO> carDetailsList = carJourneyDetails == null ? null : carJourneyDetails.getCarJourney();

        AccommodationDetailsDTO accomodationDetailsDTO = requestInfo.getAccommodationDetails();
        List<AccommodationDTO> accomodationDetailsList = accomodationDetailsDTO == null ? null : accomodationDetailsDTO.getAccommodations();

        AdvanceDetailsDTO advanceDetailsDTO = requestInfo.getAdvanceForexDetails();
        List<AdvanceDTO> advanceDTOList = advanceDetailsDTO == null ? null : advanceDetailsDTO.getAdvanceAmountDetails();

        TravelJourneyDetailDTO travelJourneyDetailDTO = null;
        List<TravelJourneyDetailDTO> travelJourneyDetailDTOList = new ArrayList<>();

        List<VisaDetailDTO> visaDetailList = requestInfo.getVisa();
        PassportDetailDTO passport = requestInfo.getPassport();
        InsuranceDetailDTO insurance = requestInfo.getInsurance();
        BudgetActualDTO actualBudgetDetailDTO = requestInfo.getBudgetActualDetails();
        TravelFareDTO travelFare = requestInfo.getTotalTravelFareDetails();

        List<ApproverDTO> approversList = requestInfo.getApprovers();

        VisaDetailDTO visa = new VisaDetailDTO();

        ApproverDTO approverDTO = requestInfo.getBillingApprover();

        List<ApproverDTO> approverList = requestInfo.getApproverLevels();
        String approverLevel1 = null;
        String approverLevel2 = null;
        if (approverList != null && !approverList.isEmpty())
        {
            approverLevel1 = approverList.get(0).getName();
        }
        if (approverList != null && approverList.size() > 1)
        {
            approverLevel2 = approverList.get(1).getName();
        }

        if (approverDTO == null)
        {
            approverDTO = new ApproverDTO();
        }
        
        
        boolean otherAccExist= "Y".equals(requestInfo.getOtherAccommodationsFlag());
        
        
        String flightRemarks = "";
        String trainRemarks = "";
        String carRemarks = "";        
        boolean otherInfoExist = false;

        String commonDiv = "border: none;font-family: roboto,sans-serif;height: auto;margin: 0;padding: 0;vertical-align: initial;display: block;box-sizing: border-box;";
        String containerScope = "style=\"padding: 10px 15px;width:100%;\"";
        String tableResponsive = "style=\"overflow-x: auto;min-height: 0.01%;\"";
        String table = "style=\"border-collapse: collapse;border-spacing: 0;font: inherit;width: 100%;max-width: 100%;margin-bottom: 20px;border-color: #d1d3d4;border: none;\"";
        String th = "style=\"border: 1px Solid #ccc;font-family: Roboto, sans-serif;background: #f6f6f6;font-size: 12px;vertical-align: bottom;padding: 8px;line-height: 1.42857143;text-align: left;display: table-cell;\"";
        String td = "style=\"border: 1px Solid #ccc;font-size: 12px;padding: 8px;line-height: 1.42857143;vertical-align: top;font-weight: 400;display: table-cell;font-family:roboto,sans-serif;\"";
        String tdAdv = "style=\"border: 1px Solid #ccc;font-size: 12px;padding: 8px;line-height: 1.42857143;vertical-align: top;font-weight: 400;display: table-cell;font-family:roboto,sans-serif;text-align:right;\"";
        String rowWithSeperator = "style=\"border-bottom: 1px solid #ebebeb;margin-bottom: 10px;margin-left: -6px;margin-right: -6px;border: none;font: inherit;height: auto;margin: 0;padding: 0;vertical-align: initial;\"";
        String col3 = "style=\"" + commonDiv + "width: 25%;float: left;position: relative;min-height: 1px;padding-left: 6px;padding-right: 6px;border-bottom: 1px solid #ebebeb;\"";
        String col6 = "style=\"" + commonDiv + "width: 50%;float: left;position: relative;min-height: 1px;padding-left: 6px;padding-right: 6px;border-bottom: 1px solid #ebebeb;\"";
        String col12 = "style=\"" + commonDiv + "width: 100%;float: left;position: relative;min-height: 1px;padding-left: 6px;padding-right: 6px;border-bottom: 1px solid #ebebeb;\"";
        String formGroup = "style=\"" + commonDiv + "margin-bottom: 8px;position: relative;\"";
        String controlLable = "style=\"display: block;font-size: 13px;color: #404142;font-family: \'Roboto\', sans-serif;padding-bottom: .25em;padding-top: .25em;border: none;font: inherit;height: auto;margin: 0;vertical-align: initial;max-width: 100%;box-sizing: border-box;cursor: default;\"";
        String value = "style=\"" + commonDiv + "display: block;font-size: 12px;color: #6d6e71;\"";
        String headingScope = "style=\"" + commonDiv + "display: block;border-bottom: 1px solid #ccc;margin: 0px;background: none;padding: 0px;\"";
        String h1 = "style=\"margin: 0;font-size: 16px;position: relative;color: #da2128;padding: 10px;border-bottom: 3px solid #da3128;display: inline-block;font-family: Roboto,sans-serif;line-height: 1.3;height: auto;\"";
        String h2 = "style=\"margin: 0;font-size: 13px;position: relative;color: #da2128;padding: 10px;display: inline-block;font-family: Roboto,sans-serif;line-height: 1.3;height: auto;\"";
        UserDetailDTO originator = requestInfo.getOriginator();
        UserDetailDTO traveller = requestInfo.getTraveller();

        StringBuilder strMailMsg = new StringBuilder();
        strMailMsg.append("<html><head><title class='inputbox'>WELCOME TO STARS:  SAMVARDHANA MOTHERSON TRAVEL  APPROVAL SYSTEM </title> ").append(
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
        ).append("<style type=\"text/css\">").append("").append(".lableFontSize{font-size:12px !important}").append("</style>").append("</head>");

        String container = "style=\"margin: 19px;border:none !important;background: #ffffff;\"";
        String labelFontSize = "style=\"font-size:10px ;\" class=\"lableFontSize\" ";
        strMailMsg.append("<body style=\"margin:0;vertical-align: middle;\" align=\"left\">").append("<div " + container + ">");

        strMailMsg.append("<div " + headingScope + ">");

        if (travelType.equalsIgnoreCase("D"))
        {
            strMailMsg.append("<h1 " + h1 + ">Domestic Journey Report</h1>");
        } else
        {
            strMailMsg.append("<h1 " + h1 + ">International Journey Report</h1>");
        }
        strMailMsg.append("<br>");
        strMailMsg.append(Constants.HTML_DIV_C);

        strMailMsg.append("<table width=\"100%\" style=\"padding-left:10px;margin:0;\" border=\"0\" cellpadding=\"5\" cellspacing=\"1\">");

        // Request Details Start
        strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O).append("<div " + headingScope + ">").append("<h1 " + h1 + ">Request Details</h1>").append(Constants.HTML_DIV_C)
                .append(Constants.HTML_BR_C).append("<table " + containerScope + ">")

                .append("<tr " + rowWithSeperator + " >")

                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Requisition No")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(requestInfo.getRequestNo()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Originator")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(originator.getFullName()).toUpperCase())
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Division")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(originator.getDivisionName()).toUpperCase())
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Unit")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(originator.getSiteName()).toUpperCase())
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append(Constants.HTML_TR_C)

                .append("<tr " + rowWithSeperator + " >")

                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Department")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(originator.getDepartmentName()).toUpperCase())
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Cost Centre")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getCostCentreName()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append("<td colspan='2' " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Originated On")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(requestInfo.getCreationDate()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append(Constants.HTML_TR_C)

                .append("<tr " + rowWithSeperator + " >")

                .append("<td colspan='4' " + col12 + " >")
                .append("<div " + formGroup + "  >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Reason for travel")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(requestInfo.getReasonForTravel()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append(Constants.HTML_TR_C)

                .append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        // Request Details End

        // personal detail new starts
        strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O).append("<div " + headingScope + ">").append("<h1 " + h1 + ">Traveller Information</h1>").append(Constants.HTML_DIV_C)
                .append(Constants.HTML_BR_C).append("<table " + containerScope + ">")

                .append("<tr " + rowWithSeperator + " >")
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Traveller Name").append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getUserName()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Emp. Code")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getEmpCode()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Designation")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getDesignationName()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Email ID")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getEmail()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append(Constants.HTML_TR_C)
                
                .append("<tr " + rowWithSeperator + " >")
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Contact No.")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getContactNo()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Date of Birth")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getDateOfBirth()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Gender")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getGender()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)
                
                .append("<td " + col3 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Meal Preference")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getMealPreferenceDesc()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append(Constants.HTML_TR_C)
                .append("<tr " + rowWithSeperator + " >")

                .append("<td colspan=\"2\" " + col6 + " >")
                .append("<div " + formGroup + "  >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Permanent Address")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getPermanentAddress()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append("<td colspan=\"2\" " + col6 + " >")
                .append("<div " + formGroup + " >")
                .append("<span " + controlLable + " >")
                .append("<strong " + labelFontSize + ">")
                .append("Current Address")
                .append(Constants.HTML_STRONG_C)
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_BR_C)
                .append("<span " + value + " >")
                .append(AppUtility.blankToMinus(traveller.getCurrentAddress()))
                .append(Constants.HTML_SPAN_C)
                .append(Constants.HTML_DIV_C)
                .append(Constants.HTML_TD_C)

                .append(Constants.HTML_TR_C)

                .append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);

        // Personal Details End
        // Itinerary Details Section Start
        log.info("[getMailBodyForMataIndia] Itinerary Details Section start ---->");
        if (("Y".equals(requestInfo.getTrainTravelFlag()) && trainJourneyDetails != null && trainJourneyList != null) || ("Y".equals(requestInfo.getCarTravelFlag()) && carJourneyDetails != null
                && carDetailsList != null) || ("Y".equals(requestInfo.getFlightTravelFlag()) && flightJourneyDetails != null && flightDetailList != null))
        {
            
            if(travelType.equalsIgnoreCase("D") && (("Y".equals(requestInfo.getTrainTravelFlag()) && trainJourneyDetails != null && trainJourneyList != null) 
                    || ("Y".equals(requestInfo.getCarTravelFlag()) && carJourneyDetails != null && carDetailsList != null))){
                otherInfoExist=true;
                
        }
            int colspan=9;
            if(otherInfoExist) {
                colspan=10;
            }
            
            strMailMsg.append(Constants.HTML_TR_O)
            .append(Constants.HTML_TD_O)
            .append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Itinerary Information</h1>")
            .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_BR_C)
                    .append("<div " + containerScope + ">")
                    .append("<div " + tableResponsive + ">")
                    .append("<table " + table + ">");

            if ("Y".equals(requestInfo.getCarTravelFlag()) || ("Y".equals(requestInfo.getTrainTravelFlag())) || ("Y".equals(requestInfo.getFlightTravelFlag())))
            {
        	strMailMsg.append("<tr><th colspan=\""+(colspan+1)+"\" " + th + " >Travel Details</th></tr>")
                .append(Constants.HTML_TR_O).append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.journey") + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTURE_CITY) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_ARRIVALCITY) + Constants.HTML_TH_C)
                .append( "<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_DEPARTUREDATE) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_PREFERREDTIME) + Constants.HTML_TH_C)
                .append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.travelmode") + Constants.HTML_TH_C )
                .append("<th " + th + " >" + appMessageUtility.getLabel(Constants.LABEL_GLOBAL_CLASS) + Constants.HTML_TH_C)
                .append("<th " + th + " >Preferred Airline/Train/Cab</th>")
                .append("<th " + th + " >Preferred Seat/Location</th>")
                .append("<th " + th + " >" + appMessageUtility.getLabel("label.global.staytype") + Constants.HTML_TH_C);
                if(otherInfoExist) {
                    strMailMsg.append("<th " + th + " >" + appMessageUtility.getLabel("label.requestdetails.otherinformation") + Constants.HTML_TH_C);
                }
                strMailMsg.append(Constants.HTML_TR_C);
            }
            // Flight Detail Section Start
            log.info("[getMailBodyForMataIndia] " + Constants.FLIGHT + " Details Section start ---->");

            /** Flight Details */
            if (flightDetailList != null)
            {
                for (FlightJourneyDTO flightJourneyDetailsDTO : flightDetailList)
                {
                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(flightJourneyDetailsDTO.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(flightJourneyDetailsDTO.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(flightJourneyDetailsDTO.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(flightJourneyDetailsDTO.getDateOfJourney()));
                    
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredTimeModeDesc()) + " " + flightJourneyDetailsDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTime(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredTimeDesc()));
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.FLIGHT);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(flightJourneyDetailsDTO.getTravelClassDesc()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredAirline()));
                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToMinus(flightJourneyDetailsDTO.getPreferredSeatDesc()));
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(flightJourneyDetailsDTO.getStayTypeDesc()));
                    travelJourneyDetailDTO.setOtherInfo("-");
                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }
            /** Flight Details Ends Here **/

            /** Train Details */
            if (trainJourneyList != null)
            {
                for (TrainJourneyDTO trainJourneyDTO : trainJourneyList)
                {

                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(trainJourneyDTO.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(trainJourneyDTO.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(trainJourneyDTO.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(trainJourneyDTO.getDateOfJourney()));
                    
                    
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTimeModeDesc()) + " " + trainJourneyDTO.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTime(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTimeDesc()));
                    
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.TRAIN);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(trainJourneyDTO.getTravelClassDesc()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(trainJourneyDTO.getPreferredTrain()));
                    travelJourneyDetailDTO.setPrefferedSeat(AppUtility.blankToMinus(trainJourneyDTO.getPreferredSeatDesc()));
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(trainJourneyDTO.getStayTypeDesc()));
                    travelJourneyDetailDTO.setOtherInfo("Tatkaal" + " - " + ((trainJourneyDTO!=null && trainJourneyDTO.getType()!=null && trainJourneyDTO.getType().equals("Intermediate Journey")) ? "NA" : (trainJourneyDTO.isTatkaalTicket() ? "Yes" : "No")));

                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }
            /** Train Details Ends Here **/

            /** Car Details */

            if (carDetailsList != null)
            {
                for (CarJourneyDTO carJourneyDetail : carDetailsList)
                {

                    travelJourneyDetailDTO = new TravelJourneyDetailDTO();
                    travelJourneyDetailDTO.setJourney(AppUtility.blankToMinus(carJourneyDetail.getType()));
                    travelJourneyDetailDTO.setDeparture(AppUtility.blankToMinus(carJourneyDetail.getSource()));
                    travelJourneyDetailDTO.setArrival(AppUtility.blankToMinus(carJourneyDetail.getDestination()));
                    travelJourneyDetailDTO.setDepartureDate(AppUtility.blankToMinus(carJourneyDetail.getDateOfJourney()));

                    travelJourneyDetailDTO.setPreferredTime(carJourneyDetail.getPreferredTimeDesc());
                    travelJourneyDetailDTO.setPreferredTimeDesc(AppUtility.blankToMinus(carJourneyDetail.getPreferredTimeModeDesc()) + " " + carJourneyDetail.getPreferredTimeDesc());
                    
                    travelJourneyDetailDTO.setTravelMode(Constants.CAR);
                    travelJourneyDetailDTO.setTravelClass(AppUtility.blankToMinus(carJourneyDetail.getCarClass()));
                    travelJourneyDetailDTO.setPrefferedTravel(AppUtility.blankToMinus(carJourneyDetail.getCarCategory()));
                    travelJourneyDetailDTO.setPrefferedSeat(carJourneyDetail.getLocationDesc());
                    travelJourneyDetailDTO.setStayType(AppUtility.blankToMinus(""));
                    travelJourneyDetailDTO.setOtherInfo(AppUtility.checkNull(carJourneyDetail.getMobileNo())?("N.A."):("Mobile No." + " - " + AppUtility.blankToMinus(carJourneyDetail.getMobileNo())));
                    travelJourneyDetailDTOList.add(travelJourneyDetailDTO);

                }
            }

            /** Car Details Ends Here **/

            /** To Sort Time and date wise **/
            Collections.sort(travelJourneyDetailDTOList);
            for(int listCounter = 0; listCounter < travelJourneyDetailDTOList.size(); listCounter++)
            {
                TravelJourneyDetailDTO tempTravelObj =  travelJourneyDetailDTOList.get(listCounter);
                    
                    if(listCounter == 0)
                    {       tempTravelObj.setJourney("Onward");
                    }
                    else if(listCounter == travelJourneyDetailDTOList.size()-1)
                    {
                            tempTravelObj.setJourney("Return");
                    }
                    else
                    {
                            tempTravelObj.setJourney("Intermediate");
                    }
            }

            /** Iterate over all the travel type requests **/
            for (TravelJourneyDetailDTO tempJourneyDetailDTO : travelJourneyDetailDTOList)
            {
                strMailMsg.append(Constants.HTML_TR_O).append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getJourney()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getDeparture()) + Constants.HTML_TD_C )
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getArrival()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getDepartureDate()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + tempJourneyDetailDTO.getPreferredTimeDesc() + Constants.HTML_TD_C)
                .append("<td " + td + " >" + tempJourneyDetailDTO.getTravelMode() + Constants.HTML_TD_C);

                if ("flight".equalsIgnoreCase(tempJourneyDetailDTO.getTravelMode().trim()) && !"economy".equalsIgnoreCase(AppUtility.blankToMinus(tempJourneyDetailDTO.getTravelClass()).trim()))
                {
                    strMailMsg.append("<td " + td + " ><font color='blue' style='font-weight: bold;'>" + tempJourneyDetailDTO.getTravelClass() + "</font></td>");
                } else
                {
                    strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getTravelClass()) + Constants.HTML_TD_C);
                }

                strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getPrefferedTravel()) + Constants.HTML_TD_C)
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getPrefferedSeat()) + Constants.HTML_TD_C )
                .append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getStayType()) + Constants.HTML_TD_C);
                 if(otherInfoExist) {
                     strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(tempJourneyDetailDTO.getOtherInfo()) + Constants.HTML_TD_C);
                 }
                strMailMsg.append(Constants.HTML_TR_C);
            }
           
            if (flightJourneyDetails != null && !AppUtility.isBlankOrHyphenString(flightJourneyDetails.getFlightJourneyRemarks()))
            {
                flightRemarks = AppUtility.checkNullString(flightJourneyDetails.getFlightJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + " >").append("Flight Remarks").append("</td>").append("<td " + td + " colspan="+colspan+" >").append(AppUtility.blankToMinus(flightRemarks))
                        .append("</td>").append("</tr>");

            }
            if (trainJourneyDetails != null && !AppUtility.isBlankOrHyphenString(trainJourneyDetails.getTrainJourneyRemarks()))
            {
                trainRemarks = AppUtility.checkNullString(trainJourneyDetails.getTrainJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + ">").append("Train Remarks").append("</td>").append("<td " + td + " colspan="+colspan+" >").append(AppUtility.blankToMinus(trainRemarks))
                        .append("</td>").append("</tr>");
            }
            if (carJourneyDetails != null && !AppUtility.isBlankOrHyphenString(carJourneyDetails.getCarJourneyRemarks()))
            {
                carRemarks = AppUtility.checkNullString(carJourneyDetails.getCarJourneyRemarks());
                strMailMsg.append("<tr>").append("<td " + td + ">").append("Car Remarks").append("</td>").append("<td " + td + " colspan="+colspan+">").append(AppUtility.blankToMinus(carRemarks))
                        .append("</td>").append("</tr>");
            }
           

            strMailMsg.append(Constants.HTML_TABLE_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C);
            
            
            
            strMailMsg.append("<div " + containerScope + ">")
            .append("<div " + tableResponsive + ">")
            .append("<table " + table + ">");
            /** Accommodation Detail Section Start **/
            log.info("[getMailBodyForMataIndia] Accommodation Details Section start ---->");
            if ("Y".equals(requestInfo.getAccommodationDetailsFlag()) && accomodationDetailsDTO != null)
            {
                List<AccommodationDTO> accommodationList = accomodationDetailsList;
                AccommodationDTO accommodation = null;

                if (accommodationList != null && !accommodationList.isEmpty())
                {
                    strMailMsg
                            // .append("<tr><td height=\"0\" colspan=\""+headerColspan+"\"
                            // "+reportSubHeadingStr+">Accommodation</td></tr>")
                    	    .append("<tr><th colspan=\"9\" " + th + " >Accommodation Details</th></tr>")
                            .append("<tr><th " + th + " >S.No.</th>")
                            .append("<th " + th + " >Stay Type</th><th " + th + " >Currency</th>")
                            .append("<th " + th + " >Budget</th>")
                            .append("<th " + th + " >Check In Date</th>")
                            .append("<th " + th + " >Check Out Date</th>")
                            .append("<th " + th + " >Preferred Place</th>")
                            .append("<th " + th + " >Address</th>");
                            if(otherAccExist) {
                        	strMailMsg.append("<th " + th + " >Reason</th>");
                        	
                            }

                    strMailMsg.append(Constants.HTML_TR_C);

                    for (int i = 0; i < accommodationList.size(); i++)
                    {
                        accommodation = accommodationList.get(i);

			strMailMsg.append(Constants.HTML_TR_O);
			strMailMsg.append("<td " + td + " >" + (i + 1) + Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getStayTypeDesc())+ Constants.HTML_TD_C);
			
			if(accommodation.getStayType()==1) {
			    strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCurrency())+ Constants.HTML_TD_C);
			    strMailMsg.append("<td " + td + " >"+ AppUtility.blankToMinus(AppUtility.checkNullString(accommodation.getBudget()))+ Constants.HTML_TD_C);
			}else {
			    strMailMsg.append("<td " + td + " >-" + Constants.HTML_TD_C);
			    strMailMsg.append("<td " + td + " >-"+ Constants.HTML_TD_C);
			}
			
			
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCheckInDate())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getCheckOutDate())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getPlace())+ Constants.HTML_TD_C);
			strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getAddress())+ Constants.HTML_TD_C);
			 if(otherAccExist) {
			     strMailMsg.append("<td " + td + " >" + AppUtility.blankToMinus(accommodation.getReason())+ Constants.HTML_TD_C);
			 }

                        strMailMsg.append(Constants.HTML_TR_C);
                    }
                    

                }
            }
            log.info("[getMailBodyForMataIndia] Accommodation Details Section end.");
            // Accommodation Detail Section End
            strMailMsg.append(Constants.HTML_TABLE_C)
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_DIV_C)
            .append("</td></tr>");
        }
        log.info("[getMailBodyForMataIndia] Itinerary Details Section end.");
        // Itinerary Details Section End

        // Advance Forex Details Start

        log.info("[getMailBodyForMataIndia] Advance/Expense Detail Section start ---->");
        if ((requestInfo.getAdvanceForexFlag() != null && requestInfo.getAdvanceForexFlag().equals("Y")) && (advanceDTOList != null && !advanceDTOList.isEmpty()))
        {

            strMailMsg.append("<tr><td>")
            .append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Advance/Forex Details</h1>")
            .append(Constants.HTML_DIV_C).append(Constants.HTML_BR_C)
            .append("<div " + containerScope + ">")
            .append("<div " + tableResponsive + ">")
            .append( "<table width=\"100%\" align=\"left\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" " + table + " bordercolor=\"#c7c7c5\" style=\"border-collapse: collapse;margin:0;\"> " );
           
            if (travelType.equalsIgnoreCase("D")) {
        	
        	strMailMsg.append("<tr><th height=\"0\" colspan=\"10\" " + th + ">Advance Required</th>")
        	.append("<th  height=\"0\" colspan=\"4\" " + th + ">" + requestInfo.getAdvanceForex() + "</th></tr>");
            }
            if (travelType.equalsIgnoreCase("I")) {
        	
        	strMailMsg.append("<tr><th height=\"0\" colspan=\"10\" " + th + ">Forex Details</th>")
        	.append("<th  height=\"0\" colspan=\"5\" " + th + ">" + requestInfo.getAdvanceForex() + "</th></tr>");
            }

            strMailMsg.append(Constants.HTML_TR_O)
            .append("<th " + th + " rowspan=\"2\" nowrap=\"nowrap\">S No.</th>")
            .append("<th " + th + " rowspan=\"2\" >Traveller Name</th><th " + th + " rowspan=\"2\" >Currency</th>" )
            .append("<th " + th + " colspan=\"3\" >Daily Allowances</th><th " + th + " colspan=\"3\" >Hotel Charges</th>")
            .append("<th " + th + " rowspan=\"2\" >Contingencies<br>(C)</th><th " + th + " rowspan=\"2\" >Others<br>(D)</th>")
            .append("<th " + th + " rowspan=\"2\" >Total<br>(A+B+C+D)</th><th " + th + " rowspan=\"2\" >Total Amount</th>")
            .append("<th " + th + " rowspan=\"2\" >Remarks for Contingencies/Any other Expenditure</th>");
            
            if (travelType.equalsIgnoreCase("I"))
            {
                strMailMsg.append("<th " + th + " rowspan=\"2\" >Currency Denomination Details</th>");
            }
            strMailMsg.append(Constants.HTML_TR_C);

            strMailMsg.append(Constants.HTML_TR_O)
            .append("<th " + th + " nowrap=\"nowrap\">Exp/Day</th>")
            .append("<th " + th + " >Day(s)</th><th " + th + " >Total<br>(A)</th>")
            .append("<th " + th + " nowrap=\"nowrap\">Exp/Day</th>")
            .append("<th " + th + " >Day(s)</th><th " + th + " >Total<br>(B)</th>")
            .append(Constants.HTML_TR_C);

            AdvanceDTO advanceForex = null;
            for (int i = 0; i < advanceDTOList.size(); i++)
            {
                advanceForex = advanceDTOList.get(i);
                strMailMsg.append(Constants.HTML_TR_O).append("<td " + td + " nowrap=\"nowrap\">" + (i + 1) + Constants.HTML_TD_C).append(
                        "<td " + td + " >" + advanceForex.getTravellerName() + Constants.HTML_TD_C
                        ).append("<td  " + td + " >" + advanceForex.getCurrencyCode() + Constants.HTML_TD_C).append(
                                "<td " + tdAdv + ">" + advanceForex.getDailyAllowanceExpPerDay() + Constants.HTML_TD_C
                                )
                        .append("<td  " + tdAdv + ">" + advanceForex.getDailyAllowanceTourDays() + Constants.HTML_TD_C).append("<td " + tdAdv + "><b>" + advanceForex.getDailyAllowanceExpTotal() +"</b>" +Constants.HTML_TD_C).append("<td  " + tdAdv + ">" + advanceForex.getHotelChargesExpPerDay() + Constants.HTML_TD_C).append("<td " + tdAdv + ">" + advanceForex.getHotelChargesTourDays() + Constants.HTML_TD_C).append("<td  " + tdAdv + "><b>" + advanceForex.getHotelChargesExpTotal() +"</b>"+ Constants.HTML_TD_C).append("<td " + tdAdv + "><b>" + advanceForex.getContingencies() +"</b>"+ Constants.HTML_TD_C).append("<td  " + tdAdv + "><b>" + advanceForex.getOthers() +"</b>"+ Constants.HTML_TD_C).append("<td " + tdAdv + "><b>" + advanceForex.getTotal() +"</b>"+ Constants.HTML_TD_C).append("<td  " + tdAdv + "><b>" + advanceForex.getTotalINR() +"</b>"+ Constants.HTML_TD_C).append("<td " + td + ">" + advanceForex.getRemarks() + Constants.HTML_TD_C);
                if (travelType.equalsIgnoreCase("I"))
                {
                    strMailMsg.append("<td " + tdAdv + ">" + advanceForex.getCashBreakupRemarks() + Constants.HTML_TD_C);
                }
                strMailMsg.append(Constants.HTML_TR_C);

            }
            strMailMsg.append(Constants.HTML_TABLE_C).append(Constants.HTML_DIV_C).append(Constants.HTML_DIV_C);

            strMailMsg.append(Constants.HTML_TR_C);
            strMailMsg.append(Constants.HTML_TD_C);
        }
        // Advance Forex Detail Ends

        // Total Travel Fare Starts Here
        if ("Y".equals(requestInfo.getTotalTravelFareDetailsFlag()) && !AppUtility.checkNull(travelFare))
        {
            strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O)
            .append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Total Travel Fare</h1>")
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C)
            .append("<table " + containerScope + ">")

                    .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Currency")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(travelFare.getCurrencyCode()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Total Fare")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToNA(travelFare.getFareAmount()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        // Total Travel Fare Ends Here
        
        // YTM Budget Start
        if ("Y".equals(requestInfo.getBudgetActualDetailsFlag()) && !AppUtility.checkNull(actualBudgetDetailDTO))
        {
            strMailMsg
            .append(Constants.HTML_TR_O)
            .append(Constants.HTML_TD_O)
            .append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Budget Actual Details</h1>")
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C)
            .append("<table " + containerScope + ">")

                    .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("YTM Budget")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToNA(actualBudgetDetailDTO.getYtmBudget()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >").append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >").append("<strong " + labelFontSize + ">")
		    .append("YTD Actual").append(Constants.HTML_STRONG_C).append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C).append("<span " + value + " >")
		    .append(AppUtility.blankToNA(actualBudgetDetailDTO.getYtdActual())).append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >").append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Available Budget")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToNA(actualBudgetDetailDTO.getAvailableBudget()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Estimate")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToNA(actualBudgetDetailDTO.getEstimate()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col12 + " colspan='4'>")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Remarks")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(actualBudgetDetailDTO.getExpRemarks()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append(Constants.HTML_TABLE_C).append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);
        }
        // YTM Actual Budget End
        
        // Billing Insturctions & Approvers Details Start
        if (approverDTO != null)
        {
            strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O).append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Billing Instructions and Approvers</h1>")
            .append( Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C)
            .append("<table " + containerScope + ">")

                    .append("<tr " + rowWithSeperator + " >")
                    .append("<td " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("Billing Client")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(approverDTO.getSiteName()))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append("<td " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("Billing Approver")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(approverDTO.getName()))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append("<td " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("Billing Client - Location")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(requestInfo.getSiteLocation()))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append("<td " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("GST No.")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(requestInfo.getGstNo()))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append("<tr " + rowWithSeperator + " >")

                    .append("<td " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("Approver Level 1")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(approverLevel1))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append("<td colspan='3' " + col3 + " >")
                    .append("<div " + formGroup + " >")
                    .append("<span " + controlLable + " >")
                    .append("<strong " + labelFontSize + ">")
                    .append("Approver Level 2")
                    .append(Constants.HTML_STRONG_C)
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_BR_C)
                    .append("<span " + value + " >")
                    .append(AppUtility.blankToMinus(approverLevel2))
                    .append(Constants.HTML_SPAN_C)
                    .append(Constants.HTML_DIV_C)
                    .append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append("</table>").append("</td>").append("</tr>");

        }
       // Billing Instructions and approval level Ends here
      
        
     // Proof of Identity Detail Section Start
     			log.info("[getMailBodyForMataIndia] Proof of Identity Detail Section start ---->");
     			if(travelType.equalsIgnoreCase("D")){
     			  strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O).append("<div " + headingScope + ">")
     		            .append("<h1 " + h1 + ">Identity Proof Details</h1>")
     		            .append( Constants.HTML_DIV_C)
     		            .append(Constants.HTML_BR_C)
     		            .append("<table " + containerScope + ">")

     		                    .append("<tr " + rowWithSeperator + " >")
     		                    .append("<td " + col6 + " >")
     		                    .append("<div " + formGroup + " >")
     		                    .append("<span " + controlLable + " >")
     		                    .append("<strong " + labelFontSize + ">")
     		                    .append("Proof of Identity")
     		                    .append(Constants.HTML_STRONG_C)
     		                    .append(Constants.HTML_SPAN_C)
     		                    .append(Constants.HTML_BR_C)
     		                    .append("<span " + value + " >")
     		                    .append(AppUtility.blankToMinus(traveller.getIdentityProofType()))
     		                    .append(Constants.HTML_SPAN_C)
     		                    .append(Constants.HTML_DIV_C)
     		                    .append(Constants.HTML_TD_C)

     		                    .append("<td " + col6 + " >")
     		                    .append("<div " + formGroup + " >")
     		                    .append("<span " + controlLable + " >")
     		                    .append("<strong " + labelFontSize + ">")
     		                    .append("Identity Proof Number")
     		                    .append(Constants.HTML_STRONG_C)
     		                    .append(Constants.HTML_SPAN_C)
     		                    .append(Constants.HTML_BR_C)
     		                    .append("<span " + value + " >")
     		                    .append(AppUtility.blankToMinus(traveller.getIdentityProofNo()))
     		                    .append(Constants.HTML_SPAN_C)
     		                    .append(Constants.HTML_DIV_C)
     		                    .append(Constants.HTML_TD_C);
     			  
     			  	strMailMsg.append("</tr>")
     			  	.append("</table>").append("</td>").append("</tr>");
     			  
     			    
     				
     			}
     			log.info("[getMailBodyForMataIndia] Proof of Identity Detail Section end.");
     			// Proof of Identity Detail Section End			
        
        
        /// Passport and Visa Information

        if ("Y".equals(requestInfo.getTravelVisaFlag()) && !AppUtility.checkNull(passport))
        {

            strMailMsg.append(Constants.HTML_TR_O).append(Constants.HTML_TD_O)
            .append("<div " + headingScope + ">")
            .append("<h1 " + h1 + ">Passport/ Visa/ Insurance Information</h1>")
            .append(Constants.HTML_DIV_C)
            .append(Constants.HTML_BR_C);

            strMailMsg.append("<table " + containerScope + ">")
            .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Passport No.")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(passport.getPassportNo()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Nationality")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(passport.getNationality()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Place of Issue")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(passport.getIssuePlace()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Date of Issue")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(passport.getIssueDate()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Date of Expire")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(passport.getExpiryDate()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >").append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Visa Required")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.boolToString(requestInfo.isVisaRequired()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >").append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >").append("<strong " + labelFontSize + ">")
		    .append("Travel Insurance Required").append(Constants.HTML_STRONG_C).append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C).append("<span " + value + " >")
		    .append(AppUtility.boolToString(requestInfo.isInsuranceRequired())).append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C).append(Constants.HTML_TD_C)
		    
		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Nominee")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(insurance == null ? null : insurance.getNominee()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)
		    
		    .append(Constants.HTML_TR_C)
		    
		    .append("<tr " + rowWithSeperator + " >")

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Relation")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(insurance == null ? null : insurance.getRelation()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

		    .append("<td " + col3 + " >")
		    .append("<div " + formGroup + " >")
		    .append("<span " + controlLable + " >")
		    .append("<strong " + labelFontSize + ">")
		    .append("Insurance Comments")
		    .append(Constants.HTML_STRONG_C)
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_BR_C)
		    .append("<span " + value + " >")
		    .append(AppUtility.blankToMinus(insurance == null ? null : insurance.getComments()))
		    .append(Constants.HTML_SPAN_C)
		    .append(Constants.HTML_DIV_C)
		    .append(Constants.HTML_TD_C)

                    .append(Constants.HTML_TR_C)

                    .append(Constants.HTML_TABLE_C);
        }

        //Visa Details Stars Here
        if ("Y".equals(requestInfo.getTravelVisaFlag()) && visaDetailList != null && !visaDetailList.isEmpty())
        {

            log.info("[getMailBodyForMataIndia] Visa Detail Section start ---->");
            strMailMsg.append(
                    "<table width=\"100%\" align=\"left\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" " + table + " bordercolor=\"#c7c7c5\" style=\"border-collapse: collapse;margin:0;\"> ");

            strMailMsg.append("<tr>");
            strMailMsg.append("<th " + th + ">Visa Country</th>");
            strMailMsg.append("<th " + th + ">Visa Valid From</th>");
            strMailMsg.append("<th " + th + ">Visa Valid To</th>");
            strMailMsg.append("<th " + th + ">Visa Duration</th>");
            strMailMsg.append("<th " + th + ">Visa Comments</th>");
            strMailMsg.append("<th " + th + ">Validity</th>");
            strMailMsg.append("</tr>");

            for (int i = 0; i < visaDetailList.size(); i++)
            {
                visa = visaDetailList.get(i);

                strMailMsg.append("<tr>");
                strMailMsg.append("<td " + td + ">" + AppUtility.blankToMinus(visa.getCountry()).trim() + "</td>");
                strMailMsg.append("<td " + td + ">" + AppUtility.blankToMinus(visa.getValidFrom()).trim() + "</td>");
                strMailMsg.append("<td " + td + ">" + AppUtility.blankToMinus(visa.getValidTo()).trim() + "</td>");
                strMailMsg.append("<td " + td + ">" + AppUtility.blankToMinus(visa.getStayDuration()).trim() + "</td>");
                strMailMsg.append("<td " + td + ">" + AppUtility.blankToMinus(visa.getComment()).trim() + "</td>");
                if (visa.getValidityFlag().trim().equalsIgnoreCase("Y"))
                {
                    strMailMsg.append("<td " + td + ">Valid</td>");
                } else
                {
                    strMailMsg.append("<td " + td + ">Invalid</td>");
                }
                strMailMsg.append("</tr>");
            }
            strMailMsg.append("</table>");

            log.info("[getMailBodyForMataIndia] Insurance Detail Section end.");
            
            strMailMsg.append(Constants.HTML_TD_C).append(Constants.HTML_TR_C);

        }

        // Visa Details End here

        
        // Approvers List Section Start
        log.info("[getMailBodyForMataIndia] Approvers List Section start ---->");
        ApproverDTO approverObj = null;
        if (approversList != null && !approversList.isEmpty())
        {
	    strMailMsg.append("<table " + table + ">")
	    .append(Constants.HTML_TR_O)
	    .append(Constants.HTML_TD_O)
	    .append("<div " + headingScope + ">")
	    .append("<h1 " + h1 + ">Approval Workflow</h1>")
	    .append(Constants.HTML_DIV_C)
	    .append(Constants.HTML_BR_C)
	    .append("<div " + containerScope + ">")
	    .append("<div " + tableResponsive + ">")
	    .append("<table " + table + ">")
	    .append("<tr><td>")
	    .append("<table width=\"100%\" align=\"left\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" " + table + " bordercolor=\"#c7c7c5\">")
	    .append("<tr><th  " + th + " nowrap=\"nowrap\">S No.</th>")
	    .append("<th " + th + ">Name</th><th " + th + ">Designation</th>")
	    .append("<th " + th + ">Status</th><th " + th + ">Approval Date Time</th></tr>");

            for (int i = 0; i < approversList.size(); i++)
            {
                approverObj = approversList.get(i);
                String apprvTime = (approverObj.getApproveTime() == null || approverObj.getApproveTime().trim().equals("")) ? "" : "  " + approverObj.getApproveTime().trim();

		strMailMsg.append("<tr><td  " + td + " >" + (i + 1) + "</td>")
			.append("<td " + td + ">" + approverObj.getName() + "</td><td " + td + ">"+ approverObj.getDesignationName() + "</td>")
			.append("<td " + td + ">"+ ("10".equals(approverObj.getApproveStatus()) ? "Approved" : "Pending") + "</td>")
			.append("<td " + td + ">" + approverObj.getApproveDate().trim() + apprvTime + "</td></tr>");
            }
            strMailMsg.append("</table></td></tr>");
        }
        log.info("[getMailBodyForMataIndia] Approvers List Section end.");
        // Approvers List Section End

       

       

        strMailMsg.append("</table></div></div></td></tr>");
        strMailMsg.append("</table><br></div></body></html>");

        return strMailMsg.toString();

    }

    public String getMailBodyForCancellation(String purchaseRequisitionId, String mailUserId, String cancelComments, String travelType, String sUserId)
    {

        String strMailUserId = "";
        String sSqlStr = "";
        String strRequistionId = null;
        String strRequistionNumber = null;
        String strMailSubject = null;
        String strMailRefNumber = null;
        String strRequistionCreatorName = null;
        String strRequistionCreatorMail = null;
        String strRequistionCreatedDate = null;
        String strRequisitionComments = null;
        StringBuilder strMailMsg = new StringBuilder();
        String strstrRequistionApproverName = null;
        String strstrRequistionApproverEmail = null;
        String strSiteName = null;
        String strTravelAgencyTypeId = null;
        String strUserNm = "";
        String strTravelDate = "";
        String strTravellerSex = "";
        String strSex = "";
        String strSql = "";
        String strTravelFrom = "";
        String strTravelTo = "";
        String strCreationDate = "";
        String strCurrentDate = "";
        String tType = "";

        String strGroupTravel = "";
        String strGroupTravelFlag = "";
        StringBuilder strMailIdTravelMata = new StringBuilder();
        String strTravelMataMailSql = "";
        String strMailToMATA = null;
        String strSiteID = null;

        strRequistionId = purchaseRequisitionId;
        strMailUserId = mailUserId;
        strRequisitionComments = cancelComments;
        String reqTyp = travelType;
        String reqTyp1 = "";
        String strsesLanguage = null;
        String strCurrentYear = null;
        if (reqTyp != null && reqTyp.equals("D"))
        {
            reqTyp = Constants.DOMESTIC_S_TRAVEL;
            reqTyp1 = "label.mail.domestictravel";
        }
        if (reqTyp != null && reqTyp.equals("I"))
        {
            reqTyp = Constants.INTERNATIONAL_S_TRAVEL;
            reqTyp1 = "label.mail.internationaltravel";
        }

        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy H:mm");
        strCurrentDate = (sdf.format(currentDate)).trim();
        strCurrentYear = strCurrentDate.split(" ")[2];

        // FETCH THE LATEST MAIL ID FROM REQ_MAILBOX
        strMailRefNumber = getMaxMailRefNo();// Mail Reference Number

        // FETCH Requisition Details
        if (reqTyp.equals(Constants.DOMESTIC_S_TRAVEL))
        {
            tType = "Domestic Travel Requisition No:";
            sSqlStr = "SELECT TRAVEL_REQ_NO ,.DBO.user_name('" + strMailUserId + "') AS ORIGINATOR,DBO.USEREMAIL('" + strMailUserId + "')AS ORIGINATOR_MAIL,.DBO.USER_NAME('" + sUserId
                    + "') AS APPROVER_NAME,DBO.USEREMAIL('" + sUserId
                    + "') AS APPROVER_MAIL,convert(varchar(30), C_DATETIME,113) C_DATETIME,.DBO.USER_NAME(TRAVELLER_ID) AS TRAVELLER_NAME ,convert(varchar(10),TRAVEL_DATE,103) AS TRAVEL_DATE,SEX ,.DBO.SITENAME(T.SITE_ID) AS SITE_NAME,(select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = T.SITE_ID) as TRAVEL_AGENCY_ID, ISNULL(T.GROUP_TRAVEL_FLAG,'N') AS GROUP_TRAVEL_FLAG, (select MAIL_TO_MATA from M_SITE where SITE_ID = T.SITE_ID) as MAIL_TO_MATA, ISNULL(T.SITE_ID,'0') AS SITE_ID  FROM T_TRAVEL_DETAIL_DOM  T  WHERE TRAVEL_ID="
                    + strRequistionId + "";
        } else
        {
            tType = "International Travel Requisition No:";
            sSqlStr = "SELECT TRAVEL_REQ_NO ,.DBO.user_name('" + strMailUserId + "') AS ORIGINATOR,DBO.USEREMAIL('" + strMailUserId + "')AS ORIGINATOR_MAIL,.DBO.USER_NAME('" + sUserId
                    + "') AS APPROVER_NAME,DBO.USEREMAIL('" + sUserId
                    + "') AS APPROVER_MAIL,convert(varchar(30), C_DATETIME,113) C_DATETIME,.DBO.USER_NAME(TRAVELLER_ID) AS TRAVELLER_NAME ,convert(varchar(10),TRAVEL_DATE,103) AS TRAVEL_DATE ,SEX ,.DBO.SITENAME(T.SITE_ID) AS SITE_NAME,(select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = T.SITE_ID) as TRAVEL_AGENCY_ID, ISNULL(T.GROUP_TRAVEL_FLAG,'N') AS GROUP_TRAVEL_FLAG, (select MAIL_TO_MATA from M_SITE where SITE_ID = T.SITE_ID) as MAIL_TO_MATA, ISNULL(T.SITE_ID,'0') AS SITE_ID  FROM T_TRAVEL_DETAIL_INT  T  WHERE TRAVEL_ID="
                    + strRequistionId + "";
        }

        Map<String, Object> requisitionDetails = getFirstResult(sSqlStr);
        if (requisitionDetails != null)
        {
            strRequistionNumber = AppUtility.blankToNA(requisitionDetails.get("TRAVEL_REQ_NO"));
            strRequistionCreatorName = AppUtility.blankToNA(requisitionDetails.get("ORIGINATOR"));
            strRequistionCreatorMail = AppUtility.blankToNA(requisitionDetails.get("ORIGINATOR_MAIL"));
            strstrRequistionApproverName = AppUtility.blankToNA(requisitionDetails.get("APPROVER_NAME"));
            strstrRequistionApproverEmail = AppUtility.blankToNA(requisitionDetails.get("APPROVER_MAIL"));
            String strRequistionCreatedDate1 = AppUtility.blankToNA(requisitionDetails.get("C_DATETIME"));

            String str = strRequistionCreatedDate1.substring(0, 17);
            strRequistionCreatedDate = str;

            strUserNm = AppUtility.blankToNA(requisitionDetails.get("TRAVELLER_NAME"));
            strTravelDate = AppUtility.blankToNA(requisitionDetails.get("TRAVEL_DATE"));
            strTravellerSex = AppUtility.blankToNA(requisitionDetails.get("SEX"));
            strSiteName = AppUtility.blankToNA(requisitionDetails.get("SITE_NAME"));
            strTravelAgencyTypeId = AppUtility.blankToNA(requisitionDetails.get("TRAVEL_AGENCY_ID"));
            strMailToMATA = AppUtility.blankToNA(requisitionDetails.get("MAIL_TO_MATA"));
            strSiteID = AppUtility.blankToNA(requisitionDetails.get("SITE_ID"));

            String groupGuestLabel = "";
            if (strTravelAgencyTypeId != null && strTravelAgencyTypeId.equals("2"))
            {
                groupGuestLabel = "label.global.guest";
            } else
            {
                groupGuestLabel = "label.approverequest.groupguest";
            }

            if (strTravellerSex != null && strTravellerSex.equals("1"))
            {
                strSex = "Mr.";
            } else
            {
                strSex = "Ms";
            }
            if (reqTyp.equals(Constants.DOMESTIC_S_TRAVEL))
            {
                strSql = "select * FROM  [dbo].[FN_GetDeparturecity](" + strRequistionId + ",'d')";

                strGroupTravelFlag = AppUtility.blankToNA(requisitionDetails.get("GROUP_TRAVEL_FLAG"));
                if (strGroupTravelFlag == null)
                {
                    strGroupTravel = "";
                }
                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {
                    strGroupTravel = appMessageUtility.getLabel("label.mail.groupguestisscheduledtotravel");
                    if (strTravelAgencyTypeId.equals("2"))
                    {
                        strGroupTravel = appMessageUtility.getLabel("label.mail.guestisscheduledtotravel");
                    }
                } else
                {
                    strGroupTravel = strSex + " " + "<b>" + strUserNm + "</b> " + appMessageUtility.getLabel("label.mail.isscheduledtotravel");
                }

                strTravelMataMailSql = "SELECT TRAVEL_MATA_EMAIL as MATAGMBH FROM M_SITE WHERE SITE_ID IN (SELECT SITE_ID FROM T_TRAVEL_DETAIL_DOM WHERE TRAVEL_ID=" + strRequistionId
                        + ") AND TRAVEL_AGENCY_ID=2 AND STATUS_ID=10";

            } else
            {

                strGroupTravelFlag = AppUtility.blankToNA(requisitionDetails.get("GROUP_TRAVEL_FLAG"));
                if (strGroupTravelFlag == null)
                {
                    strGroupTravel = "";
                }
                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {
                    strGroupTravel = appMessageUtility.getLabel("label.mail.groupguestisscheduledtotravel");
                    if (strTravelAgencyTypeId.equals("2"))
                    {
                        strGroupTravel = appMessageUtility.getLabel("label.mail.guestisscheduledtotravel");
                    }
                } else
                {
                    strGroupTravel = strSex + " " + "<b>" + strUserNm + "</b> " + appMessageUtility.getLabel("label.mail.isscheduledtotravel");
                }

                strSql = "select * FROM  [dbo].[FN_GetDeparturecity](" + strRequistionId + ",'i')";

                strTravelMataMailSql = "SELECT TRAVEL_MATA_EMAIL as MATAGMBH FROM M_SITE WHERE SITE_ID IN (SELECT SITE_ID FROM T_TRAVEL_DETAIL_INT WHERE TRAVEL_ID=" + strRequistionId
                        + ") AND TRAVEL_AGENCY_ID=2 AND STATUS_ID=10";

            }
            Map<String, Object> departureDetails = getFirstResult(strSql);
            if (departureDetails != null)
            {
                strTravelFrom = AppUtility.blankToNA(departureDetails.get("DEPARTURE_CITY"));
                strTravelTo = AppUtility.blankToNA(departureDetails.get("ARRIVAL_CITY"));
            }

            Map<String, Object> travelMataEmail = getFirstResult(strTravelMataMailSql);
            if (travelMataEmail != null)
            {
                strMailIdTravelMata.append(AppUtility.blankToNA(travelMataEmail.get("MATAGMBH")));
            }

            if ("N".equals(strMailToMATA))
            {
                strMailIdTravelMata.append("");
            }

            strCreationDate = strCurrentDate;
            if (!strRequistionCreatorMail.equals(strstrRequistionApproverEmail))
            {

                strSql = "SELECT LANGUAGE_PREF FROM M_USERINFO WHERE EMAIL =N'" + strRequistionCreatorMail + "' AND STATUS_ID=10";
                Map<String, Object> langPref = getFirstResult(strSql);
                if (langPref != null)
                {
                    strsesLanguage = AppUtility.blankToNA(langPref.get("LANGUAGE_PREF"));
                    if (strsesLanguage == null || strsesLanguage.equals(""))
                    {
                        strsesLanguage = "en_US";
                    }
                }

                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {
                    strMailSubject = appMessageUtility.getLabel("label.mail.starsnotification") + " " + tType + " '" + strRequistionNumber.trim() + "' "
                            + appMessageUtility.getLabel(groupGuestLabel) + " " + appMessageUtility.getLabel("label.mail.hasbeencancelled") + " ";// Mail Subject //added by vijay on 16/04/2007S
                } else
                {
                    strMailSubject = appMessageUtility.getLabel("label.mail.starsnotification") + " " + tType + " '" + strRequistionNumber.trim() + "' "
                            + appMessageUtility.getLabel("label.mail.hasbeencancelled") + " ";// Mail Subject //added by vijay on 16/04/2007S
                }

                try
                {

                    if (!strTravelAgencyTypeId.equals("2") && Constants.DOMESTIC_S_TRAVEL.equalsIgnoreCase(reqTyp) && "86".equals(strSiteID))
                    {

                        List<Map<String, Object>> rSet = jdbcTemplate.queryForList(
                                "(select USERID, ISNULL(EMAIL,'') AS EMAIL FROM M_USERINFO WHERE STATUS_ID=10 AND ROLE_ID='AC' AND SITE_ID='" + strSiteID
                                        + "') UNION (select UR.USERID, ISNULL(UI.EMAIL,'') AS EMAIL FROM M_USERROLE UR LEFT OUTER JOIN M_USERINFO UI ON UR.USERID=UI.USERID WHERE UR.STATUS_ID=10 AND UR.ROLE_ID='AC' AND UR.SITE_ID='"
                                        + strSiteID + "')"
                                );
                        Iterator<Map<String, Object>> emails = rSet.iterator();
                        while (emails.hasNext())
                        {
                            Map<String, Object> tempEmail = emails.next();
                            String acEmail = AppUtility.blankToNA(tempEmail.get(Constants.EMAIL)).trim();

                            if (strRequistionCreatorMail != null && !strRequistionCreatorMail.contains(acEmail) && strMailIdTravelMata != null && !strMailIdTravelMata.toString().contains(acEmail))
                            {
                                if ("".equals(
                                        strMailIdTravelMata.toString().trim()
                                        ) || (!"".equals(strMailIdTravelMata.toString().trim()) && ";".equals(strMailIdTravelMata.substring(strMailIdTravelMata.toString().trim().length() - 1))))
                                {
                                    strMailIdTravelMata.append(acEmail);
                                } else
                                {

                                    strMailIdTravelMata.append(";" + acEmail);
                                }
                            }
                        }

                    }

                    String strSSOUrl = sSSOUrlByMailid(strRequistionCreatorMail);

                    strMailMsg.append("<html><style>.formhead{ font-family:Arial;font-size:11px;font-style: normal;font-weight:normal;color:#000000;text-decoration:none;letter-spacing:normal;word-spacing:normal;border:1px #333333 solid;background:#E2E4D6;}</style><body bgcolor=\"#d0d0d0\">"+ "<br>");
                    strMailMsg.append(" <table style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%' width=80% border=0 cellspacing=0 cellpadding=0 align=center><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center>");
                    strMailMsg.append("</td></tr><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=10 style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%'><tr><td align='left' style='background:#aa1220;color:#fff;font-size:28px;padding:12px 12px 0;'><strong>"+ appMessageUtility.getLabel("label.mail.starsmailnotification") + "</strong></td>")
                    .append("<td  align='right' style='background:#aa1220;color:#fff;font-size:13px;padding:12px 12px 0;'>[" + strCreationDate + "]<br/>" + appMessageUtility.getLabel("label.mail.mailreferencenumber") + Constants.HTML_NBSP + strMailRefNumber + "/" + strCurrentYear + "</td></tr></table></td></tr><tr><td bgcolor=#000000> </td></tr><tr>");
                    strMailMsg.append( "<td bgcolor=#B6DCDC></td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#FFFFFF><table width=100% border=0 cellspacing=0 cellpadding=5>");
                    strMailMsg
                            .append("  <tr><td align=left style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:15px'>" + appMessageUtility.getLabel(reqTyp1) + " " + appMessageUtility.getLabel("label.mail.requisition") + Constants.HTML_NBSP + "" + " : [" + strRequistionNumber.trim() + "]&nbsp;|&nbsp;[" + "" + " " + strRequistionCreatedDate + "]</td>").append("<td  align=right style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:13px'>" + appMessageUtility.getLabel("label.mail.forinternalcirculationonly") + "</td></tr></table>")

                            .append("<table style='width:100%;font-family:Calibri;font-size:15px'><tr><td style='padding-left:10px;'><p style='color:black;'><br>" + "\n").append(appMessageUtility.getLabel("label.mail.dear") + " " + strRequistionCreatorName + ",</p><p>" + appMessageUtility.getLabel("label.global.requisitionnumber") + " " + strRequistionNumber.trim() + " " + appMessageUtility.getLabel("label.approverrequest.from") + " " + strSiteName + " " + appMessageUtility.getLabel("label.mail.whichwasgeneratedon") + " " + strRequistionCreatedDate + " " + appMessageUtility.getLabel("label.mail.hasbeencancelledbyme") + "</p>" + "\n").append("<u style='font-weight: bold;color: blue;'>" + appMessageUtility.getLabel("label.mail.requisitionsdetails") + ":-</u><br>" + strGroupTravel + "<br> Departure Date was " + strTravelDate + ".  </font>\n<br>").append("<u style='font-weight: bold;color: blue;'>" + appMessageUtility.getLabel("label.global.departurecity") + ":-</u><br>" + strTravelFrom + "\n<br>").append("<u style='font-weight: bold;color: blue;'>" + appMessageUtility.getLabel("label.global.arrivalcity") + ":-</u><br>" + strTravelTo + " \n<br>").append("<br><u style='font-weight: bold;color: blue;'>" + strstrRequistionApproverName + " Comments :- </u><br><label>" + strRequisitionComments.trim() + "</label> " + "\n")

                            .append("</font><br><br><p><font size=2 face=Verdana, Arial, Helvetica, sans-serif><a href='" + strSSOUrl + "'>"+ appMessageUtility.getLabel("label.mail.pleaseclickheretologintostars") + " </a> ")
                            .append("   </b></form></font><p><br><p>" + appMessageUtility.getLabel("label.mail.bestregards") + "<br><label>" + strstrRequistionApproverName.trim() + "<br></label></p></td><td  align=right style='padding:0 12px 12px;font-weight:300;font-size: 13px;vertical-align:bottom'>" + appMessageUtility.getLabel("label.mail.starsadministratorcanbecontacted") + "<br/><a href=mailto:administrator.stars@mind-infotech.com>administrator.stars@mind-infotech.com</a></td></tr></table></td>")

                            .append("</tr></td></tr></table></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=5>" + "\n")
                            .append("<tr><td  align=left style='padding:12px;font-weight:300;background:#e7e8e9;color:#6d6e71;font-size:11px;font-family:Calibri;'> <strong>" + appMessageUtility.getLabel("label.mail.disclaimer") + "</strong> : " + appMessageUtility.getLabel("label.mail.thiscommunicationissystemgenererated") + " " + "\n")
                            .append(appMessageUtility.getLabel("label.mail.pleasedonnotreplytothismail") + " <br>" + appMessageUtility.getLabel("label.mail.ifyouarenotthecorrectrecipientforthisnotification") + "<br><br>&copy;" + appMessageUtility.getLabel("label.mail.mindallrightsreserved") + "</td></tr></table></td></tr><tr><td bgcolor=#000000></td></tr>" + "\n")
                            .append("</table><p>&nbsp;</p></body></html>" + "\n");

                    log.info("*****************************************");
                    log.info("cancellation mail " + strMailMsg.toString());
                    log.info("*****************************************");

                    pushEmailDataToTable(
                            strRequistionId, strRequistionNumber, strRequistionCreatorMail, strstrRequistionApproverEmail, strMailIdTravelMata.toString(), strMailSubject, strMailMsg.toString(),
                            sUserId, "Signatory Cancel IT");

                } catch (Exception e)
                {
                    log.error("ERROR in getMailBodyForCancellation()" + e);
                }
            }
        }
        return strMailMsg.toString();
    }

    public String getRequisitionAdvanceInfo(String travelId, String travelType) throws SQLException
    {

        String advReq = "No";
        try
        {

            Map<String, Object> advanceRequiredMap = getFirstResult("SELECT DBO.[FN_GET_EXPENDITURE] (" + travelId + ",'" + travelType + "') AS ADVANCE_REQUIRED;");
            if (advanceRequiredMap != null)
            {
                advReq = "-".equals(AppUtility.checkNullString(advanceRequiredMap.get("ADVANCE_REQUIRED"))) ? "No" : AppUtility.checkNullString(advanceRequiredMap.get("ADVANCE_REQUIRED"));
            }

        } catch (Exception e)
        {

            log.error("Error occured in [getRequisitionAdvanceInfo] : " + e);
        }
        return advReq;
    }

    public List<Map<String, Object>> getCCMailListOnIntialTempList(String strSiteId, String strTravellerId, String strType)
    {

        String strNewQuery = "SELECT MDA.MATA_CC_MAIL CC_MAIL" + " FROM M_DEFAULT_APPROVERS AS MDA  WHERE MDA.APPLICATION_ID = 1 AND MDA.STATUS_ID = 10 AND MDA.SITE_ID = ? AND MDA.TRV_TYPE = ?"
                + " AND SP_ROLE =(select sp_role from M_userinfo where userid=? and loginstatus in ('ACTIVE','ENABLE')) ";

        List<Map<String, Object>> strCCMailOnInitialTempList = null;
        try
        {
            strCCMailOnInitialTempList = jdbcTemplate.queryForList(strNewQuery, new Object[] { strSiteId, strType, strTravellerId });

        } catch (Exception ex)
        {
            log.error("Error in EmailDAOImpl.java at getCCMailListOnIntialTempList()" + ex);
            ExceptionLogger.logExceptionToDB(ex);
        }
        return strCCMailOnInitialTempList;
    }

    public String getActiveCCUserEmail(String strActiveCCUserMail, String strSiteID)
    {

        StringBuilder activeCCMail = strActiveCCUserMail == null ? null : new StringBuilder(strActiveCCUserMail);
        try
        {
            log.info("sendRequisitionMailOnOriginating [Pending for Approval] Start of get Accountant(s) of Site block");

            List<Map<String, Object>> accountantBlockEmailList = jdbcTemplate.queryForList(
                    "(select USERID, ISNULL(EMAIL,'') AS EMAIL " + "FROM M_USERINFO WHERE STATUS_ID=10 AND ROLE_ID='AC' AND SITE_ID=?) "
                            + "UNION (select UR.USERID, ISNULL(UI.EMAIL,'') AS EMAIL FROM M_USERROLE UR " + "LEFT OUTER JOIN M_USERINFO UI ON UR.USERID=UI.USERID "
                            + "WHERE UR.STATUS_ID=10 AND UR.ROLE_ID='AC' AND UR.SITE_ID=?)",
                    new Object[] { strSiteID, strSiteID }
                    );

            if (accountantBlockEmailList != null)
            {

                for (Map<String, Object> tempMap : accountantBlockEmailList)
                {
                    String acEmail = AppUtility.checkNullString(tempMap.get(Constants.EMAIL));

                    if (strActiveCCUserMail != null && !strActiveCCUserMail.contains(acEmail))
                    {
                        if ("".equals(strActiveCCUserMail.trim()) || (!"".equals(strActiveCCUserMail.trim()) && ";".equals(strActiveCCUserMail.substring(strActiveCCUserMail.trim().length() - 1))))
                        {
                            activeCCMail.append(acEmail);
                        } else
                        {
                            activeCCMail.append(";").append(acEmail);
                        }
                    }
                }
            }

            log.info("sendRequisitionMailOnOriginating [Pending for Approval] End of get Accountant(s) of Site block");

        } catch (Exception e)
        {

            log.error("sendRequisitionMailOnOriginating [Pending for Approval] Error occured while getting Accountant(s) of Site : " + e);
        }
        return activeCCMail == null ? null : activeCCMail.toString();
    }

    public void pushStarsRequestDetailsToERPMata(String strTravelId, String strType, String sUserId)
    {

        try
        {
            pushStarsReqDetai.pushStarsReqDetailsToERPMATA(strTravelId, strType, sUserId, false, true);

        } catch (Exception ex)
        {
            log.info("pushStarsRequestDetailsToERPMata Error Occured while pushing STARS data to ERP Web Service.");
        }

    }

    public void sendMailToChairman(String purchaseRequisitionId, String reqType, String strRequisitionComments, String strTravellerSiteId, String strRoleId, String mailSubject, String userId)
    {

        // DECLARE VARIABLES
        String sSqlStr = "";
        String strRequistionId = null;
        String strRequistionNumber = null;
        String strMailSubject = null;
        String strMailRefNumber = null;
        String strRequistionCreatorName = null;
        String strRequistionCreatedDate = null;
        StringBuilder strMailMsg = new StringBuilder();
        String strstrRequistionApproverName = null;
        String strstrRequistionApproverEmail = null;
        String strstrRequistionNextApproverName = null;
        String strstrRequistionNextApproverEmail = null;

        String strVisaRequired = null;
        String strVisaRequiredComment = null;
        String strECRRequired = null;
        String strTravellerId = null;

        String strSiteName = null;
        String strUserNm = "";
        String strTravelDate = "";
        String strTravellerSex = "";
        String strSex = "";
        String strSql = "";

        String strSiteId = "";
        String strHrNm = "";
        String strHrMail = "";

        String strCreationDate = "";
        String tType = "";

        String strGroupTravel = "";
        String strGroupTravelFlag = "";
        String strBillingSite = "";
        String strTraverlDept = "";
        String strEmpcode = "";
        String strtext = "";
        String strBillingSiteName = "";
        String strTravelAgencyTypeId = "";

        String strHrNmofownsite = "";
        String strHrMailofownsite = "";
        String strTrvType = "";
        String strApproverId = "";
        String strLanguage = "";
        strMailSubject = mailSubject;
        String strMailMessage = "";
        ArrayList<String> aList = new ArrayList();
        String strCurrentDate = null;
        String strCurrentYear = null;
        strRequistionId = purchaseRequisitionId;

        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy H:mm");
        strCurrentDate = (sdf.format(currentDate)).trim();
        strCurrentYear = strCurrentDate.split(" ")[2];
        String strMailSubject1 = strMailSubject;
        String reqTyp1 = "";
        if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
        {
            reqTyp1 = "label.mail.domestictravel";
        } else
        {
            reqTyp1 = "label.mail.internationaltravel";
        }
        strMailRefNumber = getMaxMailRefNo();

        // ADDED BY MANOJ TO SEND EMAIL FROM CORRECT MAIL ID IN SPECIAL WORKFLOW CASE ON
        // 10 MAY 2012
        if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
        {
            strTrvType = "D";
        } else
        {
            strTrvType = "I";
        }

        sSqlStr = " SELECT TA.APPROVER_ID FROM T_APPROVERS TA" + " inner join VW_PAGE_ACCESS_PERMISSION PAP on pap.viewToUser=TA.PAP_APPROVER"
                + " and pap.pendingWithUser = TA.APPROVER_ID	 WHERE TA.TRAVEL_ID='" + strRequistionId + "'" + " AND TA.ORDER_ID=(SELECT MAX(ORDER_ID) FROM T_APPROVERS WHERE T_APPROVERS.TRAVEL_ID='"
                + strRequistionId + "'" + " AND SITE_ID='" + strTravellerSiteId + "' AND TRAVEL_TYPE='" + strTrvType + "' AND PAP_APPROVER='" + userId + "' AND PAP_FLAG ='A')" + "AND TA.SITE_ID='"
                + strTravellerSiteId + "' AND TA.TRAVEL_TYPE='" + strTrvType + "'";

        Map<String, Object> approverIdRes = getFirstResult(sSqlStr);
        if (approverIdRes != null)
        {
            strApproverId = AppUtility.getResultFromMap(approverIdRes, "APPROVER_ID");
        }

        if (strApproverId == null || strApproverId.equals(""))
        {
            strApproverId = userId;
        }

        if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
        {
            sSqlStr = "SELECT TRAVEL_REQ_NO ,.DBO.user_name(C_USERID) AS ORIGINATOR, DBO.USEREMAIL(C_USERID)AS ORIGINATOR_MAIL, .DBO.USER_NAME('" + strApproverId
                    + "') AS APPROVER_NAME,DBO.USEREMAIL('" + strApproverId + "') AS APPROVER_MAIL, ISNULL(RTRIM(.DBO.PRESENTAPPROVER(" + strRequistionId + ",'D'," + userId
                    + ")),'NA') AS NEXTWITH, ISNULL(RTRIM(.DBO.PRESENTAPPROVER_USERID(" + strRequistionId + ",'D'," + userId + ")),'NA') AS NEXTWITH_USERID, ISNULL(RTRIM(.DBO.PRESENTAPPROVER_MAIL("
                    + strRequistionId + ",'D'," + userId
                    + ")),'NA') AS NEXTWITH_MAIL, convert(varchar(30), C_DATETIME,113) REQCREATEDATE, .DBO.USER_NAME(TRAVELLER_ID) AS TRAVELLER_NAME ,convert(varchar(10),TRAVEL_DATE,103) TRAVEL_DATE,SEX ,.DBO.SITENAME(t.SITE_ID) AS SITE_NAME,t.SITE_ID,TRAVELLER_ID,dbo.DEPTNAME_FROM_USERID(TRAVELLER_ID) as TRAVELER_DEPT,isnull(EMP_CODE,'') as EMPCODE, T.GROUP_TRAVEL_FLAG, t.BILLING_SITE AS BILLING_SITE_ID, DBO.SITENAME(t.BILLING_SITE) as BILLING_SITE, t.BILLING_CLIENT, (select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = t.SITE_ID) as TRAVEL_AGENCY_ID FROM T_TRAVEL_DETAIL_DOM  t inner join m_userinfo on t.TRAVELLER_ID=m_userinfo.userid  WHERE TRAVEL_ID="
                    + strRequistionId + " ";
        } else
        {
            sSqlStr = "SELECT TRAVEL_REQ_NO ,.DBO.user_name(C_USERID) AS ORIGINATOR, DBO.USEREMAIL(C_USERID)AS ORIGINATOR_MAIL, .DBO.USER_NAME('" + strApproverId
                    + "') AS APPROVER_NAME, DBO.USEREMAIL('" + strApproverId + "') AS APPROVER_MAIL, ISNULL(RTRIM(.DBO.PRESENTAPPROVER(" + strRequistionId + ",'I'," + userId
                    + ")),'NA') AS NEXTWITH, ISNULL(RTRIM(.DBO.PRESENTAPPROVER_USERID(" + strRequistionId + ",'I'," + userId + ")),'NA') AS NEXTWITH_USERID, ISNULL(RTRIM(.DBO.PRESENTAPPROVER_MAIL("
                    + strRequistionId + ",'I'," + userId
                    + ")),'NA') AS NEXTWITH_MAIL, convert(varchar(30),C_DATETIME,113) REQCREATEDATE,.DBO.USER_NAME(TRAVELLER_ID) AS TRAVELLER_NAME ,convert(varchar(10),TRAVEL_DATE,103) TRAVEL_DATE, SEX, .DBO.SITENAME(T.SITE_ID) AS SITE_NAME,t.SITE_ID,TRAVELLER_ID,dbo.DEPTNAME_FROM_USERID(TRAVELLER_ID) as TRAVELER_DEPT,isnull(EMP_CODE,'') as EMPCODE,  T.GROUP_TRAVEL_FLAG, t.BILLING_SITE AS BILLING_SITE_ID, DBO.SITENAME(t.BILLING_SITE) as BILLING_SITE, t.BILLING_CLIENT, (select TRAVEL_AGENCY_ID from M_SITE where SITE_ID = T.SITE_ID) as TRAVEL_AGENCY_ID FROM T_TRAVEL_DETAIL_INT  T inner join m_userinfo on t.TRAVELLER_ID=m_userinfo.userid WHERE TRAVEL_ID="
                    + strRequistionId + "  ";
        }

        Map<String, Object> requestDetails = getFirstResult(sSqlStr);
        if (requestDetails != null)
        {
            strRequistionNumber = AppUtility.getResultFromMap(requestDetails, "TRAVEL_REQ_NO");
            strRequistionCreatorName = AppUtility.getResultFromMap(requestDetails, "ORIGINATOR"); // Creator Name
            strstrRequistionApproverName = AppUtility.getResultFromMap(requestDetails, "APPROVER_NAME");
            strstrRequistionApproverEmail = AppUtility.getResultFromMap(requestDetails, "APPROVER_MAIL");
            strstrRequistionNextApproverName = AppUtility.getResultFromMap(requestDetails, "NEXTWITH"); // NEXT APPROVER NAME
            strstrRequistionNextApproverEmail = AppUtility.getResultFromMap(requestDetails, "NEXTWITH_MAIL"); // NEXT APPROVER MAIL

            String strRequistionCreatedDate1 = AppUtility.getResultFromMap(requestDetails, "REQCREATEDATE");// Req Created Date & time
            String str = strRequistionCreatedDate1.substring(0, 17);
            strRequistionCreatedDate = str;

            strUserNm = AppUtility.getResultFromMap(requestDetails, "TRAVELLER_NAME");
            strTravelDate = AppUtility.getResultFromMap(requestDetails, "TRAVEL_DATE");
            strTravellerSex = AppUtility.getResultFromMap(requestDetails, "SEX"); // TRAVELLER SEX
            strSiteName = AppUtility.getResultFromMap(requestDetails, "SITE_NAME");
            strSiteId = AppUtility.getResultFromMap(requestDetails, "SITE_ID");
            strTravellerId = AppUtility.getResultFromMap(requestDetails, "TRAVELLER_ID");
            strTraverlDept = AppUtility.getResultFromMap(requestDetails, "TRAVELER_DEPT");
            strEmpcode = AppUtility.getResultFromMap(requestDetails, "EMPCODE");

            strBillingSiteName = AppUtility.getResultFromMap(requestDetails, "BILLING_SITE");
            String strbillingSiteid = AppUtility.getResultFromMap(requestDetails, "BILLING_SITE_ID");
            String strBillingClientName = AppUtility.getResultFromMap(requestDetails, "BILLING_CLIENT");
            strTravelAgencyTypeId = AppUtility.getResultFromMap(requestDetails, "TRAVEL_AGENCY_ID");

            if (!AppUtility.isBlankString(strEmpcode))
            {
                strtext = "(" + strEmpcode + ")";
            }

            if (("1").equals(strTravellerSex))
            {
                strSex = "Mr.";
            } else
            {
                strSex = "Ms";
            }

            if (("-1").equals(strbillingSiteid))
            {
                strBillingSiteName = strBillingClientName;
            }

            if (("0").equals(strbillingSiteid))
            {
                strBillingSiteName = strBillingClientName;
            }

            if (reqType.equals(Constants.DOMESTIC_S_TRAVEL))
            {
                tType = "Domestic Travel Requisition No:";

                strSql = "select * FROM  [dbo].[FN_GetDeparturecity](" + strRequistionId + ",'d')";

                strGroupTravelFlag = AppUtility.getResultFromMap(requestDetails, "GROUP_TRAVEL_FLAG");

                if (strGroupTravelFlag == null)
                {
                    strGroupTravel = "";
                    strGroupTravelFlag = "N";
                }

                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {

                    strGroupTravel = "A <B>Group</b> is scheduled to travel.";
                } else
                {

                    strGroupTravel = strSex + " " + " <b>" + strUserNm + " " + strtext + "</b> from <b> " + strTraverlDept + " </b> department is scheduled to travel.";
                }

            } else
            {

                strGroupTravelFlag = AppUtility.getResultFromMap(requestDetails, "GROUP_TRAVEL_FLAG");

                if (strGroupTravelFlag == null)
                {
                    strGroupTravel = "";
                    strGroupTravelFlag = "N";
                }

                if (strGroupTravelFlag != null && (strGroupTravelFlag.trim().equals("Y")))
                {
                    strGroupTravel = "A <B>Group</b> is scheduled to travel.";
                } else
                {
                    strGroupTravel = strSex + " " + " <b>" + strUserNm + " " + strtext + " </b> from <B>" + strTraverlDept + "</b> department  is scheduled to travel.";
                }

                tType = "International Travel Requisition No:";

            }

            if (reqType.equals(Constants.INTERNATIONAL_S_TRAVEL))
            {
                strSql = "SELECT ISNULL(VISA_REQUIRED,'1') AS VISA_REQUIRED, ISNULL(VISA_COMMENT,'-') AS VISA_COMMENT FROM T_TRAVEL_DETAIL_INT WHERE TRAVEL_ID=" + strRequistionId;

                Map<String, Object> visaReqMap = getFirstResult(strSql);
                if (visaReqMap != null)
                {
                    strVisaRequired = AppUtility.getResultFromMap(visaReqMap, "VISA_REQUIRED");
                    if (strVisaRequired.equals("2"))
                        strVisaRequired = "no";
                    else
                        strVisaRequired = "yes";
                    strVisaRequiredComment = AppUtility.getResultFromMap(visaReqMap, "VISA_COMMENT");
                }

            }

            strSql = "SELECT ISNULL(ECNR,'1') AS ECNR FROM M_USERINFO WHERE USERID=" + strTravellerId;

            Map<String, Object> ecnrDetailMap = getFirstResult(strSql);

            if (ecnrDetailMap != null)
            {
                strECRRequired = AppUtility.getResultFromMap(ecnrDetailMap, "ECNR");

                if (strECRRequired.equals("1"))
                {
                    strECRRequired = "yes";
                } else if (strECRRequired.equals("2"))
                {
                    strECRRequired = "no";
                } else if (strECRRequired.equals("3"))
                {
                    strECRRequired = "N/A";
                }
            }

            List<Map<String, Object>> hrMapList = null;
            if (strRoleId != null && strRoleId.equals("HR"))
            {
                sSqlStr = "SELECT DBO.USER_NAME(USERID) USERNAME,EMAIL FROM M_USERINFO WHERE SITE_ID=" + strTravellerSiteId + " AND ROLE_ID IN ('HR') AND STATUS_ID=10";

                hrMapList = jdbcTemplate.queryForList(sSqlStr);
                for (Map<String, Object> hrDetailsMap : hrMapList)
                {
                    strHrNm = AppUtility.getResultFromMap(hrDetailsMap, Constants.USERNAME);
                    strHrMail = AppUtility.getResultFromMap(hrDetailsMap, Constants.EMAIL);
                    aList.add(strHrNm);
                    aList.add(strHrMail);
                }
            } else if (strRoleId != null && strRoleId.equals("AC"))
            {
                sSqlStr = "SELECT DBO.USER_NAME(USERID) USERNAME,EMAIL FROM M_USERINFO WHERE SITE_ID=" + strTravellerSiteId + " AND ROLE_ID IN ('AC') AND STATUS_ID=10";

                hrMapList = jdbcTemplate.queryForList(sSqlStr);
                for (Map<String, Object> hrDetailsMap : hrMapList)
                {
                    strHrNm = AppUtility.getResultFromMap(hrDetailsMap, Constants.USERNAME);
                    strHrMail = AppUtility.getResultFromMap(hrDetailsMap, Constants.EMAIL);
                    aList.add(strHrNm);
                    aList.add(strHrMail);
                }
            } else if (strRoleId != null && strRoleId.equals("HR&AC"))
            {
                /// Query to find Billing Site
                if (reqType.equals(Constants.INTERNATIONAL_S_TRAVEL))
                {
                    sSqlStr = "SELECT  BILLING_SITE FROM  T_TRAVEL_DETAIL_INT TTIINT WHERE  (TRAVEL_ID = " + strRequistionId + ") AND 		  (STATUS_ID = 10)";
                } else
                {
                    sSqlStr = "SELECT  isnull(BILLING_SITE,-1) as BILLING_SITE   FROM T_TRAVEL_DETAIL_DOM TTDDOM WHERE (TRAVEL_ID = " + strRequistionId + ") AND (STATUS_ID = 10)";
                }

                Map<String, Object> billingSiteMap = getLastResult(sSqlStr);
                if (billingSiteMap != null)
                {

                    strBillingSite = AppUtility.getResultFromMap(billingSiteMap, "BILLING_SITE");

                }

                if (reqType.equals(Constants.INTERNATIONAL_S_TRAVEL))
                {

                    if (strBillingSite.equals("-1") || strBillingSite.equals("0"))
                    {

                        sSqlStr = "SELECT DBO.USER_NAME(USERID),EMAIL FROM M_USERINFO WHERE SITE_ID=" + strTravellerSiteId + " AND ROLE_ID IN ('HR','AC') AND STATUS_ID=10";
                    } else
                    {

                        sSqlStr = "SELECT dbo.user_name(MUINFO.USERID), MUINFO.EMAIL FROM  T_TRAVEL_DETAIL_INT TTDINT INNER JOIN "
                                + " M_USERINFO MUINFO ON TTDINT.BILLING_SITE = MUINFO.SITE_ID WHERE     (MUINFO.ROLE_ID IN ('ac','hr')) AND  (MUINFO.STATUS_ID = 10) AND (TTDINT.TRAVEL_ID = "
                                + strRequistionId + ")";

                    }

                } else
                {

                    if (strBillingSite.equals("-1") || strBillingSite.equals("0"))
                    {

                        sSqlStr = "SELECT DBO.USER_NAME(USERID) USERNAME,EMAIL FROM M_USERINFO WHERE SITE_ID=" + strTravellerSiteId + " AND ROLE_ID IN ('HR','AC') AND STATUS_ID=10";

                    } else
                    {
                        sSqlStr = "SELECT dbo.user_name(MUINFO.USERID) USERNAME, MUINFO.EMAIL FROM T_TRAVEL_DETAIL_DOM TTDDOM INNER JOIN "
                                + " M_USERINFO MUINFO ON TTDDOM.BILLING_SITE = MUINFO.SITE_ID WHERE     (MUINFO.ROLE_ID IN ('ac','hr')) AND " + " (MUINFO.STATUS_ID = 10) AND (TTDDOM.TRAVEL_ID = "
                                + strRequistionId + ") ";

                    }
                }

                hrMapList = jdbcTemplate.queryForList(sSqlStr);
                for (Map<String, Object> hrDetailsMap : hrMapList)
                {
                    strHrNm = AppUtility.getResultFromMap(hrDetailsMap, Constants.USERNAME);
                    strHrMail = AppUtility.getResultFromMap(hrDetailsMap, Constants.EMAIL);
                    aList.add(strHrNm);
                    aList.add(strHrMail);
                }

                if (strBillingSite.equals("-1") || strBillingSite.equals("0"))
                {
                } else
                {
                    if (!strTravellerSiteId.trim().equals(strBillingSite.trim()))
                    {
                        sSqlStr = "SELECT DBO.USER_NAME(USERID) USERNAME,EMAIL FROM M_USERINFO WHERE SITE_ID in(" + strTravellerSiteId + ") AND ROLE_ID IN ('AC') AND STATUS_ID=10";

                        hrMapList = jdbcTemplate.queryForList(sSqlStr);
                        for (Map<String, Object> hrDetailsMap : hrMapList)
                        {
                            strHrNm = AppUtility.getResultFromMap(hrDetailsMap, Constants.USERNAME);
                            strHrMail = AppUtility.getResultFromMap(hrDetailsMap, Constants.EMAIL);
                            aList.add(strHrNm);
                            aList.add(strHrMail);
                        }
                    }
                }

            }

            Iterator<String> itr = aList.iterator();

            while (itr.hasNext())
            {
                strstrRequistionNextApproverName = itr.next();
                strstrRequistionNextApproverEmail = itr.next();

                strLanguage = getLangPrefByEmail(strstrRequistionNextApproverEmail);

                strMailMsg = new StringBuilder();

                strCreationDate = strCurrentDate;

                strMailSubject = appMessageUtility.getLabel("label.mail.starsnotification") + tType + " '" + strRequistionNumber.trim() + "' " + appMessageUtility.getLabel(strMailSubject1);// Mail
                                                                                                                                                                                             // Subject
                try
                {
                    String strSSOUrl = sSSOUrlByMailid(strstrRequistionNextApproverEmail);

                    strMailMsg.append( "<html><style>.formhead{ font-family:Arial;font-size:11px;font-style: normal;font-weight:normal;color:#000000;text-decoration:none;letter-spacing:normal;word-spacing:normal;border:1px #333333 solid;background:#E2E4D6;}</style><body bgcolor=\"#d0d0d0\">"+ "\n");
                    strMailMsg.append(" <table style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%' width=80% border=0 cellspacing=0 cellpadding=0 align=center><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center>"+ "\n");
                    strMailMsg.append("</td></tr><tr><td bgcolor=#000000></td></tr><tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=10 style='font-family:Calibri;font-size:12px;color:#666666;margin:0 auto; width:100%'><tr><td align='left' style='background:#aa1220;color:#fff;font-size:28px;padding:12px 12px 0;'><strong>"+ appMessageUtility.getLabel("label.mail.starsmailnotification") + "</strong></td>")
                    .append("<td  align='right' style='background:#aa1220;color:#fff;font-size:13px;padding:12px 12px 0;'>[" + strCreationDate + "]<br/>" + appMessageUtility.getLabel("label.mail.mailreferencenumber") + Constants.HTML_NBSP + strMailRefNumber + "/" + strCurrentYear + "</td></tr></table></td></tr><tr><td bgcolor=#000000> </td></tr><tr>" + "\n");
                    strMailMsg.append("<td bgcolor=#B6DCDC></td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#000000> </td></tr><tr><td bgcolor=#FFFFFF><table width=100% border=0 cellspacing=0 cellpadding=5>"+ "\n");
                    strMailMsg.append("  <tr><td align=left style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:15px'>" + appMessageUtility.getLabel(reqTyp1) + " " + appMessageUtility.getLabel("label.mail.requisitionintimation") + Constants.HTML_NBSP + "" + " : [" + strRequistionNumber.trim() + "]&nbsp;|&nbsp;[" + "" + " " + strRequistionCreatedDate + "]</td>").append("<td  align=right style='background:#aa1220;color:#fff;padding:0 12px 12px;font-size:13px'>" + appMessageUtility.getLabel("label.mail.forinternalcirculationonly") + "</td></tr></table>");

                    strMailMsg.append("<table style='font-size:13px;'><tr><td><p><font color=#FFFFFF>.</font><br>");
                    if (!strMailMessage.equals(""))
                    {
                        strRequistionCreatedDate = strRequistionCreatedDate + " ";
                    }
                    strMailMsg.append(" " + appMessageUtility.getLabel("label.mail.dear") + " " + strstrRequistionNextApproverName + ",</p><p>" + appMessageUtility.getLabel("label.global.requisitionnumber") + " " + strRequistionNumber.trim() + " " + appMessageUtility.getLabel("label.mail.wasgeneratedby") + " " + strRequistionCreatorName + " " + appMessageUtility.getLabel("label.mail.from") + " " + strSiteName + " " + appMessageUtility.getLabel("label.mail.on") + " " + strRequistionCreatedDate + " .</p>" + " \n");
                    strMailMsg.append("<u><font color=blue>" + appMessageUtility.getLabel("label.mail.requisitionsdetails") + ":-</font></u><br>" + strGroupTravel + "<br> " + appMessageUtility.getLabel("label.mail.departuredateis") + " " + strTravelDate + ".  \n<br>");

                    if ((strTravelAgencyTypeId != null && strTravelAgencyTypeId.equals("2")))
                    {
                        strMailMsg.append(customizeApproverMailBodyGmbh(strRequistionId, strTrvType));
                    } else
                    {
                        strMailMsg.append(customizeApproverMailBodyIndia(strRequistionId, strTrvType, strGroupTravelFlag));
                    }

                    strMailMsg.append("<u><font color=blue>" + appMessageUtility.getLabel("label.requestdetails.billingclient") + ": </font></u><font  >" + strBillingSiteName + " </font>\n<br>");
                    if (reqType.equals(Constants.INTERNATIONAL_S_TRAVEL) && (strGroupTravelFlag.trim().equals("N")))
                    {
                        strMailMsg.append("<u><font  color=blue>" + appMessageUtility.getLabel("label.global.visarequired") + ": </font></u>" + strVisaRequired + " \n<br>");
                        strMailMsg.append("<u><font  color=blue>" + appMessageUtility.getLabel("label.mail.visacomment") + ": </font></u>" + strVisaRequiredComment + " \n<br>");
                        strMailMsg.append("<u><font  color=blue>" + appMessageUtility.getLabel("label.mail.ecrrequired") + ": </font></u>" + strECRRequired + " \n<br>");
                    }

                    strMailMsg.append("</font><br><font  color=blue>" + appMessageUtility.getLabel("label.mail.thismailisforyourinformationonly") + " " + "\n")
                    .append("</font><br><br><a href='" + strSSOUrl + "'>" + appMessageUtility.getLabel("label.mail.pleaseclickheretologintostars") + " </a> " + "\n")
                    .append("   </b></form><br><p>" + appMessageUtility.getLabel("label.mail.bestregards") + "<br>" + strstrRequistionApproverName.trim() + "<br></p></td></tr></table></td></tr>");
                   
                    strMailMsg.append("<tr><td bgcolor=#FFFFFF align=center><table width=100% border=0 cellspacing=0 cellpadding=5>" + "\n")
                            .append("<tr><td  align=left style='padding:12px;font-weight:300;background:#e7e8e9;color:#6d6e71;font-size:11px;font-family:Calibri;'> <strong>"+ appMessageUtility.getLabel("label.mail.disclaimer") + "</strong> : " + appMessageUtility.getLabel("label.mail.thiscommunicationissystemgenererated") + " " + "\n")
                            .append(appMessageUtility.getLabel("label.mail.pleasedonnotreplytothismail") + " <br>" + appMessageUtility.getLabel("label.mail.ifyouarenotthecorrectrecipientforthisnotification") + "<br><br>&copy;" + appMessageUtility.getLabel("label.mail.mindallrightsreserved") + "</td></tr></table></td></tr><tr><td bgcolor=#000000></td></tr>" + "\n").append("</table><p>&nbsp;</p></body></html>" + "\n");

                    pushEmailDataToTable(
                            strRequistionId, strRequistionNumber, strstrRequistionNextApproverEmail, strstrRequistionApproverEmail, "", strMailSubject, strMailMsg.toString(), strApproverId,
                            "Signatory Approves IT", "Approval Process");

                } catch (Exception e)
                {
                    log.error("Error in EmailDAOImpl -> sendMailToChairman() in main try and catch block" + e);
                }
            }
        }

    }

    public void sendCancellationEmail(String strTravelType, String purchaseRequisitionId, String strRequisitionId, String strComments, String ipAddr, String sUserId, String sUserRole)
    {

        String sSqlStr = null;
        String strFlag1 = "no";
        String strMailUserId = null;
        String strTravllerSiteId = null;
        String strTravellerId = null;
        String unitheadcheck = null;
        String chairmancheck = "";
        String strTableName = null;
        String strUserBillingClient = null;
        sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType + "' AND APPROVE_STATUS=10 AND APPLICATION_ID=1 AND STATUS_ID=10";
        Map<String, Object> approverFlag = getFirstResult(sSqlStr);
        String strUserID = null;
        if (approverFlag != null)
        {
            strFlag1 = "yes";
        }

        if (strFlag1 != null && strFlag1.equals("no") && !sUserRole.equals("MATA"))
        {
            sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType + "' AND STATUS_ID=10 AND APPLICATION_ID=1 AND ORDER_ID=1";

            Map<String, Object> approverMailMap = getFirstResult(sSqlStr);
            if (approverMailMap != null)
            {
                strMailUserId = AppUtility.checkNullString(approverMailMap.get("APPROVER_ID"));

                getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
            }

            sSqlStr = "SELECT DISTINCT C_USER_ID AS ORIGINATOR_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                    + "' AND  STATUS_ID=10 AND APPLICATION_ID=1";

            Map<String, Object> originatorMap = getLastResult(sSqlStr);
            if (originatorMap != null)
            {
                strMailUserId = AppUtility.getResultFromMap(originatorMap, "ORIGINATOR_ID");
            }

            sSqlStr = "";

            if (strTravelType != null && strTravelType.equals("I"))
            {
                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
            } else if (strTravelType != null && strTravelType.equals("D"))
            {
                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
            }

            Map<String, Object> siteTravelMap = getFirstResult(sSqlStr);
            if (siteTravelMap != null)
            {
                strTravllerSiteId = AppUtility.getResultFromMap(siteTravelMap, "site_id");
                strTravellerId = AppUtility.getResultFromMap(siteTravelMap, "traveller_id");
            }

            if (strTravllerSiteId != null && !strTravllerSiteId.equals(""))
            {
                sSqlStr = "SELECT ISNULL(UNIT_HEAD,'0') AS UNIT_HEAD   FROM USER_MULTIPLE_ACCESS where userid=" + strTravellerId + " and site_id=" + strTravllerSiteId
                        + " and status_id=10 and unit_head=1";

                if (!AppUtility.checkNull(getFirstResult(sSqlStr)))
                {
                    unitheadcheck = "yes";
                }
            }

            sSqlStr = "SELECT USERID FROM M_USERINFO WHERE   userid=" + strTravellerId + " AND   ROLE_ID IN ('CHAIRMAN')  AND   STATUS_ID=10  AND APPLICATION_ID=1    ";

            if (getFirstResult(sSqlStr) != null)
            {
                chairmancheck = "yes";
            }

            sSqlStr = "SELECT USERID FROM M_USERINFO WHERE   userid=" + strMailUserId + " AND   ROLE_ID IN ('CHAIRMAN')  AND   STATUS_ID=10  AND APPLICATION_ID=1    ";

            if (!AppUtility.checkNull(getFirstResult(sSqlStr)))
            {
                chairmancheck = "yes";
            }

            if (strTravelType.equals("I"))
            {
                strTableName = " T_TRAVEL_detail_INT";
            } else
            {
                strTableName = " T_TRAVEL_detail_DOM";
            }
            sSqlStr = "SELECT BILLING_CLIENT  FROM " + strTableName + "  WHERE  TRAVEL_ID=" + strRequisitionId + "  AND STATUS_ID=10 AND APPLICATION_ID=1";

            Map<String, Object> billingClientMap = getLastResult(sSqlStr);
            if (billingClientMap != null)
            {
                strUserBillingClient = AppUtility.getResultFromMap(billingClientMap, "BILLING_CLIENT");
            }

            if (strUserBillingClient != null && strUserBillingClient.equals("self"))
            {
            } else if (unitheadcheck != null && unitheadcheck.equals("yes") || chairmancheck.equals("yes"))
            {
                sSqlStr = "";
                if (strTravelType != null && strTravelType.equals("I"))
                {
                    sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                } else if (strTravelType != null && strTravelType.equals("D"))
                {
                    sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                }

                Map<String, Object> siteATravelInfoMap = getFirstResult(sSqlStr); // get the result set
                if (siteATravelInfoMap != null)
                {
                    strTravllerSiteId = AppUtility.getResultFromMap(siteATravelInfoMap, "site_id");
                    strTravellerId = AppUtility.getResultFromMap(siteATravelInfoMap, "traveller_id");
                }

                sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' ) AND  STATUS_ID=10   AND  SITE_ID =" + strTravllerSiteId + " ";

                List<Map<String, Object>> userIdMapList = jdbcTemplate.queryForList(sSqlStr);
                for (Map<String, Object> userIdMap : userIdMapList)
                {
                    strUserID = AppUtility.getResultFromMap(userIdMap, "USERID");
                    getMailBodyForCancellation(purchaseRequisitionId, strUserID, strComments, strTravelType, sUserId);
                }

            }

        } else
        {
            String strFlag2 = "no";

            sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                    + "' AND APPROVE_STATUS=3 AND STATUS_ID=10 AND APPLICATION_ID=1 AND ORDER_ID=1";

            if (getFirstResult(sSqlStr) != null)
            {
                strFlag2 = "yes";
            }

            if (strFlag2 != null && strFlag2.equals("yes"))
            {
                // mail goes to first approver
                sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                        + "' AND APPROVE_STATUS=3 AND STATUS_ID=10 AND APPLICATION_ID=1 AND ORDER_ID=1";

                Map<String, Object> approverIdMap = getFirstResult(sSqlStr);
                if (approverIdMap != null)
                {
                    strMailUserId = AppUtility.getResultFromMap(approverIdMap, "APPROVER_ID");
                    getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);

                }
            } else
            {
                String strFlag3 = "no";

                sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                        + "' AND APPROVE_STATUS=3 AND STATUS_ID=10 AND APPLICATION_ID=1";

                if (!AppUtility.checkNull(getFirstResult((sSqlStr))))
                {
                    strFlag3 = "yes";
                }
                if (strFlag3 != null && strFlag3.equals("yes"))
                {
                    sSqlStr = "SELECT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                            + "' AND APPROVE_STATUS=3 AND STATUS_ID=10 AND APPLICATION_ID=1";

                    Map<String, Object> approverIdMap = getFirstResult(sSqlStr); // get the result set
                    if (approverIdMap != null)
                    {
                        strMailUserId = AppUtility.getResultFromMap(approverIdMap, "APPROVER_ID");
                        getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);

                    }

                    sSqlStr = "";
                    if (strTravelType != null && strTravelType.equals("I"))
                    {
                        sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                    } else if (strTravelType != null && strTravelType.equals("D"))
                    {
                        sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                    }
                    Map<String, Object> siteIdDetailMap = getFirstResult(sSqlStr); // get the result set
                    if (siteIdDetailMap != null)
                    {
                        strTravllerSiteId = AppUtility.getResultFromMap(siteIdDetailMap, "site_id");
                        strTravellerId = AppUtility.getResultFromMap(siteIdDetailMap, "traveller_id");
                    }

                    sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' ) AND  STATUS_ID=10   AND  SITE_ID =" + strTravllerSiteId + "";

                    List<Map<String, Object>> userIdList = jdbcTemplate.queryForList(sSqlStr);

                    for (Map<String, Object> tempMap : userIdList)
                    {

                        String tempUserId = AppUtility.getResultFromMap(tempMap, "USERID");
                        getMailBodyForCancellation(purchaseRequisitionId, tempUserId, strComments, strTravelType, sUserId);

                    }
                } else // When no approver return the requisition (Simple Case)
                {
                    sSqlStr = "SELECT APPROVER_ID, ROLE FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                            + "' AND APPROVE_STATUS=10 AND APPLICATION_ID=1 AND STATUS_ID=10";// AND ROLE<>'MATA' (changed by sachin 3/20/2007)

                    List<Map<String, Object>> approverIdList = jdbcTemplate.queryForList(sSqlStr);

                    for (Map<String, Object> tempMap : approverIdList)
                    {
                        strMailUserId = AppUtility.getResultFromMap(tempMap, "APPROVER_ID");
                        getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                    }

                    // NEW CODE ON 3/26/2007 OPEN
                    sSqlStr = "SELECT DISTINCT C_USER_ID AS ORIGINATOR_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                            + "' AND  STATUS_ID=10 AND APPLICATION_ID=1";
                    Map<String, Object> cUserIdMap = getLastResult(sSqlStr); // get the result set
                    if (cUserIdMap != null)
                    {
                        strMailUserId = AppUtility.getResultFromMap(cUserIdMap, "ORIGINATOR_ID");
                    }
                    // check for self billing open

                    if (strTravelType.equals("I"))
                    {
                        strTableName = " T_TRAVEL_detail_INT";
                    } else
                    {
                        strTableName = " T_TRAVEL_detail_DOM";
                    }
                    sSqlStr = "SELECT BILLING_CLIENT  FROM " + strTableName + "  WHERE  TRAVEL_ID=" + strRequisitionId + "  AND STATUS_ID=10 AND APPLICATION_ID=1";

                    Map<String, Object> billingClienntMap = getLastResult(sSqlStr); // get the result set
                    if (cUserIdMap != null)
                    {
                        strUserBillingClient = AppUtility.getResultFromMap(billingClienntMap, "BILLING_CLIENT");
                    }

                    if (strUserBillingClient != null && strUserBillingClient.equals("self"))
                    {

                    } else if (unitheadcheck.equals("yes"))
                    {

                        sSqlStr = "";
                        if (strTravelType != null && strTravelType.equals("I"))
                        {
                            sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                        } else if (strTravelType != null && strTravelType.equals("D"))
                        {
                            sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                        }

                        Map<String, Object> siteTravelMap = getFirstResult(sSqlStr);
                        if (siteTravelMap != null)
                        {
                            strTravllerSiteId = AppUtility.getResultFromMap(siteTravelMap, "site_id");
                            strTravellerId = AppUtility.getResultFromMap(siteTravelMap, "traveller_id");
                        }

                        sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' ) AND   STATUS_ID=10  AND  SITE_ID =" + strTravllerSiteId + "	";

                        List<Map<String, Object>> userIdMapList = jdbcTemplate.queryForList(sSqlStr);

                        for (Map<String, Object> tempMap : userIdMapList)
                        {
                            strMailUserId = AppUtility.getResultFromMap(tempMap, "USERID");
                            getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                        }

                    } else if (strUserBillingClient != null && !strUserBillingClient.equals("self"))
                    {
                        sSqlStr = "";
                        if (strTravelType != null && strTravelType.equals("I"))
                        {
                            sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                        } else if (strTravelType != null && strTravelType.equals("D"))
                        {
                            sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                        }

                        Map<String, Object> siteIdMap = getFirstResult(sSqlStr); // get the result set
                        if (siteIdMap != null)
                        {
                            strTravllerSiteId = AppUtility.getResultFromMap(siteIdMap, "site_id");
                            strTravellerId = AppUtility.getResultFromMap(siteIdMap, "traveller_id");
                        }

                        sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' ) AND  STATUS_ID=10   AND  SITE_ID =" + strTravllerSiteId + "";

                        List<Map<String, Object>> userIdMapList = jdbcTemplate.queryForList(sSqlStr);

                        for (Map<String, Object> tempMap : userIdMapList)
                        {
                            strMailUserId = AppUtility.getResultFromMap(tempMap, "USERID");
                            getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                        }

                    }

                    // NEW CODE ON 3/26/2007 CLOSE

                    if (sUserRole != null && sUserRole.equals("MATA"))
                    {
                        sSqlStr = "SELECT DISTINCT C_USER_ID AS ORIGINATOR_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                                + "' AND  STATUS_ID=10 AND APPLICATION_ID=1";

                        List<Map<String, Object>> cUserIdMapO = jdbcTemplate.queryForList(sSqlStr);

                        for (Map<String, Object> tempMap : cUserIdMapO)
                        {
                            strMailUserId = AppUtility.getResultFromMap(tempMap, "ORIGINATOR_ID");
                            getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                        }

                        if (strTravelType.equals("I"))
                        {
                            strTableName = " T_TRAVEL_detail_INT";
                        } else
                        {
                            strTableName = " T_TRAVEL_detail_DOM";
                        }

                        sSqlStr = "SELECT BILLING_CLIENT  FROM " + strTableName + "  WHERE  TRAVEL_ID=" + strRequisitionId + "  AND STATUS_ID=10 AND APPLICATION_ID=1";

                        Map<String, Object> billingClientMap = getLastResult(sSqlStr);
                        if (billingClientMap != null)
                        {
                            strUserBillingClient = AppUtility.getResultFromMap(billingClientMap, "BILLING_CLIENT");
                        }

                        if (strUserBillingClient != null && strUserBillingClient.equals("self"))
                        {

                        } else
                        {
                            sSqlStr = "";
                            if (strTravelType != null && strTravelType.equals("I"))
                            {
                                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                            } else if (strTravelType != null && strTravelType.equals("D"))
                            {
                                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                            }

                            Map<String, Object> siteIdDetailMap = getFirstResult(sSqlStr); // get the result set
                            if (siteIdDetailMap != null)
                            {
                                strTravllerSiteId = AppUtility.getResultFromMap(siteIdDetailMap, "site_id");
                                strTravellerId = AppUtility.getResultFromMap(siteIdDetailMap, "traveller_id");
                            }

                            ////

                            sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' )  AND  STATUS_ID=10   AND  SITE_ID =" + strTravllerSiteId + " ";

                            List<Map<String, Object>> userIdMapList = jdbcTemplate.queryForList(sSqlStr);

                            for (Map<String, Object> tempMap : userIdMapList)
                            {
                                strMailUserId = AppUtility.getResultFromMap(tempMap, "USERID");
                                getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                            }

                        }
                    } ////// case close when mata cancel the request
                    else // ELSE BLOCK FOR THE ORIGINATOR WHEN ORIGINATOR CANCEL THE REQUSITION
                    {
                        sSqlStr = "SELECT DISTINCT C_USER_ID AS ORIGINATOR_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                                + "' AND  STATUS_ID=10 AND APPLICATION_ID=1";

                        List<Map<String, Object>> cUserIdMapList = jdbcTemplate.queryForList(sSqlStr);

                        for (Map<String, Object> tempMap : cUserIdMapList)
                        {
                            strMailUserId = AppUtility.getResultFromMap(tempMap, "ORIGINATOR_ID");
                            getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                        }

                        if (strTravelType.equals("I"))
                        {
                            strTableName = " T_TRAVEL_detail_INT";
                        } else
                        {
                            strTableName = " T_TRAVEL_detail_DOM";
                        }

                        sSqlStr = "SELECT BILLING_CLIENT  FROM " + strTableName + "  WHERE  TRAVEL_ID=" + strRequisitionId + "  AND STATUS_ID=10 AND APPLICATION_ID=1";

                        Map<String, Object> billingClientMap = getLastResult(sSqlStr);
                        if (billingClientMap != null)
                        {
                            strUserBillingClient = AppUtility.getResultFromMap(billingClientMap, "BILLING_CLIENT");
                        }

                        if (strUserBillingClient != null && strUserBillingClient.equals("self"))
                        {

                        } else
                        {

                            String maxOrderId = "";
                            String minOrderId = "";

                            sSqlStr = "";
                            if (strTravelType != null && strTravelType.equals("I"))
                            {
                                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Int where travel_id=" + strRequisitionId + " and status_id=10";
                            } else if (strTravelType != null && strTravelType.equals("D"))
                            {
                                sSqlStr = "select site_id,traveller_id from t_travel_Detail_Dom where travel_id=" + strRequisitionId + " and status_id=10";
                            }

                            Map<String, Object> siteIdDetailMap = getFirstResult(sSqlStr); // get the result set
                            if (siteIdDetailMap != null)
                            {
                                strTravllerSiteId = AppUtility.getResultFromMap(siteIdDetailMap, "site_id");
                                strTravellerId = AppUtility.getResultFromMap(siteIdDetailMap, "traveller_id");
                            }

                            /// case when cancle by origanator when pending with MATA 11-May-2007

                            sSqlStr = "SELECT USERID FROM M_USERINFO WHERE  ROLE_ID IN ('HR','AC' )  AND  STATUS_ID=10  AND  SITE_ID=" + strTravllerSiteId + "";

                            List<Map<String, Object>> userIdMapList = jdbcTemplate.queryForList(sSqlStr);

                            for (Map<String, Object> tempMap : userIdMapList)
                            {
                                strMailUserId = AppUtility.getResultFromMap(tempMap, "USERID");
                                getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                            }

                            sSqlStr = "SELECT MAX(ORDER_ID) AS Expr1 FROM T_APPROVERS WHERE TRAVEL_ID =" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType + "'";

                            Map<String, Object> exprMap = getFirstResult(sSqlStr); // get the result set
                            if (exprMap != null)
                            {
                                maxOrderId = AppUtility.getResultFromMap(exprMap, "Expr1");
                            }

                            sSqlStr = "SELECT DISTINCT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                                    + "' AND ROLE NOT IN('MATA') AND APPROVE_STATUS = '0' AND  STATUS_ID=10 AND APPLICATION_ID=1";
                            List<Map<String, Object>> appIdMapList = jdbcTemplate.queryForList(sSqlStr);

                            for (Map<String, Object> tempMap : appIdMapList)
                            {
                                strMailUserId = AppUtility.getResultFromMap(tempMap, "APPROVER_ID");
                                getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                            }

                            sSqlStr = "SELECT  MIN(ORDER_ID) AS Expr1 FROM T_APPROVERS WHERE APPROVE_STATUS = '0' AND TRAVEL_ID = " + strRequisitionId + " AND  TRAVEL_TYPE='" + strTravelType + "'";

                            Map<String, Object> exprMap1 = getFirstResult(sSqlStr); // get the result set
                            if (exprMap1 != null)
                            {
                                minOrderId = AppUtility.getResultFromMap(exprMap1, "Expr1");
                            }

                            if (maxOrderId.equals(minOrderId))
                            {

                                sSqlStr = "SELECT DISTINCT APPROVER_ID FROM T_APPROVERS WHERE TRAVEL_ID=" + strRequisitionId + " AND TRAVEL_TYPE='" + strTravelType
                                        + "' AND ROLE='MATA' AND  STATUS_ID=10 AND APPLICATION_ID=1";

                                List<Map<String, Object>> appIdList = jdbcTemplate.queryForList(sSqlStr);

                                for (Map<String, Object> tempMap : appIdList)
                                {
                                    strMailUserId = AppUtility.getResultFromMap(tempMap, "APPROVER_ID");
                                    getMailBodyForCancellation(purchaseRequisitionId, strMailUserId, strComments, strTravelType, sUserId);
                                }
                            }

                        }

                    }
                }

            }
        }

    }
}

