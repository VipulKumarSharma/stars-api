package com.mind.api.stars.dao;

import java.sql.SQLException;
import java.util.List;

import com.mind.api.stars.dto.AttachmentResponseDTO;
import com.mind.api.stars.model.AppRequest;
import com.mind.api.stars.model.Attachment;
import com.mind.api.stars.model.CityResponse;
import com.mind.api.stars.model.CountryResponse;
import com.mind.api.stars.model.Currency;
import com.mind.api.stars.model.Time;
import com.mind.api.stars.model.Unit;

public interface MastersDataDAO {
	
	CityResponse getCityMasterData(AppRequest appRequest);
	
	CountryResponse getCountryMasterData(AppRequest appRequest);
	
	List<Time> getTimeMasterData(AppRequest appRequest);
	
	List<Unit> getUnitMasterData(AppRequest appRequest);
	
	List<Currency> getCurrencyMasterData(AppRequest appRequest);
	
	AttachmentResponseDTO uploadAttachment(Attachment attachment) throws SQLException;
	
	List<Attachment> getAttachment(Attachment attachment);
}
