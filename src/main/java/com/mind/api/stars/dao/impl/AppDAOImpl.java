package com.mind.api.stars.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.dao.AppDAO;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.model.LoginBean;
import com.mind.api.stars.model.UserInfo;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.TokenUtility;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class AppDAOImpl extends BaseDAO implements AppDAO {

	@Override
	public UserInfo getUserInfo(LoginBean loginBean) {
		UserInfo userInfo = null;
		
		if(!AppUtility.isNull(loginBean)) {
			try {
				String password = AppUtility.checkNullString(loginBean.getEncPassword());
				String decryptedPassword = AppUtility.decrypt(password);
				
				userInfo = getUserInfoByCredentials(loginBean.getUserName(), decryptedPassword);
				
				if(AppUtility.isNull(userInfo)) {
					String decryptedPasswordInDecimal = AppUtility.decryptInDecimal(password);
					userInfo = getUserInfoByCredentials(loginBean.getUserName(), decryptedPasswordInDecimal);
				}
				
				if(userInfo!=null) {
					String tokenId=TokenUtility.generateTokenId(userInfo.getDomainName(), userInfo.getWinUserId());
					userInfo.setTokenId(tokenId);
				}
			            
			} catch (Exception e) {
				log.error("Error occurred in AppDAOImpl: getUserInfo() ", e);
			}
		}
		
		return userInfo;
	}
	
	@SuppressWarnings("unchecked")
	private UserInfo getUserInfoByCredentials(String userName, String password) {
		UserInfo userInfo = null;
		
		if(!AppUtility.isNull(userName) && !AppUtility.isNull(password)) {
			
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
				.withProcedureName(Procedure.AUTHENTICATE_USER.getName())
				.returningResultSet(ParamConstant.RESULT_LIST, new UserInfoMapper());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue("P_USER_NAME", userName)
				.addValue("P_USER_PIN", password)
				.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
				
				Map<String, Object> result = jdbcCall.execute(procParams);
				
				String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
	            
	            if(AppUtility.isSuccess(returnedCode)) {
	            	List<UserInfo> dataList = (List<UserInfo>) result.get(ParamConstant.RESULT_LIST);
	        		
	            	if(AppUtility.containsData(dataList)) {
	            		userInfo = dataList.get(0);
	        		}
	            }
			            
			} catch (Exception e) {
				log.error("Error occurred in AppDAOImpl: getUserInfoByCredentials() ", e);
			}
		}
		
		return userInfo;
	}
	
	private static final class UserInfoMapper implements RowMapper<UserInfo> {

		@Override
		public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			UserInfo bean = new UserInfo();
			
			bean.setUserId(rs.getInt("USER_ID"));
			bean.setUserName(AppUtility.checkNullString(rs.getString("USER_NAME")));
			bean.setEncFlag(AppUtility.checkNullString(rs.getString("ENC_FLAG")));
			bean.setDisabledTime(AppUtility.checkNullString(rs.getString("DISABLED_TIME")));
			bean.setEmail(AppUtility.checkNullString(rs.getString("EMAIL")));
			bean.setTravelAgencyId(rs.getInt("TRAVEL_AGENCY_ID"));
			bean.setAcceptanceFlag(AppUtility.checkNullString(rs.getString("ACCEPTANCE_FLAG")));
			bean.setSiteId(rs.getInt("SITE_ID"));
			bean.setRelievingDateFlag(AppUtility.checkNullString(rs.getString("RELIEVING_DATE_FLAG")));
			bean.setDummyUserFlag(AppUtility.checkNullString(rs.getString("DUMMY_USER_FLAG")));
			bean.setWinUserId(AppUtility.checkNullString(rs.getString("WIN_USER_ID")));
			bean.setDomainName(AppUtility.checkNullString(rs.getString("DOMAIN_NAME")));
			bean.setSsoFlag(AppUtility.checkNullString(rs.getString("ENABLE_SSO")));
			bean.setCustomSsoFlag(AppUtility.checkNullString(rs.getString("CAPTURE_SSO")));
			
			return bean;
		}
		
	}
	
	@Override
	public UserInfo acceptPolicy(UserInfo userInformation) {
		UserInfo userInfo = null;
		
		if(!AppUtility.isNull(userInformation)) {
			
			int userId = userInformation.getUserId();
			String acceptanceFlag = AppUtility.checkNullString(userInformation.getAcceptanceFlag());
			String encFlag = AppUtility.checkNullString(userInformation.getEncFlag());
			
			if(userId > 0 && !AppUtility.isNull(acceptanceFlag)) {
				try {
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(Procedure.UPDATE_EUROPEAN_USER_DETAILS.getName());

					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue("USERID", userId)
					.addValue("ACCEPTANCE_FLAG", acceptanceFlag)
					.addValue("POLICY_NAME", "GDPR-Privacy Policy")
					.addValue("POLICY_LANGUAGE", "English")
					.addValue("APPLICATION", "STARS")
					.addValue("MACHINE_IP", userInformation.getIpAddress())
					.addValue("DEVICE_DETAILS", "DESKTOP")
					.addValue(ParamConstant.STATUS_ID, 0);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode) && "Y".equalsIgnoreCase(acceptanceFlag)) {
		            	if("Y".equalsIgnoreCase(encFlag)) {
		            		userInfo = getAuthenticatedUserDetails(userInformation);
		            	} else {
		            		userInfo = userInformation;
		            	}
		            }
					
				} catch (Exception e) {
					log.error("Error occurred in AppDAOImpl: acceptPolicy() ", e);
				}
			}
		}
		
		return userInfo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserInfo getAuthenticatedUserDetails(UserInfo userInformation) {
		UserInfo userInfo = null;
		
		if(!AppUtility.isNull(userInformation)) {
			
			String ssoFlag = AppUtility.checkNullString(userInformation.getSsoFlag());
			String userName = AppUtility.checkNullString(userInformation.getUserName());
			String password = AppUtility.checkNullString(userInformation.getPassword());
			String language = AppUtility.checkNullString(userInformation.getLanguage());
			
			if(!AppUtility.isNull(userName) && !AppUtility.isNull(password) && !AppUtility.isNull(language)) {
				try {
					
					if(!"Y".equalsIgnoreCase(ssoFlag)) {
						password = AppUtility.decryptInDecimal(password);
					}
					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.AUTHENTICATED_USER_DETAILS.getName())
					.returningResultSet(ParamConstant.RESULT_LIST, new AuthenticatedUserDetailsMapper(userInformation));
	
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue("P_USER_NAME", userName)
					.addValue("P_USER_PIN", password)
					.addValue("P_LANGUAGE", language)
					.addValue(ParamConstant.OUTPUT_STATUS_ID, 0);
					
					Map<String, Object> result = jdbcCall.execute(procParams);
					
					String returnedCode = AppUtility.checkNullString(result.get(ParamConstant.OUTPUT_STATUS_ID));
		            
		            if(AppUtility.isSuccess(returnedCode)) {
		            	List<UserInfo> dataList = (List<UserInfo>) result.get(ParamConstant.RESULT_LIST);
		        		
		            	if(AppUtility.containsData(dataList)) {
		            		userInfo = dataList.get(0);
		            		
		            		//userInfo.setSsoFlag(userInformation.getSsoFlag());
		            		
		            		String tokenId=TokenUtility.generateTokenId(userInformation.getDomainName(),userInformation.getWinUserId());
		            		userInfo.setTokenId(tokenId);
		            		createUserLoginInfo(userInfo.getUserId(), userInfo.getUserName(), userInfo.getSsoFlag(), userInfo.getIpAddress(), userInfo.getBrowser());
		        		}
		            }
				            
				} catch (Exception e) {
					log.error("Error occurred in AppDAOImpl: getAuthenticatedUserDetails() ", e);
				}
			}
		}
		
		return userInfo;
	}
	
	private static final class AuthenticatedUserDetailsMapper implements RowMapper<UserInfo> {
		
		UserInfo userInfo = null;
		
		AuthenticatedUserDetailsMapper(UserInfo userInfo) {
			this.userInfo = userInfo;
		}
		
		@Override
		public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			if(AppUtility.isNull(userInfo)) {
				userInfo = new UserInfo();
			}
			
			userInfo.setUserId(rs.getInt("USER_ID"));
			userInfo.setFirstName(AppUtility.checkNullString(rs.getString("FIRST_NAME")));
			userInfo.setMiddleName(AppUtility.checkNullString(rs.getString("MIDDLE_NAME")));
			userInfo.setLastName(AppUtility.checkNullString(rs.getString("LAST_NAME")));
			userInfo.setDivId(rs.getInt("DIV_ID"));
			userInfo.setSiteId(rs.getInt("SITE_ID"));
			userInfo.setDesignationName(AppUtility.checkNullString(rs.getString("DESIGNATION")));
			userInfo.setShortDesignationName(AppUtility.checkNullString(rs.getString("DESIG_SHORT_NAME")));
			userInfo.setSiteName(AppUtility.checkNullString(rs.getString("SITE_NAME")));
			userInfo.setDepartmentName(AppUtility.checkNullString(rs.getString("SITE_NAME")));
			userInfo.setCurrentYear(rs.getInt("CURRENT_YEAR"));
			userInfo.setWorkflowNo(rs.getInt("SP_ROLE"));
			userInfo.setStartedNewStars(AppUtility.checkNullString(rs.getString("USER_STARTED_NEW_STAR")));
			userInfo.setUpdateProfileFlag(AppUtility.checkNullString(rs.getString("UPDATE_PROFILE_FLAG")));
			userInfo.setApproverLevel(AppUtility.checkNullString(rs.getString("APP_LVL")));
			userInfo.setGender(AppUtility.checkNullString(rs.getString("GENDER")));
			userInfo.setLanguage(AppUtility.checkNullString(rs.getString("LANGUAGE_PREF")));
			userInfo.setRoles(AppUtility.checkNullString(rs.getString("ROLES")));
			userInfo.setVisaFlag(rs.getInt("FLAG_VISA"));
			userInfo.setShowRequestForApproverFlag(AppUtility.checkNullString(rs.getString("SHOW_REQUEST_FOR_APPROVAL_FLAG")));
			userInfo.setDomainName(AppUtility.checkNullString(rs.getString("DOMAIN_NAME")));
			userInfo.setWinUserId(AppUtility.checkNullString(rs.getString("WIN_USER_ID")));
			userInfo.setSsoFlag(AppUtility.checkNullString(rs.getString("ENABLE_SSO")));
			userInfo.setCustomSsoFlag(AppUtility.checkNullString(rs.getString("CAPTURE_SSO")));
			userInfo.setTravelAgencyId(rs.getInt("TRAVEL_AGENCY_ID"));
			userInfo.setLoginDateTime(AppUtility.checkNullString(rs.getString("LOGIN_DATE_TIME")));
			userInfo.setLastLoginTime(AppUtility.checkNullString(rs.getString("LAST_LOGIN_TIME")));
			userInfo.setLastLoginDuration(AppUtility.checkNullString(rs.getString("LAST_LOGIN_DURATION")));
			
			return userInfo;
		}
		
	}
	
	public void createUserLoginInfo(int userId, String userName, String ssoFlag, String ipAddress, String browser) {
		
		if(userId > 0 && !AppUtility.isNull(userName) &&  !AppUtility.isNull(ssoFlag)) {
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(Procedure.CREATE_USER_LOGIN_LOGOUT_INFO.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, userId)
				.addValue("P_IP_ADDRESS", ipAddress)
				.addValue("P_SESSION_FLAG", "Y")
				.addValue("P_ACTION", Constants.INSERT)
				.addValue("P_BROWSER", browser)
				.addValue("P_SSO_LOGIN", ssoFlag)
				.addValue("P_USERNAME", userName);
				
				jdbcCall.execute(procParams);
			
			} catch (Exception e) {
				log.error("Error occurred in AppDAOImpl: createUserLoginInfo() ", e);
			}
		}
	}
	
	public void updateLogoutInfoOnSessionTimeOut(int loggedInUserId, String ipAddress) {
		
		if(loggedInUserId > 0) {
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(Procedure.CREATE_USER_LOGIN_LOGOUT_INFO.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, loggedInUserId)
				.addValue("P_IP_ADDRESS", ipAddress)
				.addValue("P_SESSION_FLAG", "")
				.addValue("P_ACTION", Constants.UPDATE)
				.addValue("P_BROWSER", "")
				.addValue("P_SSO_LOGIN", "")
				.addValue("P_USERNAME", "");
				
				jdbcCall.execute(procParams);
				
			} catch (Exception e) {
				log.error("Error occurred in AppDAOImpl: updateLogoutInfoOnSessionTimeOut() ", e);
			}
		}
	}

	public void removeSsoHistoryTableEntry(int loggedInUserId) {
		
		if(loggedInUserId > 0) {
			try {
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(Procedure.DELETE_SSO_HISTORY_ENTRY.getName());

				SqlParameterSource procParams = new MapSqlParameterSource()
				.addValue(ParamConstant.USER_ID, loggedInUserId)
				.addValue("P_APP_CODE", "STAR");
				
				jdbcCall.execute(procParams);
				
			} catch (Exception e) {
				log.error("Error occurred in AppDAOImpl: removeSsoHistoryTableEntry() ", e);
			}
		}
	}

	@Override
	public UserInfo ssoLogin(String winUserId, String domain) {
		UserInfo userInfo = null;
		
		try {
			String sql = " SELECT M.USERNAME, M.PIN, M.LANGUAGE_PREF FROM M_USERINFO M INNER JOIN M_SITE S ON M.SITE_ID=S.SITE_ID WHERE M.STATUS_ID=10 AND S.STATUS_ID=10 AND SSO_ENABLE='Y' "+
						 " AND M.WIN_USER_ID =? AND (M.DOMAIN_NAME=? OR CASE WHEN ISNULL(M.SECONDARY_DOMAIN_NAME,'')<>'' THEN M.SECONDARY_DOMAIN_NAME ELSE NULL END=?)";
			
			LoginBean credentials = jdbcTemplate.queryForObject(sql, new Object[]{winUserId, domain, winUserId}, new CredentialsMapper());
			
			if(!AppUtility.checkNull(credentials)) {
				userInfo = getUserInfoByCredentials(credentials.getUserName(), credentials.getEncPassword());
				
				if(AppUtility.checkNull(userInfo)) {
					userInfo = new UserInfo();
				}
				userInfo.setUserName(credentials.getUserName());
				userInfo.setPassword(credentials.getEncPassword());
				userInfo.setLanguage(credentials.getLanguage());
				userInfo.setEncFlag("Y");
			}
		
		} catch(EmptyResultDataAccessException ex) {
			log.error("No result set found in AppDAOImpl : ssoLogin() : ", ex);
			
		} catch (Exception e) {
			log.error("Error occurred in AppDAOImpl: ssoLogin() ", e);
		} 
		
		return userInfo;
	}
	
	private static final class CredentialsMapper implements RowMapper<LoginBean> {

		@Override
		public LoginBean mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			LoginBean bean = new LoginBean();
			
			bean.setUserName(AppUtility.checkNullString(rs.getString("USERNAME")));
			bean.setEncPassword(AppUtility.checkNullString(rs.getString("PIN")));
			bean.setLanguage(AppUtility.checkNullString(rs.getString("LANGUAGE_PREF")));
			
			return bean;
		}
		
	}

	@Override
	public void logout(int loggedInUserId, String ipAddress) {
		
		updateLogoutInfoOnSessionTimeOut(loggedInUserId, ipAddress);
		removeSsoHistoryTableEntry(loggedInUserId);
	}
}
