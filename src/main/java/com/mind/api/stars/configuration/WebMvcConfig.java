package com.mind.api.stars.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.mind.api.stars.interceptor.APIInterceptor;
import com.mind.api.stars.interceptor.TemplateInterceptor;

@SuppressWarnings("deprecation")
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	 
	   @Override
	   public void addInterceptors(InterceptorRegistry registry) {
		   registry.addInterceptor(new TemplateInterceptor()).excludePathPatterns("/api/");
		   registry.addInterceptor(new APIInterceptor()).addPathPatterns("/api/**");
	   }
	   
}
