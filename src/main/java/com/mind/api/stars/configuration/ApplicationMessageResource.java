package com.mind.api.stars.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("spring.messages")
@Getter
@Setter
public class ApplicationMessageResource {
	@Bean
	ReloadableResourceBundleMessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames("messages");
		messageSource.setDefaultEncoding(encoding);
		messageSource.setCacheSeconds(cacheDuration);
		messageSource.setAlwaysUseMessageFormat(alwaysUseMessageFormat);
		messageSource.setFallbackToSystemLocale(fallbackToSystemLocale);
		messageSource.setUseCodeAsDefaultMessage(false);

		return messageSource;

	}

	private boolean alwaysUseMessageFormat;
	private String basename;
	private int cacheDuration;
	private String encoding;
	private boolean fallbackToSystemLocale;
	private boolean useCodeAsDefaultMessage;
}
