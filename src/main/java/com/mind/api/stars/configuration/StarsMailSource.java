package com.mind.api.stars.configuration;

import java.util.Properties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("spring.mail")
@Getter
@Setter
public class StarsMailSource {
	
	@Bean
	public JavaMailSender getJavaMailSender() {
	    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    mailSender.setHost(host);
	    mailSender.setPort(port);
	    mailSender.setProtocol(protocol);
	     
	    Properties props = mailSender.getJavaMailProperties();
	    props.put("mail.transport.protocol", "smtp");
	    props.put("mail.defaultEncoding", "UTF-8");
	   // props.put("mail.debug", "true");
	     
	    return mailSender;
	}
	
	
	private String host;
	private int port;
	private String username;
	private String password;
	private String protocol;
	
}
