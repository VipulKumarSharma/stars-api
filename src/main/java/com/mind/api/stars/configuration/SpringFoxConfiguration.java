package com.mind.api.stars.configuration;

import java.util.Collections;

import org.springframework.context.annotation.Bean;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.common.EndpointURL;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/*@Configuration
@EnableSwagger2*/
public class SpringFoxConfiguration {

	@Bean
    public Docket apiDocket() {
        
		return new Docket(DocumentationType.SWAGGER_2)
	       .select()
		   .apis(RequestHandlerSelectors.basePackage(Constants.SWAGGER_SCANNED_PACKAGE))	// .apis(RequestHandlerSelectors.any())
		   .paths(PathSelectors.regex(EndpointURL.SWAGGER_FILTER_URL))						// .paths(PathSelectors.any())
		   .build()
		   .apiInfo(getApiInfo());
        
    }
	
	private ApiInfo getApiInfo() {
	    return new ApiInfo(
	            "STARS-API",
	            "desc",
	            "v1.0",
	            "Terms and conditions",
	            new Contact("STARS Administrator","mailto:administrator.stars@mind-infotech.com","administrator.stars@mind-infotech.com"),
	            "License",
	            "mailto:administrator.stars@mind-infotech.com",
	            Collections.emptyList()
	    );
	}
}
