package com.mind.api.stars.configuration;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("spring.datasource")
@Getter
@Setter
public class ApplicationDataSource {

	@Bean
	DataSource dataSource() {
		
		HikariConfig config = new HikariConfig();
		config.setDriverClassName(driverClassName);
		config.setUsername(userName);
		config.setPassword(password);
        config.setJdbcUrl(url);
        config.setMaximumPoolSize(50);
        config.setMinimumIdle(5);
        config.setIdleTimeout(30000);
        config.setMaxLifetime(60000);
        config.setPoolName("STARSConnectionPool");
        config.setConnectionTestQuery("SELECT 1");
			
		return new HikariDataSource(config);
	}
	
	private String userName;
	private String password;
	private String url;
	private String showSql;
	private String driverClassName;

}
