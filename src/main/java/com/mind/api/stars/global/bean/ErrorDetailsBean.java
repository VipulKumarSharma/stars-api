package com.mind.api.stars.global.bean;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class ErrorDetailsBean {

	private String timestamp;
	private String message;
	private String details;

	
}

