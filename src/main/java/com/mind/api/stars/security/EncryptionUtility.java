package com.mind.api.stars.security;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import javax.crypto.Cipher;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import com.mind.api.stars.utility.ExceptionLogger;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncryptionUtility {

	/** 
	 * Encrypt Data 
	 * @param data 
	 * @throws IOException 
	 */  
	public static String encryptData(String data) throws IOException {  
		//log.info("\n----------------ENCRYPTION STARTED------------");  
		Resource resource = new ClassPathResource("Public.key");
        File file = resource.getFile();
        String publicKeyFile = file.getAbsolutePath();
		byte[] dataToEncrypt = data.getBytes("UTF-8");  
		byte[] encryptedData = null; 
		String encodeVal = "";
		try {  
			PublicKey pubKey = readPublicKeyFromFile(publicKeyFile);  
			Cipher cipher = Cipher.getInstance("RSA");  
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);  
			encryptedData = cipher.doFinal(dataToEncrypt); 
			byte[] bytesEncoded = Base64.encodeBase64(encryptedData);
			encodeVal = new String(bytesEncoded);

		} catch (Exception e) {  
			ExceptionLogger.logExceptionToDB(e); 
		}   

		//log.info("----------------ENCRYPTION COMPLETED------------");    
		return encodeVal;  
	}  

	/** 
	 * Encrypt Data 
	 * @param data 
	 * @throws IOException 
	 */  
	public static String decryptData(String data) throws IOException {  
		//log.info("\n----------------DECRYPTION STARTED------------"); 
		Resource resource = new ClassPathResource("Private.key");
        File file = resource.getFile();
        String privateKeyFile = file.getAbsolutePath();
		byte[] descryptedData = null;  
		byte[] valueDecoded = Base64.decodeBase64(data.getBytes());
		String result ="";

		try {  
			PrivateKey privateKey = readPrivateKeyFromFile(privateKeyFile);  
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);  
			descryptedData = cipher.doFinal(valueDecoded);
			result = new String(descryptedData);
		} catch (Exception e) {  
			ExceptionLogger.logExceptionToDB(e);  
		}   
		//log.info("----------------DECRYPTION COMPLETED------------");

		return result;
	}  

	/** 
	 * read Public Key From File 
	 * @param fileName 
	 * @return PublicKey 
	 * @throws IOException 
	 */  
	public static PublicKey readPublicKeyFromFile(String fileName) throws IOException {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			fis = new FileInputStream(new File(fileName));
			ois = new ObjectInputStream(fis);

			BigInteger modulus = (BigInteger) ois.readObject();
			BigInteger exponent = (BigInteger) ois.readObject();

			// Get Public Key
			RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus,
					exponent);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			return fact.generatePublic(rsaPublicKeySpec);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ois != null) {
				ois.close();
				if (fis != null) {
					fis.close();
				}
			}
		}
		return null;
	}

	/** 
	 * read Public Key From File 
	 * @param fileName 
	 * @return 
	 * @throws IOException 
	 */  
	public static PrivateKey readPrivateKeyFromFile(String fileName) throws Exception {  
		FileInputStream fis = null;  
		ObjectInputStream ois = null;  
		try {  
			fis = new FileInputStream(new File(fileName));  
			ois = new ObjectInputStream(fis);  

			BigInteger modulus = (BigInteger) ois.readObject();  
			BigInteger exponent = (BigInteger) ois.readObject();  

			//Get Private Key  
			RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modulus, exponent);  
			KeyFactory fact = KeyFactory.getInstance("RSA");  
			PrivateKey privateKey = fact.generatePrivate(rsaPrivateKeySpec);  

			return privateKey;  

		} catch (Exception e) {  
			e.printStackTrace();  
		}  
		finally{  
			if(ois != null){  
				ois.close();  
				if(fis != null){  
					fis.close();  
				}  
			}  
		}  
		return null;  
	}  

	static String getElement(String name, BigInteger bigInt) throws Exception {
		byte[] bytesFromBigInt = getBytesFromBigInt(bigInt);
		String elementContent = getBase64(bytesFromBigInt);
		return String.format("  <%s>%s</%s>%s", name, elementContent, name, System.getProperty("line.separator"));
	}

	static byte[] getBytesFromBigInt(BigInteger bigInt){
		byte[] bytes = bigInt.toByteArray();
		int length = bytes.length;

		if(length % 2 != 0 && bytes[0] == 0) {
			bytes = Arrays.copyOfRange(bytes, 1, length);
		}

		return bytes;
	}

	static String getBase64(byte[] bytes){
		return encode(bytes);
	}
	public static final String encode(byte[] d)
	{
		if (d == null) return null;
		byte data[] = new byte[d.length+2];
		System.arraycopy(d, 0, data, 0, d.length);
		byte dest[] = new byte[(data.length/3)*4];

		// 3-byte to 4-byte conversion
		for (int sidx = 0, didx=0; sidx < d.length; sidx += 3, didx += 4)
		{
			dest[didx]   = (byte) ((data[sidx] >>> 2) & 077);
			dest[didx+1] = (byte) ((data[sidx+1] >>> 4) & 017 |
					(data[sidx] << 4) & 077);
			dest[didx+2] = (byte) ((data[sidx+2] >>> 6) & 003 |
					(data[sidx+1] << 2) & 077);
			dest[didx+3] = (byte) (data[sidx+2] & 077);
		}

		// 0-63 to ascii printable conversion
		for (int idx = 0; idx <dest.length; idx++)
		{
			if (dest[idx] < 26)     dest[idx] = (byte)(dest[idx] + 'A');
			else if (dest[idx] < 52)  dest[idx] = (byte)(dest[idx] + 'a' - 26);
			else if (dest[idx] < 62)  dest[idx] = (byte)(dest[idx] + '0' - 52);
			else if (dest[idx] < 63)  dest[idx] = (byte)'+';
			else            dest[idx] = (byte)'/';
		}

		for (int idx = dest.length-1; idx > (d.length*4)/3; idx--)
		{
			dest[idx] = (byte)'=';
		}
		return new String(dest);
	}

	static String getPrivateKeyAsXml(PrivateKey privateKey) throws Exception{
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPrivateCrtKeySpec spec = keyFactory.getKeySpec(privateKey, RSAPrivateCrtKeySpec.class);
		StringBuilder sb = new StringBuilder();

		sb.append("<RSAKeyValue>" + System.getProperty("line.separator"));
		sb.append(getElement("Modulus", spec.getModulus()));
		sb.append(getElement("Exponent", spec.getPublicExponent()));
		sb.append(getElement("P", spec.getPrimeP()));
		sb.append(getElement("Q", spec.getPrimeQ()));
		sb.append(getElement("DP", spec.getPrimeExponentP()));
		sb.append(getElement("DQ", spec.getPrimeExponentQ()));
		sb.append(getElement("InverseQ", spec.getCrtCoefficient()));
		sb.append(getElement("D", spec.getPrivateExponent()));
		sb.append("</RSAKeyValue>");

		return sb.toString();
	}


	static String getPublicKeyAsXml(PublicKey publicKey) throws Exception{
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPublicKeySpec spec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		StringBuilder sb = new StringBuilder();

		sb.append("<RSAKeyValue>" + System.getProperty("line.separator"));
		sb.append(getElement("Modulus", spec.getModulus()));
		sb.append(getElement("Exponent", spec.getPublicExponent()));
		sb.append("</RSAKeyValue>");

		return sb.toString();
	}

	static void writeFile(String text, String filename) {

		File file = new File(filename);
		try {
			if (file.getParentFile() != null) {
				file.getParentFile().mkdirs();
			}
			if(!file.exists()){
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(text);
				bw.close();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
