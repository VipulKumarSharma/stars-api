package com.mind.wsclient;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.mind.api.stars.common.Constants;
import com.mind.api.stars.global.BaseDAO;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.wsclient.errormail.WSerrormail;
import com.mind.wsclient.matawebservice.TicketingServiceSoapProxy;
import com.mind.wsclient.wslogging.ResponseObject;
import com.mind.wsclient.wslogging.WSLoggingUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WSClient extends BaseDAO
{

    @Autowired
    private WSLoggingUtility wsLoggingUtility;

    @Autowired
    WSerrormail objerrormail;

    /***
     * method is to send information on request is received by last approver i.e.
     * MATA
     * 
     * @return reply
     */
    public String pushStarsReqDetailsToMATA(String endpoint, String xmlstring, String methodname, String strTravelId, String strSiteId)
    {

        java.lang.String reply = "";
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, xmlstring);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java - pushSiteDetailToMATAERP()");
        }
        String requestBeanResponse = jObj.toString();
        String responseBeanResponse = "";
        try
        {

            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.SEND_STARS_REQDETAIL_TOERP, Constants.KEY_FAILED, strSiteId);
        } catch (Exception ex)
        {
            log.error("*********Error occurred pushStarsReqDetailsToMATA() method of MATA in WSClient.java********");
            if (endpoint != null && !endpoint.equals("temp"))
            {

                objerrormail.sendErrorMail(endpoint, ex.getMessage(), methodname, strTravelId, "", strSiteId);
            }
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.SEND_STARS_REQDETAIL_TOERP, Constants.KEY_FAILED, strSiteId);

        }
        return reply;
    }

    /***
     * To Test Web service connection
     * 
     * @param path URL of web service
     * @return
     */
    public String testWSConnection(String path)
    {

        String status = "N";
        URL url;
        InputStream is = null;
        DataInputStream dis = null;
        String line;
        try
        {
            url = new URL(path);
            is = url.openStream();
            dis = new DataInputStream(new BufferedInputStream(is));
            boolean check = false;
            while ((line = dis.readLine()) != null)
            {
                if (line.contains("wsdl:definitions"))
                {
                    check = true;
                    break;
                }
            }
            if (check)
            {
                log.info("success");
            } else
            {
                log.info("fail");
            }
        } catch (MalformedURLException mue)
        {
            log.error(Constants.WEB_SERVICE_NOT_FOUND);
            log.error("Error in WsClient.java -> testWSConnection()====" + mue);
            ExceptionLogger.logExceptionToDB(mue);

        } catch (IOException ioe)
        {

            log.error(Constants.WEB_SERVICE_NOT_FOUND);
            log.error("Error in WsClient.java testWSConnection()====" + ioe);
            ExceptionLogger.logExceptionToDB(ioe);
        } finally
        {
            try
            {
                if (is != null)
                    is.close();
                if (dis != null)
                    dis.close();
            } catch (IOException ioe)
            {
                log.error(Constants.WEB_SERVICE_NOT_FOUND);
                log.error("Error in WsClient.java -> testWSConnection()====" + ioe);
                ExceptionLogger.logExceptionToDB(ioe);
            }

        }
        return status;
    }

    /**
     * To push site data to site web service
     * 
     * @return reply
     */
    public String pushSiteDetailToMATAERP(String endpoint, String xmlstring, String methodname, String strsiteId)
    {

        java.lang.String reply = "";
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, xmlstring);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> pushSiteDetailToMATAERP()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {
            TicketingServiceSoapProxy obj = new TicketingServiceSoapProxy();
            obj.setEndpoint(endpoint);
            reply = obj.sendSiteDetailToERP(xmlstring);
            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put(Constants.RESPONSE_OBJECT_WS, responseObject.toString());
            responseBeanResponse = jRespObj.toString();
            if (reply != null && !reply.substring(0, reply.indexOf('|')).equalsIgnoreCase("True"))
            {
                WSerrormail objerrormail = new WSerrormail();
                objerrormail.sendErrorMail(endpoint, reply, methodname, strsiteId, "", strsiteId);
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.SEND_SITE_DETAILTO_ERP, Constants.KEY_FAILED, strsiteId);
            } else
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.SEND_SITE_DETAILTO_ERP, Constants.KEY_SUCCESS, strsiteId);
            }
        } catch (Exception ex)
        {
            log.error("*********Error occurred in pushSiteDetailToMATAERP() method of MATA in WSClient.java********");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), methodname, strsiteId, "", strsiteId);
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.SEND_SITE_DETAILTO_ERP, Constants.KEY_FAILED, strsiteId);
            ExceptionLogger.logExceptionToDB(ex);
        }
        return reply;
    }

    /**
     * To Send Information on request is recieved by last approver i.e. MATA,To push
     * data to ERP when request is submit to workflow.
     * 
     * @return reply
     */
    public String pushStarsReqDetailsToERP(String endpoint, String methodname, String strXML, String strTravelId, String strSiteId)
    {

        java.lang.String reply = "";
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> pushStarsReqDetailsToERP()");
        }
        String requestBeanResponse = jObj.toString();
        String responseBeanResponse = "";
        try
        {

            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_STARS_REQDETAIL_TOERP, Constants.KEY_FAILED, strSiteId);
        } catch (Exception ex)
        {
            log.error("*********Error occurred in pushStarsReqDetailsToERP() method of eMPro in WSClient.java********");
            if (endpoint != null && !endpoint.equals("temp"))
            {
                objerrormail.sendErrorMail(endpoint, ex.getMessage(), methodname, strTravelId, "", strSiteId);
            }
            ExceptionLogger.logExceptionToDB(ex);
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_STARS_REQDETAIL_TOERP, Constants.KEY_FAILED, strSiteId);
        }
        return reply;
    }

    /**
     * To Push Site Data To Site Web Service
     * 
     * @return reply
     */
    public String pushSiteDetailToEMPROERP(String company, String endpoint, String methodname, int intSiteID, String strXML, String strXMLParam)
    {

        java.lang.String reply = "";
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> pushSiteDetailToMATAERP()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {

            // if block added by manoj chand on 04 oct 2013 to call different webservice
            if (company.equalsIgnoreCase("MATE"))
            {
                com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
                reply = obj.sendSiteDetailToERP(strXML, strXMLParam);
            }
            if (company.equalsIgnoreCase("MIND"))
            {
                com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
                reply = obj.sendSiteDetailToERP(strXML, strXMLParam);
            }

            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put("ResponseObject", responseObject.toString());
            responseBeanResponse = jRespObj.toString();

            if (reply != null && reply.substring(0, reply.indexOf('¤')).equalsIgnoreCase("Y") && reply.substring(reply.indexOf('¤') + 1, reply.length()).equalsIgnoreCase("SAVED"))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_SITE_DETAILTO_ERP, "Success", intSiteID + "");
            } else if (reply != null && reply.substring(0, reply.indexOf('¤')).equalsIgnoreCase("N") && reply.substring(reply.indexOf('¤') + 1, reply.length()).equalsIgnoreCase("Already Saved"))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_SITE_DETAILTO_ERP, "Success", intSiteID + "");
            } else if (reply != null && reply.substring(0, reply.indexOf('¤')).equalsIgnoreCase("N") && !reply.substring(reply.indexOf('¤') + 1, reply.length()).equalsIgnoreCase("Already Saved"))
            {
                objerrormail.sendErrorMail(endpoint, reply, methodname, intSiteID + "", "", intSiteID + "");
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_SITE_DETAILTO_ERP, "Failed", intSiteID + "");
            }
        } catch (Exception ex)
        {
            log.error("*********Error occurred in pushSiteDetailToEMPROERP() method of eMPro in WSClient.java********");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), methodname, intSiteID + "", "", intSiteID + "");
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.SEND_SITE_DETAILTO_ERP, "Failed", intSiteID + "");
        }
        return reply;
    }

    /**
     * To call canCancelRequest method of web service
     * 
     * @return
     */
    public String[] canCancelRequestMATA(String endpoint, String travelId, int intSiteID, String strXML, String strXMLParam)
    {

        String reply = "";
        String strcancelflag = "";
        String[] strOutput = null;
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> canCancelRequestMATA()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {

            TicketingServiceSoapProxy obj = new TicketingServiceSoapProxy();
            obj.setEndpoint(endpoint);
            reply = obj.canCancelRequest(strXML, strXMLParam);
            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put(Constants.RESPONSE_OBJECT_WS, responseObject.toString());
            responseBeanResponse = jRespObj.toString();

            Parser objparse = new Parser();
            strOutput = objparse.parsexmlstring(reply);
            strcancelflag = strOutput[0];

            if (strcancelflag != null && !strcancelflag.equals("") && (strcancelflag.equalsIgnoreCase("YES") || strcancelflag.equalsIgnoreCase("NO")))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.CAN_CANCEL_REQUEST, Constants.KEY_SUCCESS, intSiteID + "");
            } else
            {
                strOutput[0] = "NO";
                strOutput[1] = "Wrong XML output string";
            }
        } catch (Exception ex)
        {
            if (strOutput == null)
                strOutput = new String[1];
            log.error("*********Error occurred in canCancelRequestMATA() method of MATA in WSClient.java********");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), Constants.CAN_CANCEL_REQUEST, travelId, "", intSiteID + "");
            strOutput[0] = "NO";
            strOutput[1] = "Error occured while calling web service.";
            ExceptionLogger.logExceptionToDB(ex);
        }
        return strOutput;
    }

    /**
     * To call canCancelRequest method of web service
     */
    public String[] canCancelRequestERP(String company, String endpoint, String travelId, int intSiteID, String strXML, String strXMLParam)
    {

        String reply = "";
        String strcancelflag = "";
        String[] strOutput = null;
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> canCancelRequestERP()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {

            if (company.equalsIgnoreCase("MATE"))
            {
                com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
                reply = obj.canCancelRequest(strXML, strXMLParam);
            }
            if (company.equalsIgnoreCase("MIND"))
            {
                com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
            }

            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put(Constants.RESPONSE_OBJECT_WS, responseObject.toString());
            responseBeanResponse = jRespObj.toString();

            Parser objparse = new Parser();
            strOutput = objparse.parsexmlstring(reply);
            strcancelflag = strOutput[0];

            if (!strcancelflag.equals("") && (strcancelflag.equalsIgnoreCase("YES") || strcancelflag.equalsIgnoreCase("NO")))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.CAN_CANCEL_REQUEST, Constants.KEY_SUCCESS, intSiteID + "");
            }

        } catch (Exception ex)
        {
            log.error("*********Error occurred in canCancelRequestERP() method of eMPro in WSClient.java********");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), Constants.CAN_CANCEL_REQUEST, travelId, "", intSiteID + "");
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.CAN_CANCEL_REQUEST, Constants.KEY_FAILED, intSiteID + "");
            ExceptionLogger.logExceptionToDB(ex);
        }
        return strOutput;
    }

    /**
     * To call canCancelRequest method of web service
     * 
     * @param company
     * @param endpoint
     * @param travelId
     * @param intSiteID
     * @param strXML
     * @param strXMLParam
     * @return
     */
    public String[] cancelRequestMATA(String endpoint, String travelId, int intSiteID, String strXML, String strXMLParam, String strTravelType)
    {

        String reply = "";
        String strcancelflag = "";
        String[] strOutput = null;
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> cancelRequestMATA()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {

            TicketingServiceSoapProxy obj = new TicketingServiceSoapProxy();
            obj.setEndpoint(endpoint);
            reply = obj.cancelRequest(strXML, strXMLParam);

            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put(Constants.RESPONSE_OBJECT_WS, responseObject.toString());
            responseBeanResponse = jRespObj.toString();

            Parser objparse = new Parser();
            strOutput = objparse.parsexmlstring(reply);
            strcancelflag = strOutput[0];

            if (strcancelflag != null && !strcancelflag.equals("") && (strcancelflag.equalsIgnoreCase("YES") || strcancelflag.equalsIgnoreCase("NO")))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, "MATA", Constants.CANCEL_REQUEST_WS, Constants.KEY_SUCCESS, intSiteID + "");
                if (strcancelflag.equalsIgnoreCase("NO"))
                {
                    updatereqstatus(travelId, strTravelType);
                }
            } else
            {
                strOutput[0] = "NO";
                strOutput[1] = "Wrong XML output string";
                updatereqstatus(travelId, strTravelType);
            }
        } catch (Exception ex)
        {
            if (strOutput == null)
                strOutput = new String[1];
            log.error("*********Error occurred in CancelRequestMATA() method of MATA in WSClient.java********");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), Constants.CANCEL_REQUEST_WS, travelId, "", intSiteID + "");
            strOutput[0] = "NO";
            strOutput[1] = "Error occured while calling web service.";
            updatereqstatus(travelId, strTravelType);
            ExceptionLogger.logExceptionToDB(ex);
        }
        return strOutput;
    }

    /**
     * To call canCancelRequest method of web service
     * 
     * @param company
     * @param endpoint
     * @param travelId
     * @param intSiteID
     * @param strXML
     * @param strXMLParam
     * @return
     */
    public String[] cancelRequestERP(String company, String endpoint, String travelId, int intSiteID, String strXML, String strXMLParam)
    {

        String reply = "";
        String strcancelflag = "";
        String[] strOutput = null;
        JSONObject jObj = new JSONObject();
        try
        {
            jObj.put(Constants.RESPONSE_XML, strXML);
        } catch (JSONException e1)
        {
            ExceptionLogger.logExceptionToDB(e1);
            log.error("error in putting xmlstring json data WSClient.java -> cancelRequestERP()");
        }
        String requestBeanResponse = jObj.toString();
        ResponseObject responseObject = new ResponseObject();
        String responseBeanResponse = "";
        try
        {

            if (company.equalsIgnoreCase("MATE"))
            {
                com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mate.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
                reply = obj.cancelRequest(strXML, strXMLParam);
            }
            if (company.equalsIgnoreCase("MIND"))
            {
                com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy obj = new com.mind.wsclient.erpwebservice.mind.ServiceSoapProxy();
                obj.setEndpoint(endpoint);
            }

            responseObject.setStrErrorMessage(reply);
            // logging
            JSONObject jRespObj = new JSONObject();
            jRespObj.put(Constants.RESPONSE_OBJECT_WS, responseObject.toString());
            responseBeanResponse = jRespObj.toString();

            Parser objparse = new Parser();
            strOutput = objparse.parsexmlstring(reply);
            strcancelflag = strOutput[0];

            if (strcancelflag != null && !strcancelflag.equals("") && (strcancelflag.equalsIgnoreCase("YES") || strcancelflag.equalsIgnoreCase("NO")))
            {
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.CANCEL_REQUEST_WS, Constants.KEY_SUCCESS, intSiteID + "");
            } else
            {
                objerrormail.sendErrorMail(endpoint, reply, Constants.CANCEL_REQUEST_WS, travelId, "", intSiteID + "");
                wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.CANCEL_REQUEST_WS, Constants.KEY_FAILED, intSiteID + "");
            }
        } catch (Exception ex)
        {
            log.error("Error occurred in CancelRequestERP() method of eMPro in WSClient.java");
            objerrormail.sendErrorMail(endpoint, ex.getMessage(), Constants.CANCEL_REQUEST_WS, travelId, "", intSiteID + "");
            wsLoggingUtility.logRequestResponse(requestBeanResponse, responseBeanResponse, Constants.EM_PRO, Constants.CANCEL_REQUEST_WS, Constants.KEY_FAILED, intSiteID + "");
            ExceptionLogger.logExceptionToDB(ex);

        }
        return strOutput;
    }

    public void updatereqstatus(String travelid, String traveltype)
    {

        String strSql = "UPDATE dbo.T_TRAVEL_STATUS set TRAVEL_STATUS_ID = '10' WHERE TRAVEL_TYPE=? AND TRAVEL_ID=?";
        try
        {
            jdbcTemplate.update(strSql, new Object[] { traveltype, travelid });
        } catch (Exception ex)
        {
            log.error("error at WSClient.java -> updatereqstatus" + ex);
            ExceptionLogger.logExceptionToDB(ex);
        }
    }
}
