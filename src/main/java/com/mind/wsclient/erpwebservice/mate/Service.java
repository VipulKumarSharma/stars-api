package com.mind.wsclient.erpwebservice.mate;

public interface Service extends javax.xml.rpc.Service {
    public java.lang.String getServiceSoapAddress();

    public com.mind.wsclient.erpwebservice.mate.ServiceSoap getServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.mind.wsclient.erpwebservice.mate.ServiceSoap getServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
