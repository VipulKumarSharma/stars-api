/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.erpwebservice.mate;

public interface ServiceSoap extends java.rmi.Remote {
    public java.lang.String sendStarsReqDetailToERP(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String setProjectFreezed(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String allTicketInfo(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String getAdvanceDetail(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String sendSiteDetailToERP(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String sendInvoiceDetailToERP(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String sendTESDetailToERP(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String canCancelRequest(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String list_ProjectCodes(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String cancelRequest(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String getDataFromeMproEasy(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String sendInvoiceDetailToERP_LOG(java.lang.String xmlstring, java.lang.String UNIT_CODE, java.lang.String MODE) throws java.rmi.RemoteException;
    public java.lang.String callGroupCompInvoice(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String sendContractDetailToERP(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String callGroupCompInvoice_MIND(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
}
