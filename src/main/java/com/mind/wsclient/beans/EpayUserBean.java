package com.mind.wsclient.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)

public class EpayUserBean {

	@XmlElement(name = "STATUS")
	private String status;
	
	@XmlElement(name = "MESSAGE")
	private String message;
	
	@XmlElement(name = "COMPANYCODE")
	private String unit;
	
	@XmlElement(name = "EMPLOYEECODE")
	private String empCode;
	
	@XmlElement(name = "FIRSTNAME_PASSPORT")
	private String passportFirstName = "" ;
	
	@XmlElement(name = "MIDDLENAME_PASSPORT")
	private String passportMiddleName = "" ;
	
	@XmlElement(name = "LASTNAME_PASSPORT")
	private String passportLastName = "" ;
	
	@XmlElement(name = "FIRSTNAME")
	private String firstName = "" ;
	
	@XmlElement(name = "MIDDLENAME")
	private String middleName = "" ;
	
	@XmlElement(name = "LASTNAME")
	private String lastName = "" ;
	
	@XmlElement(name = "Passport_No")
	private String passportNo = "" ;

	@XmlElement(name = "Passport_Issue_Date")
	private String passportIssueDate = "" ;
	
	@XmlElement(name = "Passport_Expiry_Date")
	private String passportExpiryDate = "" ;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getPassportFirstName() {
		return passportFirstName;
	}

	public void setPassportFirstName(String passportFirstName) {
		this.passportFirstName = passportFirstName;
	}

	public String getPassportMiddleName() {
		return passportMiddleName;
	}

	public void setPassportMiddleName(String passportMiddleName) {
		this.passportMiddleName = passportMiddleName;
	}

	public String getPassportLastName() {
		return passportLastName;
	}

	public void setPassportLastName(String passportLastName) {
		this.passportLastName = passportLastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
}
