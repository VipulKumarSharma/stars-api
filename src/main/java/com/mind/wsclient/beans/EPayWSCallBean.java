package com.mind.wsclient.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ePay")

public class EPayWSCallBean {

	@XmlElement(name="Employee")
    private EpayUserBean userDetailsBean;

	public EpayUserBean getUserDetailsBean() {
		return userDetailsBean;
	}

	public void setUserDetailsBean(EpayUserBean userDetailsBean) {
		this.userDetailsBean = userDetailsBean;
	}
	
}
