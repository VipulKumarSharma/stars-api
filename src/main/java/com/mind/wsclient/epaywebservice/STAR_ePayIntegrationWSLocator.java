/**
 * STAR_ePayIntegrationWSLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.epaywebservice;

public class STAR_ePayIntegrationWSLocator extends org.apache.axis.client.Service implements com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWS {

    public STAR_ePayIntegrationWSLocator() {
    }


    public STAR_ePayIntegrationWSLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public STAR_ePayIntegrationWSLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for STAR_ePayIntegrationWSSoap
    private java.lang.String STAR_ePayIntegrationWSSoap_address = "http://172.29.55.179/easy_empro_ws/STAR_ePayIntegrationWS.asmx";

    public java.lang.String getSTAR_ePayIntegrationWSSoapAddress() {
        return STAR_ePayIntegrationWSSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String STAR_ePayIntegrationWSSoapWSDDServiceName = "STAR_ePayIntegrationWSSoap";

    public java.lang.String getSTAR_ePayIntegrationWSSoapWSDDServiceName() {
        return STAR_ePayIntegrationWSSoapWSDDServiceName;
    }

    public void setSTAR_ePayIntegrationWSSoapWSDDServiceName(java.lang.String name) {
        STAR_ePayIntegrationWSSoapWSDDServiceName = name;
    }

    public com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap getSTAR_ePayIntegrationWSSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(STAR_ePayIntegrationWSSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSTAR_ePayIntegrationWSSoap(endpoint);
    }

    public com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap getSTAR_ePayIntegrationWSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
        	com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoapStub _stub = new com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoapStub(portAddress, this);
            _stub.setPortName(getSTAR_ePayIntegrationWSSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSTAR_ePayIntegrationWSSoapEndpointAddress(java.lang.String address) {
        STAR_ePayIntegrationWSSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap.class.isAssignableFrom(serviceEndpointInterface)) {
            	com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoapStub _stub = new com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoapStub(new java.net.URL(STAR_ePayIntegrationWSSoap_address), this);
                _stub.setPortName(getSTAR_ePayIntegrationWSSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("STAR_ePayIntegrationWSSoap".equals(inputPortName)) {
            return getSTAR_ePayIntegrationWSSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "STAR_ePayIntegrationWS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "STAR_ePayIntegrationWSSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("STAR_ePayIntegrationWSSoap".equals(portName)) {
            setSTAR_ePayIntegrationWSSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
