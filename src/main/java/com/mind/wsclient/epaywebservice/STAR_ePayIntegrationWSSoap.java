/**
 * STAR_ePayIntegrationWSSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.epaywebservice;

public interface STAR_ePayIntegrationWSSoap extends java.rmi.Remote {
    public java.lang.String get_EmployeeInfo(java.lang.String sXMLString, java.lang.String sSourceIPAddress) throws java.rmi.RemoteException;
}
