/**
 * STAR_ePayIntegrationWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.epaywebservice;

public interface STAR_ePayIntegrationWS extends javax.xml.rpc.Service {
    public java.lang.String getSTAR_ePayIntegrationWSSoapAddress();

    public com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap getSTAR_ePayIntegrationWSSoap() throws javax.xml.rpc.ServiceException;

    public com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap getSTAR_ePayIntegrationWSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
