package com.mind.wsclient.epaywebservice;

public class STAR_ePayIntegrationWSSoapProxy implements com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap {
  private String _endpoint = null;
  private com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap sTAR_ePayIntegrationWSSoap = null;
  
  public STAR_ePayIntegrationWSSoapProxy() {
    _initSTAR_ePayIntegrationWSSoapProxy();
  }
  
  public STAR_ePayIntegrationWSSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSTAR_ePayIntegrationWSSoapProxy();
  }
  
  private void _initSTAR_ePayIntegrationWSSoapProxy() {
    try {
      sTAR_ePayIntegrationWSSoap = (new com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSLocator()).getSTAR_ePayIntegrationWSSoap();
      if (sTAR_ePayIntegrationWSSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sTAR_ePayIntegrationWSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sTAR_ePayIntegrationWSSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sTAR_ePayIntegrationWSSoap != null)
      ((javax.xml.rpc.Stub)sTAR_ePayIntegrationWSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoap getSTAR_ePayIntegrationWSSoap() {
    if (sTAR_ePayIntegrationWSSoap == null)
      _initSTAR_ePayIntegrationWSSoapProxy();
    return sTAR_ePayIntegrationWSSoap;
  }
  
  public java.lang.String get_EmployeeInfo(java.lang.String sXMLString, java.lang.String sSourceIPAddress) throws java.rmi.RemoteException{
    if (sTAR_ePayIntegrationWSSoap == null)
      _initSTAR_ePayIntegrationWSSoapProxy();
    return sTAR_ePayIntegrationWSSoap.get_EmployeeInfo(sXMLString, sSourceIPAddress);
  }
  
  
}