package com.mind.wsclient;

import java.rmi.RemoteException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.wsclient.mrswebservice.MRSServiceSoapProxy;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class MRSWSClient extends BaseDAO{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MRSWSClient obj=new MRSWSClient();
		obj.getTravelHotelDetails("http://172.29.55.243/MRSWebService/MRSService.asmx");
	}
	
	public void getTravelHotelDetails(String endpoint) {
		
		String xmlData = "";
		MRSServiceSoapProxy mrsService = new MRSServiceSoapProxy();
		mrsService.setEndpoint(endpoint);
		
		
		try {
				xmlData = mrsService.THDtlService(); //service called
				//System.out.println("xmlData--> " + xmlData);
				
				if(xmlData != null && !xmlData.equals("")) {
					
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
							.withProcedureName(Procedure.PROC_MRS_ACCOMMODATION.getName());
							
							SqlParameterSource procParams = new MapSqlParameterSource()
							.addValue("P_MRS_DATA", xmlData);
							

							jdbcCall.execute(procParams);
				}
				
		} catch(RemoteException re) {
			log.error("Error in getTravelHotelDetails.........RemoteException" + re);
			re.printStackTrace();
		}  catch(Exception e) {
			log.error("Error in getTravelHotelDetails.........Exception" + e);
			e.printStackTrace();
		} 
	}

}
