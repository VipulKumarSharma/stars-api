package com.mind.wsclient.wslogging;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.ExceptionLogger;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WSLoggingUtility extends AppDbUtility{
	//method to make a entry of request and response on every call 
	
	 public void logRequestResponse(String requestBeanResponse,String responseBeanResponse,String strReqAppName,String methodName,String strCallingStatus,String strSiteId){	   
		 
			try{
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(Procedure.REQUEST_RESP_DATA.getName());
						
						SqlParameterSource procParams = new MapSqlParameterSource()
						.addValue("p_REQ", requestBeanResponse)
						.addValue("p_RESP", responseBeanResponse)
						.addValue("p_METHOD_NAME", methodName)
						.addValue("P_SITEID", strSiteId)
						.addValue("P_SOURCE_SYSTEM", strReqAppName)
						.addValue("P_CALLING_STATUS", strCallingStatus)
						.addValue("P_RESEND_FLAG", "NEW");

						jdbcCall.execute(procParams);
			}catch(Exception e){
				log.error("ERROR in REQUEST_RESP_DATA insertion");
				ExceptionLogger.logExceptionToDB(e);
			}	
	  }
	 
	//method to make a entry of request and response on recall
	 public  void updateStatusOnRecall(String strLogId,String strWSResponse){	   
			try{
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(Procedure.PROC_UPDATE_REQUEST_RESP_DATA.getName());
						
						SqlParameterSource procParams = new MapSqlParameterSource()
						.addValue("P_LOGID", strLogId)
						.addValue("P_WSRESPONSE", strWSResponse)
						.addValue("P_CALLING_STATUS", strWSResponse)
						.addValue("P_RESEND_FLAG", "Success")
						.addValue(ParamConstant.OUTPUT_STATUS, 0);
						
						jdbcCall.execute(procParams);

			}catch(Exception e){
				log.error("ERROR in PROC_UPDATE_REQUEST_RESP_DATA UPDATION CALLING");
				ExceptionLogger.logExceptionToDB(e);
			}	
	  }	
	 
	 public  void updateWSStatusOnRecall(String strLogId, String strWSResponse, String status, String resendFlag) {
		 	
		 try{
				SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
						.withProcedureName(Procedure.PROC_UPDATE_REQUEST_RESP_DATA.getName());
						
						SqlParameterSource procParams = new MapSqlParameterSource()
						.addValue("P_LOGID", strLogId)
						.addValue("P_WSRESPONSE", strWSResponse)
						.addValue("P_CALLING_STATUS", strWSResponse)
						.addValue("P_RESEND_FLAG", "Success")
						.addValue(ParamConstant.OUTPUT_STATUS, 0);

						jdbcCall.execute(procParams);
			}catch(Exception e) {
				log.error("ERROR in updateStatusOnRecall_Failure() : PROC_UPDATE_REQUEST_RESP_DATA UPDATION CALLING");
				ExceptionLogger.logExceptionToDB(e);
			} 
	  }	
}
