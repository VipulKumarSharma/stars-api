package com.mind.wsclient.wslogging;

public class ResponseObject {
	private String strErrorMessage		=	"";

	public String getStrErrorMessage() {
		return strErrorMessage;
	}

	public void setStrErrorMessage(String strErrorMessage) {
		this.strErrorMessage = strErrorMessage;
	}
	public String toString(){
		String jsonResult = "{'strErrorMessage':'"+strErrorMessage+"'} ";		
		return jsonResult;
	}
}
