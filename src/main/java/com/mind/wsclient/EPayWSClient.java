package com.mind.wsclient;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.global.BaseDAO;
import com.mind.wsclient.beans.EPayWSCallBean;
import com.mind.wsclient.epaywebservice.STAR_ePayIntegrationWSSoapProxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EPayWSClient extends BaseDAO{

	public EPayWSCallBean getUserDetails(String unitId, String empCode) {
		
		STAR_ePayIntegrationWSSoapProxy ePayService = new STAR_ePayIntegrationWSSoapProxy();
		ePayService.setEndpoint("http://172.29.55.179/easy_empro_ws/STAR_ePayIntegrationWS.asmx");
		
		EPayWSCallBean userBean		= null;
		String userDataXML 			= "";
		String status 				= "Failed1";
		
		try {
				try {
					String sourceIPAddress 	= "10.32.6.39";
					String WS_XML_INPUT 	= "<Stars><COMPANYCODE>"+unitId+"</COMPANYCODE><EMPLOYEECODE>"+empCode+"</EMPLOYEECODE></Stars>";
					
					userDataXML = ePayService.get_EmployeeInfo(WS_XML_INPUT, sourceIPAddress);
					
				} catch (Exception ex) {
					System.out.println("Error occured while calling ePay web Service : "+ex);
				}
				
				if(userDataXML != null && !userDataXML.equals("")) {
					
					JAXBContext jc = JAXBContext.newInstance(EPayWSCallBean.class);
					Unmarshaller unmarshaller = jc.createUnmarshaller();
					
					File file = new File("EPayUserDetails.xml");
					
					if(!file.exists()) {
						file.createNewFile();
					} else {
						PrintWriter writer = new PrintWriter(file);
						writer.print("");
						writer.close();
					}
					
					FileWriter fileWriter = new FileWriter(file.getName(),true);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(userDataXML);
			        bufferWriter.close();
				
			        userBean = (EPayWSCallBean) unmarshaller.unmarshal(file);
					
			        if (file.exists()) {
			        	file.delete();
			        }
			        
			        if("success".equalsIgnoreCase(userBean.getUserDetailsBean().getStatus())) {
						status	= "Success";
					}
			        
				}
				logRequestResponse(userDataXML, "", "getUserDetails", status , unitId);
				
		} catch(Exception e) {
			logRequestResponse(userDataXML, "", "getUserDetails", "Failed1" , unitId);
			
			log.error("Error in EPayWSClient : getUserDetails.........Exception" + e);
			e.printStackTrace();
		}
		return userBean;
	}
	
	private void logRequestResponse(String requestBeanResponse, String responseBeanResponse, String methodName, String strCallingStatus, String strSiteId){	   
	 	
		try{
			
			
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName(Procedure.REQUEST_RESP_DATA.getName());
					
					SqlParameterSource procParams = new MapSqlParameterSource()
					.addValue("p_REQ", requestBeanResponse)
					.addValue("p_RESP", responseBeanResponse)
					.addValue("p_METHOD_NAME", methodName)
					.addValue("P_SITEID", strSiteId)
					.addValue("P_SOURCE_SYSTEM", "ePay")
					.addValue("P_CALLING_STATUS", strCallingStatus)
					.addValue("P_RESEND_FLAG", "NEW1");

					jdbcCall.execute(procParams);
		
		} catch(Exception e) {
			log.error("ERROR in EPayWSClient:logRequestResponse() : REQUEST_RESP_DATA insertion : "+e);
			
		}	
	}
	
	public static void main(String[] args) {
		
		EPayWSClient obj=new EPayWSClient();
		obj.getUserDetails("7","1163");
	}
}
