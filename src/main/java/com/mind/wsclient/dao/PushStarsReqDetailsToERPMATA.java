package com.mind.wsclient.dao;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.mind.api.stars.common.ParamConstant;
import com.mind.api.stars.enums.Procedure;
import com.mind.api.stars.utility.AppDbUtility;
import com.mind.api.stars.utility.AppUtility;
import com.mind.api.stars.utility.ExceptionLogger;
import com.mind.wsclient.WSClient;
import com.mind.wsclient.errormail.WSerrormail;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class PushStarsReqDetailsToERPMATA extends AppDbUtility{

	@Autowired
	WSClient wsClientObj ;
	public PushStarsReqDetailsToERPMATA() {
		super();
	}
	
	public String pushStarsReqDetailsToERPMATA(String strTravelId, String strTrvType, String sUserid, boolean mataWsCallFlag, boolean erpWsCallFlag)  {
		
		
		log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] block start");
		
		String strSql				= ""; 
		
		String strLastApprover 		= "";
		String strTravellerSiteId 	= "";
		String strApproveStatus		= ""; 
		String strERPWSUrl			= "";
		String strMATAXml			= "";
		String strERPXml			= ""; 
		String strWSResponse		= "Web Service is not called.";
		String strWSUrl				= "";
		try {
			if(strTrvType != null && strTrvType.equalsIgnoreCase("D")) {
				strSql = "SELECT SITE_ID FROM T_TRAVEL_DETAIL_DOM WHERE TRAVEL_ID=? AND STATUS_ID=10";			
			} else if(strTrvType != null && strTrvType.equalsIgnoreCase("I")) {
				strSql = "SELECT SITE_ID FROM T_TRAVEL_DETAIL_INT WHERE TRAVEL_ID=? AND STATUS_ID=10";
			}
			
			Map<String,Object> travellerSiteIdMap=getFirstResult(strSql,new Object[]{strTravelId});
			if(travellerSiteIdMap != null) {	
				strTravellerSiteId = AppUtility.checkNullString(travellerSiteIdMap.get("SITE_ID"));			
			}
			
			strSql 	= "SELECT ltrim(rtrim(Travel_status_id)) as Travel_status_id from dbo.T_TRAVEL_STATUS tts where travel_id=? and tts.TRAVEL_TYPE=? AND tts.STATUS_ID=10";
			Map<String,Object> travelStatusMap = getFirstResult(strSql,new Object[] {strTravelId,strTrvType});
			if(travelStatusMap!=null) {
				strApproveStatus=AppUtility.checkNullString(travelStatusMap.get("Travel_status_id"));
			}
			
			strSql	= " SELECT CASE WHEN EXISTS(SELECT 1 FROM T_APPROVERS WHERE ORDER_ID=(SELECT MAX(ORDER_ID) FROM T_APPROVERS"+ 
					  " WHERE TRAVEL_ID=? AND TRAVEL_TYPE=? AND STATUS_ID=10)"+
					  " AND APPROVE_STATUS=10 AND APPROVER_ID=? AND STATUS_ID=10 AND TRAVEL_ID=? AND TRAVEL_TYPE=?)"+
					  " THEN 'Y' ELSE 'N' END";
			
			strLastApprover=jdbcTemplate.queryForObject(strSql,new Object[] {strTravelId,strTrvType,sUserid,strTravelId,strTrvType}, String.class);
			
			
			if((strApproveStatus.equals("10") && strLastApprover.equalsIgnoreCase("Y")) || (strTravellerSiteId.equals("86") && strTrvType.equalsIgnoreCase("D"))) {
				
				log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch WS URL & XML details block start");
				
				if(strTrvType.equalsIgnoreCase("D")) {
					strSql= " select mc.COMPANY_NAME,mc.MATA_WS_URL,mc.ERP_WS_URL,ttdd.TRAVEL_REQ_NO,ttdd.REASON_FOR_TRAVEL, "+
				 			" ttdd.C_USERID,mu.EMP_CODE,ttdd.TOTAL_AMOUNT,isnull(ttdd.Group_Travel_Flag,'N') as Group_Travel_Flag, "+
							" isnull(ttdd.TA_CURRENCY,'') as TA_CURRENCY,ISNULL(mc.ERP_XML,'') AS ERP_XML,ISNULL(convert(varchar(12), "+
				 			" ttdd.TRAVEL_DATE,106),'-') as DEP_DATE,CASE WHEN (CONVERT(VARCHAR(10), TRJDD.TRAVEL_DATE, 20)) IS NULL "+
							" OR CONVERT(VARCHAR(10), TRJDD.TRAVEL_DATE, 20) = '1900-01-01' THEN CONVERT(VARCHAR(12), "+
				 			" MAX(TJDD.TRAVEL_DATE),106) ELSE CONVERT(VARCHAR(12),TRJDD.TRAVEL_DATE, 106) END as RET_DATE "+
							" FROM m_site "+
							" INNER JOIN dbo.T_TRAVEL_DETAIL_DOM TTDD (NOLOCK) ON M_SITE.SITE_ID=TTDD.SITE_ID "+ 
							" INNER JOIN  T_JOURNEY_DETAILS_DOM TJDD (NOLOCK)  ON TTDD.TRAVEL_ID = TJDD.TRAVEL_ID "+
							" LEFT OUTER JOIN T_RET_JOURNEY_DETAILS_DOM TRJDD (NOLOCK)  ON TTDD.TRAVEL_ID = TRJDD.TRAVEL_ID "+
							" AND TRJDD.STATUS_ID=10 "+ 
							" INNER JOIN dbo.M_USERINFO mu on mu.USERID=ttdd.TRAVELLER_ID "+
							" INNER JOIN dbo.M_COMPANY mc on mc.CID=m_site.COMPANY_ID "+
							" WHERE MC.STATUS_ID=10 AND M_SITE.STATUS_ID=10 AND TTDD.STATUS_ID=10 AND MU.STATUS_ID=10 "+
							" AND TJDD.STATUS_ID=10 "+
							" AND TTDD.TRAVEL_ID=? "+
							" GROUP BY MC.COMPANY_NAME,MC.MATA_WS_URL,MC.ERP_WS_URL,TTDD.TRAVEL_REQ_NO,"+
							" TTDD.REASON_FOR_TRAVEL,TTDD.C_USERID,MU.EMP_CODE,TTDD.TOTAL_AMOUNT,"+
							" TTDD.GROUP_TRAVEL_FLAG,TTDD.TA_CURRENCY,mc.ERP_XML,ttdd.TRAVEL_DATE,TRJDD.TRAVEL_DATE";
				
				} else if(strTrvType.equalsIgnoreCase("I")) {
					strSql= " select mc.COMPANY_NAME,mc.MATA_WS_URL,mc.ERP_WS_URL,ttdi.TRAVEL_REQ_NO,ttdi.REASON_FOR_TRAVEL, "+
				 			" ttdi.C_USERID,mu.EMP_CODE,ttdi.TOTAL_AMOUNT,isnull(ttdi.Group_Travel_Flag,'N') as Group_Travel_Flag, "+
							" isnull(ttdi.TA_CURRENCY,'') as TA_CURRENCY,ISNULL(mc.ERP_XML,'') AS ERP_XML,ISNULL(convert(varchar(12), "+
				 			" ttdi.TRAVEL_DATE,106),'-') as DEP_DATE,CASE WHEN (CONVERT(VARCHAR(10), TRJDI.TRAVEL_DATE, 20)) IS NULL "+
							" OR CONVERT(VARCHAR(10), TRJDI.TRAVEL_DATE, 20) = '1900-01-01' THEN CONVERT(VARCHAR(12), "+
				 			" MAX(TJDI.TRAVEL_DATE),106) ELSE CONVERT(VARCHAR(12),TRJDI.TRAVEL_DATE, 106) END as RET_DATE "+
							" FROM m_site "+
							" INNER JOIN dbo.T_TRAVEL_DETAIL_INT TTDI (NOLOCK) ON M_SITE.SITE_ID=ttdi.SITE_ID "+ 
							" INNER JOIN  T_JOURNEY_DETAILS_INT TJDI (NOLOCK)  ON ttdi.TRAVEL_ID = TJDI.TRAVEL_ID "+
							" LEFT OUTER JOIN T_RET_JOURNEY_DETAILS_INT TRJDI (NOLOCK)  ON ttdi.TRAVEL_ID = TRJDI.TRAVEL_ID "+
							" INNER JOIN dbo.M_USERINFO mu on mu.USERID=ttdi.TRAVELLER_ID "+
							" INNER JOIN dbo.M_COMPANY mc on mc.CID=m_site.COMPANY_ID "+
							" WHERE MC.STATUS_ID=10 AND M_SITE.STATUS_ID=10 AND ttdi.STATUS_ID=10 AND MU.STATUS_ID=10 "+
							" AND TJDI.STATUS_ID=10 "+
							" AND ttdi.TRAVEL_ID=? "+
							" GROUP BY MC.COMPANY_NAME,MC.MATA_WS_URL,MC.ERP_WS_URL,ttdi.TRAVEL_REQ_NO,"+
							" ttdi.REASON_FOR_TRAVEL,ttdi.C_USERID,MU.EMP_CODE,ttdi.TOTAL_AMOUNT,"+
							" ttdi.GROUP_TRAVEL_FLAG,ttdi.TA_CURRENCY,mc.ERP_XML,ttdi.TRAVEL_DATE,TRJDI.TRAVEL_DATE";
				}
				
				Map<String,Object> companyDetails = getFirstResult(strSql,new Object[]{strTravelId});
				if(companyDetails != null) {
					strWSUrl		= AppUtility.getResultFromMap(companyDetails, "MATA_WS_URL");
					strERPWSUrl		= AppUtility.getResultFromMap(companyDetails, "ERP_WS_URL");
				}
				log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch WS URL & XML details block end");
				
				if(mataWsCallFlag && !strWSUrl.equals("") && !strMATAXml.equals("")) {
					log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch XML data for MATA WS block start");
					
					Map< String,Object> resultMap = null;
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
							.withProcedureName(Procedure.SPG_ERP_TRAVEL_REQUISITION_XML.getName()).withReturnValue();
							
							SqlParameterSource procParams = new MapSqlParameterSource()
							.addValue(ParamConstant.P_TRAVEL_ID, strTravelId)
							.addValue(ParamConstant.P_TRAVEL_TYPE, strTrvType)
							.addValue(ParamConstant.CONN_ID, "0");
							
							
							resultMap=jdbcCall.execute(procParams);
							
					
					
					
					if(resultMap!=null){
						
						List<Map<String, Object>> strMATAXmlList = (List<Map<String, Object>>)resultMap.get("#result-set-1");
						if(strMATAXmlList != null && !strMATAXmlList.isEmpty()) {
							Map<String,Object> strERPXMLMap = strMATAXmlList.get(0);
							strMATAXml = AppUtility.checkNullString(strERPXMLMap.get(ParamConstant.XML));
						}
						else {
							strMATAXml = null;
						}
					}
				    log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch XML data for MATA WS block end");
				    
				    log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] MATA WS calling start");
					strWSResponse = wsClientObj.pushStarsReqDetailsToMATA(strWSUrl, strMATAXml, "sendStarsReqDetailToERP", strTravelId, strTravellerSiteId);
					log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] MATA WS calling end");
				}
				
				if(erpWsCallFlag && !strERPWSUrl.equals("")) {
					log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch XML data for ERP WS block start");
					
					Map< String,Object> resultMap = null;
					SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
							.withProcedureName(Procedure.SPG_ERP_TRAVEL_REQUISITION_XML.getName()).withReturnValue();
							
							SqlParameterSource procParams = new MapSqlParameterSource()
							.addValue(ParamConstant.P_TRAVEL_ID, strTravelId)
							.addValue(ParamConstant.P_TRAVEL_TYPE, strTrvType)
							.addValue(ParamConstant.CONN_ID, "0");
							
							
					resultMap=jdbcCall.execute(procParams);
					
					if(resultMap!=null){
						List<Map<String, Object>> strErpXMLList = (List<Map<String, Object>>)resultMap.get("#result-set-1");
						if(strErpXMLList != null &&  !strErpXMLList.isEmpty()) {
							Map<String,Object> strERPXMLMap = strErpXMLList.get(0);
							strERPXml = AppUtility.checkNullString(strERPXMLMap.get(ParamConstant.XML));
						}
						else {
							strERPXml = null;
						}
					}
				    log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] fetch XML data for ERP WS block end");
				    
				    log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] ERP WS calling start");
					strWSResponse = wsClientObj.pushStarsReqDetailsToERP( strERPWSUrl, "sendStarsReqDetailToERP", strERPXml, strTravelId, strTravellerSiteId);
					log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] ERP WS calling end");
				}
			}
			log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] block end.");
			
		} catch(Exception e) {
			log.info("PushStarsReqDetailsToERPMATA - [pushStarsReqDetailsToERPMATA] error occured :: "+e);
			ExceptionLogger.logExceptionToDB(e);
			WSerrormail wsErrormailObj = new WSerrormail();
			wsErrormailObj.sendErrorMail("", e.getMessage(), "", strTravelId, "PushStarsReqDetailsToERPMATA.java", strTravellerSiteId);
		}
		return strWSResponse;
	}
	}
