/**
 * MRSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.mrswebservice;

public interface MRSService extends javax.xml.rpc.Service {
    public java.lang.String getMRSServiceSoapAddress();

    public MRSServiceSoap getMRSServiceSoap() throws javax.xml.rpc.ServiceException;

    public MRSServiceSoap getMRSServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
