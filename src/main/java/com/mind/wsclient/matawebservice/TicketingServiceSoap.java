/**
 * TicketingServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.matawebservice;

public interface TicketingServiceSoap extends java.rmi.Remote {
    public java.lang.String getAdvanceDetail(java.lang.String strStarsREQ_ID, java.lang.String strEmpCode, java.lang.String strConID) throws java.rmi.RemoteException;
    public java.lang.String sendStarsReqDetailToERP(java.lang.String xmlstring) throws java.rmi.RemoteException;
    public java.lang.String sendSiteDetailToERP(java.lang.String xmlstring) throws java.rmi.RemoteException;
    public java.lang.String sendCompanyDetailToERP(java.lang.String xmlstring) throws java.rmi.RemoteException;
    public java.lang.String sendExpenseEntryStatusToERP(java.lang.String xmlstring) throws java.rmi.RemoteException;
    public java.lang.String canCancelRequest(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String cancelRequest(java.lang.String xmlstring, java.lang.String xmlOptstring) throws java.rmi.RemoteException;
    public java.lang.String getTicketDetailAgainstSTARRefNo(java.lang.String strStarsREQ_ID, java.lang.String strConID) throws java.rmi.RemoteException;
}
