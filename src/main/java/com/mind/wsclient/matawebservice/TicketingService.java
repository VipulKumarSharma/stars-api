/**
 * TicketingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mind.wsclient.matawebservice;

public interface TicketingService extends javax.xml.rpc.Service {
    public java.lang.String getTicketingServiceSoapAddress();

    public com.mind.wsclient.matawebservice.TicketingServiceSoap getTicketingServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.mind.wsclient.matawebservice.TicketingServiceSoap getTicketingServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
